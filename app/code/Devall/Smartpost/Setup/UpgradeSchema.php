<?php

namespace Devall\Smartpost\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @inheritdoc
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        if (\version_compare($context->getVersion(), '1.0.2') === -1) {
            $this->addSmartpostPlaceIdToQuote($setup);
        }

        if (\version_compare($context->getVersion(), '1.0.3') === -1) {
            $this->addPostalAndRouteColumnsToMainTable($setup);
        }
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    protected function addPostalAndRouteColumnsToMainTable(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->addColumn($setup->getTable('smartpost_parcel_points'), 'postalcode', [
            'type' => Table::TYPE_TEXT,
            'comment' => 'Smartpost Postal Code',
            'nullable' => true,
            'after' => 'place_id'
        ]);

        $setup->getConnection()->addColumn($setup->getTable('smartpost_parcel_points'), 'routingcode', [
            'type' => Table::TYPE_TEXT,
            'comment' => 'Smartpost Routing Code',
            'nullable' => true,
            'after' => 'postalcode'
        ]);
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    protected function addSmartpostPlaceIdToQuote(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->addColumn($setup->getTable('quote_address'), 'smartpost_place_id', [
            'type' => Table::TYPE_TEXT,
            'comment' => 'Smartpost Shipping Place Id',
            'nullable' => true,
        ]);
    }
}

<?php

namespace Devall\Smartpost\Setup;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Backend\Boolean;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean as BooleanSource;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Quote\Setup\QuoteSetup;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Sales\Setup\SalesSetup;
use Magento\Sales\Setup\SalesSetupFactory;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var SalesSetupFactory
     */
    private $salesSetupFactory;

    /**
     * @var QuoteSetupFactory
     */
    private $quoteSetupFactory;

    /**
     * @param EavSetupFactory $eavSetupFactory
     * @param SalesSetupFactory $salesSetupFactory
     * @param QuoteSetupFactory $quoteSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        SalesSetupFactory $salesSetupFactory,
        QuoteSetupFactory $quoteSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->salesSetupFactory = $salesSetupFactory;
        $this->quoteSetupFactory = $quoteSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (\version_compare($context->getVersion(), '1.0.4') <= 0) {
            $this->createProductShippingFlagAttributes($setup);
        }

        if (\version_compare($context->getVersion(), '1.0.6') <= 0) {
            $this->createBarcodeAttribute($setup);
        }

        if (\version_compare($context->getVersion(), '1.0.7') <= 0) {
            $this->addTimeWindowAttribute($setup);
        }

        $setup->endSetup();
    }

    /**
     * Create barcode attribute for order entity.
     *
     * @param ModuleDataSetupInterface $setup
     *
     * @return void
     */
    private function createBarcodeAttribute(ModuleDataSetupInterface $setup): void
    {
        /** @var SalesSetup $salesInstaller */
        $salesInstaller = $this->salesSetupFactory->create(['resourceName' => 'sales_setup', 'setup' => $setup]);

        $salesInstaller->addAttribute('order', 'barcodes', [
            'type' => 'varchar',
            'comment' => 'Barcodes comma separated data',
            'nullable' => true,
        ]);
    }

    /**
     * Add time window attribute to quote address and order
     *
     * @param ModuleDataSetupInterface $setup
     */
    private function addTimeWindowAttribute(ModuleDataSetupInterface $setup): void
    {
        /** @var QuoteSetup $quoteInstaller */
        $quoteInstaller = $this->quoteSetupFactory->create(['resourceName' => 'quote_setup', 'setup' => $setup]);
        /** @var SalesSetup $salesInstaller */
        $salesInstaller = $this->salesSetupFactory->create(['resourceName' => 'sales_setup', 'setup' => $setup]);

        $attributeCode = 'smartpost_courier_time_id';
        $attributeData = [
            'type' => Table::TYPE_INTEGER,
            'comment' => 'Smartpost courier time window Id',
            'nullable' => true,
        ];

        $quoteInstaller->addAttribute('quote_address', $attributeCode, $attributeData);
        $salesInstaller->addAttribute('order', $attributeCode, $attributeData);
    }

    /**
     * @param ModuleDataSetupInterface $setup
     */
    protected function createProductShippingFlagAttributes(ModuleDataSetupInterface $setup)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $eavSetup->addAttribute(Product::ENTITY, 'allow_smartpost_parcelpoint', [
            'group' => 'General',
            'type' => 'int',
            'input' => 'boolean',
            'model' => Attribute::class,
            'backend' => Boolean::class,
            'source' => BooleanSource::class,
            'label' => 'Allowed to ship with Smartpost ParcelPoint',
            'global' => Attribute::SCOPE_GLOBAL,
            'visible' => true,
            'required' => false,
            'user_defined' => true,
            'default' => true,
        ]);
    }
}

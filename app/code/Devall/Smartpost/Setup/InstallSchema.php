<?php

namespace Devall\Smartpost\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Zend_Db_Exception;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * @inheritdoc
     * @throws Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        if (\version_compare($context->getVersion(), '1.0.1') <= 0) {
            $this->createParcelPointsTable($setup);
        }
    }

    /**
     * @param SchemaSetupInterface $setup
     *
     * @throws Zend_Db_Exception
     */
    protected function createParcelPointsTable(SchemaSetupInterface $setup)
    {
        $tableName = $setup->getTable('smartpost_parcel_points');

        if (!$setup->getConnection()->isTableExists($tableName)) {
            $table = $setup->getConnection()->newTable($tableName);

            $table->addColumn('id', Table::TYPE_INTEGER, null, [
                'primary' => true,
                'unsigned' => true,
                'identity' => true,
                'nullable' => false,
            ], 'ID');

            $table->addColumn('place_id', Table::TYPE_TEXT, 255,  [
                'unsigned' => true,
                'nullable' => false,
            ], 'Smartpost Place ID');

            $table->addColumn('name', Table::TYPE_TEXT, 255, [
                'nullable' => false,
            ], 'Parcel Point Name');

            $table->addColumn('city', Table::TYPE_TEXT, 255, [
                'nullable' => false,
            ], 'Parcel Point City');

            $table->addColumn('address', Table::TYPE_TEXT, 255, [
                'nullable' => false,
            ], 'Parcel Point Address');

            $table->addColumn('country', Table::TYPE_TEXT, 2, [
                'nullable' => false,
            ], 'Parcel Point Country');

            $table->addColumn('availability', Table::TYPE_TEXT, 255, [
                'nullable' => false,
            ], 'Parcel Point Availability');

            $table->addColumn('description', Table::TYPE_TEXT, null, [
                'nullable' => false,
            ], 'Parcel Point Description');

            $setup->getConnection()->createTable($table);
        }
    }
}

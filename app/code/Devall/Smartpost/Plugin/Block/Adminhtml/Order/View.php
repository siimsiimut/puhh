<?php

namespace Devall\Smartpost\Plugin\Block\Adminhtml\Order;

use Devall\Smartpost\Model\Carrier\SmartpostCourier;
use Magento\Sales\Block\Adminhtml\Order\View as SubjectView;

class View
{
    /**
     * @param SubjectView $view
     */
    public function beforeSetLayout(SubjectView $view)
    {
        $order = $view->getOrder();
        $shippingMethod = $order->getShippingMethod();

        if (\strpos($shippingMethod, 'smartpost') === 0 || \strpos($shippingMethod, SmartpostCourier::COURIER_CODE) === 0) {
            $view->addButton('send_data_to_smartpost', [
                'label' => 'Send Order To Smartpost',
                'onclick' => 'setLocation(\'' . $view->getUrl('smartpost/order/send', [
                        'order_id' => $view->getOrder()->getId(),
                    ]) . '\')',
            ]);

            if (!empty($order->getData('barcodes'))) {
                $view->addButton('print_labels_smartpost', [
                    'label' => 'Print Labels Smartpost',
                    'onclick' => 'setLocation(\'' . $view->getUrl('smartpost/printing/labels', [
                            'order_id' => $view->getOrder()->getId(),
                        ]) . '\')',
                ]);
            }
        }
    }
}

<?php

declare(strict_types=1);

namespace Devall\Smartpost\Plugin;

use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Checkout\Model\ShippingInformationManagement;

class ShippingInformationManagementPlugin
{
    /**
     * Before save address information. Save time window id into shipping address from extension attributes
     *
     * @param ShippingInformationManagement $subject
     * @param $cartId
     * @param ShippingInformationInterface $addressInformation
     *
     * @return array
     */
    public function beforeSaveAddressInformation(
        ShippingInformationManagement $subject,
        $cartId,
        ShippingInformationInterface $addressInformation
    ) {
        $shippingAddress = $addressInformation->getShippingAddress();

        // Reset data
        $shippingAddress->setData('smartpost_courier_time_id', null);

        if ($addressInformation->getExtensionAttributes()
            && ($timeId = $addressInformation->getExtensionAttributes()->getSmartpostCourierTimeId())
        ) {
            $shippingAddress->setData('smartpost_courier_time_id', $timeId);
        }

        return [$cartId, $addressInformation];
    }
}

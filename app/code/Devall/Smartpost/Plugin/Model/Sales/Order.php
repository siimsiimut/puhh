<?php

namespace Devall\Smartpost\Plugin\Model\Sales;

use Devall\Smartpost\Helper\TimeWindowProvider;
use Devall\Smartpost\Model\Carrier\SmartpostCourier;
use Devall\Smartpost\Helper\ParcelPoints;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\Order as OrderModel;

class Order
{
    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * @var ParcelPoints
     */
    private $parcelPointsHelper;

    /**
     * @var TimeWindowProvider
     */
    private $timeWindowProvider;

    /**
     * Construct function.
     *
     * @param CartRepositoryInterface $cartRepository
     * @param ParcelPoints $parcelPointsHelper
     * @param TimeWindowProvider $timeWindowProvider
     */
    public function __construct(
        CartRepositoryInterface $cartRepository,
        ParcelPoints $parcelPointsHelper,
        TimeWindowProvider $timeWindowProvider
    ) {
        $this->cartRepository = $cartRepository;
        $this->parcelPointsHelper = $parcelPointsHelper;
        $this->timeWindowProvider = $timeWindowProvider;
    }

    /**
     * @param OrderModel $order
     * @param string $description
     *
     * @return string
     */
    public function afterGetShippingDescription(OrderModel $order, $description)
    {
        try {
            if (\strpos($order->getShippingMethod(), 'smartpost') !== false) {
                /** @var $quote Quote */
                $quote = $this->cartRepository->get($order->getQuoteId());
                $address = $quote->getShippingAddress();

                if ($address !== null && $address->hasData($key = 'smartpost_place_id')) {
                    $location = $this->parcelPointsHelper->getParcelPointByPlaceId($address->getData($key));

                    if ($location !== null && \strpos($description, $location->getData('name')) === false) {
                        return \implode(' - ', [$description, $location->getData('name')]);
                    }
                }
            }

            if (\strpos($order->getShippingMethod(), SmartpostCourier::COURIER_CODE) !== false) {
                $timeWindowLabel = $this->timeWindowProvider->getTimeWindowLabelById($order->getData('smartpost_courier_time_id'));

                return \implode(' - ', [$description, \sprintf('(%s)', $timeWindowLabel ?: __('Undefined time'))]);
            }

            return $description;
        } catch (\Throwable $exception) {
            return $description;
        }
    }
}

<?php

namespace Devall\Smartpost\Controller\Adminhtml\Order;

use Devall\Smartpost\Helper\Client;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Sales\Api\OrderRepositoryInterface;
use Throwable;

class Send extends Action
{
    /**
     * @var Client
     */
    private $clientHelper;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * Construct function.
     *
     * @param Context $context
     * @param Client $clientHelper
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(Context $context, Client $clientHelper, OrderRepositoryInterface $orderRepository)
    {
        parent::__construct($context);

        $this->clientHelper = $clientHelper;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @inheritdoc
     */
    public function execute()
    {
        try {
            $this->clientHelper->sendOrder($this->orderRepository->get($this->_request->getParam('order_id')));

            $this->messageManager->addSuccessMessage(__('Order export request performed. Check order comments for details.'));
        } catch (Throwable $exception) {
            $this->messageManager->addErrorMessage(__('Order sending failed. Error: "%1"', $exception->getMessage()));
        }

        return $this->resultRedirectFactory->create()->setUrl(
            $this->_redirect->getRefererUrl()
        );
    }
}

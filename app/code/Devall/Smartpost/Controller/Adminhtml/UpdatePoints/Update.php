<?php

namespace Devall\Smartpost\Controller\Adminhtml\UpdatePoints;

use Devall\Smartpost\Helper\ParcelPoints;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\Redirect;
use Throwable;

class Update extends Action
{
    /**
     * @var ParcelPoints
     */
    private $parcelPointsHelper;

    /**
     * Construct function.
     *
     * @param Context $context
     * @param ParcelPoints $parcelPointsHelper
     */
    public function __construct(Context $context, ParcelPoints $parcelPointsHelper)
    {
        parent::__construct($context);

        $this->parcelPointsHelper = $parcelPointsHelper;
    }

    /**
     * @return Redirect
     */
    public function execute()
    {
        try {
            $this->parcelPointsHelper->refreshParcelPointsList();
            $this->messageManager->addSuccessMessage(__('Parcel points updated'));
        } catch (Throwable $exception) {
            $this->messageManager->addErrorMessage(__('Error updating parcel points: "%1"', $exception->getMessage()));
        }

        return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
    }
}

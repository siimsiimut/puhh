<?php

declare(strict_types=1);

namespace Devall\Smartpost\Controller\Adminhtml\Printing;

use Devall\Shipping\Model\Response\Label\LabelResponseGeneratorInterface;
use Devall\Shipping\Model\Response\Label\LabelResponseGeneratorPool;
use Devall\Shipping\Model\Service\RetrieveLabelsInterface;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Shipping\Model\Shipping\LabelGenerator;

/**
 * Entrypoint for labels print action
 */
class Labels extends Action implements HttpGetActionInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var RetrieveLabelsInterface
     */
    private $retrieveLabels;

    /**
     * @var LabelResponseGeneratorPool
     */
    private $labelResponseGeneratorPool;

    /**
     * @var LabelGenerator
     */
    private $labelGenerator;

    /**
     * @param Context $context
     * @param OrderRepositoryInterface $orderRepository
     * @param RetrieveLabelsInterface $retrieveLabels
     * @param LabelResponseGeneratorPool $labelResponseGeneratorPool
     * @param LabelGenerator $labelGenerator
     */
    public function __construct(
        Context $context,
        OrderRepositoryInterface $orderRepository,
        RetrieveLabelsInterface $retrieveLabels,
        LabelResponseGeneratorPool $labelResponseGeneratorPool,
        LabelGenerator $labelGenerator
    ) {
        parent::__construct($context);

        $this->orderRepository = $orderRepository;
        $this->retrieveLabels = $retrieveLabels;
        $this->labelResponseGeneratorPool = $labelResponseGeneratorPool;
        $this->labelGenerator = $labelGenerator;
    }

    /**
     * @inheritdoc
     */
    public function execute()
    {
        try {
            $order = $this->orderRepository->get($this->_request->getParam('order_id'));
            $pdfs = $this->retrieveLabels->execute([$order]);
            $pdf = \count($pdfs) > 1 ? $this->labelGenerator->combineLabelsPdf($pdfs)->render() : \reset($pdfs);

            return $this->labelResponseGeneratorPool
                ->get(LabelResponseGeneratorInterface::GENERATOR_TYPE_PDF)
                ->generate([$order->getIncrementId() => $pdf]);
        } catch (\Throwable $exception) {
            $this->messageManager->addErrorMessage(__('Error retrieving order labels: "%1"', $exception->getMessage()));
        }

        return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
    }
}

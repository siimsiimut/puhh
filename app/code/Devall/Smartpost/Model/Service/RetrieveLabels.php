<?php

declare(strict_types=1);

namespace Devall\Smartpost\Model\Service;

use Devall\Shipping\Model\Service\RetrieveLabelsInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\Client\CurlFactory;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Framework\Xml\GeneratorFactory as XmlGeneratorFactory;
use Magento\Store\Model\ScopeInterface;

/**
 * Class responsible for retrieving labels for SmartPost order
 */
class RetrieveLabels implements RetrieveLabelsInterface
{
    private const LABEL_FORMAT = 'A6-4';
    private const LABEL_RETRIEVE_URL = 'https://gateway.posti.fi/smartpost/api/ext/v1/labels?';
    private const SUCCESS_STATUS = 200;
    private const MAX_REQUEST_ITEMS = 20;

    /**
     * @var CurlFactory
     */
    private $curlFactory;

    /**
     * @var XmlGeneratorFactory
     */
    private $xmlGeneratorFactory;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param CurlFactory $curlFactory
     * @param XmlGeneratorFactory $xmlGeneratorFactory
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        CurlFactory $curlFactory,
        XmlGeneratorFactory $xmlGeneratorFactory,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->curlFactory = $curlFactory;
        $this->xmlGeneratorFactory = $xmlGeneratorFactory;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @inheritdoc
     */
    public function execute(array $orders): array
    {
        $output = [];
        $ordersData = $this->buildOrdersInformationArray($orders);

        foreach ($ordersData as $data) {
            $barcodes = $data[0];

            foreach (\array_chunk($barcodes, self::MAX_REQUEST_ITEMS) as $chunk) {
                $output[] = $this->fetchLabels($this->buildXmlRequest($chunk));
            }
        }

        return $output;
    }

    /**
     * Fetch labels using API request.
     *
     * @param string $xmlRequest
     *
     * @return string
     */
    private function fetchLabels(string $xmlRequest): string
    {
        $curl = $this->curlFactory->create();
        $curl->setHeaders(array("Authorization: " . $this->retrieveCredentials() . "", "Content-Type: application/xml"));
        $curl->post(self::LABEL_RETRIEVE_URL, $xmlRequest);

        if (self::SUCCESS_STATUS !== $curl->getStatus()) {
            throw new \RuntimeException(
                \sprintf(
                    'SmartPost label request returned incorrect status. Status: %s. Message: %s',
                    $curl->getStatus(),
                    $curl->getBody(),
                ),
                $curl->getStatus()
            );
        }

        return $curl->getBody();
    }

    /**
     * Build orders information array.
     *
     * @param OrderInterface[] $orders
     *
     * @return array
     */
    private function buildOrdersInformationArray(array $orders): array
    {
        $output = [];

        foreach ($orders as $order) {
            $barcodes = \array_filter(\explode(',', $order->getData('barcodes')));
            $output['barcodes'][] = $barcodes;
        }

        return $output;
    }

    /**
     * Retrieve xml request data.
     *
     * @param string[] $barcodes
     *
     * @return string
     *
     * @throws \DOMException
     */
    private function buildXmlRequest(array $barcodes): string
    {
        $data = [
            'format' => self::LABEL_FORMAT,
        ];

        $xml = $this->xmlGeneratorFactory->create()->arrayToXml(['labels' => $data]);
        $dom = $xml->getDom();

        foreach ($barcodes as $barcode) {
            $element = $dom->createElement('barcode');
            $element->appendChild($dom->createTextNode($barcode));
            $dom->documentElement->appendChild($element);
        }

        return $xml->__toString();
    }

    /**
     * Retrieve API credentials
     *
     * @return string
     */
    private function retrieveCredentials(): string
    {
        return $this->scopeConfig->getValue('shipping/smartpost/api_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}

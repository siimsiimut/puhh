<?php

namespace Devall\Smartpost\Model;

use Devall\Smartpost\Helper\TimeWindowProvider;
use Devall\Smartpost\Helper\ParcelPoints;
use LogicException;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Checkout\Model\Session;

class ShippingConfigProvider implements ConfigProviderInterface
{
    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @var ParcelPoints
     */
    private $parcelPointsHelper;

    /**
     * @var TimeWindowProvider
     */
    private $timeWindowProvider;

    /**
     * Construct function.
     *
     * @param ParcelPoints $parcelPointsHelper
     * @param Session $checkoutSession
     * @param TimeWindowProvider $timeWindowProvider
     */
    public function __construct(
        ParcelPoints $parcelPointsHelper,
        Session $checkoutSession,
        TimeWindowProvider $timeWindowProvider
    ) {
        $this->parcelPointsHelper = $parcelPointsHelper;
        $this->checkoutSession = $checkoutSession;
        $this->timeWindowProvider = $timeWindowProvider;
    }

    /**
     * @inheritdoc
     * @throws LogicException
     */
    public function getConfig()
    {
        return [
            'devallSmartpost' => [
                'pickupPoints' => $this->parcelPointsHelper->getParcelPointsGroupedListByMethod(),
                'courierTimeWindows' => $this->timeWindowProvider->provideTimeWindows(),
                'savedPlaceId' => $this->checkoutSession->getQuote()->getShippingAddress()->getData('smartpost_place_id'),
                'savedTimeId' => $this->checkoutSession->getQuote()->getShippingAddress()->getData('smartpost_courier_time_id'),
            ],
        ];
    }
}

<?php

namespace Devall\Smartpost\Model;

use Devall\Smartpost\Model\ResourceModel\ParcelPoint as ParcelPointResource;
use Magento\Framework\Model\AbstractModel;

class ParcelPoint extends AbstractModel
{
    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(ParcelPointResource::class);
    }
}

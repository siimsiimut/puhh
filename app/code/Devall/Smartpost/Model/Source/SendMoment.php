<?php

namespace Devall\Smartpost\Model\Source;

use Magento\Framework\Option\ArrayInterface;

class SendMoment implements ArrayInterface
{
    const TYPE_SUCCESS_PAGE = 'checkout_success';
    const TYPE_INVOICE_CREATE = 'invoice_create';
    const TYPE_MANUAL = 'manual';
    const TYPE_PAYMENT_SUCCESS = 'payment_success';

    /**
     * @inheritdoc
     */
    public function toOptionArray()
    {
        return [
            [
                'label' => __('On Success Page'),
                'value' => static::TYPE_SUCCESS_PAGE,
            ],
            [
                'label' => __('On Invoice Create'),
                'value' => static::TYPE_INVOICE_CREATE,
            ],
            [
                'label' => __('On Payment Success'),
                'value' => static::TYPE_PAYMENT_SUCCESS,
            ],
            [
                'label' => __('Manual'),
                'value' => static::TYPE_MANUAL,
            ],
        ];
    }
}

<?php

namespace Devall\Smartpost\Model\Carrier;

class SmartpostFinland extends AbstractSmartpostCarrier
{
    /**
     * @var string
     */
    protected $_code = 'smartpostfinland';
}

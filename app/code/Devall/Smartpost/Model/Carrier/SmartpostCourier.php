<?php

declare(strict_types=1);

namespace Devall\Smartpost\Model\Carrier;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\ResultFactory;
use Psr\Log\LoggerInterface;

class SmartpostCourier extends AbstractCarrier implements CarrierInterface
{
    const COURIER_CODE = 'smartcourier';

    /**
     * @inheritDoc
     */
    protected $_code = self::COURIER_CODE;

    /**
     * @var ResultFactory
     */
    private $resultFactory;

    /**
     * @var MethodFactory
     */
    private $methodFactory;

    /**
     * SmartpostCourier constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param ErrorFactory $rateErrorFactory
     * @param LoggerInterface $logger
     * @param ResultFactory $resultFactory
     * @param MethodFactory $methodFactory
     * @param array $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $resultFactory,
        MethodFactory $methodFactory,
        array $data = []
    ) {
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);

        $this->resultFactory = $resultFactory;
        $this->methodFactory = $methodFactory;
    }

    /**
     * @inheritDoc
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        return $this->resultFactory->create()->append($this->methodFactory->create()->addData([
            'carrier' => $this->_code,
            'carrier_title' => $this->getConfigData('title'),
            'method' => $this->_code,
            'method_title' => $this->getConfigData('name'),
        ])->setPrice($this->getShippingPrice($request)));
    }

    /**
     * @inheritDoc
     */
    public function getAllowedMethods()
    {
        return [$this->_code => $this->getConfigData('name')];
    }

    /**
     * Get shipping price
     *
     * @param RateRequest $request
     *
     * @return float|int
     */
    private function getShippingPrice(RateRequest $request)
    {
        $freeShippingEnabled = $this->getConfigFlag('free_shipping_enabled');
        $freeShippingAmount = (float)$this->getConfigData('free_shipping_amount');

        if ($freeShippingEnabled && $request->getPackageValueWithDiscount() >= $freeShippingAmount) {
            return 0;
        }

        return (float)$this->getConfigData('price');
    }
}

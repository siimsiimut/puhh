<?php

namespace Devall\Smartpost\Model\Carrier;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\Result;
use Magento\Shipping\Model\Rate\ResultFactory;
use Devall\Smartpost\Helper\ParcelPoints;
use Devall\Smartpost\Model\SmartpostBox;
use Devall\Smartpost\Model\SmartpostItem;
use Psr\Log\LoggerInterface;
use Throwable;
use DVDoug\BoxPacker\Packer;

abstract class AbstractSmartpostCarrier extends AbstractCarrier implements CarrierInterface
{
    /**
     * @var MethodFactory
     */
    private $methodFactory;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var ResultFactory
     */
    private $resultFactory;

    /**
     * @var ParcelPoints
     */
    private $parcelPointsHelper;

    /**
     * @var Packer
     */
    private $packer;

    
    /**
     * Construct function.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param ErrorFactory $rateErrorFactory
     * @param LoggerInterface $logger
     * @param ResultFactory $resultFactory
     * @param MethodFactory $methodFactory
     * @param ProductRepositoryInterface $productRepository
     * @param ParcelPoints $parcelPointsHelper
     * @param array $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $resultFactory,
        MethodFactory $methodFactory,
        ProductRepositoryInterface $productRepository,
        ParcelPoints $parcelPointsHelper,
        Packer $packer,
        array $data = []
    ) {
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);

        $this->resultFactory = $resultFactory;
        $this->methodFactory = $methodFactory;
        $this->productRepository = $productRepository;
        $this->parcelPointsHelper = $parcelPointsHelper;
        $this->packer = $packer;
    }

    /**
     * @inheritdoc
     */
    public function checkAvailableShipCountries(DataObject $request)
    {
        return $this->getConfigData('country') === $request->getData('dest_country_id') || $this->getConfigData('country') === null;
    }

    /**
     * Collect and get rates
     *
     * @param RateRequest $request
     *
     * @return bool|DataObject|Result
     * @api
     */
    public function collectRates(RateRequest $request)
    {
        $result = $this->resultFactory->create();

        if (!$this->getConfigFlag('active') || !$this->checkAllProductsAreApplicable($request)) {
            return false;
        }

        
        if ($this->_code === 'smartpostfinland') {
            $packageCount = $this->calculatePackageCount($request);
            if (!$packageCount || $packageCount > 1) {
                return false;
            }     
        }
           

        $pickupPointMethodCode = $this->getPPMethod($request);
        
        if (!$pickupPointMethodCode) {

            $result->append($this->methodFactory->create()->addData([
                'carrier' => $this->_code,
                'carrier_title' => $this->getConfigData('title'),
                'method' => $this->_code,
                'method_title' => $this->getConfigData('name'),
            ])->setPrice($this->getShippingPrice($request)));
            return $result;

        } else {

            $methodTitle = $this->parcelPointsHelper->getParcelPointByPlaceId($pickupPointMethodCode)->getName();
            $result->append($this->methodFactory->create()->addData([
                'carrier' => $this->_code,
                'carrier_title' => $this->getConfigData('title'),
                'method' => $pickupPointMethodCode,
                'method_title' => $methodTitle,
            ])->setPrice($this->getShippingPrice($request)));
    
            return $result;
        }
    }

    /**
     * Gets pickuppoint code from quote item via request
     * Necessary because otherwise shipping method on quote wont match 
     * and will be reverted to '' (specific to Acty_Checkout logic) 
     * @return bool|string
     */
    protected function getPPMethod($request) 
    {
        $fullShippingMethod = $request->getAllItems()[0]->getQuote()->getShippingAddress()->getShippingMethod(); 
        if (!$fullShippingMethod || \strpos($fullShippingMethod, 'smartpost') !== 0) {
            return false;
        }
        return explode('_', $fullShippingMethod)[1];
    }
    /**
     * Calculates necessary smartpost packages
     * in magento dimensions are in cm, need mm
     * in magento weight is in kg, need g
     */
    protected function calculatePackageCount($request)
    {
        $items = [];
        $smartPostLBox = new SmartpostBox('Smartpost L locker', 380, 360, 600, 500, 370, 350,590, 35000);
        $smartPostXLBox = new SmartPostBox('Smartpost XL Locker', 360, 600, 600, 500, 350, 590, 590, 35000);
        
        $this->packer->addBox($smartPostLBox);
        $this->packer->addBox($smartPostXLBox);

        foreach ($request->getAllItems() as $item) {
            /** @var \Magento\Quote\Model\Quote\Item $item */
            try {
                /** @var Product $product */
                $product = $this->productRepository->get($item->getSku());
                $fmt = numfmt_create( 'et_EE', \NumberFormatter::DECIMAL );
                $d = numfmt_parse($fmt, $product->getData('depth')) * 10;
                $w = numfmt_parse($fmt, $product->getData('width')) * 10;
                $h = numfmt_parse($fmt, $product->getData('height')) * 10;
                $m = floatval($product->getData('weight')) * 1000;
                $boxItem = new SmartpostItem($item->getSku(), $w, $d, $h, $m, false);
                array_push($items, ['item' => $boxItem, 'qty' => $item->getQty()]);
            } catch (Throwable $exception) {
                $this->_logger->error(
                    'error creating smartpostitem', [
                        'sku' => $item->getSku(),
                        'error' => $exception
                    ]
                );
            }
        }
        foreach ($items as $smartpostItem) {
            $this->packer->addItem($smartpostItem['item'], $smartpostItem['qty']);
        }
        try {
            $packedBoxes = $this->packer->pack();
            return count($packedBoxes);
        } catch (\Throwable $th) {
            //throw $th;
            $this->_logger->error('Some item does not fit into Smartpost', [
                'msg' => $th
            ]);
            return false;
        }
    }

    /**
     * Get allowed shipping methods
     *
     * @return array
     * @api
     */
    public function getAllowedMethods()
    {
        return [$this->_code => __($this->getConfigData('name'))];
    }

    /**
     * @param RateRequest $request
     *
     * @return bool
     */
    private function checkAllProductsAreApplicable(RateRequest $request)
    {
        $areApplicable = true;

        if ($this->getConfigFlag('check_enabled') === false) {
            return $areApplicable;
        }

        foreach ($request->getAllItems() as $item) {
            /** @var \Magento\Quote\Model\Quote\Item $item */
            try {
                /** @var Product $product */
                $product = $this->productRepository->get($item->getSku());
                $areApplicable &= !(bool)$product->getData('noestoniansmartpost');
            } catch (Throwable $exception) {
                return false;
            }
        }

        return $areApplicable;
    }

    /**
     * @param RateRequest $request
     *
     * @return mixed
     */
    private function getShippingPrice(RateRequest $request)
    {
        $freeShippingEnabled = $this->getConfigFlag('free_shipping_enabled');
        $freeShippingAmount = (float)$this->getConfigData('free_shipping_amount');

        if ($freeShippingEnabled && $request->getPackageValueWithDiscount() >= $freeShippingAmount) {
            return 0;
        }

        return $this->getConfigData('handling_fee');
    }
}

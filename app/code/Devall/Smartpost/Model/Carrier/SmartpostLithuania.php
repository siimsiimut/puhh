<?php

namespace Devall\Smartpost\Model\Carrier;

class SmartpostLithuania extends AbstractSmartpostCarrier
{
    /**
     * @var string
     */
    protected $_code = 'smartpostlithuania';
}

<?php

namespace Devall\Smartpost\Model\Carrier;

class SmartpostLatvia extends AbstractSmartpostCarrier
{
    /**
     * @var string
     */
    protected $_code = 'smartpostlatvia';
}

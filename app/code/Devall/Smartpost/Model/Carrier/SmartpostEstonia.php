<?php

namespace Devall\Smartpost\Model\Carrier;

class SmartpostEstonia extends AbstractSmartpostCarrier
{
    /**
     * @var string
     */
    protected $_code = 'smartpostestonia';
}

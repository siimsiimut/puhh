<?php

namespace Devall\Smartpost\Model;

use Devall\Smartpost\Api\ParcelPointsRetrieverInterface;
use Magento\Framework\App\Helper\AbstractHelper;

class ParcelPointsRetriever extends AbstractHelper implements ParcelPointsRetrieverInterface
{
    const EE_LIST_URL = 'https://gateway.posti.fi/smartpost/api/ext/v1/places?country=EE';
    const FI_LIST_URL = 'https://gateway.posti.fi/smartpost/api/ext/v1/places?country=FI';
    const LV_LIST_URL = 'https://gateway.posti.fi/smartpost/api/ext/v1/places?country=LV';
    const LT_LIST_URL = 'https://gateway.posti.fi/smartpost/api/ext/v1/places?country=LT';

    /**
     * @inheritdoc
     */
    public function getParcelPointsList()
    {       
        return \array_merge(...\array_map(function ($list) {
            $opts = [
                "http" => [
                    "method" => "GET",
                    "header" => 
                    "Authorization: " . $this->retrieveCredentials() . "\r\n" .
                    "Content-Type: application/xml\r\n"
                ]
            ];
            $context = stream_context_create($opts);
            $content = \Zend_Json::decode(\Zend_Json::fromXml(\file_get_contents($list, false, $context)));

            return $content['places']['item'] ?? [];
        }, [static::EE_LIST_URL, static::FI_LIST_URL]));
    }

    /**
     * Retrieve API credentials
     *
     * @return string
     */
    private function retrieveCredentials(): string
    {
        return $this->scopeConfig->getValue('shipping/smartpost/api_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}

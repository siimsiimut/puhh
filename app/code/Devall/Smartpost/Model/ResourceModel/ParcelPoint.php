<?php

namespace Devall\Smartpost\Model\ResourceModel;

class ParcelPoint extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init('smartpost_parcel_points', 'id');
    }
}

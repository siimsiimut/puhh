<?php

namespace Devall\Smartpost\Model\ResourceModel\ParcelPoint;

use Devall\Smartpost\Model\ParcelPoint;
use Devall\Smartpost\Model\ResourceModel\ParcelPoint as ParcelPointResource;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(ParcelPoint::class, ParcelPointResource::class);
    }
}

<?php

namespace Devall\Smartpost\Block\System\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Button as ButtonWidget;
use Magento\Backend\Model\UrlInterface;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Exception\LocalizedException;

class UpdateParcelPointsButton extends Field
{
    /**
     * @var UrlInterface
     */
    private $backendUrl;

    /**
     * Construct function.
     *
     * @param Context $context
     * @param UrlInterface $backendUrl
     * @param array $data
     */
    public function __construct(Context $context, UrlInterface $backendUrl, array $data = [])
    {
        parent::__construct($context, $data);

        $this->backendUrl = $backendUrl;
    }

    /**
     * @inheritdoc
     */
    public function _getElementHtml(AbstractElement $element)
    {
        try {
            /** @var Button $button */
            $button = $this->getLayout()->createBlock(ButtonWidget::class);

            return $button->setData([
                'label' => $element->getData('label'),
                'onclick' => sprintf(
                    'setLocation("%s");', $this->backendUrl->getUrl('smartpost/updatepoints/update')
                ),
            ])->toHtml();
        } catch (LocalizedException $exception) {
            return '';
        }
    }
}

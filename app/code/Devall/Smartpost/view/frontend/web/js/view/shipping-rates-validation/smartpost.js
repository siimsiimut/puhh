define([
    'uiComponent',
    'Magento_Checkout/js/model/shipping-rates-validation-rules',
    'Magento_Checkout/js/model/default-validation-rules'
], function (Component, defaultShippingRatesValidationRules, validationRules) {
    'use strict';

    return Component.extend({
        /**
         * @inheritdoc
         */
        initialize: function () {
            this._super();

            defaultShippingRatesValidationRules.registerRules(this.carrier_code, validationRules);

            return this;
        }
    });
});

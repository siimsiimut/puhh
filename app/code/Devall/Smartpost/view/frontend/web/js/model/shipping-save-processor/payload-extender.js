/* global define */
define(['Magento_Checkout/js/model/quote'], function (quote) {
    'use strict';

    return function (originalFunction) {
        return function (payload) {
            var result = originalFunction(payload);

            if (result.addressInformation.shipping_carrier_code.indexOf('smartpost') === 0) {
                result.addressInformation['extension_attributes']['smartpost_place_id'] = quote.shippingMethod()['smartpost_place_id'];
            }

            if (result.addressInformation.shipping_carrier_code.indexOf('smartcourier') === 0) {
                result.addressInformation['extension_attributes']['smartpost_courier_time_id'] = quote.shippingMethod()['smartpost_courier_time_id'];
            }

            return result;
        }
    }
});

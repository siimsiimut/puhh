/* global define */
define([
    'ko',
    'underscore',
    'jquery',
    'Magento_Checkout/js/model/quote',
    'mage/translate',
    'mage/validation'
], function (ko, _, $, quote, $t) {
    'use strict';

    return function (Component) {
        return Component.extend({
            defaults: {
                smartpostDropdownTemplate: 'Devall_Smartpost/shipping-address/smartpost-dropdown',
                smartpostCourierDropdownTemplate: 'Devall_Smartpost/shipping-address/smartcourier-dropdown',
            },

            selectedSmartpostPickupPoint: ko.observable(window.checkoutConfig.devallSmartpost.savedPlaceId),
            selectedTimeWindowId: ko.observable(window.checkoutConfig.devallSmartpost.savedTimeId),
            smartpostFormSelector: '#smartpost_parcel_point_form',
            smartpostCourierTimeForm: '#smartpost_courier_time_form',

            initialize: function () {
                this._super();

                var originalMethodTemplate = this.shippingMethodItemTemplate;

                this.shippingMethodItemTemplate = ko.computed(function () {
                    var method = quote.shippingMethod();

                    if (method && method.carrier_code.indexOf('smartpost') === 0) {
                        return this.smartpostDropdownTemplate;
                    }

                    if (method && method.carrier_code.indexOf('smartcourier') === 0) {
                        return this.smartpostCourierDropdownTemplate;
                    }

                    return ko.isComputed(originalMethodTemplate) ? originalMethodTemplate() : originalMethodTemplate;
                }, this);

                return this;
            },

            validateShippingInformation: function () {
                if (quote.shippingMethod() && quote.shippingMethod()['carrier_code'].indexOf('smartpost') === 0) {
                    var smartpostForm = $(this.smartpostFormSelector).validation();
                    var isDataValid = smartpostForm.validation('isValid') && this._super();

                    if (isDataValid && quote.shippingMethod().smartpost_place_id !== this.selectedSmartpostPickupPoint()) {
                        quote.shippingMethod($.extend({}, quote.shippingMethod(), {
                            smartpost_place_id: this.selectedSmartpostPickupPoint()
                        }));
                    }

                    return isDataValid;
                }

                if (quote.shippingMethod() && quote.shippingMethod()['carrier_code'].indexOf('smartcourier') === 0) {
                    const isDataValid = $(this.smartpostCourierTimeForm).validation().validation('isValid') && this._super();

                    if (isDataValid && quote.shippingMethod().smartpost_courier_time_id !== this.selectedTimeWindowId()) {
                        quote.shippingMethod($.extend({}, quote.shippingMethod(), {
                            smartpost_courier_time_id: this.selectedTimeWindowId()
                        }));
                    }

                    return isDataValid;
                }

                return this._super();
            },

            getSmartpostParcelPointsOptionsArray: function (method) {
                if (_.has(window.checkoutConfig.devallSmartpost.pickupPoints, method.method_code)) {
                    return _.toArray(window.checkoutConfig.devallSmartpost.pickupPoints[method.method_code]);
                }

                return [];
            },

            /**
             * Provide smartpost courier time windows array
             *
             * @returns {*}
             */
            getSmartpostCourierTimeWindowsArray: function () {
                return _.map(window.checkoutConfig.devallSmartpost.courierTimeWindows, function (value, key) {
                    return {
                        'value': key,
                        'label': $t(value),
                    };
                });
            }
        });
    };
});

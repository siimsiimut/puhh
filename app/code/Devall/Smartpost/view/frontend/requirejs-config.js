var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/view/shipping': {
                'Devall_Smartpost/js/view/shipping': true
            },

            'Magento_Checkout/js/model/shipping-save-processor/payload-extender': {
                'Devall_Smartpost/js/model/shipping-save-processor/payload-extender': true
            }
        }
    }
};

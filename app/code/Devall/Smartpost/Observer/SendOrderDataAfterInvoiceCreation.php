<?php

namespace Devall\Smartpost\Observer;

use Devall\Smartpost\Helper\Client;
use Devall\Smartpost\Model\Source\SendMoment;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Psr\Log\LoggerInterface;

class SendOrderDataAfterInvoiceCreation implements ObserverInterface
{
    /**
     * @var Client
     */
    private $clientHelper;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Construct function.
     *
     * @param Client $clientHelper
     * @param OrderRepositoryInterface $orderRepository
     * @param LoggerInterface $logger
     */
    public function __construct(Client $clientHelper, LoggerInterface $logger)
    {
        $this->clientHelper = $clientHelper;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    public function execute(Observer $observer)
    {
        /** @var Order\Invoice $invoice */
        $invoice = $observer->getData('invoice');

        if ($this->clientHelper->getOrderSendMoment($invoice->getOrder()) === SendMoment::TYPE_INVOICE_CREATE) {
            $this->clientHelper->sendOrder($invoice->getOrder());

            $this->logger->info('SMARTPOST: Order data was sent on invoice creation', [
                'order_increment_id' => $invoice->getOrder()->getIncrementId(),
                'order_id' => $invoice->getOrder()->getId(),
            ]);
        }
    }
}

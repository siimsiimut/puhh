<?php

namespace Devall\Smartpost\Observer;

use Devall\Smartpost\Helper\ParcelPoints;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Quote\Model\Quote\Address;

class SalesQuoteAddressSaveBefore implements ObserverInterface
{
    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * @var ParcelPoints
     */
    private $parcelPointsHelper;

    /**
     * @var RequestInterface|Http
     */
    private $request;

    /**
     * Construct function.
     *
     * @param RequestInterface $request
     * @param ArrayManager $arrayManager
     * @param ParcelPoints $parcelPointsHelper
     */
    public function __construct(RequestInterface $request, ArrayManager $arrayManager, ParcelPoints $parcelPointsHelper)
    {
        $this->request = $request;
        $this->arrayManager = $arrayManager;
        $this->parcelPointsHelper = $parcelPointsHelper;
    }

    /**
     * @inheritdoc
     */
    public function execute(Observer $observer)
    {
        /** @var Address $address */
        $address = $observer->getData('quote_address');
            $isSmartpostShipping = \strpos($address->getShippingMethod() ?? '', 'smartpost') === 0;

        if ($isSmartpostShipping && $address->getAddressType() === Address::ADDRESS_TYPE_SHIPPING) {
            if (($placeId = $this->getSmartpostPlaceId($address)) !== null) {
                $address->setData('smartpost_place_id', $placeId);
            }
        }
    }

    /**
     * @param Address $address
     *
     * @return mixed|null
     */
    private function getSmartpostPlaceId(Address $address)
    {
        if (\json_last_error() === JSON_ERROR_NONE) {
            $countryId = $address->getCountryModel()->getCountryId();
            $placeId = explode('_', $address->getShippingMethod())[1];
            $places = $this->parcelPointsHelper->getParcelPointsListByCountry();
            if (\array_key_exists($countryId, $places) && \array_key_exists($placeId, $places[$countryId])) {
                return $placeId;
            }

            return null;
        }

        return null;
    }
}

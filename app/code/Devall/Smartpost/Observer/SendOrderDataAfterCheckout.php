<?php

namespace Devall\Smartpost\Observer;

use Devall\Smartpost\Helper\Client;
use Devall\Smartpost\Model\Source\SendMoment;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Psr\Log\LoggerInterface;
use Throwable;

class SendOrderDataAfterCheckout implements ObserverInterface
{
    /**
     * @var Client
     */
    private $clientHelper;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * Construct function.
     *
     * @param Client $clientHelper
     * @param OrderRepositoryInterface $orderRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        Client $clientHelper,
        OrderRepositoryInterface $orderRepository,
        LoggerInterface $logger
    ) {
        $this->clientHelper = $clientHelper;
        $this->orderRepository = $orderRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    public function execute(Observer $observer)
    {
        foreach ($observer->getData('order_ids') as $orderId) {
            try {
                /** @var Order $order */
                $order = $this->orderRepository->get($orderId);
            } catch (Throwable $exception) {
                $this->logger->error('SMARTPOST: Failed to export order on checkout success', [
                    'order_id' => $orderId,
                    'error_message' => $exception->getMessage(),
                ]);

                continue;
            }

            if ($this->clientHelper->getOrderSendMoment($order) === SendMoment::TYPE_SUCCESS_PAGE) {
                $this->clientHelper->sendOrder($order);

                $this->logger->info('SMARTPOST: Order data was sent from checkout success page', [
                    'order_increment_id' => $order->getIncrementId(),
                    'order_id' => $order->getId(),
                ]);
            }
        }
    }
}

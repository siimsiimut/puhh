<?php

declare(strict_types=1);

namespace Devall\Smartpost\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\Order;

class SalesEventQuoteSubmitBefore implements ObserverInterface
{
    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getData('order');
        /** @var Quote $quote */
        $quote = $observer->getData('quote');

        $order->setData('smartpost_courier_time_id', $quote->getShippingAddress()->getData('smartpost_courier_time_id'));
    }
}

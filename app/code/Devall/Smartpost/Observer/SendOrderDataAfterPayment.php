<?php

namespace Devall\Smartpost\Observer;

use Devall\Smartpost\Helper\Client;
use Devall\Smartpost\Model\Source\SendMoment;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Psr\Log\LoggerInterface;
use Throwable;

class SendOrderDataAfterPayment implements ObserverInterface
{
    /**
     * @var Client
     */
    private $clientHelper;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * Construct function.
     *
     * @param Client $clientHelper
     * @param OrderRepositoryInterface $orderRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        Client $clientHelper,
        OrderRepositoryInterface $orderRepository,
        LoggerInterface $logger
    ) {
        $this->clientHelper = $clientHelper;
        $this->orderRepository = $orderRepository;
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    public function execute(Observer $observer)
    {
        $order = $observer->getPayment()->getOrder();
        if ($this->clientHelper->getOrderSendMoment($order) === SendMoment::TYPE_PAYMENT_SUCCESS) {
            $this->clientHelper->sendOrder($order);
            $this->logger->info('SMARTPOST: Order data was sent after payment success', [
                'order_increment_id' => $order->getIncrementId(),
                'order_id' => $order->getId(),
            ]);
        }
    }
}

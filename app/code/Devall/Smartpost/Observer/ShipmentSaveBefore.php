<?php

namespace Devall\Smartpost\Observer;

use Devall\Smartpost\Model\Carrier\SmartpostCourier;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order\Shipment;
use Magento\Sales\Model\Order\Shipment\Track;
use Magento\Sales\Model\Order\Shipment\TrackFactory;
use Magento\Sales\Api\ShipmentRepositoryInterface;

/**
 * Class responsible for saving tracking numbers into shipment entity.
 */
class ShipmentSaveBefore implements ObserverInterface
{
    /**
     * @var TrackFactory
     */
    private $trackFactory;

    /**
     * @var ShipmentRepositoryInterface
     */
    private $shipmentRepository;

    /**
     * @param TrackFactory $trackFactory
     * @param ShipmentRepositoryInterface $shipmentRepository
     */
    public function __construct(TrackFactory $trackFactory, ShipmentRepositoryInterface $shipmentRepository)
    {
        $this->trackFactory = $trackFactory;
        $this->shipmentRepository = $shipmentRepository;
    }

    /**
     * @inheritdoc
     */
    public function execute(Observer $observer): void
    {
        /** @var Shipment $shipment */
        $shipment = $observer->getEvent()->getShipment();
        $order = $shipment->getOrder();

        if (
            \strpos($order->getShippingMethod(), 'smartpost') === 0 ||
            \strpos($order->getShippingMethod(), SmartpostCourier::COURIER_CODE) === 0
        ) {
            foreach ($this->getTrackingNumbersFromOrder($order) as $trackingNumber) {
                /** @var Track $track */
                $track = $this->trackFactory->create();
                $track->setCarrierCode(
                    \substr($order->getShippingMethod(), 0, \strrpos($order->getShippingMethod(), '_'))
                );
                $track->setTitle('Smartpost shipping');
                $track->setNumber($trackingNumber);

                $shipment->addTrack($track);
            }
        }
    }

    /**
     * Get tracking numbers from order
     *
     * @param OrderInterface $order
     *
     * @return array
     */
    private function getTrackingNumbersFromOrder(OrderInterface $order): array
    {
        return \array_filter(\explode(',', $order->getData('barcodes')));
    }
}

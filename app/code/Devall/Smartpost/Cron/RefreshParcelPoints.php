<?php

namespace Devall\Smartpost\Cron;

use Devall\Smartpost\Helper\ParcelPoints;
use Magento\Framework\Exception\LocalizedException;

class RefreshParcelPoints
{
    /**
     * @var ParcelPoints
     */
    private $parcelPointsHelper;

    /**
     * Construct function.
     *
     * @param ParcelPoints $parcelPointsHelper
     */
    public function __construct(ParcelPoints $parcelPointsHelper)
    {
        $this->parcelPointsHelper = $parcelPointsHelper;
    }

    /**
     * @inheritdoc
     * @throws LocalizedException
     */
    public function execute()
    {
        $this->parcelPointsHelper->refreshParcelPointsList();
    }
}

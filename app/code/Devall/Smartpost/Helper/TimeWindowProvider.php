<?php

declare(strict_types=1);

namespace Devall\Smartpost\Helper;

class TimeWindowProvider
{
    public static $timeWindows = [
        1 => 'Any time',
        2 => '09:00 – 17:00',
        3 => '17:00 – 21:00',
    ];

    /**
     * Provide smartpost courier possible time windows
     *
     * @return array
     */
    public function provideTimeWindows(): array
    {
        return self::$timeWindows;
    }

    /**
     * Get time window label by id
     *
     * @param int $id
     *
     * @return string
     */
    public function getTimeWindowLabelById(int $id): string
    {
        if (isset(self::$timeWindows[$id])) {
            return self::$timeWindows[$id];
        }

        return '';
    }
}

<?php

namespace Devall\Smartpost\Helper;

use Devall\Smartpost\Model\Carrier\SmartpostCourier;
use Devall\Smartpost\Model\Source\SendMoment;
use LogicException;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Convert\Xml as XmlConverter;
use Magento\Framework\DataObject;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Store\Model\ScopeInterface;
use RuntimeException;
use SimpleXMLElement;
use Throwable;
use Zend_Http_Client_Exception;

class Client extends AbstractHelper
{
    const SMARTPOST_ORDER_SEND_URL = 'https://gateway.posti.fi/smartpost/api/ext/v1/orders';

    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * @var ZendClientFactory
     */
    private $clientFactory;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var ParcelPoints
     */
    private $parcelPointsHelper;

    /**
     * @var XmlConverter
     */
    private $xmlConverter;

    /**
     * @param Context $context
     * @param CartRepositoryInterface $cartRepository
     * @param ZendClientFactory $clientFactory
     * @param OrderRepositoryInterface $orderRepository
     * @param ParcelPoints $parcelPointsHelper
     * @param XmlConverter $xmlConverter
     */
    public function __construct(
        Context $context,
        CartRepositoryInterface $cartRepository,
        ZendClientFactory $clientFactory,
        OrderRepositoryInterface $orderRepository,
        ParcelPoints $parcelPointsHelper,
        XmlConverter $xmlConverter
    ) {
        parent::__construct($context);

        $this->cartRepository = $cartRepository;
        $this->clientFactory = $clientFactory;
        $this->orderRepository = $orderRepository;
        $this->parcelPointsHelper = $parcelPointsHelper;
        $this->xmlConverter = $xmlConverter;
    }

    /**
     * @param Order|OrderInterface $order
     *
     * @return bool
     */
    public function getOrderSendMoment(Order $order)
    {
        if ($order->getIsVirtual()) {
            return SendMoment::TYPE_MANUAL;
        }

        if (
            \strpos($order->getShippingMethod(true)->getData('carrier_code'), 'smartpost') === false &&
            !$this->isCourierShipping($order)
        ) {
            return SendMoment::TYPE_MANUAL;
        }

        return $this->getConfiguration(
            \sprintf('carriers/%s/send_moment', $order->getShippingMethod(true)->getData('carrier_code')),
            $order->getStoreId()
        ) ?? SendMoment::TYPE_MANUAL;
    }

    /**
     * @param Order|OrderInterface $order
     *
     * @return bool
     * @throws RuntimeException
     * @throws Zend_Http_Client_Exception
     * @throws LogicException
     */
    public function sendOrder(Order $order)
    {
        if (
            \strpos($order->getShippingMethod(true)->getData('carrier_code'), 'smartpost') === false &&
            !$this->isCourierShipping($order)
        ) {
            $this->_logger->alert('SMARTPOST: Order export failed, shipping method is not Smartpost', [
                'order_increment_id' => $order->getIncrementId(),
                'order_id' => $order->getId(),
            ]);

            throw new RuntimeException('Shipping method is not Smartpost');
        }

        if (!$this->getConfiguration('shipping/smartpost/enabled', $order->getStoreId(), true)) {
            $this->_logger->alert('SMARTPOST: Order export failed, data exchange disabled', [
                'order_increment_id' => $order->getIncrementId(),
                'order_id' => $order->getId(),
            ]);

            throw new RuntimeException('Data exchange is disabled');
        }

        if (($address = $this->getShippingAddress($order)) === null) {
            $this->_logger->alert('SMARTPOST: Order export failed, missing shipping information', [
                'order_increment_id' => $order->getIncrementId(),
                'order_id' => $order->getId(),
            ]);

            throw new RuntimeException('Missing quote shipping address');
        }

        if ($this->getConnectionInfo($order) === null) {
            $this->_logger->alert('SMARTPOST: Order export failed, missing connection info', [
                'order_increment_id' => $order->getIncrementId(),
                'order_id' => $order->getId(),
            ]);

            throw new RuntimeException('Connection info is missing');
        }

        $client = $this->clientFactory->create(['uri' => static::SMARTPOST_ORDER_SEND_URL]);
        $client->setRawData($this->getOrderData($order, $address), 'text/xml');
        $client->setHeaders(array("Authorization: " . $this->retrieveCredentials() . "", "Content-Type: application/xml"));
        $client->setMethod('POST');

        $response = $client->request();

        $order->addCommentToStatusHistory(\implode(\str_repeat(PHP_EOL, 2), [
            __('SMARTPOST: Order export performed'),
            \substr(\sprintf('STATUS: %d', $response->getStatus()), 0, 1000),
        ])); 

        $parsedResponse = $this->parseResponse($response->getBody());

        if (
            true === $parsedResponse['success'] &&
            !empty($parsedResponse['data']['item']['barcode'])
        ) {
            $order->setData('barcodes', \implode(',', \array_filter(\array_merge([
                $parsedResponse['data']['item']['barcode']
            ], \explode(',', $order->getData('barcodes') ?? '')))));
        }

        $this->orderRepository->save($order);

        $this->_logger->info('SMARTPOST: Order export performed', [
            'order_increment_id' => $order->getIncrementId(),
            'order_id' => $order->getId(),
            'result' => $response->isSuccessful(),
            'status' => $response->getStatus(),
        //    'body' => $response->getBody(),
        ]);

        if (!$response->isSuccessful()) {
            throw new RuntimeException(
                \sprintf('Order export returned error response: "%s"', $response->getBody())
            );
        }

        return true;
    }

    /**
     * Parse response from Smartpost.
     *
     * @param string $response
     *
     * @return array
     */
    private function parseResponse(string $response): array
    {
        try {
            $xml = \simplexml_load_string($response);

            return [
                'success' => $xml->getName() === 'orders',
                'data' => $this->xmlConverter->xmlToAssoc($xml),
            ];
        } catch (\Throwable $e) {
            $this->_logger->error('Error parsing smartpost response.', [
                'message' => $e->getMessage(),
                'response' => $response,
            ]);

            return [
                'success' => false,
                'data' => $response,
            ];
        }
    }

    /**
     * @param array $data
     * @param SimpleXMLElement $node
     *
     * @return mixed
     */
    private function arrayToXml(array $data, SimpleXMLElement $node)
    {
        foreach ($data as $key => $datum) {
            if (\is_array($datum)) {
                $child = $node->addChild($key);
                $this->arrayToXml($datum, $child);
            } else {
                $node->addChild($key, $datum);
            }
        }

        return $node->asXML();
    }

    /**
     * @param string $path
     * @param string $storeId
     * @param bool $isFlag
     *
     * @return bool|string
     */
    private function getConfiguration($path, $storeId, $isFlag = false)
    {
        if ($isFlag) {
            return $this->scopeConfig->isSetFlag($path, ScopeInterface::SCOPE_STORES, $storeId);
        }

        return $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORES, $storeId);
    }

    /**
     * @param Order $order
     *
     * @return null|array
     */
    private function getConnectionInfo(Order $order)
    {
        return $this->getConfiguration('shipping/smartpost/api_key', $order->getStoreId());

    }

    /**
     * @param Address $address
     *
     * @return DataObject|null
     * @throws LogicException
     */
    private function getDestinationLocation(Address $address)
    {
        return $this->parcelPointsHelper->getParcelPointByPlaceId($address->getData('smartpost_place_id'));
    }

    /**
     * @param Order $order
     *
     * @param Address $address
     *
     * @return string
     * @throws LogicException
     */
    private function getOrderData(Order $order, Address $address)
    {
        $carrier = $order->getShippingMethod(true)['carrier_code'];
        $storeId = $order->getStoreId();

        $data = [
            'item' => [
                'reference' => $order->getIncrementId(),
                'weight' => $order->getWeight(),
                'recipient' => [
                    'name' => \implode(' ', [$address->getFirstname(), $address->getLastname()]),
                    'phone' => $address->getTelephone(),
                    'email' => $order->getCustomerEmail(),
                    'cash' => $this->getConfiguration(
                        \sprintf('carriers/%s/enabled_cache_on_delivery', $carrier),
                        $storeId,
                        true
                    ) ? $order->getTotalDue() : 0,
                ],
                'source' => [
                    'country' => 'EE',
                ],
                'destination' => $this->getDestinationData($order, $address),
                'additionalservices' => [
                    'agecheck' => 'false',
                ],
            ],
        ];
        return $this->arrayToXml($data, (new \SimpleXMLElement('<root/>'))->addChild('orders'));
    }

    /**
     * Get destination data
     *
     * @param Order $order
     * @param Address $address
     *
     * @return array
     */
    private function getDestinationData(Order $order, Address $address): array
    {
        if ($this->isCourierShipping($order)) { 
            return [    
                'postalcode' => $address->getPostcode(),    
                'street' => \implode(',', $address->getStreet()),   
                'city' => $address->getCity(),  
                'country' => $address->getCountryId(),  
                'timewindow' => (int)$address->getData('smartpost_courier_time_id'),    
            ];  
        }   


        if (!$location = $this->getDestinationLocation($address)) {
            return [];
        }

        if ($location->getData('country') === 'EE') {
            return [
                'place_id' => $location->getData('place_id'),
                'country' => $location->getData('country'),
            ];
        }

        if ($this->isCourierShipping($order)) {
            return [
                'postalcode' => $address->getPostcode(),
                'street' => \implode(',', $address->getStreet()),
                'city' => $address->getCity(),
                'country' => $address->getCountryId(),
                'timewindow' => (int)$address->getData('smartpost_courier_time_id'),
                'postalcode' => $location->getData('postalcode'),
                'routingcode' => $location->getData('routingcode'),
            ];
        }

        if ($location->getData('country') === 'FI') {
            return [
                'postalcode' => $location->getData('postalcode'),
                'routingcode' => $location->getData('routingcode'),
                'country' => $address->getCountryId()
            ];
        }
    }

    /**
     * Check if is courier shipping
     *
     * @param OrderInterface $order
     *
     * @return bool
     */
    private function isCourierShipping(OrderInterface $order): bool
    {
        return \strpos($order->getShippingMethod(), SmartpostCourier::COURIER_CODE) === 0;
    }

    /**
     * @param Order $order
     *
     * @return Quote|CartInterface
     */
    private function getQuote(Order $order)
    {
        try {
            return $this->cartRepository->get($order->getQuoteId());
        } catch (Throwable $exception) {
            return null;
        }
    }

    /**
     * @param Order $order
     *
     * @return Address
     */
    private function getShippingAddress(Order $order)
    {
        if ($quote = $this->getQuote($order)) {
            return $quote->getShippingAddress();
        }

        return null;
    }

    /**
     * Retrieve API credentials
     *
     * @return string
     */
    private function retrieveCredentials(): string
    {
        return $this->scopeConfig->getValue('shipping/smartpost/api_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}

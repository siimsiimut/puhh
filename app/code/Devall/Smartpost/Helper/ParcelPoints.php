<?php

namespace Devall\Smartpost\Helper;

use Devall\Smartpost\Api\ParcelPointsRetrieverInterface;
use Devall\Smartpost\Model\ParcelPoint;
use Devall\Smartpost\Model\ResourceModel\ParcelPoint\CollectionFactory;
use Devall\Smartpost\Model\ResourceModel\ParcelPointFactory as ParcelPointResourceFactory;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;

class ParcelPoints extends DataObject
{
    /**
     * @var ParcelPointResourceFactory
     */
    private $parcelPointResourceFactory;

    /**
     * @var CollectionFactory
     */
    private $parcelPointsCollectionFactory;

    /**
     * @var ParcelPointsRetrieverInterface
     */
    private $parcelPointsRetriever;

    /**
     * Construct function.
     *
     * @param ParcelPointsRetrieverInterface $parcelPointsRetriever
     * @param ParcelPointResourceFactory $parcelPointResourceFactory
     * @param CollectionFactory $parcelPointsCollectionFactory
     * @param array $data
     */
    public function __construct(
        ParcelPointsRetrieverInterface $parcelPointsRetriever,
        ParcelPointResourceFactory $parcelPointResourceFactory,
        CollectionFactory $parcelPointsCollectionFactory,
        array $data = []
    ) {
        parent::__construct($data);

        $this->parcelPointsRetriever = $parcelPointsRetriever;
        $this->parcelPointResourceFactory = $parcelPointResourceFactory;
        $this->parcelPointsCollectionFactory = $parcelPointsCollectionFactory;
    }

    /**
     * @param mixed $id
     *
     * @return ParcelPoint|null|DataObject
     */
    public function getParcelPointByPlaceId($id)
    {
        $locations = $this->parcelPointsCollectionFactory->create();
        $locations->addFieldToFilter('place_id', $id);
        $locations->setPageSize(1)->setCurPage(0);

        if ($locations->getSize() > 0) {
            return $locations->getFirstItem();
        }

        return null;
    }

    /**
     * @return array
     */
    public function getParcelPointsGroupedListByMethod()
    {
        $data = $this->getParcelPointsListByMethod();

        \array_walk($data, function (&$parcelPoints) {
            $grouped = [];

            foreach ($parcelPoints as $parcelPoint) {
                if (!isset($grouped[$parcelPoint['city']])) {
                    $grouped[$parcelPoint['city']] = [
                        'label' => $parcelPoint['city'],
                        'locations' => [],
                    ];
                }

                $grouped[$parcelPoint['city']]['locations'][] = $parcelPoint;
            }

            \ksort($grouped);

            $parcelPoints = $grouped;
        });

        return $data;
    }

    /**
     * @return array
     */
    public function getParcelPointsListByCountry()
    {
        $countryBasedList = [];
        $locations = $this->parcelPointsCollectionFactory->create();
        $locations->addFieldToSelect(['name', 'country', 'address', 'place_id', 'city'])->load();

        foreach ($locations as $location) {
            /** @var $location ParcelPoint */
            if (!\array_key_exists($country = $location->getData('country'), $countryBasedList)) {
                $countryBasedList[$country] = [];
            }

            $countryBasedList[$country][$location->getData('place_id')] = $location;
        }

        return $countryBasedList;
    }

    /**
     * @return array
     */
    public function getParcelPointsListByMethod()
    {
        $locations = $this->parcelPointsCollectionFactory->create();
        $locations->addFieldToSelect(['name', 'country', 'place_id', 'city', 'address'])->load();
        $relations = $this->getParcelPointsPerMethodConfiguration();

        return \array_combine(
            \array_map(function ($data) {
                return $data['code'];
            }, $relations),
            \array_map(function ($data) use ($locations) {
                return array_map(function ($location) {
                    return \array_merge($location, [
                        'fullName' => \sprintf('%s (%s)', $location['name'], $location['address']),
                    ]);
                }, \array_filter($locations->toArray()['items'], function ($location) use ($data) {
                    return $location['country'] === $data['country'];
                }));
            }, $relations)
        );
    }

    /**
     * @return array
     */
    public function getParcelPointsPerMethodConfiguration()
    {
        return $this->getData('method_relation_data');
    }

    /**
     * @throws LocalizedException
     */
    public function refreshParcelPointsList()
    {
        $resource = $this->parcelPointResourceFactory->create();
        $list = $this->parcelPointsRetriever->getParcelPointsList();

        if (\count($list) > 0) {
            $resource->getConnection()->truncateTable($resource->getMainTable());
            $resource->getConnection()->insertMultiple($resource->getMainTable(), \array_map(function ($item) {
                return [
                    'place_id' => $item['place_id'],
                    'name' => $item['name'],
                    'city' => $item['city'],
                    'address' => $item['address'],
                    'country' => $item['country'],
                    'availability' => $item['availability'],
                    'description' => $item['description'],
                    'postalcode' => $item['postalcode'],
                    'routingcode' => $item['routingcode'],
                ];
            }, $list));
        }
    }
}

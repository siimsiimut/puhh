<?php

namespace Devall\Smartpost\Api;

interface ParcelPointsRetrieverInterface
{
    /**
     * @return array
     */
    public function getParcelPointsList();
}

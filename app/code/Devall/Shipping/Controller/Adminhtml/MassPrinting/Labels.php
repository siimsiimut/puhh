<?php

declare(strict_types=1);

namespace Devall\Shipping\Controller\Adminhtml\MassPrinting;

use Devall\Shipping\Model\Resolver\CarrierCodeResolver;
use Devall\Shipping\Model\Response\Label\LabelResponseGeneratorInterface;
use Devall\Shipping\Model\Response\Label\LabelResponseGeneratorPool;
use Devall\Shipping\Model\Service\LabelRetrieverPool;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Shipping\Model\Shipping\LabelGenerator;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;

/**
 * Mass label printing action
 */
class Labels extends Action implements HttpPostActionInterface
{
    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var OrderCollectionFactory
     */
    private $orderCollectionFactory;

    /**
     * @var LabelRetrieverPool
     */
    private $labelRetrieverPool;

    /**
     * @var LabelResponseGeneratorPool
     */
    private $labelResponseGeneratorPool;

    /**
     * @var CarrierCodeResolver
     */
    private $carrierCodeResolver;

    /**
     * @var LabelGenerator
     */
    private $labelGenerator;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param OrderCollectionFactory $orderCollectionFactory
     * @param LabelRetrieverPool $labelRetrieverPool
     * @param LabelResponseGeneratorPool $labelResponseGeneratorPool
     * @param CarrierCodeResolver $carrierCodeResolver
     * @param LabelGenerator $labelGenerator
     */
    public function __construct(
        Context $context,
        Filter $filter,
        OrderCollectionFactory $orderCollectionFactory,
        LabelRetrieverPool $labelRetrieverPool,
        LabelResponseGeneratorPool $labelResponseGeneratorPool,
        CarrierCodeResolver $carrierCodeResolver,
        LabelGenerator $labelGenerator
    ) {
        parent::__construct($context);

        $this->filter = $filter;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->labelRetrieverPool = $labelRetrieverPool;
        $this->labelResponseGeneratorPool = $labelResponseGeneratorPool;
        $this->carrierCodeResolver = $carrierCodeResolver;
        $this->labelGenerator = $labelGenerator;
    }

    /**
     * @inheritdoc
     */
    public function execute()
    {
        $labels = [];

        try {
            $ordersArray = $this->retrieveCollection();

            foreach ($ordersArray as $carrier => $orders) {
                $labels[] = $this->labelRetrieverPool->get($carrier)->execute($orders);
            }

            $pdfs = \array_merge([], ...$labels);
            $pdf = \count($pdfs) > 1 ? $this->labelGenerator->combineLabelsPdf($pdfs)->render() : \reset($pdfs);

            return $this->generateResponse(['mass_printing' => $pdf]);
        } catch (\Throwable $exception) {
            $this->messageManager->addErrorMessage(__('Error retrieving order labels: "%1"', $exception->getMessage()));
        }

        return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
    }

    /**
     * Retrieve collection sorted by shipping method
     *
     * @return array
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function retrieveCollection(): array
    {
        $collection = $this->filter->getCollection($this->orderCollectionFactory->create());
        $output = [];

        foreach ($collection as $order) {
            if (empty($order->getData('barcodes'))) {
                continue;
            }

            $output[$this->carrierCodeResolver->resolve($order)][] = $order;
        }

        return $output;
    }

    /**
     * Generate response.
     *
     * @param array $labels
     *
     * @return ResponseInterface
     */
    private function generateResponse(array $labels): ResponseInterface
    {
        $type = \count($labels) === 1
            ? LabelResponseGeneratorInterface::GENERATOR_TYPE_PDF
            : LabelResponseGeneratorInterface::GENERATOR_TYPE_ZIP;

        return $this->labelResponseGeneratorPool->get($type)->generate($labels);
    }
}

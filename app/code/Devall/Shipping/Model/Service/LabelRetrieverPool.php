<?php

declare(strict_types=1);

namespace Devall\Shipping\Model\Service;

/**
 * Pool of label retrievers.
 */
class LabelRetrieverPool
{
    /**
     * @var array
     */
    private $objects;

    /**
     * @param array $objects
     */
    public function __construct(array $objects = [])
    {
        $this->objects = $objects;
    }

    /**
     * Get label retriever by carrier.
     *
     * @param string $carrier
     *
     * @return RetrieveLabelsInterface
     */
    public function get(string $carrier): RetrieveLabelsInterface
    {
        if (!isset($this->objects[$carrier])) {
            throw new \RuntimeException("Label retriever for $carrier carrier does not exist in the pool");
        }

        return $this->objects[$carrier];
    }
}
<?php

declare(strict_types=1);

namespace Devall\Shipping\Model\Service;

/**
 * Class responsible for retrieving order labels
 */
class RetrieveLabels implements RetrieveLabelsInterface
{
    /**
     * @inheritdoc
     */
    public function execute(array $orders): array
    {
        return [];
    }
}

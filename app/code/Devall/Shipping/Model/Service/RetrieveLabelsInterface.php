<?php

declare(strict_types=1);

namespace Devall\Shipping\Model\Service;

use Magento\Sales\Api\Data\OrderInterface;

/**
 * Abstraction layer for labels retrieve service.
 */
interface RetrieveLabelsInterface
{
    /**
     * Retrieve label PDF.
     *
     * @param OrderInterface[] $orders
     *
     * @return string[]
     */
    public function execute(array $orders): array;
}

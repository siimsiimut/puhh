<?php

declare(strict_types=1);

namespace Devall\Shipping\Model\Resolver;

use Magento\Sales\Api\Data\OrderInterface;

/**
 * Class responsible for resolving API carrier code by using order shipping method.
 */
class CarrierCodeResolver
{
    private const DEFAULT_CODE = 'default';

    /**
     * @var array
     */
    private $carriers;

    /**
     * @param array $carriers
     */
    public function __construct(array $carriers = [])
    {
        $this->carriers = $carriers;
    }

    /**
     * Resolve API carrier code by order shipping method.
     *
     * @param OrderInterface $order
     *
     * @return string
     */
    public function resolve(OrderInterface $order): string
    {
        $shippingMethod = $order->getShippingMethod();

        foreach ($this->carriers as $carrier => $methods) {
            foreach ($methods as $method) {
                if (\strpos($shippingMethod, $method) === 0) {
                    return $carrier;
                }
            }
        }

        return self::DEFAULT_CODE;
    }
}

<?php

declare(strict_types=1);

namespace Devall\Shipping\Model\Response\Label;

/**
 * Pool of label file response generators.
 */
class LabelResponseGeneratorPool
{
    /**
     * @var array
     */
    private $responseGenerators;

    /**
     * @param array $responseGenerators
     */
    public function __construct(array $responseGenerators = [])
    {
        $this->responseGenerators = $responseGenerators;
    }

    /**
     * Retrieve label response generator by type.
     *
     * @param string $type
     *
     * @return LabelResponseGeneratorInterface
     */
    public function get(string $type): LabelResponseGeneratorInterface
    {
        if (!isset($this->responseGenerators[$type])) {
            throw new \RuntimeException("Label response generator type $type does not exist in the pool");
        }

        return $this->responseGenerators[$type];
    }
}

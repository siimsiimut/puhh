<?php

declare(strict_types=1);

namespace Devall\Shipping\Model\Response\Label;

use Magento\Framework\App\ResponseInterface;
use Magento\Sales\Api\Data\OrderInterface;

/**
 * Abstraction for label response generators.
 */
interface LabelResponseGeneratorInterface
{
    public const GENERATOR_TYPE_PDF = 'pdf';
    public const GENERATOR_TYPE_ZIP = 'zip';

    /**
     * Generate response to download PDF.
     *
     * @param array $labels
     *
     * @return ResponseInterface
     */
    public function generate(array $labels): ResponseInterface;
}

<?php

declare(strict_types=1);

namespace Devall\Shipping\Model\Response\Label;

use Magento\Backend\App\Response\Http\FileFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResponseInterface;
use Magento\Sales\Api\Data\OrderInterface;

/**
 * Generate response in PDF format.
 */
class PdfLabelResponseGenerator implements LabelResponseGeneratorInterface
{
    /**
     * @var FileFactory
     */
    private $fileFactory;

    /**
     * @param FileFactory $fileFactory
     */
    public function __construct(FileFactory $fileFactory)
    {
        $this->fileFactory = $fileFactory;
    }

    /**
     * @inheritdoc
     */
    public function generate(array $labels): ResponseInterface
    {
        $labels = \array_filter($labels);

        if (\count($labels) === 0) {
            throw new \RuntimeException('No labels to generate.');
        }

        $key = \array_key_first($labels);
        $label = $labels[$key];
        $filename = \sprintf('%s-labels_%s.pdf', $key, \time());

        return $this->fileFactory->create($filename, $label, DirectoryList::TMP);
    }
}

<?php

declare(strict_types=1);

namespace Devall\Shipping\Model\Response\Label;

use Magento\Backend\App\Response\Http\FileFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Filesystem;
use Magento\Sales\Api\Data\OrderInterface;

/**
 * Generate response in zip format.
 */
class ZipLabelResponseGenerator implements LabelResponseGeneratorInterface
{
    /**
     * @var FileFactory
     */
    private $fileFactory;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @param FileFactory $fileFactory
     * @param Filesystem $filesystem
     */
    public function __construct(FileFactory $fileFactory, Filesystem $filesystem)
    {
        $this->fileFactory = $fileFactory;
        $this->filesystem = $filesystem;
    }

    /**
     * @inheritdoc
     */
    public function generate(array $labels): ResponseInterface
    {
        $labels = \array_filter($labels);

        if (\count($labels) === 0) {
            throw new \RuntimeException('No labels to generate.');
        }

        $filename = \sprintf('orders-labels_%s.zip', \time());
        $tmpArchivePath = $this->createArchive($labels);
        $content = \file_get_contents($tmpArchivePath);
        $this->filesystem->getDirectoryWrite(DirectoryList::TMP)->delete($tmpArchivePath);

        return $this->fileFactory->create($filename, $content, DirectoryList::TMP);
    }

    /**
     * Create archive
     *
     * @param array $files
     *
     * @return string
     */
    private function createArchive(array $files): string
    {
        $archive = new \ZipArchive;
        $archiveName = $this->filesystem->getDirectoryRead(DirectoryList::TMP)->getAbsolutePath(\uniqid() . '.zip');

        $archive->open($archiveName, \ZipArchive::CREATE);

        foreach ($files as $key => $content) {
            $filename = \sprintf('%s-labels_%s.pdf', $key, \time());
            $archive->addFromString($filename, $content);
        }

        $archive->close();

        return $archiveName;
    }
}

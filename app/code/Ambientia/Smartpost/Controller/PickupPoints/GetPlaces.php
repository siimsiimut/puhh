<?php

namespace Ambientia\Smartpost\Controller\PickupPoints;

class GetPlaces extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $helper;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Ambientia\Smartpost\Helper\Data $helper
    )
    {
        parent::__construct($context);

        $this->resultJsonFactory = $resultJsonFactory;
        $this->helper = $helper;

    }

    /**
     * return all pickup places
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        try {
            $returnData = $this->helper->getPlaces();

            $result = $this->resultJsonFactory->create();
            $result->setData($returnData, false, JSON_UNESCAPED_UNICODE);
            return $result;

        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }
}
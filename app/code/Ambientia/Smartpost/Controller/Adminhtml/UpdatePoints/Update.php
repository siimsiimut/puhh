<?php
namespace Ambientia\Smartpost\Controller\Adminhtml\UpdatePoints;
use Magento\Framework\Controller\ResultFactory;

class Update extends \Magento\Backend\App\Action
{
    protected $_helper;

    protected $_itellaPlaces;

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magento_AdminNotification::show_list');
    }

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Ambientia\Smartpost\Helper\Data $helper,
        \Ambientia\Smartpost\Model\ItellaEstoniaFactory $itellaEstoniaPlaces
    ){
        parent::__construct($context);

        $this->_helper = $helper;
        $this->_itellaPlaces = $itellaEstoniaPlaces;
    }


    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $places = $this->_helper->getPlacesFromSmartpost();

        foreach ($places as $place) {
            $parcelPoint = $this->_itellaPlaces->create();
            $oldEntry = $parcelPoint->getCollection()->addFieldToFilter('place_id', $place['place_id'])->addFieldToSelect('id')->getFirstItem();
            if ($oldEntry->getId()) {
                $parcelPoint->setData('id', $oldEntry->getId());
            }
            $parcelPoint->setData('place_id', $place['place_id']);
            $parcelPoint->setData('name', $place['name']);
            $parcelPoint->setData('city', $place['city']);
            $parcelPoint->setData('group_name', $place['city']);
            $parcelPoint->setData('address', $place['address']);
            $parcelPoint->setData('availability', $place['availability']);
            $parcelPoint->setData('description', is_array($place['description']) ? '' : $place['description']);
            try {
                $parcelPoint->save();
            } catch (\Throwable $th) {
                throw $th;
            }
        }
        $this->messageManager->addSuccessMessage('Pickup points updated');
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}
<?php
namespace Ambientia\Smartpost\Setup;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Framework\Setup\SchemaSetupInterface;


class UpgradeData implements UpgradeDataInterface
{

    /**
     * @var SchemaSetupInterface
     */
    private $schemaSetup;
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavAttributeSetupInterface;

    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        SchemaSetupInterface $schemaSetup, 
        EavSetupFactory $eavSetupFactory, 
        AttributeRepositoryInterface $eavAttributeSetupInterface)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavAttributeSetupInterface = $eavAttributeSetupInterface;
        $this->schemaSetup = $schemaSetup;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            /** @var EavSetup $eavSetup */
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            $attributesExist = true;

            try {
                $this->eavAttributeSetupInterface->get(\Magento\Catalog\Model\Product::ENTITY, 'noestoniansmartpost');
                $attributesExist = true;
            }
            catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                $attributesExist = false;
            }

            if (!$attributesExist) {
                $eavSetup->addAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    'noestoniansmartpost',
                    [
                        'group' => 'General',
                        'type' => 'int',
                        'input' => 'boolean',
                        'model' => 'Magento\Catalog\Model\ResourceModel\Eav\Attribute',
                        'backend' => 'Magento\Catalog\Model\Product\Attribute\Backend\Boolean',
                        'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                        'label' => 'Smartpost keelatud',
                        'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => true,
                        'used_in_product_listing' => true,
                        'default' => null
                    ]
                );
            }
        }

        if (version_compare($context->getVersion(), '1.1.0') < 0) {
            $tableName = 'itella_smartpost_estonia_parcels';
            $setup->getConnection()
                ->changeColumn($setup->getTable($tableName),
                'opened',
                'availability',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT
                ]
            );
            $setup->getConnection()
                ->dropColumn($setup->getTable($tableName),
                'group_id'
            );
            $setup->getConnection()
                ->dropColumn($setup->getTable($tableName),
                'group_sort'
            );
        }


        $setup->endSetup();
    }
}
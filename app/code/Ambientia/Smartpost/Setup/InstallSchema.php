<?php
/**
 * Copyright © 2015 Ambientia. All rights reserved.
 */

namespace Ambientia\Smartpost\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('itella_smartpost_estonia_parcels');
        // Check if the table already exists
        if ($installer->getConnection()->isTableExists($tableName) != true) {
            $table = $installer->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'place_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Place ID'
                )
                ->addColumn(
                    'name',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'Parcel name'
                )
                ->addColumn(
                    'city',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'City'
                )
                ->addColumn(
                    'address',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'Address'
                )
                ->addColumn(
                    'opened',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'Open hours'
                )
                ->addColumn(
                    'group_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => true],
                    'Group id'
                )
                ->addColumn(
                    'group_name',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'Group name'
                )
                ->addColumn(
                    'group_sort',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => true],
                    'Group sort'
                )
                ->addColumn(
                    'description',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'Description'
                )
                ->setComment('Itella Estonia parcel points')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}

<?php

namespace Ambientia\Smartpost\Block\System\Config;


use Magento\Backend\Block\Template\Context;
use Magento\Customer\Model\Session;
use Magento\Framework\ObjectManagerInterface;

class Button extends \Magento\Config\Block\System\Config\Form\Field {

    /**
     * Path to block template
     */
    const CHECK_TEMPLATE = 'system/config/button.phtml';
    protected $adminUrl;

    public function __construct(
        Context $context,
        \Magento\Backend\Model\UrlInterface $adminUrl,
                                $data = array())
    {
        parent::__construct($context, $data);
        $this->adminUrl = $adminUrl;
    }

    /**
     * Set template to itself
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->getTemplate()) {
            $this->setTemplate(static::CHECK_TEMPLATE);
        }
        return $this;
    }

    /**
     * Render button
     *
     * @param  \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        // Remove scope label
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $this->addData(
            [
                'location' => $this->adminUrl->getUrl('smartpost/UpdatePoints/Update'),
                'title' => 'Update parcel points',
                'html_id' => $element->getHtmlId(),
            ]
        );

        return $this->_toHtml();
    }
}
require(['jquery', 'mage/url', 'Magento_Checkout/js/model/quote', 'Magento_Checkout/js/model/shipping-rate-processor/new-address','Magento_Checkout/js/model/shipping-service', 'mage/translate', 'domReady'],
    function($, urlBuilder, quote, newAddress, shippingService){


    $(document).on('click', '#co-shipping-method-form tr', function () {
        var selectedMethod = $('#co-shipping-method-form input[type=radio]:checked').val();
        if (selectedMethod) {
            if (selectedMethod == 'estoniansmartpost_null') {
                if (this.id == 'smartpost-added-tr') {
                    return false;
                }
                if ($('#smartpost-added-tr').length > 0) {
                    $('#smartpost-added-tr').html('').fadeOut().remove();
                }
                $(this).after('<tr id="smartpost-added-tr">' +
                    '<td colspan="5">Vali sobiv pakiautomaat: <select name="smartpost_places"></select></td>' +
                    '</tr>');
                populatePlaces();
            }
            else {
                //hide dropdown
                if ($('#smartpost-added-tr').length > 0) {
                    $('#smartpost-added-tr').html('').fadeOut().remove();
                }
            }
        }
    });
    $(document).on('change', 'select[name="smartpost_places"]', function () {
        addPickupPointToQuote(this.value);
    });

    function populatePlaces() {
        shippingService.isLoading(true);
        var urlbase = urlBuilder.build('/smartpost/PickupPoints/');
        var lastOptgroup = '';
        $.ajax({
            url: urlbase + 'GetPlaces',
            dataType: 'json',
            success: function (data) {
                $('select[name="smartpost_places"]').append($('<option>').text('-- Vali pakiautomaat -- ').attr('value', ''));
                $.each(data, function(i, value) {
                    if (lastOptgroup != value.group_name) {
                        lastOptgroup = value.group_name;
                        $('select[name="smartpost_places"]').append($('<optgroup>').attr('label', value.group_name));
                    }
                    $('select[name="smartpost_places"]').append($('<option>').text(value.name).attr('value', value.place_id));
                });
                shippingService.isLoading(false);
            },
            error: function () {
                shippingService.isLoading(false);
            }
        });
    }

    function addPickupPointToQuote(placeId) {
        var urlbase = urlBuilder.build('/smartpost/PickupPoints/');
        $.ajax({
            url: urlbase + 'AddPickupToQuote/place/' + placeId,
            dataType: 'json',
            success: function (data) {
                quote.shippingMethod().method_code = data;
                var address = quote.shippingAddress();
                address.cache_disabled = true;
                newAddress.getRates(address);
            },
            error: function () {
            }
        });
    }
});
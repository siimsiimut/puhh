<?php

namespace Ambientia\Smartpost\Helper;

use Magento\Checkout\Model\Session;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Catalog\Model\ProductRepository;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const SMARTPOST_TIME_CACHE_ID = 'estoniansmartpost_time';
    const SMARTPOST_TIME_CACHE_LIFETIME = 43200;

    const SMARTPOST_PLACES_CACHE_ID = 'estoniansmartpost_places';
    const SMARTPOST_PLACES_CACHE_LIFETIME = 43200;

    const SMARTPOST_PLACES_URL = 'https://iseteenindus.smartpost.ee/api/?request=destinations&country=EE';


    private $_places = null;
    private $_placesMap = null;
    private $_placesGrouped = null;

    protected $scopeConfig;
    protected $cache;
    protected $checkoutSession;
    protected $taxHelper;
    protected $currency;
    protected $itellaEstoniaPlaces;
    protected $isAllowed = true;
    protected $productRepository;
    protected $attributeRepository;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\CacheInterface $cache,
        Session $session,
        PriceCurrencyInterface $currency,
        \Ambientia\Smartpost\Model\ItellaEstoniaFactory $itellaEstoniaPlaces,
        \Magento\Tax\Helper\Data $taxHelper,
        ProductRepository $productRepository,
        \Magento\Eav\Api\AttributeRepositoryInterface $attributeRepository,
        \Magento\Framework\Serialize\Serializer\Json $serializer
    ) {
        $this->scopeConfig = $context->getScopeConfig();
        $this->cache = $cache;
        $this->checkoutSession = $session;
        $this->taxHelper = $taxHelper;
        $this->currency = $currency;
        $this->itellaEstoniaPlaces = $itellaEstoniaPlaces;
        $this->productRepository = $productRepository;
        $this->attributeRepository = $attributeRepository;
        $this->serializer = $serializer;

        return parent::__construct($context);
    }

    public function checkCart()
    {
        $parcelConfig = $this->scopeConfig->getValue('carriers/estoniansmartpost/parcelsize');
        if ($parcelConfig) {
            $parcelConfigResults = $this->serializer->unserialize($parcelConfig);
            if (is_array($parcelConfigResults)) {
                $attributesValid = $this->checkIfAttributesExists();
                if (!$attributesValid) {
                    return false;
                }

                $calculationType = $this->scopeConfig->getValue('carriers/estoniansmartpost/calculation_type');
                if ($calculationType == 'order_total') {
                    return $this->checkOrderTotalSize($parcelConfigResults);
                } else if ($calculationType == 'largest_item') {
                    return $this->checkOrderLargestItem($parcelConfigResults);
                } else {
                    return false;
                }
            }
        }
    }

    private function checkOrderTotalSize($parcelSizeConfig) {
        $orderDimensions = Array(
            'width' => 0,
            'depth' => 0,
            'height' => 0,
            'weight' => 0,
        );
        foreach ($this->checkoutSession->getQuote()->getAllItems() as $item) {
            $product = $this->productRepository->getById($item->getProduct()->getId());
            if ($product->getData('width')) {
                $orderDimensions['width'] += $product->getData('width');
            }
            if ($product->getData('depth')) {
                $orderDimensions['depth'] += $product->getData('depth');
            }
            if ($product->getData('height')) {
                $orderDimensions['height'] += $product->getData('height');
            }
            if ($product->getData('weight')) {
                $orderDimensions['weight'] += $product->getData('weight');
            }
        }
        foreach ($parcelSizeConfig as $parcelSize) {
            $parcelDimensions = explode('x', $parcelSize['dimensions']);
            if ($orderDimensions['height'] <= $parcelDimensions[0] && $orderDimensions['width'] <= $parcelDimensions[1] && $orderDimensions['depth'] <= $parcelDimensions[2]
                && $orderDimensions['weight'] <= $parcelSize['weight']) {
                return $parcelSize['price'];
            }
        }
        return false;
    }

    private function checkOrderLargestItem($parcelSizeConfig) {
        $orderDimensions = Array(
            'width' => 0,
            'depth' => 0,
            'height' => 0,
            'weight' => 0,
        );
        foreach ($this->checkoutSession->getQuote()->getAllItems() as $item) {
            $product = $this->productRepository->getById($item->getProduct()->getId());
            if ($product->getData('width') && $product->getData('width') > $orderDimensions['width']) {
                $orderDimensions['width'] = $product->getData('width');
            }
            if ($product->getData('depth') && $product->getData('depth') > $orderDimensions['depth']) {
                $orderDimensions['depth'] = $product->getData('depth');
            }
            if ($product->getData('height') && $product->getData('height') > $orderDimensions['height']) {
                $orderDimensions['height'] = $product->getData('height');
            }
            if ($product->getData('weight') && $product->getData('weight') > $orderDimensions['weight']) {
                $orderDimensions['weight'] = $product->getData('weight');
            }
        }
        foreach ($parcelSizeConfig as $parcelSize) {
            $parcelDimensions = explode('x', $parcelSize['dimensions']);
            if ($orderDimensions['height'] <= $parcelDimensions[0] && $orderDimensions['width'] <= $parcelDimensions[1] && $orderDimensions['depth'] <= $parcelDimensions[2]
                && $orderDimensions['weight'] <= $parcelSize['weight']) {
                return $parcelSize['price'];
            }
        }
    }


    private function checkIfAttributesExists() {
        $attributesExist = false;
        try {
            $this->attributeRepository->get(\Magento\Catalog\Model\Product::ENTITY, 'height');
            $attributesExist = true;
        }
        catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return false;
        }
        try {
            $this->attributeRepository->get(\Magento\Catalog\Model\Product::ENTITY, 'depth');
            $attributesExist = true;
        }
        catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return false;
        }
        try {
            $this->attributeRepository->get(\Magento\Catalog\Model\Product::ENTITY, 'width');
            $attributesExist = true;
        }
        catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return false;
        }

        return $attributesExist;
    }

    public function getWarningMessage() {
        return $this->scopeConfig->getValue('carriers/estoniansmartpost/specificerrmsg');
    }

    public function getPlaces() {
        //Mage::log('>> getPlaces() ');

        if (is_null($this->_places)) {
            $this->_places = $this->_getPlaces();
        }
        return $this->_places;
    }

    public function getPlacesJson() {
        return json_encode($this->getPlaces());
    }

    public function getPlacesMap() {
        if (is_null($this->_placesMap)) {
            $result = array();
            foreach ($this->getPlaces() as $place) {
                $result[$place['place_id']] = $place;
            }
            $this->_placesMap = $result;
        }
        return $this->_placesMap;
    }

    public function getPlacesMapJson() {
        return json_encode($this->getPlacesMap());
    }

    public function getPlacesGrouped() {
        if (is_null($this->_placesGrouped)) {
            $result = array();
            foreach ($this->getPlaces() as $place) {
                $groupId = $place['group_id'];
                if (!isset($result[$groupId])) {
                    $result[$groupId] = array(
                        'group_name' => $place['group_name'],
                        'places'     => array()
                    );
                }

                $result[$groupId]['places'][] = $place;
            }
            $this->_placesGrouped = $result;
        }
        return $this->_placesGrouped;
    }

    public function getPlacesFromSmartpost() {
        $serializedPlaces = file_get_contents(self::SMARTPOST_PLACES_URL);
        if (!$serializedPlaces) {
            return false;
        }
        $unserializedPlaces = json_decode(json_encode((array)simplexml_load_string($serializedPlaces)),true);
        if (!$unserializedPlaces) {
            return false;
        }

        $result = array();
        foreach ($unserializedPlaces['item'] as $place) {
            
            $result[] = array(
                'place_id'      => $place['place_id'],
                'name'          => $place['name'],
                'city'          => $place['city'],
                'group_name'    => $place['city'],
                'address'       => $place['address'],
                'availability'  => $place['availability'],
                'description'   => $place['description']
            );
        }

        return $result;

    }

    // data exchange documentation http://www.estoniansmartpost.ee/andmevahetus.html
    private function _getPlaces() {
        //Mage::log('>> _getPlaces()');
        $chachedPlaces = $this->_getPlacesCache();
        if ($chachedPlaces !== false) {
            return $chachedPlaces;
        }
        $parcels = $this->itellaEstoniaPlaces->create();
        $parcelPoints = $parcels->getCollection()->setOrder('group_name', 'ASC')->setOrder(new \Zend_Db_Expr("`group_name`='Tallinn'"))->getData();

        if ($this->_setPlacesCache($parcelPoints) != true) {
            $this->_logger->debug('Unable to use cache');
        }

        return $parcelPoints;
    }

    private function _getPlacesCache() {
        //Mage::log('>> _getPlacesCache()');
        $cachedTime = $this->cache->load(self::SMARTPOST_TIME_CACHE_ID);
        if ($cachedTime !== false) {
            $cacheAge = time() - $cachedTime;
            if ($cacheAge > self::SMARTPOST_TIME_CACHE_LIFETIME) {
                return false;
            }
        } else {
            return false;
        }

        $chachedPlaces = $this->cache->load(self::SMARTPOST_PLACES_CACHE_ID);
        if ($chachedPlaces !== false) {
            $unserializedPlaces = unserialize($chachedPlaces);
            if (!$unserializedPlaces) {
                return false;
            }
            return $unserializedPlaces;
        }
        return false;
    }

    private function _setPlacesCache($places) {
        //Mage::log('>> _setPlacesCache($places)');
        $placesCacheId = self::SMARTPOST_PLACES_CACHE_ID;
        $placesCacheTags = array($placesCacheId);
        $placesCacheLifeTime = self::SMARTPOST_PLACES_CACHE_LIFETIME;

        $timeCacheId = self::SMARTPOST_TIME_CACHE_ID;
        $timeCacheTags = array($timeCacheId);
        $timeCacheLifeTime = self::SMARTPOST_TIME_CACHE_LIFETIME;

        $placesSave = $this->cache->save(serialize($places), $placesCacheId, $placesCacheTags, $placesCacheLifeTime);
        $timeSave = $this->cache->save(time(), $timeCacheId, $timeCacheTags, $timeCacheLifeTime);

        return $placesSave && $timeSave;
    }

    public static function sort($a, $b) {
        $a = str_pad(100 - $a['group_sort'], 3, "0", STR_PAD_LEFT).$a['name'];
        $b = str_pad(100 - $b['group_sort'], 3, "0", STR_PAD_LEFT).$b['name'];
        return strcmp($a, $b);
    }

    /**
     * @param $code
     * @param $_rates
     * @return string
     *
     * @TODO Where this function is called?
     */
    public function getParcelsHtmlOutput($code, $_rates){

        $quote = $this->checkoutSession->getQuote();

        $selectedShippingMethod = $quote->getShippingAddress()->getShippingMethod();
        $html = '';
        $html .= '<dd> <ul> <li>';
        $html .= '<input name="shipping_method" type="radio" value="' . $selectedShippingMethod .'" id="'. $code .'_value"';
        if(strpos($selectedShippingMethod, $code . '_') !== false){
            $html .= ' checked="checked" ';
        }
        $html .= 'class="radio"/>';
        $html .= '<input id="location_name" type="hidden" name="location_name" value="-" />';
        $html .='<select name="' . $code . '_select" id="' . $code . '_select_id" style="width: 280px;">';

        $html .= '<option value=""> ' . __("-- Vali pakiautomaat --") . '</option>';
        foreach ($_rates as $_rate){
            if($_rate->getCode() === $selectedShippingMethod){
                $html .= '<option value=" ' .  $_rate->getCode() . '" selected="selected">' . $_rate->getMethodTitle() . '</option>';
            }else{
                $html .= ' <option value="' . $_rate->getCode() .'">' . $_rate->getMethodTitle() . '</option>';
            }
        }
        $html .= '</select>';
        $html .= '</li></ul></dd>';
        $html .='<script type="text/javascript">
                    // <![CDATA[
                    if ($("' . $code . '_value") != undefined) {
                            Event.observe("' . $code . '_value", "change", function(event) {
                                var selectValue = $(" ' . $code . '_select_id").getValue();
                            });
                            Event.observe("'. $code .'_select_id", "change", function(event) {
                                var element = Event.element(event);
                                    var selected_index = element.selectedIndex;
                                    $("location_name").value = element.options[element.selectedIndex].text;
                                $$("input[type=' . "'radio'" . '][name='."'shipping_method'".']").each(function(el) {
                                    el.checked = false;
                                });
                                $("' . $code . '_value").value = element.getValue();
                                $("' . $code . '_value").checked = true;
                            });
                        }
                    // ]]>
                    var terminals = $("estoniansmartpost_select_id");
                    $("location_name").value = terminals.options[terminals.selectedIndex].text;
                </script>';
        $html .= '<label for="s_method_' . '123' . '"> ' ;

        $_incl = $this->taxHelper->getShippingPrice(
                    $_rate->getPrice(),
                    true,
                    $this->checkoutSession->getQuote()->getShippingAddress()
                );

        $_incl = $this->currency->convertAndFormat(
            $_incl,
            true,
            PriceCurrencyInterface::DEFAULT_PRECISION,
            $quote->getStore()
        );

        $_excl = $this->taxHelper->getShippingPrice(
                    $_rate->getPrice(),
                    $this->taxHelper->displayShippingPriceIncludingTax(),
                    $quote->getShippingAddress()
                );

        $_excl = $this->currency->convertAndFormat(
            $_excl,
            true,
            PriceCurrencyInterface::DEFAULT_PRECISION,
            $quote->getStore()
        );

        $html .= $_excl;
        if ($this->taxHelper->displayShippingBothPrices() && $_incl != $_excl){
            $html .= ' (' . __('Incl. Tax') . $_incl . ')';
        }
        $html .= '</label>';
        return $html;
    }

    /**
     * Function to set quote shipping price
     * @param $shippingPrice
     * @return bool
     */
    public function setQuoteShipping($placeId) {
        $quote = $this->checkoutSession->getQuote();

        $places = $this->getPlaces();

        if ($placeId) {
            foreach ($places as $place) {
                if ($place['place_id'] == $placeId) {
                    $method = 'estoniansmartpost_' . $place['place_id'];
                    $quote->getShippingAddress()->setShippingDescription('Estonian SmartPost - ' . trim($place['name']));
                    $quote->getShippingAddress()->setShippingMethod($method);
                    $quote->getShippingAddress()->save();
                    $quote->save();
                    $methodCode = $place['place_id'];
                    return $methodCode;
                }
            }
        }
        return true;
    }
}

<?php
namespace Ambientia\Smartpost\Model\Carrier;

use Magento\Catalog\Model\ProductRepository;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;
use Ambientia\Smartpost\Model\Observer\CheckoutAfter;

class Smartpost extends \Magento\Shipping\Model\Carrier\AbstractCarrier
    implements \Magento\Shipping\Model\Carrier\CarrierInterface
{

    const SMARTPOST_NOSMARTPOST_PRODUCT_ATTRIBUTE = 'noestoniansmartpost';

    const CARRIER_CODE = 'estoniansmartpost';

    /**
     * @var string
     */
    protected $_code = self::CARRIER_CODE;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart = null;

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $_http = null;

    protected $service;

    protected $productRepository;

    protected $rateResult;

    protected $_rateResultFactory;
    protected $_rateMethodFactory;

    protected $_helper;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Ambientia\Smartpost\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Magento\Shipping\Model\Rate\Result $rateResult,
        \Ambientia\Smartpost\Helper\Data $helper,
        ProductRepository $productRepository,
        array $data = []
    ) {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        $this->_rateErrorFactory = $rateErrorFactory;

        $this->_cart = $cart;
        $this->_http = $httpClientFactory;

        $this->productRepository = $productRepository;
        $this->rateResult = $rateResult;
        $this->_helper = $helper;

        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        $data = [];
        foreach ($this->_helper->getPlaces() as $point) {
            $data[$point['place_id']] = $point;
        }
        return $data;
    }

    /**
     * @param RateRequest $request
     * @return bool|Result
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        // check for excluded products
        $isCheckEnabled = $this->getConfigFlag('check_enable');

        if ($isCheckEnabled) {
            $excludedProduct = false;

            foreach ($request->getAllItems() as $item) {
                  $product = $this->productRepository->getById($item->getProduct()->getId());

                 $noestoniansmartpost = $product->getData(self::SMARTPOST_NOSMARTPOST_PRODUCT_ATTRIBUTE);
                 if ($noestoniansmartpost) {
                     $excludedProduct = true;
                 }
             }

            if ($excludedProduct) {
                //return false;
                if ($this->getConfigData('showmethod')) {
                    $error = $this->_rateErrorFactory->create();
                    $error->setCarrier($this->_code);
                    $error->setCarrierTitle($this->getConfigData('title'));
                    $errorMsg = $this->getConfigData('specificerrmsg');
                    $error->setErrorMessage($errorMsg ? $errorMsg : __('One or more products are not allowed to be shipped with Smartpost.'));
                    return $error;
                }
                else {
                    return false;
                }
            }
        }

        // check for parcel size & price
        $isSizeCheckEnabled = $this->getConfigFlag('calculate_size');

        if ($isSizeCheckEnabled) {
             $parcelPrice = $this->_helper->checkCart();
             if ($parcelPrice) {
                 $specificParcelShippingPrice = $parcelPrice;
             }
             else {
                 $specificParcelShippingPrice = false;
             }
         }
        else {
            $specificParcelShippingPrice = false;
        }

        $freeBoxes = 0;
        if ($request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {

                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }

                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                            $freeBoxes += $item->getQty() * $child->getQty();
                        }
                    }
                } elseif ($item->getFreeShipping()) {
                    $freeBoxes += $item->getQty();
                }
            }
        }
        $this->setFreeBoxes($freeBoxes);

        $isFreeShippingEnabled = $this->getConfigFlag('free_shipping_enable');
        $packageValue = $request->getPackageValueWithDiscount();

        if ($specificParcelShippingPrice) {
            $shippingPrice = $specificParcelShippingPrice;
        }
        else {
            $shippingPrice = $this->getConfigData('handling_fee');
        }

        if (empty($shippingPrice) || !is_numeric($shippingPrice) || $shippingPrice < 0) {
            $shippingPrice = '0.00';
        }

        if ($isFreeShippingEnabled && $packageValue >= $this->getConfigData('free_shipping_subtotal')) {
            $shippingPrice = '0.00';
        }

        if ($request->getPackageQty() == $this->getFreeBoxes()) {
            $shippingPrice = '0.00';
        }

        $places = $this->_helper->getPlaces();

        $result = $this->_rateResultFactory->create();

        $method = $this->_rateMethodFactory->create();

        $method->setCarrier($this->_code);
        $method->setCarrierTitle($this->getConfigData('title'));

        //$method->setMethod($this->_code);
        $method->setMethodTitle($this->getConfigData('name'));

        $method->setPrice($shippingPrice);
        $method->setCost($shippingPrice);

        $result->append($method);

        //check if shipping method has been pre-set to estoniansmartpost

        $items = $request->getAllItems();
        if (!$items) {
            return $result;
        }

        $shippingMethod = $items[0]->getQuote()->getShippingAddress()->getShippingMethod();
        if (strpos($shippingMethod,'estoniansmartpost') !== false) {
            $preSetPlaceId = preg_replace("/[^0-9]/","",$shippingMethod);
            foreach ($places as $place) {
                if($place['place_id'] == $preSetPlaceId) {
                    $method = $this->_rateMethodFactory->create();
                    $method->setCarrier($this->_code);
                    $method->setCarrierTitle($this->getConfigData('title'));
                    $method->setMethod($place['place_id']);
                    $method->setMethodTitle($place['name']);
                    $method->setCost($shippingPrice);
                    $method->setPrice($shippingPrice);
                    $result->append($method);
                }
            }
        }

        return $result;
    }
}

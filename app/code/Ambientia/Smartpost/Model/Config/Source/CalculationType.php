<?php
namespace Ambientia\Smartpost\Model\Config\Source;

class CalculationType implements \Magento\Framework\Option\ArrayInterface {

    public function toOptionArray() {
        return [
        ['value' => 'order_total', 'label' => __('Order items combined')],
        ['value' => 'largest_item', 'label' => __('Largest order item')],
        ];
    }
}
<?php
namespace Ambientia\Smartpost\Model\ResourceModel\ItellaEstonia;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Ambientia\Smartpost\Model\ItellaEstonia','Ambientia\Smartpost\Model\ResourceModel\ItellaEstonia');
    }
}

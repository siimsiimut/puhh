<?php
namespace Ambientia\Smartpost\Model\ResourceModel;
class ItellaEstonia extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('itella_smartpost_estonia_parcels','id');
    }
}

<?php

namespace Ambientia\Smartpost\Model\Source;

class Lang implements \Magento\Framework\Option\ArrayInterface
{

    const SMARTPOST_LANG_ET = 'et';
    const SMARTPOST_LANG_EN = 'en';
    const SMARTPOST_LANG_RU = 'ru';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray() {
        $options = array();
        $options[] = array(
            'label' => __('Estonian'),
            'value' => self::SMARTPOST_LANG_ET
        );
        $options[] = array(
            'label' => __('English'),
            'value' => self::SMARTPOST_LANG_EN
        );
        $options[] = array(
            'label' => __('Russian'),
            'value' => self::SMARTPOST_LANG_RU
        );
        return $options;
    }
}

<?php
/**
 * Copyright © 2017 Ambientia. All rights reserved.
 */

namespace Ambientia\Smartpost\Model;


class Cron extends \Magento\Framework\Model\AbstractModel
{
    protected $helper;

    protected $smartpostPlaces;

    protected $logger;

    public function __construct(
        \Ambientia\Smartpost\Helper\Data $helper,
        \Ambientia\Smartpost\Model\ItellaEstoniaFactory $itellaEstoniaPlaces,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->helper = $helper;
        $this->smartpostPlaces = $itellaEstoniaPlaces;
        $this->logger = $logger;
    }

    public function execute()
    {
        $places = $this->helper->getPlacesFromSmartpost();

        foreach ($places as $place) {
            $parcelPoint = $this->smartpostPlaces->create();
            $oldEntry = $parcelPoint->getCollection()->addFieldToFilter('place_id', $place['place_id'])->addFieldToSelect('id')->getFirstItem();
            if ($oldEntry->getId()) {
                $parcelPoint->setData('id', $oldEntry->getId());
            }
            $parcelPoint->setData('place_id', $place['place_id']);
            $parcelPoint->setData('name', $place['name']);
            $parcelPoint->setData('city', $place['city']);
            $parcelPoint->setData('address', $place['address']);
            $parcelPoint->setData('opened', $place['opened']);
            $parcelPoint->setData('group_id', $place['group_id']);
            $parcelPoint->setData('group_name', $place['group_name']);
            $parcelPoint->setData('group_sort', $place['group_sort']);
            $parcelPoint->setData('description', $place['description']);
            $parcelPoint->save();
        }
    }
}

<?php

namespace Ambientia\Smartpost\Model\Observer;

use Magento\Checkout\Model\Session;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderRepository;
use Magento\Framework\Event\ObserverInterface;

class CheckoutAfter
    implements ObserverInterface
{

    const SMARTPOST_ALLOWED_CURRENCY = 'EUR';
    const SMARTPOST_METHOD_PREFIX = 'estoniansmartpost';

    const SMARTPOST_LIVE_URL = 'http://eteenindus.smartpost.ee/data/orders.phpserialize';

    protected $checkoutSession;
    protected $repository;
    protected $scopeConfig;
    protected $helper;
    protected $logger;

    public function __construct(
        Session $session,
        OrderRepository $repository,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Ambientia\Smartpost\Helper\Data $helper,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->checkoutSession = $session;
        $this->repository = $repository;
        $this->helper = $helper;
        $this->logger = $logger;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /* @var $order \Magento\Sales\Model\Order */
        // $incrementId = $this->checkoutSession->getLastRealOrder()->getId();
        // $order = $this->repository->get($incrementId);
        $order = $observer->getEvent()->getInvoice()->getOrder();
        $this->_handleOrder($order);
    }

    /**
     * Data exchange documentation http://www.estoniansmartpost.ee/andmevahetus.html
     *
     * @param $order Order
     */
    private function _handleOrder($order) {

        $shippingMethod = $order->getShippingMethod();

        if (stripos($shippingMethod, self::SMARTPOST_METHOD_PREFIX) !== false) {
            $errors = array();

            $placeId = preg_replace("/[^0-9]/","",$shippingMethod);

            $places = $this->helper->getPlaces();

            foreach ($places as $place) {
                if ($place['place_id'] == $placeId) {
                    $placeName = trim($place['name']);
                }
            }

            $barcode = null;
            if ($this->scopeConfig->getValue('carriers/estoniansmartpost/data_exchange_enable')) {
                if ($order->getOrderCurrencyCode() != self::SMARTPOST_ALLOWED_CURRENCY) {
                    $msg = __('Estonian SmartPost supported currency is EUR!');
                    $errors[] = $msg;
                }

                $id = $this->scopeConfig->getValue('carriers/estoniansmartpost/id');
                if (empty($id)) {
                    $msg = __('Estonian SmartPost id is not set!');
                    $errors[] = $msg;
                }

                $username = $this->scopeConfig->getValue('carriers/estoniansmartpost/username');
                $password = $this->scopeConfig->getValue('carriers/estoniansmartpost/password');
                if (empty($username) || empty($password)) {
                    $msg = __('Estonian SmartPost username or password is not set!');
                    $errors[] = $msg;
                }

                if (empty($placeId) || !is_numeric($placeId)) {
                    $msg = __('Estonian SmartPost missing place id!');
                    $errors[] = $msg;
                }

                if (count($errors) == 0) {
                    $lang = $this->scopeConfig->getValue('carriers/estoniansmartpost/lang');

                    $orderData = $this->_createOrderData($order, $placeId, $lang, $barcode, $this->scopeConfig->getValue('carriers/estoniansmartpost/cash_delivery_enable'));
                    try {
                        $response = $this->clientRequest($username, $password, 'POST', $orderData);
                        if ($response->isSuccessful()) {
                            $response2 = $this->clientRequest($username, $password, 'GET', []);
                            if ($response2->isSuccessful()) {
                                $body = unserialize($response2->getBody());
                                $last = end($body);
                                if($last[8] == $order->getIncrementId()){
                                    $barcode = $last[0];
                                    $msg = sprintf('Shipping data successfully sent to Estonian SmartPost. Shipment barcode: %s', $barcode);
                                }
                                else{
                                    $msg = 'Shipping data successfully sent to Estonian SmartPost. But BARCODE not found!';
                                }
                            }
                            else{
                                $msg = 'Shipping data successfully sent to Estonian SmartPost. But BARCODE query not successful';
                            }
                            $body = unserialize($response2->getBody());
                            $order->addStatusToHistory($order->getStatus(), $msg, false);
                        } else {
                            $msg = sprintf('Failed to send shipping data to Estonian SmartPost: %s', $response->getMessage(). ' - ' .$response->getBody());
                            $this->logger->debug(print_r($msg, true));
                            $order->addStatusToHistory($order->getStatus(), $msg, false);
                        }
                    } catch (\Exception $ex) {
                        $msg = sprintf('Failed to send shipping data to Estonian SmartPost: %s ', $ex->getMessage());
                        $this->logger->debug(print_r($msg, true));
                        $order->addStatusToHistory($order->getStatus(), $msg, false);
                    }
                } else {
                    $msg = sprintf('Estonian SmartPost cannot complete data exchange because following errors:') . '<br />';
                    foreach ($errors as $error) {
                        $msg .= ' - ' .$error. '<br />';
                    }
                    $order->addStatusToHistory($order->getStatus(), $msg, false);
                }
            }
            $shippingDescription = strip_tags($order->getShippingDescription());
            $shippingDescription .= !empty($placeName) ? ' - ' .$placeName. ' (' .$placeId. ')' : '';
            $order->setShippingDescription($shippingDescription);
            if($barcode){
                $order->setData('tracking_code', $barcode);
            }
            $order->save();
        }
    }

    private function clientRequest($username, $password, $type, $orderData) {
        $client = new \Zend_Http_Client(self::SMARTPOST_LIVE_URL);
        $client->setAuth($username, $password);
        if($type == 'POST'){
            $client->setParameterPost(array('data' => serialize($orderData)));
        }
        $response = $client->request($type);
        return $response;
    }

    /**
     * @param $order Order
     * @param $placeId
     * @param $language
     * @param $barcode
     * @param $enableCash
     * @return array
     */
    private function _createOrderData($order, $placeId, $language, $barcode, $enableCash) {
        $result = array();
        //$result['barcode'] = $barcode;
        $result['place_id'] = (int)$placeId;
        $result['ref'] = $order->getIncrementId();
        $result['name'] = $order->getShippingAddress()->getFirstname().' '.$order->getShippingAddress()->getLastname();
        $result['phone'] = $order->getShippingAddress()->getTelephone();
        $result['weight'] = round($order->getWeight(), 1);
        if ($enableCash) {
            $result['cash'] = round($order->getTotalDue(), 2);
        }
        $result['email'] = $order->getCustomerEmail();
        $result['lang'] = $language;
        return array($result);
    }

}

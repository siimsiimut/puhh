<?php
namespace Ambientia\Smartpost\Model;
class ItellaEstonia extends \Magento\Framework\Model\AbstractModel implements \Ambientia\Smartpost\Api\Data\ItellaEstoniaInterface, \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'itella_smartpost_estonia_parcels';

    protected function _construct()
    {
        $this->_init('Ambientia\Smartpost\Model\ResourceModel\ItellaEstonia');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}

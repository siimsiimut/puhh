<?php
namespace Ambientia\Smartpost\Api;

use Ambientia\Smartpost\Api\Data\ItellaEstoniaInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface ItellaEstoniaRepositoryInterface 
{
    public function save(ItellaEstoniaInterface $page);

    public function getById($id);

    public function getList(SearchCriteriaInterface $criteria);

    public function delete(ItellaEstoniaInterface $page);

    public function deleteById($id);
}

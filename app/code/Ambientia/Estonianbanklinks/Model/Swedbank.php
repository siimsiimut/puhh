<?php
namespace Ambientia\Estonianbanklinks\Model;

use Magento\Payment\Model\InfoInterface;
use Magento\Payment\Model\Method\AbstractMethod;

class Swedbank extends AbstractMethod {

    protected $_code = 'ambientia_estonianbanklinks_swedbank';

    public function capture(InfoInterface $payment, $amount)
    {

    }

}
<?php
namespace Ambientia\Estonianbanklinks\Model;
class Nonce extends \Magento\Framework\Model\AbstractModel implements NonceInterface, \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'ambientia_estonianbanklinks_nonce';

    protected function _construct()
    {
        $this->_init('Ambientia\Estonianbanklinks\Model\ResourceModel\Nonce');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}

<?php
namespace Ambientia\Estonianbanklinks\Model;

use Magento\Payment\Model\InfoInterface;
use Magento\Payment\Model\Method\AbstractMethod;

class Seb extends AbstractMethod {

    protected $_code = 'ambientia_estonianbanklinks_seb';

    public function capture(InfoInterface $payment, $amount)
    {

    }

}
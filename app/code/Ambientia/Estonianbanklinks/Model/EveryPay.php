<?php
namespace Ambientia\Estonianbanklinks\Model;

use Magento\Payment\Model\InfoInterface;
use Magento\Payment\Model\Method\AbstractMethod;

class EveryPay extends AbstractMethod {

    protected $_code = 'ambientia_estonianbanklinks_everypay';

    public function capture(InfoInterface $payment, $amount)
    {

    }

}
<?php
namespace Ambientia\Estonianbanklinks\Model\ResourceModel\Nonce;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Ambientia\Estonianbanklinks\Model\Nonce','Ambientia\Estonianbanklinks\Model\ResourceModel\Nonce');
    }
}

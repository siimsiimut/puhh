<?php
namespace Ambientia\Estonianbanklinks\Model\ResourceModel;
class Nonce extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('ambientia_estonianbanklinks_nonce','ambientia_estonianbanklinks_nonce_id');
    }
}

<?php
namespace Ambientia\Estonianbanklinks\Model;

use Ambientia\Estonianbanklinks\Api\NonceRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\ObjectManagerInterface;

class NonceManagement
{
    protected $objectManager;
    protected $nonceRepository;
    protected $searchCriteriaBuilder;

    function __construct(
        ObjectManagerInterface $objectManager,
        NonceRepositoryInterface $nonceRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        $this->objectManager = $objectManager;
        $this->nonceRepository = $nonceRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    public function generateNonce()
    {
        /** @var Nonce $nonce */
        $nonce = $this->objectManager->create(Nonce::class);
        $nonce->setValue(uniqid(true));
        $nonce->getResource()->save($nonce);
        return $nonce->getValue();
    }

    public function saveNonce($value)
    {
        /** @var Nonce $nonce */
        $nonce = $this->objectManager->create(Nonce::class);
        $nonce->setValue($nonce);
        $nonce->getResource()->save($nonce);
    }

    public function verifyNonce($value)
    {
        /** @var NonceRepository $nonceList */
        $nonceList = $this->nonceRepository->getList(
            $this->searchCriteriaBuilder->addFilter('value', $value)->create()
        );
        if ($nonceList->getTotalCount() > 0) {
            return time() - strtotime($nonceList->getList()[0]['created_at']) <= 60;
        }
        /** @var Nonce $nonce */
        $nonce = $this->objectManager->create(Nonce::class);
        $nonce->setValue($value);
        $nonce->getResource()->save($nonce);
        return true;
    }


}
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'ambientia_estonianbanklinks_swedbank',
                component: 'Ambientia_Estonianbanklinks/js/view/payment/method-renderer/swedbank'
            },
            {
                type: 'ambientia_estonianbanklinks_seb',
                component: 'Ambientia_Estonianbanklinks/js/view/payment/method-renderer/seb'
            },
            {
                type: 'ambientia_estonianbanklinks_lhv',
                component: 'Ambientia_Estonianbanklinks/js/view/payment/method-renderer/lhv'
            },
            {
                type: 'ambientia_estonianbanklinks_everypay',
                component: 'Ambientia_Estonianbanklinks/js/view/payment/method-renderer/everypay'
            }
        );
        /** Add view logic here if needed */
        return Component.extend({});
    }
);
define(
    [
        'Magento_Checkout/js/view/payment/default',
        'Magento_Paypal/js/action/set-payment-method',
        'mage/url'
    ],
    function (Component, setPaymentInformationAction, url) {
        'use strict';

        return Component.extend({
            redirectAfterPlaceOrder: false,
            defaults: {
                template: 'Ambientia_Estonianbanklinks/payment/default'
            },
            startPayment: function(){
                this.selectPaymentMethod();
                this.placeOrder();
            },
            afterPlaceOrder: function(){
                window.location.replace(url.build('ambientia_estonianbanklinks/payment/start/bankcode/lhv'));
            }
        });
    }
);
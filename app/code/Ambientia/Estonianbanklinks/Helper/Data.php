<?php
namespace Ambientia\Estonianbanklinks\Helper;

use Magento\Backend\App\Config;
use Magento\Framework\App\Helper\AbstractHelper;
use Banklink\Protocol\iPizza;
use Banklink\Swedbank;
use Banklink\SEB;
use Banklink\LHV;
use Banklink\EveryPay;
use Magento\Framework\App\Helper\Context;

class Data extends AbstractHelper
{
    protected $config;

    public function __construct(
        Context $context,
        Config $config
    )
    {
        $this->config = $config;
        parent::__construct($context);
    }

    public function getBanklink($bankcode)
    {
        switch ($bankcode) {
            case 'swedbank':
                $protocol = new iPizza(
                    $this->config->getValue('payment/ambientia_estonianbanklinks_swedbank/vk_snd_id'),
                    $this->config->getValue('payment/ambientia_estonianbanklinks_swedbank/vk_name'),
                    $this->config->getValue('payment/ambientia_estonianbanklinks_swedbank/vk_acc'),
                    $this->config->getValue('payment/ambientia_estonianbanklinks_swedbank/vk_privkey'),
                    $this->config->getValue('payment/ambientia_estonianbanklinks_swedbank/vk_pubkey'),
                    $this->_urlBuilder->getUrl('ambientia_estonianbanklinks/payment/end', ['bankcode' => $bankcode]),
                    true
                );
                return new Swedbank(
                    $protocol,
                    false,
                    $this->config->getValue('payment/ambientia_estonianbanklinks_swedbank/url')
                );

            case 'seb':
                $protocol = new iPizza(
                    $this->config->getValue('payment/ambientia_estonianbanklinks_seb/vk_snd_id'),
                    $this->config->getValue('payment/ambientia_estonianbanklinks_seb/vk_name'),
                    $this->config->getValue('payment/ambientia_estonianbanklinks_seb/vk_acc'),
                    $this->config->getValue('payment/ambientia_estonianbanklinks_seb/vk_privkey'),
                    $this->config->getValue('payment/ambientia_estonianbanklinks_seb/vk_pubkey'),
                    $this->_urlBuilder->getUrl('ambientia_estonianbanklinks/payment/end', ['bankcode' => $bankcode]),
                    true
                );
                return new SEB(
                    $protocol,
                    false,
                    $this->config->getValue('payment/ambientia_estonianbanklinks_seb/url')
                );

            case 'lhv':
                $protocol = new iPizza(
                    $this->config->getValue('payment/ambientia_estonianbanklinks_lhv/vk_snd_id'),
                    $this->config->getValue('payment/ambientia_estonianbanklinks_lhv/vk_name'),
                    $this->config->getValue('payment/ambientia_estonianbanklinks_lhv/vk_acc'),
                    $this->config->getValue('payment/ambientia_estonianbanklinks_lhv/vk_privkey'),
                    $this->config->getValue('payment/ambientia_estonianbanklinks_lhv/vk_pubkey'),
                    $this->_urlBuilder->getUrl('ambientia_estonianbanklinks/payment/end', ['bankcode' => $bankcode]),
                    true
                );
                return new LHV(
                    $protocol,
                    false,
                    $this->config->getValue('payment/ambientia_estonianbanklinks_lhv/url')
                );

            case 'everypay':
                return new EveryPay(
                    $this->config->getValue('payment/ambientia_estonianbanklinks_everypay/api_username'),
                    $this->config->getValue('payment/ambientia_estonianbanklinks_everypay/api_secret'),
                    $this->config->getValue('payment/ambientia_estonianbanklinks_everypay/account_id'),
                    $this->config->getValue('payment/ambientia_estonianbanklinks_everypay/request_url'),
                    $this->_urlBuilder->getUrl('ambientia_estonianbanklinks/everypay/end'),
                    $this->_urlBuilder->getUrl('ambientia_estonianbanklinks/everypay/end', ['auto' => 1])
                );

            default:
                throw new \Exception('Invalid bankcode ' . $bankcode);

        }
    }
}
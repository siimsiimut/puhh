<?php
namespace Ambientia\Estonianbanklinks\Logger;

use Magento\Framework\Logger\Handler\Base;

class Handler extends Base
{
    protected $fileName = '/var/log/ambientia_estonianbanklinks.log';
    protected $loggerType = \Monolog\Logger::DEBUG;
}
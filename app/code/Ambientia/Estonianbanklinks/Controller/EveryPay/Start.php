<?php
namespace Ambientia\Estonianbanklinks\Controller\EveryPay;

use Ambientia\Estonianbanklinks\Helper\Data;
use Ambientia\Estonianbanklinks\Model\Nonce;
use Banklink\EveryPay;
use Magento\Backend\App\Config;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Sales\Model\Order;
use \Ambientia\Estonianbanklinks\Logger\Logger;

class Start extends Action {

    protected $cartManagement;
    protected $checkoutSession;
    protected $config;
    protected $logger;
    protected $helper;

    public function __construct(Context $context,
        CartManagementInterface $cartManagement,
        Session $checkoutSession,
        Config $config,
        Logger $logger,
        Data $helper
    )
    {
        $this->cartManagement = $cartManagement;
        $this->checkoutSession = $checkoutSession;
        $this->config = $config;
        $this->logger = $logger;
        $this->helper = $helper;
        parent::__construct($context);
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        /** @var Order $order */
        $order = $this->checkoutSession->getLastRealOrder();
        $order->setStatus('pending_payment');
        $order->setState('pending_payment');
        $order->getResource()->save($order);

        /** @var EveryPay $banklink */
        $banklink = $this->helper->getBanklink('everypay');

        $billingAddress = $order->getBillingAddress();
        /** @var Nonce $nonce */
        $nonce = $this->_objectManager->create(Nonce::class);
        $nonce->setValue(uniqid(true));
        $nonce->getResource()->save($nonce);

        $banklink->preparePaymentRequest(
            $order->getId(),
            round($order->getGrandTotal(), 2),
            $order->getCustomerEmail(),
            $order->getRemoteIp(),
            implode(', ', $billingAddress->getStreet()),
            $billingAddress->getCountryId(),
            $billingAddress->getCity(),
            $billingAddress->getPostcode(),
            $nonce->getValue()
        );
        $this->logger->info('Start payment', $banklink->getRequestData());

        $html = '<html><body onload="document.querySelector(\'form\').submit()"><form action="' . $banklink->getRequestUrl() . '" method="post">' .
            $banklink->buildRequestHtml()
        . '</form></body></html>';

        echo $html;

    }
}
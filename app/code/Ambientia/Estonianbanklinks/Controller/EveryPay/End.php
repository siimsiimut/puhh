<?php
namespace Ambientia\Estonianbanklinks\Controller\EveryPay;

use Ambientia\Estonianbanklinks\Api\NonceRepositoryInterface;
use Ambientia\Estonianbanklinks\Helper\Data;
use Ambientia\Estonianbanklinks\Model\NonceManagement;
use Banklink\EveryPay;
use Magento\Backend\App\Config;
use Magento\Checkout\Model\DefaultConfigProvider;
use Magento\Checkout\Model\Session;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Model\Service\InvoiceService;
use Ambientia\Estonianbanklinks\Logger\Logger;

class End extends Action {

    protected $cartManagement;
    protected $checkoutSession;
    protected $config;
    protected $invoiceService;
    protected $defaultConfigProvider;
    protected $orderRepository;
    protected $logger;
    protected $helper;
    protected $nonceRepository;
    protected $searchCriteriaBuilder;
    protected $nonceManagement;


    public function __construct(Context $context,
        CartManagementInterface $cartManagement,
        Session $checkoutSession,
        Config $config,
        InvoiceService $invoiceService,
        DefaultConfigProvider $defaultConfigProvider,
        OrderRepository $orderRepository,
        Logger $logger,
        Data $helper,
        NonceRepositoryInterface $nonceRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        NonceManagement $nonceManagement
    )
    {
        $this->cartManagement = $cartManagement;
        $this->checkoutSession = $checkoutSession;
        $this->config = $config;
        $this->invoiceService = $invoiceService;
        $this->defaultConfigProvider = $defaultConfigProvider;
        $this->orderRepository = $orderRepository;
        $this->logger = $logger;
        $this->helper = $helper;
        $this->nonceRepository = $nonceRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->nonceManagement = $nonceManagement;

        parent::__construct($context);
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        /** @var EveryPay $banklink */
        $banklink = $this->helper->getBanklink('everypay');

        $paymentResponse = $banklink->handleResponse($this->getRequest()->getParams());
        $order = $this->orderRepository->get($paymentResponse->getOrderId());


        if ($paymentResponse->isSuccessful()
        && $this->nonceManagement->verifyNonce($paymentResponse->getRawResponseData()['nonce'])) {

            $this->logger->info('End payment success', $paymentResponse->getRawResponseData());

            if ($order->getTotalDue() > 0) {
                $order->setState('processing');
                $order->setStatus('processing');
                $order->setTotalPaid($paymentResponse->getSum());
                $order->save();

                /** @var Order\Invoice $invoice */
                $invoice = $this->invoiceService->prepareInvoice($order);
                $invoice->setGrandTotal($paymentResponse->getSum());
                $invoice->capture();

                /** @var Order\Invoice\Sender\EmailSender $emailSender */
                $emailSender = $this->_objectManager->get(Order\Invoice\Sender\EmailSender::class);
                $emailSender->send($order, $invoice);

                $invoiceResource = $invoice->getResource();
                $invoiceResource->save($invoice);
            }


            if (!$this->getRequest()->getParam('auto')) {
                return $this->resultRedirectFactory->create()->setUrl($this->defaultConfigProvider->getDefaultSuccessPageUrl());
            }

        } else {

            $this->logger->info('End payment fail: ' . $paymentResponse->getErrorMessage(), $paymentResponse->getRawResponseData());

            $order->setState('canceled');
            $order->setStatus('canceled');
            $order->save();
            $this->checkoutSession->restoreQuote();

            return $this->resultRedirectFactory->create()->setUrl($this->defaultConfigProvider->getCheckoutUrl());
        }

    }
}
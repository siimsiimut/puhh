<?php
namespace Ambientia\Estonianbanklinks\Controller\Payment;

use Ambientia\Estonianbanklinks\Helper\Data;
use Banklink\Response\PaymentResponse;
use Magento\Backend\App\Config;
use Magento\Checkout\Model\DefaultConfigProvider;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Model\Service\InvoiceService;
use Ambientia\Estonianbanklinks\Logger\Logger;
use Magento\Quote\Api\CartRepositoryInterface;


class End extends Action implements CsrfAwareActionInterface
{

    protected $cartManagement;
    protected $checkoutSession;
    protected $config;
    protected $invoiceService;
    protected $defaultConfigProvider;
    protected $orderRepository;
    protected $helper;
    protected $logger;
    protected $quoteRepository;

    public function __construct(Context $context,
        CartManagementInterface $cartManagement,
        Session $checkoutSession,
        Config $config,
        InvoiceService $invoiceService,
        DefaultConfigProvider $defaultConfigProvider,
        OrderRepository $orderRepository,
        Data $helper,
        Logger $logger,
        CartRepositoryInterface $quoteRepository
    )
    {
        $this->cartManagement = $cartManagement;
        $this->checkoutSession = $checkoutSession;
        $this->config = $config;
        $this->invoiceService = $invoiceService;
        $this->defaultConfigProvider = $defaultConfigProvider;
        $this->orderRepository = $orderRepository;
        $this->helper = $helper;
        $this->logger = $logger;
        $this->quoteRepository = $quoteRepository;

        parent::__construct($context);
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $banklink = $this->helper->getBanklink($this->getRequest()->getParam('bankcode'));

        /** @var PaymentResponse $paymentResponse */
        $paymentResponse = $banklink->handleResponse($this->getRequest()->getParams());
        $order = $this->orderRepository->get($paymentResponse->getOrderId());

        if ($paymentResponse->isSuccesful()) {
            $this->logger->info('End payment success', $paymentResponse->getRawResponseData());

            if ($order->getTotalDue() > 0) {
                $order->setState('processing');
                $order->setStatus('processing');
                $order->setTotalPaid($paymentResponse->getSum());
                $order->save();

                /** @var Order\Invoice $invoice */
                $invoice = $this->invoiceService->prepareInvoice($order);
                $invoice->setGrandTotal($paymentResponse->getSum());
                $invoice->capture();

                /** @var Order\Invoice\Sender\EmailSender $emailSender */
                $emailSender = $this->_objectManager->get(Order\Invoice\Sender\EmailSender::class);
                $emailSender->send($order, $invoice);

                $invoiceResource = $invoice->getResource();
                $invoiceResource->save($invoice);
            }


            if ($paymentResponse->getRawResponseData()['VK_AUTO'] != 'Y') {
                return $this->resultRedirectFactory->create()->setUrl($this->defaultConfigProvider->getDefaultSuccessPageUrl());
            }

        } else {

            $this->logger->info('End payment fail', $paymentResponse->getRawResponseData());


            if ($order->getId() && $order->getState() != Order::STATE_CANCELED) {
                $order->registerCancellation('')->save();
            }

            $this->restoreQuote($order);

            return $this->resultRedirectFactory->create()->setUrl($this->defaultConfigProvider->getCheckoutUrl());
        }

    }
    public function restoreQuote($order)
    {
        if ($order->getId()) {
            try {
                $quote = $this->quoteRepository->get($order->getQuoteId());
                $quote->setIsActive(1)->setReservedOrderId(null);
                $this->quoteRepository->save($quote);
                $this->checkoutSession->replaceQuote($quote)->unsLastRealOrderId();
                return true;
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            }
        }
        return false;
    }
    /**
     * @inheritDoc
     */
    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }
    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }
}
<?php
namespace Ambientia\Estonianbanklinks\Controller\Payment;

use Ambientia\Estonianbanklinks\Helper\Data;
use Magento\Backend\App\Config;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Sales\Model\Order;

class Start extends Action {

    protected $cartManagement;
    protected $checkoutSession;
    protected $config;
    protected $helper;
    protected $logger;
    protected $_storeManager;

    public function __construct(Context $context,
        CartManagementInterface $cartManagement,
        Session $checkoutSession,
        Config $config,
        Data $helper,
        \Ambientia\Estonianbanklinks\Logger\Logger $logger,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->cartManagement = $cartManagement;
        $this->checkoutSession = $checkoutSession;
        $this->config = $config;
        $this->helper = $helper;
        $this->logger = $logger;
        $this->_storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $allowed_methods = array(
            'ambientia_estonianbanklinks_seb',
            'ambientia_estonianbanklinks_lhv',
            'ambientia_estonianbanklinks_swedbank',
            'ambientia_estonianbanklinks_everypay'
        );
        /** @var Order $order */
        $order = $this->checkoutSession->getLastRealOrder();
        $payment = $order->getPayment();
        if ($payment) {
            $method = $payment->getMethodInstance();
            $paymentMethod = $method->getCode();
        }


        if (!$payment or !in_array($paymentMethod, $allowed_methods) or $order->getStatus() != 'processing') {
            return $this->resultRedirectFactory->create()->setUrl($this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB));
        } else {
            $order->setStatus('pending_payment');
            $order->setState('pending_payment');

            $order->getResource()->save($order);

            $banklink = $this->helper->getBanklink($this->getRequest()->getParam('bankcode'));

            $paymentRequest = $banklink->preparePaymentRequest(
                $order->getId(),
                round($order->getGrandTotal(), 2),
                __('Arve') . ' ' . $order->getIncrementId()
            );
            $this->logger->info('Start payment', $paymentRequest->getRequestData());

            $html = '<html><body onload="document.querySelector(\'form\').submit()"><form action="' . $paymentRequest->getRequestUrl() . '" method="post">' .
                $paymentRequest->buildRequestHtml()
            . '</form></body></html>';

            exit($html);
        }


    }
}

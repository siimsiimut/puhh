<?php
namespace Ambientia\Estonianbanklinks\Api;

use Ambientia\Estonianbanklinks\Model\NonceInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface NonceRepositoryInterface 
{
    public function save(NonceInterface $page);

    public function getById($id);

    public function getList(SearchCriteriaInterface $criteria);

    public function delete(NonceInterface $page);

    public function deleteById($id);
}

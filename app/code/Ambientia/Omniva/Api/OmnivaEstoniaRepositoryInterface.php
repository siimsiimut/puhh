<?php
/**
 * Copyright © 2017 Ambientia. All rights reserved.
 */

namespace Ambientia\Omniva\Api;

use Ambientia\Omniva\Api\Data\OmnivaEstoniaInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface OmnivaEstoniaRepositoryInterface
{
    public function save(OmnivaEstoniaInterface $page);

    public function getById($id);

    public function getList(SearchCriteriaInterface $criteria);

    public function delete(OmnivaEstoniaInterface $page);

    public function deleteById($id);
}

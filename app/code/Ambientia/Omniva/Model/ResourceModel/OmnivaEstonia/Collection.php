<?php
/**
 * Copyright © 2017 Ambientia. All rights reserved.
 */

namespace Ambientia\Omniva\Model\ResourceModel\OmnivaEstonia;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Ambientia\Omniva\Model\OmnivaEstonia','Ambientia\Omniva\Model\ResourceModel\OmnivaEstonia');
    }
}

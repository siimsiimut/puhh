<?php
/**
 * Copyright © 2017 Ambientia. All rights reserved.
 */

namespace Ambientia\Omniva\Model\Rate;

class Result extends \Magento\Shipping\Model\Rate\Result
{

    /*
     * Disable sorting by price.
     */
    public function sortRatesByPrice()
    {
        return $this;
    }

}

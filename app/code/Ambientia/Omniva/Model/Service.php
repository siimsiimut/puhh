<?php
/**
 * Copyright © 2017 Ambientia. All rights reserved.
 */


namespace Ambientia\Omniva\Model;

class Service
{

    /**
     * Prefix used to determine Post24 shipping method.
     */
    const METHOD_PREFIX = 'post24ee_';

    /**
     * Pack machine lifetime cache ID.
     */
    const PO_TIME_CACHE_ID = 'po_time';

    /**
     * Pack machine lifetime cache TTL.
     */
    const PO_TIME_CACHE_LIFETIME = 86400;

    /**
     * Pack machine cache ID.
     */
    const PO_CACHE_ID = 'po';

    /**
     * Pack machine cache TTL.
     */
    const PO_CACHE_LIFETIME = 86400;

    /**
     * The Offload Post Office type for pack machines.
     */
    const OFFLOAD_PO_TYPE_PACK_MACHINE  = 'PACK_MACHINE';

    /**
     * The Offload Post Office type for callboxes.
     */
    const OFFLOAD_PO_TYPE_CALLBOX       = 'CALLBOX';

    /**
     * EPLIS WSDL URL.
     */
    //const EPLIS_WEB_SERVICE_WSDL    = 'https://testeservice.post.ee/epmx/services/messagesService.wsdl';
    const EPLIS_WEB_SERVICE_WSDL    = 'https://edixml.post.ee/epmx/services/messagesService.wsdl';

    /**
     * EPLIS web service URL.
     */
    //const EPLIS_WEB_SERVICE         = 'https://testeservice.post.ee/epmx/services/messagesService';
    const EPLIS_WEB_SERVICE         = 'https://edixml.post.ee/epmx/services/messagesService';

    /**
     * EPLIS web service schema.
     */
    const EPLIS_WEB_SERVICE_SCHEMA  = 'http://service.core.epmx.application.eestipost.ee/xsd';

    /**
     * Default options for SOAP client.
     *
     * @var array
     */
    private static $_soapOptions = array(
        'soap_version'  => SOAP_1_2,
        'exceptions'    => true,
        'trace'         => 1,
        'cache_wsdl'    => WSDL_CACHE_NONE,
        'location'      => self::EPLIS_WEB_SERVICE,
        'uri'           => self::EPLIS_WEB_SERVICE_SCHEMA
    );


    /**
     * SOAP client for EPLIS.
     *
     * @var SoapClient
     */
    private $_soapClient = null;

    protected $helper;

    protected $logger;
    /**
     * Default constructor.
     */
    public function __construct(
        \Ambientia\Omniva\Helper\Data $helper,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->helper = $helper;
        $this->logger = $logger;
    }


    /**
     * Is data exchange enabled.
     *
     * @var boolean
     */
    public function isExchangeEnabled()
    {
        return $this->helper->isDataExchangeEnabled();
    }

    /**
     * Get Eesti Post partner ID.
     *
     * @var string
     */
    private function _getPartnerId()
    {
        return $this->helper->getPartnerId();
    }

    /**
     * Get username for EPLIS basic authentication.
     *
     * @var string
     */
    private function _getUsername()
    {
        return $this->helper->getUserName();
    }

    /**
     * Get password for EPLIS basic authentication.
     * @var string
     */
    private function _getPassword()
    {
        return $this->helper->getPassword();
    }

    /**
     * Get Post24 main service.
     *
     * @see AmbientiaShippings_post24ee_Model_Source_Servicetype
     * @var string
     */
    private function _getService()
    {
        return $this->helper->getService();
    }

    /**
     * Get EPLIS SOAP client.
     */
    private function _getSoapClient()
    {
        if (is_null($this->_soapClient)) {
            $this->initSoapClient();
        }
        return $this->_soapClient;
    }


    /**
     * Initialize EPLIS SOAP client.
     */
    private function initSoapClient()
    {
        $username = $this->_getUsername();
        $password = $this->_getPassword();

        if (empty($username) or empty($password)) {
            throw new \Exception(__('Cannot send to EPLIS because of missing partner credentials!'));
        }

        $options = self::$_soapOptions;
        $options['login'] = $username;
        $options['password'] =  $password;

        try {
            $this->_soapClient = new \SoapClient(self::EPLIS_WEB_SERVICE_WSDL, $options);
        } catch (\Exception $e) {
            $this->logger->debug('Error creating EPLIS SOAP client: ' . $e->getMessage());
        }
    }

    /**
     * Send shipment to EPLIS.
     *
     * @param $shipment Mage_Sales_Model_Order_Shipment
     * @return boolean
     */
    public function sendEplisPreSendMsg($shipment)
    {
        if (!$this->isExchangeEnabled()) {
            return false;
        }

        $partnerId = $this->_getPartnerId();
        if (empty($partnerId)) {
            return false;
        }



        $request = $this->_createEplisPreSendMsg($partnerId, $shipment);
        if ($request !== false) {

            $client = $this->_getSoapClient();
            if (is_null($client)) {
                $this->logger->debug('The EPLIS client is not initialized');
                return false;
            }

            try {
                $response = $client->preSendMsg($request);
                $result = $this->_readEplisResult($response);
                return $result;
            } catch (\SoapFault $e) {
                $this->logger->debug('Error sending message to EPLIS (' . $e->faultcode . '): ' . $e->faultstring);
            } catch (\Exception $e) {
                $this->logger->debug('Error sending message to EPLIS: ' . $e->getMessage());
            }
        }

        return false;
    }

    /**
     * Send shipment to EPLIS.
     *
     * @param \Magento\Sales\Model\Order $shipment
     * @return boolean
     */
    public function sendEplisBusinessToClientMsg($shipment)
    {
        if (!$this->isExchangeEnabled()) {
            return false;
        }

        $partnerId = $this->_getPartnerId();
        if (empty($partnerId)) {
            return false;
        }



        $request = $this->_createEplisBusinessToClientMsg($partnerId, $shipment);
        if ($request !== false) {

            $client = $this->_getSoapClient();
            if (is_null($client)) {
                $this->logger->debug('The EPLIS client is not initialized');
                return false;
            }

            try {
                $response = $client->businessToClientMsg($request);
                $result = $this->_readEplisResult($response);
                return $result;
            } catch (\SoapFault $e) {
                $this->logger->debug('Error sending message to EPLIS (' . $e->faultcode . '): ' . $e->faultstring);
            } catch (\Exception $e) {
                $this->logger->debug('Error sending message to EPLIS: ' . $e->getMessage());
            }
        }

        return false;
    }

    /**
     * Create BusinessToClientMsg for EPLIS.
     *
     * @param $partnerId string
     * @param $shipment Mage_Sales_Model_Order_Shipment
     * @return array
     */
    private function _createEplisBusinessToClientMsg($partnerId, $shipment)
    {
        $order = $shipment;
        $email = $order->getCustomerEmail();

        $packMachineId = null;
        $shippingMethod = $order->getShippingMethod();
        if (stripos($shippingMethod, self::METHOD_PREFIX) !== false) {
            $packMachineId = substr($shippingMethod, strlen(self::METHOD_PREFIX));
        }

        if (is_null($packMachineId) or !is_numeric($packMachineId)) {
            return false;
        }

        $request = array(
            'partner'     => $partnerId,
            'interchange' => array (
                    'msg_type'  => 'elsinfov1',
                    'header'    => array(
                        'file_id'       => $shipment->getIncrementId(),
                        'sender_cd'     => $partnerId,
                        'currency_cd'   => $order->getOrderCurrencyCode()
                ),
                'item_list' => array(
                    'item' => array()
                )
            )
        );
        $weight = 0;
        foreach ($shipment->getAllItems() as $item) {
            if ($item->getProductType() != 'configurable') {
                $weight += $item->getWeight();
            }
        }
        $requestItem = $this->_createEplisItem($weight, $shipment->getShippingAddress(), $email, $packMachineId);
        $request['interchange']['item_list']['item'][] = $requestItem;

        return $request;
    }

    private function _readEplisResult($eplisResponse)
    {
        $eplisResponseArray = $this->_convertObjectToArray($eplisResponse);

        $this->logger->info('Omniva EPLIS raw response', [
            'response' => $eplisResponseArray
        ]);

        $message = isset($eplisResponseArray['prompt']) ? $eplisResponseArray['prompt'] : 'Unknown response from EPLIS';

        $result = array(
            'msg'       => $message,
            'saved'     => array(),
            'faulty'    => array()
        );

        $result['saved'] = $this->_createResult($eplisResponseArray, 'savedPacketInfo');
        $result['faulty'] = $this->_createResult($eplisResponseArray, 'faultyPacketInfo');

        return $result;
    }

    private function _createResult($eplisResponseArray, $packetInfoType)
    {
        $barcodes = array();
        if (isset($eplisResponseArray[$packetInfoType]) and !empty($eplisResponseArray[$packetInfoType])) {
            $faultyPacketInfo = $eplisResponseArray[$packetInfoType];
            if (isset($faultyPacketInfo['barcodeInfo'])) {
                $barcodeInfo = $faultyPacketInfo['barcodeInfo'];
                if (isset($barcodeInfo['clientItemId'])) {
                    $barcode = array(
                        'sku'       => $barcodeInfo['clientItemId'],
                        'barcode'   => isset($barcodeInfo['barcode']) ? $barcodeInfo['barcode'] : ''
                    );
                    $barcodes[] = $barcode;
                } else {
                    foreach ($barcodeInfo as $info) {
                        $barcode = array(
                            'sku'       => isset($info['clientItemId']) ? $info['clientItemId'] : '',
                            'barcode'   => isset($info['barcode']) ? $info['barcode'] : ''
                        );
                        $barcodes[] = $barcode;
                    }
                }
            }
        }
        return $barcodes;
    }

    /**
     * Create PreSendMsg for EPLIS.
     *
     * @param $partnerId string
     * @param $shipment Mage_Sales_Model_Order_Shipment
     * @return array
     */
    private function _createEplisPreSendMsg($partnerId, $shipment)
    {
        $order = $shipment;
        $email = $order->getCustomerEmail();

        $packMachineId = null;
        $shippingMethod = $order->getShippingMethod();
        if (stripos($shippingMethod, self::METHOD_PREFIX) !== false) {
            $packMachineId = substr($shippingMethod, strlen(self::METHOD_PREFIX));
        }

        if (is_null($packMachineId) or !is_numeric($packMachineId)) {
            return false;
        }

        $request = array(
            'partner'     => $partnerId,
            'interchange' => array (
                    'msg_type'  => 'elsinfov1',
                    'header'    => array(
                        'file_id'       => $shipment->getIncrementId(),
                        'sender_cd'     => $partnerId,
                        'currency_cd'   => $order->getOrderCurrencyCode()
                ),
                'item_list' => array(
                    'item' => array()
                )
            )
        );
        $weight = 0;
        foreach ($shipment->getAllItems() as $item) {
            if ($item->getProductType() != 'configurable') {
                $weight += $item->getWeight();
            }
        }
        $requestItem = $this->_createEplisItem($weight, $shipment->getShippingAddress(), $email, $packMachineId);
        $request['interchange']['item_list']['item'][] = $requestItem;

        return $request;
    }

    /**
     * Create shipment items for PreSendMsg.
     *
     * @param $weight string
     * @param $address  Mage_Sales_Model_Order_Address
     * @param $email string
     * @param $packMachineId string
     * @return array
     */
    private function _createEplisItem($weight, $address, $email, $packMachineId)
    {
        $service = $this->_getService();

        $eplisItem = array(
            'service'       => $service,
            'add_service' => array(
                'option' => array(
                    array(
                        'code' => 'ST'
                    ),
                    array(
                        'code' => 'SF'
                    ),
                ),
            ),
            'measures' => array(
                'weight' => $weight
            ),
            'receiverAddressee' => array(
                'person_name'       => $address->getName(),
                'phone'             => $address->getTelephone(),
                'mobile'            => $address->getTelephone(),
                'email'             => $email,
                'address'           => array(
                    'offloadPostcode'   => $packMachineId,
                    'country'           => 'EE'

                )
            ),
            'returnAddressee' => [
                'person_name' => 'Karupoeg Puhh OÜ',
                'mobile' => '+37256261262',
                'address' => \array_merge([
                    'country' =>'EE',
                    'postcode' => '11612',
                    'street' => 'Salve 2a',
                ]),
            ],
        );

        return $eplisItem;
    }

    public function getPackMachines()
    {
        return $this->_places = $this->_getPlaces();
    }

    private function _getPlaces()
    {

        $places = json_decode(file_get_contents('https://www.omniva.ee/locations.json'));
        $result = array();

        foreach ($places as $place) {
            if($place->A0_NAME == 'EE'  && $place->TYPE == 0){
                $machine = array(
                    'zip'       => $place->ZIP,
                    'name'      => $place->NAME,
                    'group_name' => $place->A1_NAME,
                    'country' => $place->A0_NAME
                );
                $result[] = $machine;
            }
        }

        if (count($result) > 0) {
            usort($result, array($this, 'sort'));
        }

        return $result;
    }

    /**
     * Sorts pack machines by the first three characters of their group_name.
     */
    public static function sort($a, $b)
    {
        $a = $a['group_name'];
        $b = $b['group_name'];
        return strncmp($a, $b, 3);
    }

    /**
     * Get Post24 pack machines map (zip codes used as keys) using Ambientia's Eesti Post account.
     */
    public function getPackMachinesMap()
    {
        $result = array();
        foreach ($this->getPackMachines() as $pm) {
            $result[$pm['zip']] = $pm;
        }
        return $result;
    }

    /**
     * Get Post24 pack machine based on given zip.
     *
     * @return array
     */
    public function getPackMachineAddress($zip)
    {
        foreach ($this->getPackMachines() as $pm) {
            if ($pm['zip'] == $zip) {
                return $pm;
            }
        }
        return array();
    }

    /**
     * Get Offload Post Offices.
     *
     * @param $partnerId string
     * @param $postOfficeType string
     * @see OFFLOAD_PO_TYPE_PACK_MACHINE
     * @see OFFLOAD_PO_TYPE_CALLBOX
     * @return array
     */
    private function _getOffloadPostOffices($partnerId, $postOfficeType = 'PACK_MACHINE')
    {
        $chachedPostOffices = $this->_getPostOfficesCache($postOfficeType);
        if ($chachedPostOffices !== false) {
            return $chachedPostOffices;
        }

        $client = $this->_getSoapClient();
        if (is_null($client)) {
            return array();
        }

        $params = array('partner' => $partnerId);

        try {
            $response = $client->offloadPOMsg($params);
        } catch (SoapFault $e) {
            $this->logger->debug('Error loading Offload Post Offices (' . $e->faultcode . '): ' . $e->faultstring);
            $response = false;
        } catch (\Exception $e) {
            $this->logger->debug('Error loading Offload Post Offices: ' . $e->getMessage());
            $response = false;
        }

        if ($response === false) {
            return array();
        }

        if (is_object($response)) {
            if (isset($response->messageError)) {
                $this->logger->debug('Error loading Offload Post Offices: ' . $response->messageError);
                return array();
            }
            if (isset($response->postOffices)) {
                $postOffices = $response->postOffices;
                if (isset($postOffices->postOffice) and is_array($postOffices->postOffice)) {
                    $result = array();
                    foreach ($postOffices->postOffice as $postOffice) {
                        if ($postOffice->type == $postOfficeType) {
                            $result[] = $this->_convertObjectToArray($postOffice);
                        }
                    }

                    if ($this->_setPostOfficesCache($postOfficeType, $result) != true) {
                        $this->logger->debug('Unable to save Offload Post Offices to cache', Zend_Log::WARN);
                    }

                    return $result;
                }
            }
        } else {
            $this->logger->debug('Error unknown Offload Post Offices response');
        }

        return array();
    }

    /**
     * Get post offices from cache based on post office type.
     *
     * @param $id string
     */
    private function _getPostOfficesCache($id)
    {
        $timeCacheId = self::PO_TIME_CACHE_ID . '_' . $id;
        $cachedTime = Mage::app()->loadCache($timeCacheId);
        if ($cachedTime !== false) {
            $cacheAge = time() - $cachedTime;
            if ($cacheAge > self::PO_TIME_CACHE_LIFETIME) {
                return false;
            }
        } else {
            return false;
        }

        $poCacheId = self::PO_CACHE_ID . '_' . $id;
        $chachedPostOffices = Mage::app()->loadCache($poCacheId);
        if ($chachedPostOffices !== false) {
            $unserializedPostOffices = unserialize($chachedPostOffices);
            if (!$unserializedPostOffices) {
                return false;
            }
            return $unserializedPostOffices;
        }
        return false;
    }

    /**
     * Set post office cache based on post office type.
     *
     * @param $id string
     * @param $postOffices array
     */
    private function _setPostOfficesCache($id, $postOffices)
    {
        $poCacheId = self::PO_CACHE_ID . '_' . $id;
        $poCacheTags = array($poCacheId);
        $poCacheLifeTime = self::PO_CACHE_LIFETIME;

        $timeCacheId = self::PO_TIME_CACHE_ID . '_' . $id;
        $timeCacheTags = array($timeCacheId);
        $timeCacheLifeTime = self::PO_TIME_CACHE_LIFETIME;

        $poSave = Mage::app()->saveCache(serialize($postOffices), $poCacheId, $poCacheTags, $poCacheLifeTime);
        $timeSave = Mage::app()->saveCache(time(), $timeCacheId, $timeCacheTags, $timeCacheLifeTime);

        return $poSave && $timeSave;
    }

    /**
     * Converts stdClass object to arry.
     *
     * @param $data stdClass
     */
    private function _convertObjectToArray($data)
    {
        if (is_object($data)) {
            $data = get_object_vars($data);
        }
        return is_array($data) ? array_map(array($this, '_convertObjectToArray'), $data) : $data;
    }

}

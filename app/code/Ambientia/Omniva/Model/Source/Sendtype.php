<?php
/**
 * Copyright © 2017 Ambientia. All rights reserved.
 */

namespace Ambientia\Omniva\Model\Source;

class Sendtype implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * Defines after order place type
     */
    const post24ee_MAIN_SEND_AFTER_ORDER = 'after_order_place';

    /**
     * Defines after order payment paid
     */
    const post24ee_MAIN_AFTER_PAYMENT = 'after_payment_paid';


    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = array();
        $options[] = array(
            'label' => __('Peale tellimuse tegemist'),
            'value' => self::post24ee_MAIN_SEND_AFTER_ORDER
        );
        $options[] = array(
            'label' => __('Peale tellimuse maksmist'),
            'value' => self::post24ee_MAIN_AFTER_PAYMENT
        );
        return $options;
    }
}

<?php
/**
 * Copyright © 2017 Ambientia. All rights reserved.
 */

namespace Ambientia\Omniva\Model\Source;

class Servicetype implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * Defines shipping service 'Postkontorist - pakiautomaati'.
     */
    const post24ee_MAIN_SERVICE_PP = 'PP';

    /**
     * Defines shipping service 'Kulleriga - pakiautomaati'.
     */
    const post24ee_MAIN_SERVICE_PU = 'PU';

    /**
     * Defines shipping service 'Pakiautomaadist - pakiautomaati'.
     */
    const post24ee_MAIN_SERVICE_PA = 'PA';


    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = array();
        $options[] = array(
            'label' => __('Postkontorist - pakiautomaati'),
            'value' => self::post24ee_MAIN_SERVICE_PP
        );
        $options[] = array(
            'label' => __('Kulleriga - pakiautomaati'),
            'value' => self::post24ee_MAIN_SERVICE_PU
        );
        $options[] = array(
            'label' => __('Pakiautomaadist - pakiautomaati'),
            'value' => self::post24ee_MAIN_SERVICE_PA
        );
        return $options;
    }
}

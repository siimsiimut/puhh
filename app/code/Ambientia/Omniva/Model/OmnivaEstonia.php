<?php
/**
 * Copyright © 2017 Ambientia. All rights reserved.
 */

namespace Ambientia\Omniva\Model;
class OmnivaEstonia extends \Magento\Framework\Model\AbstractModel implements \Ambientia\Omniva\Api\Data\OmnivaEstoniaInterface, \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'omniva_estonia_parcels';

    protected function _construct()
    {
        $this->_init('Ambientia\Omniva\Model\ResourceModel\OmnivaEstonia');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}

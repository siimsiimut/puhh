<?php
/**
 * Copyright © 2017 Ambientia. All rights reserved.
 */

namespace Ambientia\Omniva\Model\Observer;
use Magento\Framework\Event\ObserverInterface;

class PaymentAfter implements ObserverInterface
{
    protected $_objectManager;
    protected $_logger;
    protected $helper;
    protected $service;

    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Psr\Log\LoggerInterface $logger,
        \Ambientia\Omniva\Helper\Data $helper,
        \Ambientia\Omniva\Model\Service $service
    )
    {
        $this->_objectManager = $objectmanager;
        $this->_logger = $logger;
        $this->helper = $helper;
        $this->service = $service;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $sendType = $this->helper->getSendType();
        if ($sendType != \Ambientia\Omniva\Model\Source\Sendtype::post24ee_MAIN_AFTER_PAYMENT)  {
            return;
        }
        /* @var $order \Magento\Sales\Model\Order */
        $order = $observer->getPayment()->getOrder();

        $result = $this->service->sendEplisBusinessToClientMsg($order);

        if ($result !== false and is_array($result)) {
            $this->_logger->info('Payment after Omniva order send: ',
            ['response' => $result]);
            $data = $this->_createOrderComment($result);
            $order->addStatusHistoryComment($data['comment'])->setIsCustomerNotified(null);
            if(array_key_exists('tracking', $data)){
                $order->setData('tracking_code', $data['tracking']);
            }
            $order->save();
        }
    }

    private function _createOrderComment($result)
    {
        $data = [];
        $orderComment = '<b>' . __('Post24ee EPLIS') . '</b><br />';
        $orderComment .= ' - ' . __('Message') . ': ' . __($result['msg']) . '<br />';
        if (!empty($result['saved'])) {
            $orderComment .= ' --- <i>' . __('Saved') . ': </i><br />';
            foreach($result['saved'] as $saved) {
                $orderComment .= ' --- --- ' . __('SKU') . ': ' . $saved['sku'] . ' ' . __('Barcode') . ': ' . $saved['barcode'] .  '<br />';
                $data['tracking'] = $saved['barcode'];
            }
        }
        if (!empty($result['faulty'])) {
            $orderComment .= ' --- <i>' . __('Faulty') . ': </i><br />';
            foreach($result['faulty'] as $faulty) {
                $orderComment .= ' --- --- ' . __('SKU') . ': ' . $faulty['sku'] . ' ' . __('Barcode') . ': ' . $faulty['barcode'] .  '<br />';
            }
        }
        $data['comment'] = $orderComment;
        return $data;
    }

}
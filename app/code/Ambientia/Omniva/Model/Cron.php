<?php
/**
 * Copyright © 2017 Ambientia. All rights reserved.
 */

namespace Ambientia\Omniva\Model;


class Cron extends \Magento\Framework\Model\AbstractModel
{
    protected $service;

    protected $omnivaPlaces;

    protected $logger;

    public function __construct(
        \Ambientia\Omniva\Model\Service $service,
        \Ambientia\Omniva\Model\OmnivaEstoniaFactory $omnivaEstoniaFactory,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->service = $service;
        $this->omnivaPlaces = $omnivaEstoniaFactory;
        $this->logger = $logger;
    }

    public function execute()
    {
        $places = $this->service->getPackMachines();

        foreach ($places as $place) {
            $parcelPoint = $this->omnivaPlaces->create();
            $oldEntry = $parcelPoint->getCollection()->addFieldToFilter('place_id', $place['zip'])->addFieldToSelect('id')->getFirstItem();
            if ($oldEntry->getId()) {
                $parcelPoint->setData('id', $oldEntry->getId());
            }
            $parcelPoint->setData('place_id', $place['zip']);
            $parcelPoint->setData('name', $place['name']);
            $parcelPoint->setData('country', $place['country']);
            $parcelPoint->setData('group', $place['group_name']);
            $parcelPoint->save();
        }
    }
}

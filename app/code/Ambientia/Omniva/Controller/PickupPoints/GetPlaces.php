<?php
/**
 * Copyright © 2017 Ambientia. All rights reserved.
 */

namespace Ambientia\Omniva\Controller\PickupPoints;

class GetPlaces extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $helper;
    protected $service;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Ambientia\Omniva\Helper\Data $helper,
        \Ambientia\Omniva\Model\Service $service
    )
    {
        parent::__construct($context);

        $this->resultJsonFactory = $resultJsonFactory;
        $this->helper = $helper;
        $this->service = $service;

    }

    /**
     * return all pickup places
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        try {
            $packMachines = $this->helper->getPackMachines();

            $result = $this->resultJsonFactory->create();
            $result->setData($packMachines, false, JSON_UNESCAPED_UNICODE);
            return $result;

        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }
}
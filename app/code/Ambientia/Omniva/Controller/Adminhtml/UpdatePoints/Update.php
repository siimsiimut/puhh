<?php
/**
 * Copyright © 2017 Ambientia. All rights reserved.
 */

namespace Ambientia\Omniva\Controller\Adminhtml\UpdatePoints;
use Magento\Framework\Controller\ResultFactory;
use Ambientia\Omniva\Model\Service;

class Update extends \Magento\Backend\App\Action
{
    protected $_helper;

    protected $_omnivaPlaces;

    protected $service;

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magento_AdminNotification::show_list');
    }

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Ambientia\Omniva\Helper\Data $helper,
        \Ambientia\Omniva\Model\OmnivaEstoniaFactory $omnivaEstoniaFactory,
        \Ambientia\Omniva\Model\Service $service
    ){
        parent::__construct($context);

        $this->_helper = $helper;
        $this->_omnivaPlaces = $omnivaEstoniaFactory;
        $this->service = $service;
    }


    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $places = $this->service->getPackMachines();

        foreach ($places as $place) {
            $parcelPoint = $this->_omnivaPlaces->create();
            $oldEntry = $parcelPoint->getCollection()->addFieldToFilter('place_id', $place['zip'])->addFieldToSelect('id')->getFirstItem();
            if ($oldEntry->getId()) {
                $parcelPoint->setData('id', $oldEntry->getId());
            }
            $parcelPoint->setData('place_id', $place['zip']);
            $parcelPoint->setData('name', $place['name']);
            $parcelPoint->setData('country', $place['country']);
            $parcelPoint->setData('group', $place['group_name']);
            $parcelPoint->save();
        }
        $this->messageManager->addSuccessMessage('Pickup points updated');
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}
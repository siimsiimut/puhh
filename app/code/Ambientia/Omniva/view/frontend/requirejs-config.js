var config = {
    map: {
        '*': {
            "populateOmnivaDropdowns" : "Ambientia_Omniva/js/populateOmnivaDropdowns",
            "Magento_Checkout/js/model/shipping-rate-processor/new-address" : "Ambientia_Omniva/js/new-address",
        }
    }
};
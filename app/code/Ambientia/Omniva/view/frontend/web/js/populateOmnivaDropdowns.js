require(['jquery', 'mage/url', 'Magento_Checkout/js/model/quote', 'Magento_Checkout/js/model/shipping-rate-processor/new-address', 'Magento_Checkout/js/model/shipping-service', 'mage/translate', 'domReady'],
    function($, urlBuilder, quote, newAddress, shippingService){


    $(document).on('click', '#co-shipping-method-form tr', function () {
        var selectedMethod = $('#co-shipping-method-form input[type=radio]:checked').val();
        if (selectedMethod) {
            if (selectedMethod == 'post24ee_null') {
                if (this.id == 'omniva-added-tr') {
                    return false;
                }
                if ($('#omniva-added-tr').length > 0) {
                    $('#omniva-added-tr').html('').fadeOut().remove();
                }
                $(this).after('<tr id="omniva-added-tr">' +
                    '<td colspan="5">Vali sobiv pakiautomaat: <select name="omniva_places"></select></td>' +
                    '</tr>');
                populatePlaces();
            }
            else {
                //hide dropdown
                if ($('#omniva-added-tr').length > 0) {
                    $('#omniva-added-tr').html('').fadeOut().remove();
                }
            }
        }
    });
    $(document).on('change', 'select[name="omniva_places"]', function () {
        addPickupPointToQuote(this.value);
    });

    function populatePlaces() {
        shippingService.isLoading(true);
        var urlbase = urlBuilder.build('/omniva/PickupPoints/');
        var lastOptgroup = '';
        $.ajax({
            url: urlbase + 'GetPlaces',
            dataType: 'json',
            success: function (data) {
                $('select[name="omniva_places"]').append($('<option>').text('-- Vali pakiautomaat -- ').attr('value', ''));
                $.each(data, function(i, value) {
                    if (lastOptgroup != value.group) {
                        lastOptgroup = value.group;
                        $('select[name="omniva_places"]').append($('<optgroup>').attr('label', value.group));
                    }
                    $('select[name="omniva_places"]').append($('<option>').text(value.name).attr('value', value.place_id));
                });
                shippingService.isLoading(false);
            },
            error: function () {
                shippingService.isLoading(false);
            }
        });
        //shippingService.isLoading(false);
    }

    function addPickupPointToQuote(placeId) {
        var urlbase = urlBuilder.build('/omniva/PickupPoints/');
        $.ajax({
            url: urlbase + 'AddPickupToQuote/place/' + placeId,
            dataType: 'json',
            success: function (data) {
                quote.shippingMethod().method_code = data;
                var address = quote.shippingAddress();
                address.cache_disabled = true;
                newAddress.getRates(address);
            },
            error: function () {
            }
        });
    }
});
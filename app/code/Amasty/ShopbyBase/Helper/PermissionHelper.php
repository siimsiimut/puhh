<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_ShopbyBase
 */


namespace Amasty\ShopbyBase\Helper;

use Amasty\Shopby\Model\Layer\Filter\Category;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Module\Manager;
use Magento\Store\Model\ScopeInterface;

class PermissionHelper extends AbstractHelper
{
    const CUSTOMER_GROUPS = 'catalog/magento_catalogpermissions/grant_catalog_category_view_groups';

    const FOR_SPECIFIED_CUSTOMER_GROUP = 2;

    const PERMISSIONS_ENABLED = 'catalog/magento_catalogpermissions/enabled';

    const CATALOG_PERMISSIONS = 'catalog/magento_catalogpermissions/grant_catalog_category_view';

    /**
     * @var \Magento\Catalog\Model\CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Amasty\ShopbyBase\Model\Di\Wrapper
     */
    private $permissionModel;

    /**
     * @var Manager
     */
    private $moduleManager;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        Context $context,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Customer\Model\Session $customerSession,
        \Amasty\ShopbyBase\Model\Di\Wrapper $permissionModel,
        Manager $moduleManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->categoryRepository = $categoryRepository;
        $this->customerSession = $customerSession;
        $this->permissionModel = $permissionModel;
        $this->moduleManager = $moduleManager;
        $this->storeManager = $storeManager;
    }

    /**
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function checkPermissions()
    {
        $isAllowed = true;
        $isAllowGlobalPermissions = true;
        if ($this->moduleManager->isEnabled('Magento_CatalogPermissions')
            && $this->scopeConfig->isSetFlag(self::PERMISSIONS_ENABLED, ScopeInterface::SCOPE_STORE)
        ) {
            $store = $this->storeManager->getStore();
            $category = $this->categoryRepository->get($store->getRootCategoryId(), $store->getId());

            $permissions = $this->permissionModel->getIndexForCategory(
                $category->getId(),
                $this->customerSession->getCustomerGroupId(),
                $store->getWebsiteId()
            );

            $permissions = $permissions ? array_shift($permissions) : false;
            $isAllowed = $permissions
                && isset($permissions['grant_catalog_category_view'])
                && $permissions['grant_catalog_category_view'] !== Category::DENY_PERMISSION;
            $isAllowGlobalPermissions = $permissions ? false : $this->isAllowedPermissions();
        }

        return $isAllowed || $isAllowGlobalPermissions;
    }

    /**
     * @return bool|mixed
     */
    private function isAllowedPermissions()
    {
        $allowCategories = $this->scopeConfig->getValue(self::CATALOG_PERMISSIONS, ScopeInterface::SCOPE_STORE);
        if ($allowCategories == self::FOR_SPECIFIED_CUSTOMER_GROUP) {
            $customerGroupId = $this->customerSession->isLoggedIn()
                ? $this->customerSession->getCustomer()->getGroupId()
                : 0;
            $allowedCustomerGroups = $this->scopeConfig->getValue(
                self::CUSTOMER_GROUPS,
                ScopeInterface::SCOPE_STORE
            );
            $allowCategories = in_array($customerGroupId, explode(',', $allowedCustomerGroups));
        }

        return $allowCategories;
    }
}

<?php

namespace Balticode\Venipak\Model\ResourceModel\DeliveryDetails;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Balticode\Venipak\Api\Data\DeliveryDetailsInterface;

/**
 * Class Collection
 *
 * @package Balticode\Venipak\Model\ResourceModel\DeliveryDetails
 */
class Collection extends AbstractCollection
{
    /**
     * @codingStandardsIgnoreStart
     * @var string
     */
    protected $_idFieldName = DeliveryDetailsInterface::ENTITY_ID;

    /**
     * Define resource model
     *
     * @return void
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    protected function _construct()
    {
        $this->_init('Balticode\Venipak\Model\DeliveryDetails', 'Balticode\Venipak\Model\ResourceModel\DeliveryDetails');
        // @codingStandardsIgnoreEnd
    }
}

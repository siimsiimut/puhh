<?php

namespace Balticode\Venipak\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Balticode\Venipak\Api\Data\CarrierDataInterface;

/**
 * Class CarrierData
 *
 * @package Balticode\Venipak\Model\ResourceModel
 */
class CarrierData extends AbstractDb
{
    /**
     * Define resource model
     *
     * @return void
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        $this->_init('venipak_courier_data', CarrierDataInterface::ENTITY_ID);
        // @codingStandardsIgnoreEnd
    }
}

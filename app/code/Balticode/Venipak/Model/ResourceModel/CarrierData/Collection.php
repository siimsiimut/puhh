<?php

namespace Balticode\Venipak\Model\ResourceModel\CarrierData;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Balticode\Venipak\Api\Data\CarrierDataInterface;

/**
 * Class Collection
 *
 * @package Balticode\Venipak\Model\ResourceModel\CarrierData
 */
class Collection extends AbstractCollection
{
    /**
     * @codingStandardsIgnoreStart
     * @var string
     */
    protected $_idFieldName = CarrierDataInterface::ENTITY_ID;

    /**
     * Define resource model
     *
     * @return void
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    protected function _construct()
    {
        $this->_init('Balticode\Venipak\Model\CarrierData', 'Balticode\Venipak\Model\ResourceModel\CarrierData');
        // @codingStandardsIgnoreEnd
    }
}

<?php

namespace Balticode\Venipak\Model;

use Magento\Framework\Model\AbstractModel;
use Balticode\Venipak\Api\Data\DeliveryDetailsInterface;
use Balticode\Venipak\Model\ResourceModel\DeliveryDetails as DeliveryDetailsResource;
use Balticode\Venipak\Helper\StoreConfig;
use Balticode\Venipak\Helper\Order as OrderHelper;
use Balticode\Venipak\Model\PickupPoints\Provider as PickupPointsProvider;

/**
 * Class DeliveryDetails
 *
 * @package Balticode\Venipak\Model
 */
class DeliveryDetails extends AbstractModel implements DeliveryDetailsInterface
{

    /** @var  array  */
    private $deliveryDetails;

    protected $storeConfig;

    protected $orderHelper;

    protected $pickUpPointsProvider;

    public function __construct (
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        StoreConfig $storeConfig,
        OrderHelper $orderHelper,
        PickupPointsProvider $pickUpPointsProvider,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->storeConfig = $storeConfig;
        $this->orderHelper = $orderHelper;
        $this->pickUpPointsProvider = $pickUpPointsProvider;

        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Init resource model
     *
     * @return void
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        $this->_init(DeliveryDetailsResource::class);
        // @codingStandardsIgnoreEnd
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * Get quote_id
     * @return string
     */
    public function getQuoteId()
    {
        return $this->getData(self::QUOTE_ID);
    }

    /**
     * Set quote_id
     * @param string $quoteId
     * @return \Balticode\Venipak\Api\Data\DeliveryDetailsInterface
     */
    public function setQuoteId($quoteId)
    {
        return $this->setData(self::QUOTE_ID, $quoteId);
    }

    public function getLabelSize()
    {
        return $this->getData(self::LABEL_SIZE);
    }

    public function setLabelSize($value)
    {
        return $this->setData(self::LABEL_SIZE, $value);
    }

    public function getPackCount()
    {
        $packageCount =  $this->getData(self::PACK_COUNT);

        if (!$packageCount) {
            $packageCount = $this->orderHelper->countPackagesByQuote($this->getQuoteId());
        }

        return $packageCount;
    }

    public function setPackCount($value)
    {
        return $this->setData(self::PACK_COUNT, $value);
    }

    public function getWarehouseId()
    {
        $id = $this->getData(self::WAREHOUSE_ID);
        if (!$id) {
            $id = $this->storeConfig->getDefaultWarehouseId();
        }

        return $id;
    }

    public function setWarehouseId($value)
    {
        return $this->setData(self::WAREHOUSE_ID, $value);
    }

    public function getWarehouseNumber()
    {
        return $this->getData(self::CLIENT_WAREHOUSE);
    }

    public function setWarehouseNumber($value)
    {
        return $this->setData(self::CLIENT_WAREHOUSE, $value);
    }

    public function getReturnDoc()
    {
        return $this->getData(self::RETURN_DOC);
    }

    public function setReturnDoc($value)
    {
        return $this->setData(self::RETURN_DOC, $value);
    }

    public function getManifestNumber()
    {
        return $this->getData(self::MANIFEST_NO);
    }

    public function setManifestNumber($value)
    {
        return $this->setData(self::MANIFEST_NO, $value);
    }

    public function getPackNumber()
    {
        return $this->getData(self::PACK_NO);
    }

    public function setPackNumber($value)
    {
        return $this->setData(self::PACK_NO, $value);
    }

    public function getDeliveryTime()
    {
        return $this->getData(self::TIME_STAMP);
    }

    public function setDeliveryTime($value)
    {
        return $this->setData(self::TIME_STAMP, $value);
    }

    public function getOfficeNumber()
    {
        return $this->getData(self::OFFICE_NO);
    }

    public function setOfficeNumber($value)
    {
        return $this->setData(self::OFFICE_NO, $value);
    }

    public function getDoorCode()
    {
        return $this->getData(self::DOOR_NO);
    }

    public function setDoorCode($value)
    {
        return $this->setData(self::DOOR_NO, $value);
    }

    public function getNeedCall()
    {
        return $this->getData(self::DELIVERY_CALL);
    }

    public function setNeedCall($value)
    {
        return $this->setData(self::DELIVERY_CALL, $value);
    }

    public function getSent()
    {
        return $this->getData(self::SENT);
    }

    public function setSent($value)
    {
        return $this->setData(self::SENT, $value);
    }

    /**
     * @return string
     */
    public function getPickupPointId()
    {
        return $this->getData(self::PICKUP_POINT_ID);
    }

    /**
     * @param string $value
     * @return mixed
     */
    public function setPickupPointId($value)
    {
        return $this->setData(self::PICKUP_POINT_ID, $value);
    }

    /**
     * @return string
     */
    public function getPickupPointData()
    {
        return $this->getData(self::PICKUP_POINT_DATA);
    }

    /**
     * @param string $value
     * @return mixed
     */
    public function setPickupPointData($value)
    {
        return $this->setData(self::PICKUP_POINT_DATA, $value);
    }

    /**
     * @inheritdoc
     */
    public function getExtensionAttributes()
    {
        return $this->deliveryDetails;
    }

    public function setExtensionAttributes(
        \Balticode\Venipak\Api\Data\DeliveryDetailsExtensionInterface $deliveryDetails
    ) {
        return $this->deliveryDetails = $deliveryDetails;
    }

    public function updatePickupPointData()
    {
        $pickupPointId = $this->getPickupPointId();
        if ($pickupPointId) {
            $pickUpData = $this->pickUpPointsProvider->getByPointId($pickupPointId)->getData();
            $this->setPickupPointData(json_encode($pickUpData));
        }

        return $this;
    }
}

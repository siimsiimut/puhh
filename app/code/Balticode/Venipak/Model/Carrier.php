<?php

namespace Balticode\Venipak\Model;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;
use Balticode\Venipak\Helper\StoreConfig;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;

/**
 * Class Carrier
 *
 * @package Balticode\Venipak\Model
 */
class Carrier extends AbstractCarrier implements CarrierInterface
{
    /**
     * @var string
     * @codingStandardsIgnoreStart
     */
    protected $_code = 'venipak';

    /**
     * @var \Magento\Shipping\Model\Rate\ResultFactory
     */
    protected $rateResultFactory;

    /**
     * @var array
     */
    protected $methods;

    /**
     * @var array
     */
    protected $allowedMethods;

    /**
     * @var array
     */
    protected $allMethods;

    /**
     * @var \Balticode\Venipak\Helper\StoreConfig
     */
    protected $storeConfig;

    /**
     * Carrier constructor.
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface         $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface                                   $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory                 $rateResultFactory
     * @param StoreConfig                                                $storeConfig
     * @param array                                                      $methods
     * @param array                                                      $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        StoreConfig $storeConfig,
        array $methods = [],
        array $data = []
    ) {
        $this->rateResultFactory = $rateResultFactory;
        $this->methods = $methods;
        $this->storeConfig = $storeConfig;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * Flag for check carriers for activity
     *
     * @var string
     */
    protected $_activeFlag = 'active';
    // @codingStandardsIgnoreEnd

    /**
     * Get all shipping methods
     *
     * @return array
     */
    public function getAllMethods()
    {
        if (empty($this->allMethods)) {
            $result = [];
            foreach ($this->methods as $methodCode => $method) {
                $result[$methodCode] = $method->getMethodTitle();
            }
            $this->allMethods = $result;
        }

        return $this->allMethods;
    }

    /**
     * Get allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        if (empty($this->allowedMethods)) {
            $allowed = explode(',', $this->storeConfig->getAllowedMethods());

            $result = [];
            foreach ($allowed as $method) {
                $result[$method] = $this->methods[$method]->getMethodTitle();
            }
            $this->allowedMethods = $result;
        }

        return $this->allowedMethods;
    }

    public function getHasAddress()
    {
        return true;
    }

    /**
     * Collect and get rates
     *
     * @param RateRequest $request
     * @return \Magento\Quote\Model\Quote\Address\RateResult\Error|bool|Result
     */
    public function collectRates(RateRequest $request)
    {
        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->rateResultFactory->create();
        foreach ($this->getAllowedMethods() as $methodName => $title) {
            // If this method not available for current quote
            if (!$this->methods[$methodName]->isAvailable($request)) {
                continue;
            }

            /** @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $result */
            $method = $this->methods[$methodName]->getRateMethod($request);

            if ($method) {
                $method->setCarrier($this->getCarrierCode());
                $method->setCarrierTitle('Venipak');

                $result->append($method);
            }
        }

        return $result;
    }

    /**
     * Get Carrier method object
     *
     * @param string $methodCode
     * @return mixed|\Balticode\Venipak\Model\Carrier\General
     */
    public function getMethod($methodCode)
    {
        return $this->methods[$methodCode] ?? null;
    }

    /**
     * @param $order
     */
    public function setShippingDescription($order)
    {
        $shippingMethod = $order->getShippingMethod();
        $shippingMethod = str_replace($this->getCarrierCode().'_', '', $shippingMethod);
        $method = $this->getMethod($shippingMethod);
        if ($method) {
            $shippingDescription = $method->getMethodDescription($order);

            $order->setShippingDescription($shippingDescription);
        }

        return $this;
    }
}

<?php

namespace Balticode\Venipak\Framework;

/**
 * Class DataObject
 *
 * @package Balticode\Venipak\Framework
 * @method getMethod()
 * @method getConverter(string)
 */
class DataObject extends \Magento\Framework\DataObject
{
    protected $self = false;

    /**
     * Set/Get attribute wrapper
     *
     * @param string $method
     * @param array  $args
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function __call($method, $args)
    {
        $result = parent::__call($method, $args);

        if (substr($method, 0, 3) == 'get') {
            $args = isset($args[0]) ? $args[0] : null;
            if ($result instanceof \Balticode\Venipak\Helper\StoreConfig) {
                return call_user_func([$result, $method], $args);
            }

            if ($result instanceof \Balticode\Venipak\Model\Source\Generic) {
                return call_user_func([$result, 'execute'], $args);
            }
        }

        return $result;
    }

    /**
     * Object data getter
     *
     * If $key is not defined will return all the data as an array.
     * Otherwise it will return value of the element specified by $key.
     * It is possible to use keys like a/b/c for access nested array data
     *
     * If $index is specified it will assume that attribute data is an array
     * and retrieve corresponding member. If data is the string - it will be explode
     * by new line character and converted to array.
     *
     * @param string     $key
     * @param string|int $index
     * @return mixed
     */
    public function getData($key = '', $index = null)
    {
        if ('' === $key) {
            return $this->_data;
        }

        /* process a/b/c key as ['a']['b']['c'] */
        if (strpos($key, '/')) {
            $data = $this->getDataByPath($key);
        } else {
            $data = $this->_getData($key);
        }

        if ($index !== null) {
            if ($data === (array)$data) {
                $data = isset($data[$index]) ? $data[$index] : null;
            } elseif (is_string($data)) {
                $data = explode(PHP_EOL, $data);
                $data = isset($data[$index]) ? $data[$index] : null;
            } elseif ($data instanceof DataObject) {
                return call_user_func([$data, $this->_getter($key)], $index);
            } elseif ($data instanceof \Magento\Framework\DataObject) {
                $data = $data->getData($index);
            } elseif (is_scalar($index)) {
                $data = null;
            }
        }
        return $data;
    }

    /**
     * Get object data by path
     *
     * Method consider the path as chain of keys: a/b/c => ['a']['b']['c']
     *
     * @param string $path
     * @return mixed
     */
    public function getDataByPath($path)
    {
        $keys = explode('/', $path);

        $data = $this->_data;
        foreach ($keys as $key) {
            if ((array)$data === $data && isset($data[$key])) {
                $data = $data[$key];
            } elseif ($data instanceof \Magento\Framework\DataObject) {
                $data = $data->getDataByKey($key);
            } else {
                return null;
            }

            if ($data instanceof \Balticode\Venipak\Helper\StoreConfig) {
                $data = call_user_func([$data, $this->_getter($key)]);
            }
        }

        return $data;
    }

    /**
     * Set data by path
     *
     * @param $path
     * @param $value
     * @return $this
     */
    public function setByPath($path, $value)
    {
        $result = array();
        $temp = &$result;

        $steps = explode('/', $path);
        $firstKey = reset($steps);

        $target[$firstKey] = $this->_getData($firstKey) ?? [];

        foreach ($steps as $key) {
            $temp =& $temp[$key];
        }

        $temp = $value;

        $result = $this->array_merge_recursive_distinct($target, $result);
        $this->setData($result);

        return $this;
    }

    /**
     * Unset data from the object by path
     *
     * @param $path
     * @return $this
     */
    public function unsetByPath($path)
    {
        $temp = &$this->_data;

        $steps = explode('/', $path);
        $i = 0;

        $deep = count($steps);
        foreach ($steps as $key) {
            // Last one
            if ($i == $deep - 1) {
                unset($temp[$key]);
                break;
            }
            $temp =& $temp[$key];
            $i++;
        }

        return $this;
    }

    /**
     * Array merge recursive without duplicate
     *
     * @param array $array1
     * @param array $array2
     * @return array
     */
    protected function array_merge_recursive_distinct(array &$array1, array &$array2)
    {
        $merged = $array1;
        foreach ($array2 as $key => &$value) {
            if (is_array($value) && isset($merged[$key]) && is_array($merged[$key])) {
                $merged[$key] = $this->array_merge_recursive_distinct($merged[$key], $value);
            } else {
                $merged[$key] = $value;
            }
        }

        return $merged;
    }

    /**
     * Convert string of snake style to camel style
     *
     * @param        $input
     * @param string $separator
     * @return mixed
     */
    public function camel($input, $separator = '_')
    {
        return str_replace($separator, '', ucwords($input, $separator));
    }

    public function execute()
    {
        $method = $this->_data['method'] ?? null;
        $result = null;

        if ($method
            && method_exists($this, $method)
            && !$this->self
        ) {
            $result = call_user_func([$this, $method]);
        } elseif ($method) {
            $getMethod = $this->_getter($method);
            $result = call_user_func([$this, $getMethod]);
        }

        if ($result && $this->getDependence()) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $conditionCombine = $objectManager->create('\Magento\Rule\Model\Condition\Combine');

            $source = $this->getDependence('source');
            if (is_object($source) && $source instanceof DataObject) {
                $source = $source->execute();
            } elseif (is_array($source)
                && array_key_exists('target', $source)
                && array_key_exists('method', $source)
            ) {
                $source = call_user_func([$source['target'], $this->_getter($source['method'])]);
            }

            $condition = $this->getDependence('condition');

            $conditionCombine->setOperator($condition['operator'] ?? '==');
            $conditionCombine->setValue($source);

            foreach ($result as $line => $row) {
                if (!$conditionCombine->validateAttribute($row[$condition['field']])) {
                    unset($result[$line]);
                }
            }
            $result = array_values($result);
        }

        if ($this->getConverter()) {
            $target = $this->getConverter('target');
            $target->setMethod($this->getConverter('method'));
            $result = $target->convert($result);
        }

        return $result;
    }

    /**
     * Convert snake to camel case and add get prefix
     *
     * @param string $field
     * @return string
     */
    private function _getter($field)
    {
        return 'get'.$this->camel($field);
    }
}

<?php

namespace Balticode\Venipak\Http\Builder;

use Balticode\Venipak\Http\BuilderInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Balticode\Venipak\Model\Carrier;

/**
 * Class ImportSendBuilder
 *
 * @package Balticode\Venipak\Http\Builder
 */
class ImportSendBuilder implements BuilderInterface
{
    protected $store_config;

    protected $carrier;

    protected $builder;

    public function __construct (
        \ArrayAccess $store_config,
        Carrier $carrier,
        BuilderInterface $builder
    ) {
        $this->store_config = $store_config;
        $this->carrier = $carrier;
        $this->builder = $builder;
    }

    public function build (array $buildSubject)
    {
        $result = [
            'user' => $this->store_config->getUsername(),
            'pass' => $this->store_config->getPassword(),
            'xml_text' => $this->builder->build($buildSubject),
            'sandbox' => $this->store_config->getSandbox(),
        ];

        return $result;
    }

    /**
     * Data about what we collect to send XML
     *
     * @return DataObject
     */
    public function getBuilderPattern()
    {
        return $this->builder->getTransferData();
    }
}

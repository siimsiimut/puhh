<?php

namespace Balticode\Venipak\Http;

/**
 * Interface BuilderInterface
 *
 * @package Balticode\Venipak\Http
 */
interface BuilderInterface
{
    /**
     * Builds ENV request
     *
     * @param array $buildSubject
     * @return array
     */
    public function build(array $buildSubject);
}

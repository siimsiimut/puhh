<?php

namespace Balticode\Venipak\Http\Transfer;

use Balticode\Venipak\Http\TransferInterface;

/**
 * Class Transfer
 *
 * @package Balticode\Venipak\Http
 */
class Transfer implements TransferInterface
{
    /**
     * @var array
     */
    private $clientConfig;

    /**
     * @var array
     */
    private $headers;

    /**
     * @var string
     */
    private $method;

    /**
     * @var array|string
     */
    private $body;

    /**
     * @var string
     */
    private $uri;

    /**
     * @var bool
     */
    private $encode;

    /**
     * Transfer constructor.
     *
     * @param array $clientConfig
     * @param array $headers
     * @param array|string $body
     * @param string $method
     * @param string $uri
     * @param string $encode
     */
    public function __construct(
        array $clientConfig,
        array $headers,
        $body,
        $method,
        $uri,
        $encode
    ) {
        $this->clientConfig = $clientConfig;
        $this->headers = $headers;
        $this->body = $body;
        $this->method = $method;
        $this->uri = $uri;
        $this->encode = $encode;
    }

    /**
     * Returns gateway client configuration
     *
     * @return array
     */
    public function getClientConfig()
    {
        return $this->clientConfig;
    }

    /**
     * Returns method used to place request
     *
     * @return string|int
     */
    public function getMethod()
    {
        return (string)$this->method;
    }

    /**
     * Returns headers
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Returns request body
     *
     * @return array|string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Returns URI
     *
     * @return string
     */
    public function getUri()
    {
        return (string)$this->uri;
    }

    /**
     * @return boolean
     */
    public function shouldEncode()
    {
        return $this->encode;
    }
}

<?php

namespace Balticode\Venipak\Command;

use Balticode\Venipak\Http\ClientInterface;
use Balticode\Venipak\Http\TransferFactoryInterface;
use Balticode\Venipak\Http\BuilderInterface;

/**
 * Class PrintManifestCommand
 *
 * @package Balticode\Venipak\Command
 */
class PrintManifestCommand implements CommandInterface
{
    /**
     * @var TransferFactoryInterface
     */
    private $transferFactory;

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var BuilderInterface
     */
    private $requestBuilder;

    /**
     * Command constructor.
     *
     * @param BuilderInterface $requestBuilder
     * @param TransferFactoryInterface $transferFactory
     * @param ClientInterface $client
     */
    public function __construct(
        BuilderInterface $requestBuilder,
        TransferFactoryInterface $transferFactory,
        ClientInterface $client
    ) {

        $this->transferFactory = $transferFactory;
        $this->client = $client;
        $this->requestBuilder = $requestBuilder;
    }

    /**
     * @param array $commandSubject
     * @return array
     */
    public function execute(array $commandSubject = [])
    {
        $transferObj = $this->transferFactory->create(
            $this->requestBuilder->build($commandSubject)
        );

        $result = $this->client->placeRequest($transferObj);

        if (strpos(strtok($result, "\n"),'%PDF-') === FALSE){
            // @TODO: throw exeption about not PDF file format
        }

        return $result;
    }
}

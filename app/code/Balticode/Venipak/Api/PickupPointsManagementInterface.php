<?php

namespace Balticode\Venipak\Api;

/**
 * Interface PickupPointsManagementInterface
 *
 * @package Balticode\Venipak\Api
 */
interface PickupPointsManagementInterface
{
    /**
     * Find terminals for the customer
     *
     * @param string $cartId
     * @param string $countryId
     * @return \Balticode\Venipak\Api\Data\PickupPointsInterface[]
     */
    public function fetch($cartId, $countryId);

    /**
     * Find terminals for the customer
     *
     * @param string $cartId
     * @param string $countryId
     * @return \Balticode\Venipak\Api\Data\PickupPointsInterface[]
     */
    public function fetchCustomer($cartId, $countryId);

    /**
     * Find terminals for order
     *
     * @param string $orderId
     * @return \Balticode\Venipak\Api\Data\PickupPointsInterface[]
     */
    public function fetchOrder($orderId);
}

<?php

namespace Balticode\Venipak\Api;

use Balticode\Venipak\Api\Data\DeliveryDetailsInterface;

/**
 * Interface DeliveryDetailsProviderInterface
 *
 * @package Balticode\Venipak\Api
 */
interface DeliveryDetailsProviderInterface
{
    /**
     * Save Delivery Details
     *
     * @param string                   $cartId
     * @param \Balticode\Venipak\Api\Data\DeliveryDetailsInterface $deliveryDetails
     * @return \Balticode\Venipak\Api\Data\DeliveryDetailsInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save($cartId,
        DeliveryDetailsInterface $deliveryDetails
    );

    /**
     * Save Delivery Details
     *
     * @param string                   $cartId
     * @param \Balticode\Venipak\Api\Data\DeliveryDetailsInterface $deliveryDetails
     * @return \Balticode\Venipak\Api\Data\DeliveryDetailsInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function saveCustomer($cartId,
         DeliveryDetailsInterface $deliveryDetails
    );

    /**
     * Retrieve Delivery details Data by quote id
     *
     * @param string $quoteId
     * @return \Balticode\Venipak\Api\Data\DeliveryDetailsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByQuoteId($quoteId);

    /**
     * Delete quote additional data
     *
     * @param \Balticode\Venipak\Api\Data\DeliveryDetailsInterface $deliveryDetails
     * @return void
     */
    public function delete(DeliveryDetailsInterface $deliveryDetails);
}

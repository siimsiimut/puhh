/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/url-builder',
        'Magento_Checkout/js/model/quote',
        'mageUtils'
    ],
    function(
        customer,
        urlBuilder,
        quote,
        utils
    ) {
        "use strict";
        return {
            getUrlForSetDeliveryDetails: function() {
                var params = {cartId: quote.getQuoteId()};
                var urls = {
                    'guest': '/guest-carts/:cartId/delivery-details',
                    'customer': '/carts/mine/delivery-details'
                };
                return this.getUrl(urls, params);
            },

            getUrlForSetPickupPoint: function() {
                var params = {cartId: quote.getQuoteId()};
                var urls = {
                    'guest': '/guest-carts/:cartId/selected-pickup',
                    'customer': '/carts/mine/selected-pickup'
                };
                return this.getUrl(urls, params);
            },

            getUrlForPickupPointsList: function() {
                var params = {
                    cartId: quote.getQuoteId(),
                    countryId: (quote.shippingAddress() ?
                        quote.shippingAddress().countryId :
                        window.checkoutConfig.defaultCountryId
                    )
                };
                var urls = {
                    'guest': '/module/get-pickup-points-list/:cartId/:countryId',
                    'customer': '/module/get-pickup-points-list/:countryId'
                };
                return this.getUrl(urls, params);
            },

            /** Get url for service */
            getUrl: function(urls, urlParams) {
                var url;

                if (utils.isEmpty(urls)) {
                    return 'Provided service call does not exist.';
                }

                if (!utils.isEmpty(urls['default'])) {
                    url = urls['default'];
                } else {
                    url = urls[this.getCheckoutMethod()];
                }
                return urlBuilder.createUrl(url, urlParams);
            },

            getCheckoutMethod: function() {
                return customer.isLoggedIn() ? 'customer' : 'guest';
            }
        };
    }
);

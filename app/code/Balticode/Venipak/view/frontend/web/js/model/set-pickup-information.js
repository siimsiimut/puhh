/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define,alert*/
define(
    [
        'Balticode_Venipak/js/model/resource-url-manager',
        'mage/storage'
    ],
    function (
        resourceUrlManager,
        storage
    ) {
        'use strict';
        return {
            saveShippingInformation: function() {
                var payload = {
                    delivery_details: {
                        pickup_point_id: jQuery('#venipak-pickup-block-wrapper :input[name="pickup-points"]').val()
                    }
                };

                return storage.post(
                    resourceUrlManager.getUrlForSetPickupPoint(),
                    JSON.stringify(payload)
                ).done(
                ).fail(
                );
            }
        }
    }
);

/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-save-processor'
], function ($, wrapper, quote, shippingSaveProcessor) {
    'use strict';

    return function (setShippingInformationAction) {
        return wrapper.wrap(setShippingInformationAction, function (originalAction) {
            var shippingMethod = quote.shippingMethod();
            if (shippingMethod != null) {
                shippingSaveProcessor.saveShippingInformation(
                    shippingMethod.carrier_code + '-' + shippingMethod.method_code
                );
            }
            return originalAction();
        });
    };
});

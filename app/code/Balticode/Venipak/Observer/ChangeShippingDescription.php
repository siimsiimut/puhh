<?php

namespace Balticode\Venipak\Observer;

use Balticode\Venipak\Helper\Order;
use Magento\Framework\Event\Observer;
use Balticode\Venipak\Model\Carrier;

class ChangeShippingDescription implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var Order
     */
    protected $orderHelper;

    /**
     * @var Carrier
     */
    protected $carrier;

    /**
     * ChangeShippingDescription constructor.
     *
     * @param Order $orderHelper
     */
    public function __construct (
        Order $orderHelper,
        Carrier $carrier
    ) {
        $this->orderHelper = $orderHelper;
        $this->carrier = $carrier;
    }

    /**
     * Save/reset the order as transactional data.
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return $this
     *
     */
    public function execute (Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();

        if ($this->orderHelper->isMyShippingMethod($order)) {
            $this->carrier->setShippingDescription($order);
        }

        return $this;
    }
}

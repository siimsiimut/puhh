<?php

namespace Balticode\Venipak\Observer;

use Balticode\Venipak\Cron\UpdatePickupPoints as PickupPointsUpdater;
use Magento\Framework\Event\Observer;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class UpdatePickupPoints
 *
 * @package Balticode\Venipak\Observer
 */
class UpdatePickupPoints implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var PickupPointsUpdater
     */
    protected $pickupPoints;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * UpdatePickupPoints constructor.
     *
     * @param PickupPointsUpdater $pickupPoints
     */
    public function __construct (
        PickupPointsUpdater $pickupPoints,
        ManagerInterface $messageManager
    ) {
        $this->pickupPoints = $pickupPoints;
        $this->messageManager = $messageManager;
    }

    /**
     * @param Observer $observer
     */
    public function execute (Observer $observer)
    {
        try {
            $this->pickupPoints->execute();
        } catch (\Magento\Framework\Exception\LocalizedException $exception) {
            $message = sprintf('Error on Update Pickup Points: %s. Check API address', $exception->getMessage());
            $this->messageManager->addWarningMessage($message);
        }
    }
}

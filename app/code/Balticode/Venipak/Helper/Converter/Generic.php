<?php

namespace Balticode\Venipak\Helper\Converter;

class Generic extends \Balticode\Venipak\Framework\DataObject
{
    public function convert($data)
    {
        $method = $this->getMethod();
        if (method_exists($this, $method)) {
            $data = call_user_func([$this, $method], $data);
        }

        return $data;
    }
}

<?php

namespace Lumav\XmlFeed\Block\Adminhtml\XmlFeed\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Lumav\XmlFeed\Block\Adminhtml\XmlFeed\Edit\GenericButton;

/**
 * Class Generic
 */
class Back extends GenericButton implements ButtonProviderInterface
{

    public function getButtonData()
    {
        return [
            'label' => __('Back'),
            'on_click' => sprintf("location.href = '%s';", $this->getBackUrl()),
            'class' => 'back',
            'sort_order' => 10
        ];
    }

    public function getBackUrl()
    {
        return $this->getUrl('*/*/');
    }
}

<?php
declare(strict_types = 1);

namespace Lumav\XmlFeed\Setup;

use Lumav\XmlFeed\Api\Data\ScheduleInterface;
use Lumav\XmlFeed\Model\ResourceModel\Schedule;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Lumav\XmlFeed\Api\Data\XmlFeedInterface;
use Lumav\XmlFeed\Model\ResourceModel\XmlFeed;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    protected $serializer;

    /**
     * InstallSchema constructor.
     *
     * @param \Magento\Framework\Serialize\SerializerInterface $serializer
     */
    public function __construct(
        \Magento\Framework\Serialize\SerializerInterface $serializer
    )
    {
        $this->serializer = $serializer;
    }

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $emptySerializedArray = $this->serializer->serialize([]);

        $table = $setup->getConnection()->newTable(
            $setup->getTable('xml_feed_entity')
        )->addColumn(
            XmlFeedInterface::ENTITY_ID,
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Xml Feed ID'
        )->addColumn(
            XmlFeedInterface::NAME,
            Table::TYPE_TEXT,
            255,
            [],
            'Feed Name'
        )->addColumn(
            XmlFeedInterface::HASH,
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Feed Hash'
        )->addColumn(
            XmlFeedInterface::IP_ADDRESSES,
            Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => $emptySerializedArray],
            'Allowed Ip Addresses'
        )->addColumn(
            XmlFeedInterface::VAT_PERCENT,
            Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => ""],
            'Vat percent (informative)'
        )->addColumn(
            XmlFeedInterface::CUSTOMER_GROUP_ID,
            Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => 0],
            'Customer Group ID'
        )->addColumn(
            XmlFeedInterface::IGNORED_CATEGORY_IDS,
            Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => $emptySerializedArray],
            'Ignored Category IDs'
        )->addColumn(
            XmlFeedInterface::IGNORED_PRODUCT_IDS,
            Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => $emptySerializedArray],
            'Ignored Product IDs'
        )->addColumn(
            XmlFeedInterface::IS_ACTIVE,
            Table::TYPE_BOOLEAN,
            null,
            ['nullable' => false, 'default' => 1],
            'Is Active'
        )->addColumn(
            XmlFeedInterface::CREATED_AT,
            Table::TYPE_DATETIME,
            null,
            ['nullable' => false],
            'Created At'
        )->addColumn(
            XmlFeedInterface::UPDATED_AT,
            Table::TYPE_DATETIME,
            null,
            [],
            'Updated At'
        )->addColumn(
            XmlFeedInterface::FEED_TYPE,
            Table::TYPE_TEXT,
            255,
            [],
            'Map data to xml based on type'
        );

        $setup->getConnection()->createTable($table);

        $table = $setup->getConnection()->newTable(
            $setup->getTable(Schedule::TABLE)
        )->addColumn(
            ScheduleInterface::ID,
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'ID'
        )->addColumn(
            ScheduleInterface::CRON_TIME,
            Table::TYPE_INTEGER,
            null,
            [],
            'Cron Time Execution'
        )->addColumn(
            ScheduleInterface::CRON_DAY,
            Table::TYPE_INTEGER,
            null,
            [],
            'Cron Day Execution'
        )->addColumn(
            ScheduleInterface::FEED_ID,
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Feed Id'
        )->addColumn(
            ScheduleInterface::LAST_EXEC_TIME,
            Table::TYPE_DATETIME,
            null,
            [],
            'Last Schedule Execution Time'
        )->addForeignKey(
            $setup->getFkName(
                Schedule::TABLE,
                ScheduleInterface::FEED_ID,
                XmlFeed::TABLE_NAME,
                XmlFeed::ID
            ),
            ScheduleInterface::FEED_ID,
            $setup->getTable(XmlFeed::TABLE_NAME),
            XmlFeed::ID,
            Table::ACTION_CASCADE
        )->setComment(
            'Cron Schedule Execution'
        );
        $setup->getConnection()->createTable($table);

        $setup->endSetup();
    }
}

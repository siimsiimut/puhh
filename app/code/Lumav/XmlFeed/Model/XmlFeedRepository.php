<?php
declare(strict_types=1);

namespace Lumav\XmlFeed\Model;

use Lumav\XmlFeed\Api\Data\XmlFeedInterface;


use Lumav\XmlFeed\Model\XmlFeedFactory as ModelFactory;
use Lumav\XmlFeed\Model\ResourceModel\XmlFeed as ModelResource;

use Magento\Framework\Config\Dom\ValidationException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Class XmlFeedRepository
 * @package Lumav\XmlFeed\Model
 */
class XmlFeedRepository implements \Lumav\XmlFeed\Api\XmlFeedRepositoryInterface
{

    /**
     * @var ModelFactory
     */
    private $modelFactory;

    /**
     * @var ModelResource
     */
    private $modelResource;

    public function __construct(
        ModelFactory $modelFactory,
        ModelResource $modelResource,
        FilterBuilder $filterBuilder,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SearchResultsInterfaceFactory $searchResultsFactory,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->modelFactory = $modelFactory;
        $this->modelResource = $modelResource;
        $this->filterBuilder = $filterBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->_logger = $logger;
    }
    /**
     * @inheritDoc
     */
    public function save(XmlFeedInterface $model)
    {
        try {
            $this->modelResource->save( $model );
        } catch (ValidationException $e) {
            $this->_logger->error($e->getMessage()); 
            $this->_logger->error( $e->getTraceAsString() ); 
            throw new CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            $this->_logger->error($e->getMessage()); 
            $this->_logger->error( $e->getTraceAsString() ); 
            throw new CouldNotSaveException(__('Unable to save model %1', $model->getEntityId()));
        }

        return $model;
    }

    /**
     * @inheritDoc
     */
    public function getById(int $id)
    {
        $model = $this->modelFactory->create();
        $this->modelResource->load($model, $id);

        if (!$model->getEntityId()) {
            throw new NoSuchEntityException(__('Entity with specified ID "%1" not found.', $id));
        }

        return $model;
    }

    /**
     * @inheritDoc
     */
    public function getByHash(string $hash)
    {
     
        $model = $this->modelFactory->create();

        $filter = $this->filterBuilder->setField(XmlFeedInterface::HASH)->setConditionType('=')->setValue($hash)->create();
        $this->searchCriteriaBuilder->addFilters([$filter]);
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $items = $this->getList( $searchCriteria )->getItems();

        foreach($items as $item ){
           $model = $item;
            break;
        }

        if (!$model->getEntityId()) {
            throw new NoSuchEntityException(__('Entity with specified hash "%1" not found.', $hash));
        }

        return $model;
    }

    /**
     * @inheritDoc
     */
    public function delete(XmlFeedInterface $model)
    {
        try {
            $this->modelResource->delete($model);
        } catch (ValidationException $e) {
            throw new CouldNotDeleteException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__('Unable to remove entity with ID "%1"', $model->getEntityId()));
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById(int $id)
    {
        $model = $this->getById($id);
        $this->delete($model);

        return true;
    }

    /**
     * @inheritDoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->modelFactory->create()->getCollection();
        $searchResults = $this->searchResultsFactory->create(); 
        $searchResults->setSearchCriteria($searchCriteria);

        $this->applySearchCriteriaToCollection($searchCriteria, $collection);
       
        $searchResults->setTotalCount($collection->getSize()); 
        $searchResults->setItems( $collection->getItems() );
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function getNewModel()
    {
        $model = $this->modelFactory->create();
        return $model;
    }

    //Convert SearchCriteria back to normal filter,sort and paging
    private function applySearchCriteriaToCollection(SearchCriteriaInterface $searchCriteria, $collection){ 

        //Filters
        foreach ($searchCriteria->getFilterGroups() as $group) {
            foreach ($group->getFilters() as $filter) {
                    $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                    $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        //Sort order
        $sortOrders = $searchCriteria->getSortOrders(); 
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {
                $isAscending = $sortOrder->getDirection() == SortOrder::SORT_ASC;
                $collection->addOrder(
                    $sortOrder->getField(),
                    $isAscending ? 'ASC' : 'DESC'
                );
            }
        }
        //Paging
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
    }
}

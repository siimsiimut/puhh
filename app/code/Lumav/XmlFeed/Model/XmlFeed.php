<?php


namespace Lumav\XmlFeed\Model;


use Lumav\XmlFeed\Api\Data\XmlFeedInterface;
use Magento\Framework\Model\AbstractModel;

class XmlFeed extends AbstractModel implements XmlFeedInterface
{
    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    protected $serializer;

    /**
     * Collection constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ResourceModel\XmlFeed $resource
     * @param \Lumav\XmlFeed\Model\ResourceModel\XmlFeed\Collection $resourceCollection
     * @param \Magento\Framework\Serialize\SerializerInterface $serializer
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Lumav\XmlFeed\Model\ResourceModel\XmlFeed $resource,
        \Lumav\XmlFeed\Model\ResourceModel\XmlFeed\Collection $resourceCollection,
        \Magento\Framework\Serialize\SerializerInterface $serializer,
        array $data = []
    )
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->serializer = $serializer;
    }

    /**
     * @inheridoc
     */
    public function setEntityId($entity_id)
    {
        $this->setData(XmlFeedInterface::ENTITY_ID, $entity_id);

        return $this;
    }

    /**
     * @inheridoc
     */
    public function getEntityId()
    {
        return $this->getData(XmlFeedInterface::ENTITY_ID);
    }

    /**
     * @inheridoc
     */
    public function setName($name)
    {
        $this->setData(XmlFeedInterface::NAME, $name);

        return $this;
    }

    /**
     * @inheridoc
     */
    public function getName()
    {
        return $this->getData(XmlFeedInterface::NAME);
    }

    /**
     * @inheridoc
     */
    public function setHash($hash)
    {
        $this->setData(XmlFeedInterface::HASH, $hash);

        return $this;
    }

    /**
     * @inheridoc
     */
    public function getHash()
    {
        return $this->getData(XmlFeedInterface::HASH);
    }

    /**
     * @inheridoc
     */
    public function setIpAddresses($ipAddresses)
    {
        $data = $this->serializer->serialize($ipAddresses);
        $this->setData(XmlFeedInterface::IP_ADDRESSES, $data);

        return $this;
    }

    /**
     * @inheridoc
     */
    public function getIpAddresses()
    {
        if ($data = $this->getData(XmlFeedInterface::IP_ADDRESSES)) {
            $data = $this->serializer->unserialize($data);
        }
        return $data;
    }

    /**
     * @inheridoc
     */
    public function setCustomerGroupId($customerGroupId)
    {
        $this->setData(XmlFeedInterface::CUSTOMER_GROUP_ID, $customerGroupId);

        return $this;
    }

    /**
     * @inheridoc
     */
    public function getCustomerGroupId()
    {
        return $this->getData(XmlFeedInterface::CUSTOMER_GROUP_ID);
    }

    /**
     * @inheridoc
     */
    public function setVatPercent($vatPercent)
    {
        $this->setData(XmlFeedInterface::VAT_PERCENT, $vatPercent);

        return $this;
    }

    /**
     * @inheridoc
     */
    public function getVatPercent()
    {
        return $this->getData(XmlFeedInterface::VAT_PERCENT);
    }

    /**
     * @inheridoc
     */
    public function setIgnoredCategoryIds($ignoredCategoryIds)
    {
        $data = $this->serializer->serialize($ignoredCategoryIds);
        $this->setData(XmlFeedInterface::IGNORED_CATEGORY_IDS, $data);

        return $this;
    }

    /**
     * @inheridoc
     */
    public function getIgnoredCategoryIds()
    {
        if ($data = $this->getData(XmlFeedInterface::IGNORED_CATEGORY_IDS)) {
            $data = $this->serializer->unserialize($data);
        }
        return $data;
    }

    /**
     * @inheridoc
     */
    public function setIgnoredProductIds($ignoredProductIds)
    {
        $data = $this->serializer->serialize($ignoredProductIds);
        $this->setData(XmlFeedInterface::IGNORED_PRODUCT_IDS, $data);

        return $this;
    }

    /**
     * @inheridoc
     */
    public function getIgnoredProductIds()
    {
        if ($data = $this->getData(XmlFeedInterface::IGNORED_PRODUCT_IDS)) {
            $data = $this->serializer->unserialize($data);
        }
        return $data;
    }

    /**
     * @inheridoc
     */
    public function setIsActive($isActive)
    {
        $this->setData(XmlFeedInterface::IS_ACTIVE, $isActive);

        return $this;
    }

    /**
     * @inheridoc
     */
    public function getIsActive()
    {
        return $this->getData(XmlFeedInterface::IS_ACTIVE);
    }

    /**
     * @inheridoc
     */
    public function setCreatedAt($createdAt)
    {
        $this->setData(XmlFeedInterface::CREATED_AT, $createdAt);

        return $this;
    }

    /**
     * @inheridoc
     */
    public function getCreatedAt()
    {
        return $this->getData(XmlFeedInterface::CREATED_AT);
    }

    /**
     * @inheridoc
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->setData(XmlFeedInterface::UPDATED_AT, $updatedAt);

        return $this;
    }

    /**
     * @inheridoc
     */
    public function getUpdatedAt()
    {
        return $this->getData(XmlFeedInterface::UPDATED_AT);
    }

    /**
     * @inheridoc
     */
    public function setFeedType($feedType)
    {
        $this->setData(XmlFeedInterface::FEED_TYPE, $feedType);

        return $this;
    }

    /**
     * @inheridoc
     */
    public function getFeedType()
    {
        return $this->getData(XmlFeedInterface::FEED_TYPE);
    }

    public function getFeedFileName(){
        return $this->getFeedType() . "_" . $this->getEntityId();
    }
}

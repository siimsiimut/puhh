<?php

namespace Lumav\XmlFeed\Model\Options;

/**
 * Feed type options
 */
class FeedOptions implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        if (null == $this->options) {
            $this->options = [
                ["value" => "sample_xml", "label" => "Sample XML"],
                ["value" => "hansapost_xml", "label" => "Hansapost Products"],
                ["value" => "pigu_1a", "label" => "1A/Pigu XML"],
                ["value" => "stockupdate_xml", "label" => "Stock Update"],
            ];
        }

        return $this->options;
    }
}

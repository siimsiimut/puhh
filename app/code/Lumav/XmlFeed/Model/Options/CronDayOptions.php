<?php
declare(strict_types=1);

namespace Lumav\XmlFeed\Model\Options;

/**
 * Class CronDayOptions
 * @package Lumav\XmlFeed\Model\Options
 */
class CronDayOptions implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var \Magento\Framework\Locale\ListsInterface
     */
    protected $list;
    /**
     * @var array
     */
    protected $options;

    /**
     * CronDayOptions constructor.
     * @param \Magento\Framework\Locale\ListsInterface $list
     */
    public function __construct(
        \Magento\Framework\Locale\ListsInterface $list
    )
    {
        $this->list = $list;
    }

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            $this->options = $this->list->getOptionWeekdays();
        }

        return $this->options;
    }
}

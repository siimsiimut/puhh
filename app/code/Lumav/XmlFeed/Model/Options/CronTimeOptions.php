<?php
declare(strict_types=1);

namespace Lumav\XmlFeed\Model\Options;

/**
 * Class CronTimeOptions
 * @package Lumav\XmlFeed\Model\Options
 */
class CronTimeOptions implements \Magento\Framework\Data\OptionSourceInterface
{
    const MINUTES_IN_DAY = 1440;
    const MINUTES_IN_HOUR = 60;
    const MINUTES_IN_STEP = 5;
    const EVERY_DAY = '7';

    /**
     * @var array
     */
    protected $timeOptions;

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        if($this->timeOptions === null) {
            $stTime = strtotime(date('Y-m-d'));

            for ($time = 0; $time < self::MINUTES_IN_DAY; $time += self::MINUTES_IN_STEP) {
                $this->timeOptions[$time] = ['label' => date('H:i', $stTime + ($time * self::MINUTES_IN_HOUR)), 'value' => $time];
            }
        }

        return $this->timeOptions;
    }
}

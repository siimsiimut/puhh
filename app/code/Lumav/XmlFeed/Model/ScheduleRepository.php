<?php
declare(strict_types=1);

namespace Lumav\XmlFeed\Model;

use Lumav\XmlFeed\Api\Data\ScheduleInterface;
use Lumav\XmlFeed\Model\ResourceModel\Schedule as ScheduleResourceModel;
use Magento\Framework\Config\Dom\ValidationException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class ScheduleRepository
 * @package Lumav\XmlFeed\Model
 */
class ScheduleRepository implements \Lumav\XmlFeed\Api\ScheduleRepositoryInterface
{
    /**
     * @var ScheduleResourceModel
     */
    private $scheduleResource;

    /**
     * @var \Lumav\XmlFeed\Model\ScheduleFactory
     */
    private $scheduleFactory;

    public function __construct(
        ScheduleResourceModel $scheduleResource,
        \Lumav\XmlFeed\Model\ScheduleFactory $scheduleFactory
    ) {
        $this->scheduleResource = $scheduleResource;
        $this->scheduleFactory = $scheduleFactory;
    }
    /**
     * @inheritDoc
     */
    public function save(ScheduleInterface $scheduleModel)
    {
        try {
            $this->scheduleResource->save($scheduleModel);
        } catch (ValidationException $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__('Unable to save model %1', $scheduleModel->getId()));
        }

        return $scheduleModel;
    }

    /**
     * @inheritDoc
     */
    public function get(int $id)
    {
        /** @var \Lumav\XmlFeed\Model\Schedule $scheduleModel */
        $scheduleModel = $this->scheduleFactory->create();
        $this->scheduleResource->load($scheduleModel, $id);

        if (!$scheduleModel->getId()) {
            throw new NoSuchEntityException(__('Entity with specified ID "%1" not found.', $id));
        }

        return $scheduleModel;
    }

    /**
     * @inheritDoc
     */
    public function delete(ScheduleInterface $scheduleModel)
    {
        try {
            $this->scheduleResource->delete($scheduleModel);
        } catch (ValidationException $e) {
            throw new CouldNotDeleteException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__('Unable to remove entity with ID "%1"', $scheduleModel->getId()));
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById(int $id)
    {
        $model = $this->get($id);
        $this->delete($model);

        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteByFeedId(int $feedId)
    {
        try {
            $this->scheduleResource->deleteByFeedId($feedId);
        } catch (ValidationException $e) {
            throw new CouldNotDeleteException(__($e->getMessage()));
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__('Unable to remove entities with Feed ID "%1"', $feedId));
        }

        return true;
    }
}

<?php

namespace Lumav\XmlFeed\Model;

use Lumav\XmlFeed\Model\ResourceModel\XmlFeed\CollectionFactory as XmlFeedCollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var ResourceModel\Schedule\Management
     */
    protected $scheduleManager;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param XmlFeedCollectionFactory $xmlFeedCollectionFactory
     * @param array $meta
     * @param array $data
     * @param \Magento\Framework\Serialize\SerializerInterface $serializer
     * @param ResourceModel\Schedule\Management $scheduleManager
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        XmlFeedCollectionFactory $xmlFeedCollectionFactory,
        \Magento\Framework\Serialize\SerializerInterface $serializer,
        \Lumav\XmlFeed\Model\ResourceModel\Schedule\Management $scheduleManager,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $xmlFeedCollectionFactory->create();
        $this->serializer = $serializer;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->scheduleManager = $scheduleManager;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if ( !isset($this->_loadedData) ) {
            $this->_loadedData = [];
            $items = $this->collection->getItems();
            foreach ($items as $feed) {
                $feed = $this->scheduleManager->prepareScheduleData($feed);
                $data = $feed->getData();

                $val = $this->serializer->unserialize($data["ip_addresses"]);
                $val = implode(",", $val);
                $data["ip_addresses"] = $val;

                $val = $this->serializer->unserialize($data["ignored_category_ids"]);
                $val = implode(",", $val);
                $data["ignored_category_ids"] = $val;

                $val = $this->serializer->unserialize($data["ignored_product_ids"]);
                $val = implode(",", $val);
                $data["ignored_product_ids"] = $val;

                $this->_loadedData[$feed->getId()] = $data;
            }
        }
        
        return $this->_loadedData;
    }
}

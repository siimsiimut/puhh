<?php

namespace Lumav\XmlFeed\Model;

use Lumav\XmlFeed\Model\XmlFeed;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\ResourceConnection;

class FeedGenerator
{

    public function __construct(
        ObjectManagerInterface $objectmanager,
        ResourceConnection $resource,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->objectManager = $objectmanager;
        $this->resource = $resource;
        $this->_logger = $logger;

        $this->generators = [
            "sample_xml" => "Lumav\XmlFeed\Model\Generators\Sample",
            "hansapost_xml" => "Lumav\XmlFeed\Model\Generators\HansapostProductsXML",
            "pigu_1a" => "Lumav\XmlFeed\Model\Generators\Pigu1AXML"
        ];
    }

    /**
     * Generate feed xml based on feed model settings
     * @var XmlFeed $feedModel
     */
    public function generate(XmlFeed $feedModel)
    {
        ini_set('memory_limit', '3G');
        $sqlResult = $this->getSqlResult($feedModel);
        $dataArray = $this->mapSqlToArray($sqlResult);
        $this->mapDataArrayToXml($dataArray, $feedModel);
    }

    public function getSqlResult(XmlFeed $feedModel)
    {
        $connection = $this->resource->getConnection();
        $sqlResult = [];

        //Get product IDs to exclude
        $productIdsToExclude = [];

        $ignoreProductsFromFeed = $feedModel->getIgnoredProductIds();
        foreach ($ignoreProductsFromFeed as $productId) {
            $productIdsToExclude[trim($productId)] = true;
        }
        
        //Get product ids form category
        $ignoreCategoriesFromFeed = $feedModel->getIgnoredCategoryIds();
        $ignoreCategoriesFromFeedString = implode(',', $ignoreCategoriesFromFeed);
        if( $ignoreCategoriesFromFeedString ){
            $categoriesResult = $connection->fetchAll(
                $connection->select()
                    ->from(['ccp' => 'catalog_category_product'])
                    ->where("category_id IN ({$ignoreCategoriesFromFeedString})")
            );
            foreach ($categoriesResult as $category) {
                $productIdsToExclude[$category["product_id"]] = true;
            }
        }
        
        $disabledProducts = $connection->fetchAll("
            SELECT catalog_product_entity_int.entity_id FROM catalog_product_entity_int
            LEFT JOIN eav_attribute ON eav_attribute.attribute_id = catalog_product_entity_int.attribute_id
            WHERE eav_attribute.attribute_code = 'status'
            AND ( value = 2 OR value = 0 OR value IS NULL )
        ");

        foreach ($disabledProducts as $disabledProduct) {
            $productIdsToExclude[$disabledProduct["entity_id"]] = true;
        }

        $productIdsToExclude[0] = true; //Always add 0 so query doesnt break when there are no actual products to exclude
        $productIdsToExclude = array_keys($productIdsToExclude);
        $productIdExcludeString = implode(',', $productIdsToExclude);

        $attributeResult = $connection->fetchAll(
            $connection->select()
                ->from(['product' =>
                    $connection->select()
                        ->union([
                            // all int attribute option values that are attribute_options
                            $connection->select()
                                // need to select columns in specific order otherwise UNION wont work correctly
                                ->from(['attr' => 'catalog_product_entity_int'], ['eaov.value_id', 'eaov.store_id', 'eaov.value', 'cpe.*', 'ea.*'])
                                ->joinLeft(['cpe' => 'catalog_product_entity'], 'attr.entity_id = cpe.entity_id', [])
                                ->joinLeft(['ea' => 'eav_attribute'], 'ea.attribute_id = attr.attribute_id', [])
                                ->joinLeft(['eao' => 'eav_attribute_option'], 'ea.attribute_id = eao.attribute_id', [])
                                ->joinLeft(['eaov' => 'eav_attribute_option_value'], 'eaov.option_id = attr.value AND eao.option_id = attr.value', [])
                                ->where('ea.frontend_input = ?', 'select'),
                            // all int attribute values that arent attribute options
                            $connection->select()
                                ->from(['attr' => 'catalog_product_entity_int'], ['value_id', 'store_id', 'value'])
                                ->joinLeft(['cpe' => 'catalog_product_entity'], 'attr.entity_id = cpe.entity_id')
                                ->joinLeft(['ea' => 'eav_attribute'], 'ea.attribute_id = attr.attribute_id')
                                ->where('ea.frontend_input != ?', 'select'),
                            $connection->select()
                                ->from(['attr' => 'catalog_product_entity_decimal'], ['value_id', 'store_id', 'value'])
                                ->joinLeft(['cpe' => 'catalog_product_entity'], 'attr.entity_id = cpe.entity_id')
                                ->joinLeft(['ea' => 'eav_attribute'], 'ea.attribute_id = attr.attribute_id'),
                            $connection->select()
                                ->from(['attr' => 'catalog_product_entity_text'], ['value_id', 'store_id', 'value'])
                                ->joinLeft(['cpe' => 'catalog_product_entity'], 'attr.entity_id = cpe.entity_id')
                                ->joinLeft(['ea' => 'eav_attribute'], 'ea.attribute_id = attr.attribute_id'),
                            $connection->select()
                                ->from(['attr' => 'catalog_product_entity_varchar'], ['value_id', 'store_id', 'value'])
                                ->joinLeft(['cpe' => 'catalog_product_entity'], 'attr.entity_id = cpe.entity_id')
                                ->joinLeft(['ea' => 'eav_attribute'], 'ea.attribute_id = attr.attribute_id'),
                        ], \Zend_Db_Select::SQL_UNION_ALL
                        )
                ])
                ->where("product.value IS NOT NULL AND product.entity_id NOT IN ( {$productIdExcludeString} )")
        );

        $sqlResult["attributes"] = $attributeResult;

        //Related products. Make sure product itself and related product is not disabled or excluded
        $relatedResult = $connection->fetchAll(
            $connection->select()->from('catalog_product_link')
                ->joinLeft('catalog_product_entity', '`catalog_product_entity`.`entity_id` = `catalog_product_link`.`linked_product_id`', ['*'])
                ->where("`catalog_product_link`.`product_id` NOT IN ( {$productIdExcludeString} )")
                ->where("`catalog_product_entity`.`entity_id` NOT IN ( {$productIdExcludeString} )")
        );
        $sqlResult["relatedProducts"] = $relatedResult;

        //Categories
        $categorySubQuery = new \Zend_Db_Expr(
            '(
                SELECT category.*,attr.value_id, attr.store_id, attr.value, eav_attribute.*
                FROM catalog_category_product AS category
                LEFT JOIN catalog_category_entity_varchar AS attr ON attr.entity_id = category.category_id
                LEFT JOIN eav_attribute ON eav_attribute.attribute_id = attr.attribute_id
                )'
        );
        $categoriesResult = $connection->fetchAll(
            $connection->select()->from(['category' => $categorySubQuery])
                ->where("`category`.`product_id` NOT IN ( {$productIdExcludeString} )")
        );
        $sqlResult["categories"] = $categoriesResult;

        $customerGroupId = $feedModel->getCustomerGroupId();
        
        //GroupPrices
        $groupPriceResult = $connection->fetchAll(
            $connection->select()->from(['cpip' => 'catalog_product_index_price'])
                ->joinLeft(['cg' => 'customer_group'], '`cg`.`customer_group_id` = `cpip`.`customer_group_id`', ['customer_group_code'])
                ->where("`cpip`.`entity_id` NOT IN ( {$productIdExcludeString} )")
                ->where("`cg`.`customer_group_id` = {$customerGroupId}")
        );
        $sqlResult["groupPrices"] = $groupPriceResult;

        //Stock
        $stockResult = $connection->fetchAll(
            $connection->select()
                ->from('cataloginventory_stock_item')
                ->where("`cataloginventory_stock_item`.`product_id` NOT IN ( {$productIdExcludeString} )")
        );
        $sqlResult["stocks"] = $stockResult;

        //Images
        $imageResult = $connection->fetchAll(
            $connection->select()
                ->from(['mgvte' => $connection->getTableName('catalog_product_entity_media_gallery_value_to_entity')], ['mgvte.entity_id'])
                ->join(['mgv' => $connection->getTableName('catalog_product_entity_media_gallery_value')], 'mgv.value_id = mgvte.value_id', ['label', 'position', 'disabled'])
                ->join(['mg' => $connection->getTableName('catalog_product_entity_media_gallery')], 'mg.value_id = mgvte.value_id', ['attribute_id', 'value', 'media_type'])
                //->join(['die' => $connection->getTableName('directo_image_entry')], 'die.value_id = mgvte.value_id', ['url'])
                ->where("mgvte.entity_id NOT IN ( {$productIdExcludeString} )")
                ->where('mgv.store_id = 0')
        );
        $sqlResult["images"] = $imageResult;

        //Videos
        $videoResult = $connection->fetchAll(
            $connection->select()
                ->from(['mgv' => 'catalog_product_entity_media_gallery_value'])
                ->joinLeft(['mg' => 'catalog_product_entity_media_gallery'], '`mg`.`value_id` = `mgv`.`value_id`', ['*'])
                ->joinLeft(['mgvv' => 'catalog_product_entity_media_gallery_value_video'], '`mgvv`.`value_id` = `mgv`.`value_id`', ['*'])
                ->where("`mgv`.`entity_id` NOT IN ( {$productIdExcludeString} )")
                ->where('`mgv`.`store_id` = ?', 0)
                ->where('`mg`.`media_type` = ?', 'external-video')
        );
        $sqlResult["videos"] = $videoResult;

        // Websites
        $websitesResult = $connection->fetchAll(
            $connection->select()
                ->from('catalog_product_website')
                ->where("`catalog_product_website`.`product_id` NOT IN ( {$productIdExcludeString} )")
        );
        $sqlResult["websites"] = $websitesResult;

        return $sqlResult;
    }

    public function mapSqlToArray($sqlResult)
    {

        $attributes = $sqlResult["attributes"];
        $relatedProducts = $sqlResult["relatedProducts"];
        $categories = $sqlResult["categories"];
        $groupPrices = $sqlResult["groupPrices"];
        $stocks = $sqlResult["stocks"];
        $images = $sqlResult["images"];
        $videos = $sqlResult["videos"];
        $websites = $sqlResult["websites"];

        $dataArr = [];
        foreach ($attributes as $attribute) {
            $id = $attribute["entity_id"]; //Product entityt id
            $storeId = $attribute["store_id"];
            $value = $attribute["value"];
            $attributeCode = $attribute["attribute_code"]; //Code in text form like "status"

            //Add basic product entity data
            if (!array_key_exists($id, $dataArr)) {

                $dataArr[$id] = [
                    "entity_id" => $id,
                    "sku" => $attribute["sku"],
                    "attribute_set_id" => $attribute["attribute_set_id"],
                    "created_at" => $attribute["created_at"],
                    "updated_at" => $attribute["updated_at"],
                    "attributes" => [],
                    "attributes_core" => [],
                    "related" => [],
                    "categories" => [],
                    "group_prices" => [],
                    "stocks" => [],
                    "images" => [],
                    'videos' => [],
                    'websites' => []
                ];
            }

            //Add attributes basic data to current product
            $productAttributes = &$dataArr[$id]["attributes"]; //Reference attributes directly
            if (in_array($attributeCode, ["image", "small_image", "thumbnail", "options_container", "msrp_display_actual_price_type", "gift_message_available", "swatch_image", "quantity_and_stock_status"])) {
                $productAttributes = &$dataArr[$id]["attributes_core"];
            }

            if (!array_key_exists($attributeCode, $productAttributes)) {
                $productAttributes[$attributeCode] = [
                    "attribute_id" => $attribute["attribute_id"],
                    "frontend_label" => $attribute["frontend_label"],
                    "frontend_input" => $attribute["frontend_input"],
                    "backend_type" => $attribute["backend_type"],
                    "source_model" => $attribute["source_model"],
                    "values" => []
                ];
            }

            //Add languafe specific attribute value to current attribute
            $values = &$productAttributes[$attributeCode]["values"];
            $values[$storeId] = $value;
        }

        //Add related prodcts
        foreach ($relatedProducts as $relatedProduct) {
            $id = $relatedProduct["product_id"]; //Product entityt id
            if (array_key_exists($id, $dataArr)) {
                $dataArr[$id]["related"][] = [
                    "linked_product_id" => $relatedProduct["linked_product_id"],
                    "linked_product_sku" => $relatedProduct["sku"]
                ];
            }
        }

        //Categories
        foreach ($categories as $category) {
            $id = $category["product_id"]; //Product entityt id
            $categoryId = $category["category_id"];
            $storeId = $category["store_id"];
            $value = $category["value"];
            $attributeCode = $category["attribute_code"]; //Code in text form like "status"

            if (array_key_exists($id, $dataArr)) {

                $categoriesArr = &$dataArr[$id]["categories"];
                if (!array_key_exists($categoryId, $categoriesArr)) {
                    $categoriesArr[$categoryId] = [];
                }

                if (!array_key_exists($attributeCode, $categoriesArr[$categoryId])) {
                    $categoriesArr[$categoryId][$attributeCode] = [
                        "attribute_id" => $category["attribute_id"],
                        "frontend_label" => $category["frontend_label"],
                        "frontend_input" => $category["frontend_input"],
                        "backend_type" => $category["backend_type"],
                        "source_model" => $category["source_model"],
                        "values" => []
                    ];
                }
                $categoriesArr[$categoryId][$attributeCode]["values"][$storeId] = $value;
            }
        }

        foreach ($groupPrices as $groupPrice) {
            $id = $groupPrice["entity_id"]; //Product entity id
            if (array_key_exists($id, $dataArr)) {
                unset($groupPrice["entity_id"]);
                $dataArr[$id]["group_prices"][] = $groupPrice;
            }
        }

        foreach ($stocks as $stock) {
            $id = $stock["product_id"]; //Product entity id
            if (array_key_exists($id, $dataArr)) {
                unset($stock["product_id"]);
                $dataArr[$id]["stocks"][] = $stock;
            }
        }

        foreach ($images as $image) {
            $id = $image["entity_id"]; //Product entity id
            if (array_key_exists($id, $dataArr)) {
                unset($image["entity_id"]);


                if (array_key_exists("thumbnail", $dataArr[$id]["attributes_core"])) {
                    $thumbValue = isset($dataArr[$id]["attributes_core"]["thumbnail"]["values"][0]) ? $dataArr[$id]["attributes_core"]["thumbnail"]["values"][0] : '';
                    $imgValue = $image["value"];
                    if ($thumbValue == $imgValue) {
                        $image["thumbnail"] = true;
                    }
                }
                $dataArr[$id]["images"][] = $image;
            }
        }

        foreach ($videos as $video) {
            $id = $video["entity_id"]; //Product entity id
            if (array_key_exists($id, $dataArr)) {
                unset($video["entity_id"]);
                unset($video["store_id"]);

                $dataArr[$id]["videos"][] = $video;
            }
        }

        foreach ($websites as $website) {
            $id = $website["product_id"]; //Product entity id
            if (array_key_exists($id, $dataArr)) {
                $dataArr[$id]["websites"][] = $website["website_id"];
            }
        }

        unset($sqlResult);
        return $dataArr;
    }

    public function mapDataArrayToXml($dataArray, XmlFeed $feedModel)
    {
        $feedType = $feedModel->getFeedType();
        $feedFileName = $feedModel->getFeedFileName();

        if (array_key_exists($feedType, $this->generators)) {
            $generator = $this->objectManager->create($this->generators[$feedType]);
            echo "Generating feed " . $feedType . PHP_EOL;
            $this->_logger->info(  "Generating feed " . $feedType ); 
            $generator->generate($feedFileName, $dataArray, $feedModel);
        }
    }
}

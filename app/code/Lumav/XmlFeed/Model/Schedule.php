<?php
declare(strict_types = 1);

namespace Lumav\XmlFeed\Model;

use Lumav\XmlFeed\Api\Data\ScheduleInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Schedule
 *
 * @package Lumav\XmlFeed\Model
 */
class Schedule extends AbstractModel implements ScheduleInterface
{
    protected function _construct()
    {
        $this->_init(\Lumav\XmlFeed\Model\ResourceModel\Schedule::class);
    }

    /**
     * @inheritDoc
     */
    public function getFeedId()
    {
        return $this->getData(self::FEED_ID);
    }

    /**
     * @inheritDoc
     */
    public function setFeedId(int $feedId)
    {
        $this->setData(self::FEED_ID, $feedId);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCronTime()
    {
        return $this->getData(self::CRON_TIME);
    }

    /**
     * @inheritDoc
     */
    public function setCronTime(int $cronTime)
    {
        $this->setData(self::CRON_TIME, $cronTime);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCronDay()
    {
        return $this->getData(self::CRON_DAY);
    }

    /**
     * @inheritDoc
     */
    public function setCronDay(int $cronDay)
    {
        $this->setData(self::CRON_DAY, $cronDay);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getLastExecTime()
    {
        return $this->getData(self::LAST_EXEC_TIME);
    }

    /**
     * @inheritDoc
     */
    public function setLastExecTime(string $lastExecTime)
    {
        $this->setData(self::LAST_EXEC_TIME, $lastExecTime);
        return $this;
    }
}

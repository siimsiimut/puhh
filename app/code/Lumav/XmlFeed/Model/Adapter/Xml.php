<?php
declare(strict_types = 1);

namespace Lumav\XmlFeed\Model\Adapter;

/**
 * Class Xml
 *
 * added here because @see Xml::addCDataChild() would not autocomplete for child elements
 * @method $this addChild(string $name, $value = null, $namespace = null)
 *
 * @package Lumav\XmlFeed\Model\Adapter
 */
class Xml extends \SimpleXMLElement
{
    /**
     * Add element with CData value type
     *
     * @param $name
     * @param null $value
     * @return $this
     */
    public function addCDataChild($name, $value = null) {
        $child = $this->addChild($name);
        if ($child !== null) {
            $node = dom_import_simplexml($child);
            $no = $node->ownerDocument;
            $node->appendChild($no->createCDATASection($value));
        }
        return $child;
    }

    public function formatXml()
    {
        $xmlDocument = new \DOMDocument('1.0');
        $xmlDocument->preserveWhiteSpace = false;
        $xmlDocument->formatOutput = true;
        $xmlDocument->loadXML($this->asXML());

        return htmlentities($xmlDocument->saveXML());
    }

    /**
     * Removes xml header and return xml in string format
     * @return false|string
     */
    public function asString()
    {
        $dom = dom_import_simplexml($this);
        return $dom->ownerDocument->saveXML($dom->ownerDocument->documentElement);
    }

    /**
     * Add SimpleXMLElement code into a SimpleXMLElement
     * @param Xml $xml
     * @return bool
     */
    public function appendXML(Xml $xml)
    {
        $parent = $this;
        $xml = $xml->asString();
        // check if there is something to add
        if ($nodata = !strlen($xml) or $parent[0] == NULL) {
            return $nodata;
        }

        // add the XML
        $node     = dom_import_simplexml($parent);
        $fragment = $node->ownerDocument->createDocumentFragment();
        $fragment->appendXML($xml);

        return (bool)$node->appendChild($fragment);
    }
}

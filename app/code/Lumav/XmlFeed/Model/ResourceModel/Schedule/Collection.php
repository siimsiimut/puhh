<?php
declare(strict_types = 1);

namespace Lumav\XmlFeed\Model\ResourceModel\Schedule;

use Lumav\XmlFeed\Api\Data\ScheduleInterface;
use \Lumav\XmlFeed\Model\Options\CronTimeOptions;

/**
 * Class Collection
 *
 * @package Lumav\XmlFeed\Model\ResourceModel\Schedule
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(\Lumav\XmlFeed\Model\Schedule::class, \Lumav\XmlFeed\Model\ResourceModel\Schedule::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }

    /**
     * Only return feed schedules which are in range now timestamp and now-step
     *
     * @param int $timestamp
     * @return Collection
     */
    public function addValidateTimeFilter(int $timestamp)
    {
        $minutesFromMidnight = floor(($timestamp - strtotime('today')) / 60);
        $currentDay = idate('w', $timestamp);
        $lastExecMinusStepMinutes = date('Y-m-d H:i:s', ($timestamp - CronTimeOptions::MINUTES_IN_STEP * 60));

        $this
            ->addFieldToFilter(ScheduleInterface::CRON_DAY, $currentDay)
            ->addFieldToFilter(ScheduleInterface::CRON_TIME, ['lteq' => $minutesFromMidnight])
            ->addFieldToFilter(ScheduleInterface::CRON_TIME, ['gt' => $minutesFromMidnight - CronTimeOptions::MINUTES_IN_STEP])
            ->addFieldToFilter(ScheduleInterface::LAST_EXEC_TIME, [
                ['lteq' => $lastExecMinusStepMinutes],
                ['null' => true]
            ])
        ;
        return $this;
    }
}

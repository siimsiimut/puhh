<?php
declare(strict_types=1);

namespace Lumav\XmlFeed\Model\ResourceModel\Schedule;

use Lumav\XmlFeed\Api\Data\ScheduleInterface;
use Lumav\XmlFeed\Api\ScheduleRepositoryInterface;
use Lumav\XmlFeed\Model\Options\CronDayOptions;
use Lumav\XmlFeed\Model\Options\CronTimeOptions;
use Lumav\XmlFeed\Model\ResourceModel\Schedule\CollectionFactory;
use Lumav\XmlFeed\Model\ScheduleFactory;
use \Magento\Framework\Validator\Exception as ValidatorException;

/**
 * Class Management
 * @package Lumav\XmlFeed\Model\ResourceModel\Schedule
 */
class Management extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var ScheduleFactory
     */
    protected $scheduleFactory;

    /**
     * @var ScheduleRepositoryInterface
     */
    protected $scheduleRepository;

    /**
     * @var CollectionFactory
     */
    protected $scheduleCollectionFactory;
    /**
     * @var CronDayOptions
     */
    protected $cronDayOptions;
    /**
     * @var CronTimeOptions
     */
    protected $cronTimeOptions;

    public function __construct(
        ScheduleFactory $scheduleFactory,
        ScheduleRepositoryInterface $scheduleRepository,
        CollectionFactory $scheduleCollectionFactory,
        CronDayOptions $cronDayOptions,
        CronTimeOptions $cronTimeOptions,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        $this->scheduleFactory = $scheduleFactory;
        $this->scheduleRepository = $scheduleRepository;
        $this->scheduleCollectionFactory = $scheduleCollectionFactory;
        $this->cronDayOptions = $cronDayOptions;
        $this->cronTimeOptions = $cronTimeOptions;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @param int $feedId
     * @param array $data
     */
    public function saveScheduleData(int $feedId, array $data)
    {
        $this->removeExistData($feedId);

        if (!empty($data[ScheduleInterface::CRON_DAY]) && !empty($data[ScheduleInterface::CRON_TIME])) {
            foreach ($data[ScheduleInterface::CRON_DAY] as $cronDay) {
                foreach ($data[ScheduleInterface::CRON_TIME] as $cronTime) {
                    /** @var \Lumav\XmlFeed\Model\Schedule $schedule */
                    $schedule = $this->scheduleFactory->create();
                    $schedule->setFeedId($feedId)
                        ->setCronDay((int)$cronDay)
                        ->setCronTime((int)$cronTime);
                    $this->scheduleRepository->save($schedule);
                }
            }
        }
    }

    /**
     * @param \Lumav\XmlFeed\Model\XmlFeed $feed
     *
     * @return \Lumav\XmlFeed\Model\XmlFeed
     */
    public function prepareScheduleData(\Lumav\XmlFeed\Model\XmlFeed $feed)
    {
        $cronDay = [];
        $cronTime = [];
        /** @var \Lumav\XmlFeed\Model\ResourceModel\Schedule\Collection $scheduleCollection */
        $scheduleCollection = $this->scheduleCollectionFactory->create();
        $scheduleCollection->addFieldToFilter(ScheduleInterface::FEED_ID, $feed->getId());

        /** @var \Lumav\XmlFeed\Model\Schedule $schedule */
        foreach ($scheduleCollection->getItems() as $schedule) {
            //Get distinctive values
            $cronDay[  $schedule->getCronDay() ] = $schedule->getCronDay();
            $cronTime[  $schedule->getCronTime() ] = $schedule->getCronTime();
        }
        $feed[ScheduleInterface::CRON_DAY] =  array_values( $cronDay ); //Replace assoc keys with numbers
        $feed[ScheduleInterface::CRON_TIME] = array_values( $cronTime );

        return $feed;
    }

    /**
     * @param int $feedId
     */
    public function removeExistData(int $feedId)
    {
        $this->scheduleRepository->deleteByFeedId($feedId);
    }

    /**
     * Validate schedule data
     *
     * @param array $data
     * @return void
     * @throws ValidatorException
     */
    public function validateScheduleData(array $data)
    {
        if(!isset($data[\Lumav\XmlFeed\Api\Data\XmlFeedInterface::ENTITY_ID])) {
            //throw new ValidatorException(__('Feed ID not found'));
        }
        if (!isset($data[ScheduleInterface::CRON_DAY], $data[ScheduleInterface::CRON_TIME])
        ) {
            throw new ValidatorException(__('Schedule not found'));
        }
        $hasDays = is_array($data[ScheduleInterface::CRON_DAY]);
        $hasTimes = is_array($data[ScheduleInterface::CRON_TIME]);
        if (!$hasDays && !$hasTimes) {
            throw new ValidatorException(__('Please select schedule time(s) and day(s).'));
        } elseif (!$hasTimes) {
            throw new ValidatorException(__('Please select schedule time(s).'));
        } elseif (!$hasDays) {
            throw new ValidatorException(__('Please select schedule day(s).'));
        } else {
            $dayOptions = $this->cronDayOptions->toOptionArray();
            foreach($data[ScheduleInterface::CRON_DAY] as $value) {
                if(!isset($dayOptions[$value])) {
                    throw new ValidatorException(__('Error in schedule day'));
                }
            }
            $timeOptions = $this->cronTimeOptions->toOptionArray();
            foreach($data[ScheduleInterface::CRON_TIME] as $value) {
                if(!isset($timeOptions[$value])) {
                    throw new ValidatorException(__('Error in schedule time'));
                }
            }
        }
    }
}

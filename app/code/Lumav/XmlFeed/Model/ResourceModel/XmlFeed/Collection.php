<?php


namespace Lumav\XmlFeed\Model\ResourceModel\XmlFeed;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    
    protected function _construct()
    {
       // parent::_construct();
        $this->_init(\Lumav\XmlFeed\Model\XmlFeed::class, \Lumav\XmlFeed\Model\ResourceModel\XmlFeed::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }

}

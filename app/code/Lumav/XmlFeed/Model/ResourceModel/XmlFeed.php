<?php


namespace Lumav\XmlFeed\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class XmlFeed extends AbstractDb
{
    const TABLE_NAME = 'xml_feed_entity';
    const ID = 'entity_id';

    /* 
     * @inheridoc
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, self::ID);
    }
}

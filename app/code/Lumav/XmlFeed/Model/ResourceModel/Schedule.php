<?php
declare(strict_types = 1);

namespace Lumav\XmlFeed\Model\ResourceModel;

use Lumav\XmlFeed\Api\Data\ScheduleInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Schedule
 *
 * @package Lumav\XmlFeed\Model\ResourceModel
 */
class Schedule extends AbstractDb
{
    const TABLE = 'xml_feed_schedule';

    /**
     * @inheridoc
     */
    public function _construct()
    {
        $this->_init(self::TABLE, ScheduleInterface::ID);
    }

    /**
     * @param int $feedId
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteByFeedId(int $feedId)
    {
        /** @var \Magento\Framework\DB\Adapter\AdapterInterface $connection */
        $connection = $this->getConnection();

        $query = $connection->deleteFromSelect(
            $connection->select()->from($this->getMainTable(), ScheduleInterface::FEED_ID)->where(
                ScheduleInterface::FEED_ID . ' = ?',
                $feedId
            ),
            $this->getMainTable()
        );

        $connection->query($query);
    }
}

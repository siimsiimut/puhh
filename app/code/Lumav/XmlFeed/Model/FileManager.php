<?php
declare(strict_types = 1);

namespace Lumav\XmlFeed\Model;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\File\Uploader;

/**
 * Class FileManager
 *
 * @package Lumav\XmlFeed\Model
 */
class FileManager
{
    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $fileSystem;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param \Magento\Framework\Filesystem $fileSystem
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\Filesystem $fileSystem,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->fileSystem = $fileSystem;
        $this->storeManager = $storeManager;
    }

    /**
     * Save Feed xml to file generated from $feedName
     *
     * @param string $feedName
     * @param string $content
     * @return bool true when succeeded|false otherwise
     */
    public function saveFeed(string $feedName, string $content): bool
    {
        $filePath = $this->getFilePath($feedName);

        try {
            $directoryInstance = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
            if (!$directoryInstance->isFile($filePath) || $directoryInstance->delete($filePath)) {
                $directoryInstance->writeFile($filePath, $content);
                return true;
            }
        } catch (\Magento\Framework\Exception\FileSystemException $e) {
            echo $e->getMessage();
            // @TODO Add logger
        }
        return false;
    }

    /**
     * Delete feed content if it exists
     *
     * @param string $feedName
     * @return bool true when found file to delete|false otherwise
     */
    public function deleteFeed(string $feedName): bool
    {
        $filePath = $this->getFilePath($feedName);

        try {
            $directoryInstance = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
            if ($directoryInstance->isFile($filePath)) {
                $directoryInstance->delete($filePath);
                return true;
            }
        } catch (\Magento\Framework\Exception\FileSystemException $e) {
            // @TODO Add logger?
        }
        return false;
    }

    /**
     * Get feed content by feed name
     *
     * @param string $feedName
     * @return string|bool return content when found|false otherwise
     */
    public function getFeedContent(string $feedName)
    {
        $filePath = $this->getFilePath($feedName);

        try {
            $directoryInstance = $this->fileSystem->getDirectoryRead(DirectoryList::MEDIA);
            if ($directoryInstance->isFile($filePath)) {
                return $directoryInstance->readFile($filePath);
            }
        } catch (\Magento\Framework\Exception\FileSystemException $e) {
            // @TODO Add logger?
        }
        return false;
    }

    /**
     * Get feed web url
     *
     * @param string $feedName
     * @param null $storeId
     * @return false|string
     */
    public function getFeed(string $feedName, $storeId = null)
    {
        if ($this->getFeedContent($feedName)) {
            try {
                $url = $this->storeManager->getStore($storeId)
                                          ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                $url .= self::_addDirSeparator($this->getBaseMediaPath()) . $this->getFileName($feedName);
                return $url;
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                // @TODO Add logger?
            }
        }
        return false;
    }

    /**
     * Get file path by feed name
     *
     * @param string $feedName
     * @return string
     */
    protected function getFilePath(string $feedName): string
    {
        $absoluteMediaPath = $this->fileSystem->getDirectoryRead(DirectoryList::MEDIA)
                                              ->getAbsolutePath() . $this->getBaseMediaPath();
        $fileName = $this->getFileName($feedName);
        return self::_addDirSeparator($absoluteMediaPath) . $fileName;
    }

    /**
     * Get base path from media path
     *
     * @return string
     */
    protected function getBaseMediaPath(): string
    {
        return 'xml_feed';
    }

    /**
     * Get file name from feed name
     *
     * @param $feedName
     * @return string
     */
    protected function getFileName($feedName): string
    {
        return Uploader::getCorrectFileName($feedName) . '.xml';
    }

    /**
     * Add directory separator when needed
     *
     * @param $dir
     * @return string
     */
    protected static function _addDirSeparator($dir): string
    {
        if (substr($dir, -1) != DIRECTORY_SEPARATOR) {
            $dir .= DIRECTORY_SEPARATOR;
        }
        return $dir;
    }
}

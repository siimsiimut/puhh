<?php
declare(strict_types=1);

namespace Lumav\XmlFeed\Model\Generators;

use Lumav\XmlFeed\Model\Adapter\Xml;
use Lumav\XmlFeed\Model\FileManager;
use Lumav\XmlFeed\Model\XmlFeed;

/**
 * Class AbstractGenerator
 * @package Lumav\XmlFeed\Model\Generators
 */
abstract class AbstractGenerator
{
    /**
     * @var string
     */
    protected $xmlInit = '<?xml version="1.0" encoding="utf-8" ?><products />';
    /**
     * @var string
     */
    protected $feedName;

    /**
     * @var FileManager
     */
    protected $fileManager;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    public function __construct(
        FileManager $fileManager,
        \Magento\Framework\App\ResourceConnection $resource
    )
    {
        $this->fileManager = $fileManager;
        $this->resource = $resource;
    }
    /**
     * return @void
     */
    protected abstract function initialize();

    /**
     * @param string $feedName
     * @param array $feedData
     * @param XmlFeed $feedModel
     * @return void
     * @throws \Exception
     */
    public function generate(string $feedName, array $feedData, XmlFeed $feedModel )
    {
        $this->feedName = $feedName;
        $this->initialize();
        //1 Generate xml
        $xml = new Xml($this->xmlInit);
        $xml = $this->createXml($feedData, $xml, $feedModel);

        //2 Save xml
        $this->fileManager->saveFeed($feedName, $xml->asXML());
    }

    /**
     * @param array $feedData
     * @param Xml $xml
     * @param XmlFeed $feedModel
     * @return Xml
     */
    protected abstract function createXml(array $feedData, Xml $xml, XmlFeed $feedModel);


    /**
     * Get isset attribute values
     *
     * @param array $values
     * @return bool
     */
    protected function isSet(array $values): bool
    {
        if (isset($values)) {
            $firstValue = reset($values);
            if ($firstValue && $firstValue !== 'NULL') {
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $price
     * @return string
     */
    protected function roundPrice(string $price)
    {
        return (string)round($price, 4);
    }
}

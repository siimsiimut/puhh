<?php
declare(strict_types=1);

namespace Lumav\XmlFeed\Model\Generators;

use Lumav\XmlFeed\Helper\CategoryHelper;
use Lumav\XmlFeed\Model\Adapter\Xml;
use Lumav\XmlFeed\Model\FileManager;
use Lumav\XmlFeed\Model\XmlFeed;
use Magento\Framework\UrlInterface;


class HansapostProductsXML extends \Lumav\XmlFeed\Model\Generators\AbstractGenerator
{
    /**
     * @var array
     */
    protected $storeData;
    /**
     * @var \Lumav\XmlFeed\Helper\K24Helper
     */
    protected $categoryRequiredProperties;
    /**
     * @var
     */
    protected $productAttributes;

    /**
     * HansapostProductsXML constructor.
     * @param FileManager $fileManager
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        FileManager $fileManager,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        parent::__construct($fileManager, $resource);
        $this->storeManager = $storeManager;
    }

    /**
     * @inheritDoc
     */
    protected function initialize()
    {
        $connection = $this->resource->getConnection();

        // get all store ids and create suffixes for them
        $data = $connection->fetchAll(
            $connection->select()->from($connection->getTableName('store'))
        );

        $mapping = [
            0 => 'default',
            1 => 'et',
            //2 => 'ru',
            //3 => 'lv', // toys
            //4 => 'ru', // toys
            //5 => 'en', // toys
            //6 => 'en'
        ];
        foreach ($data as $row) {
            if (isset($mapping[$row['store_id']])) {
                $this->storeData[$row['store_id']] = $mapping[$row['store_id']];
            }
        }
    }

    /**
     * @param array $feedData
     * @param Xml $xml
     * @param XmlFeed $feedModel
     * @return Xml
     */
    protected function createXml(array $feedData, Xml $xml, XmlFeed $feedModel)
    {
        $i=0;
        /** @var array $productData */
        foreach ($feedData as $productId => $productData) {
            $valid = true;
            $productNode = new Xml('<toode/>');

            $this->addId($productNode, $productData);
            $this->addCategory($productNode, $productData);
            $this->addSku($productNode, $productData);
            $this->addEan($productNode, $productData);
            $this->addQuantity($productNode, $productData);
            $this->addUrl($productNode, $productData);
            $this->addStoreBasedProductData($productNode, $productData, $valid);
            $this->addDimensions($productNode, $productData);
            $this->addImages($productNode, $productData);
            $this->addPrice($productNode, $productData);

            if ($valid) {
                $xml->appendXML($productNode);
            }

            $i++;
        }

        echo "Products count: " . $i . PHP_EOL;

        return $xml;
    }

    /**
     * @param Xml $node
     * @param array $productData
     */
    protected function addId(Xml $node, array $productData)
    {
        $node->addChild('id', $productData['entity_id']);
    }

    /**
     * @param Xml $node
     * @param array $productData
     * @return void
     */
    protected function addSku(Xml $node, array $productData)
    {
        $node->addChild('tootekood', $productData['sku']);
    }

    /**
     * @param Xml $node
     * @param array $productData
     * @return void
     */
    protected function addEan(Xml $node, array $productData)
    {
        $value = '';
        if (
            isset($productData['attributes']['ean13'])
            && $this->isSet($productData['attributes']['ean13']['values'])
        ) {
            $value = isset($productData['attributes']['ean13']['values'][0]) ? $productData['attributes']['ean13']['values'][0] : $productData['attributes']['ean13']['values'][1];
        }
        $node->addChild('EANribakood', $value);
    }

    /**
     * @param Xml $node
     * @param array $productData
     */
    protected function addCategory(Xml $node, array $productData)
    {
        $value = '';
        $categories = $productData['categories'];
        if (!empty($categories)) {
            foreach ($categories as $categoryId => $category) {
                $value = $category['name']['values'][0];
                break;
            }
        }
        $node->addCDataChild('kategooria', $value);
    }

    /**
     * @param Xml $node
     * @param array $productData
     */
    protected function addQuantity(Xml $node, array $productData)
    {
        $value = 0;
        $stocks = $productData['stocks'];
        if (!empty($stocks)) {
            $isInStock = $stocks[0]['is_in_stock'];
            $qty = $stocks[0]['qty'];
            $value = $qty >= 5 ? 1 : 0;
        }
        $node->addChild('kogus', (string) $value);
    }

    /**
     * @param Xml $node
     * @param array $productData
     */
    protected function addUrl(Xml $node, array $productData)
    {
        $value = '';
        $baseUrl = $this->storeManager->getStore()->getBaseUrl();
        $urlKey = $productData['attributes']['url_key']['values'][0];
        $value = $baseUrl . $urlKey;
        $node->addChild('url', $value);
    }

    /**
     * @param Xml $node
     * @param array $productData
     */
    protected function addPrice(Xml $node, array $productData)
    {
        if (isset($productData['attributes']['price']) && $this->isSet($productData['attributes']['price']['values'])) {
            $price = $productData['attributes']['price']['values'][0];
            $node->addChild('tarnijahind', (string) ($price / 1.2 * 0.75));
        }
    }

    /**
     * Add product data that is based on store
     * eg. name, description
     *
     * @param Xml $node
     * @param array $productData
     * @param $valid
     * @return void
     */
    protected function addStoreBasedProductData(XML $node, array $productData, &$valid)
    {
        $productAttributes = &$productData['attributes'];
        $fields = [];

        if (isset($productAttributes['name']) && $this->isSet($productAttributes['name']['values'])) {
            $names = [];
            foreach ($productAttributes['name']['values'] as $storeId => $name) {
                if (isset($this->storeData[$storeId])) {
                    $storeCode = $this->storeData[$storeId];
                    $names[$storeCode] = ['label' => 'nimetus', 'value' => $name];
                }
            }
            if (!isset($names['et']) || isset($names['et']['value']) && !$names['et']['value']) {
                $names['et'] = isset($names['default']) ? $names['default'] : [];
                unset($names['default']);
                if (empty($names['et'])) {
                    unset($names['et']);
                }
            }
            if (!empty($names)) {
                $fields['nimetused'] = $names;
            }
        }

        if (isset($productAttributes['description']) && $this->isSet($productAttributes['description']['values'])) {
            $descriptions = [];
            foreach ($productAttributes['description']['values'] as $storeId => $description) {
                if (isset($this->storeData[$storeId])) {
                    $storeCode = $this->storeData[$storeId];
                    $descriptions[$storeCode] = ['label' => 'kirjeldus', 'value' => $description];
                }
            }
            if (!isset($descriptions['et']) || isset($descriptions['et']['value']) && !$descriptions['et']['value']) {
                $descriptions['et'] = isset($descriptions['default']) ? $descriptions['default'] : [];
                unset($descriptions['default']);
                if (empty($descriptions['et'])) {
                    unset($descriptions['et']);
                }
            }
            if (!empty($descriptions)) {
                $fields['kirjeldused'] = $descriptions;
            }
        }
        if (!empty($fields)) {
            foreach ($fields as $key => $values) {
                $keyNode = $node->addChild($key);

                foreach ($values as $storeCode => $value) {
                    $localNode = $keyNode->addChild('lokaalne' . $value['label']);
                    $localNode->addChild('riik', $storeCode);
                    $localNode->addCDataChild($value['label'], $value['value']);
                }
            }
        }
    }

    /**
     * Add product images to node
     *
     * @param Xml $node
     * @param array $productData
     * @return void
     */
    protected function addImages(Xml $node, array $productData)
    {
        $baseUrl = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        $baseUrl = $baseUrl . UrlInterface::URL_TYPE_MEDIA .  '/catalog/product';

        if (isset($productData['attributes_core']['image']) && $this->isSet($productData['attributes_core']['image']['values'])) {

            $mainImage = isset($productData['attributes_core']['image']['values'][0]) ? $productData['attributes_core']['image']['values'][0] : $productData['attributes_core']['image']['values'][1];
            $url = $url = $baseUrl . $mainImage;
            $node->addChild('peapilt', htmlspecialchars($url));
        }

        $imagesNode = $node->addChild('pildid');

        foreach ($productData['images'] as $image) {
            $url = $baseUrl . $image['value'];
            $imagesNode->addChild('pilt', htmlspecialchars($url));
        }
    }

    /**
     * @param Xml $node
     * @param array $productData
     */
    protected function addDimensions(Xml $node, array $productData )
    {
        $weight = '0';
        if ( isset($productData['attributes']['weight']) && $this->isSet($productData['attributes']['weight']['values'])) {
            $weight = isset($productData['attributes']['weight']['values'][0]) ? $productData['attributes']['weight']['values'][0] : $productData['attributes']['weight']['values'][1];
        }

        $depth = '0';
        if ( isset($productData['attributes']['depth']) && $this->isSet($productData['attributes']['depth']['values'])) {
            $depth = isset($productData['attributes']['depth']['values'][0]) ? $productData['attributes']['depth']['values'][0] : $productData['attributes']['depth']['values'][1];
        }

        $height = '0';
        if ( isset($productData['attributes']['height']) && $this->isSet($productData['attributes']['height']['values'])) {
            $height = isset($productData['attributes']['height']['values'][0]) ? $productData['attributes']['height']['values'][0] : $productData['attributes']['height']['values'][1];
        }

        $width = '0';
        if ( isset($productData['attributes']['width']) && $this->isSet($productData['attributes']['width']['values'])) {
            $width = isset($productData['attributes']['width']['values'][0]) ? $productData['attributes']['width']['values'][0] : $productData['attributes']['width']['values'][1];
        }

        $dimensionsNode = $node->addChild('moodud');
        $dimensionsNode->addChild('pikkus', (string) $depth);
        $dimensionsNode->addChild('korgus', (string) $height);
        $dimensionsNode->addChild('laius', (string) $width);
        $dimensionsNode->addChild('kaal', (string) $weight);
    }
}

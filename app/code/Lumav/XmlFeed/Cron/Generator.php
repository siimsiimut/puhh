<?php

namespace Lumav\XmlFeed\Cron;

use Lumav\XmlFeed\Api\Data\ScheduleInterface;
use Lumav\XmlFeed\Api\Data\XmlFeedInterface;
use Lumav\XmlFeed\Model\FeedGenerator;
use Lumav\XmlFeed\Model\ResourceModel\XmlFeed\CollectionFactory as XmlFeedCollectionFactory;
use Lumav\XmlFeed\Model\ResourceModel\Schedule\CollectionFactory as ScheduleCollectionFactory;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

/**
 * Class Generator
 * @package Lumav\XmlFeed\Cron
 */
class Generator
{
    /**
     * @var XmlFeedCollectionFactory
     */
    protected $xmlFeedCollectionFactory;
    /**
     * @var ScheduleCollectionFactory
     */
    protected $scheduleCollectionFactory;
    /**
     * @var TimezoneInterface
     */
    protected $timezone;
    /**
     * @var FeedGenerator
     */
    protected $feedGenerator;

    public function __construct(
        FeedGenerator $feedGenerator,
        XmlFeedCollectionFactory $xmlFeedCollectionFactory,
        ScheduleCollectionFactory $scheduleCollectionFactory,
        TimezoneInterface $timezone

    )
    {
        $this->feedGenerator = $feedGenerator;
        $this->xmlFeedCollectionFactory = $xmlFeedCollectionFactory;
        $this->scheduleCollectionFactory = $scheduleCollectionFactory;
        $this->timezone = $timezone;
    }

    /**
     * @param $force - Force generation if running manually from script
     * @return Generator
     */
    public function generate( $force = true )
    {
        try {
            /** @var \Lumav\XmlFeed\Model\ResourceModel\XmlFeed\Collection $feedCollection */
            $feedCollection = $this->xmlFeedCollectionFactory->create();
            $feedCollection->addFieldToFilter(XmlFeedInterface::IS_ACTIVE, ['eq' => 1]);

            /** @var \Lumav\XmlFeed\Model\ResourceModel\Schedule\Collection $scheduleCollection */
            $scheduleCollection = $this->scheduleCollectionFactory->create();
            $scheduleCollection->addFieldToSelect('*');
            $time = $this->formatDate('now');
            // The unix timestamp isn't affected by a timezone setting.
            // Setting the timezone only affects the interpretation of the timestamp value.
            // so cant use $dateTime->getTimestamp() here to get timestamp which includes the timezone difference
            $timestamp = strtotime($time);
            // filter for schedules that have not been

            $scheduleCollection->addFieldToFilter(ScheduleInterface::FEED_ID, ['in' => $feedCollection->getAllIds()]);
            if ($force){
                $scheduleCollection->addValidateTimeFilter($timestamp);
            }
            $feedIds = [];
            /** @var \Lumav\XmlFeed\Api\Data\ScheduleInterface $schedule */
            foreach ($scheduleCollection->getItems() as $schedule) {
                if (!in_array($schedule->getFeedId(), $feedIds)) {
                    $feedIds[] = $schedule->getFeedId();
                }
                $schedule->setLastExecTime($time);
                $schedule->save();
            }
            /** @var \Lumav\XmlFeed\Model\ResourceModel\XmlFeed\Collection $feedCollection */
            $feedCollection = $this->xmlFeedCollectionFactory->create();
            $feedCollection
                ->addFieldToSelect('*')
                ->addFieldToFilter(XmlFeedInterface::ENTITY_ID, ['in' => $feedIds])
                ->addFieldToFilter(XmlFeedInterface::IS_ACTIVE, ['eq' => 1]);
            foreach ($feedCollection as $feed) {
                $this->feedGenerator->generate($feed);
            }
        } catch (\Exception $exception) {
            echo $exception->getMessage()();
            //@TODO Add logger?
        }
        return $this;
    }

    /**
     * Formats current time to include timezone difference
     *
     * @param string|\DateTime $date
     * @param string|null $fromTimezone
     * @return string
     * @throws \Exception
     */
    public function formatDate($date, $fromTimezone = null)
    {
        if (!$fromTimezone) {
            $fromTimezone = $this->timezone->getDefaultTimezone();
        }
        $toTimezone = $this->timezone->getConfigTimezone();

        if ($date instanceof \DateTime) {
            $dateTime = $date;
        } else {
            $dateTime = new \DateTime($date, new \DateTimeZone($fromTimezone));
        }
        $dateTime->setTimezone(new \DateTimeZone($toTimezone));

        return $dateTime->format('Y-m-d\TH:i:s');
    }
}

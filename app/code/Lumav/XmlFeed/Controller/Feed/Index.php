<?php
namespace Lumav\XmlFeed\Controller\Feed;

use Magento\Framework\App\Action\Context;
use Lumav\XmlFeed\Api\Data\XmlFeedInterface;
use Lumav\XmlFeed\Model\XmlFeedRepository;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Lumav\XmlFeed\Model\FileManager;
use Magento\Framework\Controller\Result\RawFactory;

class Index extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;

	public function __construct(
		Context $context,
        XmlFeedRepository $xmlFeedRepository,
        RemoteAddress $remoteAddress,
        FileManager $fileManager,
        RawFactory $resultRawFactory
    )
	{
        $this->xmlFeedRepository = $xmlFeedRepository;
        $this->remoteAddress = $remoteAddress;
        $this->fileManager = $fileManager;
        $this->resultRawFactory     = $resultRawFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
        $noRoute = false;
        $request = $this->getRequest();
        $hash = $request->getParam( XmlFeedInterface::HASH );

        try{
            $model = $this->xmlFeedRepository->getByHash( $hash );

            $remoteIp = $this->remoteAddress->getRemoteAddress();
            $ips = $model->getIpAddresses();
          
            if( $ips ){
                if( !in_array( $remoteIp, $ips ) ){
                    $noRoute = true;
                }
            }
    
            if( !$model->getIsActive() ){
                $noRoute = true;
            }
        }
        catch (\Exception $e) {
            $noRoute = true;
        }

        if( $noRoute ){
            return $this->_redirect("no-route");
        }
       
        $feedContent = $this->fileManager->getFeedContent( $model->getFeedFileName() );
        if( !$feedContent ){
            $feedContent = "<notice>Feed not generated yet</notice>";
        }

        $result = $this->resultRawFactory->create();
        $result->setHeader('Content-Type', 'text/xml');
        $result->setContents( $feedContent );
        return $result;
	}
}
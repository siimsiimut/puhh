<?php
namespace Lumav\XmlFeed\Controller;

use Magento\Framework\App\RouterInterface;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\RequestInterface;

use Lumav\XmlFeed\Api\Data\XmlFeedInterface;

class Router implements RouterInterface
{
    public function __construct(
        ActionFactory $actionFactory
    ) {
        $this->actionFactory = $actionFactory;
    }

    public function match( RequestInterface $request )
    {
        $identifier = trim( $request->getPathInfo(), '/');
        $hash =  $request->getParam( XmlFeedInterface::HASH, null  );

        if( $identifier != "feed" || !$hash ) {
            return false;
        }

        //Redirect to controller
        $request->setModuleName('xmlfeed')->setControllerName('feed')->setActionName('index');

        return $this->actionFactory->create(
            'Magento\Framework\App\Action\Forward',
            ['request' => $request]
        );
    }
}
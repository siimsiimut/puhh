<?php

namespace Lumav\XmlFeed\Controller\Adminhtml\Feed;

use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\Controller\ResultFactory;

class MassDelete extends AbstractMassAction
{   
    protected function massAction(AbstractCollection $collection)
    {
       
        try{
            $feedsDeleted = 0;
            foreach ($collection->getItems() as $feed) {
                $feedName = $feed->getFeedFileName();
                $this->fileManager->deleteFeed($feedName);
                $this->scheduleRepository->deleteByFeedId( $feed->getEntityId() );
                $this->xmlFeedRepository->delete($feed);
                $feedsDeleted++;
            }
        }catch(\Exception $e){
            $this->messageManager->addErrorMessage( $e->getMessage() );
        }
       

        if ($feedsDeleted) {
            $this->messageManager->addSuccessMessage(__('A total of %1 record(s) were deleted.', $feedsDeleted));
        }

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath($this->getComponentRefererUrl());

        return $resultRedirect;
    }
}

<?php

namespace Lumav\XmlFeed\Controller\Adminhtml\Feed;

use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\Controller\ResultFactory;

class MassDisable extends AbstractMassAction
{   
    protected function massAction(AbstractCollection $collection)
    {
        try{
            $disabled = 0;
            foreach ($collection->getItems() as $feed) {
                $feed->setIsActive(0);
                $this->xmlFeedRepository->save($feed);
                $disabled++;
            }

            
        }catch(\Exception $e){
            $this->messageManager->addErrorMessage( $e->getMessage() );
        }

        if ($disabled) {
            $this->messageManager->addSuccessMessage(__('A total of %1 record(s) were disabled.', $disabled));
        }

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath($this->getComponentRefererUrl());

        return $resultRedirect;
    }
}

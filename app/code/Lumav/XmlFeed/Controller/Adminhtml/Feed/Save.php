<?php

namespace Lumav\XmlFeed\Controller\Adminhtml\Feed;

use Lumav\XmlFeed\Api\Data\XmlFeedInterface;
use Magento\Backend\App\Action;
use Lumav\XmlFeed\Model\XmlFeedRepository;
use Magento\Framework\Math\Random;
use Magento\Framework\Validator\Exception as ValidatorException;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Lumav\XmlFeed\Model\ResourceModel\Schedule\Management;


class Save extends Action
{

    const ADMIN_RESOURCE = 'Lumav_XmlFeed::xmlfeed';

	protected $resultPageFactory = false;
    /**
     * @var \Lumav\XmlFeed\Model\ResourceModel\Schedule\Management
     */
    protected $scheduleManager;

    public function __construct(
		Action\Context $context,
        XmlFeedRepository $xmlFeedRepository,
        Management $scheduleManager,
        Random $mathRandom,
        TimezoneInterface $timeZone,
        \Psr\Log\LoggerInterface $logger
	)
	{
		parent::__construct($context);
        $this->xmlFeedRepository = $xmlFeedRepository;
        $this->scheduleManager = $scheduleManager;
        $this->mathRandom = $mathRandom;
        $this->timeZone = $timeZone;
        $this->_logger = $logger;
    }

	public function execute()
	{

        $request = $this->getRequest()->getParams();
       
        $feedId = null;
        //Key doesnt excsit for new feed
        if(  array_key_exists( XmlFeedInterface::ENTITY_ID, $request) ){
            $feedId = $request[ XmlFeedInterface::ENTITY_ID ];
        }

        $this->_logger->info( print_r( $request, true ) ); 

        try {

            $this->scheduleManager->validateScheduleData($request);

            $model = $this->populateModel( $request );
            $this->xmlFeedRepository->save( $model );

            $this->_logger->info( "Feed saved"); 
            //Get new id for scheduler (when creating new feed)
            $feedId = $model->getEntityId();

            $this->scheduleManager->saveScheduleData( $feedId, $request);

            $this->messageManager->addSuccessMessage(__('You saved the feed.'));
        } catch (\Magento\Framework\Validator\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->_logger->error($e->getMessage()); 
            $this->_logger->error( $e->getTraceAsString() ); 
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            $this->_logger->error($e->getMessage()); 
            $this->_logger->error( $e->getTraceAsString() ); 
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        if ( $feedId ) {
            $resultRedirect->setPath(
                'xmlfeed/*/edit',
                ['id' => $feedId, '_current' => true]
            );
        } else {
            $resultRedirect->setPath(
                'xmlfeed/*/edit',
                ['_current' => true]
            );
        }

        return $resultRedirect;
    }
    
    private function populateModel( $request )
    {
        $dateNow = $this->formatDate('now');

        //New or update
        $model = $this->xmlFeedRepository->getNewModel();
        if(  array_key_exists( XmlFeedInterface::ENTITY_ID, $request) ){
            $model = $this->xmlFeedRepository->getById( (int)$request[ XmlFeedInterface::ENTITY_ID ] );
        }

        $data = $this->getSerializedData( $request[ XmlFeedInterface::IP_ADDRESSES ] );
        foreach( $data as $ip){
            if( !filter_var($ip, FILTER_VALIDATE_IP) ){
                throw new ValidatorException(__('IP %1 is invalid', $ip));
            }
        }
        $model->setIpAddresses($data);

        if( $request[ XmlFeedInterface::NAME ] ){
            $model->setName( $request[ XmlFeedInterface::NAME ] );
        }
        else{
            throw new ValidatorException(__('Please add name for feed'));
        }

        $model->setIsActive( $request[ XmlFeedInterface::IS_ACTIVE ] );
        
        //$model->setCustomerGroupId( $request[ XmlFeedInterface::CUSTOMER_GROUP_ID ] );
        //$model->setVatPercent( $request[ XmlFeedInterface::VAT_PERCENT ] );

        $data = $this->getSerializedData( $request[ XmlFeedInterface::IGNORED_CATEGORY_IDS ] );
        $model->setIgnoredCategoryIds($data);

        $data = $this->getSerializedData( $request[ XmlFeedInterface::IGNORED_PRODUCT_IDS ] );
        $model->setIgnoredProductIds($data);

        $model->setFeedType( $request[ XmlFeedInterface::FEED_TYPE ] );

        if( !$model->getEntityId() ){
            $data = $this->mathRandom->getUniqueHash();
            $model->setHash( $data );
            $model->setCreatedAt( $dateNow );
        }

        $model->setUpdatedAt( $dateNow );

        return $model;
    }

    private function getSerializedData( $data )
    {
        if( $data ){
            $data = str_replace(" ", "", $data);
            $data = explode(",", $data);
        }
        else{
            $data = [];
        }
        return $data;
    }

    /**
     * Formats current time to include timezone difference
     *
     * @param string|\DateTime $date
     * @param string|null $fromTimezone
     * @return string
     * @throws \Exception
     */
    public function formatDate($date, $fromTimezone = null)
    {
        if (!$fromTimezone) {
            $fromTimezone = $this->timeZone->getDefaultTimezone();
        }
        $toTimezone = $this->timeZone->getConfigTimezone();

        if ($date instanceof \DateTime) {
            $dateTime = $date;
        } else {
            $dateTime = new \DateTime($date, new \DateTimeZone($fromTimezone));
        }
        $dateTime->setTimezone(new \DateTimeZone($toTimezone));

        return $dateTime->format('Y-m-d\TH:i:s');
    }
}

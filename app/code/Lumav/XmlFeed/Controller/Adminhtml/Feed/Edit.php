<?php

namespace Lumav\XmlFeed\Controller\Adminhtml\Feed;

use \Magento\Backend\App\Action;
use \Magento\Framework\View\Result\PageFactory;

class Edit extends Action
{
    
    const ADMIN_RESOURCE = 'Lumav_XmlFeed::xmlfeed';

	protected $resultPageFactory = false;

	public function __construct(
		Action\Context $context,
		PageFactory $resultPageFactory
	)
	{
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}

	public function execute()
	{
		$resultPage = $this->resultPageFactory->create();
		$feedId = (int) $this->getRequest()->getParam('id');
		if( $feedId ){
			$resultPage->getConfig()->getTitle()->prepend((__('Edit Feed')));
		}
		else{
			$resultPage->getConfig()->getTitle()->prepend((__('New Feed')));
		}
		return $resultPage;
	}
}
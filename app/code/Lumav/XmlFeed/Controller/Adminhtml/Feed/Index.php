<?php

namespace Lumav\XmlFeed\Controller\Adminhtml\Feed;

use \Magento\Backend\App\Action;
use \Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    
    const ADMIN_RESOURCE = 'Lumav_XmlFeed::xmlfeed';

	protected $resultPageFactory = false;

	public function __construct(
		Action\Context $context,
		PageFactory $resultPageFactory
	)
	{
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}

	public function execute()
	{
		$resultPage = $this->resultPageFactory->create();
		$resultPage->getConfig()->getTitle()->prepend((__('Feeds')));

		return $resultPage;
	}
}
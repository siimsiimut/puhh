<?php
declare(strict_types=1);

namespace Lumav\XmlFeed\Helper;

use Magento\CatalogUrlRewrite\Model\CategoryUrlRewriteGenerator;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;

class CategoryHelper
{
    /**
     * @var array
     */
    protected $categoryTree;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;
    protected $categoryAttributes;
    /**
     * @var \Magento\UrlRewrite\Model\UrlFinderInterface
     */
    protected $urlFinder;
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $url;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\UrlRewrite\Model\UrlFinderInterface $urlFinder,
        \Magento\Framework\UrlInterface $url
    )
    {
        $this->resource = $resource;
        $this->urlFinder = $urlFinder;
        $this->url = $url;
    }

    /**
     * @param bool $forceReload
     * @return array
     */
    public function getCategoryTree(bool $forceReload = false): array
    {
        if (!$this->categoryTree || $forceReload) {
            $nameAttributeId = (int)$this->getCategoryAttributes()['name']['attribute_id'];
            $connection = $this->resource->getConnection();
            $categories = $connection->fetchAll(
                $connection->select()
                    ->from(['cce' => 'catalog_category_entity'], ['cce.entity_id', 'cce.parent_id'])
                    ->join(['ccev' => 'catalog_category_entity_varchar'],
                        'ccev.entity_id = cce.entity_id',
                        ['ccev.value'])
                    ->where('ccev.attribute_id = ?', $nameAttributeId)
            );
            // skip the first 2 categories(root category, default category)
            $this->categoryTree = $this->buildTree(
                $categories,
                2
            );
        }
        return $this->categoryTree;
    }

    /**
     * @param bool $forceReload
     * @return array
     */
    public function getCategoryAttributes(bool $forceReload = false): array
    {
        if (!$this->categoryAttributes || $forceReload) {
            $connection = $this->resource->getConnection();
            $attributes = $connection->fetchAll(
                $connection->select()
                    ->from('eav_attribute')
                    ->where('entity_type_id = 3')
            );
            foreach ($attributes as $row) {
                $this->categoryAttributes[$row['attribute_code']] = $row;
            }
        }

        return $this->categoryAttributes;
    }

    /**
     * @param array $elements
     * @param int $parentId
     * @return array
     * @see \Lumav\Directo\Model\Import\Handler\Product::buildTree()
     *
     */
    protected function buildTree(array $elements, int $parentId = 0): array
    {
        $branch = [];

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                unset($element['parent_id']);
                $children = $this->buildTree($elements, (int)$element['entity_id']);
                if ($children) {
                    $element += $children;
                }
                $value = strtolower($element['value']);
                unset($element['value']);
                $branch[$value] = $element;
            }
        }

        return $branch;
    }


    /**
     * @param $categoryId
     * @param $urlKey
     * @param int $storeId
     * @param null $categoryPath
     * @return string
     * @see \Magento\Catalog\Model\Category::getUrl
     */
    public function getUrl($categoryId, $urlKey, $categoryPath = null)
    {
        $rewrite = $this->urlFinder->findOneByData(
                [
                    UrlRewrite::ENTITY_ID => $categoryId,
                    UrlRewrite::ENTITY_TYPE => CategoryUrlRewriteGenerator::ENTITY_TYPE,
                ]
            );
        if($rewrite) {
            return $this->url->getDirectUrl($rewrite->getRequestPath());
        }
        return $this->url->getUrl('catalog/category/view', ['s' => $urlKey, 'id' => $categoryId]);
    }
}

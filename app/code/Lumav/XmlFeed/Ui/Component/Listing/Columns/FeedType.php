<?php


namespace Lumav\XmlFeed\Ui\Component\Listing\Columns;

use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;

use Lumav\XmlFeed\Model\Options\FeedOptions;

class FeedType extends Column
{
    
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = [],
        FeedOptions $feedOptions
    ) {
       
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->feedOptions = $feedOptions;
    }

    public function prepareDataSource(array $dataSource)
    {
        $options = $this->feedOptions->toOptionArray();
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
          
                foreach($options as $option){
                    if( $item['feed_type'] == $option["value"]){
                        $item[$this->getData('name')] = $option["label"];
                    }
                }
            }
        }

        return $dataSource;
    }
}

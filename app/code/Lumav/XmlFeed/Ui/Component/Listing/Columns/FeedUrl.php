<?php


namespace Lumav\XmlFeed\Ui\Component\Listing\Columns;

use Magento\Ui\Component\Listing\Columns\Column;
use Lumav\XmlFeed\Api\Data\XmlFeedInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\UrlInterface;


class FeedUrl extends Column
{
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = [],
        UrlInterface $urlInterface
    ) {
       
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->urlInterface = $urlInterface;
    }
    
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $url = $this->urlInterface->getBaseUrl();
            
            // echo '<pre>'; print_r($dataSource['data']['items']); die;
            foreach ($dataSource['data']['items'] as & $item) {            	  
                 if($item['feed_type']== 'stockupdate_xml')
	            {
	            	$url = 'https://www.toysoutlet.lv/';
	            }   
                    $item[$this->getData('name')] = $url. "feed?" . XmlFeedInterface::HASH . "=" . $item[ XmlFeedInterface::HASH ];
            }
        }

        return $dataSource;
    }
}

<?php
declare(strict_types=1);

namespace Lumav\XmlFeed\Api;

use Lumav\XmlFeed\Api\Data\ScheduleInterface;

/**
 * Interface ScheduleRepositoryInterface
 * @package Lumav\XmlFeed\Api
 */
interface ScheduleRepositoryInterface
{
    /**
     * @param ScheduleInterface $scheduleModel
     * @return ScheduleInterface
     */
    public function save(ScheduleInterface $scheduleModel);

    /**
     * @param int $id
     * @return ScheduleInterface
     */
    public function get(int $id);

    /**
     * @param ScheduleInterface $scheduleModel
     * @return bool
     */
    public function delete(ScheduleInterface $scheduleModel);

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id);

    /**
     * @param int $feedId
     * @return bool
     */
    public function deleteByFeedId(int $feedId);
}

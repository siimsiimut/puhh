<?php


namespace Lumav\XmlFeed\Api;


interface XmlFeedRepositoryInterface
{
    /**
     * @param \Lumav\XmlFeed\Api\Data\XmlFeedInterface $model
     * @return \Lumav\XmlFeed\Api\Data\XmlFeedInterface
     */
    public function save(\Lumav\XmlFeed\Api\Data\XmlFeedInterface $model);

    /**
     * @param int $id
     * @return \Lumav\XmlFeed\Api\Data\XmlFeedInterface
     */
    public function getById(int $id);

     /**
     * @param string $hash
     * @return \Lumav\XmlFeed\Api\Data\XmlFeedInterface
     */
    public function getByHash(string $hash);

    /**
     * @param \Lumav\XmlFeed\Api\Data\XmlFeedInterface $model
     * @return bool
     */
    public function delete(\Lumav\XmlFeed\Api\Data\XmlFeedInterface $model);

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Lumav\XmlFeed\Api\Data\XmlFeedInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * @return \Lumav\XmlFeed\Api\Data\XmlFeedInterface
     */
    public function getNewModel();
}

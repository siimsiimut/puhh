<?php
declare(strict_types = 1);

namespace Lumav\XmlFeed\Api\Data;

interface ScheduleInterface
{
    const ID = 'id';
    const FEED_ID = 'feed_id';
    const CRON_TIME = 'cron_time';
    const CRON_DAY = 'cron_day';
    const LAST_EXEC_TIME = 'last_execution_time';

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getFeedId();

    /**
     * @param int $feedId
     * @return $this
     */
    public function setFeedId(int $feedId);

    /**
     * @return int
     */
    public function getCronTime();

    /**
     * @param int $cronTime
     * @return $this
     */
    public function setCronTime(int $cronTime);

    /**
     * @return int
     */
    public function getCronDay();

    /**
     * @param int $cronDay
     * @return $this
     */
    public function setCronDay(int $cronDay);

    /**
     * @return string
     */
    public function getLastExecTime();

    /**
     * @param string $lastExecTime
     * @return $this
     */
    public function setLastExecTime(string $lastExecTime);
}

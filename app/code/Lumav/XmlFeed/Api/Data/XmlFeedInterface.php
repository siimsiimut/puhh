<?php


namespace Lumav\XmlFeed\Api\Data;


interface XmlFeedInterface
{
    const ENTITY_ID = 'entity_id';
    const NAME = 'name';
    const HASH = 'hash';
    const IP_ADDRESSES = 'ip_addresses';
    const CUSTOMER_GROUP_ID = 'customer_group_id';
    const VAT_PERCENT = 'vat_percent';
    const IGNORED_CATEGORY_IDS = 'ignored_category_ids';
    const IGNORED_PRODUCT_IDS = 'ignored_product_ids';
    const IS_ACTIVE = 'is_active';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const FEED_TYPE = 'feed_type';

    /**
     * @param int $entity_id
     * @return $this
     */
    public function setEntityId($entity_id);

    /**
     * @return int
     */
    public function getEntityId();

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * @return string|null
     */
    public function getName();

    /**
     * @param string $hash
     * @return $this
     */
    public function setHash($hash);

    /**
     * @return string
     */
    public function getHash();

    /**
     * @param $ipAddresses
     * @return $this
     */
    public function setIpAddresses($ipAddresses);

    /**
     * @return array|null
     */
    public function getIpAddresses();

    /**
     * @param int $customerGroupId
     * @return $this
     */
    public function setCustomerGroupId($customerGroupId);

    /**
     * @return int
     */
    public function getCustomerGroupId();

     /**
     * @param $vatPercent
     * @return $this
     */
    public function setVatPercent($vatPercent);

    /**
     * @return string
     */
    public function getVatPercent();

    /**
     * @param array $ignoredCategoryIds
     * @return $this
     */
    public function setIgnoredCategoryIds($ignoredCategoryIds);

    /**
     * @return array
     */
    public function getIgnoredCategoryIds();

    /**
     * @param array $ignoredProductIds
     * @return $this
     */
    public function setIgnoredProductIds($ignoredProductIds);

    /**
     * @return array
     */
    public function getIgnoredProductIds();

    /**
     * @param bool $isActive
     * @return $this
     */
    public function setIsActive($isActive);

    /**
     * @return bool
     */
    public function getIsActive();

    /**
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt);

    /**
     * @return string
     */
    public function getUpdatedAt();

    /**
     * @param int $feedType
     * @return $this
     */
    public function setFeedType($feedType);

    /**
     * @return int
     */
    public function getFeedType();
}

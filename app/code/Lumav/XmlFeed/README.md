xml_feed_entity {
	id,
	name,
	hash,
	ip_addresses,
	pricerule,
	group_price_id,
	vat_percent,
	ignore_category_ids,
	ignore_product_ids,
	is_active,
	created_at,
	updated_at,
	last_generation_at,
	generation_interval
}

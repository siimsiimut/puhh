<?php
namespace Makesyoulocal\MakesyoulocalAPI\Controller\Api;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

class Index extends \Magento\Framework\App\Action\Action
{
    const MODULE_NAME = 'Makesyoulocal_MakesyoulocalAPI';

    protected $resultPageFactory;
    protected $resultForwardFactory;
    protected $helper;
    protected $storeManager;
    protected $scopeConfigInterface;
    protected $moduleList;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ForwardFactory $resultForwardFactory,
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfigInterface,
        ModuleListInterface $moduleList,
        WriterInterface $configWriter,
        \Makesyoulocal\MakesyoulocalAPI\Helper\Data $helper
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->helper = $helper;
        $this->storeManager = $storeManager;
        $this->scopeConfigInterface = $scopeConfigInterface;
        $this->moduleList = $moduleList;
        $this->configWriter = $configWriter;

        parent::__construct($context);
    }

    public function dispatch(RequestInterface $request)
    {
        if (!$this->helper->isEnabled()) {
            throw new NotFoundException(__('Page not found.'));
        }
        return parent::dispatch($request);
    }

    public function execute()
    {
        /* ini_set('display_errors', '1'); */
        /* error_reporting(E_ALL); */
        /* define('GOA_DEBUG', false); */
        /* define('GOA_CHECK', true); */
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $this->configWriter->save('makesyoulocalAPI/general/apiUrl', 'https://client.makesyoulocal.com/api/exchange_tokens');
        $this->configWriter->save('makesyoulocalAPI/general/webhooksUrl', 'https://client.makesyoulocal.com/api/v1/magento2');
        $this->configWriter->save('makesyoulocalAPI/general/apiPublicKey', '637b6161a3c3cce08eb094b84b31ff575f3b0a0c7e92d4daff41a97af797666d');

        if ($this->scopeConfigInterface->getValue('makesyoulocalAPI/general/shopPublicKey') === null) {

            $keyPair = \GOA\Makesyoulocal\Common\Utils\AsymetricEncryption::createKeyPair();
            $this->configWriter->save('makesyoulocalAPI/general/shopPrivateKey', $keyPair[0]);
            $this->configWriter->save('makesyoulocalAPI/general/shopPublicKey', $keyPair[1]);
        }

        if (!class_exists('\GOA\Makesyoulocal\Magento2\Router')) {
            require_once(__DIR__ . '/../../vendor/autoload.php');
        }

        $localeResolver = $objectManager->get('Magento\Framework\Locale\Resolver');
        $locale  = $localeResolver->getLocale();
        list($lang, $country) = explode('_', $locale);

        $settings = new \GOA\Makesyoulocal\Magento2\Settings(array(
            'namespace' => 'Magento2',
            'country' => $country,
            'language' => $lang,
            'currency' => $this->storeManager->getStore()->getCurrentCurrencyCode(),
            'supportedCurrencies' => $this->storeManager->getStore()->getAvailableCurrencyCodes(),
            'moduleUrlPrefix' => 'makesyoulocal',
            'moduleCodeName' => 'makesyoulocal',
            'version' => $this->moduleList->getOne(self::MODULE_NAME)['setup_version'],
            'apiUrl' => $this->scopeConfigInterface->getValue('makesyoulocalAPI/general/apiUrl'),
            'storeId' => $this->storeManager->getStore()->getId(),
            'password' => $this->scopeConfigInterface->getValue('makesyoulocalAPI/general/password'),
            'webhooksUrl' => $this->scopeConfigInterface->getValue('makesyoulocalAPI/general/webhooksUrl'),
            'apiPublicKey' => $this->scopeConfigInterface->getValue('makesyoulocalAPI/general/apiPublicKey'),
            'shopPrivateKey' => $this->scopeConfigInterface->getValue('makesyoulocalAPI/general/shopPrivateKey'),
            'shopPublicKey' => $this->scopeConfigInterface->getValue('makesyoulocalAPI/general/shopPublicKey'),
        ));

        # Hack to make the matching rules work
        $requestedUrl = str_replace('/api/index/', '/api/', $_SERVER['REQUEST_URI']);
        $requestedUrl = str_replace('/api/execute/', '/api/', $requestedUrl);

        $shopApi = new \GOA\Makesyoulocal\Common\ShopApi();

        $router = \GOA\Makesyoulocal\Magento2\Router::create($settings, $requestedUrl, $shopApi);
        $response = $router->dispatch();

        if (!$response) {
            die('error');
        }

        $response->printResponse();
    }
}

<?php
namespace Makesyoulocal\MakesyoulocalAPI\Controller\Export;

use Magento\Framework\App\Action\Action;

class Trigger extends Action
{
    protected $observer;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Makesyoulocal\MakesyoulocalAPI\Model\Observer $observer
    )
    {
        $this->context = $context;
        $this->observer = $observer;

        parent::__construct($context);
    }

    public function execute()
    {
        $this->observer->triggerWebhooks();
        die();
    }
}

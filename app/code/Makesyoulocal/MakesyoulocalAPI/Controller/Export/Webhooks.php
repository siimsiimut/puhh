<?php
namespace Makesyoulocal\MakesyoulocalAPI\Controller\Export;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Area;

class Webhooks extends Action
{
    const MODULE_NAME = 'Makesyoulocal_MakesyoulocalAPI';

    protected $observer;
    protected $emulation;
    protected $scopeConfigInterface;
    protected $moduleList;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\App\Emulation $emulation,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface,
        \Makesyoulocal\MakesyoulocalAPI\Model\Observer $observer,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->context = $context;
        $this->observer = $observer;
        $this->emulation = $emulation;
        $this->scopeConfigInterface = $scopeConfigInterface;
        $this->moduleList = $moduleList;
        $this->storeManager = $storeManager;

        parent::__construct($context);
    }

    public function execute()
    {
        $initialEnvironmentInfo = false;
        $storeId = null;

        if (isset($_GET['___store'])) {
            $storeCode = $_GET['___store'];
            $stores = $this->storeManager->getStores(true, true);

            if ($storeCode === 'admin' || !isset($stores[$storeCode])) {
                die("<ERROR>Invalid store code</ERROR>");
            }

            $storeId = $stores[$storeCode]->getStoreId();
            $this->emulation->startEnvironmentEmulation(
                $storeId,
                Area::AREA_FRONTEND
            );

            $initialEnvironmentInfo = true;
        }

        require_once(__DIR__ . '/../../../lib/GOA/autoload.php');

        $requestedUrl = $_SERVER['REQUEST_URI'];
        $settings = new \GOA\Makesyoulocal\Magento2\Settings(array(
            'namespace' => 'Magento2',
            'moduleUrlPrefix' => 'makesyoulocal',
            'moduleCodeName' => 'makesyoulocal',
            'version' => $this->moduleList->getOne(self::MODULE_NAME)['setup_version'],
            'currency' => $this->storeManager->getStore()->getCurrentCurrencyCode(),
            'apiUrl' => 'https://client.makesyoulocal.com/api/exchange_tokens',
            'password' => $this->scopeConfigInterface->getValue('makesyoulocalAPI/general/password'),
            'contentEncryptionKey' => $this->scopeConfigInterface->getValue('makesyoulocalAPI/general/content_encryption_key'),
            'contentEncryptionIv' => $this->scopeConfigInterface->getValue('makesyoulocalAPI/general/content_encryption_iv')
        ));

        try {
            $router = new \GOA\Makesyoulocal\Magento2\Router($settings, $requestedUrl);
        } catch (\Exception $ex) {
            $router->getResponse()->error($ex);
            return $router->getResponse()->printResponse();
        }

        $router->dispatch();

        if ($initialEnvironmentInfo) {
            $this->emulation->stopEnvironmentEmulation();
        }
        $router->getResponse()->printResponse();
    }
}

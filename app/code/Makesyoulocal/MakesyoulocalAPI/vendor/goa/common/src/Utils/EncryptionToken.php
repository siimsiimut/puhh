<?php
namespace GOA\Makesyoulocal\Common\Utils;

class EncryptionToken
{
    private $iv;
    private $key;

    public function __construct($key, $iv)
    {
        $this->key = $key;
        $this->iv = $iv;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getIv()
    {
        return $this->iv;
    }

    public function encrypt($data)
    {
        return openssl_encrypt($data, 'aes-256-cbc', hex2bin($this->key), 0, hex2bin($this->iv));
    }

    public function decrypt($data)
    {
        return openssl_decrypt($data, 'aes-256-cbc', hex2bin($this->key), 0, hex2bin($this->iv));
    }
}

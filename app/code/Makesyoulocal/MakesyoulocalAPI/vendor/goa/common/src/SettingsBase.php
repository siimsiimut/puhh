<?php
namespace GOA\Makesyoulocal\Common;

class SettingsBase
{
    protected $namespace;
    protected $password;
    protected $moduleUrlPrefix;
    protected $moduleCodeName;
    protected $apiUrl;
    protected $contentEncryptionKey;
    protected $contentEncryptionIv;
    protected $webhooksTokens;
    protected $webhooksUrl;
    protected $shopPublicKey;
    protected $shopPrivateKey;
    protected $apiPublicKey;
    protected $supportedCurrencies;
    protected $language;
    protected $country;

    protected $productsFields;
    protected $ordersFields;

    public function __construct($options)
    {
        $this->productsFields = array();
        $this->ordersFields = array();

        foreach ($options as $key => $value) {
            $this->$key = $value;
        }
    }

    public function getProductsFields()
    {
        return $this->productsFields;
    }

    public function getOrdersFields()
    {
        return $this->ordersFields;
    }

    public function getNamespace()
    {
        return $this->namespace;
    }

    public function getApiUrl()
    {
        if (!$this->apiUrl) {
            throw new \Exception('Module api url not set');
        }

        return $this->apiUrl;
    }

    public function getModuleUrlPrefix()
    {
        if (!$this->moduleUrlPrefix) {
            throw new \Exception('Module url prefix not set');
        }

        return $this->moduleUrlPrefix;
    }

    public function getModuleCodeName()
    {
        if (!$this->moduleCodeName) {
            throw new \Exception('Module code name not set');
        }

        return $this->moduleCodeName;
    }

    public function getIsEncryptionPossible()
    {
        $phpVersion = Utils\PHPVersion::get();
        return $phpVersion >= 50400
            && function_exists('openssl_encrypt')
            && function_exists('hex2bin');
    }

    public function getIsFopenAvailable()
    {
        return !(!ini_get('allow_url_fopen')
            || ini_get('allow_url_fopen') === 'false'
            || ini_get('allow_url_fopen') === '0');
    }
    public function getPhpVersion()
    {
        return PHPVersion::get();
    }

    public function getPassword()
    {
        if (!$this->password) {
            throw new \Exception('Module password setting not set');
        }

        return $this->password;
    }

    public function getVersion()
    {
        throw new \Exception('Module version setting not implemented');
    }

    public function getSoftwareVersion()
    {
        throw new \Exception('Software version setting not implemented');
    }

    public function getWebhooksUrl()
    {
        if (!$this->webhooksUrl) {
            throw new \Exception('Webhooks url not implemented');
        }
        return $this->webhooksUrl;
    }

    public function getStoreId()
    {
        return 'main';
    }

    public function getSupportedCurrencies() {
        if (!$this->supportedCurrencies) {
            throw new \Exception('Supported curriencies not implemented');
        }
        return $this->supportedCurrencies;
    }

    public function getWebhooksTokens($store = null)
    {
        if ($store === null) {
            $store = $this->getStoreId();
        }

        if (!$this->webhooksTokens || !isset($this->webhooksTokens[$store])) {
            return array();
        }
        return array_unique($this->webhooksTokens[$store]);
    }

    public function getContentEncryptionKey()
    {
        if (!$this->moduleUrlPrefix) {
            throw new \Exception('Module content encryption key not set');
        }
        return $this->contentEncryptionKey;
    }

    public function getApiPublicKey()
    {
        if (!$this->apiPublicKey) {
            throw new \Exception('Module public key not set');
        }
        return $this->apiPublicKey;
    }

    public function getShopPrivateKey()
    {
        return $this->shopPrivateKey;
    }

    public function getShopPublicKey()
    {
        return $this->shopPublicKey;
    }

    public function getContentEncryptionIv()
    {
        if (!$this->moduleUrlPrefix) {
            throw new \Exception('Module content encryption iv not set');
        }
        return $this->contentEncryptionIv;
    }

    public function getLanguage()
    {
        if (!$this->language) {
            throw new \Exception('Language is not set');
        }
        return $this->language;
    }

    public function getCountry()
    {
        if (!$this->country) {
            throw new \Exception('Country is not set');
        }
        return $this->country;
    }
}

<?php

namespace GOA\Makesyoulocal\Common\Webhooks;

abstract class Webhooks {
    protected $storage;
    protected $settings;
    protected $lastError;

    public function __construct($settings, Storage $storage)
    {
        $this->storage = $storage;
        $this->settings = $settings;
    }

    public function queueProduct($identifier, $payload, $storeId = 1)
    {
        $this->storage->addProduct($identifier, $payload, $storeId);
    }

    public function queueOrder($identifier, $payload, $storeId = 1)
    {
        $this->storage->addOrder($identifier, $payload, $storeId);
    }

    protected function onSuccesfulSubmit($type, $tokensToRemove, $storeId = 1)
    {
        foreach ($tokensToRemove as $key) {
            $this->storage->removeToken($storeId, $key);
        }
    }

    protected function onFailureSubmit($type, $lastError)
    {
        $this->lastError = $lastError;
    }

    public function getLastError()
    {
        return $this->lastError;
    }

    abstract protected function prepareOrdersPayload($ordersIds, $storeId);

    abstract protected function prepareProductsPayload($productIds, $storeId);

    public function submitProducts($storeId)
    {
        $productsData = $this->storage->getAllProducts($storeId);
        $payload = $this->prepareProductsPayload($productsData, $storeId);
        $this->storage->removeAllProducts($storeId);
        $this->submitRequest($payload, 'products', $storeId);
    }

    public function submitOrders($storeId)
    {
        $ordersData = $this->storage->getAllOrders($storeId);
        $payload = $this->prepareOrdersPayload($ordersData, $storeId);
        $this->storage->removeAllOrders($storeId);
        $this->submitRequest($payload, 'orders', $storeId);
    }

    private function submitRequest($payload, $type, $storeId)
    {
        $url = $this->settings->getWebhooksUrl() . '/' .$type;
        $curl = curl_init($url);

        $timestamp = \date(\DateTime::ISO8601);
        $payload = \GOA\Makesyoulocal\Common\Response::parseDataAsJson($payload);
        $payload = array(
            'header' => array(
                'version' => $this->settings->getVersion(),
                'timestamp' => $timestamp
            ),
            $type => $payload
        );

        $encodedPayload = json_encode($payload);

        $signatures = array();
        foreach ($this->settings->getWebhooksTokens() as $token) {
            $payloadSignature = \GOA\Makesyoulocal\Common\Utils\AsymetricEncryption::sign(
                $this->settings->getShopPrivateKey(),
                $token . $encodedPayload
            );
            array_push($signatures, $payloadSignature);
        }



        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $encodedPayload);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type:application/json',
            'Authorization: Basic ' . base64_encode(implode(',', $this->storage->getTokens($storeId))),
            'shop-signatures: ' . implode(',', $signatures),
            'timestamp: ' . $timestamp
        ));

        $curlResponse = curl_exec($curl);

        $isError = false;
        $curlErrno = curl_errno($curl);
        if($curlErrno) {
             $isError = true;
        } elseif ($curlResponse === false) {
            $isError = true;
        }

        $httpStatus = (int)curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        $decoded = array();
        try {
            $decoded = json_decode($curlResponse, true);
        } catch(\Exception $err) {
            // Ignore
        }

        $jsonDecodingResult = json_last_error_msg();
        if ($jsonDecodingResult !== JSON_ERROR_NONE) {
            $isError = true;
        }

        if ($httpStatus >= 400) {
            $isError = true;

        }

        if ($isError) {
            return $this->onFailureSubmit($type, array(
                'jsonDecodingResult' => $jsonDecodingResult,
                'curlResponse' => $curlResponse,
                'curlErrorNumber' => $curlErrno
            ));
        }

        $tokensToRemove = array();
        if (is_array($decoded)) {
            foreach ($decoded as $key => $value) {
                $hasError = $value && isset($value['error']);
                $shouldRemoveToken = $hasError && $value['error'] === 'inactive';
                if ($shouldRemoveToken) {
                    array_push($tokensToRemove, $key);
                }
            }
        }
        return $this->onSuccesfulSubmit($type, $tokensToRemove);
    }
}

<?php
namespace GOA\Makesyoulocal\Common\Model;

class ValidationError extends \Exception {
    public function __construct($value, $field = null)
    {
        $this->collection = [];
        $this->value = $value;
        $this->field = $field;
    }

    public function setField($field) {
        $this->field = $field;
    }

    public function merge(ValidationError $error) {
        array_push($this->collection, $error);
    }

    public function hasErrors()
    {
        return count($this->collection);
    }

    public function formatValue()
    {
        $fieldName = "";
        if (is_array($this->field)) {
            $fieldName = implode(' or ', $this->field);
        } else {
            $fieldName = $this->field;
        }
        return sprintf($this->value, $fieldName);
    }

    public function print() {
        $items = array(
            'msg' => $this->formatValue()
        );

        if (count($this->collection)) {
            $items['children'] = array_map(function($item) {
                return $item->print();
            }, $this->collection);
        }
        return $items;
    }
}

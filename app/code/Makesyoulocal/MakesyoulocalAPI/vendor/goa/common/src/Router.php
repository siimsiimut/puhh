<?php
namespace GOA\Makesyoulocal\Common;

abstract class Router
{
    public static $CONTROLLER_API = 'Api';
    public static $ACTION_API_EXECUTE = 'Execute';
    public static $CONTROLLER_BACKWARD_SUPPORT = 'Export';

    protected $shopApi;
    protected $response;
    protected $settings;
    protected $requestedUrl;
    protected $isGoaDebugging;

    abstract protected function isAdministrator();
    abstract protected function getAvailableActions();

    public function __construct($response, $settings, $requestedUrl, $shopApi)
    {
        $this->requestedUrl = $requestedUrl;
        $this->response = $response;
        $this->settings = $settings;
        $this->shopApi = $shopApi;
        $this->isGoaDebugging = defined('GOA_DEBUG') && GOA_DEBUG === true;
    }

    public static function create($settings, $requestedUrl, $shopApi) {
        $response = new \GOA\Makesyoulocal\Common\Response();
        return new static($response, $settings, $requestedUrl, $shopApi);
    }

    private function getAllHeaders() {
        if (function_exists('getallheaders')) {
            return getallheaders();
        }
        $headers = [];
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }

    public function createRequestObject()
    {
        list($pathQuery, $urlQuery) = strpos($this->requestedUrl, '?') === false ?
            array($this->requestedUrl, '') : explode('?', $this->requestedUrl);

        $pathParams = $this->parseParamsFromPath($pathQuery);
        $urlParams = $this->parseParamsFromQuery($urlQuery);
        $headerParams = $this->parseParamsFromHeaders($this->getAllHeaders());

        $postBody = null;

        if (isset($_SERVER['CONTENT_TYPE']) && $_SERVER['CONTENT_TYPE'] === 'application/json') {
            $body = file_get_contents('php://input');
            $postBody = @json_decode($body, true);
        }

        if (isset($_SERVER['HTTP_GOABODY'])) {
            $postBody = @json_decode($_SERVER['HTTP_GOABODY'], true);
        }

        $params = new UrlParams(array_merge(array(),
            $pathParams,
            $urlParams,
            $headerParams,
            array(
            'requested_url' => $this->requestedUrl,
            'is_administrator' => $this->isAdministrator()
        )));

        return new Request($params, $postBody);
    }


    private function parseParamsFromPath($pathQuery)
    {
        $matches = array();
        $regex = '/^.*' . $this->settings->getModuleUrlPrefix() . '\/([^\?\&]+)/';

        if (!preg_match_all($regex, $pathQuery, $matches)) {
            return array();
        }

        $path = count($matches) > 1 && count($matches[1]) > 0 ? $matches[1][0] : null;

        $jsonMatches = array();
        $decoded = urldecode($path);
        preg_match_all("/(\{(((?>[^{}]+)|(?1))*)\})/im", $decoded, $jsonMatches);

        $decodedParts = array();
        $decodedRest = $decoded;
        if (count($jsonMatches) > 0 && count($jsonMatches[0]) > 0) {
            foreach ($jsonMatches[0] as $jsonMatch) {
                $r = explode($jsonMatch, $decodedRest);

                $decodedRest = $r[1];
                array_push($decodedParts, $r[0]);
                array_push($decodedParts, array($jsonMatch));
            }
        }

        if (!count($decodedParts)) {
            $decodedParts = array($decoded);
        }

        $pathElements = array();
        foreach ($decodedParts as $decodedPart) {
            if (is_array($decodedPart)) {
                array_push($pathElements, $decodedPart[0]);
            } else {
                $pathElements = array_merge($pathElements, explode('/', rtrim($decodedPart, '/')));
            }
        }


        if (count($pathElements) % 2 !== 0) {
            return array();
        }

        return array_reduce($pathElements, function ($carry, $next) {
            $lastElement = end($carry);
            if ($lastElement !== null) {
                $carry[$next] = null;
                return $carry;
            }
            $carry[key($carry)] = $next;
            return $carry;
        }, array());
    }

    private function parseParamsFromQuery($urlQuery)
    {
        $urlParams = null;
        parse_str($urlQuery, $urlParams);
        return $urlParams;
    }

    private function parseParamsFromHeaders($headers)
    {

        $result = array();
        foreach ($headers as $key => $value) {
            if (strtolower($key) === 'api-key') {
                $result['key'] = $value;
            }
            if (strtolower($key) === 'exchange-token') {
                $result['exchange_token'] = $value;
            }
        }
        return $result;
    }

    public function dispatch()
    {
        $request = $this->createRequestObject();

        $this->response->setEncryptionIsPossible($this->settings->getIsEncryptionPossible());

        $result = null;
        try {
            if (!$this->isGoaDebugging) {
                $this->checkRequest($request);
            }
            if ($request->getControllerName() === self::$CONTROLLER_API) {
                if ($request->getActionName() === self::$ACTION_API_EXECUTE) {
                    $this->enhanceMagicRequest($request);
                } else if (!$this->isGoaDebugging) {
                    throw new \Exception('The action name cannot be set directly');
                } else if ($request->postBody) {
                    $request->setBody($request->postBody);
                } else {
                    $request->setBody($request->urlParams->body);
                }

                if (in_array($request->getActionName(), $this->getAvailableActions())) {
                    $actionNamespace = str_replace("\\Common", "\\" . $this->settings->getNamespace(), __NAMESPACE__) . "\\Actions";
                    $ActionClass = $actionNamespace . "\\" . $request->getActionName();

                    $api = new $ActionClass($this->settings, $this->shopApi);
                    $result = $api->executeWithResult($request);
                }
            }

            if (!$result) {
                throw new \Exception('The module did not return any data');
            } else {
                $this->response->set($result);
            }

        } catch (\GOA\Makesyoulocal\Common\Model\ValidationError $ex) {
            $this->response->error($ex);
        } catch (\Exception $ex) {
            if (!$this->isGoaDebugging) {
                $this->response->error($ex);
            } else {
                throw $ex;
            }
        }

        return $this->response;
    }

    protected function checkRequest($request)
    {
        $basicKeyCorrect = $request->urlParams->key === md5($this->settings->getPassword());
        if (!$basicKeyCorrect) {
            throw new \Exception('Access denied');
        }
    }

    private function enhanceRequestWithEncryptionToken($request)
    {
        if (!$request->getExchangeToken() && !$this->isGoaDebugging) {
            throw new \Exception('Exchange token is missing from the request');
        } elseif ($this->isGoaDebugging) {
            return;
        }

        $mainEncryption = new Utils\MainEncryption($this->settings);
        $encryptionToken = $mainEncryption->getEncryptionToken($request->getExchangeToken());
        if (!($encryptionToken->getKey() && $encryptionToken->getIv())) {
            throw new \Exception('Couldn\'t get encryption key');
        }
        $request->setEncryptionToken($encryptionToken);
    }

    private function enhanceMagicRequest($request)
    {
        $isError = false;

        if ($request->getExchangeToken() === null) {
            throw new \Exception('Exchange token must be defined');
        }

        $url = $this->settings->getApiUrl() . '/' . $request->getExchangeToken();
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'shop-signature: ' . \GOA\Makesyoulocal\Common\Utils\AsymetricEncryption::sign(
                $this->settings->getShopPrivateKey(),
                $request->getExchangeToken()
            ),
            'Content-Type: application/json'
        ));

        $curlResponse = curl_exec($curl);

        if  (curl_errno($curl)) {
            $isError = true;
        }

        curl_close($curl);

        $decoded = array();
        if (!$isError)
        {
            try {
                $curlResponse = $curlResponse; // Decrypt the data using temporaryKey
                $decoded = json_decode($curlResponse, true);
            } catch(\Exception $err) {
                $isError = true;
            }
        }

        if ($isError) {
            throw new \Exception('Could not read the request data');
        }

        $isSignatureValid = false;
        if (isset($decoded['signature'])) {
            $signature = $decoded['signature'];

            $isSignatureValid = \GOA\Makesyoulocal\Common\Utils\AsymetricEncryption::verify(
                $this->settings->getApiPublicKey(),
                $signature,
                $request->getExchangeToken()
            );
        }

        if (!$isSignatureValid) {
            throw new \Exception('API Signature is invalid. Could not receive request details');
        }

        $payload = array();
        if (isset($decoded['store_id'])) {
            $payload['context_store_id'] = $decoded['store_id'];
        }

        if (isset($decoded['payload'])) {
            $payload = array_merge($payload, $decoded['payload']);
        }

        $request->setBody($payload);
        $request->setActionName($decoded['action']);
    }

    public function getResponse()
    {
        return $this->responseHelper;
    }
}


<?php
namespace GOA\Makesyoulocal\Common\Utils;

class SimpleXMLCdata extends \SimpleXMLElement
{
    public function addCData($cdata_text)
    {
        if ($cdata_text && strlen($cdata_text)) {
            $node = dom_import_simplexml($this);
            $no = $node->ownerDocument;
            $node->appendChild($no->createCDATASection($cdata_text));
        }
    }
}

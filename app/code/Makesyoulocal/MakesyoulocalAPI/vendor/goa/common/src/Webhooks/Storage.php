<?php

namespace GOA\Makesyoulocal\Common\Webhooks;

abstract class Storage {
    public static $TYPE_PRODUCTS = 'products';
    public static $TYPE_ORDERS = 'orders';

    protected $settings;

    public function __construct($settings) {
        $this->settings = $settings;
    }

    abstract protected function getAll($type, $storeId);
    abstract protected function removeAll($type, $storeId);
    abstract protected function add($type, $identifier, $item, $storeId);
    abstract public function removeToken($store, $token);
    abstract public function addToken($store, $token);
    abstract public function replaceTokens($store, $token);
    abstract public function getTokens($store);

    public function getAllProducts($storeId)
    {
        return $this->getAll(self::$TYPE_PRODUCTS, $storeId);
    }

    public function getAllOrders($storeId)
    {
        return $this->getAll(self::$TYPE_ORDERS, $storeId);
    }

    public function removeAllProducts($storeId)
    {
        return $this->removeAll(self::$TYPE_PRODUCTS, $storeId);
    }

    public function removeAllOrders($storeId)
    {
        return $this->removeAll(self::$TYPE_ORDERS, $storeId);
    }

    public function addProduct($identifier, $payload, $storeId)
    {
        $this->add(self::$TYPE_PRODUCTS, $identifier, $payload, $storeId);
    }

    public function addOrder($identifier, $payload, $storeId)
    {
        $this->add(self::$TYPE_ORDERS, $identifier, $payload, $storeId);
    }
}

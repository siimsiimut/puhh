<?php
namespace GOA\Makesyoulocal\Common\Api;

class Webhooks extends Base
{
    public static $HOOK_NAME_ORDERS = 'orders';
    public static $HOOK_NAME_PRODUCTS = 'products';

    protected $rootName = 'WEBHOOKS';
    protected $sku = null;
    protected $webhooksToken = null;

    public function __construct($params, $settings)
    {
        parent::__construct($params, $settings);

        $this->webhooksToken = $params->token;
        $this->replaceTokens = $params->replace;
    }
}

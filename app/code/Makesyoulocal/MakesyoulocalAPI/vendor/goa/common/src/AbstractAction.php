<?php
namespace GOA\Makesyoulocal\Common;

abstract class AbstractAction
{
    protected $shopApi;
    protected $settings;

    public function __construct($settings, $shopApi = null)
    {
       $this->settings = $settings;
       $this->shopApi = $shopApi;
    }

    protected static function parsePayload($payload) {
        return array();
    }

    protected function execute($payload, $request)
    {
        throw new \Exception('Not implemented');
    }

    final public function executeWithResult($request)
    {
        return $this->executeWithPayload($request->getBody(), $request);
    }

    final public function executeWithPayload($rawPayload, $request) {
        $payload = $this->processPayload($rawPayload);

        $executeResult = $this->execute($payload, $request);

        if (!isset($executeResult['header'])) {
            $executeResult = array(
                'header' => array(
                    'version' => $this->settings->getVersion()
                ),
                'details' => $executeResult
            );
        }

        return $executeResult;

    }

    public function processPayload($payload)
    {
        $payloadParsers = static::parsePayload($payload);
        $payloadValidator = is_array($payloadParsers)
            ? \GOA\Makesyoulocal\Common\Validators::ObjectOf($payloadParsers)
            : $payloadParsers;

        $result = $payloadValidator($payload, $this->settings, $payload);

        if ($result instanceof \GOA\Makesyoulocal\Common\Model\ValidationError) {
            $result->setField('root');
            throw $result;
        }
        if (!($result instanceof \GOA\Makesyoulocal\Common\Model\ValueModified)) {
            throw new \Exception("Validation error: \n" . implode(",\n", $result));
        }

        return $result->value;
    }
}


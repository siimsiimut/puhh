<?php
namespace GOA\Makesyoulocal\Common\Api;

class Products extends Base
{
    protected $rootName = 'PRODUCTS';
    protected $sku = null;
    protected $productGetter;

    public function __construct($params, $settings)
    {
        parent::__construct($params, $settings);

        $this->sku = $params->sku;
        $this->from = $params->updated_since;
    }

    protected function prepareRow($productId)
    {
        return $this->productGetter->getById($productId);
    }
}

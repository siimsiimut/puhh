<?php
namespace GOA\Makesyoulocal\Common;

function isAssoc(array $arr)
{
    if (array() === $arr) {
        return false;
    }
    return array_keys($arr) !== range(0, count($arr) - 1);
}

class Response
{
    private $data;
    private $encryptionToken = null;
    private $encryptResults = true;

    public function set($data)
    {
        $this->data = $data;
        return $this;
    }

    public function setEncryptionIsPossible($encryptResults)
    {
        $this->encryptResults = $encryptResults;
    }

    public function setExportAsXml($exportAsXml)
    {
        $this->exportAsXml = $exportAsXml;
    }

    public function setEncryptionToken($encryptionToken)
    {
        $this->encryptionToken = $encryptionToken;
    }

    public function error($error)
    {
        if ($error instanceof \GOA\Makesyoulocal\Common\Model\ValidationError) {
            $this->data = array(
                'ERROR' => $error->print()
            );
        } else {
            $this->data = array(
                'ERROR' => $error->getMessage(),
                'TRACE' => $error->getTraceAsString()
            );
        }

        return $this;
    }

    private function getContentType()
    {
        if ($this->encryptionToken && $this->encryptResults) {
            return 'text/plain';
        }

        return 'application/json';
    }

    public function printResponse()
    {
        if (!$this->encryptionToken || !$this->encryptResults) {
            if (isset($this->data['TRACE'])) {
                unset($this->data['TRACE']);
            }
        }

        header('Content-Type: ' . $this->getContentType());

        $dataAsString = $this->getDataAsJson();

        if ((
            !$this->encryptionToken
            || ($this->encryptionToken && !$this->encryptionToken->getIv() && defined('GOA_DEBUG') && GOA_DEBUG === true)
        ) || !$this->encryptResults) {
            echo $dataAsString;
            die();
        }

        echo $this->encryptionToken->encrypt($dataAsString);
        die();
    }

    private function getDataAsJson()
    {
        $data = $this->data;
        return json_encode(self::parseDataAsJson($data), JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
    }

    public static function parseDataAsJson($data)
    {
        $newData = array();

        foreach ($data as $key => $value) {
            if (strpos($key, '__') === 0) {
                continue;
            }

            if ($value && $value instanceof \GOA\Makesyoulocal\Common\Model\DatetimeFormatter) {
                $newData[strtolower($key)] = $value->getValue();
            } else if ($value && $value instanceof \GOA\Makesyoulocal\Common\Model\Currency) {
                $newData[strtolower($key)] = $value->getValue();
            } else if ($value && $value instanceof \GOA\Makesyoulocal\Common\Model\NotImplemented) {
                // don't do anything
            } else if (is_array($value)) {
                $newData[strtolower($key)] = self::parseDataAsJson($value);
            } else {
                $newData[strtolower($key)] = $value;
            }
        }

        return $newData;
    }
}

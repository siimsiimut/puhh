<?php
namespace GOA\Makesyoulocal\Common\Utils;

class MainEncryption
{
    public function __construct($settings)
    {
        $this->settings = $settings;
        $this->isFopenAvailable = $settings->getIsFopenAvailable();
        $this->apiUrl = $settings->getApiUrl();
    }

    private function buildUrl($exchangeToken)
    {
        return $this->apiUrl . '?exchange_token=' . $exchangeToken;
    }

    private function fetchByCurl($url)
    {
        $curlError = null;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        ob_start();
        curl_exec($ch);

        if ($errno = curl_errno($ch)) {
            $error_message = curl_strerror($errno);
            $curlError = "cURL error ({$errno}): {$error_message}";
        }

        curl_close($ch);
        $string = ob_get_contents();
        ob_end_clean();

        return array('response' => $string, 'error' => $curlError);
    }

    private function fetch($url)
    {
        $result = $this->fetchByCurl($url);
        if ($result['error']) {
            throw new \Exception($result['error']);
        }

        return $result['response'];
    }

    private function decodeResponse($responseString)
    {
        $error = null;
        $data = json_decode($responseString);

        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                break;
            case JSON_ERROR_DEPTH:
                $error = 'Response for the exchange token is invalid - json_decode of the: \'' . $responseString . '\' - Maximum stack depth exceeded';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Response for the exchange token is invalid - json_decode of the: \'' . $responseString . '\' - Underflow or the modes mismatch';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Response for the exchange token is invalid - json_decode of the: \'' . $responseString . '\' - Unexpected control character found';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Response for the exchange token is invalid - json_decode of the: \'' . $responseString . '\' - Syntax error, malformed JSON';
                break;
            case JSON_ERROR_UTF8:
                $error = 'Response for the exchange token is invalid - json_decode of the: \'' . $responseString . '\' - Malformed UTF-8 characters, possibly incorrectly encoded';
                break;
            default:
                $error = 'Response for the exchange token is invalid - json_decode of the: \'' . $responseString . '\' - Unknown error';
                break;
        }

        return array('data' => $data, 'error' => $error);
    }

    public function getEncryptionToken($exchangeToken)
    {
        if (defined('GOA_DEBUG') && GOA_DEBUG === true) {
            return new EncryptionToken(
                $this->settings,
                null,
                null
            );
        }

        $response = $this->fetch($this->buildUrl($exchangeToken));
        $decodedResult = $this->decodeResponse($response);

        if ($decodedResult['error']) {
            throw new \Exception($decodedResult['error']);
        }

        if (!($decodedResult['data']->key && $decodedResult['data']->iv)) {
            throw new \Exception('Response is missing the key or iv. Response: \'' . $response . '\'');
        }

        return new EncryptionToken(
            $decodedResult['data']->key,
            $decodedResult['data']->iv
        );
    }
}

<?php
namespace GOA\Makesyoulocal\Common\Model;

class PageUri {
    private $value;
    public function __construct($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        $newPage = $this->value;
        $url = trim($this->params->requested_url, '/');
        $url = preg_replace('/exchange_token\=[^\&\/\=]*/', '', $url);
        $url = str_replace("?&", "?", rtrim(rtrim($url, '?'), '&'));

        $protocol = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === true ? 'https://' : 'http://';
        $host = $_SERVER['HTTP_HOST'];

        if (strpos($url, 'page/') === false) {
            if (strpos($url, '?') === false) {
                return $protocol . $host . '/' . rtrim($url, '/') . '/page/' . $newPage;
            }
            list($path, $urlParams) = explode('?', $url);
            return $protocol . $host . '/' . rtrim($path, '/') . '/page/' . $newPage . '?' . $urlParams;
        }

        return $protocol
            . $host
            . '/'
            . preg_replace('/page\/(\d+)/', 'page/'. $newPage, $url);
    }
}

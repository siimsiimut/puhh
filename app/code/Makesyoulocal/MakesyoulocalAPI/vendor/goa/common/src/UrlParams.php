<?php
namespace GOA\Makesyoulocal\Common;

class UrlParams
{
    private $data = array(
        'controller' => null,
        'body' => null,
        'action' => null,
        'token' => null,
        'exchange_token' => null,
        'requested_url' => '',
        'key' => null,
    );

    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    public function __set($name, $value)
    {
        if ($name === 'api') {
            $this->controller = 'api';
            $this->action = $value;
            return;
        }

        if (!array_key_exists($name, $this->data)) {
            return;
        }

        if (gettype($value) !== 'string') {
            $this->data[$name] = $value;
            return;
        }

        switch ($name) {
            case 'token':
                if (gettype($value) === 'string') {
                    if (strpos($value, ',') === false) {
                        $value = $value ? array($value) : array();
                    } else {
                        $value = explode(',', $value);
                    }
                } else {
                    $value = null;
                }
                break;
            case 'json':
                $value = filter_var($value, FILTER_VALIDATE_BOOLEAN);
                break;
            case 'body':
                $value = @\json_decode($value, true);
            case 'requested_url':
                $value = $value;
                break;
            default:
                $value = htmlspecialchars($value, ENT_QUOTES);
        }


        $this->data[$name] = $value;
    }

    public function __get($name)
    {
        if (!array_key_exists($name, $this->data)) {
            return;
        }

        return $this->data[$name];
    }
}


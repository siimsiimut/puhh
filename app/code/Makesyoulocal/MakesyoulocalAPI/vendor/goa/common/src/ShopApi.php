<?php
namespace GOA\Makesyoulocal\Common;

class ShopApi {
    protected $api;

    public function __construct()
    {
        $this->api = array();
    }

    public function set($name, $obj)
    {
        $this->api[$name] = $obj;
    }

    public function get($name)
    {
        return $this->api[$name];
    }
}

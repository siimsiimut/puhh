<?php
namespace GOA\Makesyoulocal\Common\Model;

class Getter {
    private $value;
    public function __construct($value)
    {
        $this->value = $value;
    }

    public function getValue($settings)
    {
        return $settings->{'get' . ucfirst($this->value)}();
    }
}

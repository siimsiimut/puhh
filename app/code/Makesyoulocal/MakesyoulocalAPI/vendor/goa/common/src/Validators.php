<?php
namespace GOA\Makesyoulocal\Common;

use \GOA\Makesyoulocal\Common\Model\ValidationError as VE;

class Validators {
    public static function isAssoc(array $arr)
    {
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    public static function getValueByPath($path, $payload, $any = true) {
        if (is_string($path)) {
            if (strpos($path, "||") !== false) {
                $path = explode("||", $path);
                $result = array();
                foreach ($path as $p) {
                    $r = self::getValueByPath($p, $payload, $any);
                    if (!($r instanceof \GOA\Makesyoulocal\Common\Model\Missing)) {
                        array_push($result, $r);
                    }
                }
                if (!count($result)) {
                    return new \GOA\Makesyoulocal\Common\Model\Missing();
                }
                return $result;
            }

            $path = explode(".", $path);
            $path = array_map(function($x) { return trim($x); }, $path);
        }
        $head = array_shift($path);

        $result = null;
        if ($head === '*' && (is_array($payload) && !self::isAssoc($payload))) {
            $result = array();
            foreach ($payload as $p) {
                $r = self::getValueByPath($path, $p, $any);
                if (!($r instanceof \GOA\Makesyoulocal\Common\Model\Missing)) {
                    array_push($result, $r);
                }
            }

            if (!(($any && count($result)) || (!$any && count($result) === count($payload)))) {
                $result = new \GOA\Makesyoulocal\Common\Model\Missing();
            }
        } else if (isset($payload[$head])) {
            $isNextAstrix = count($path);
            if (is_array($payload[$head])) {
                $result = self::getValueByPath($path, $payload[$head], $any);
            } else if (count($path) === 0) {
                $result = $payload[$head];
            } else {
                $result = new \GOA\Makesyoulocal\Common\Model\Missing();
            }
        } else {
             $result = new \GOA\Makesyoulocal\Common\Model\Missing();
        }

        return $result;
    }

    public static function whenNotMissing($fn) {
        return function($value, $settings, $payload) use ($fn) {
            if ($value instanceof \GOA\Makesyoulocal\Common\Model\PayloadGetter) {
                $value = $value->solve($payload);
            }

            if ($value instanceof \GOA\Makesyoulocal\Common\Model\Missing) {
                if ($value->requiredIfEmpty) {
                    if (self::getValueByPath($value->requiredIfEmpty, $payload) instanceof \GOA\Makesyoulocal\Common\Model\Missing) {
                        return new VE("Field %s is required when the '{$value->requiredIfEmpty}' is not present");
                    }
                }
                if ($value->isRequired) {
                    return new VE('Field %s is required');
                }
                return $value;
            }

            return $fn($value, $settings, $payload);
        };
    }

    public static function UniqueInPath($path)
    {
        return self::whenNotMissing(function($value, $settings, $payload) use ($path) {
            $result = self::getValueByPath($path, $payload);
            if ($result instanceof \GOA\Makesyoulocal\Common\Model\Missing) {
                return new VE("Invariant: Validation rules failed");
            }

            if (!is_array($result) || count($result) === 1) {
                return $value;
            }
            return new VE("Field %s must be unique across '$path'");
        });
    }

    public static function ForbiddenIfNotEmpty($path)
    {
        return self::whenNotMissing(function($value, $settings, $payload) use ($path) {
            if (!(self::getValueByPath($path, $payload) instanceof \GOA\Makesyoulocal\Common\Model\Missing)) {
                return new VE("Field %s is forbidden when the '{$path}' is present");
            }
            return $value;
        });
    }

    public static function ArrayOfAlphaNumeric()
    {
        return self::whenNotMissing(function($value, $settings, $payload) {
            if (!is_array($value)) {
                return new VE("Field %s must be a an array");
            }
            foreach ($value as $v) {
                if (!self::AlphaNumeric()($v, $settings, $payload)) {
                    return new VE("All items of %s array must be alphanumeric");
                }
            }

            return $value;
        });
    }

    public static function Object()
    {
        function hasStringKeys(array $array) {
          return count(array_filter(array_keys($array), 'is_string')) > 0;
        }

        return self::whenNotMissing(function ($value) {
            if (!is_array($value)) {
                return new VE("Field %s must be a an object");
            }

            foreach ($value as $k => $v) {
                if (!is_string($k)) {
                    return new VE("Key ${k} of the %s must be a string");
                }

                if (!is_string($v)) {
                    return new VE("Value of the ${k} of the %s must be an integer or string");
                }
            }
            return $value;
        });
    }

    public static function ArrayOf($rules, $maxElements = null)
    {
        return self::whenNotMissing(function($values, $settings, $full) use ($rules, $maxElements) {
            if (is_array($rules) && !is_array($values)) {
                return new VE("Field %s must be an array of objects");
            }

            $results = array();
            $errors = new VE("Field %s must be a valid array");

            foreach ($values as $idx => $value) {
                $result = $rules($value, $settings, $full);
                if ($result instanceof \GOA\Makesyoulocal\Common\Model\ValidationError) {
                    $result->setField($idx);
                    $errors->merge($result);
                } else if ($result instanceof \GOA\Makesyoulocal\Common\Model\ValueModified) {
                    array_push($results, $result->value);
                } else {
                    array_push($results, $result);
                }
            }
            if (count($errors->collection)) {
                return $errors;
            }

            if ($maxElements !== null && count($results) > $maxElements) {
                return new VE("Field %s must contain at most $maxElements elements");
            }

            return new \GOA\Makesyoulocal\Common\Model\ValueModified($results);
        });
    }

    public static function ObjectOf($rules, $validationOrder = array())
    {
        uksort($rules, function($a, $b) use ($validationOrder) {
            $indexA = array_search($a, $validationOrder);
            $indexB = array_search($b, $validationOrder);

            if ($indexA !== False && $indexB === False) {
                return -1;
            }
            if ($indexA === False && $indexB !== False) {
                return 1;
            }
            if ($indexA === False && $indexB === False) {
                return 0;
            }

            return $indexB - $indexA;
        });

        return self::whenNotMissing(function($value, $settings, $full) use ($rules) {
            if (!is_array($value)) {
                return new VE("Field %s must be an object");
            }

            $errors = new VE("Field %s must have valid properties");

            $keysInPayload = array_keys($value);
            $keysSupported = array();
            $payload = array();


            foreach ($rules as $key => $parser) {
                $inputKey = $parser[0];

                $defValue = isset($parser[2]) ? $parser[2] : new \GOA\Makesyoulocal\Common\Model\Missing();

                if ($defValue instanceof \GOA\Makesyoulocal\Common\Model\Getter) {
                    $defValue = $defValue->getValue($settings);
                }

                $payloadVal = null;
                $indxOfVal = 0;

                if (is_array($inputKey)) {
                    $tmpVal = $defValue;
                    foreach ($inputKey as $iIdx => $iKey) {
                        if ($tmpVal instanceof \GOA\Makesyoulocal\Common\Model\Missing && isset($value[$iKey])) {
                            $tmpVal = $value[$iKey];
                            $indxOfVal = $iIdx;
                        }
                    }
                    $payloadVal = $tmpVal;
                } else {
                    $payloadVal = isset($value[$inputKey]) ? $value[$inputKey] : $defValue;
                }

                $parserFn = (is_array($inputKey) && is_array($parser[1]))
                    ? $parser[1][$indxOfVal]
                    : $parser[1];


                $parserResult = $parserFn($payloadVal, $settings, $full);

                if ($parserResult instanceof \GOA\Makesyoulocal\Common\Model\ValueModified) {
                    $payload[$key] = $parserResult->value;
                } else if ($parserResult instanceof VE) {
                    $parserResult->setField($inputKey);
                    $errors->merge($parserResult);
                } else {
                    $payload[$key] = $parserResult;
                }

                if (is_array($inputKey)) {
                    $keysSupported = array_merge($keysSupported, $inputKey);
                } else {
                    array_push($keysSupported, $inputKey);
                }
            }

            $notSupportedKeys = array_diff($keysInPayload, $keysSupported, [""]);
            if (count($notSupportedKeys) > 0) {
                $notSupportedAsString = implode(',', $notSupportedKeys);
                $errors->merge(new VE("The payload contains invalid keys: $notSupportedAsString"));
            }

            if ($errors->hasErrors()) {
                return $errors;
            }

            return new \GOA\Makesyoulocal\Common\Model\ValueModified($payload);
        });
    }

    public static function AlphaNumeric()
    {
        return self::whenNotMissing(function($value, $settings, $payload) {
            $matches = array();
            if (is_string($value) || is_numeric($value)) {
                preg_match("/^[A-Za-z0-9_\-]+$/", $value, $matches);
            }

            if (count($matches) > 0) {
                return $value;
            }
            return new VE('Field %s must be alphanumeric');
        });
    }

    public static function ISODateTime()
    {
        return self::whenNotMissing(function($value, $settings, $payload) {
            if (!is_string($value)) {
                return new VE("Field %s must be a string ISO DateTime");
            }

            $dateTime = new \DateTimeImmutable($value);
            $dateTime = $dateTime->setTimezone(new \DateTimeZone('UTC'));

            if ($dateTime) {
                return new \GOA\Makesyoulocal\Common\Model\ValueModified($dateTime);
            }

            return new VE("Field %s must be valid ISO8601 DateTime");
        });
    }

    public static function Integer($min = null, $max = null)
    {
        return self::whenNotMissing(function($value) use($min, $max) {
            if (is_numeric($value)) {
                $value = +($value);
                if (!is_int($value)) {
                    return new VE("Field %s must be an Integer");
                }
                $value = intval($value);

                if ($min !== null && $value < $min) {
                    return new VE("Field %s must be greater or equal to $min");
                }
                if ($max !== null && $value > $max) {
                    return new VE("Field %s must be lower or equal to $max");
                }
                return new \GOA\Makesyoulocal\Common\Model\ValueModified($value);
            }
            return new VE("Field %s must be an Integer");
        });
    }

    public static function Float($min = null, $max = null)
    {
        return self::whenNotMissing(function($value) use($min, $max) {
            if (is_string($value)) {
                if (preg_match("/^-?[0-9.]+$/", $value)) {
                    return new VE("Field %s must be an Float");
                }
                $value = floatval($value);
            }
            if (is_numeric($value)) {
                if ($min !== null && $value < $min) {
                    return new VE("Field %s must be greater or equal to $min");
                }
                if ($max !== null && $value > $max) {
                    return new VE("Field %s must be lower or equal to $max");
                }
                return new \GOA\Makesyoulocal\Common\Model\ValueModified($value);
            }
            return new VE("Field %s must be an Integer");
        });
    }

    public static function Boolean()
    {
        return self::whenNotMissing(function($value) {
            if (!is_bool($value)) {
                return new VE("Field %s must be a Boolean");
            }
            return $value;
        });
    }

    public static function Text()
    {
        return self::whenNotMissing(function($value) {
            if (!is_string($value)) {
                return new VE("Field %s must be a String");
            }
            return $value;
        });
    }

    public static function Url()
    {
        return self::whenNotMissing(function($value) {
            if (!is_string($value) || !filter_var($value, FILTER_VALIDATE_URL)) {
                return new VE("Field %s must be a valid url");
            }
            return $value;
        });
    }

    public static function ImageFilename()
    {
        return self::whenNotMissing(function($value) {
            if (!is_string($value)) {
                return new VE("Field %s must be a valid url");
            }


            $extensions = ['jpg', 'gif', 'png'];
            $extensionsAsStr = implode(',', $extensions);
            $parts = explode(".", $value);

            if (!(count($parts) > 1
                && array_search($parts[count($parts) - 1], $extensions) !== False)) {
                return new VE("Field %s must have be a filename with extension that is one of: $extensionsAsStr");
            }

            preg_match("/^[A-Za-z0-9_\-\.]+$/", $value, $matches);

            if (count($matches) > 0) {
                return $value;
            }
            return new VE('Field %s must be a valid filename');
        });
    }

    public static function OneOf($enum)
    {
        return self::whenNotMissing(function($value, $settings) use ($enum) {
            if ($enum instanceof \GOA\Makesyoulocal\Common\Model\Getter) {
                $enum = $enum->getValue($settings);
            }

            $enumAsStr = implode(", ", $enum);

            if (!is_string($value) || !in_array($value, $enum)) {
                return new VE("Field %s must be one of the '$enumAsStr'");
            }
            return $value;
        });
    }

    public static function Or($schemas)
    {
        return self::whenNotMissing(function($value, $settings, $full) use ($schemas)  {
            $error = new VE("Field %s does not fit to either schema");
            foreach ($schemas as $schema) {
                $result = $schema($value, $settings, $full);
                if (!$result instanceof VE) {
                    return $result;
                }
                $error->merge($result);
            }
            return $error;
        });
    }

    public static function Email()
    {
        return self::whenNotMissing(function($value) {
            if (!is_string($value) || !(filter_var($value, FILTER_VALIDATE_EMAIL))) {
                return new VE("Field %s must be a valid email");
            }
            return $value;
        });
    }

    public static function And(...$rules)
    {
        return self::whenNotMissing(function($value, $settings, $payload) use($rules) {
            $r = $value;
            foreach ($rules as $rule) {
                $r = $rule($r, $settings, $payload);
                if ($r instanceof VE) {
                    return $r;
                }
                if ($r instanceof \GOA\Makesyoulocal\Common\Model\ValueModified) {
                    $r = $r->value;
                }
            }
            return new \GOA\Makesyoulocal\Common\Model\ValueModified($r);
        });
    }

    public static function Phone()
    {
        return self::whenNotMissing(function($value) {
            $matches = array();
            preg_match("/^[0-9_\-\.\+\s]+$/", $value, $matches);

            if (count($matches) > 0) {
                return $value;
            }
            return new VE('Field %s must be a valid phone number');
        });
    }

    public static function WithAtMostOneKey($path)
    {
        return self::whenNotMissing(function($value, $settings, $payload) use ($path) {
            $r = self::getValueByPath($path, $value);
            if (count($r) > 1) {
                return new VE("Object %s can have at most one key of: '$path'");
            }
            return $value;
        });
    }

    public static function WithFieldRequiredWhen($field, $path)
    {
        return self::whenNotMissing(function($value, $settings, $payload) use ($field, $path) {
            $r = self::getValueByPath($path, $value);
            if (!$r instanceof \GOA\Makesyoulocal\Common\Model\Missing && count($r) > 0) {
                $r = self::getValueByPath($field, $value);
                if ($r instanceof \GOA\Makesyoulocal\Common\Model\Missing) {
                    return new VE("Object %s must have '$field' if '$path' is not empty");
                }
            }
            return $value;
        });
    }

    public static function WithAtLeastOneKey($path)
    {
        return self::whenNotMissing(function($value, $settings, $payload) use ($path) {
            $r = self::getValueByPath($path, $value);
            if (!count($r)) {
                return new VE("Object %s must have at least one key of: '$path'");
            }
            return $value;
        });
    }

    public static function WithAlwaysOrNever($path)
    {
        return self::whenNotMissing(function($value, $settings, $payload) use ($path) {
            $r = self::getValueByPath($path, $value);
            if (count($r) && count($r) !== count($value)) {
                return new VE("Field %s is incorrect. '$path' must be omitted or specified for all childred");

            }
            return $value;
        });
    }
}

<?php
namespace GOA\Makesyoulocal\Common\Model;



class PayloadGetter {
    public function __construct($path)
    {
        $arguments = func_get_args();
        $this->defaultValue = new Missing();
        if (count($arguments) > 1) {
            $this->defaultValue = $arguments[1];
        }
        $this->path = $path;
    }

    public function solve($payload) {
        if (isset($payload[$this->path])) {
            return $payload[$this->path];
        }
        return $this->defaultValue;
    }
}

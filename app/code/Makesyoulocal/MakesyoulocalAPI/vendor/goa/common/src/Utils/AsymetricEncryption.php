<?php
namespace GOA\Makesyoulocal\Common\Utils;

class AsymetricEncryption
{
    public static function createKeyPair()
    {
        $keyPair = \sodium_crypto_sign_keypair();
        $secKey = \sodium_crypto_sign_secretkey($keyPair);
        $pubKey = \sodium_crypto_sign_publickey($keyPair);
        return array(\bin2hex($secKey), \bin2hex($pubKey));
    }

    public static function sign($privateKey, $content) {
        $signature = \sodium_crypto_sign_detached(
            $content,
            \hex2bin($privateKey)
        );
        return bin2hex($signature);
    }

    public static function verify($publicKey, $signature, $content) {
        return \sodium_crypto_sign_verify_detached(
            \hex2bin($signature), $content, \hex2bin($publicKey)
        );
    }
}

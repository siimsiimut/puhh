<?php
namespace GOA\Makesyoulocal\Common\Api;

function isAssoc(array $arr)
{
    if (array() === $arr) {
        return false;
    }
    return array_keys($arr) !== range(0, count($arr) - 1);
}

class Base
{
    protected $rootName;

    protected $page = 1;
    protected $limit = 20;
    protected $from = null;
    protected $contentEncryption = null;

    public function __construct($params, $settings)
    {
        $this->params = $params;
        $this->page = $params->page;
        $this->limit = $params->limit;
        $this->from = $params->from;
        $this->settings = $settings;


        $contentEncryptionKey = $this->settings->getContentEncryptionKey();
        $contentEncryptionIv = $this->settings->getContentEncryptionIv();
        if ($contentEncryptionKey && $contentEncryptionIv) {
            $this->contentEncryption = new \GOA\Makesyoulocal\Common\Utils\EncryptionToken(
                $this->settings,
                $contentEncryptionKey,
                $contentEncryptionIv
            );
        }
    }

    protected function constructPageUri($newPage)
    {
        $url = trim($this->params->requested_url, '/');
        $url = preg_replace('/exchange_token\=[^\&\/\=]*/', '', $url);
        $url = str_replace("?&", "?", rtrim(rtrim($url, '?'), '&'));

        $protocol = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === true ? 'https://' : 'http://';
        $host = $_SERVER['HTTP_HOST'];

        if (strpos($url, 'page/') === false) {
            if (strpos($url, '?') === false) {
                return $protocol . $host . '/' . rtrim($url, '/') . '/page/' . $newPage;
            }
            list($path, $urlParams) = explode('?', $url);
            return $protocol . $host . '/' . rtrim($path, '/') . '/page/' . $newPage . '?' . $urlParams;
        }

        return $protocol
            . $host
            . '/'
            . preg_replace('/page\/(\d+)/', 'page/'. $newPage, $url);
    }


    public function changeOutputFields($data)
    {
        $contentEncryption = $this->contentEncryption;
        $fieldsVisibility = array(
            'ROOT' => array(
                'PRODUCTS' => $this->settings->getProductsFields(),
                'ORDERS' => $this->settings->getOrdersFields()
            )
        );

        function encodeTree($d, $ce)
        {
            if (!$ce) {
                return $d;
            }

            if (is_object($d) || is_array($d)) {
                $newData = array();
                foreach ($d as $key => $value) {
                    $newData[$key] = encodeTree($value, $ce);
                }
                return $newData;
            }

            return $ce->encrypt($d);
        }

        function transform($d, $lastFromConfig, $parentKey, $ce)
        {
            if (isset($lastFromConfig[$parentKey]) && $lastFromConfig[$parentKey] === 0) {
                return null;
            }

            if (!isset($lastFromConfig[$parentKey]) || $lastFromConfig[$parentKey] === 1) {
                return $d;
            }

            if (isset($lastFromConfig[$parentKey]) && $lastFromConfig[$parentKey] === 2) {
                return encodeTree($d, $ce);
            }

            if (is_array($d) && !isAssoc($d)) {
                return array_map(function ($newD) use ($lastFromConfig, $parentKey, $ce) {
                    return transform($newD, $lastFromConfig, $parentKey, $ce);
                }, $d);
            }

            $newData = array();

            foreach ($d as $k => $v) {
                if (!is_object($v) && !is_array($v)) {
                    if (!isset($lastFromConfig[$parentKey][$k]) || $lastFromConfig[$parentKey][$k] === 1) {
                        $newData[$k] = $v;
                    }
                    if (isset($lastFromConfig[$parentKey][$k]) && $lastFromConfig[$parentKey][$k] === 2) {
                        $newData[$k] = encodeTree($v, $ce);
                    }
                } else {
                    $newData[$k] = transform($v, $lastFromConfig[$parentKey], $k, $ce);
                }
            }
            return $newData;
        }

        return transform($data, $fieldsVisibility, 'ROOT', $this->contentEncryption);
    }

    protected function getIds()
    {
        return array();
    }

    protected function getQuerySize()
    {
        return 0;
    }

    public function export()
    {
        $rows = array();
        foreach ($this->getIds() as $id) {
            array_push($rows, $this->prepareRow($id));
        }

        $data = array(
            'HEADER' => array(
                'RESULTS'   => $this->getQuerySize(),
                'VERSION'   => $this->settings->getVersion(),
                'PER_PAGE'  => count($rows)
            )
        );
        if ($this->limit * $this->page < $this->getQuerySize()) {
            $data['HEADER']['NEXT_PAGE'] = $this->constructPageUri($this->page + 1);
        }

        if ($this->page > 1) {
            $data['HEADER']['PREVIOUS_PAGE'] = $this->constructPageUri($this->page - 1);
        }

        return array_merge($data, $this->changeOutputFields(array($this->rootName => $rows)));
    }
}

<?php
namespace GOA\Makesyoulocal\Common\Model;

class Currency {
    private $value;
    public function __construct($value)
    {
        if ($value === null || is_float($value) || is_int($value)) {
            $this->value = $value;
        } else if (is_string($value) && trim($value) === '') {
            $this->value = null;
        } else if (is_string($value)) {
            $this->value = floatval(trim($value));
        } else {
            $this->value = null;
        }
    }

    public function getValue()
    {
        if ($this->value === null) {
            return 0;
        }
        return \round($this->value, 2);
    }

    public function add($mixed)
    {
        if (!($mixed instanceof self)) {
            $mixed = new Currency($mixed);
        }

        if ($mixed->getValue() === null && $this->value === null) {
            return new Currency(0);
        }

        $normalisedValue = $this->value ? $this->value : 0;
        $normalisedMixed = $mixed->getValue() ? $mixed->getValue() : 0;
        $result = $normalisedValue + $normalisedMixed;
        return new Currency($result);
    }

    public function divide($mixed)
    {
        if (!($mixed instanceof self)) {
            $mixed = new Currency($mixed);
        }

        if ($mixed->getValue() === null && $this->value === null) {
            return new Currency(0);
        }

        $normalisedValue = $this->value ? $this->value : 0;
        $normalisedMixed = $mixed->getValue() ? $mixed->getValue() : 1;
        $result = $normalisedValue / $normalisedMixed;
        return new Currency($result);
    }
}

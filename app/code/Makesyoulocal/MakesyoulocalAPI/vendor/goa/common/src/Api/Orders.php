<?php
namespace GOA\Makesyoulocal\Common\Api;

class Orders extends Base
{
    protected $rootName = 'ORDERS';
    protected $id = null;
    protected $fromId = null;
    protected $to = null;
    protected $ordersGetter;

    public function __construct($params, $settings)
    {
        parent::__construct($params, $settings);

        $this->id = $params->id;
        $this->fromId = $params->id_from;
        $this->to = $params->to;
        $this->upFrom = $params->updated_since;
        $this->upTo = $params->up_to;
    }

    protected function prepareRow($orderId)
    {
        return $this->orderGetter->getById($orderId);
    }
}

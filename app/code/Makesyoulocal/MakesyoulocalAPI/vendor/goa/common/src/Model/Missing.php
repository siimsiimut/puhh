<?php
namespace GOA\Makesyoulocal\Common\Model;

class Missing {
    public $requiredIfEmpty = null;
    public $isRequired = false;

    public function __construct($isRequired = false)
    {
        if (is_string($isRequired)) {
            $this->requiredIfEmpty = $isRequired;
        } else {
            $this->isRequired = $isRequired;
        }
    }
}

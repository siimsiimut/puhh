<?php
namespace GOA\Makesyoulocal\Common;

class Params
{
    private $data = array(
        'controller' => null,
        'body' => null,
        'action' => null,
        'token' => null,
        'replace' => false,
        'exchange_token' => null,
        'json' => true,
        'requested_url' => '',
        'key' => null,
        'limit' => 50,
        'page' => 1,
        'from' => false,
        'to' => false,
        'id' => false,
        'id_from' => false,
        'up_from' => false,
        'up_to' => false,
        'updated_since' => false,
        'sku' => false
    );

    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    public function __set($name, $value)
    {
        if ($name === 'export') {
            $this->controller = 'export';
            $this->action = $value;
            return;
        }

        if ($name === 'api') {
            $this->controller = 'api';
            $this->action = $value;
            return;
        }

        if (!isset($this->data[$name])) {
            return;
        }

        if (gettype($value) !== 'string') {
            $this->data[$name] = $value;
            return;
        }

        switch ($name) {
            case 'token':
                if (gettype($value) === 'string') {
                    if (strpos($value, ',') === false) {
                        $value = $value ? array($value) : array();
                    } else {
                        $value = explode(',', $value);
                    }
                } else {
                    $value = null;
                }
                break;
            case 'json':
                $value = filter_var($value, FILTER_VALIDATE_BOOLEAN);
                break;
            case 'limit':
                $value = intval($value);
                if (!$value || $value <= 0) {
                    $value = 50;
                } elseif ($value > 10000) {
                    $value = 10000;
                }

                break;
            case 'page':
                $value = intval($value);
                if ($value <= 0) {
                    $value = 1;
                }
                break;
            case 'updated_since':
            case 'from':
            case 'to':
            case 'up_to':
            case 'up_from':
                $value = intval($value);
                break;
            case 'body':
                $value = @\urldecode($value);
                $value = @\json_decode($value, true);
            case 'requested_url':
                $value = $value;
                break;
            default:
                $value = htmlspecialchars($value, ENT_QUOTES);
        }


        $this->data[$name] = $value;
    }

    public function __get($name)
    {
        if (!isset($this->data[$name])) {
            return;
        }

        return $this->data[$name];
    }
}

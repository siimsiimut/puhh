<?php
namespace GOA\Makesyoulocal\Common\Model;

class ActionResult {
    public static $errorEmpty = 'error_validation';
    public static $errorNotImplemented = 'error_not_implemented';
    public static $errorNotFound = 'error_not_found';
    public static $orderStatusChanged = 'status_changed';
    public static $orderNoteCreated = 'order_note_created';
    public static $orderRefundCreated = 'order_refund_created';
    public static $productStockChange = 'product_stock_change';
    public static $data = 'data';
}


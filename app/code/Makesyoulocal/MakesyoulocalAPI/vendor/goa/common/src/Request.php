<?php
namespace GOA\Makesyoulocal\Common;

class Request
{
    protected $encryptionToken;
    public $postBody;

    public function __construct($urlParams, $postBody)
    {
        $this->urlParams = $urlParams;
        $this->postBody = $postBody;


        $this->body = null;
    }

    public function getControllerName()
    {
        return str_replace('_', '', ucwords(strtolower($this->urlParams->controller), '_'));
    }

    public function getActionName() {
        return str_replace('_', '', ucwords(strtolower($this->urlParams->action), '_'));
    }

    public function setActionName($action)
    {
        $this->urlParams->action = $action;
    }

    public function getEncryptionToken()
    {
        return $this->encryptionToken;
    }

    public function setEncryptionToken($encryptionToken) {
        $this->encryptionToken = $encryptionToken;
    }

    public function getExchangeToken() {
        return $this->urlParams->exchange_token;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    public function getBody()
    {
        return $this->body;
    }
}

<?php

namespace GOA\Makesyoulocal\Common\Utils;

class PHPVersion
{
    public static function get()
    {
        if (defined('PHP_VERSION_ID')) {
            return PHP_VERSION_ID;
        }

        $version = array_map('intval', explode('.', phpversion()));
        return ($version[0] * 10000) + ($version[1] * 100) + $version[2];
    }
}


<?php
namespace GOA\Makesyoulocal\Common\Model;

class DatetimeFormatter {
    private $value;
    public function __construct($value)
    {
        $this->value = $value;
        if ($this->value instanceof \DateTime) {
            $this->value = new \DateTimeImmutable($this->value);
        }
        if ($this->value instanceof \DateTimeImmutable) {
            $this->value = $this->value->setTimezone(new \DateTimeZone('UTC'));
            $this->value = preg_replace('/(\d{2})(\d{2})$/', '$1:$2', $value->format(\DateTime::ISO8601));
        } else {
            $this->value = null;
        }
    }

    public function getValue()
    {
        return $this->value;
    }
}

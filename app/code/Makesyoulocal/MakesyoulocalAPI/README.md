# Makesyoulocal module

## Installation steps

## Using composer

### Adding custom composer repository
Makesyoulocal module is available through the private composer registry. To add the repository, you can run the command below:
```
composer config repositories.makesyoulocal composer https://client.makesyoulocal.com/composer/
```

Or, edit the composer.json directly in your root magento2 directory. Add the code below into the existing file:

```
    "repositories": {

    	{Do not delete the existing items}

        "makesyoulocal": {
            "type": "composer",
            "url": "https://client.makesyoulocal.com/composer/"
        }
```

### Requiring the package
After the support for registry is added we can require our module:

You can either run:

```
composer require makesyoulocal/magento2 @stable
```

Or edit the composer.json directly.


### Installation
Finally after everything is done run 

```
composer update
```

Refresh your magento2 configuration:
```
bin/magento setup:upgrade
```

And enable our module:
```
bin/magento module:enable Makesyoulocal_MakesyoulocalAPI    
```

## Manually moving files

* Login to your FTP, navigate to the root folder.
* Create an `app/codes` folder if one does not exist already.
* Upload the content of the unzipped folder to the `app/codes` folder. The folder structure should resemble: `app/code/Makesyoulocal/MakesyoulocalAPI`.
* Disable the cache under System → Cache Management.
* Enter the following at the command line: `php bin/magento setup:upgrade`.
* Login to admin panel and switch to the store view you wish to connect. Navigate to Stores → Configuration (the path can vary with Magento versions), the module will be shown.
* Expand the Basic tab and make sure the module is activated (if not, it should be activated from within the Default Config scope). Then expand the Register tab.

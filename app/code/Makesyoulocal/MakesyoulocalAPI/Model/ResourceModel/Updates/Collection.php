<?php
namespace Makesyoulocal\MakesyoulocalAPI\Model\ResourceModel\Updates;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection{
	public function _construct(){
		$this->_init("Makesyoulocal\MakesyoulocalAPI\Model\Updates","Makesyoulocal\MakesyoulocalAPI\Model\ResourceModel\Updates");
	}
}


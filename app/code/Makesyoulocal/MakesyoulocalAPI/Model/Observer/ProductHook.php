<?php

namespace Makesyoulocal\MakesyoulocalAPI\Model\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class ProductHook implements ObserverInterface {

    protected $updatesFactory;
    protected $logger;
    protected $dateTime;
    protected $storeWebsiteRelation;

    public function __construct(
        \Makesyoulocal\MakesyoulocalAPI\Model\UpdatesFactory $updatesFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
        \Magento\Store\Api\StoreWebsiteRelationInterface $storeWebsiteRelation
    ) {
        $this->dateTime = $dateTime;
        $this->logger = $logger;
        $this->updatesFactory = $updatesFactory;
        $this->storeWebsiteRelation = $storeWebsiteRelation;
    }

    public function execute(Observer $observer) {
        try {
            $product = null;
            if ($observer->getEvent()->getItem() instanceof \Magento\CatalogInventory\Model\Stock\Item\Interceptor) {
                $productId = $observer->getEvent()->getItem()->getProductId();

                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $product = $objectManager->create('\Magento\Catalog\Model\Product')
                                       ->load($productId);
            } else {
                $product = $observer->getProduct();
            }

            if ($product === null) {
                return;
            }

            $productId = $product->getId();
            $websitesIds = $product->getWebsiteIds();

            foreach ($websitesIds as $websiteId) {
                $storeIds = $this->storeWebsiteRelation->getStoreByWebsiteId($websiteId);
                foreach ($storeIds as $storeId) {
                    $update = $this->updatesFactory->create();
                    $update->addData([
                        'entity_id' => $productId,
                        'store_id' => $storeId,
                        'type' => 'products',
                        'updated_at' => $this->dateTime->gmtDate()
                    ]);
                    $update->save();
                }
            }

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}

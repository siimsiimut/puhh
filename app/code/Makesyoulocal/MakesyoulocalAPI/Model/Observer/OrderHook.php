<?php

namespace Makesyoulocal\MakesyoulocalAPI\Model\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class OrderHook implements ObserverInterface {

    protected $updatesFactory;
    protected $logger;
    protected $dateTime;

    public function __construct(
        \Makesyoulocal\MakesyoulocalAPI\Model\UpdatesFactory $updatesFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
    ) {
        $this->dateTime = $dateTime;
        $this->logger = $logger;
        $this->updatesFactory = $updatesFactory;
    }

    public function execute(Observer $observer) {
        if ($order = $observer->getOrder()) {
            $this->updateData($order);
        } elseif ($orders = $observer->getOrders()) {
            foreach ($orders as $order) {
                $this->updateData($order);
            }
        }
    }

    protected function updateData($order) {
        try {
            $orderId = $order->getId();
            $storeId = $order->getStoreId();

            $items = $order->getAllItems();
            foreach ($items as $item) {
                $update = $this->updatesFactory->create();
                $update->addData([
                    'entity_id' => $item->getProductId(),
                    'store_id' => $storeId,
                    'type' => 'products',
                    'updated_at' => $this->dateTime->gmtDate()
                ]);
                $update->save();
            }

            $update = $this->updatesFactory->create();
            $update->addData([
                'entity_id' => $orderId,
                'store_id' => $storeId,
                'type' => 'orders',
                'updated_at' => $this->dateTime->gmtDate()
            ]);
            $update->save();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}

<?php

namespace Makesyoulocal\MakesyoulocalAPI\Model\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class OrderShipmentHook implements ObserverInterface {

    protected $updatesFactory;
    protected $logger;
    protected $dateTime;

    public function __construct(
        \Makesyoulocal\MakesyoulocalAPI\Model\UpdatesFactory $updatesFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Stdlib\DateTime\DateTime $dateTime
    ) {
        $this->dateTime = $dateTime;
        $this->logger = $logger;
        $this->updatesFactory = $updatesFactory;
    }

    public function execute(Observer $observer) {
        try {
            $shipment = $observer->getData()['data_object'];
            $order = $shipment->getOrder();

            $orderId = $order->getId();
            $storeId = $order->getStoreId();

            $update = $this->updatesFactory->create();
            $update->addData([
                'entity_id' => $orderId,
                'store_id' => $storeId,
                'type' => 'orders',
                'updated_at' => $this->dateTime->gmtDate()
            ]);
            $update->save();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}

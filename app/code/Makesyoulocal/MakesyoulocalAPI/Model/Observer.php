<?php 
namespace Makesyoulocal\MakesyoulocalAPI\Model;

use Magento\Framework\App\Area;

class Observer {
    protected $storeManager;
    protected $emulation;
    protected $scopeConfig;
    protected $updatesFactory;
    protected $updatesCollectionFactory;
    protected $cacheTypeList;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Store\Model\App\Emulation $emulation,
        \Makesyoulocal\MakesyoulocalAPI\Model\UpdatesFactory $updatesFactory,
        \Makesyoulocal\MakesyoulocalAPI\Model\ResourceModel\Updates\CollectionFactory $updatesCollectionFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
    )
    {
        $this->storeManager = $storeManager;
        $this->emulation = $emulation;
        $this->scopeConfig = $scopeConfig;
        $this->configWriter = $configWriter;
        $this->updatesFactory = $updatesFactory;
        $this->updatesCollectionFactory = $updatesCollectionFactory;
        $this->cacheTypeList = $cacheTypeList;
    }

    public function addProductEvent($productSKU)
    {
        $product = $objectManager->get('Magento\Catalog\Model\Product')->loadByAttribute('sku', $productSKU);
        if ($product) {
            $this->webhooks->scheduleEvent($product->getId(), 'products');
        }
    }

    public function updateTokens($storeId, $tokens, $mode = 0) {
        $storeTokens = $this->scopeConfig->getValue("makesyoulocal/tokens/list", \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);

        if ($storeTokens && trim($storeTokens) !== "") {
            $storeTokens = explode(PHP_EOL, $storeTokens);
        } else {
            $storeTokens = array();
        }

        if ($mode === 0) {
            $storeTokens = $tokens;
        } else if ($mode === 1) {
            $storeTokens = array_unique(array_merge($storeTokens, $tokens));
        } else if ($mode === -1) {
            $storeTokens = array_diff($storeTokens, $tokens);
        }

        $this->configWriter->save('makesyoulocalAPI/tokens/list', implode("\n", $storeTokens), \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);

        $this->cacheTypeList->cleanType('config');

        return $storeTokens;
    }

    public function getScheduledEvents($matchinEventName, $storeId)
    {
    }

    public function clearScheduledEvents($matchinEventName, $storeId)
    {
    }

    public function triggerWebhooks()
    {
    }

    public function triggerWebhooksFromCron()
    {
        $this->triggerWebhooks();
    }
}


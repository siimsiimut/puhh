<?php 
namespace Makesyoulocal\MakesyoulocalAPI\Model;

class Updates extends \Magento\Framework\Model\AbstractModel {

    public function _construct()
    {
		$this->_init("Makesyoulocal\MakesyoulocalAPI\Model\ResourceModel\Updates");
	}
}

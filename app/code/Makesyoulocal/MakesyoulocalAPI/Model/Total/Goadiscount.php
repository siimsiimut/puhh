<?php
namespace Makesyoulocal\MakesyoulocalAPI\Model\Total;

use Magento\Quote\Model\Quote\Address\Total\AbstractTotal;
use Magento\Quote\Model\Quote;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote\Address\Total;

class Goadiscount extends AbstractTotal
{
    protected $priceCurrency;

    /**
     * Custom constructor.
     */
    public function __construct(
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
    )
    {
        $this->priceCurrency = $priceCurrency;
        $this->setCode('goadiscount');
    }

    /**
     * @param Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Total $total
     * @return $this
     */
    public function collect(
        Quote $quote,
        ShippingAssignmentInterface $shippingAssignment,
        Total $total
    ) {
        parent::collect($quote, $shippingAssignment, $total);

        if ($quote->getGoaDiscountAmount() === null) {
            return $this;
        }

        $baseCurrency = $quote->getBaseCurrencyCode();
        $quoteCurrency = $quote->getQuoteCurrencyCode();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $rate = 1;
        if ($baseCurrency !== $quoteCurrency) {
            $modelRequestedCurrency = $objectManager
                ->create('\Magento\Directory\Model\Currency')
                ->load($baseCurrency);

            $rate = $modelBaseCurrency->getRate($quoteCurrency);

            if ($rate === false) {
                return $this;
            }
        }
        
        $discount = $quote->getData('goa_discount_amount');
        $baseDiscount = $discount / $rate;


        $total->setDiscountAmount($total->getDiscountAmount() - $discount);
        $total->setBaseDiscountAmount($total->getBaseDiscountAmount() - $baseDiscount);
        $this->distributeDiscountRows($total, $quote);

        $total->setBaseGrandTotal($total->getBaseGrandTotal() - $baseDiscount);
        $total->setGrandTotal($total->getGrandTotal() - $discount);

        if ($total->getSubtotal() === $total->getSubtotalWithDiscount()) {
            $total->setSubtotalWithDiscount($total->getSubtotalWithDiscount() - $discount);
            $total->setBaseSubtotalWithDiscount($total->getBaseSubtotalWithDiscount() - $baseDiscount);
        }

        return $this;
    }

    protected function distributeDiscountRows(Total $total, Quote $quote)
    {
        $parentTotal = $total->getBaseSubtotal();
        $keys = [
            'discount_amount',
            'base_discount_amount',
            'original_discount_amount',
            'base_original_discount_amount',
        ];
        $roundingDelta = [];
        foreach ($keys as $key) {
            //Initialize the rounding delta to a tiny number to avoid floating point precision problem
            $roundingDelta[$key] = 0.0000001;
        }
        foreach ($quote->getAllItems() as $child) {
            $ratio = $parentTotal != 0 ? $total->getBaseSubtotal() / $parentTotal : 0;

            foreach ($keys as $key) {
                if (!$total->hasData($key)) {
                    continue;
                }
                $value = $total->getData($key) * $ratio;

                $roundedValue = $this->priceCurrency->round($value + $roundingDelta[$key]);

                $roundingDelta[$key] += $value - $roundedValue;
                $child->setData($key, -1 * $roundedValue);
                if ($child->getHasChildren() && $child->isChildrenCalculated()) {
                    try {
                        $this->distributeDiscountItem($child);
                    } catch (\Exception $ex) {
                        // Ignore Error here
                    }
                }
            }

        }
        return $this;
    }

    protected function distributeDiscountItem(\Magento\Quote\Model\Quote\Item\AbstractItem $item)
    {
        $parentBaseRowTotal = $item->getBaseRowTotal();
        $keys = [
            'discount_amount',
            'base_discount_amount',
            'original_discount_amount',
            'base_original_discount_amount',
        ];
        $roundingDelta = [];
        foreach ($keys as $key) {
            //Initialize the rounding delta to a tiny number to avoid floating point precision problem
            $roundingDelta[$key] = 0.0000001;
        }
        foreach ($item->getChildren() as $child) {
            $ratio = $parentBaseRowTotal != 0 ? $child->getBaseRowTotal() / $parentBaseRowTotal : 0;
            foreach ($keys as $key) {
                if (!$item->hasData($key)) {
                    continue;
                }
                $value = $item->getData($key) * $ratio;
                $roundedValue = $this->priceCurrency->round($value + $roundingDelta[$key]);
                $roundingDelta[$key] += $value - $roundedValue;
                $child->setData($key, $roundedValue);
            }
        }

        foreach ($keys as $key) {
            $item->setData($key, 0);
        }
        return $this;
    }
}

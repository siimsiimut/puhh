<?php
namespace Makesyoulocal\MakesyoulocalAPI\Model\Total;

use Magento\Quote\Model\Quote\Address\Total\AbstractTotal;
use Magento\Quote\Model\Quote;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote\Address\Total;

class Goashipping extends AbstractTotal
{
    /**
     * Custom constructor.
     */
    public function __construct()
    {
        $this->setCode('goashipping');
    }

    /**
     * @param Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Total $total
     * @return $this
     */
    public function collect(
        Quote $quote,
        ShippingAssignmentInterface $shippingAssignment,
        Total $total
    ) {
        parent::collect($quote, $shippingAssignment, $total);

        if ($quote->getGoaShippingAmount() === null) {
            return $this;
        }

        $baseCurrency = $quote->getBaseCurrencyCode();
        $quoteCurrency = $quote->getQuoteCurrencyCode();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $rate = 1;
        if ($baseCurrency !== $quoteCurrency) {
            $modelRequestedCurrency = $objectManager
                ->create('\Magento\Directory\Model\Currency')
                ->load($baseCurrency);

            $rate = $modelBaseCurrency->getRate($quoteCurrency);

            if ($rate === false) {
                return $this;
            }
        }

        $total->setTotalAmount('shipping', $quote->getData('goa_shipping_amount'));
        $total->setBaseTotalAmount('shipping', $quote->getData('goa_shipping_amount') / $rate);
        $total->setTotalAmount('shipping', $quote->getData('goa_shipping_amount'));
        $total->setBaseTotalAmount('shipping', $quote->getData('goa_shipping_amount') / $rate);
        
        return $this;
    }
}

<?php

namespace Makesyoulocal\MakesyoulocalAPI\Setup;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    protected $scopeConfigInterface;
    protected $configWriter;

    public function __construct(
        ScopeConfigInterface $scopeConfigInterface,
        WriterInterface $configWriter
    )
    {
        $this->scopeConfigInterface = $scopeConfigInterface;
        $this->configWriter = $configWriter;
    }

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (!$this->scopeConfigInterface->getValue('makesyoulocalAPI/general/apiUrl')) {
            $this->configWriter->save('makesyoulocalAPI/general/apiUrl', 'https://client.makesyoulocal.com/api/exchange_tokens');
        }

        if (!$this->scopeConfigInterface->getValue('makesyoulocalAPI/general/webhooksUrl')) {
            $this->configWriter->save('makesyoulocalAPI/general/webhooksUrl', 'https://client.makesyoulocal.com/api/v1/magento2');
        }

        if (!$this->scopeConfigInterface->getValue('makesyoulocalAPI/general/apiPublicKey')) {
            $this->configWriter->save('makesyoulocalAPI/general/apiPublicKey', '637b6161a3c3cce08eb094b84b31ff575f3b0a0c7e92d4daff41a97af797666d');
        }

        if (!$this->scopeConfigInterface->getValue('makesyoulocalAPI/general/shopPrivateKey')) {
            $this->configWriter->save('makesyoulocalAPI/general/shopPrivateKey', '');
        }

        if (!$this->scopeConfigInterface->getValue('makesyoulocalAPI/general/shopPublicKey')) {
            $this->configWriter->save('makesyoulocalAPI/general/shopPublicKey', '');
        }

        if (!$this->scopeConfigInterface->getValue('makesyoulocalAPI/general/password')) {
            $pass = mt_rand(10000000, 99999999);
            $this->configWriter->save('makesyoulocalAPI/general/password', (string)$pass);
        }

        if (!$this->scopeConfigInterface->getValue('makesyoulocalAPI/general/content_encryption_key')) {
            $this->configWriter->save(
                'makesyoulocalAPI/general/content_encryption_key',
                bin2hex(openssl_random_pseudo_bytes(32))
            );
        }

        if (!$this->scopeConfigInterface->getValue('makesyoulocalAPI/general/content_encryption_iv')) {
            $this->configWriter->save(
                'makesyoulocalAPI/general/content_encryption_iv',
                bin2hex(openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc')))
            );
        }

        if (!$this->scopeConfigInterface->getValue('makesyoulocalAPI/general/content_encryption_iv')) {
            $this->configWriter->save(
                'makesyoulocalAPI/general/content_encryption_iv',
                bin2hex(openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc')))
            );
        }

        $installer->endSetup();
    }
}


<?php
namespace Makesyoulocal\MakesyoulocalAPI\Block\Adminhtml;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\ObjectManager;

class AdminBlock extends Field
{
    const MODULE_NAME = 'Makesyoulocal_MakesyoulocalAPI';

    protected $_template = 'Makesyoulocal_MakesyoulocalAPI::adminblock.phtml';
    protected $scopeConfigInterface;
    protected $moduleList;
    protected $storeManager;

    public function __construct(
        Context $context,
        array $data = []
    ) {
        $objectManager =  ObjectManager::getInstance();
        $this->scopeConfigInterface = $objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
        $this->moduleList = $objectManager->get('\Magento\Framework\Module\ModuleListInterface');
        $this->productMetadata = $objectManager->get('Magento\Framework\App\ProductMetadataInterface');
        parent::__construct($context, $data);
    }


    public function render(AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

    public function getPassword()
    {
        return $this->scopeConfigInterface->getValue('makesyoulocalAPI/general/password');
    }

    public function getVersion()
    {

        return $this->moduleList->getOne(self::MODULE_NAME)['setup_version'];
    }

    public function getSystemVersion()
    {
        return $this->productMetadata->getVersion();
    }

    public function getBaseUrl()
    {
        return rtrim($this->_storeManager->getStore()->getBaseUrl(), '/');
    }

    public function getStoreCode()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return $this->_storeManager->getStore($storeId)->getCode();
    }
}

<?php
require_once(dirname(__FILE__) . '/vendor/autoload.php');

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Makesyoulocal_MakesyoulocalAPI',
    __DIR__
);


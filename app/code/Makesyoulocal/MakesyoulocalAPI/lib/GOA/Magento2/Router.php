<?php
namespace GOA\Makesyoulocal\Magento2;

class Router extends \GOA\Makesyoulocal\Common\Router
{
    protected function getAvailableActions()
    {
        return array(
            'CustomersCreate',
            'CategoriesCreate',
            'CategoriesIndex',
            'OrdersCancel',
            'OrdersCreate',
            'CategoriesUpdate',
            'OrdersClose',
            'OrdersCreateShipment',
            'OrdersCreateInvoice',
            'OrdersCapturePayment',
            'OrdersIndex',
            'CustomersIndex',
            'OrdersReturn',
            'ProductsIndex',
            'ProductsCreate',
            'ProductsUpdate',
            'ProductsChangeStock',
            'StatusesIndex',
            'WebhooksCreate',
            'WebhooksDebug',
            'WebhooksClear',
            'WebhooksSubmit',
            'PasswordsUpdate',
            'UrlsUpdate',
            'VerifyKeysUpdate'
        );
    }

    protected function isAdministrator()
    {
        return false;
    }
}

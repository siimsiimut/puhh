<?php

namespace GOA\Makesyoulocal\Magento2\Webhooks;

class Webhooks extends \GOA\Makesyoulocal\Common\Webhooks\Webhooks {
    protected function prepareOrdersPayload($ordersIds, $storeId)
    {
        $orderGetter = new \GOA\Makesyoulocal\Magento2\Api\OrderGetter($this->settings);

        $ordersIds = array_unique($ordersIds);
        $orders = array();

        foreach ($ordersIds as $orderId) {
            $order = $orderGetter->getById($orderId);
            if ($order['ORDER_LINES'] && isset($this->webhooks->addProductEvent)) {
                foreach ($order['ORDER_LINES'] as $orderLine) {
                    $this->storage->addProduct($orderLine['SKU'], $orderLine['SKU'], $storeId);
                }
            }
            array_push($orders, $order);
        }

        return $orders;
    }

    protected function prepareProductsPayload($productIds, $storeId)
    {
        $productGetter = new \GOA\Makesyoulocal\Magento2\Api\ProductGetter($this->settings);

        $productIds = array_unique($productIds);
        $products = array();
        foreach ($productIds as $productId) {
            array_push($products, $productGetter->getById($productId));
        }

        return $products;
    }
}


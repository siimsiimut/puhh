<?php

namespace GOA\Makesyoulocal\Magento2\Webhooks;

class Storage extends \GOA\Makesyoulocal\Common\Webhooks\Storage {
    protected $tableName;

    public function __construct($settings)
    {
        parent::__construct($settings);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->updatesFactory = $objectManager->get('\Makesyoulocal\MakesyoulocalAPI\Model\UpdatesFactory');
        $this->updatesCollectionFactory = $objectManager->get('\Makesyoulocal\MakesyoulocalAPI\Model\ResourceModel\Updates\CollectionFactory');
        $this->dateTime = $objectManager->get('\Magento\Framework\Stdlib\DateTime\DateTime');
        $this->scopeConfig = $objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
        $this->configWriter = $objectManager->get('\Magento\Framework\App\Config\Storage\WriterInterface');
        $this->cacheTypeList = $objectManager->get('\Magento\Framework\App\Cache\TypeListInterface');
    }

    protected function getAll($type, $storeId)
    {
        $items = $this->updatesCollectionFactory
            ->create()
            ->addFieldToFilter('type', array('eq' => $type))
            ->addFieldToFilter('store_id', array('eq' => $storeId));

        $norm = array();
        foreach ($items as $item) {
            $norm[$item->getEntityId()] = $item->getEntityId();
        }
        return $norm;
    }

    protected function removeAll($type, $storeId)
    {
        $items = $this->updatesCollectionFactory
            ->create()
            ->addFieldToFilter('type', array('eq' => $type))
            ->addFieldToFilter('store_id', array('eq' => $storeId));

        $norm = array();
        foreach ($items as $item) {
            $norm[$item->getEntityId()] = $item->getEntityId();
            $item->delete();
        }
        return true;
    }

    protected function add($type, $identifier, $item, $storeId)
    {
        $update = $this->updatesFactory->create();

        $update->addData([
            'entity_id' => $identifier,
            'store_id' => $storeId,
            'type' => $type,
            'updated_at' => $this->dateTime->gmtDate()
        ]);
        $update->save();
    }

    protected function readTokens($store)
    {
        $webhooksTokens = $this->scopeConfig->getValue("makesyoulocal/tokens/list", \Magento\Store\Model\ScopeInterface::SCOPE_STORES, $store);

        if ($webhooksTokens) {
            $webhooksTokens = str_replace("\r\n", "\n", $webhooksTokens);
            $webhooksTokens = explode("\n", $webhooksTokens);
        } else {
            $webhooksTokens = array();
        }
        if (!is_array($webhooksTokens)) {
            $webhooksTokens = array();
        }
        $webhooksTokens = array_values(array_unique($webhooksTokens));
        return $webhooksTokens;
    }

    protected function writeTokens($store, $storeTokens)
    {
        $storeTokens = array_values(array_unique($storeTokens));
        $this->configWriter->save('makesyoulocal/tokens/list', implode("\n", $storeTokens), \Magento\Store\Model\ScopeInterface::SCOPE_STORES, $store);

        $this->cacheTypeList->cleanType('config');
    }

    public function addToken($store, $token)
    {
        $webhooksTokens = $this->getTokens($store);
        array_push($webhooksTokens, trim($token));
        $this->writeTokens($store, array_unique($webhooksTokens));

        return $webhooksTokens;
    }

    public function removeToken($store, $token)
    {
        $webhooksTokens = $this->getTokens($store, $token);
        $webhooksTokens = array_filter($webhooksTokens, function($t) use($token) {
            return $t !== $token;
        });
        $this->writeTokens($store, $webhooksTokens);
    }

    public function replaceTokens($store, $tokens)
    {
        $this->writeTokens($store, $webhooksTokens);
    }

    public function getTokens($store)
    {
        return $this->readTokens($store);
    }
}

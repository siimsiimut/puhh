<?php
namespace GOA\Makesyoulocal\Magento2;

class Settings extends \GOA\Makesyoulocal\Common\SettingsBase
{
    protected $version;
    protected $currency;

    public function getVersion()
    {
        if (!$this->version) {
            throw new \Exception('The settings must be initialized with the version option');
        }

        return $this->version;
    }

    public function getCurrency()
    {
        if (!$this->currency) {
            throw new \Exception('The settings must be initialized with the currency option');
        }

        return $this->currency;
    }
}

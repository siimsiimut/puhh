<?php
namespace GOA\Makesyoulocal\Magento2\Actions;

use \GOA\Makesyoulocal\Common\Validators as Va;

class WebhooksCreate extends \GOA\Makesyoulocal\Common\AbstractAction
{
    protected static function parsePayload($payload = null)
    {
        return array(
            'token' => ['token', Va::AlphaNumeric(), new \GOA\Makesyoulocal\Common\Model\Missing(true)],
            'replace_tokens' => ['replace_tokens', Va::Boolean(), false],
            'store_id' => ['store_id', \GOA\Makesyoulocal\Magento2\Validators::StoreId(), new \GOA\Makesyoulocal\Common\Model\Missing(true)],
            'context_store_id' => ['context_store_id', \GOA\Makesyoulocal\Magento2\Validators::StoreId(), new \GOA\Makesyoulocal\Common\Model\Missing(true)]
        );
    }

    protected function execute($payload, $request)
    {
        $storeId = $payload['store_id'];

        $webhooksStorage = new \GOA\Makesyoulocal\Magento2\Webhooks\Storage($this->settings);
        if ($payload['replace_tokens']) {
            $webhooksStorage->replaceTokens($storeId, array($payload['token']));
        } else {
            $webhooksStorage->addToken($storeId, $payload['token']);
        }

        return array('webhooks' => $webhooksStorage->getTokens($storeId));
    }
}


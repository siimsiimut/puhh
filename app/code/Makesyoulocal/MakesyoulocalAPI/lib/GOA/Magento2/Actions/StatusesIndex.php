<?php
namespace GOA\Makesyoulocal\Magento2\Actions;

class StatusesIndex extends \GOA\Makesyoulocal\Common\AbstractAction {
    protected static function parsePayload($payload = null)
    {
        return array(
            'context_store_id' => ['context_store_id', \GOA\Makesyoulocal\Magento2\Validators::StoreId(), new \GOA\Makesyoulocal\Common\Model\Missing(true)]
        );
    }

    protected function execute($payload, $request)
    {
        $storeId = $payload['context_store_id'];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $orderStatusCollectionFactory = $objectManager->get('\Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory');

        $statuses = $orderStatusCollectionFactory->create()->toOptionArray();

        $orderStatesNames = [];
        foreach ($statuses as $status) {
            array_push($orderStatesNames, array(
                'id' => $status['value'],
                'name' => $status['label']
            ));
        }

        return array(
            'header' => $this->settings->getVersion(),
            'statuses' => $orderStatesNames
        );
    }

}


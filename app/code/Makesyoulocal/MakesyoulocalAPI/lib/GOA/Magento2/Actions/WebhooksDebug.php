<?php
namespace GOA\Makesyoulocal\Magento2\Actions;

use \GOA\Makesyoulocal\Common\Validators as Va;

class WebhooksDebug extends \GOA\Makesyoulocal\Common\AbstractAction
{
    protected static function parsePayload($payload = null)
    {
        return array(
            'context_store_id' => ['context_store_id', \GOA\Makesyoulocal\Magento2\Validators::StoreId(), new \GOA\Makesyoulocal\Common\Model\Missing(true)]
        );
    }

    protected function execute($payload, $request)
    {
        $storage = new \GOA\Makesyoulocal\Magento2\Webhooks\Storage($this->settings);
        $storeId = $payload['context_store_id'];

        $productsIds = array_unique(array_values($storage->getAllProducts($storeId)));
        $ordersIds = array_unique(array_values($storage->getAllOrders($storeId)));

        $orderGetter = new \GOA\Makesyoulocal\Magento2\Api\OrderGetter($this->settings);
        $productGetter = new \GOA\Makesyoulocal\Magento2\Api\ProductGetter($this->settings);

        $products = array();
        foreach ($productsIds as $productId) {
            array_push($products, $productGetter->getById($productId));
        }

        $orders = array();
        foreach ($ordersIds as $orderId) {
            array_push($orders, $orderGetter->getById($orderId));
        }

        return array(
            'products' => $products,
            'orders' => $orders,
            'webhooks' => $storage->getTokens($storeId)
        );
    }
}


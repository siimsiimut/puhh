<?php
namespace GOA\Makesyoulocal\Magento2\Actions;

use \GOA\Makesyoulocal\Common\Validators as Va;

class OrdersIndex extends \GOA\Makesyoulocal\Common\AbstractAction {
    protected static function parsePayload($payload = null)
    {
        return array(
            'ids' => ['ids', Va::ArrayOfAlphaNumeric()],
            'updated_since' => ['updated_since', Va::ISODateTime()],
            'created_since' => ['created_since', Va::ISODateTime()],
            'per_page' => ['per_page', Va::Integer(1, 1000), 50],
            'page' => ['page', Va::Integer(0), 1],
            'context_store_id' => ['context_store_id', \GOA\Makesyoulocal\Magento2\Validators::StoreId(), new \GOA\Makesyoulocal\Common\Model\Missing(true)]
        );
    }

    protected function execute($payload, $request)
    {
        $ordersQuery = $this->getOrdersQuery($payload, $request);

        $rows = array();

        $orderGetter = new \GOA\Makesyoulocal\Magento2\Api\OrderGetter($this->settings);
        $querySize = $ordersQuery->getSize();

        foreach ($ordersQuery as $order) {
            array_push($rows, $orderGetter->getById($order));
        }

        $data = array(
            'header' => array(
                'version'   => $this->settings->getVersion(),
                'results'   => $querySize,
                'per_page'  => count($rows)
            )
        );

        if ($payload['per_page'] * $payload['page'] < $querySize) {
            $data['header']['next_page'] = $payload['page'] + 1;
        }

        if ($payload['page'] > 1) {
            $data['header']['previous_page'] = $payload['page'] - 1;
        }
        $data['orders'] = $rows;

        return $data;
    }

    private function getOrdersQuery($payload, $request)
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $orderStatusesCollection = $objectManager->get('\Magento\Sales\Model\Order\Status\History')->getCollection();
        $orderCollectionFactory = $objectManager->get(
            '\Magento\Sales\Model\ResourceModel\Order\CollectionFactory'
        );

        $ordersCollection = $orderCollectionFactory
            ->create()
            ->addFieldToFilter('store_id', $payload['context_store_id'])
            ->setPageSize($payload['per_page'])
            ->setCurPage($payload['page']);

        if (!($payload['ids'] instanceof \GOA\Makesyoulocal\Common\Model\Missing)) {
            $ordersCollection->addAttributeToFilter('increment_id', $payload['ids']);
        }

        if (!($payload['created_since'] instanceof \GOA\Makesyoulocal\Common\Model\Missing)) {
            $ordersCollection->addAttributeToFilter('created_at', array('gteq' => $payload['created_since']->format('Y-m-d H:i:s')));
        }

        if (!($payload['updated_since'] instanceof \GOA\Makesyoulocal\Common\Model\Missing)) {
            $orderStatuses = $orderStatusesCollection
                ->addAttributeToFilter('created_at', array('gteq' => $payload['updated_since']->format('Y-m-d H:i:s')));
            $modifiedOrderIds = array();
            foreach ($orderStatuses as $status) {
                array_push($modifiedOrderIds, $status->getParentId());
            }

            $ordersCollection
                ->addFieldToFilter(array('updated_at', 'entity_id'),
                    array(
                        array('gteq' => $payload['updated_since']->format('Y-m-d H:i:s')),
                        array('in' => $modifiedOrderIds)
                    )
                );
        }

        return $ordersCollection;
    }
}


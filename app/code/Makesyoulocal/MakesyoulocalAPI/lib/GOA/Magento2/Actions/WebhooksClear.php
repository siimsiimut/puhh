<?php
namespace GOA\Makesyoulocal\Magento2\Actions;

use \GOA\Makesyoulocal\Common\Validators as Va;

class WebhooksClear extends \GOA\Makesyoulocal\Common\AbstractAction
{
    protected static function parsePayload($payload = null)
    {
        return array(
            'type' => ['type', Va::AlphaNumeric(), new \GOA\Makesyoulocal\Common\Model\Missing(true)],
            'context_store_id' => ['context_store_id', \GOA\Makesyoulocal\Magento2\Validators::StoreId(), new \GOA\Makesyoulocal\Common\Model\Missing(true)]
        );
    }

    protected function execute($payload, $request)
    {
        $storage = new \GOA\Makesyoulocal\Magento2\Webhooks\Storage($this->settings);
        $storeId = $payload['context_store_id'];

        if ($payload['type'] === 'orders') {
            $storage->removeAllOrders($storeId);
        }

        if ($payload['type'] === 'products') {
            $storage->removeAllProducts($storeId);
        }

        return array(
            'products' => $storage->getAllProducts($storeId),
            'orders' => $storage->getAllOrders($storeId),
            'webhooks' => $storage->getTokens($storeId)
        );
    }
}


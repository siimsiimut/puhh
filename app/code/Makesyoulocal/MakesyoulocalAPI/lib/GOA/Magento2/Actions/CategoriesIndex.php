<?php
namespace GOA\Makesyoulocal\Magento2\Actions;

use \GOA\Makesyoulocal\Common\Validators as Va;

class CategoriesIndex extends \GOA\Makesyoulocal\Common\AbstractAction {
    protected static function parsePayload($payload = null)
    {
        return array(
            'per_page' => ['per_page', Va::Integer(1, 1000), 50],
            'page' => ['page', Va::Integer(0), 1],
            'updated_since' => ['updated_since', Va::ISODateTime()],
            'created_since' => ['created_since', Va::ISODateTime()],
            'context_store_id' => ['context_store_id', \GOA\Makesyoulocal\Magento2\Validators::StoreId(), new \GOA\Makesyoulocal\Common\Model\Missing(true)]
        );
    }

    protected function execute($payload, $request)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeId = $payload['context_store_id'];
        $storeManager = $objectManager
                             ->create('\Magento\Store\Model\StoreManagerInterface');

        $baseUrl = $storeManager
            ->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        $baseUrl = rtrim($baseUrl, '/');

        $allCategories = $objectManager
            ->create('\Magento\Catalog\Model\Category')
            ->setStoreId($storeId);

        $categoryTree = $allCategories
            ->getTreeModel()
            ->load()
            ->getCollection();

        function parseCategoryTree($categories, $base = [], $parentId = 0) {
            $result = array();
            foreach ($categories as $category) {
                if ($category->getParentId() != $parentId) {
                    continue;
                }
                $children = $category->getChildrenCategories();
                $newBase = array_merge($base, [$category->getName()]);

                array_push($result, array(
                    $category->getId(),
                    $parentId,
                    $newBase
                ));
                if ($children->getSize() > 0) {
                    $result = array_merge(
                        $result,
                        parseCategoryTree($children, $newBase, $category->getId())
                    );
                }
            } 
            return $result;
        }

        $themes = $objectManager
            ->create('\Magento\Theme\Model\Theme\Source\Theme')
            ->toOptionArray();
        $themeIdToName = [];
        foreach ($themes as $theme) {
            $themeIdToName[$theme['value']] = $theme['label'];
        }

        $pageLayouts = $objectManager
            ->create('\Magento\Cms\Model\Page\Source\PageLayout')
            ->toOptionArray();
        $layoutIdToName = [];
        foreach ($pageLayouts as $layout) {
            $layoutIdToName[$layout['value']] = $layout['label'];
        }

        $flatCategories = parseCategoryTree($categoryTree);

        $numResults = count($flatCategories);
        $flatCategories = array_slice(
            $flatCategories,
            ($payload['page'] - 1) * $payload['per_page'],
            $payload['per_page']
        );

        $flatCategories = array_map(function($x) use(
            $objectManager,
            $baseUrl,
            $themeIdToName,
            $layoutIdToName
        ) {
            $category = $objectManager
                ->create('\Magento\Catalog\Model\Category')
                ->load($x[0]); 
            $image = $category->getImageUrl();

            $layout = [];
            if ($category->getCustomDesign()
                && isset($themeIdToName[$category->getCustomDesign()])) {
                array_push($layout, $themeIdToName[$category->getCustomDesign()]);
            }
            if (isset($layoutIdToName[$category->getPageLayout()])) {
                array_push($layout, $layoutIdToName[$category->getPageLayout()]);
            }
            $layout = implode(':', $layout);

            return array(
                'id' => $x[0],
                'name' => $category->getName(),
                'path' => $category->getUrlKey(),
                'parent_id' => $x[1] === 0 ? null : $x[1],
                'breadcrumbs' => $x[2],
                'description_1' => $category->getDescription(),
                'images' => $image ? [[
                    'url' => $baseUrl . $image,
                ]] : [],
                'meta_title' => $category->getMetaTitle(),
                'meta_description' => $category->getMetaDescription(),
                'searchable' => $category->getIncludeInMenu() === '1',
                'template' => strlen($layout) ? $layout : null,
                'published' => $category->getIsActive()
            );
        }, $flatCategories);

        $data = array(
            'header' => array(
                'version'   => $this->settings->getVersion(),
                'results'   => $numResults,
                'per_page'  => count($flatCategories)
            ),
            'categories' => $flatCategories
        );

        if ($payload['per_page'] * $payload['page'] < $numResults) {
            $data['header']['next_page'] = $payload['page'] + 1;
        }

        if ($payload['page'] > 1) {
            $data['header']['previous_page'] = $payload['page'] - 1;
        }
        return $data;
    }
}


<?php
namespace GOA\Makesyoulocal\Magento2\Actions;

use \GOA\Makesyoulocal\Common\Validators as Va;
use Magento\Framework\App\ObjectManager;

class ProductsIndex extends \GOA\Makesyoulocal\Common\AbstractAction {
    protected static function parsePayload($payload = null)
    {
        return array(
            'ids' => ['ids', Va::ArrayOfAlphaNumeric()],
            'updated_since' => ['updated_since', Va::ISODateTime()],
            'created_since' => ['created_since', Va::ISODateTime()],
            'per_page' => ['per_page', Va::Integer(1, 1000), 50],
            'page' => ['page', Va::Integer(0), 1],
            'context_store_id' => ['context_store_id', \GOA\Makesyoulocal\Magento2\Validators::StoreId(), new \GOA\Makesyoulocal\Common\Model\Missing(true)]
        );
    }

    protected function execute($payload, $request)
    {
        $productsQuery = $this->getProductsQuery($payload, $request);
        $prodctsWithParents = $this->getProdctsWithParents($productsQuery);

        $rows = array();
        $querySize = $productsQuery->getSize();

        $productGetter = new \GOA\Makesyoulocal\Magento2\Api\ProductGetter($this->settings);

        foreach ($prodctsWithParents as $product) {
            array_push($rows, $productGetter->getById($product));
        }

        $data = array(
            'header' => array(
                'version'   => $this->settings->getVersion(),
                'results'   => $querySize,
                'per_page'  => count($rows)
            )
        );

        if ($payload['per_page'] * $payload['page'] < $querySize) {
            $data['header']['next_page'] = $payload['page'] + 1;
        }

        if ($payload['page'] > 1) {
            $data['header']['previous_page'] = $payload['page'] - 1;
        }
        $data['products'] = $rows;

        return $data;
    }

    protected function getProductsQuery($payload, $request)
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $productCollectionFactory = $objectManager->get('\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
        $collection = $productCollectionFactory->create();
        $collection->addStoreFilter($payload['context_store_id']);
        $collection->addAttributeToSelect('*');
        $collection->setPageSize($payload['per_page']);
        $collection->setCurPage($payload['page']);

        if (!($payload['ids'] instanceof \GOA\Makesyoulocal\Common\Model\Missing)) {
            $collection->addFieldToFilter('sku', array('in' => $payload['ids']));
        }

        if (!($payload['updated_since'] instanceof \GOA\Makesyoulocal\Common\Model\Missing)) {
            $interval = new \DateInterval('PT' . $payload['updated_since']->format('Z') . 'S');
            $result = $payload['updated_since']->sub($interval);
            $collection->addFieldToFilter('updated_at', array('gteq' => $result->format('Y-m-d H:i:s')));
        }

        if (!($payload['created_since'] instanceof \GOA\Makesyoulocal\Common\Model\Missing)) {
            $interval = new \DateInterval('PT' . $payload['created_since']->format('Z') . 'S');
            $result = $payload['created_since']->sub($interval);

            $collection->addFieldToFilter('created_at', array('gteq' => $result->format('Y-m-d H:i:s')));
        }

        return $collection;
    }

    protected function getProdctsWithParents($productCollection)
    {
        $idToProduct = array();
        $productIds = array();
        foreach($productCollection as $product) {
            array_push($productIds, $product->getId());
            $idToProduct[(int)$product->getId()] = $product;
        }

        $parentsCollection = $this->getParentsCollection($productIds);
        $parentsIds = array();
        foreach($parentsCollection as $product) {
            array_push($parentsIds, $product->getId());
            $idToProduct[(int)$product->getId()] = $product;
        }

        $parentsChildren = $this->getChildrenIdsForParentsIds($parentsIds);

        $stockData = $this->getStockData(array_keys($idToProduct));

        $result = array();
        foreach($idToProduct as $productId => $product) {
            $parentId = isset($parentsChildren[$productId])
                ? $parentsChildren[$productId]
                : null;

            if (!$parentId) {
                $result[$productId] = array('product' => $product, 'stock' => $stockData[$productId]);
            } else {
                $result[$productId] = array('product' => $product, 'stock' => $stockData[$productId], 'parent' => $idToProduct[$parentId]);
            }
        }

        return array_values($result);
    }

    private function getParentsCollection($childrenIds)
    {
        $objectManager =  ObjectManager::getInstance();
        $ids = $objectManager->create('\Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')
            ->getParentIdsByChild($childrenIds);

        $productCollectionFactory = $objectManager->get('\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
        $collection = $productCollectionFactory
            ->create()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('entity_id', array('in' => $ids));

        return $collection;
    }


    private function getStockData($productIds)
    {
        $stockItemCollectionFactory = ObjectManager::getInstance()
            ->get('\Magento\CatalogInventory\Api\StockStateInterface');

        $mapStockQuantities = array();
        foreach($productIds as $productId) {
            $mapStockQuantities[$productId] = $stockItemCollectionFactory->getStockQty($productId);
        }
        return $mapStockQuantities;
    }

    private function getChildrenIdsForParentsIds($parentsIds)
    {
        $parentsIds = array_map(function($a) { return (int)$a; }, $parentsIds);
        $parentsIds = empty($parentsIds) ? array(-1) : $parentsIds;

        $objectManager = ObjectManager::getInstance(); // Instance of object manager
        $resourceTypeConfigurable = $objectManager
            ->create('\Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable');
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');

        $connection = $resource->getConnection();
        $result = $connection->fetchAll("SELECT product_id, parent_id FROM `"
            . $resourceTypeConfigurable->getMainTable()
            . "` WHERE parent_id in ("
            . join(",", $parentsIds)
            . ")");


        return array_reduce($result, function($acc, $v) {
            $acc[(int)$v["product_id"]] = (int)$v["parent_id"];
            return $acc;
        }, array());
    }
}


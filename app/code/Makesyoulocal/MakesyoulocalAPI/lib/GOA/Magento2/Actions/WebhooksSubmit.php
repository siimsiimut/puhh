<?php
namespace GOA\Makesyoulocal\Magento2\Actions;

use \GOA\Makesyoulocal\Common\Validators as Va;

class WebhooksSubmit extends \GOA\Makesyoulocal\Common\AbstractAction
{
    protected static function parsePayload($payload = null)
    {
        return array(
            'type' => ['type', Va::AlphaNumeric(), new \GOA\Makesyoulocal\Common\Model\Missing(true)],
            'context_store_id' => ['context_store_id', \GOA\Makesyoulocal\Magento2\Validators::StoreId(), new \GOA\Makesyoulocal\Common\Model\Missing(true)]
        );
    }

    protected function execute($payload, $request)
    {
        $storeId = $payload['context_store_id'];

        $storage = new \GOA\Makesyoulocal\Magento2\Webhooks\Storage($this->settings);
        $webhooks = new \GOA\Makesyoulocal\Magento2\Webhooks\Webhooks($this->settings, $storage);

        if ($payload['type'] === $storage::$TYPE_ORDERS) {
            $webhooks->submitOrders($storeId);
        }

        if ($payload['type'] === $storage::$TYPE_PRODUCTS) {
            $webhooks->submitProducts($storeId);
        }

        return $webhooks->getLastError();
    }
}


<?php
namespace GOA\Makesyoulocal\Magento2;
use \GOA\Makesyoulocal\Common\Model\ValidationError as VE;
use \GOA\Makesyoulocal\Common\Validators as Va;
use \GOA\Makesyoulocal\Common\Model\Missing as Missing;

class Validators extends \GOA\Makesyoulocal\Common\Validators {
    public static function StoreId()
    {
        return self::whenNotMissing(function($value) {
            if (is_numeric($value)) {
                return new \GOA\Makesyoulocal\Common\Model\ValueModified($value);
            }
            if (is_string($value)) {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $storeManager = $objectManager->create('\Magento\Store\Model\StoreManagerInterface');
                $stores = $storeManager->getStores();
                foreach ($stores as $store) {
                    if ($store->getCode() === $value) {
                        return new \GOA\Makesyoulocal\Common\Model\ValueModified($store->getId());
                    }
                }
            }

            return new VE("Field %s must be store id");
        });
    }

    public static function Status()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $orderStatusCollectionFactory = $objectManager
            ->get('\Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory');

        $statuses = $orderStatusCollectionFactory->create()->toOptionArray();

        $statusesAsStr = implode(',', array_map(function($x) {
            return $x['value'];
        }, $statuses));

        return self::whenNotMissing(function($value) use ($statuses, $statusesAsStr) {
            foreach ($statuses as $status) {
                if ($status['label'] === $value || $status['value'] === $value) {
                    return new \GOA\Makesyoulocal\Common\Model\ValueModified($status['value']);
                }
            }
            return new VE("Field %s must be an existing order status. Available options: [$statusesAsStr]");
        });
    }

    public static function UniqueSku()
    {
        return self::whenNotMissing(function($value) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $exProduct = $objectManager->create('\Magento\Catalog\Model\Product')
                                       ->loadByAttribute('sku', $value);
            if (!$exProduct || !$exProduct->getId()) {
                return new \GOA\Makesyoulocal\Common\Model\ValueModified($value);
            }
            return new VE("Product with %s: $value already exists");
        });
    }

    public static function ProductBySku()
    {
        return self::whenNotMissing(function($value) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $exProduct = $objectManager->create('\Magento\Catalog\Model\Product')
                                       ->loadByAttribute('sku', $value);
            if (!$exProduct || !$exProduct->getId()) {
                return new VE("Product with %s: $value does not exist");
            }

            return new \GOA\Makesyoulocal\Common\Model\ValueModified($exProduct);
        });
    }


    public static function Attribute($attributeName) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $attribute = $objectManager
            ->create('\Magento\Eav\Model\ResourceModel\Entity\Attribute\Collection')
            ->addFieldToFilter('attribute_code', $attributeName);

        $attributeValue = $objectManager
            ->create('Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection')
            ->setAttributeFilter($attribute->getData('attribute_id'))
            ->setStoreFilter(0, false);

        $options = $attributeValue->getData();
        $optionsAsStr = implode(',', array_map(function($o) { return $o['value']; }, $options));
        return self::whenNotMissing(function($value) use ($options, $optionsAsStr, $attributeName) {
            $foundOptionId = null;
            foreach ($options as $option) {
                if ($option['value'] === $value) {
                    $foundOptionId = $option['option_id'];
                }
            }

            if ($foundOptionId === null) {
                return new VE("Field %s must be an existing $attributeName name. Available options: [$optionsAsStr]");
            }

            return $foundOptionId;
        });
    }

    public static function TaxClass()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $options = $objectManager
            ->create('\Magento\Tax\Model\TaxClass\Source\Product')
            ->toOptionArray();
        
        return self::whenNotMissing(function($value) use ($options) {
            $availableOptions = array();
            foreach ($options as $option) {
                if ((is_string($option['label']) ? $option['label'] : $option['label']->getText()) === $value) {
                    return new \GOA\Makesyoulocal\Common\Model\ValueModified((int)$option['value']);
                }
                if ($option['value'] == $value) {
                    return new \GOA\Makesyoulocal\Common\Model\ValueModified((int)$option['value']);
                }
                array_push($availableOptions, $option['label']);
            }
            $availableOptionsAsStr = implode(',', $availableOptions);
            return new VE("Field %s must be an existing tax class. Available options: [$availableOptionsAsStr]");
        });
    }

    public static function Category() {
        $categoriesMap = array();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $categories = $objectManager
            ->create('\Magento\Catalog\Model\Category')
            ->getCollection();

        foreach ($categories as $category) {
            $categoriesMap[$category->getId()] = $category;
        }

        return self::whenNotMissing(function($value) use ($categoriesMap) {
            if (isset($categoriesMap[$value])) {
                return new \GOA\Makesyoulocal\Common\Model\ValueModified($value);
            }

            return new VE("Field %s must be an existing category");
        });
    }

    public static function ProductSku()
    {
        return self::whenNotMissing(function($value) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $id = $objectManager
                ->create('\Magento\Catalog\Model\Product')
                ->getIdBySku($value);
            if ($id === false) {
                return new VE("SKU %s: $value does not exist");
            }
            return new \GOA\Makesyoulocal\Common\Model\ValueModified($id);
        });
    }

    public static function Variations($spec) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $entityType = $objectManager
            ->create('\Magento\Catalog\Model\Product')
            ->getResource()
            ->getTypeId();

        $attributeSetCollection = $objectManager
            ->create('Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection')
            ->setEntityTypeFilter($entityType);

        $attributeToSet = array();
        foreach ($attributeSetCollection as $attributeSet) {
            $attributeSetId = $attributeSet->getId();

            $attributes = $objectManager
                ->create('\Magento\Catalog\Model\Product')
                ->getResource()
                ->loadAllAttributes()
                ->getSortedAttributes($attributeSetId);


            foreach ($attributes as $attribute) {
                if (!isset($attributeToSet[$attribute['attribute_id']])) {
                    $attributeToSet[$attribute['attribute_id']] = array(
                        $attributeSetId
                    );
                } else {
                    array_push(
                        $attributeToSet[$attribute['attribute_id']],
                        $attributeSetId
                    );
                }
            }
        }
        return self::whenNotMissing(function($value, $settings, $full) use ($spec, $attributeToSet) {
            $value = $spec($value, $settings, $full);

            if ($value instanceof VE) {
                return $value;
            }
            $variationsInfos = $value->value;
            $possibleSets = null;

            foreach ($variationsInfos as $variationInfo) {
                $setsIds = isset($attributeToSet[$variationInfo['attribute']])
                    ? $attributeToSet[$variationInfo['attribute']]
                    : array();

                if ($possibleSets === null) {
                    $possibleSets = $setsIds;
                } else {
                    $possibleSets = array_intersect($possibleSets, $setsIds);
                }
            }

            if (count($possibleSets) === 0) {
                return new VE("Field %s contains configurable attributes that do not exist in common attribute_set. Create attribute set in magento first");
            }

            foreach ($variationsInfos as &$variationInfo) {
                $variationInfo['set'] = $possibleSets;
            }

            return new \GOA\Makesyoulocal\Common\Model\ValueModified($variationsInfos);
        });
    }

    public static function AttributeValue($spec) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $eavConfig = $objectManager->get('\Magento\Eav\Model\Config');
        $attributesCollection = $eavConfig
            ->getEntityType($objectManager->get('\Magento\Catalog\Model\Product')::ENTITY)
            ->getAttributeCollection()
            ->addSetInfo();
        
        $attributes = array();
        foreach ($attributesCollection as $attribute) {
            array_push($attributes, array(
                $attribute->getData(),
                $attribute->getSource()->getAllOptions(false),
            ));
        }

        return self::whenNotMissing(function($value, $settings, $full) use ($attributes, $spec) {
            $value = self::ObjectOf($spec)($value, $settings, $full);

            if ($value instanceof VE) {
                return $value;
            }
            $value = $value->value;

            $searchedType = $value['type'];
            $searchedValue = $value['value'];
            $foundAttributeWithOptions = null;

            foreach($attributes as $attributeWithOptions) {
                $attribute = $attributeWithOptions[0];

                /* if (!isset($attribute['is_configurable']) || !(+($attribute['is_configurable']))) { */
                /*     continue; */ 
                /* } */


                if ($attribute['attribute_code'] === $searchedType) {
                    $foundAttributeWithOptions = $attributeWithOptions;
                } 
                if ($attribute['frontend_label'] === $searchedType) {
                    $foundAttributeWithOptions = $attributeWithOptions;
                } 
            }

            if (!$foundAttributeWithOptions) {
                return new VE("Field %s must be an existing attribute code / label");
            }

            $availableOptions = $foundAttributeWithOptions[1];
            $foundAttribute = $foundAttributeWithOptions[0];
            $foundOption = null;
            foreach ($availableOptions as $option) {
                if ($option['value'] === $searchedValue) {
                    $foundOption = $option;
                } 
                if ($option['label'] === $searchedValue) {
                    $foundOption = $option;
                } 
            }

            if (!$foundOption) {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

                $optionLabelFactory = $objectManager
                    ->create('\Magento\Eav\Api\Data\AttributeOptionLabelInterface');
                $optionFactory = $objectManager
                    ->create('\Magento\Eav\Api\Data\AttributeOptionInterface');
                $storeManager = $objectManager
                    ->create('\Magento\Store\Model\StoreManagerInterface');

                $optionLabel = $optionLabelFactory;
                $optionLabel->setStoreId($storeManager->getStore()->getId());
                $optionLabel->setLabel($searchedValue);

                $option = $optionFactory;
                $option->setLabel($searchedValue);
                $option->setStoreLabels([$optionLabel]);
                $option->setSortOrder(0);
                $option->setIsDefault(false);

                $objectManager
                    ->create('\Magento\Eav\Api\AttributeOptionManagementInterface')
                    ->add(
                        \Magento\Catalog\Model\Product::ENTITY,
                        $foundAttribute['attribute_id'],
                        $option
                    );

                $eavConfig = $objectManager->get('\Magento\Eav\Model\Config');
                $attributeEntity = $eavConfig
                    ->getAttribute(
                        'catalog_product',
                        $foundAttribute['attribute_code']
                    );

                $allOptions = $attributeEntity->getSource()->getAllOptions(true, true);
                $foundOption = array_values(array_filter(
                    $allOptions, 
                    function($x) use ($searchedValue) {
                        return $x['label'] === $searchedValue;
                    }
                ))[0];
                array_push($availableOptions, $foundOption);
            }

            return new \GOA\Makesyoulocal\Common\Model\ValueModified(array(
                'attribute' => $foundAttribute['attribute_id'],
                'attribute_code' => $foundAttribute['attribute_code'],
                'option' => $foundOption['value']
            ));
        });
    }

    public static function PaymentMethod()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $payments = $objectManager->get('Magento\Payment\Model\Config')->getActiveMethods();
        return self::whenNotMissing(function($value) use ($payments) {
            $availableCodes = array();
            foreach ($payments as $code => $payment) {
                if ($code === $value) {
                    return new \GOA\Makesyoulocal\Common\Model\ValueModified($code);
                }
                array_push($availableCodes, $code);
            }
            $availableCodesAsStr = implode(',', $availableCodes);
            return new VE("Field %s must be an existing order payment method. Available options: [$availableCodesAsStr]");
        });
    }

    public static function DeliveryMethod()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $carriers = $objectManager->get('Magento\Shipping\Model\Config')->getActiveCarriers();
        $labelToCode = array();
        $codeToLabel = array();

        $storeConfig = $objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');

        foreach($carriers as $carrierCode => $_carrier) {
            $carrierTitle = $storeConfig->getValue("carriers/$carrierCode/title");
            $methods = $_carrier->getAllowedMethods();
            foreach ($methods as $methodCode => $methodLabel) {
                $code = $carrierCode . '_' . $methodCode;
                $label = $carrierTitle . ' ' . $methodLabel;
                $codeToLabel[$code] = $label;
                $labelToCode[$label] = $code;
            }
        }
        $allAvailable = array_merge(array_keys($codeToLabel), array_values($codeToLabel));
        $allAvailable = implode(',', $allAvailable);

        return self::whenNotMissing(function($value) use ($labelToCode, $codeToLabel, $allAvailable) {
            if (isset($codeToLabel[$value])) {
                return new \GOA\Makesyoulocal\Common\Model\ValueModified($value);
            }
            if (isset($labelToCode[$value])) {
                return new \GOA\Makesyoulocal\Common\Model\ValueModified($labelToCode[$value]);
            }
            return new VE("Field %s must be an existing order delivery method. Available options: [$allAvailable]");
        });
    }

    public static function Country()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $countriesCollection = $objectManager
            ->create('\Magento\Directory\Model\ResourceModel\Country\CollectionFactory');

        $countries = $countriesCollection->create()->getData();

        return self::whenNotMissing(function($value) use($countries) {
            $value = strtoupper($value);
            $countryIds = [];
            foreach ($countries as $country) {
                array_push($countryIds, $country['country_id']);
                if ($country['country_id'] === $value) {
                    return new \GOA\Makesyoulocal\Common\Model\ValueModified($value);
                }
                if (strtoupper($country['iso2_code']) === $value || strtoupper($country['iso3_code']) === $value) {
                    return new \GOA\Makesyoulocal\Common\Model\ValueModified($country['country_id']);
                }
            }
            $countriesAsStr = implode(',', $countryIds);
            return new VE("Field %s must be an existing country. Available options: [$countriesAsStr]");
        });
    }

    public static function Address($extraSchema = [])
    {
        $schemaOfAddress = array_merge(array(
            'first_name' => ['first_name', Va::Text(), new Missing(true)],
            'last_name' => ['last_name', Va::Text(), new Missing(true)],
            'name' => ['name', Va::Text()],
            'company' => ['company', Va::Text()],
            'address_1' => ['address_1', Va::Text(), new Missing(true)],
            'address_2' => ['address_2', Va::Text()],
            'zip_code' => ['zip_code', Va::Text(), new Missing(true)],
            'city' => ['city', Va::Text(), new Missing(true)],
            'province' => ['province', Va::Text()],
            'vat_number' => ['vat_number', Va::Text()],
            'country' => ['country', Va::ObjectOf([
                'id' => ['id', static::Country(), new \GOA\Makesyoulocal\Common\Model\Getter('country')]
            ]), array()],
            'phone' => ['phone', Va::Phone(), new Missing(true)],
        ), $extraSchema);
        
        return self::whenNotMissing(function($value, $settings, $full) use($schemaOfAddress) {
            $result = static::ObjectOf($schemaOfAddress)($value, $settings, $full);

            $error = new VE("Field %s must be correct address representation");
            if ($result instanceof VE) {
                $error->merge($result);
                return $error;
            } else if ($result instanceof \GOA\Makesyoulocal\Common\Model\ValueModified) {
                $result = $result->value;
            }

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

            $regionId = null;
            if ($result['province'] && !($result['province'] instanceof Missing)) {
                $regionModel = $objectManager->create('\Magento\Directory\Model\Region');


                $regionsCollection = $objectManager
                    ->create('\Magento\Directory\Model\ResourceModel\Region\CollectionFactory');

                $regions = $regionsCollection
                    ->create()
                    ->addCountryFilter($result['country']['id'])
                    ->getData();


                $regionsAsArr = array();
                $regionId = null;

                foreach ($regions as $region) {
                    if (strtolower($region['code']) === strtolower($result['province'])) {
                        $regionId = (int)$region['region_id'];
                    }
                    if (strtolower($region['name']) === strtolower($result['province'])) {
                        $regionId = (int)$region['region_id'];
                    }
                    array_push($regionsAsArr, $region['code']);
                    array_push($regionsAsArr, $region['name']);
                }

                if (!$regionId) {
                    $regionsAsStr = implode(',', $regionsAsArr);
                    $errorRegion = new VE("Field province must be one of the [$regionsAsStr]", 'province');
                    $error->merge($errorRegion);
                    return $error;
                }
            }

            $norm = array(
                'firstname' => $result['first_name'],
                'lastname' => $result['last_name'],
                'country_id' => $result['country']['id'],
                'street' => array()
            );

            if (!($result['address_1'] instanceof Missing)) {
                $norm['street'][] = $result['address_1'];
            }
            if (!($result['address_2'] instanceof Missing)) {
                $norm['street'][] = $result['address_2'];
            }
            if (!($result['company'] instanceof Missing)) {
                $norm['company'] = $result['company'];
            }
            if (!($result['zip_code'] instanceof Missing)) {
                $norm['postcode'] = $result['zip_code'];
            }
            if (!($result['phone'] instanceof Missing)) {
                $norm['telephone'] = $result['phone'];
            }
            if (!($result['city'] instanceof Missing)) {
                $norm['city'] = $result['city'];
            }
            if ($regionId ) {
                $norm['region_id'] = $regionId;
                $norm['province'] = $result['province'];
            } else {
                $norm['region'] = null;
                $norm['region_id'] = null;
            }
            if (!($result['vat_number'] instanceof Missing)) {
                $norm['vat_id'] = $result['vat_number'];
            }

            return new \GOA\Makesyoulocal\Common\Model\ValueModified([$norm, $result]);
        });
    }

    public static function CustomerGroup() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $groups = $objectManager
            ->create('\Magento\Customer\Model\Group')
            ->getCollection();

        return self::whenNotMissing(function($value) use ($groups)
        {
            if ($value === true) {
                $value = 'General';
            }
            $allowedGroups = [];
            foreach ($groups as $group) {
                if ($group->getCustomerGroupId() === $value 
                    || $group->getCustomerGroupCode() === $value) {
                    return new \GOA\Makesyoulocal\Common\Model\ValueModified($group);
                }

                array_push($allowedGroups, $group->getCustomerGroupCode());
            }

            $allowedGroupsStr = implode(',', $allowedGroups);
            return new VE("Field %s must be an existing group. Available options: [$allowedGroupsStr]");
        });
    }

    public static function Customer()
    {
        return self::whenNotMissing(function($value, $settings, $full) {
            $error = new VE("Field %s must be correct Customer");
            $storeResult = static::StoreId()($full['context_store_id'], $settings, $full);

            if ($storeResult instanceof VE) {
                $error->merge($storeResult);
                return $error;
            } else {
                $storeResult = $storeResult->value;
            }
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

            $store = $objectManager->create('\Magento\Store\Model\StoreManagerInterface')
                ->getStore();
            $customerModel = $objectManager->create('\Magento\Customer\Model\Customer');

            $customer = null;
            if (gettype($value) === 'string' && strpos($value, '@') !== False) {
                $customer = $customerModel
                    ->setWebsiteId($store->getWebsiteId())
                    ->loadByEmail($value);
            } else {
                $customer = $customerModel->load($value);
            }
            if (!$customer || !$customer->getId()) {
                return new VE("Customer with the ID: " . $value . " does not exists");
            }
            return new \GOA\Makesyoulocal\Common\Model\ValueModified($customer);
            /* } */
        });
    }

    public static function UniquePath()
    {
        return self::whenNotMissing(function($value, $settings, $full) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

            $exProduct = $objectManager->create('\Magento\Catalog\Model\Product')
                                    ->loadByAttribute('url_key', $value);
            if ($exProduct !== false) {
                return new VE("Path: " . $value . " already exists. Please choose another path");
            }

            $duplicated = -1; 
            array_walk_recursive($full, function($v, $k) use ($value, &$duplicated) {
                if ($k === 'path') {
                    if ($value === $v) {
                        $duplicated += 1;
                    }
                } 
            });
            if ($duplicated > 0) {
                return new VE("Path: " . $value . " must be unique within payload. Please choose another path");
            }
            return new \GOA\Makesyoulocal\Common\Model\ValueModified($value);
        });
    }

    public static function ImageId() {
        return self::whenNotMissing(function($value) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $resourceConnection = $objectManager->get('\Magento\Framework\App\ResourceConnection');

            $select = $resourceConnection->getConnection()->select()
                ->from($resourceConnection->getTableName('catalog_product_entity_media_gallery'))
                ->where('value_id = ? ', (int)$value);

            $result = $resourceConnection->getConnection()->fetchAll($select);
            if (count($result)) {
                return new \GOA\Makesyoulocal\Common\Model\ValueModified($result[0]);
            }

            return new VE("Field %s must be an existing image id");
        });
    }
}

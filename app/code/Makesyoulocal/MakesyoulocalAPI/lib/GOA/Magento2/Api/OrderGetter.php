<?php
namespace GOA\Makesyoulocal\Magento2\Api;

class OrderGetter
{
    protected $settings;

    public function __construct($settings)
    {
        $this->settings = $settings;
    }

    public function getById($order)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        if (is_int($order) || is_string($order)) {
            $order = $objectManager->create('\Magento\Sales\Model\Order')->load($order);
        }

        $shippingAddress = null;
        if ($order->getShippingAddress()) {
            $shippingAddress = $this->getAddress($order->getShippingAddress());
        }

        $billingAddress = null;
        if ($order->getBillingAddress()) {
            $billingAddress = $this->getAddress($order->getBillingAddress());
        }

        $payment = null;
        try {
            $paymentEntity = $order->getPayment()->getMethodInstance();
            if ($paymentEntity->getCode()) {
                $payment = array(
                    'ID'    => $paymentEntity->getCode(),
                    'NAME'  => $paymentEntity->getTitle(),
                    'PRICE' => new \GOA\Makesyoulocal\Common\Model\NotImplemented()
                );
            }
        } catch (\Exception $ex) {
            // ignore error
        }

        $shippings = array();
        $shippingMethodName = $order->getShippingDescription();
        $shippingMethodCode = $order->getShippingMethod();
        if ($shippingMethodName) {
            array_push($shippings, array(
                'ID'    => $shippingMethodCode,
                'NAME'  => $shippingMethodName,
                'PRICE' => new \GOA\Makesyoulocal\Common\Model\Currency($order->getData('shipping_incl_tax'))
            ));
        }

        $comments = $order->getStatusHistoryCollection(true);
        $events = array();
        foreach ($comments as $comment) {
            $status = $comment->getStatus();
            $event = array(
                'STATUS'    => $status,
                'CREATED_AT'      => new \GOA\Makesyoulocal\Common\Model\DatetimeFormatter(new \DateTimeImmutable($comment->getCreatedAt())),
            );
            if ($comment->getComment()) {
                $event['COMMENT'] = $comment->getComment();
            }
            array_push($events, $event);
        }

        $shipments = [];
        $trackCollectionFactory = $objectManager->create('\Magento\Sales\Model\ResourceModel\Order\Shipment\Track\CollectionFactory');

        foreach ($order->getShipmentsCollection() as $orderShipment) {
            $shipmentItemsCollection = $orderShipment->getItems();
            $shipmentItems = array();

            $tracking = $trackCollectionFactory->create()->setShipmentFilter($orderShipment->getId());
            $trackingItem = $tracking->getFirstItem();

            foreach ($shipmentItemsCollection as $shipmentItem) {
                $sku = $shipmentItem->getData('sku');
                if ($shipmentItem->getQty() > 0) {
                    array_push($shipmentItems, array(
                        'SKU' => $sku,
                        'QUANTITY' => (int)$shipmentItem->getQty(),
                    )); 
                }
            }

            array_push($shipments, array(
                'ID' => $orderShipment->getId(),
                'COURIER'   => $trackingItem->getTitle() ? $trackingItem->getTitle() : $order->getData('shipping_description'),
                'TRACKING' => $trackingItem->getTrackNumber() ? $trackingItem->getTrackNumber() : new \GOA\Makesyoulocal\Common\Model\NotImplemented(),
                'SHIPPED_AT'      => new \GOA\Makesyoulocal\Common\Model\DatetimeFormatter(new \DateTimeImmutable($orderShipment->getCreatedAt())),
                'ORDER_LINES' => $shipmentItems
            ));
        }

        $orderLines = $this->getOrderLines($order);
        $invoices = $this->getOrderInvoices($order);
        $creditNotes = $this->getOrderCreditNotes($order);

        return array(
            'ORDER_NUMBER'          => $order->getIncrementId(),
            'ID'                    => $order->getId(),
            'CREATED_AT'            => new \GOA\Makesyoulocal\Common\Model\DatetimeFormatter(new \DateTimeImmutable($order->getCreatedAt())),
            'UPDATED_AT'            => new \GOA\Makesyoulocal\Common\Model\DatetimeFormatter(new \DateTimeImmutable($order->getUpdatedAt())),
            'STATUS'                => $order->getStatus(),
            'EMAIL'                 => $order->getCustomerEmail(),
            'PRICE'                 => new \GOA\Makesyoulocal\Common\Model\Currency($order->getGrandTotal()),
            'CURRENCY'              => $order->getOrderCurrencyCode(),
            'CUSTOMER_NOTE'         => new \GOA\Makesyoulocal\Common\Model\NotImplemented(),
            'COMPANY_NOTE'          => new \GOA\Makesyoulocal\Common\Model\NotImplemented(),
            'BROWSER_IP'            => $order->getRemoteIp(),
            'BILLING_ADDRESS'       => $billingAddress,
            'SHIPPING_ADDRESS'      => $shippingAddress,
            'PAYMENT'               => $payment,
            'INVOICES'              => count($invoices) ? $invoices : null,
            'CREDIT_NOTES'          => count($creditNotes) ? $creditNotes : null,
            'SHIPPING'              => count($shippings) ? $shippings : null,
            'ORDER_LINES'           => count($orderLines) ? $orderLines : null,
            'SHIPMENTS'             => count($shipments) ? $shipments : null,
            'EVENTS'                => count($events) ? $events : null,
            'DISCOUNT_CODES'        => $order->getCouponCode() ? [$order->getCouponCode()] : null,
            'DISCOUNT_AMOUNT'       => $order->getDiscountAmount() == 0 ? null : (double)$order->getDiscountAmount() * -1
        );
    }

    private function getOrderLines($order)
    {
        $orderLines = array();
        foreach ($order->getAllVisibleItems() as $item) {
            $orderLine = array(
                'SKU'           => $item->getData('sku'),
                'NAME'          => $item->getData('name'),
                'QUANTITY'      => (int)$item->getData('qty_ordered'),
                'PRICE'         => new \GOA\Makesyoulocal\Common\Model\Currency($item->getData('price_incl_tax'))
            );
            array_push($orderLines, $orderLine);
        }

        return $orderLines;
    }

    private function getOrderCreditNotes($order)
    {
        $creditNotes = array();
        foreach ($order->getCreditmemosCollection() as $creditmemo) {
            $creditmemoItemsCollection = $creditmemo->getItems();
            $creditmemoItems = array();

            foreach ($creditmemoItemsCollection as $creditmemoItem) {
                $sku = $creditmemoItem->getData('sku');
                array_push($creditmemoItems, array(
                    'SKU' => $sku,
                    'QUANTITY' => (int)$creditmemoItem->getQty(),
                    'AMOUNT' => new \GOA\Makesyoulocal\Common\Model\Currency($creditmemoItem->getPrice() / $creditmemoItem->getQty()),
                )); 
            }

            array_push($creditNotes, array(
                'ID' => $creditmemo->getId(),
                'AMOUNT' => new \GOA\Makesyoulocal\Common\Model\Currency($creditmemo->getGrandTotal()),
                'REFUNDED_AMOUNT' => new \GOA\Makesyoulocal\Common\Model\Currency($creditmemo->getGrandTotal()),
                'SHIPPING_AMOUNT' => new \GOA\Makesyoulocal\Common\Model\Currency($creditmemo->getShippingAmount()),
                'CREDITED_AT' => new \GOA\Makesyoulocal\Common\Model\DatetimeFormatter(new \DateTimeImmutable($creditmemo->getCreatedAt())),
                'ORDER_LINES' => $creditmemoItems
            ));
        }
        return $creditNotes;
    }

    private function getOrderInvoices($order) 
    {
        $invoices = array();
        foreach ($order->getInvoiceCollection() as $invoice) {
            $invoiceItemsCollection = $invoice->getItems();
            $invoiceItems = array();

            foreach ($invoiceItemsCollection as $invoiceItem) {
                $sku = $invoiceItem->getData('sku');
                array_push($invoiceItems, array(
                    'SKU' => $sku,
                    'QUANTITY' => (int)$invoiceItem->getQty(),
                    'AMOUNT' => new \GOA\Makesyoulocal\Common\Model\Currency($invoiceItem->getPrice() / $invoiceItem->getQty()),
                )); 
            }

            array_push($invoices, array(
                'ID' => $invoice->getId(),
                'AMOUNT' => new \GOA\Makesyoulocal\Common\Model\Currency($invoice->getGrandTotal()),
                'CAPTURED' => $invoice->getState() === $invoice::STATE_PAID,
                'CAPTURED_AMOUNT' => $invoice->getState() !== $invoice::STATE_PAID ? 0 : new \GOA\Makesyoulocal\Common\Model\Currency($invoice->getGrandTotal()),
                'PAID' => $invoice->getState() === $invoice::STATE_PAID,
                'PAID_AMOUNT' => $invoice->getState() !== $invoice::STATE_PAID ? 0 : new \GOA\Makesyoulocal\Common\Model\Currency($invoice->getGrandTotal()),
                'SHIPPING_AMOUNT' => new \GOA\Makesyoulocal\Common\Model\Currency($invoice->getShippingAmount()),
                'INVOICED_AT' => new \GOA\Makesyoulocal\Common\Model\DatetimeFormatter(new \DateTimeImmutable($invoice->getCreatedAt())),
                'ORDER_LINES' => $invoiceItems
            ));
        }
        return $invoices;
    }

    private function getAddress($addressEntity)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $locale = $objectManager->create('\Magento\Framework\Locale\TranslatedLists');

        $streets = explode("\n", $addressEntity->getData('street'));
        $country = $addressEntity->getData('country_id');
        $regionId = $addressEntity->getData('region_id');
        $country_name = "";
        if ($country) {
            $country_name = $locale->getCountryTranslation($country);
        }

        $regionName = null;
        if ($regionId) {
            $region = $objectManager->create('Magento\Directory\Model\Region')->load($regionId);
            if ($region->getId()) {
                $regionName = $region->getName();
            }
        }
        return array(
            'COMPANY'       => $addressEntity->getCompany(),
            'NAME'          => $addressEntity->getFirstname() . ' ' . $addressEntity->getLastname(),
            'VAT_NUMBER'    => $addressEntity->getVatId(),
            'FIRST_NAME'    => $addressEntity->getFirstname(),
            'LAST_NAME'     => $addressEntity->getLastname(),
            'ADDRESS_1'     => $streets[0],
            'ADDRESS_2'     => (count($streets) > 1 && isset($streets[1])) ? $streets[1] : '',
            'CITY'          => $addressEntity->getData('city'),
            'PROVINCE'      => $regionName,
            'ZIP_CODE'      => $addressEntity->getData('postcode'),
            'COUNTRY'       => array(
                'ID'    => $country,
                'NAME'  => $country_name
            ),
            'PHONE'         => $addressEntity->getTelephone()
        );
    }
}

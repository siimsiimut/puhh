<?php
namespace GOA\Makesyoulocal\Magento2\Api;

use PDO;

class ProductGetter extends \GOA\Makesyoulocal\Common\Api\BaseGetter
{
    protected $categoriesDict;
    protected $objectManager;
    protected $settings;

    public function __construct($settings)
    {
        $this->settings = $settings;
        $this->taxClasses = null;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->categoriesDict = $this->getCategoriesDictionary();
        $storeManager = $this->objectManager->create('\Magento\Store\Model\StoreManagerInterface');
        $this->stockManager = $this->objectManager->create('\Magento\CatalogInventory\Api\StockStateInterface'); 
        $this->mediaBaseUrl = $storeManager
            ->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        $this->configurableType = $this->objectManager->create('\Magento\ConfigurableProduct\Model\Product\Type\Configurable');
        $this->groupedType = $this->objectManager->create('\Magento\GroupedProduct\Model\Product\Type\Grouped');

        $this->stockState = $this->objectManager->get('\Magento\InventorySalesAdminUi\Model\GetSalableQuantityDataBySku');
    }

    private function getCategoriesDictionary()
    {
        $categoryCollectionFactory = $this->objectManager->create('\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory');
        $collection = $categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->setPageSize(10000000);

        $categoriesDict = array();
        foreach ($collection as $category) {
            $categoriesDict[$category->getEntityId()] = $category->getName();
        }

        return $categoriesDict;
    }

    private function getParentId($product)
    {
        $parents = $this->configurableType->getParentIdsByChild($product->getId());
         if(isset($parents[0])){
             $parent = $parents[0];
             return $parent;
        }

        return null;
    }

    public function getById($productWithParent)
    {
        $id = null;
        if (is_int($productWithParent) || is_string($productWithParent)) {
            $id = $productWithParent;
            $productWithParent = array();
            $product = $this->objectManager->create('\Magento\Catalog\Model\Product')->load($id);
            $productWithParent['product'] = $product;
            $productWithParent['parent'] = $this->getParentId($product);
        }

        $product = $productWithParent['product'];
        $stockQty = $this->stockManager->getStockQty($product->getId(), $product->getStore()->getWebsiteId());

        try {
            $qty = $this->stockState->execute($product->getSku());
            if (count($qty)) {
                $stockQty = $qty[0]['qty'];
            }
        } catch (\Exception $qty) {
        }

        $parent = isset($productWithParent['parent']) ? $productWithParent['parent'] : null;
        $stock = $this->objectManager
            ->get('Magento\CatalogInventory\Api\StockRegistryInterface')
            ->getStockItem($product->getId());

        if (is_string($parent)) {
            $parent = $this->objectManager
                ->create(\Magento\Catalog\Model\Product::class)
                ->load($parent);

            if (!$parent || !$parent->getId()) {
                $parent = null;
            }
        }
        $variations = array();
        if ($parent) {
            $productAttributeOptions = $parent->getTypeInstance(true)->getConfigurableAttributesAsArray($parent);

            foreach ($productAttributeOptions as $productAttribute) {
                array_push($variations, array(
                    'TYPE' => $productAttribute['label'],
                    'VALUE' => $product->getAttributeText($productAttribute['attribute_code'])
                ));
            }
        }

        $product->load('media_gallery');
        $productImages = $product->getMediaGalleryImages();
        $images = array();
        foreach ($productImages as $productImage) {
            array_push($images, array(
                'ID' => $productImage->getId(),
                'URL' => $productImage->getUrl(),
                'TITLE' => $productImage->getLabel(),
                'POSITION' => $productImage->getPosition()
            ));
        }

        $categories = array();
        $categoryIds = $product->getCategoryIds();
        foreach ($categoryIds as $categoryId) {
            if (isset($this->categoriesDict[$categoryId])) {
                array_push($categories, array(
                    'ID' => $categoryId,
                    'NAME' => $this->categoriesDict[$categoryId]
                ));
            }
        }
        $stockData = $stock->getData();
        $minQty = null;
        if (isset($stockData['min_qty'])) {
            $minQty = $stockData['min_qty'];
        }

        return array(
            'SKU'               => $product->getSku(),
            'MASTER_SKU'        => $parent ? $parent->getSku() : $product->getSku(),
            'PATH'              => $product->getUrlKey(),
            'ID'                => $product->getId(),
            'NAME'              => $product->getName(),
            'STOCK'             => $product->getTypeId() !== 'simple' ? new \GOA\Makesyoulocal\Common\Model\NotImplemented() : (int)$stockQty,
            'ALLOW_BACKORDERS'  => $product->getTypeId() !== 'simple' ? new \GOA\Makesyoulocal\Common\Model\NotImplemented() : (bool)$stock->getBackorders(),
            'MIN_QUANTITY'      => $product->getTypeId() !== 'simple' ? new \GOA\Makesyoulocal\Common\Model\NotImplemented() : $stock->getMinSaleQty(),
            'MAX_QUANTITY'      => $product->getTypeId() !== 'simple' ? new \GOA\Makesyoulocal\Common\Model\NotImplemented() : $stock->getMaxSaleQty(),
            'LOW_STOCK_LIMIT'   => $product->getTypeId() !== 'simple' ? new \GOA\Makesyoulocal\Common\Model\NotImplemented() : (int)$minQty,
            'MANUFACTURER'      => $product->getManufacturer() ? $product->getAttributeText('manufacturer') : null,
            'IN_STOCK'          => new \GOA\Makesyoulocal\Common\Model\NotImplemented(),
            'STOCK_MESSAGE'     => new \GOA\Makesyoulocal\Common\Model\NotImplemented(),
            'DESCRIPTION_1'     => $product->getShortDescription(),
            'DESCRIPTION_2'     => $product->getDescription(),
            'URL'               => $product->getUrlInStore(),
            'PRICE'             => new \GOA\Makesyoulocal\Common\Model\Currency($product->getPrice()),
            'OFFER_PRICE'       => new \GOA\Makesyoulocal\Common\Model\Currency($product->getFinalPrice()),
            'OFFER_STARTED_AT'  => $product->getSpecialFromDate()
                                    ? new \GOA\Makesyoulocal\Common\Model\DatetimeFormatter(
                                        new \DateTimeImmutable($product->getSpecialFromDate())
                                    )
                                    : null,
            'OFFER_FINISHED_AT' => $product->getSpecialToDate()
                                    ? new \GOA\Makesyoulocal\Common\Model\DatetimeFormatter(
                                        new \DateTimeImmutable($product->getSpecialToDate())
                                    )
                                    : null,
            'BARCODE'           => $product->getEan()
                                    ? $product->getEan()
                                    : ($product->getEancode()
                                        ? $product->getEancode()
                                        : $product->getUpc()),
            'CURRENCY'          => $this->settings->getCurrency(),
            'IMAGES'             => $images,
            'VARIATIONS'        => count($variations) ? $variations : null,
            'CATEGORIES'        => count($categories) ? $categories : null,
            'META_TITLE'        => $product->getMetaTitle(),
            'META_DESCRIPTION'  => $product->getMetaDescription(),
            'BEFORE_PRICE'      => new \GOA\Makesyoulocal\Common\Model\Currency($product->getMsrp()),
            'WEIGHT'            => $product->getWeight() === null ? null : (float)$product->getWeight(),
            'LENGTH'            => $product->getDepth() === null ? null : (float)$product->getDepth(),
            'WIDTH'             => $product->getWidth() === null ? null : (float)$product->getWidth(),
            'HEIGHT'            => $product->getHeight() === null ? null : (float)$product->getHeight(),
            'UNBOXED_WEIGHT'    => new \GOA\Makesyoulocal\Common\Model\NotImplemented(),
            'UNBOXED_LENGTH'    => new \GOA\Makesyoulocal\Common\Model\NotImplemented(),
            'UNBOXED_WIDTH'     => new \GOA\Makesyoulocal\Common\Model\NotImplemented(),
            'UNBOXED_HEIGHT'    => new \GOA\Makesyoulocal\Common\Model\NotImplemented(),
            'PUBLISHED'         => $product->getStatus() === 1,
            'SEARCHABLE'        => $product->getVisibility() === \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH,
            'VAT_CLASS'         => $this->getTaxClass($product->getTaxClassId())
        );
    }

    protected function getTaxClass($id) {
        if ($this->taxClasses === null) {
            $this->taxClasses = array(); 
            $options = $this->objectManager
                 ->get('\Magento\Tax\Model\TaxClass\Source\Product')
                 ->toOptionArray();

            foreach ($options as $option) {
                $this->taxClasses[$option['value']] = $option['label'];
            }
        }
        return isset($this->taxClasses[$id]) ? $this->taxClasses[$id] : null;
    }
}

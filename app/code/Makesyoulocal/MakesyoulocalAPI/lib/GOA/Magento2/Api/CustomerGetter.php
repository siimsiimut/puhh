<?php
namespace GOA\Makesyoulocal\Magento2\Api;

class CustomerGetter
{
    protected $settings;

    public function __construct($settings)
    {
        $this->settings = $settings;

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $groupsArr = $objectManager
            ->create('\Magento\Customer\Model\Group')
            ->getCollection()
            ->getData();
        $this->group = array();
        foreach ($groupsArr as $group) {
            $this->group[$group['customer_group_id']] = $group['customer_group_code'];
        }
    }

    public function getById($customer)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        if (is_int($customer) || is_string($customer)) {
            $customer = $objectManager
                ->create('\Magento\Customer\Model\Customer')->load($customer);
        } else {
            $customer = $objectManager
                ->create('\Magento\Customer\Model\Customer')->load($customer->getId());
        }

        $defaultShipping = $customer->getDefaultShippingAddress();
        $defaultBilling = $customer->getDefaultBillingAddress();

        $addresses = array();
        $addressesCollection = $customer->getAddresses();

        $subscriber = $objectManager
            ->create('\Magento\Newsletter\Model\Subscriber')
            ->loadByCustomer($customer->getId(), $customer->getWebsiteId());

        $hasAcceptedMarketing = $subscriber->getSubscriberStatus() == '1';

        foreach ($addressesCollection as $addressEntity) {
            $country = $addressEntity->getData('country_id');
            $countryName = "";
            if ($country) {
                $countryFactory = $objectManager->create('\Magento\Directory\Model\CountryFactory');
                $countryName = $countryFactory->create()->load($country)->getName();
            }

            $streets = $addressEntity->getStreet();
            $address = array(
                'COMPANY'       => $addressEntity->getCompany(),
                'VAT_NUMBER'    => $addressEntity->getVatId(),
                'NAME'          => $addressEntity->getFirstname() . ' ' . $addressEntity->getLastname(),
                'FIRST_NAME'    => $addressEntity->getFirstname(),
                'LAST_NAME'     => $addressEntity->getLastname(),
                'ADDRESS_1'     => $streets[0],
                'ADDRESS_2'     => (count($streets) > 1 && isset($streets[1])) ? $streets[1] : '',
                'CITY'          => $addressEntity->getData('city'),
                'PROVINCE'      => $addressEntity->getRegion(),
                'ZIP_CODE'      => $addressEntity->getData('postcode'),
                'COUNTRY'       => array(
                    'ID'    => $country,
                    'NAME'  => $countryName
                ),
                'PHONE'         => $addressEntity->getTelephone(),
                'BILLING_DEFAULT'   => $addressEntity === $defaultBilling,
                'SHIPPING_DEFAULT' => $addressEntity === $defaultShipping
            );
            array_push($addresses, $address); 
        }


        return array(
            'ID'                => $customer->getId(),
            'VAT_NUMBER'        => $customer->getTaxvat(),
            'EMAIL'             => $customer->getEmail(),
            'GROUP'             => $this->group[$customer->getGroupId()],
            'CURRENCY'          => new \GOA\Makesyoulocal\Common\Model\NotImplemented(),
            'COMPANY_NOTE'      => new \GOA\Makesyoulocal\Common\Model\NotImplemented(),
            'ACCEPTS_MARKETING' => $hasAcceptedMarketing,
            'LANGUAGE'          => new \GOA\Makesyoulocal\Common\Model\NotImplemented(),
            'VAT_CLASS'         => $this->group[$customer->getGroupId()],
            'ADDRESSES'         => $addresses
        );
    }
}

ESTO PAYMENT SOLUTION

-------------
CHANGELOG
-------------

===== 1.0.8 =====

* Initial stable release
* all logos were updated


===== 1.0.9 =====

* add checkout calculator for Esto X


===== 1.0.10 =====

* compatibility with php8+ and magento 2.4.5
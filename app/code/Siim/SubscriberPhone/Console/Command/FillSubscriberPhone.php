<?php
namespace Siim\SubscriberPhone\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FillSubscriberPhone extends Command
{
    const NAME = 'FillSubscriberPhone';
    
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Magento\Framework\Model\ResourceModel\Iterator 
     */
    private $iterator;    

    /**
     * @var \Siim\SubscriberPhone\Model\ResourceModel\Newsletter\Collection
     */
    private $subscriberCollection;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    private $addressRepository;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    private $data = [];    

    private $phones = [];


    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Model\ResourceModel\Iterator $iterator,
        \Siim\SubscriberPhone\Model\ResourceModel\Newsletter\Collection $subscriberCollection,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepositoryInterface,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepositoryInterface,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    )
    {
        $this->logger = $logger;
        $this->iterator = $iterator;
        $this->subscriberCollection = $subscriberCollection;
        $this->orderRepository = $orderRepositoryInterface;
        $this->customerRepository = $customerRepositoryInterface;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->addressRepository = $addressRepositoryInterface;
        $this->resourceConnection = $resourceConnection;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('siim:subscriber:fillPhone')
            ->setDescription('Updates subscriber phone nrs');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_exec();
    }
    public function executeFromCron()
    {
        $this->_exec();
    }

    protected function _exec()
    {
        $this->logger->info('Start newsletter subscriber phone number update');
        try {

            //set $this->data to all enabled subscriber emails
            $this->iterator->walk($this->subscriberCollection->getselect(), [[$this, 'getEnabledEmails']]);
            $connection = $this->resourceConnection->getConnection();
            //subquery to select latest order for this email
            $subquery = $connection->select()->from('sales_order', ['customer_email', 'MAX(created_at) as latest_order', 'shipping_address_id'])
                ->group('customer_email');

            //get phone from latest order using subquery
            $query = $connection->select()
                ->from(['o' => $subquery])
                ->join(
                    ['a' => $connection->getTableName('sales_order_address')],
                    'o.shipping_address_id = a.entity_id',
                    ['telephone']
                )
                ->where("o.customer_email IN (?)", $this->data);
            
            $telephonePerEmail = $connection->fetchAll($query);

            if (count($telephonePerEmail) > 0) {
                foreach($telephonePerEmail as $customer) {
                    $this->saveCustomerPhone($customer['customer_email'], $customer['telephone']);
                }
            }
        }
        catch (\Exception $exception) {
            $this->logger->error('Subscriber phone update failure',[
                'error' => $exception
            ]);
        }
    
        
        $this->logger->info('Finish subscriber phone refresh');
    }
    
    public function getEnabledEmails($row)
    {
        //skip unsubscribed
        if ($row['row']['subscriber_status'] !== '1') {
            return false;
        }
        $email = $row['row']['subscriber_email'];
        $this->data[] = $email;
    }
    public function saveCustomerPhone(string $entityId, string $phone)
    {
        try {
            $this->subscriberCollection->savePhone($entityId, $phone);
        } catch (\Throwable $th) {
            $this->logger->error('Error saving subscriber',
            [
                'error' => $th
            ]);
        }
        
    }
}
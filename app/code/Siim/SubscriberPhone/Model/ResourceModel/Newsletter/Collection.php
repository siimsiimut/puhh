<?php
namespace Siim\SubscriberPhone\Model\ResourceModel\Newsletter;

class Collection extends \Magento\Newsletter\Model\ResourceModel\Subscriber\Collection
{
    const PHONE_COLUMN = 'phone';

    protected function _initSelect()
    {
        parent::_initSelect();
        $this->showCustomerInfo(true)->addSubscriberTypeField()->showStoreInfo();
        $this->_map['fields']['phone'] = 'main_table.'.self::PHONE_COLUMN;
        return $this;
    }
    public function savePhone($email, $phone)
    {
        $this->getConnection()->update(
            $this->getMainTable(),
            [self::PHONE_COLUMN => $phone],
            ['subscriber_email' . ' = ?' => $email]
        );
    }
}
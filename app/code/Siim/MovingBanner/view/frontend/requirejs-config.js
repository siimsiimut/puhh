var config = {
    map: {
        "*": {
            movingBanner: "Siim_MovingBanner/js/moving-banner",
        }
    },
    shim: {
        movingBanner: {
            'deps': ['jquery', 'underscore', 'matchMedia']
        },
        
    }
};
define([
    'jquery',
    'underscore',
    'matchMedia'
], function($, _, mediaCheck) {
    'use strict';
    var module = {
        init: function() {
            this.msgWrap = $('.moving-banner');
            this.msg = this.msgWrap.children('.content').children();
            this.speed = 75;
            
            var self = this;
                
            
            this.msgWrap.show();

            this.msgWidth = this.msg.outerWidth();                
             
            var lazyAdjust = _.debounce(self.adjustDuration, 200)
            
            window.addEventListener('resize', function(event){
                lazyAdjust(self);
            });

            
            mediaCheck({
                media: '(min-width: 768px)',
                entry: function () {
                    var dupsNeeded = Math.ceil(1920 * 2 / self.msgWidth);
                    self.adjustDups(dupsNeeded);		
                },
                exit: function () {
                    var dupsNeeded = Math.ceil(768 * 2 / self.msgWidth);
                    self.adjustDups(dupsNeeded);
                },
            }); 

            lazyAdjust(self);
            this.msgWrap.children('.close').on('click', function () {
                self.msgWrap.hide();
                $('.page-wrapper').css('padding-bottom', 0);
            });
        },

        adjustDups: function (dupsNeeded) {
            var currentDups = $('.moving-banner .dup').length,
                self = this;
            if (dupsNeeded == currentDups) { return true };

            var diff = dupsNeeded - currentDups;
            if (diff > 0) {
                for (var i = 0; i < diff; i++) {
                    self.msg.clone().addClass('dup').insertAfter(self.msg);
                }
            } else {
                $('.dup').remove();
                for (var i = 0; i < dupsNeeded; i++) {
                    self.msg.clone().addClass('dup').insertAfter(self.msg);
                }
            }
            self.msgWrap.width((dupsNeeded + 1) * self.msgWidth + 'px');
            
        },
        adjustDuration: function (self) {
            var duration = self.msgWidth * ($('.dup').length-1) / self.speed,
                delayFrag = duration / $('.dup').length;
            
            self.msgWrap.children('.content').css('animation-duration', duration + 's');
            $.each($('.dup'), function (i, e) {
                $(e).css('animation-delay', delayFrag*(i+1)+'s')
            });
        }
    }
    return module.init();
});
<?php

namespace Siim\AutomaticNewCategoryAssign\Cron\Sync;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Catalog\Api\CategoryRepositoryInterface;

class SyncNewProducts
{
    const newProductsCategory = 1940;

    /**
     * @var ProductCollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    /**
     * @param ProductCollectionFactory $productCollectionFactory
     */
    public function __construct(
        ProductCollectionFactory $productCollectionFactory,
        CategoryRepositoryInterface $categoryRepositoryInterface,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localedate,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->_localeDate = $localedate;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->categoryRepository = $categoryRepositoryInterface;
        $this->logger = $logger;
    }
    public function execute()
    {
        $this->logger->info('Starting new products category sync');

        $newProducts = $this->getNewProducts();
        $this->updateNewCategory($newProducts);

        $this->logger->info('Finished new products category sync', [ 
            'products' => $newProducts,
            'total' => count($newProducts)
        ]);
    }
    public function getNewProducts()
    {
        $productIds = [];
        
        $todayStartOfDayDate = $this->_localeDate->date()->setTime(0, 0, 0)->format('Y-m-d H:i:s');
        $todayEndOfDayDate = $this->_localeDate->date()->setTime(23, 59, 59)->format('Y-m-d H:i:s');
        
        $collection = $this->productCollectionFactory->create();
        $collection->addAttributeToFilter(
            'news_from_date',
            [
                'or' => [
                    0 => ['date' => true, 'to' => $todayEndOfDayDate],
                    1 => ['null' => true],
                ]
            ],
            'left'
        )->addAttributeToFilter(
            'news_to_date',
            [
                'or' => [
                    0 => ['date' => true, 'from' => $todayStartOfDayDate],
                    1 => ['null' => true],
                ]
            ],
            'left'
        )->addAttributeToFilter(
            [
                ['attribute' => 'news_from_date', 'is' => new \Zend_Db_Expr('not null')],
                ['attribute' => 'news_to_date', 'is' => new \Zend_Db_Expr('not null')],
            ]
        );
        foreach ($collection as $prod) {
            $productIds[$prod->getId()] = true;
        }
        return $productIds;
    }
    public function updateNewCategory($productIds) 
    {
        try {
            $category = $this->categoryRepository->get(self::newProductsCategory, 0);
            $category->setPostedProducts($productIds);
            $category->save();
        } catch (\Throwable $th) {
            $this->logger->critical('New products assign error', [
                    'err' => $th->getMessage()
                ]
            );
        }
    }
}
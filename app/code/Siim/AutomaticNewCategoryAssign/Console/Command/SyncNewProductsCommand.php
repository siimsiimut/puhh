<?php

namespace Siim\AutomaticNewCategoryAssign\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncNewProductsCommand extends Command
{
    const NAME = 'NewProductsSync';
    /**
     * @var \Siim\AutomaticNewCategoryAssign\Cron\Sync\SyncNewProducts 
     */
    protected $syncer;
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Siim\AutomaticNewCategoryAssign\Cron\Sync\SyncNewProducts $syncNewProducts
        )
    {
        $this->logger = $logger;
        $this->syncer = $syncNewProducts;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('siim:sync:new:products')
            ->setDescription('Assigns new products to category');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger->info('starting new products category assign from CLI');
        try {
            $this->syncer->execute();
        }
        catch (\Exception $exception) {
            $this->logger->error('Error setting new products to category', [
                'err' => $exception
            ]);
        }
    }

   

}
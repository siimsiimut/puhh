<?php
namespace Siim\IntervalMinPrice\Model\ResourceModel;

class MinPrice extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	
	/**
	 * SINCE WE DONT USE AUTO INCREMENTIND ID WE MUST SET THIS FALSE
	 * OTHERWISE WONT SAVE TO DB
	 */
	protected $_isPkAutoIncrement = false;

	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	)
	{
		parent::__construct($context);
	}
	
	protected function _construct()
	{
		$this->_init('siim_intervalprice_index', 'sku');
	}
	
}
<?php
namespace Siim\IntervalMinPrice\Model\ResourceModel\MinPrice;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
		
	protected function _construct()
	{
		$this->_init('Siim\IntervalMinPrice\Model\MinPrice', 'Siim\IntervalMinPrice\Model\ResourceModel\MinPrice');
	}
	
}
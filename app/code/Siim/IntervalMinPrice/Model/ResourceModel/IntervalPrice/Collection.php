<?php
namespace Siim\IntervalMinPrice\Model\ResourceModel\IntervalPrice;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
		
	protected function _construct()
	{
		$this->_init('Siim\IntervalMinPrice\Model\IntervalPrice', 'Siim\IntervalMinPrice\Model\ResourceModel\IntervalPrice');
	}
	
}
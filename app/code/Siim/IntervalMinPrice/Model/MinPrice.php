<?php
namespace Siim\IntervalMinPrice\Model;

class MinPrice extends \Magento\Framework\Model\AbstractModel
{
	protected function _construct()
	{
		$this->_init('Siim\IntervalMinPrice\Model\ResourceModel\MinPrice');
    }
    
}
<?php
namespace Siim\IntervalMinPrice\ViewModel;

use Siim\IntervalMinPrice\Console\Command\MinPriceIndexer;

class MinPriceVm implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    /**
     * @var \Siim\IntervalMinPrice\Model\ResourceModel\MinPrice\Collection
     */
    private $minPriceCollection;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    private $priceHelper;

    /**
     * @var \Magento\Catalog\Helper\Data
     */
    private $catalogHelper;
    public function __construct(
        \Siim\IntervalMinPrice\Model\ResourceModel\MinPrice\Collection $minPriceCollection,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Magento\Catalog\Helper\Data $catalogHelper
    )
    {
        $this->minPriceCollection = $minPriceCollection;
        $this->priceHelper = $priceHelper;
        $this->catalogHelper = $catalogHelper;
    }
    
    public function getIntervalPriceData($sku, $listFinalPrice)
    {
        $minPrice = $this->minPriceCollection->getItemByColumnValue('sku',$sku);
        if(!$minPrice){return [];}
        $minPrice = $minPrice->getData('min_price');
        $discountFromMin = (floatval($listFinalPrice) - floatval($minPrice)) / floatval($minPrice) * 100;
        $result = [
            'interval' => MinPriceIndexer::INTERVAL,
            'discount_from_min' => number_format($discountFromMin, 0),
            'min_price' => $this->priceHelper->currency($minPrice)
        ];

        return $result;
    }
    public function getIntervalPriceDataPDP() 
    {
        $prod = $this->catalogHelper->getProduct();
        $sku = $prod->getSku();
        $finalPrice = $prod->getPriceInfo()->getPrice('final_price')->getAmount()->getValue();
        $regularPrice = $prod->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue();
        return $finalPrice < $regularPrice ? $this->getIntervalPriceData($sku, $finalPrice) : false;
    }
    
}
<?php
namespace Siim\IntervalMinPrice\Plugin;

class ProductListWidgetPlugin {

    /**
     * @var \Siim\IntervalMinPrice\ViewModel\MinPriceVm
     */
    private $minPriceVm;

    public function __construct(
        \Siim\IntervalMinPrice\ViewModel\MinPriceVm $minPriceVm
    )
    {
        $this->minPriceVm = $minPriceVm;
    }
    public function beforeToHtml(\Magento\CatalogWidget\Block\Product\ProductsList $pl)
    {
        $pl->setData('min_price_vm', $this->minPriceVm);
        $pl->addChild('interval.price', '\Magento\Framework\View\Element\Template',
        ["template"=>"Siim_IntervalMinPrice::intervalminprice.phtml"]);
        return [];
    }
}
<?php
namespace Siim\IntervalMinPrice\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MinPriceIndexer extends Command
{
    const NAME = 'MinPriceIndexer';

    const INTERVAL = 30;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    
    /**
     * @var \Magento\Framework\Model\ResourceModel\Iterator
     */
    private $iterator;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    private $productCollection;

    /**
     * @var \Siim\IntervalMinPrice\Model\MinPriceFactory
     */
    private $minPriceFactory;

    /**
     * @var \Siim\IntervalMinPrice\Model\ResourceModel\IntervalPrice\Collection 
     */
    private $intervalPriceCollection;

    /**
     * @var \Siim\IntervalMinPrice\Model\ResourceModel\MinPrice
     */
    private $minPriceResource;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Model\ResourceModel\Iterator $iterator,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $collection,
        \Siim\IntervalMinPrice\Model\MinPriceFactory $minPriceFactory,
        \Siim\IntervalMinPrice\Model\ResourceModel\IntervalPrice\Collection $intervalPriceCollection,
        \Siim\IntervalMinPrice\Model\ResourceModel\MinPrice $minPriceResource

    )
    {
        $this->logger = $logger;
        $this->iterator = $iterator;
        $this->productRepository = $productRepository;
        $this->productCollection = $collection;
        $this->minPriceFactory = $minPriceFactory;
        $this->intervalPriceCollection = $intervalPriceCollection;
        $this->minPriceResource = $minPriceResource;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('siim:intervalminprice:index')
            ->setDescription('Builds index of minimum price for interval');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_exec();
    }
    public function normalExecute()
    {
        $this->_exec();
    }
    protected function _exec()
    {
        try {   
            $this->productCollection->addAttributeToFilter('status', ['eq' => 1]);
            
            $this->logger->info('Start indexing intervalprice',[
                'collection size' => $this->productCollection->getSize()
            ]);
            
            $this->iterator->walk(
                $this->productCollection->getSelect(),
                [[$this, 'indexMinPrice']]
            );
        }
        catch (\Exception $exception) {
            $this->logger->error('Intervalprice indexing failure',[
                'error' => $exception
            ]);
        }
        $this->logger->info('Finish Intervalprice indexing');
    }

    public function indexMinPrice ($args) {
        try {
            $sku = $args['row']['sku'];
            $intervalPrices = clone $this->intervalPriceCollection;
            $intervalPrices->addFieldToFilter(
                'sku', ['eq' => $sku]
            )->setPageSize(self::INTERVAL)->setCurPage(1);
            
            if(!$intervalPrices->getSize() > 0) {
                $this->logger->info('No intervalprice found for sku',[
                    'sku' => $sku
                ]);
                return false;
            }

            $minPrice = null;

            foreach($intervalPrices->getItems() as $ip) {
                $fp = $ip['final_price'];
                if(!$minPrice) {
                    $minPrice = $fp;
                } else if ($fp < $minPrice) {
                    $minPrice = $fp;
                }
            }
            $indexVal = $this->minPriceFactory->create();
            $indexVal->setSku($sku);
            $indexVal->setMinPrice($minPrice);
            $this->minPriceResource->save($indexVal);

        } catch (\Exception $exception) {
            $this->logger->error('Intervalprice save failure',[
                'error' => $exception
            ]);
        }
    }

    
}
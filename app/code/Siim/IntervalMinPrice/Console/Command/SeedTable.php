<?php
namespace Siim\IntervalMinPrice\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SeedTable extends Command
{
    const NAME = 'CollectIntervalTablePrices';

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Magento\Framework\Model\ResourceModel\Iterator
     */
    private $iterator;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    private $productCollection;

    /**
     * @var \Siim\IntervalMinPrice\Model\IntervalPriceFactory
     */
    private $intervalPriceFactory;

    /**
     * @var \Siim\IntervalMinPrice\Model\ResourceModel\IntervalPrice
     */
    private $intervalPriceResource;
    /**
     * @var \Siim\IntervalMinPrice\Console\Command\MinPriceIndexer
     */
    private $minPriceIndexer;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Model\ResourceModel\Iterator $iterator,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $collection,
        \Siim\IntervalMinPrice\Model\IntervalPriceFactory $intervalPriceFactory,
        \Siim\IntervalMinPrice\Model\ResourceModel\IntervalPrice $intervalPriceResource,
        \Siim\IntervalMinPrice\Console\Command\MinPriceIndexer $minPriceIndexer
    )
    {
        $this->logger = $logger;
        $this->iterator = $iterator;
        $this->productRepository = $productRepository;
        $this->productCollection = $collection;
        $this->intervalPriceFactory = $intervalPriceFactory;
        $this->intervalPriceResource = $intervalPriceResource;
        $this->minPriceIndexer = $minPriceIndexer;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('siim:intervalminprice:collectprices')
            ->setDescription('Save price data for all active skus');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_exec();
    }
    public function executeFromCron()
    {
        $this->_exec();
    }

    protected function _exec()
    {
        try {   
            $this->productCollection->addAttributeToFilter('status', ['eq' => 1]);
            
            $this->logger->info('Start collecting intervalprice',[
                'collection size' => $this->productCollection->getSize()
            ]);
            
            $this->iterator->walk(
                $this->productCollection->getSelect(),
                [[$this, 'saveCurrentMinPrice']]
            );
        }
        catch (\Exception $exception) {
            $this->logger->error('Intervalprice save failure',[
                'error' => $exception
            ]);
        }
        $this->logger->info('Finish Intervalprice run');
        $this->minPriceIndexer->normalExecute();
    }
    public function saveCurrentMinPrice ($args) {
        try {
            //we specifically get store id 1 products, beacause some web prices from buum are applied as a special price per store view only (admin scope stays blank)
            $p = $this->productRepository->getById($args['row']['entity_id'], false, 1);
            $catalogPrice = $p->getPriceInfo()->getPrice('final_price')->getAmount()->getValue();

            $intervalPrice = $this->intervalPriceFactory->create();
            $intervalPrice->setSku($p->getSku());
            $intervalPrice->setFinalPrice($catalogPrice);
            $this->intervalPriceResource->save($intervalPrice);
        } catch (\Exception $exception) {
            $this->logger->error('Intervalprice save failure',[
                'error' => $exception
            ]);
        }
    }

    
}
<?php
namespace Siim\IntervalMinPrice\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CleanupMinPrices extends Command
{
    const NAME = 'CleanupIntervalTablePrices';
    
    /**
     * lifetime of oldest record in days
     */
    const INTERVAL = 45;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;
   
    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private $db;
    

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localedate,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    )
    {
        $this->logger = $logger;
        $this->resourceConnection = $resourceConnection;
        $this->db = $this->resourceConnection->getConnection();
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('siim:intervalminprice:cleanup')
            ->setDescription('Cleans up records older than');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_exec();
    }
    public function executeFromCron()
    {
        $this->_exec();
    }

    protected function _exec()
    {
        $this->logger->info('Start Intervalprice cleanup, interval = '.self::INTERVAL.' days');
        try {   
            $intervalPricesTable = $this->db->getTableName('siim_intervalprice');
            $this->db->delete(
                $intervalPricesTable,
                "created_at < date_sub(CURDATE(), INTERVAL ".self::INTERVAL." Day)"
            );
        }
        catch (\Exception $exception) {
            $this->logger->error('Intervalprice cleanup failure',[
                'error' => $exception
            ]);
        }
        $this->logger->info('Finish Intervalprice cleanup');
    }    
}
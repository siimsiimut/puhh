<?php

namespace Siim\TimedCartRule\Plugin\Ui\Component;

use Magento\Ui\Component\Form\Element\DataType\Date AS MDate;

class Date
{
    public function afterPrepare(MDate $subject, $result)
    {
        $config = $subject->getData('config');
        $name = $subject->getData('name');
        if (in_array($name, ['from_date', 'to_date']) && isset($config['validation']['validate-date']) && !$config['validation']['validate-date']) {
            unset($config['validation']['validate-date']);
            $subject->setData('config', $config);
        }

        return $subject;
    }
}

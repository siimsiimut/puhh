<?php

namespace Siim\CouponPromoPrice\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Catalog\Model\Product\Attribute\Repository as AttributeRepository;

class MultiselectOptions extends AbstractSource
{
    /**
     * @var AttributeRepository
     */
    protected $attributeRepository;

    /**
     * MultiselectOptions constructor.
     * @param AttributeRepository $attributeRepository
     */
    public function __construct(AttributeRepository $attributeRepository)
    {
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * Get all options from the "brand" attribute
     *
     * @return array
     */
    public function getAllOptions()
    {
        $options = [];

        $attributeCode = 'brand';
        $attribute = $this->attributeRepository->get($attributeCode);

        if ($attribute) {
            // Get all options for the attribute
            $options = $attribute->getSource()->getAllOptions();
            // Custom sorting function based on the 'label' key
            usort($options, function ($a, $b) {
                return strcmp($a['label'], $b['label']);
            });
        }

        return $options;
    }
}
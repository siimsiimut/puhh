<?php

namespace Siim\CouponPromoPrice\ViewModel;

use Magento\Store\Model\ScopeInterface;

class CouponPromoVm implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    const CONF_PATH = 'couponpromoprice/';
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $config;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Catalog\Helper\Data
     */
    protected $catalogHelper;

    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $httpContext;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $priceCurrencyInterface;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Helper\Data $catalogHelper,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrencyInterface
    ) {
        $this->config = $config;
        $this->storeManager = $storeManager;
        $this->catalogHelper = $catalogHelper;
        $this->httpContext = $httpContext;
        $this->priceCurrencyInterface = $priceCurrencyInterface;
    }

    protected function getConfigValue(string $path)
    {
        return $this->config->getValue(self::CONF_PATH . $path, ScopeInterface::SCOPE_STORE, $this->storeManager->getStore()->getId());
    }

    /**
     * determines whether the brand promo (higher priority) or catalog coupon promo
     */
    public function determinePromoType($product = null)
    {
        $bpp = $this->getBrandPromoPrice($product);
        if ($bpp) {
            return 'brand_promo';
        }
        $cpp = $this->getCatPromoPrice($product);
        if ($cpp) {
            return 'cat_promo';
        }
        return false;
    }
    /**
     * try to get promo price if config set correctly
     * if brand type promo enabled return that instead (has higher priority)
     * excludes products which have a forbidden category
     * returns different prices for guest and logged in customers
     */
    public function getPromoPrice($product = null)
    {

        $bpp = $this->getBrandPromoPrice($product);
        return $bpp ? $bpp : $this->getCatPromoPrice($product);
        
    }

    public function getCatPromoPrice($product = null)
    {
        if (!$this->checkIfPromoPossible()) {
            return false;
        }
        try {
            if (!$product) {
                $product = $this->catalogHelper->getProduct();
            }
            $productCats = $product->getCategoryIds() ?? [];
            $excludedCats = explode(',', $this->getConfigValue('cat_settings/excluded_categories') ?? '');
            if (array_intersect($productCats, $excludedCats)) {
                return false;
            }
            $regularPrice = $product->getPriceInfo()->getPrice('regular_price')->getValue();
            $specialPrice = $product->getPriceInfo()->getPrice('special_price')->getValue();

            if ($product->getTypeId() == 'configurable') {
                $basePrice = $product->getPriceInfo()->getPrice('regular_price');
                $regularPrice = $basePrice->getMinRegularAmount()->getValue();
                $specialPrice = $product->getFinalPrice();
            }
            //guest
            if ($this->getGroupId() == '0') {
                $discount = $this->getConfigValue('cat_settings/guest_discount_percent');
            }
            //else registered/logged in user 
            else {
                $discount = $this->getConfigValue('cat_settings/customer_discount_percent');
            }
            $rawPromoPrice = $regularPrice * (1 - $discount / 100);

            return $rawPromoPrice < $specialPrice || !$specialPrice ?
                $this->priceCurrencyInterface->convertAndFormat($rawPromoPrice) :
                false;
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function getBrandPromoPrice($product = null)
    {
        if (!$this->checkIfBrandPromoPossible()) {
            return false;
        }
        try {
            if (!$product) {
                $product = $this->catalogHelper->getProduct();
            }
            $productBrand = explode(',',$product->getData('brand')) ?? [];
            $brandsInPromo = explode(',', $this->getConfigValue('brand_settings/included_brands')) ?? [];
            if (!array_intersect($productBrand, $brandsInPromo)) {
                return false;
            }
            $regularPrice = $product->getPriceInfo()->getPrice('regular_price')->getValue();
            $specialPrice = $product->getPriceInfo()->getPrice('special_price')->getValue();

            if ($product->getTypeId() == 'configurable') {
                $basePrice = $product->getPriceInfo()->getPrice('regular_price');
                $regularPrice = $basePrice->getMinRegularAmount()->getValue();
                $specialPrice = $product->getFinalPrice();
            }
            //guest
            if ($this->getGroupId() == '0') {
                $discount = $this->getConfigValue('brand_settings/guest_discount_percent_brand');
            }
            //else registered/logged in user 
            else {
                $discount = $this->getConfigValue('brand_settings/customer_discount_percent_brand');
            }
            $rawPromoPrice = $regularPrice * (1 - $discount / 100);

            return $rawPromoPrice < $specialPrice || !$specialPrice ?
                $this->priceCurrencyInterface->convertAndFormat($rawPromoPrice) :
                false;
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function getGroupId()
    {
        if ($this->httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH)) {
            return $this->httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_GROUP);
        }
        return '0';
    }
    public function getIsEnabled()
    {
        return $this->getConfigValue('cat_settings/coupon_promo_enabled');
    }
    public function getIsBrandPromoEnabled()
    {
        return $this->getConfigValue('brand_settings/brand_coupon_promo_enabled');
    }
    protected function checkIfPromoPossible(): bool
    {
        if (
            !$this->getIsEnabled() ||
            !$this->getConfigValue('cat_settings/customer_min_cart_value') ||
            !$this->getConfigValue('cat_settings/customer_discount_percent') ||
            !$this->getConfigValue('cat_settings/guest_min_cart_value') ||
            !$this->getConfigValue('cat_settings/guest_discount_percent')
        ) {
            return false;
        }
        return true;
    }
    protected function checkIfBrandPromoPossible() : bool
    {
        if (
            !$this->getIsBrandPromoEnabled() ||
            !$this->getConfigValue('brand_settings/customer_min_cart_value_brand') ||
            !$this->getConfigValue('brand_settings/customer_discount_percent_brand') ||
            !$this->getConfigValue('brand_settings/guest_min_cart_value_brand') ||
            !$this->getConfigValue('brand_settings/guest_discount_percent_brand')
        ) {
            return false;
        }
        return true;
    }
    public function getMinCartSumForPromoPrice(string $type)
    {
        $group = $this->getGroupId();

        $configKeys = [
            '0' => [
                'cat_promo' => 'cat_settings/guest_min_cart_value',
                'brand_promo' => 'brand_settings/guest_min_cart_value_brand',
            ],
            '1' => [
                'cat_promo' => 'cat_settings/customer_min_cart_value',
                'brand_promo' => 'brand_settings/customer_min_cart_value_brand',
            ],
        ];

        // Use '1' key values when $group is not 0
        $minCartSum = $this->getConfigValue($configKeys[$group][$type] ?? $configKeys['1'][$type] ?? '');

        return $minCartSum;
    }
}

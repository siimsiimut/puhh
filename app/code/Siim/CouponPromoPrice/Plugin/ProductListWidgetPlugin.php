<?php
namespace Siim\CouponPromoPrice\Plugin;

class ProductListWidgetPlugin {

    /**
     * @var \Siim\IntervalMinPrice\ViewModel\MinPriceVm
     */
    private $promoPriceVM;

    public function __construct(
        \Siim\CouponPromoPrice\ViewModel\CouponPromoVm $promoPriceVM
    )
    {
        $this->promoPriceVM = $promoPriceVM;
    }
    public function beforeToHtml(\Magento\CatalogWidget\Block\Product\ProductsList $pl)
    {
        $pl->setData('promo_price_view_model', $this->promoPriceVM);
        $pl->addChild('coupon.promo.price.cat', '\Magento\Framework\View\Element\Template',
        ["template"=>"Siim_CouponPromoPrice::promoprice-cat.phtml"]);
        return [];
    }
}
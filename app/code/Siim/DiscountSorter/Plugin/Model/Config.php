<?php
namespace Siim\DiscountSorter\Plugin\Model;

class Config
{
    /**
     * Add custom Sort By option
     *
     * @param \Magento\Catalog\Model\Config $catalogConfig
     * @param array $options
     * @return array []
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetAttributeUsedForSortByArray(\Magento\Catalog\Model\Config $catalogConfig, $options)
    {
        $customOption['most_discounted'] = __('Discount');
        $options = array_merge($options, $customOption); 
        return $options;
    }

    /**
     * This method is optional. Use it to set Most Viewed as the default
     * sorting option in the category view page
     *
     * @param \Magento\Catalog\Model\Config $catalogConfig
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetProductListDefaultSortBy(\Magento\Catalog\Model\Config $catalogConfig)
    {
        return 'created_at';
    }
}
<?php
namespace Siim\DiscountSorter\Plugin\Block;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\DB\Select;

/**
 * Product list toolbar plugin
 */
class Toolbar
{
    const SORT_ORDER_DESC = 'DESC';

    /**
     * @var \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    protected $_collection = null;

    /**
     * DB connection
     *
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $_conn;

    /**
     * @var boolean
     */
    protected $_subQueryApplied = false;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\ResourceConnection $resource
     */
    public function __construct(ResourceConnection $resource)
    {
        $this->_conn = $resource->getConnection('catalog');
    }

    /**
     * Plugin - Used to modify default Sort By filters
     *
     * @param \Magento\Catalog\Block\Product\ProductList\Toolbar $subject
     * @param null $result
     * @param \Magento\Framework\Data\Collection $collection
     * @return Toolbar
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterSetCollection(\Magento\Catalog\Block\Product\ProductList\Toolbar $subject,
        $result,
        $collection
    ) {
        $this->_collection = $collection;

        $currentOrder = $subject->getCurrentOrder();
        $dir = strtoupper($subject->getCurrentDirection());
                
        if ($currentOrder == 'most_discounted') {
            /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
            $collection = $subject->getCollection();
            $collection->getSelect()->columns(
                ['discount_percentage' => new \Zend_Db_Expr('(price_index.final_price / price_index.price) * 100')]
            );
            $collection->getSelect()->order('discount_percentage '.$dir);
        }
        return $this;
    }
}
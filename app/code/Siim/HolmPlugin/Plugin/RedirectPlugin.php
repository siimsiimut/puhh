<?php
namespace Siim\HolmPlugin\Plugin;
/* 
    Takes json output from HolmBank_Payments (Liisi järelmaks) and uses redirect url to make a proper redirect
*/
class RedirectPlugin {
    
    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    private $resultFactory;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    private $redirect;
    public function __construct(
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Magento\Framework\App\Response\RedirectInterface $redirect
    )
    {
        $this->redirect = $redirect;
        $this->resultFactory = $resultFactory;
    }
    public function afterExecute(\HolmBank\Payments\Controller\Payment\Create $subject, $result)
    {
        $resultRedirect = $this->resultFactory->create(
            \Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT
        );
        $response = json_decode($result->getJsonData(), true);
        if (isset($response['redirectUrl'])) {
            $resultRedirect->setUrl($response['redirectUrl']); 
        } else {
            $resultRedirect->setUrl('/');
        }
        return $resultRedirect;
    }
}
<?php

namespace Siim\TarneLabelChanger\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Siim\TarneLabelChanger\Setup\InstallData AS TarneLabelInstall;

class ChangeTarneLabels extends Command
{
    const NAME = 'ChangeTarneLabels';

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Magento\Framework\Model\ResourceModel\Iterator
     */
    private $iterator;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $productCollection;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface 
     */
    private $stockRegistry;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory
     */
    private $attributeCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product
     */
    private $productResourceModel;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    private $productFactory;

    protected $tarneaegValues;

    protected $frontendTarneValues;

    protected $startTime;

    protected $inStockFrontendTarneLabelValue;

    protected $updatedProductsCount = 0;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Model\ResourceModel\Iterator $iterator,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collection,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $attributeCollectionFactory,
        \Magento\Catalog\Model\ResourceModel\Product $productResourceModel,
        \Magento\Catalog\Model\ProductFactory $productFactory
    ) {
        $this->logger = $logger;
        $this->iterator = $iterator;
        $this->productRepository = $productRepository;
        $this->productCollection = $collection;
        $this->stockRegistry = $stockRegistry;
        $this->attributeCollectionFactory = $attributeCollectionFactory;
        $this->productResourceModel = $productResourceModel;
        $this->productFactory = $productFactory;
        
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('siim:tarnelabelchanger:productupdate')
            ->setDescription('Check and update tarne label if needed');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_exec();
    }
    public function executeFromCron()
    {
        $this->_exec();
    }

    protected function _exec()
    {
        $this->startTime = microtime(true);
        $this->logger->info('Start product frontend_tarneaeg label update');

        $this->getTarneLabelValues();

        try {
            $collection = $this->productCollection->create()
                ->addAttributeToSelect('tarneaeg', 'left')
                ->addAttributeToSelect(TarneLabelInstall::FRONT_TARNE_ATTR, 'left')
                ->addAttributeToFilter('status', ['eq' => 1]);

            $this->logger->info('Updating product tarne labels', [
                'collection size' => $collection->getSize()
            ]);

            $this->iterator->walk(
                $collection->getSelect(),
                [[$this, 'updateTarneLabel']]
            );
        } catch (\Exception $exception) {
            $this->logger->error('Product frontend_tarneaeg label change failure', [
                'error' => $exception
            ]);
        }
        $this->logger->info('Finish product frontend_tarneaeg label change', [
            'Updated products count: ' => $this->updatedProductsCount,
            'Update length: ' => microtime(true) - $this->startTime.' s'
        ]);
    }
    public function updateTarneLabel($args)
    {
        try {
            $pID = $args['row']['entity_id'];
            $tarneaeg = $args['row']['tarneaeg'];
            $frontendTarneaeg = $args['row'][TarneLabelInstall::FRONT_TARNE_ATTR];

            $stockItem = $this->stockRegistry->getStockItem($pID);
            $qty = $stockItem->getQty();
            $allowBackOrder = $stockItem->getBackorders();
            

            if ($qty > 0) {
                //make sure TarneLabelInstall::FRONT_TARNE_ATTR label is 1-3 tööpäeva
                if ($frontendTarneaeg !== $this->inStockFrontendTarneLabelValue) {
                    $product = $this->productFactory->create();
                    $this->productResourceModel->load($product, $pID);
                    $product->setData(TarneLabelInstall::FRONT_TARNE_ATTR, $this->inStockFrontendTarneLabelValue);
                    $this->productResourceModel->saveAttribute($product, TarneLabelInstall::FRONT_TARNE_ATTR);
                    $this->updatedProductsCount++;
                }                
            }

            if ($qty <= 0 && $allowBackOrder) {
                //make sure TarneLabelInstall::FRONT_TARNE_ATTR is set to same label as 'tarneaeg'
                $frontendTarneValue = $this->findFrontendTarneValue($tarneaeg);
                if ($frontendTarneaeg !== $frontendTarneValue) {
                    $product = $this->productFactory->create();
                    $this->productResourceModel->load($product, $pID);
                    $product->setData(TarneLabelInstall::FRONT_TARNE_ATTR, $frontendTarneValue);
                    $this->productResourceModel->saveAttribute($product, TarneLabelInstall::FRONT_TARNE_ATTR);
                    $this->updatedProductsCount++;
                }
            }

            if (($args['idx'] + 1) % 1000 == 0) {
                $elapsed = microtime(true) - $this->startTime;
                $this->logger->info('updateTarneSpeed is '.($args['idx']+1)/$elapsed. ' product/s');
            }

        } catch (\Exception $exception) {
            $this->logger->error('TarneLabelChanger row update failure',[
                'error' => $exception
            ]);
        }
    }
    protected function getAttributeValues($code)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection $attributeCollection */
        $attributeCollection = $this->attributeCollectionFactory->create();
        $attribute = $attributeCollection->addFieldToFilter('attribute_code', $code)->getFirstItem();

        // Get available attribute values
        $attributeValues = $attribute->getSource()->getAllOptions();

        return $attributeValues;
    }
    protected function getTarneLabelValues()
    {
        $this->tarneaegValues = $this->getAttributeValues('tarneaeg');
        $this->frontendTarneValues = $this->getAttributeValues(TarneLabelInstall::FRONT_TARNE_ATTR);
        $this->inStockFrontendTarneLabelValue = $this->frontendTarneValues[array_search("1 - 3 tööpäeva", array_column($this->frontendTarneValues, 'label'))]['value'];
    }
    /**
     * Finds frontend_tarneaeg label value by tarneaeg Label
     */
    protected function findFrontendTarneValue(string $tarneaegValue)
    {
        $tarneAegLabel = $this->tarneaegValues[array_search($tarneaegValue, array_column($this->tarneaegValues, 'value'))]['label'];
        $frontendTarneValue = $this->frontendTarneValues[array_search($tarneAegLabel, array_column($this->frontendTarneValues, 'label'))]['value'];

        return $frontendTarneValue; 
    }
}

<?php

namespace Siim\TarneLabelChanger\Console\Command;

use Magento\Catalog\Model\Product;
use Siim\TarneLabelChanger\Setup\InstallData AS TarneLabelInstall;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateFrontTarneAttributeLabels extends Command
{
    const NAME = 'UpdateFrontTarneLabels';

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Magento\Framework\Model\ResourceModel\Iterator
     */
    private $iterator;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory
     */
    private $attributeCollectionFactory;

    /**
     * @var \Magento\Eav\Model\Config
     */
    private $eavConfig;

    /**
     * @var \Magento\Eav\Api\AttributeOptionManagementInterface
     */
    private $attributeOptionManagement;

    /**
     * @var \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory
     */
    private $attributeOptionInterfaceFactory;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $attributeCollectionFactory,
        \Magento\Eav\Api\AttributeOptionManagementInterface $attributeOptionManagement,
        \Magento\Eav\Api\Data\AttributeOptionInterfaceFactory $attributeOptionInterfaceFactory
    ) {
        $this->logger = $logger;
        $this->eavConfig = $eavConfig;
        $this->attributeCollectionFactory = $attributeCollectionFactory;
        $this->attributeOptionManagement = $attributeOptionManagement;
        $this->attributeOptionInterfaceFactory = $attributeOptionInterfaceFactory;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('siim:tarnelabelchanger:attrupdate')
            ->setDescription('Update frontend_tarneaeg attribute options');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_exec();
    }
    public function executeFromCron()
    {
        $this->_exec();
    }

    protected function _exec()
    {
        $this->logger->info('Start update frontend_tarneaeg attribute options');
        
        if (!$this->doesFrontTarneExist()) {
            $this->logger->error('frontend_tarneaeg attribute does not exist');
            return false;
        }
        
        $tarneaegValues = $this->getAttributeValues('tarneaeg');
        $frontTarneValues = $this->getAttributeValues(TarneLabelInstall::FRONT_TARNE_ATTR);

        //foreach $tarneaegValues make sure equivalent exists for frontTarneValues
        foreach($tarneaegValues as $k => $v) {
            //empty label/option exists by default, skip it
            if ($v['value'] == '') {continue;}

            $labelExists = false;

            foreach ($frontTarneValues as $fv) {
                if ($fv['label'] == $v['label']) {
                    $labelExists = true;
                    break;
                }
            }

            if (!$labelExists) {
                //add label to frontEndTarneValues
                $option = $this->attributeOptionInterfaceFactory->create();
                $option->setLabel($v['label'])
                ->setSortOrder($k);

                $this->attributeOptionManagement->add(
                    Product::ENTITY,
                    TarneLabelInstall::FRONT_TARNE_ATTR,
                    $option
                );
            }
                        
        }


        $this->logger->info('Finish update frontend_tarneaeg attribute options');
    }
    

    protected function getAttributeValues($code)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection $attributeCollection */
        $attributeCollection = $this->attributeCollectionFactory->create();
        $attribute = $attributeCollection->addFieldToFilter('attribute_code', $code)->getFirstItem();

        // Get available attribute values
        $attributeValues = $attribute->getSource()->getAllOptions();

        return $attributeValues;
    }

    protected function doesFrontTarneExist() : bool
    {
        $attr = $this->eavConfig->getAttribute(Product::ENTITY, TarneLabelInstall::FRONT_TARNE_ATTR);
        return ($attr && $attr->getId());
    }
}

<?php
namespace Siim\TarneLabelChanger\Setup;


use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/* 

Adds product attribute frontend_tarneaeg if attribute does not exist.

*/

class InstallData implements InstallDataInterface
{
    const FRONT_TARNE_ATTR = 'frontend_tarneaeg';
    
    private $eavSetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $attributeCode = 'frontend_tarneaeg';

        // Check if the attribute exists
        if (!$eavSetup->getAttributeId(Product::ENTITY, $attributeCode)) {
            $eavSetup->addAttribute(
                Product::ENTITY,
                $attributeCode,
                [
                    'type' => 'int',
                    'label' => 'Frontend tarneaeg',
                    'input' => 'select',
                    'source' => \Magento\Eav\Model\Entity\Attribute\Source\Table::class,
                    'frontend' => '',
                    'backend' => '',
                    'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'default' => 0,
                    'group' => 'General',
                    'option' => '',
                    'sort_order' => 50,
                ]
            );
        }

        $setup->endSetup();
    }

}
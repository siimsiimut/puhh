<?php
namespace Siim\OrderStatusApi\Api;

/**
 * @api
 */
interface ResponseItemInterface
{
    const DATA_MESSAGE = 'message';
    const DATA_SHIPPED = 'shippedItems';

    /**
     * gets reponse success message
     * @return string
     */
    public function getMessage();

    /**
     * sets response success message
     * @param string $msg
     * @return $this
     */
    public function setMessage(string $msg);
    
    /**
     * gets shipped orders array
     * @return array
     */
    public function getShippedOrders();

    /**
     * sets response success message
     * @param array $orders
     * @return $this
     */
    public function setShippedOrders(array $orders);
}
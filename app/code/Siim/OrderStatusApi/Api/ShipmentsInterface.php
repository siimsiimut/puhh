<?php
namespace Siim\OrderStatusApi\Api;

/**
 * @api
 */
interface ShipmentsInterface
{
    /**
     * Ships orders
     * @param mixed $orderNrs
     * @return \Siim\OrderStatusApi\Api\ResponseItemInterface
     */
    public function shipOrders(array $orderNrs);
}
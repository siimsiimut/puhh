<?php
namespace Siim\OrderStatusApi\Model;

class ResponseItem extends \Magento\Framework\DataObject implements \Siim\OrderStatusApi\Api\ResponseItemInterface
{
    
    public function __construct()
    {
        $this->setData(self::DATA_SHIPPED, []);
    }

    public function getMessage() : ?string
    {
        return $this->getData(self::DATA_MESSAGE);       
    }
    public function setMessage(string $message = '')
    {
        return $this->setData(self::DATA_MESSAGE, $message);
    }
    
    public function setShippedOrders($orders)
    {
        return $this->setData(self::DATA_SHIPPED, $orders);       
    }
    public function getShippedOrders() : array
    {
        return $this->getData(self::DATA_SHIPPED);
    }

}
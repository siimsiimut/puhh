<?php
namespace Siim\OrderStatusApi\Model;

use Magento\Sales\Api\Data\OrderInterface;

class Shipments implements \Siim\OrderStatusApi\Api\ShipmentsInterface
{
    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;
    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Siim\OrderStatusApi\Logger\Logger
     */
    protected $logger;
    /**
     * @var \Acty\OrderStatusEmail\Model\Processor
     */
    protected $orderStatusProcessor;

    /**
     * @var \Siim\OrderStatusApi\Api\ResponseItemInterfaceFactory
     */
    protected $responseItemFactory;

    public function __construct(
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Siim\OrderStatusApi\Logger\Logger $logger,
        \Acty\OrderStatusEmail\Model\Processor $orderStatusProcessor,
        \Siim\OrderStatusApi\Api\ResponseItemInterfaceFactory $responseItemFactory
    )
    {
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->logger = $logger;
        $this->orderStatusProcessor = $orderStatusProcessor;
        $this->responseItemFactory = $responseItemFactory;
    }
    
    /**
     * @param mixed $orderNrs
     * @return \Siim\OrderStatusApi\Api\ResponseItemInterface
     */
    public function shipOrders ($orderNrs)
    {
        $this->logger->info('shipments from cfs', [
            'requestParam' => $orderNrs
        ]);
        $cleanOrderIncrements = $this->cleanOrderIncrements($orderNrs);
        $orders = $this->getOrderByIncrementId($cleanOrderIncrements);

        $response = $this->responseItemFactory->create();

        if (!$orders) {
            $this->logger->error('No orders found for increments', [
                'requestParam' => $orderNrs 
            ]);
            $response->setMessage('No orders found, check input');
            return $response;
        }

        $this->logger->info('Shipment sent from CFS. Try sending shipment emails for orders:');
        $ordersToTry = [];
        foreach ($orders as $o) {
            $ordersToTry[] = $o->getIncrementId();
        }
        $this->logger->info('', [
            'order_increments' => $ordersToTry
        ]);
        
        $result = $this->orderStatusProcessor->setOrderStatusPostitatud($orders);
        if (count($result)) {
            $this->logger->info('Emails sent for orders:', [
                'order_increments' => $result
            ]);
            $response->setMessage('Orders successfully shipped');
            $response->setShippedOrders($result);
            return $response;
        }
        $response->setMessage('No orders marked as shipped, orders already shipped');
        return $response;
    }
    private function getOrderByIncrementId($incrementIds)
    {
        $criteria = $this->searchCriteriaBuilder->addFilter(OrderInterface::INCREMENT_ID, $incrementIds, 'in')->create();
        $orders = $this->orderRepository->getList($criteria)->getItems();
        return count($orders) ? $orders : null;
    }
    private function cleanOrderIncrements($orderNrs) {
        $result = [];
        $orderNrs = explode(',', $orderNrs);
        foreach($orderNrs as $nr) {
            $nr = explode('MEPS', $nr);
            if (isset($nr[1]) && is_numeric($nr[1])) {
                $result[] = trim($nr[1]);
            }
        }
        return $result;
    }
    
}
<?php

namespace Siim\DisabledMailer\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendDisabledCsvEmailCommand extends Command
{
    const NAME = 'DisabledCsvSender';
    private $db;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;
    /**
     * @var \Magento\Framework\Filesystem
     */
    private $filesystem;
    /**
     * @var \Magento\Framework\File\Csv
     */
    private $csvProcessor;
    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    private $directoryList;

    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\File\Csv $csvProcessor,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Filesystem $filesystem
    )
    {
        $this->logger = $logger;
        $this->resourceConnection = $resourceConnection;
        $this->db = $this->resourceConnection->getConnection();
        $this->filesystem = $filesystem;
        $this->directoryList = $directoryList;
        $this->csvProcessor = $csvProcessor;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('siim:send:disabled:email')
            ->setDescription('Sends .csv of disabled products');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger->info('starting disabled products csv export command');
        try {
            $csv = $this->writeToCsv($this->getDisabledProducts());
            $this->sendMail($csv);
        }
        catch (\Exception $exception) {
            $this->logger->error('Error saving or sending disabled products csv', [
                'err' => $exception
            ]);
        }
    }

    public function executeFromCron()
    {
        $this->logger->info('starting disabled products csv export from cron');
        try {
            $csv = $this->writeToCsv($this->getDisabledProducts());
            $this->sendMail($csv);
        }
        catch (\Exception $exception) {
            $this->logger->error('Error saving or sending disabled products csv', [
                'err' => $exception
            ]);
        }
    }

    protected function getDisabledProducts() : array
    {

        $q = "SELECT cpe.entity_id, cpe.sku, cpev.value AS name, cisi.qty
            FROM catalog_product_entity cpe 
            RIGHT JOIN catalog_product_entity_int cpei 
                ON cpe.entity_id = cpei.entity_id
            RIGHT JOIN catalog_product_entity_varchar cpev
                ON cpe.entity_id = cpev.entity_id
            RIGHT JOIN cataloginventory_stock_item cisi
                ON cpe.entity_id = cisi.product_id
            WHERE cpei.attribute_id = 
                ( SELECT attribute_id FROM `eav_attribute` WHERE `attribute_code` LIKE 'status' )
                AND cpei.value = 2 
                AND cpei.store_id = 0
                AND cpev.attribute_id = 73
                AND cpev.store_id = 0 
                AND cisi.qty > 0
            GROUP BY cpe.sku 
            ORDER BY `cpe`.`entity_id` DESC
        ";
        return $this->db->fetchAll($q);
    }

    protected function sendMail($csvFilePath)
    {
        $senderEmail = 'info@karupoegpuhh.ee';
        $receiverEmail = [
            'annaliisa@karupoegpuhh.ee',
            'elina@karupoegpuhh.ee',
            'karoliine@karupoegpuhh.ee',
            'info@karupoegpuhh.ee'
        ];
        $subject = 'Disabled karupoegpuhh.ee tooted';
        $now = date('l jS \of F Y h:i:s A');
        $body = 'Disabled staatusega tooted seisuga: '.$now;
        $mail = new \Zend_Mail('utf-8');
        $mail->setFrom($senderEmail);
        $mail->addTo($receiverEmail);
        $mail->setSubject($subject);
        $mail->setBodyHtml($body);

        $content = file_get_contents($csvFilePath);

        $attachment = new \Zend_Mime_Part($content);
        $attachment->type = 'text/csv'; // attachment's mime type
        $attachment->disposition = \Zend_Mime::DISPOSITION_ATTACHMENT;
        $attachment->encoding = \Zend_Mime::ENCODING_BASE64;
        $attachment->filename = basename($csvFilePath);
        $mail->addAttachment($attachment);
        $mail->send();
    }
    protected function writeToCsv($data) : string
    {
        $fileDirectoryPath = $this->directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA).'/disabled_products/';
        $date = date("Ymd");
        if(!is_dir($fileDirectoryPath))
            mkdir($fileDirectoryPath, 0777, true);
        $fileName = 'disabled_products_'.$date.'.csv';
        $filePath =  $fileDirectoryPath . $fileName;
        array_unshift($data, ['id'=>'magento_id', 'sku'=>'sku', 'name'=>'name', 'qty'=>'qty']);
        $this->csvProcessor
            ->setEnclosure('"')
            ->setDelimiter(',')
            ->saveData($filePath, $data);

        return $filePath;
    }

}
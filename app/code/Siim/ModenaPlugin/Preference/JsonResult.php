<?php
namespace Siim\ModenaPlugin\Preference;

class JsonResult extends \Magento\Framework\Controller\Result\Json
{
    public function getJsonData()
    {
        return $this->json;
    }
}
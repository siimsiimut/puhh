<?php
namespace Siim\ModenaPlugin\Preference;
use Magento\Sales\Api\Data\OrderInterface;
use Webexpert\Direct\Model\Adminhtml\Source\Mode;

class Request extends \Webexpert\Direct\Model\Request
{
    /**
     * replace $send with the private method in this override class to fix bugs
     */
    public function initPurchase(OrderInterface $order) {
        $this->logger->info('Modena direct init purchase start');
        $this->authenticate();
        $send = $this->fixedSetSend($order);
        try {
          $curl = $this->setCurl();
          $curl->post(sprintf('%s/%s', $this->mode->getDirectRedirect(), Mode::PATH), json_encode($send));
          $this->logger->info('Modena direct init purchase', [
            'order' => $send,
            'curl_headers' => $curl->getHeaders()
          ]);

          if (array_key_exists('location', $curl->getHeaders())) {
            //TODO: figure out how to get this more elegantly
            $lines = explode("\n", $curl->getBody());
            $lines = array_slice($lines, -1);
            $result = json_decode(implode("\n", $lines), TRUE);
            $result['location'] = $curl->getHeaders()['location'];
            return $result;
    
          }
          else {
            $this->logger->info('Error when creating order: ' . $order->getIncrementId());
            $this->logger->error('Error when creating order: ', [
              'curl_headers' => $curl->getHeaders(),
              'order' => $send
            ]);
            throw new \Exception('Error when creating order: ' . $order->getIncrementId());
          }
    
        } catch (\Exception $e) {
          $this->logger->info($e->getMessage());
          throw $e;
        }
    }
    /**
     * the original method throws error with guest purchase
     */
    private function fixedSetSend($order) {
        if (!array_key_exists('modena_option_code', $order->getPayment()
          ->getAdditionalInformation())) {
          return FALSE;
        }
        $selectedOption = $order->getPayment()
          ->getAdditionalInformation()['modena_option_code'];
        if (strpos($selectedOption, 'MASTER') !== FALSE) {
          $selectedOption = 'MASTER_CARD';
        }
    
        $send = [
            "orderId" => $order->getIncrementId(),
            "selectedOption" => $selectedOption,
            "totalAmount" => $order->getGrandTotal(),
            "currency" => $order->getOrderCurrencyCode(),
            'orderItems' => [],
            'customer' => [
                //fix below - first logged out users getCustomerFirstName is null
                "firstName" => $order->getCustomerFirstName() ?? $order->getBillingAddress()->getFirstName(),
                "lastName" => $order->getCustomerLastName() ?? $order->getBillingAddress()->getLastName(),
                "phoneNumber" => $order->getShippingAddress()->getTelephone(),
                "email" => $order->getCustomerEmail(),
            ],
            "timestamp" => str_replace(" ", "T", $order->getCreatedAt()) . 'Z',
            "returnUrl" => $this->url->getUrl('direct/response/success'),
            "cancelUrl" => $this->url->getUrl('direct/response/cancel'),
            "callbackUrl" => $this->url->getUrl('direct/response/callback'),
        ];
        $items = $order->getAllVisibleItems();
        foreach ($items as $item) {
          $send['orderItems'][] = [
            'description' => $item->getName(),
            'amount' => (string) $item->getRowTotalInclTax(),
            'currency' => $order->getOrderCurrencyCode(),
            'quantity' => round($item->getQtyOrdered()),
          ];
        }
        $send['orderItems'][] = [
          'description' => $order->getShippingDescription(),
          'amount' => $order->getShippingInclTax(),
          'currency' => $order->getOrderCurrencyCode(),
          'quantity' => '1',
        ];
        if($order->getDiscountAmount() != '0.0000'){
          $send['orderItems'][] = [
            'description' => 'Sooduskood',
            'amount' => $order->getDiscountAmount(),
            'currency' => $order->getOrderCurrencyCode(),
            'quantity' => '1',
          ];
        }
        return $send;
    }
    /**
     * original function from module
     */
    private function setCurl() {
        //using a seprate curl solution because the other one didnt seem to use some of the options we need
        $curl = $this->curlClient;
        $curl->setHeaders(
          [
            'Authorization' => 'Bearer ' . $this->authToken,
            'Content-Type' => 'application/json',
          ]
        );
        $curl->setOption(CURLOPT_SSL_VERIFYPEER, 0);
        $curl->setOption(CURLOPT_SSL_VERIFYHOST, 0);
        $curl->setOption(CURLOPT_URL, sprintf('%s/%s', $this->mode->getDirectRedirect(), Mode::PATH));
        $curl->setOption(CURLOPT_RETURNTRANSFER, 1);
        $curl->setOption(CURLOPT_HEADER, 1);
        $curl->setOption(CURLOPT_POST, 1);
        $curl->setOption(CURLOPT_FOLLOWLOCATION, FALSE);
        return $curl;
    
      }
}

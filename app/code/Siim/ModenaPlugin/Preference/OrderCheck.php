<?php
namespace Siim\ModenaPlugin\Preference;

class OrderCheck extends \Webexpert\Direct\Model\OrderCheck
{
    const modenaDirectPaymentMethodStringPartial = 'ModenaDirect';

    public function getOrder($incrementId, $paymentId) {

        $collection = $this->orderCollectionFactory->create();
        $collection->getSelect()->join(
          ['p' => $collection->getTable('sales_order_payment')],
          'p.parent_id = main_table.entity_id',
          'p.method'
        )
          ->where('p.method LIKE ?', '%'.self::modenaDirectPaymentMethodStringPartial.'%')
          ->where('main_table.increment_id = ?', $incrementId)
          ->where('p.direct_id = ?', $paymentId);
        foreach ($collection as $order) {
          //change order payment method back to original modenadirect
          $payment = $order->getPayment();
          $payment->setMethod( \Webexpert\Direct\Model\ConfigProvider::DIRECT_CODE);
          $payment->save();
          $order->save();
          return $order;
        }
    
        return FALSE;
      }
}
<?php

namespace Siim\ModenaPlugin\Preference;

class ConfigProvider extends \Webexpert\Direct\Model\ConfigProvider
{
    
    private $storeManager;
    private $checkoutSession;
    protected $request;
    protected $urlInterface;

    public function __construct(
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Webexpert\Direct\Model\Request $request
    ) {
        $this->urlInterface = $urlInterface;
        $this->storeManager = $storeManager;
        $this->checkoutSession = $checkoutSession;
        $this->request = $request;
    }

    public function getConfig()
    {
        return [
            'payment' => [
                self::DIRECT_CODE => [
                    'redirect_url' => $this->urlInterface->getUrl('direct/request/redirect'),
                    'code' => self::DIRECT_CODE,
                    'icons' => $this->getIconsFixed(),
                ],
            ],
        ];
    }
    /* 
        this function is modified compared to original. Fixed getting quote id from checkoutsession
    */
    private function getIconsFixed()
    {
        $quoteId = '';
        $count = 1;
        if ($this->checkoutSession->getQuote()) {
            $quoteId = $this->checkoutSession->getQuote()->get('id');
        }
        $result = $this->request->banksIcons();
        if (!$result) {
            return [];
        }
        $selected = $this->getSelected();
        foreach ($result as $key => $item) {
            $result[$key]->quote = $quoteId;
            $result[$key]->domainUrl = $this->storeManager->getStore()->getBaseUrl();
            if ($selected == $item->code) {
                $result[$key]->selected = 1;
            } else {
                $result[$key]->selected = 0;
            }
            if ($item->code == 'CREDIT_CARD') {
                $result[$key]->code = $result[$key]->code . '_' . $count;
                $count++;
            }
            if ($item->code == 'HABAEE2X' && !$selected) {
                $result[$key]->selected = 1;
            }
        }
        return $result;
    }
    private function getSelected()
    {
        if ($this->checkoutSession->getQuote()) {
            $quote = $this->checkoutSession->getQuote();
            if (array_key_exists('modena_option_label', $quote->getPayment()
                ->getAdditionalInformation())) {
                return $quote->getPayment()
                    ->getAdditionalInformation()['modena_option_code'];
            }
        }
        return '';
    }
}

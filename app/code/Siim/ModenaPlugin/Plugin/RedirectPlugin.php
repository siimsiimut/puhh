<?php
namespace Siim\ModenaPlugin\Plugin;
/* 
    Takes json output from Webexpert_Direct (Modena) and uses redirect url to make a proper redirect
*/
class RedirectPlugin {
    
    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    private $resultFactory;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    private $redirect;
    public function __construct(
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Magento\Framework\App\Response\RedirectInterface $redirect
    )
    {
        $this->redirect = $redirect;
        $this->resultFactory = $resultFactory;
    }
    public function afterExecute(\Webexpert\Direct\Controller\Request\Redirect $subject, $result)
    {
        $resultRedirect = $this->resultFactory->create(
            \Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT
        );
        $response = json_decode($result->getJsonData(), true);
        if (isset($response['redirect_url'])) {
            $resultRedirect->setUrl($response['redirect_url']); 
        } else {
            $resultRedirect->setUrl('/');
        }
        return $resultRedirect;
    }
}
<?php

namespace Siim\ModenaPlugin\Plugin;

use Magento\Sales\Model\Order;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderPaymentInterface;
/* 
    Plugin to change status to 'makstud' instead of 'processing'
*/

class CaptureCommandPlugin
{
    public function afterExecute(
        \Magento\Sales\Model\Order\Payment\State\CaptureCommand $subject,
        $result,
        OrderPaymentInterface $payment, $amount, OrderInterface $order
    ) {
        if ($order->getState() == Order::STATE_PROCESSING) {
            $order->setStatus('makstud');
        }
        return $result;
    }
}

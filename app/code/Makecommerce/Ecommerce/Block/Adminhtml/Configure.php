<?php

namespace Makecommerce\Ecommerce\Block\Adminhtml;

class Configure extends \Magento\Backend\Block\Template
{
    /**
     * @var string
     */
    protected $_template = 'products/configure.phtml';

    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $url;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Model\UrlInterface $url
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Model\UrlInterface $url,
        array $data = []
    ) {
        $this->url = $url;
        parent::__construct($context, $data);
    }

    /**
     * Get text to display to customer
     *
     * @return string
     */
    public function getDisplayText()
    {
        return __('Should all products be allowed for shipping to parcel machine?');
    }

    /**
     * Get 'yes' or 'no' button html
     *
     * @param bool $type
     * @return string
     */
    public function getButtonHtml($type)
    {
        return $this->createButtonHtml(
            $type ? 'allow_parcel_true_btn' : 'allow_parcel_false_btn',
            $type ? __('Yes') : __('No'),
            $type ? 'setProductsAllowed(true)' : 'setProductsAllowed(false)'
        );
    }

    /**
     * Creates button html
     *
     * @param string $id
     * @param string $label
     * @param string $js
     * @return string
     */
    private function createButtonHtml($id, $label, $js)
    {
        $button = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Button'
        )->setData(
            [
                'id' => $id,
                'label' => $label,
                'onclick' => $js
            ]
        );

        return $button->toHtml();
    }

    /**
     * Returns url for ajax request
     *
     * @param bool $isAllowed
     * @return string
     */
    public function getActionUrl($isAllowed)
    {
        return $this->url->getUrl(
            'makecommerce/products/apply',
            ['form_key' => $this->getFormKey(),
            'allowed' => $isAllowed ? 1 : 0]
        );
    }

    /**
     * Gets title for popup after ajax
     *
     * @return string
     */
    public function getResponseTitle()
    {
        return __('Configuration Response');
    }

    /**
     * Gets success message for popup after ajax
     *
     * @return string
     */
    public function getSuccessMessage()
    {
        return __('Your setting was successfully applied!');
    }

    /**
     * Gets error message for popup after ajax
     *
     * @return string
     */
    public function getErrorMessage()
    {
        return __('Something went wrong while applying settings.');
    }
}

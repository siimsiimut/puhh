<?php

namespace Makecommerce\Ecommerce\Block\Form;

use Magento\Framework\View\Element\Template\Context;
use Magento\Payment\Block\Form;
use Makecommerce\Ecommerce\Config;
use Makecommerce\Ecommerce\Model\MakecommerceConfigProvider;

class Makecommerce extends Form
{
    /**
     * @var string
     */
    protected $_template = 'Makecommerce_Ecommerce::form/makecommerce.phtml';

    /**
     * Payment config model
     *
     * @var Config
     */
    protected $config;

    /**
     * @var MakecommerceConfigProvider
     */
    protected $configProvider;

    /**
     * Makecommerce constructor.
     * @param Context $context
     * @param Config $config
     * @param MakecommerceConfigProvider $configProvider
     * @param array $data
     */
    public function __construct(
        Context $context,
        Config $config,
        MakecommerceConfigProvider $configProvider,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->config = $config;
        $this->configProvider = $configProvider;
        $this->config = $configProvider->getConfig();
        $this->config = $this->config['mk'] ?? null;
        $this->setTemplate('form/mk.phtml');
    }

    /**
     * Retrieve mk banklinks
     *
     * @return array|null
     */
    public function getBankLinks()
    {
        return $this->config['paymentMethods']['banklinks'] ?? null;
    }

    /**
     * Get the UI mode
     *
     * @return string|null
     */
    public function getUiMode()
    {
        return $this->config['ui']['mode'] ?? null;
    }

    /**
     * Check if countries are grouped
     *
     * @return bool|null
     */
    public function areCountriesGrouped()
    {
        return $this->config['ui']['groupBanklinks'] ?? null;
    }

    /**
     * Check if countries are grouped
     *
     * @return bool|null
     */
    public function areCardsGrouped()
    {
        return $this->config['ui']['widgetGroupcc'] ?? null;
    }

    /**
     * Get credit cards
     *
     * @return mixed|null
     */
    public function getCreditCards()
    {
        return $this->config['paymentMethods']['cards'] ?? null;
    }

    /**
     * Get payment method code
     *
     * @return string
     */
    public function getMethodCode(): string
    {
        return \Makecommerce\Ecommerce\Model\Makecommerce::PAYMENT_METHOD_CODE;
    }

    /**
     * Get countries
     *
     * @return mixed|null
     */
    public function getCountries()
    {
        return $this->config['countries'] ?? null;
    }

    /**
     * Get default country
     *
     * @return string
     */
    public function getDefaultCountry(): string
    {
        return 'ee';
    }

    /**
     * {@inheritdoc}
     */
    protected function _toHtml(): string
    {
        $this->_eventManager->dispatch('mk_form_block_to_html_before', ['block' => $this]);
        return parent::_toHtml();
    }
}

<?php

namespace Makecommerce\Ecommerce\Block\System\Config;

use Magento\Framework\DataObject;

class Button extends \Magento\Config\Block\System\Config\Form\Field
{
    // @codingStandardsIgnoreStart
    /**
     * @var string
     */
    protected $_template = 'Makecommerce_Ecommerce::system/config/button.phtml';

    /**
     * @var \Makecommerce\Ecommerce\Helper\Data $helper
     */
    protected $helper;

    /**
     * Button constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Model\Url $url
     * @param \Makecommerce\Ecommerce\Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Model\Url $url,
        \Makecommerce\Ecommerce\Helper\Data $helper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_url = $url;
        $this->helper = $helper;
    }
    // @codingStandardsIgnoreEnd

    /**
     * Get html
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return $this->_toHtml();
    }
    // @codingStandardsIgnoreEnd

    /**
     * Get update url
     *
     * @return string
     */
    public function getUpdateUrl()
    {
        return $this->_url->getUrl('makecommerce/configuration/update');
    }

    /**
     * Get message title
     *
     * @return \Magento\Framework\Phrase
     */
    public function getMessageTitle()
    {
        return __('Update Message');
    }

    /**
     * Get success message
     *
     * @return \Magento\Framework\Phrase
     */
    public function getSuccessMessage()
    {
        return __('Payment methods were successfully updated.');
    }

    /**
     * Get failure message
     *
     * @return \Magento\Framework\Phrase
     */
    public function getFailureMessage()
    {
        return __('There was an error updating payment methods.');
    }

    /**
     * Get html for the button
     *
     * @return string
     */
    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Button'
        )->setData(
            [
                'id'      => 'custom_button',
                'label'   => __('Update'),
                'onclick' => 'updateLinks()',
            ]
        );

        return $button->toHtml();
    }

    /**
     * Gets current selected scope data
     *
     * @return DataObject
     */
    public function getScopeData()
    {
        return $this->helper->getScopeData();
    }
}

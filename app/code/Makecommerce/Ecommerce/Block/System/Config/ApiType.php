<?php

namespace Makecommerce\Ecommerce\Block\System\Config;

class ApiType extends \Magento\Config\Block\System\Config\Form\Field
{
    // @codingStandardsIgnoreStart
    /**
     * @var \Makecommerce\Ecommerce\Config
     */
    protected $config;

    /**
     * ApiType constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Makecommerce\Ecommerce\Config $config
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Makecommerce\Ecommerce\Config $config,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->config = $config;
    }
    // @codingStandardsIgnoreEnd

    /**
     * Render element value also add test link
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _renderValue(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        if ($element->getTooltip()) {
            $html = '<td class="value with-tooltip">';
            $html .= $this->_getElementHtml($element);
            $html .= '<div class="tooltip"><span class="help"><span></span></span>';
            $html .= '<div class="tooltip-content">' . $element->getTooltip() . '</div></div>';
            $html .= '<a style="display:block" class="mk-test-link" target="_blank" href="' .
                $this->config->getValue('testenv_homepage_url') . '">' .
            __('See more about Test environment') . '</a>' .
                '<script>
                        var link = document.querySelector(".mk-test-link");
                        var select = link.parentElement.firstChild;
                        select.onchange = function(){
                            if(select.value === "test"){
                                link.style.display = "block";
                            } else {
                                link.style.display = "none";
                            }
                        }
                        if(select.value === "live"){
                            link.style.display = "none";
                        }
                </script>';
        } else {
            $html = '<td class="value">';
            $html .= $this->_getElementHtml($element);
            $html .= '<a style="display:block" class="mk-test-link" target="_blank" href="' .
                $this->config->getValue('testenv_homepage_url') . '">' .
            __('See more about Test environment') . '</a>' .
                '<script>
                        var link = document.querySelector(".mk-test-link");
                        var select = link.parentElement.firstChild;
                        select.onchange = function(){
                            if(select.value === "test"){
                                link.style.display = "inline";
                            } else {
                                link.style.display = "none";
                            }
                        }
                        if(select.value === "live"){
                            link.style.display = "none";
                        }
                </script>';
        }
        if ($element->getComment()) {
            $html .= '<p class="note"><span>' . $element->getComment() . '</span></p>';
        }
        $html .= '</td>';
        return $html;
    }
}

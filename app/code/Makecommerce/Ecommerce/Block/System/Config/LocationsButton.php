<?php

namespace Makecommerce\Ecommerce\Block\System\Config;

class LocationsButton extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @var string
     */
    protected $_template = 'Makecommerce_Ecommerce::system/config/locations_button.phtml';

    /**
     * @var \Magento\Backend\Model\Url
     */
    protected $url;

    /**
     * @var \Magento\Backend\Block\Template\Context $context
     * @var array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Model\Url $url,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->url = $url;
    }

    /**
     * @var \Magento\Framework\Data\Form\Element\AbstractElement
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return $this->_toHtml();
    }

    /**
     * Creates custom button html
     */
    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Button'
        )->setData(
            [
                'id' => 'mc_update_locations_btn',
                'label' => __('Update'),
                'onclick' => 'updateLocations(this)',
                'mytest' => 'asdasdas'
            ]
        );
        return $button->toHtml();
    }

    /**
     * Returns popup title
     *
     * @return string
     */
    public function getMessageTitle()
    {
        return __('Update Message');
    }

    /**
     * Returns success message
     *
     * @return string
     */
    public function getSuccessMessage()
    {
        return __('Shipping method\'s locations were successfully updated.');
    }

    /**
     * Returns error message
     *
     * @return string
     */
    public function getErrorMessage()
    {
        return __('An error occurred while updating shipping method\'s locations.');
    }

    /**
     * Returns update controller's url
     *
     * @return string
     */
    public function getUpdateUrl()
    {
        return $this->url->getUrl('makecommerce/configuration/locations');
    }
}

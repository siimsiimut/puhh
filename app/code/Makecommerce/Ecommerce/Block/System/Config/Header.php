<?php

namespace Makecommerce\Ecommerce\Block\System\Config;

class Header extends CustomField
{
    // @codingStandardsIgnoreStart
    /**
     * @var string
     */
    protected $_template = 'Makecommerce_Ecommerce::system/config/header.phtml';
    // @codingStandardsIgnoreEnd

    /**
     * Get image url
     *
     * @return string
     */
    public function getImageUrl()
    {
        return $this->getViewFileUrl(
            'Makecommerce_Ecommerce::images/'
            . 'makecommerce_logo_en.svg'
        );
    }
}

<?php

namespace Makecommerce\Ecommerce\Block\System\Config\Form\Dpd;

use Makecommerce\Ecommerce\Block\System\Config\Form\Configuration;

class Parcel extends Configuration
{
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Data\Form\Element\Factory $elementFactory
     * @param \Makecommerce\Ecommerce\Model\System\Config\Source\Countries\Dpd $countries
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Data\Form\Element\Factory $elementFactory,
        \Makecommerce\Ecommerce\Model\System\Config\Source\Countries\Dpd $countries,
        array $data = []
    ) {
        parent::__construct($context, $elementFactory, $data);
        $this->countries = $countries->toOptionArray();
    }
}

<?php

namespace Makecommerce\Ecommerce\Block\System\Config\Form;

class Configuration extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
    /**#@+
     * Constants defined for keys of array, makes typos less likely
     */
    const COUNTRY_FIELD = 'country';
    const PRICE_FIELD = 'price';
    const FREE_SHIPPING_FIELD = 'free_shipping';
    /**#@-*/

    /**
     * @var \Magento\Framework\Data\Form\Element\Factory
     */
    protected $elementFactory;

    /**
     * @var array
     */
    protected $countries = [];

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Data\Form\Element\Factory $elementFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Data\Form\Element\Factory $elementFactory,
        array $data = []
    ) {
        $this->elementFactory = $elementFactory;
        parent::__construct($context, $data);
    }

    /**
     * Initialise form fields
     *
     * @return void
     */
    protected function _construct()
    {
        $this->addColumn(self::COUNTRY_FIELD, ['label' => __('Country')]);
        $this->addColumn(self::PRICE_FIELD, ['label' => __('Price')]);
        $this->addColumn(self::FREE_SHIPPING_FIELD, ['label' => __('Free Shipping From')]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
        parent::_construct();
    }

    /**
     * Render array cell for prototypeJS template
     *
     * @param string $columnName
     * @return string
     */
    public function renderCellTemplate($columnName)
    {
        if ($columnName === self::COUNTRY_FIELD) {
            $options = $this->countries;
            return $this->createElement('select', $columnName, $options, '');
        }
        return parent::renderCellTemplate($columnName);
    }

    /**
     * Creates element for column
     *
     * @param string $type
     * @param string $columnName
     * @param array $options
     * @param string $class
     * @return string
     */
    protected function createElement($type, $columnName, $options, $class = null)
    {
        $element = $this->elementFactory->create($type);
        $element->setForm(
            $this->getForm()
        )->setName(
            $this->_getCellInputElementName($columnName)
        )->setHtmlId(
            $this->_getCellInputElementId('<%- _id %>', $columnName)
        )->setClass(
            $class
        )->setValues(
            $options
        );
        return str_replace("\n", '', $element->getElementHtml());
    }
}

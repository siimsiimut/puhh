<?php

namespace Makecommerce\Ecommerce\Block\System\Config\Form\Smartpost;

use Makecommerce\Ecommerce\Block\System\Config\Form\Configuration;

class Courier extends Configuration
{
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Data\Form\Element\Factory $elementFactory
     * @param \Makecommerce\Ecommerce\Model\System\Config\Source\Countries\Smartpost $countries
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Data\Form\Element\Factory $elementFactory,
        \Makecommerce\Ecommerce\Model\System\Config\Source\Countries\Smartpost $countries,
        array $data = []
    ) {
        parent::__construct($context, $elementFactory, $data);
        $this->countries = $countries->toOptionArray();
    }
}

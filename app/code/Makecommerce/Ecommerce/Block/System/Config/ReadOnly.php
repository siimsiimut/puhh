<?php
namespace Makecommerce\Ecommerce\Block\System\Config;

class ReadOnly extends \Magento\Config\Block\System\Config\Form\Field
{
    // @codingStandardsIgnoreStart
    /**
     * ReadOnly constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }
    // @codingStandardsIgnoreEnd

    /**
     * Get html
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $element->setReadonly(true);
        return $element->getElementHtml();
    }
}

<?php

namespace Makecommerce\Ecommerce\Block\System\Config;

class ApiLink extends CustomField
{
    // @codingStandardsIgnoreStart
    /**
     * @var string
     */
    protected $_template = 'Makecommerce_Ecommerce::system/config/api_link.phtml';
    // @codingStandardsIgnoreEnd

    /**
     * Get URL for API configurations
     *
     * @return string
     */
    public function getApiUrl()
    {
        return $this->getUrl('adminhtml/*/edit', ['section' => 'makecommerce_api']);
    }
}

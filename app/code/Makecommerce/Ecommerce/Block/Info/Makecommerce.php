<?php

namespace Makecommerce\Ecommerce\Block\Info;

class Makecommerce extends \Magento\Payment\Block\Info
{
    // @codingStandardsIgnoreStart
    /**
     * Prepare information specific to current payment method
     *
     * @param null|array $transport
     * @return \Magento\Framework\DataObject
     */
    protected function _prepareSpecificInformation($transport = null)
    {
        $transport = parent::_prepareSpecificInformation($transport);
        $data = [];
        $info = $this->getInfo();

        if ($this->_appState->getAreaCode() === \Magento\Backend\App\Area\FrontNameResolver::AREA_CODE
            && $info->getAdditionalInformation()
        ) {
            $data[__('Payment method title')->getText()] = $info->getAdditionalInformation()['method_title'];
            $data[__('Last Updated')->getText()] = $info->getOrder()->getUpdatedAt();
        }
        return $transport->setData(array_merge($data, $transport->getData()));
    }
    // @codingStandardsIgnoreEnd
}

<?php

namespace Makecommerce\Ecommerce\Client;

use Magento\Framework\Exception\LocalizedException;

// @codingStandardsIgnoreFile
class MakecommerceClientWrapper
{
    /**
     * Allows clean access to payment method config
     * path: payment/$methodcode/$field
     * @var \Makecommerce\Ecommerce\Config
     */
    protected $config;

    /**
     * ModuleResource allows us to find out what setup version are we using
     * @var \Magento\Framework\Module\ModuleResource
     */
    protected $moduleResource;

    // @codingStandardsIgnoreStart
    /**
     * @var \Makecommerce\Ecommerce\Logger\Logger
     */
    protected $logger;

    /**
     * @var MakecommerceLib
     */
    protected $mkClient;

    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    protected $productMetadata;
    // @codingStandardsIgnoreEnd

    /**
     * MakecommerceClientWrapper constructor.
     * @param \Makecommerce\Ecommerce\Config $config
     * @param \Makecommerce\Ecommerce\Logger\Logger $logger
     * @param \Magento\Framework\Module\ModuleResource $moduleResource
     * @param \Magento\Framework\App\ProductMetadataInterface $productMetadata
     */
    public function __construct(
        \Makecommerce\Ecommerce\Config $config,
        \Makecommerce\Ecommerce\Logger\Logger $logger,
        \Magento\Framework\Module\ModuleResource $moduleResource,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata
    ) {
        if ($config->getApiValue('environment') === 'test') {
            $isTest     = true;
            $shopId     = $config->getApiValue('test_shop_id');
            $privateKey = $config->getApiValue('test_key_secret');
            $publicKey  = $config->getApiValue('test_key_public');
        } else {
            $shopId     = $config->getApiValue('shop_id');
            $privateKey = $config->getApiValue('api_secret');
            $publicKey  = $config->getApiValue('api_public');
            $isTest     = false;
        }

        $this->mkClient = new \Makecommerce\Ecommerce\Client\MakecommerceLib(
            $shopId,
            $publicKey,
            $privateKey,
            $isTest
        );
        $this->logger          = $logger;
        $this->config        = $config;
        $this->moduleResource = $moduleResource;
        $this->productMetadata = $productMetadata;
    }

    /**
     * Get URL's of endpoints of current environment (Test vs Live)
     *
     * @return object
     */
    public function getEnvUrls()
    {
        return $this->mkClient->getEnvUrls();
    }

    /**
     * Set API base URL
     *
     * @param string $value
     * @return void
     */
    public function setApiUrl($value)
    {
        $this->mkClient->setApiUrl($value);
    }

    /**
     * Get API base URL
     *
     * @return string
     */
    public function getApiUrl()
    {
        return $this->mkClient->getApiUrl();
    }

    /**
     * Set GW base URL
     *
     * @param string $value
     * @return void
     */
    public function setGwUrl($value)
    {
        $this->mkClient->setGwUrl($value);
    }

    /**
     * Get GW base URL
     *
     * @return string
     */
    public function getGwUrl()
    {
        return $this->mkClient->getGwUrl();
    }

    /**
     * Set URL for static resources ( js, images)
     *
     * @param string $value
     * @return void
     */
    public function setStaticsUrl($value)
    {
        $this->mkClient->setStaticsUrl($value);
    }

    /**
     * Get URL for static resources ( js, images)
     *
     * @return string
     */
    public function getStaticsUrl()
    {
        return $this->mkClient->getStaticsUrl();
    }

    /**
     * Set Shop ID
     *
     * @param string $value
     * @return void
     */
    public function setShopId($value)
    {
        $this->mkClient->setShopId($value);
    }

    /**
     * Get Shop ID
     *
     * @return string
     */
    public function getShopId()
    {
        return $this->mkClient->getShopId();
    }

    /**
     * Set Publishable Key
     *
     * @param string $value
     * @return void
     */
    public function setPublishableKey($value)
    {
        $this->mkClient->setPublishableKey($value);
    }

    /**
     * Get Publishable Key
     *
     * @return string
     */
    public function getPublishableKey()
    {
        return $this->mkClient->getPublishableKey();
    }

    /**
     * Set Secret Key
     *
     * @param string $value
     * @return void
     */
    public function setSecretKey($value)
    {
        $this->mkClient->setSecretKey($value);
    }

    /**
     * Get Secret Key
     *
     * @return string
     */
    public function getSecretKey()
    {
        return $this->mkClient->getSecretKey();
    }

    /**
     * Returns the Response object of the last API request
     *
     * @return obj
     */
    public function getLastApiResponse()
    {
        return $this->mkClient->getLastApiResponse();
    }

    /**
     * Get shop data
     *
     * @throws Exception If failed to get shop data
     * @return obj Shop object
     */
    public function getShop()
    {
        return $this->mkClient->getShop();
    }

    /**
     * Get shop config for e-shop integration
     *
     * @param string $environment json-encoded key-value pairs describing the e-shop environment
     * @throws Exception if failed to get shop configuration
     * @return obj Shop configuration object
     */
    public function getShopConfig($environment = null)
    {
        if (empty($environment['environment'])) {
            $version = $this->config->getModuleVersion();
            $magentoVersion = $this->productMetadata->getVersion();
            $environment['environment'] = json_encode(
                ["platform" => "magento " . $magentoVersion, "module" => "mk-payment " . $version]
            );
        }
        return $this->execute('getShopConfig', $environment);
    }

    /**
     * Update shop data
     *
     * @param mixed An object or array containing request body
     * @throws Exception if failed to update shop data
     * @return obj Shop object
     */
    public function updateShop($requestBody)
    {
        return $this->execute('updateShop', $requestBody);
    }

    /**
     * Create new transaction
     *
     * @throws Exception if failed to create transaction
     * @return obj Transaction object
     */
    public function createTransaction($requestBody = null)
    {
        if (empty($requestBody['transaction']) || empty($requestBody['customer'])) {
            throw new LocalizedException(
                __("Creating a transaction requires transaction and customer data")
            );
        }
        $config = $this->config;
        $this->logger->info($this->mkClient->getApiUrl());
        return $result = $this->execute('createTransaction', $requestBody);
    }

    /**
     * Get transaction details
     *
     * @param string $transactionId Transaction ID
     * @throws Exception if failed to get transaction object
     * @return obj Transaction object
     */
    public function getTransaction($transactionId)
    {
        return $this->execute('getTransaction', $transactionId);
    }

    /**
     * Get transactions list
     *
     * @param array $params Associative array of query parameters
     * @return obj Transactions list
     */
    public function getTransactions($params = [])
    {
        return $this->execute('getTransactions', $params);
    }

    /**
     * Append metadata to Transaction's merchant_data container
     *
     * @param string $transactionId Transaction ID
     * @param string $params json object, key=merchant_data, {"merchant_data":"my new metadata"}
     * @throws Exception if failed to append metadata
     *
     */
    public function addTransactionMeta($transactionId, $params)
    {
        return $this->mkClient->addTransactionMeta($transactionId, $params);
    }

    /**
     *
     *
     * @return string Token
     */
    public function createToken($requestBody = null)
    {
        if (empty($requestBody['transaction'])) {
            $this->logger->error(__('Creating a token requires a transaction'));
            throw new LocalizedException(__('Creating a token requires a transaction'));
        }
        if (empty($requestBody['customer'])) {
            $this->logger->error(__('Creating a token requires a customer'));
            throw new LocalizedException(__('Creating a token requires a customer'));
        }
        if (empty($requestBody['card'])) {
            $this->logger->error(__('Creating a token requires a card'));
            throw new LocalizedException(__('Creating a token requires a card'));
        }
        return $this->execute('createToken', $requestBody);
    }

    /**
     * Get token by email or cookie ID
     *
     * @param string $requestParams Request parameters
     * @throws Exception if failed to get token object
     * @return obj Token object
     */
    public function getToken($requestParams)
    {
        return $this->execute('getToken', $requestParams);
    }

    /**
     *
     *
     * @return string Payment
     */
    public function createPayment($transactionId, $requestBody = null)
    {
        if (empty($requestBody['currency'])) {
            $this->logger->error(__('Creating a payment requires a currency'));
            throw new LocalizedException(__('Creating a payment requires a currency'));
        }
        if (empty($requestBody['amount'])) {
            $this->logger->error(__('Creating a payment requires an amount'));
            throw new LocalizedException(__('Creating a payment requires an amount'));
        }
        if (empty($requestBody['token'])) {
            $this->logger->error(__('Creating a payment requires a token'));
            throw new LocalizedException(__('Creating a payment requires a token'));
        }
        if ($transactionId === null) {
            $this->logger->error(
                __('Creating a payment requires a transaction id')
            );
            throw new LocalizedException(
                __('Creating a payment requires a transaction id')
            );
        }
        return $this->execute('createPayment', $transactionId, $requestBody);
    }

    /**
     *
     *
     * @return string Refund
     */
    public function createRefund($transactionId, $requestBody)
    {
        return $this->execute('createRefund', $transactionId, $requestBody);
    }

    /**
     * Get refund details
     *
     * @param string $refundId Refund ID
     * @throws Exception if failed to get refund object
     * @return obj Refund object
     */
    public function getRefund($refundId)
    {
        return $this->execute('getRefund', $refundId);
    }

    /**
     * Get a list of a transaction's refunds
     *
     * @param string $transactionId Transaction ID
     * @throws Exception if failed to get refunds list
     * @return array Refund objects
     */
    public function getTransactionRefunds($transactionId)
    {
        return $this->execute('getTransactionRefunds', $transactionId);
    }

    /**
     * Get a list of refunds
     *
     * @throws Exception if failed to get refunds list
     * @return array Refund objects
     */
    public function getRefunds()
    {
        return $this->execute('getRefunds');
    }

    /**
     * Get payment methods
     *
     * @param mixed An object or array containing request parameters
     * @throws Exception if failed to get payment methods
     * @return obj An object containing grouped lists of Payment Method objects
     */
    public function getPaymentMethods($requestParams)
    {
        if (empty($requestParams['currency'])) {
            $requestParams['currency'] = $this->config->getValue(
                'default_currency'
            );
        }
        return $this->execute('getPaymentMethods', $requestParams);
    }

    /**
     * Get carrier-specific destinations for shipments (list of Automated Parcel Machines)
     *
     * @param mixed. An object or array containing request body
     * @throws Exception if failed to retrieve the listing
     * @return obj Shop configuration object
     */
    public function getDestinations($requestBody)
    {
        return $this->mkClient->getDestinations($requestBody);
    }
    
    /**
     * Create new shipments at carrier systems
     *
     * @param mixed An object or array containing request body
     * @throws Exception if failed to create transaction
     * @return obj Transaction object
     */
    public function createShipments($requestBody)
    {
        return $this->execute('createShipments', $requestBody);
    }

    /**
     * get label formats
     *
     * @throws Exception if failed to get label formats
     * @return array List of label formats
     */
    public function getLabelFormats()
    {
        return $this->execute('getLabelFormats');
    }
    
    /**
     * generate parcel labels for shipments registered at carriers
     *
     * @param mixed An object or array containing request body
     * @throws Exception if failed to create labels
     * @return obj Transaction object
     */
    public function createLabels($requestBody)
    {
        return $this->execute('createLabels', $requestBody);
    }
    
    /**
     * generate shopping cart for SimpleCheckout
     *
     * @param mixed An object or array containing request body
     * @throws Exception if failed to create transaction
     * @return obj Transaction object
     */
    public function createCart($requestBody)
    {
        return $this->execute('createCart', $requestBody);
    }

    /**
     * Executes the library methods and logs
     * the given arguments and the result
     *
     * @param string $method Name of the method to be executed
     * @param takes Undefined number of parameters specified by the library method
     * @return mixed Response from the library method
     */
    public function execute($method)
    {
        $num = func_num_args();
        $argumentArray = [];
        $logger = $this->logger;
        try {
            if ($num > 1) {
                for ($idx = 1; $idx < $num; $idx++) {
                    array_push($argumentArray, func_get_arg($idx));
                }

                $argumentArrayJson = json_encode($argumentArray, JSON_PRETTY_PRINT);
                $logger->info(__('Executing method ' . $method . ' with arguments: ' . $argumentArrayJson));

                $response = call_user_func_array([$this->mkClient, $method], $argumentArray);
            } else {
                $logger->info(__('Executing method ' . $method));

                $response = $this->mkClient->$method();
            }
        } catch (\Exception $exception) {
            throw new LocalizedException(__($exception->getMessage()));
            $logger->info(__('Error occurred. Check error logs for more detailed description.'));
            $logger->error(__($exception->getMessage()));
        }

        $responseJson = json_encode($response, JSON_PRETTY_PRINT);
        $logger->info(__('Got the response: ' . $responseJson));

        return $response;
    }
}

<?php

namespace Makecommerce\Ecommerce\Client;

use Exception;

// @codingStandardsIgnoreFile

class MKException extends Exception
{

    protected $raw_content = '';

    public function __construct($raw_content, $message, $code = 0, Exception $previous = null)
    {
        $this->raw_content = $raw_content;
        parent::__construct($message, $code, $previous);
    }

    public function getRawContent()
    {
        return $this->raw_content;
    }
}

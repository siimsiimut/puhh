<?php

namespace Makecommerce\Ecommerce\Api;

/**
 * Interface for managing quote payment information
 * @api
 */
interface PaymentInformationAdapterInterface
{
    /**
     * Create the transaction and place the order.
     * AdapterResponseInterface contains both the increment id and transaction.
     *
     * @param int $cartId
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @param string $country
     * @param \Magento\Quote\Api\Data\AddressInterface|null $billingAddress
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @return \Makecommerce\Ecommerce\Api\Data\AdapterResponseInterface
     */
    public function adaptPlaceOrder(
        $cartId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        $country,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    );
}

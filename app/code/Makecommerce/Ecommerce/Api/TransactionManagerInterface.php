<?php

namespace Makecommerce\Ecommerce\Api;

/**
 * Interface for creating mk transactions
 * @api
 */
interface TransactionManagerInterface
{
    /**
     * Calls the MakeCommerce Api and creates the transaction.
     *
     * @param \Makecommerce\Ecommerce\Api\Data\TransactionInterface $transaction
     * @param \Makecommerce\Ecommerce\Api\Data\CustomerInterface $customer
     * @return \Makecommerce\Ecommerce\Api\Data\TransactionInterface
     */
    public function create(
        \Makecommerce\Ecommerce\Api\Data\TransactionInterface $transaction,
        \Makecommerce\Ecommerce\Api\Data\CustomerInterface $customer
    );

    /**
     * Calls the MakeCommerce Api to charge the customers card using the token.
     *
     * This method should be called if webapi is used,
     * performs some validations before proceeding to pay()
     *
     * @param string $transactionId
     * @param \Makecommerce\Ecommerce\Api\Data\PaymentTokenInterface $token
     * @return \Makecommerce\Ecommerce\Api\Data\TransactionInterface
     */
    public function beforePay($transactionId, \Makecommerce\Ecommerce\Api\Data\PaymentTokenInterface $token);

    /**
     * Calls the MakeCommerce Api to charge the customers card using the token.
     *
     * @param string $transactionId
     * @param \Makecommerce\Ecommerce\Api\Data\PaymentTokenInterface $token
     * @return \Makecommerce\Ecommerce\Api\Data\TransactionInterface
     */
    public function pay($transactionId, \Makecommerce\Ecommerce\Api\Data\PaymentTokenInterface $token);

    /**
     * Calls the MakeCommerce Api to create a token for payment.
     *
     * @param string $transactionId
     * @param \Makecommerce\Ecommerce\Api\Data\CardDataInterface $cardData
     * @return \Makecommerce\Ecommerce\Api\Data\PaymentTokenInterface
     */
    public function token($transactionId, \Makecommerce\Ecommerce\Api\Data\CardDataInterface $cardData);
}

<?php

namespace Makecommerce\Ecommerce\Api\Data;

/**
 * Interface CardDataInterface
 *
 * @category DataInterface
 * @package  Makecommerce\Ecommerce\Api\Data
 * @author   Maksekeskus AS <info@maksekeskus.ee>
 * @license  OSL-3.0 https://opensource.org/licenses/OSL-3.0
 */
interface CardDataInterface
{
    /**#@+
     * Constants defined for keys of array, makes typos less likely
     */
    const NUMBER           = 'number';
    const NAME             = 'name';
    const CVC              = 'cvc';
    const EXPIRATION_MONTH = 'exp_month';
    const EXPIRATION_YEAR  = 'exp_year';

    /**#@-*/

    /**
     * Get the card number
     *
     * @return string
     */
    public function getNumber();

    /**
     * Get the card holder name
     *
     * @return string
     */
    public function getName();

    /**
     * Get security code
     *
     * @return int
     */
    public function getCvc();

    /**
     * Get the expiration month
     *
     * @return int
     */
    public function getExpMonth();

    /**
     * Get the expiration year
     *
     * @return int
     */
    public function getExpYear();

    /**
     * Set the card number
     *
     * @param string $number the card number visa|maestro|mastercard
     *
     * @return $this
     */
    public function setNumber($number);

    /**
     * Set the card holder name
     *
     * @param string $name card holders name
     *
     * @return $this
     */
    public function setName($name);

    /**
     * Set the card security code
     *
     * @param int $cvc the security code
     *
     * @return $this
     */
    public function setCvc($cvc);

    /**
     * Set the month the card expires on
     *
     * @param int $month range 1-12
     *
     * @return $this
     */
    public function setExpMonth($month);

    /**
     * Set the year the card expires on
     *
     * @param int $year range 0-99
     *
     * @return $this
     */
    public function setExpYear($year);
}

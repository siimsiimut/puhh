<?php

namespace Makecommerce\Ecommerce\Api\Data;

/**
 * Interface CustomerInterface
 *
 * @category DataInterface
 * @package  Makecommerce\Ecommerce\Api\Data
 * @author   Maksekeskus AS <info@maksekeskus.ee>
 * @license  OSL-3.0 https://opensource.org/licenses/OSL-3.0
 */
interface CustomerInterface
{
    /**#@+
     * Constants defined for keys of array, makes typos less likely
     */
    const REMOTE_ADDRESS = 'ip';
    const EMAIL          = 'email';
    const COUNTRY        = 'country';
    const LOCALE         = 'locale';
    /**#@-*/

    /**
     * Get the email of the customer
     *
     * @return string|null
     */
    public function getEmail();

    /**
     * Get country code
     *
     * @return string|null
     */
    public function getCountry();

    /**
     * Get the customer ip address
     *
     * @return string
     */
    public function getIp();

    /**
     * Get the customer locale
     *
     * @return string|null $url
     */
    public function getLocale();

    /**
     * Set the email of the customer
     *
     * @param string|null $email magento customer email
     *
     * @return $this
     */
    public function setEmail($email = null);

    /**
     * Set country code
     *
     * @param string|null $countryCode 2 characters
     *
     * @return $this
     */
    public function setCountry($countryCode = null);

    /**
     * Set the customer ip address
     *
     * @param string $ip ip4 0.0.0.0
     *
     * @return $this
     */
    public function setIp($ip);

    /**
     * Set the customer locale
     *
     * @param string|null $locale 2 characters
     *
     * @return $this
     */
    public function setLocale($locale = null);
}

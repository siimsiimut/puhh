<?php

namespace Makecommerce\Ecommerce\Api\Data;

/**
 * Interface TransactionInterface
 *
 * @category DataInterface
 * @package  Makecommerce\Ecommerce\Api\Data
 * @author   Maksekeskus AS <info@maksekeskus.ee>
 * @license  OSL-3.0 https://opensource.org/licenses/OSL-3.0
 */
interface TransactionInterface
{
    /**#@+
     * Constants defined for keys of array, makes typos less likely
     */
    const ID = 'id';

    const OBJECT_TYPE = 'object';

    const CREATED_AT = 'created_at';

    const STATUS = 'status';

    const AMOUNT = 'amount';

    const CURRENCY = 'currency';

    const REFERENCE = 'reference';

    const MERCHANT_DATA = 'merchant_data';

    const TYPE = 'type';

    const CUSTOMER = 'customer';

    const PAYMENT_METHODS = 'payment_methods';

    const ORDER_IDS = 'order_ids';

    /**#@-*/

    /**
     * Get id
     *
     * @return string
     */
    public function getId();

    /**
     * Get object
     *
     * @return string
     */
    public function getObject();

    /**
     * Get the creation datetime string
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * Get the status of the transaction
     *
     * @return string
     */
    public function getStatus();

    /**
     * Get the amount
     *
     * @return float
     */
    public function getAmount();

    /**
     * Get the currency used for payment
     *
     * @return string
     */
    public function getCurrency();

    /**
     * Get reference
     *
     * @return string|null
     */
    public function getReference();

    /**
     * Get the addition data, such as quote id, the max length is 255
     * Stored as json
     *  {
     *      "quote_id": "70"
     *  }
     *
     * @return string|null
     */
    public function getMerchantData();

    /**
     * Get get the type of the transaction respone, for example customer_return
     *
     * @return string
     */
    public function getType();

    /**
     * Get the customer used for the transaction
     *
     * @return \Makecommerce\Ecommerce\Api\Data\CustomerInterface|null
     */
    public function getCustomer();

    /**
     * Get payment methods used for the transaction
     *
     * @return \Makecommerce\Ecommerce\Api\Data\PaymentMethodsInterface|null
     */
    public function getPaymentMethods();

    /**
     * Get order ids
     *
     * @return int[]
     */
    public function getOrderIds();

    /**
     * Set Id
     *
     * @param string $id
     * @return $this
     */
    public function setId($id);

    /**
     * Set Name
     *
     * @param  string $object
     * @return $this
     */
    public function setObject($object);

    /**
     * Set Reference
     *
     * @param  string|null $reference
     * @return $this
     */
    public function setReference($reference = null);

    /**
     * Set Name
     *
     * @param  string|null $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Set Name
     *
     * @param  string|null $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * Set country code
     *
     * @param  float|null $amount
     * @return $this
     */
    public function setAmount($amount);

    /**
     * Set currency used for the transaction
     *
     * @param  string $currency
     * @return $this
     */
    public function setCurrency($currency);

    /**
     * Set the extra data
     *
     * @param  string $merchantData
     * @return $this
     */
    public function setMerchantData($merchantData);

    /**
     * Set payment method url
     *
     * @param  string $type
     * @return $this
     */
    public function setType($type);

    /**
     * Set payment method url
     *
     * @param  string|null $customer
     * @return $this
     */
    public function setCustomer($customer = null);

    /**
     * Set payment methods
     *
     * @param  \Makecommerce\Ecommerce\Api\Data\PaymentMethodsInterface $paymentMethods
     * @return $this
     */
    public function setPaymentMethods(\Makecommerce\Ecommerce\Api\Data\PaymentMethodsInterface $paymentMethods);

    /**
     * Set order ids
     *
     * @param  int[]|string $orderIds
     * @return $this
     */
    public function setOrderIds($orderIds);
}

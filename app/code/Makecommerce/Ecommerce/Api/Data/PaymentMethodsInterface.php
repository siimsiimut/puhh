<?php

namespace Makecommerce\Ecommerce\Api\Data;

/**
 * Interface PaymentMethodsInterface
 *
 * @category DataInterface
 * @package  Makecommerce\Ecommerce\Api\Data
 * @author   Maksekeskus AS <info@maksekeskus.ee>
 * @license  OSL-3.0 https://opensource.org/licenses/OSL-3.0
 * @link     https://www.aedes.ee
 */
interface PaymentMethodsInterface
{
    /**#@+
     * Constants defined for keys of array, makes typos less likely
     */

    const CARDS = 'cards';

    const BANKLINKS = 'banklinks';

    const CASH = 'cash';

    const OTHER = 'other';
    
    const PAYLATER = 'payLater';

    /**#@-*/

    /**
     * Get the available card payment methods
     *
     * @return \Makecommerce\Ecommerce\Api\Data\PaymentMethodInterface[]|null
     */
    public function getCards();

    /**
     * Get the available banklinks
     *
     * @return \Makecommerce\Ecommerce\Api\Data\PaymentMethodInterface[]|null
     */
    public function getBanklinks();

    /**
     * Get the available cash payment methods
     *
     * @return \Makecommerce\Ecommerce\Api\Data\PaymentMethodInterface[]|null
     */
    public function getCash();

    /**
     * Get other methods
     *
     * @return \Makecommerce\Ecommerce\Api\Data\PaymentMethodInterface[]|null
     */
    public function getOther();

    /**
     * Get the available paylater methods
     *
     * @return \Makecommerce\Ecommerce\Api\Data\PaymentMethodInterface[]|null
     */
    public function getPaylater();

    /**
     * Set the available card payment methods
     *
     * @param  \Makecommerce\Ecommerce\Api\Data\PaymentMethodInterface[]|null $cards
     * @return $this
     */
    public function setCards($cards = null);

    /**
     * Set the available banklink payment methods
     *
     * @param  \Makecommerce\Ecommerce\Api\Data\PaymentMethodInterface[]|null $bankLinks
     * @return $this
     */
    public function setBanklinks($bankLinks = null);

    /**
     * Set the available cash payment methods
     *
     * @param  \Makecommerce\Ecommerce\Api\Data\PaymentMethodInterface[]|null $cash
     * @return $this
     */
    public function setCash($cash = null);

    /**
     * Set the other payment methods
     *
     * @param  \Makecommerce\Ecommerce\Api\Data\PaymentMethodInterface[]|null $other
     * @return $this
     */
    public function setOther($other = null);

    /**
     * Set the paylater payment methods
     *
     * @param  \Makecommerce\Ecommerce\Api\Data\PaymentMethodInterface[]|null $other
     * @return $this
     */
    public function setPaylater($paylater = null);

}

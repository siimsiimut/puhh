<?php

namespace Makecommerce\Ecommerce\Api\Data;

interface ShipmentResponseInterface
{
    /**#@+
     * Constants defined for keys of array, makes typos less likely
     */
    const ORDER_ID = 'order_id';

    const BARCODE = 'barcode';
    
    const SHIPPING_METHOD = 'shipping_method';

    const CARRIER = 'carrier';

    const LABEL_URL = 'label_url';

    const MANIFEST_URL = 'manifest_url';
    /**#@-*/

    /**
     * Get order ID
     *
     * @return int|null
     */
    public function getOrderId();

    /**
     * Set order ID
     *
     * @param int|null $orderId
     * @return $this
     */
    public function setOrderId($orderId);

    /**
     * Get barcode
     *
     * @return string|null
     */
    public function getBarcode();

    /**
     * Set barcode
     *
     * @param string|null $barcode
     * @return $this
     */
    public function setBarcode($barcode);

    /**
     * Get carrier name
     *
     * @return srting|null
     */
    public function getCarrier();

    /**
     * Set carrier name
     *
     * @param string|null $carrier
     * @return $this
     */
    public function setCarrier($carrier);

    /**
     * Get shipping method
     *
     * @return string|null
     */
    public function getShippingMethod();

    /**
     * Set shipping method
     *
     * @param string|null $shippingMethod
     * @return $this
     */
    public function setShippingMethod($shippingMethod);

    /**
     * Get label URL
     *
     * @return string|null
     */
    public function getLabelUrl();

    /**
     * Set label URL
     *
     * @param string|null $labelUrl
     * @return $this
     */
    public function setLabelUrl($labelUrl);

    /**
     * Get manifest URL
     *
     * @return string|null
     */
    public function getManifestUrl();

    /**
     * Set manifest URL
     *
     * @param string|null $manifestUrl
     * @return $this
     */
    public function setManifestUrl($manifestUrl);
}

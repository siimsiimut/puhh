<?php

namespace Makecommerce\Ecommerce\Api\Data;

/**
 * Interface PaymentMethodInterface
 *
 * @category DataInterface
 * @package  Makecommerce\Ecommerce\Api\Data
 * @author   Maksekeskus AS <info@maksekeskus.ee>
 * @license  OSL-3.0 https://opensource.org/licenses/OSL-3.0
 */
interface PaymentMethodInterface
{
    /**#@+
     * Constants defined for keys of array, makes typos less likely
     */

    const NAME = 'name';

    const COUNTRY = 'country';

    const URL = 'url';

    const MIN_AMOUNT = 'min_amount';

    const MAX_AMOUNT = 'max_amount';

    /**#@-*/

    /**
     * Get name
     *
     * @return string
     */
    public function getName();

    /**
     * Get country code
     *
     * @return string|null
     */
    public function getCountry();

    /**
     * Get payment method url
     *
     * @return string|null
     */
    public function getUrl();

    /**
     * Get min amount for payment method
     *
     * @return string|null
     */
    public function getMinAmount();

    /**
     * Get max amount for payment method
     *
     * @return string|null
     */
    public function getMaxAmount();

    /**
     * Set Name
     *
     * @param  string $name
     * @return \Makecommerce\Ecommerce\Api\Data\MethodInterface
     */
    public function setName($name);

    /**
     * Set country code
     *
     * @param  string|null $countryCode
     * @return \Makecommerce\Ecommerce\Api\Data\MethodInterface
     */
    public function setCountry($countryCode = null);

    /**
     * Set payment method url
     *
     * @param  string|null $url
     * @return \Makecommerce\Ecommerce\Api\Data\MethodInterface
     */
    public function setUrl($url = null);

    /**
     * Set min amount for payment method
     *
     * @return string|null
     */
    public function setMinAmount($min_amount = 0);

    /**
     * Set max amount for payment method
     *
     * @return string|null
     */
    public function setMaxAmount($max_amount = 0);
}

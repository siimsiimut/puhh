<?php

namespace Makecommerce\Ecommerce\Api\Data;

/**
 * Interface PaymentResponseInterface
 *
 * @category DataInterface
 * @package  Makecommerce\Ecommerce\Api\Data
 * @author   Maksekeskus AS <info@maksekeskus.ee>
 * @license  OSL-3.0 https://opensource.org/licenses/OSL-3.0
 * @link     https://www.aedes.ee
 */
interface PaymentResponseInterface
{
    /**#@+
     * Constants defined for keys of array, makes typos less likely
     */
    const TYPE = 'type';

    const MESSAGE = 'message';

    const CODE = 'code';
    /**#@-*/

    /**
     * Get the type of response success/error
     *
     * @return string|null
     */
    public function getType();

    /**
     * Get the message of the response
     *
     * @return string|null
     */
    public function getMessage();

    /**
     * Get the code of the response
     *
     * @return int|null
     */
    public function getCode();

    /**
     *  Set the type of response success/error
     *
     * @param  string|null $type
     * @return \Makecommerce\Ecommerce\Api\Data\PaymentResponseInterface
     */
    public function setType($type = null);

    /**
     * Set the message of the response
     *
     * @param  string|null $message
     * @return \Makecommerce\Ecommerce\Api\Data\PaymentResponseInterface
     */
    public function setMessage($message = null);

    /**
     * Set the code of the response
     *
     * @param  int|null $code
     * @return \Makecommerce\Ecommerce\Api\Data\PaymentResponseInterface
     */
    public function setCode($code = null);
}

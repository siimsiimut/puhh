<?php

namespace Makecommerce\Ecommerce\Api\Data;

/**
 * Interface PaymentTokenInterface
 *
 * @category DataInterface
 * @package  Makecommerce\Ecommerce\Api\Data
 * @author   Maksekeskus AS <info@maksekeskus.ee>
 * @license  OSL-3.0 https://opensource.org/licenses/OSL-3.0
 */
interface PaymentTokenInterface
{
    /**#@+
     * Constants defined for keys of array, makes typos less likely
     */
    const JSON = 'json';

    const MAC = 'mac';

    const PAYMENT_TOKEN = 'paymentToken';

    const PAYMENT_TOKEN_MULTIUSE = 'paymentTokenMultiuse';

    const PAYMENT_TOKEN_VALID_UNTIL = 'paymentTokenValidUntil';

    /**#@-*/

    // @codingStandardsIgnoreStart
    /**
     * Get json data about the transaction
     *
     * @return mixed
     */
    public function getJson();
    // @codingStandardsIgnoreEnd

    /**
     * Get mac
     *
     * @return string
     */
    public function getMac();

    /**
     * Get the email of the customer
     *
     * @return string
     */
    public function getPaymentToken();

    /**
     * Get multiuse flag
     *
     * @return boolean
     */
    public function getPaymentTokenMultiuse();

    /**
     * Get token valid to date
     *
     * @return string
     */
    public function getPaymentTokenValidUntil();

    /**
     * Set the json transaction data
     *
     * @param string $json
     * @return \Makecommerce\Ecommerce\Api\Data\PaymentTokenInterface
     */
    public function setJson($json);

    /**
     * Set the mac
     *
     * @param string $mac
     * @return \Makecommerce\Ecommerce\Api\Data\PaymentTokenInterface
     */
    public function setMac($mac);

    /**
     * Set the email of the customer
     *
     * @param  string $token
     * @return \Makecommerce\Ecommerce\Api\Data\PaymentTokenInterface
     */
    public function setPaymentToken($token);

    /**
     * Set multiuse flag
     *
     * @param  boolean $multiuse
     * @return \Makecommerce\Ecommerce\Api\Data\PaymentTokenInterface
     */
    public function setPaymentTokenMultiuse($multiuse);

    /**
     * Set token valid to date
     *
     * @param  string $validUntil
     * @return \Makecommerce\Ecommerce\Api\Data\PaymentTokenInterface
     */
    public function setPaymentTokenValidUntil($validUntil);
}

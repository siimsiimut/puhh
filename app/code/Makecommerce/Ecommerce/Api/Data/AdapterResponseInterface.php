<?php
/**
 * Interface AdapterResponseInterface
 *
 * Copyright © 2016 Aedes Web Solutions. All rights reserved.
 *
 * PHP version 5
 *
 * @category DataInterface
 * @package  Makecommerce\Ecommerce\Api\Data
 * @author   Olavi Sau <olavi@aedes.ee>
 * @license  OSL-3.0 https://opensource.org/licenses/OSL-3.0
 * @link     https://www.aedes.ee
 */
namespace Makecommerce\Ecommerce\Api\Data;

/**
 * Interface AdapterResponseInterface
 *
 * @category DataInterface
 * @package  Makecommerce\Ecommerce\Api\Data
 * @author   Olavi Sau <olavi@aedes.ee>
 * @license  OSL-3.0 https://opensource.org/licenses/OSL-3.0
 * @link     https://www.aedes.ee
 */
interface AdapterResponseInterface
{
    /**#@+
     * Constants defined for keys of array, makes typos less likely
     */
    const INCREMENT_ID = 'increment_id';

    const TRANSACTION = 'transaction';

    /**#@-*/

    /**
     * Get the order increment id
     *
     * @return string
     */
    public function getIncrementId();

    /**
     * Get the transaction
     *
     * @return \Makecommerce\Ecommerce\Api\Data\TransactionInterface
     */
    public function getTransaction();

    /**
     * Set the order increment id
     *
     * @param string $incrementId Order increment id
     *
     * @return $this
     */
    public function setIncrementId($incrementId);

    /**
     *  Set the transaction
     *
     * @param \Makecommerce\Ecommerce\Api\Data\TransactionInterface $transaction MakeCommerce Transaction
     *
     * @return $this
     */
    public function setTransaction(\Makecommerce\Ecommerce\Api\Data\TransactionInterface $transaction);
}

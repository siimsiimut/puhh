<?php

namespace Makecommerce\Ecommerce\Api\Data;

interface LocationInterface
{
    /**#@+
     * Constants defined for keys of array, makes typos less likely
     */
    const ID = 'id';
    const TYPE = 'type';
    const NAME = 'name';
    const CITY = 'city';
    const COUNTRY = 'country';
    const AVAILABILITY = 'availability';
    const COMMENT = 'comment';
    const PROVIDER = 'provider';
    const CARRIER = 'carrier';
    const ADDRESS = 'address';
    const ZIP = 'zip';
    /**#@-*/

    /**
     * Get unique location identifier
     *
     * @return string|null
     */
    public function getId();

    /**
     * Set unique location identifier
     *
     * @param string|null $zip
     * @return $this
     */
    public function setId($id);

    /**
     * Get location type.
     *
     * @return string|null
     */
    public function getType();

    /**
     * Set location type
     *
     * @param string|null $type
     * @return $this
     */
    public function setType($type);

    /**
     * Get location name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Set location name
     *
     * @param string|null $name
     * @return $this
     */
    public function setName($name);

    /**
     * Get city
     *
     * @return string|null
     */
    public function getCity();

    /**
     * Set city
     *
     * @param string|null $city
     * @return $this
     */
    public function setCity($city);

    /**
     * Get country
     *
     * @return string|null
     */
    public function getCountry();

    /**
     * Set country
     *
     * @param string|null $country
     * @return $this
     */
    public function setCountry($country);

    /**
     * Get availability
     *
     * @return string|null
     */
    public function getAvailability();

    /**
     * Set availability
     *
     * @param string|null $availability
     * @return $this
     */
    public function setAvailability($availability);

    /**
     * Get comment
     *
     * @return string|null
     */
    public function getComment();

    /**
     * Set comment
     *
     * @param string|null $comment
     * @return $this
     */
    public function setComment($comment);

    /**
     * Get provider
     *
     * @return string|null
     */
    public function getProvider();

    /**
     * Set provider
     *
     * @param string|null $provider
     * @return $this
     */
    public function setProvider($provider);

    /**
     * Get carrier
     *
     * @return string|null
     */
    public function getCarrier();

    /**
     * Set carrier
     *
     * @param string|null $carrier
     * @return $this
     */
    public function setCarrier($carrier);

    /**
     * Get address
     *
     * @return string|null
     */
    public function getAddress();

    /**
     * Set address
     *
     * @param string|null $address
     * @return $this
     */
    public function setAddress($address);

    /**
     * Get zip
     *
     * @return string|null
     */
    public function getZip();

    /**
     * Set zip
     *
     * @param string|null $zip
     * @return $this
     */
    public function setZip($zip);
}

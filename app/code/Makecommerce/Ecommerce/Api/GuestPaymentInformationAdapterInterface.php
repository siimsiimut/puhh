<?php

namespace Makecommerce\Ecommerce\Api;

/**
 * Interface for managing guest payment information
 * @api
 */
interface GuestPaymentInformationAdapterInterface
{
    /**
     * Create the transaction and place the order.
     * AdapterResponseInterface contains both the increment id and transaction.
     *
     * @param string $cartId
     * @param string $email
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @param string $country
     * @param \Magento\Quote\Api\Data\AddressInterface|null $billingAddress
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     * @return \Makecommerce\Ecommerce\Api\Data\AdapterResponseInterface
     */
    public function adaptPlaceOrder(
        $cartId,
        $email,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        $country,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    );
}

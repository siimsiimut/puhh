require(
    [
        'Magento_Ui/js/modal/confirm',
        'Magento_Ui/js/modal/alert'
    ],
    function (
        confirmModal,
        alertModal
    ) {
    

        executeWithConfirmation = function (button) {
            if (isValidForRefund(jQuery('.order-payment-method-title tbody  tr:nth-child(2) td').html())) {
                var total = jQuery('.order-subtotal-table tbody .price').text();
                var currency = total.replace(/[\d,]/g, '');
                var adjustmentRefund = (!jQuery('#adjustment_positive').val()) ? "0" : jQuery('#adjustment_positive').val();
                var adjustmentFee = (!jQuery('#adjustment_negative').val()) ? "0" : jQuery('#adjustment_negative').val();
                var shippingRefund = (!jQuery('#shipping_amount').val()) ? "0" : jQuery('#shipping_amount').val();


                if (adjustmentRefund.match("([0-9])(.?|,?)([0-9])*") && adjustmentFee.match("([0-9])(.?|,?)([0-9])*") && shippingRefund.match("([0-9])(.?|,?)([0-9])*")) {
                    var totalSum = parseFloat(total.split('.')[0].replace(/[^\d,]/g, '')) + parseFloat(shippingRefund) + parseFloat(adjustmentRefund) - parseFloat(adjustmentFee);
                    confirmModal({
                        title: 'Confirmation',
                        content: 'The sum to be returned is ' + totalSum + currency + ' Are you sure you want to continue?',
                        actions: {
                            confirm: function () {
                                if (button.attr("data-ui-id").search('offline') != -1) {
                                    submitCreditMemoOffline();
                                } else {
                                    submitCreditMemo();
                                }
                            },
                            cancel: function () {
                                enableElements('submit-button');
                            }
                        }
                    });
                } else {
                    alertModal({
                        title: 'Wrong values inserted',
                        content: 'Please check inserted values and try again.'
                    });
                    enableElements('submit-button');
                }
            } else {
                alertModal({
                    title: 'Refund error',
                    content: 'Refund was attempted too soon after order placement. Please wait and try again.'
                });
                enableElements('submit-button');
            }
        };

        isValidForRefund = function (dateString) {
            dateString = dateString.trim();
            var orderDate = Date.UTC.apply(Date, formatString(dateString));
            var currentDate = Date.now();
            return (currentDate - orderDate) > 300000;
        };

        formatString = function (dateString) {
            var splitString = dateString.split(' ');
            var splitDate = splitString[0].split('-');
            var time = splitString[1].split(':');
            splitDate = splitDate.concat(time);
            splitDate[1] = parseInt(splitDate[1], 10) - 1;
            return splitDate;
        };

        getMethodTitle = function () {
            var method = jQuery('.order-payment-method-title table tbody tr:nth-child(1) td').html();
            if (typeof(method) === 'undefined') {
                method = jQuery('.order-payment-method-title').html();
            }
            return method.trim();
        };

        jQuery(document).ready(function () {
            if (getMethodTitle() == 'Makecommerce') {
                var button = jQuery('.creditmemo-totals .actions button');
                button.removeAttr("onclick");
                button.click(function () {
                    executeWithConfirmation(jQuery(this));
                });
            }
        });
    }
);
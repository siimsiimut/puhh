var config = {
    "config": {
        "mixins": {
            "Magento_Checkout/js/model/shipping-save-processor/default": {
                "Makecommerce_Ecommerce/js/model/shipping-save-processor/default-mixin": true
            },
            "Magento_Checkout/js/view/shipping": {
                "Makecommerce_Ecommerce/js/view/shipping-mixin": true
            }
        }
    }
};
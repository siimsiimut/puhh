/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
 
define(
    [
        'ko',
        'Makecommerce_Ecommerce/js/model/country-manager'
    ],
    function (ko, CountryManager) {
        'use strict';
        var useLogo = window.checkoutConfig.payment.makecommerce.ui.inlineUselogo;
        CountryManager.initialize(window.checkoutConfig.payment.makecommerce);
        function Bank( name, imagePath, methodType, country, disabled)
        {
            var bank = this;
            this.name = name;
            this.imagePath = imagePath;
            this.methodType = methodType;
            this.country = country;
            this.disabled = ko.observable(disabled);

            this.setCountry = function (country) {
                this.country = country;
                return this;
            };
            this.getMinAmount = function () {
            
            };
            this.getTitle  = function () {
                var titles = window.checkoutConfig.payment.makecommerce.titles;
                var countryTitle = CountryManager.getCountryTitle(bank.country);
                if (useLogo === 'logo') {
                    return (countryTitle ? '<span class="makecommerce-title">'+ countryTitle  +'</span>' : '')+'<img class="makecommerce-logo" src="'+ bank.imagePath +'">';
                }
                if (useLogo === 'text' ) {
                    return '<span class="makecommerce-title">'+(titles[bank.name] ? titles[bank.name] : bank.name )+ (countryTitle ? '('+ countryTitle+ ')': '')  +'</span>';
                }
                if (useLogo === 'text&logo') {
                    return '<span class="makecommerce-title logo-inline">'+(titles[bank.name] ? titles[bank.name] : bank.name )+ (countryTitle ? '('+ countryTitle+ ')': '')+ '</span>'+'<img class="makecommerce-logo" src="'+ bank.imagePath +'">';
                }
            };
            this.getId = function () {
                return this.name + '_' + this.country + '_bank';
            }
        }
 
 
        Bank.prototype.fromData = function (data, selectedMethod, amount) {
            var data1 = [];
            var remove = 0;
            for( var i = 0; i < data.length; i++ ) { 
                if (typeof data[i].min_amount === 'undefined') data[i].min_amount = 0; 
                if (typeof data[i].max_amount === 'undefined') data[i].max_amount = 0; 
                if ( data[i].min_amount > 0 && data[i].min_amount > amount ) {
                    remove = 1;
                }
                if ( data[i].max_amount > 0 && data[i].max_amount < amount ) {
                    remove = 1;
                }
                if (remove == 0)
                    data1.push(data[i]);
                remove = 0;
            }
            if (Array.isArray(data1)) {
                return data1
                .map(function (bank, idx) {
                    return new Bank(bank.name, bank.imagePath, bank.methodType, bank.country);
                });
            }
        };
        Bank.prototype.newFromInstance = function (instance) {
            return new Bank(instance.name, instance.imagePath, instance.methodType, instance.country, disabled);
        };
        return Bank;
    }
);
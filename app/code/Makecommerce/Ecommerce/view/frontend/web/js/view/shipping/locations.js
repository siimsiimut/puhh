/*browser:true*/
/*global define*/
define(
    [
        'ko',
        'jquery',
        'uiComponent',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/view/shipping',
        'mage/translate'
    ],
    function (
        ko,
        $,
        Component,
        quote,
        shipping,
        $t
    ) {
        'use strict';
        var shippingConfig = window.checkoutConfig.shipping;
        var dropdownSelector = '#mc-shipping-data-row';
        var shippingSelector = 'li#shipping';
        var selectedLocation = ko.observable(null);
        var isDropdownVisible = ko.observable(false);
        var locations = ko.observableArray();
        var methodsWithDropdown = collectMethodsWithDropdown();
        var previousCarrier = quote.shippingMethod() ? quote.shippingMethod().carrier_code : null;

        window.quote = quote;

        quote.shippingMethod.subscribe(function(method) {
            if (!method) {
                moveDropdownTo(null);
            } else {
                if (shippingMethodHasDropdown(method)) {
                    moveDropdownTo(method);
                } else {
                    moveDropdownTo(null);
                }
            }
        });

        quote.shippingAddress.subscribe(function(){
            //for when address is changed and rates recalculated, select is moved , otherwise it would be deleted 
            $(dropdownSelector).css('display', 'none');
            $(dropdownSelector).insertAfter($(shippingSelector));
            selectedLocation(selectedLocation());
        });

        selectedLocation.subscribe(function(location) {
            if (!location) {
                delete quote.shippingAddress()['custom_attributes'];
            } else {
                quote.shippingAddress().custom_attributes = {
                    'location_id': location.location_id
                };
            }
        });

        function isShippingMethodSame() {
            var result = false;
            if (quote.shippingAddress().custom_attributes) {
                var method = quote.shippingMethod();
                var methodLocations = shippingConfig[method.carrier_code + '_' + method.method_code].locations;
                var currentLocationId = quote.shippingAddress().custom_attributes.location_id;
                $.each(methodLocations, function (idx, group) {
                    if (result) {
                        return false;
                    }
                    $.each(group.locations, function (i, location) {
                        if (location.location_id == currentLocationId) {
                            result = true;
                            return false;
                        }
                    });
                });
            }
            return result;
        }

        function collectMethodsWithDropdown() {
            var methods = [];
            $.each(shippingConfig, function(shippingMethodName, data) {
                if (data.locations.length > 0) {
                    methods.push(shippingMethodName);
                }
            });
            return methods;
        }

        function shippingMethodHasDropdown(shippingMethod) {
            if (!shippingMethod) {
                return false;
            }
            return $.inArray(shippingMethod.carrier_code + '_' + shippingMethod.method_code, methodsWithDropdown) > -1;
        }

        function moveDropdownTo(shippingMethod) {
            var dropdown = $(dropdownSelector);
            if (!shippingMethod) {
                isDropdownVisible(false);
                dropdown.insertAfter($(shippingSelector));
            } else {
                var shippingMethodSelector = '#label_method_' + shippingMethod.method_code + '_' + shippingMethod.carrier_code;
                var shippingMethodRow = $(shippingMethodSelector).parent();

                if (shippingMethodRow.length > 0) {
                    if (dropdown.find('select').prop('disabled')) {
                        dropdown.find('select').prop('disabled', '');
                    }
                    dropdown.insertAfter(shippingMethodRow);
                    isDropdownVisible(true);
                }
            }
        }

        /** Add view logic here if needed */
        var self = Component.extend({
            defaults: {
                template: 'Makecommerce_Ecommerce/dropdown'
            },

            getLocations: function(){
                var quoteCarrier = quote.shippingMethod() ? quote.shippingMethod().carrier_code + '_' + quote.shippingMethod().method_code : null;
                if (quoteCarrier !== previousCarrier) {
                    locations.removeAll();
                    if (shippingMethodHasDropdown(quote.shippingMethod())) {
                        this.getLocationsByCountry();
                        previousCarrier = quoteCarrier;
                    } else {
                        selectedLocation(null);
                        previousCarrier = null;
                    }
                }
                return locations;
            },

            getLocationsByCountry: function(){
                var allLocations = shippingConfig[quote.shippingMethod().carrier_code + '_' + quote.shippingMethod().method_code].locations;
                var country = quote.shippingAddress().countryId;

                $.each(allLocations, function(idx, obj) {
                    if (obj.groupCountry === country) {
                        locations.push(obj);
                    }
                });
                selectedLocation(locations()[0].locations[0]);
            },

            selectedLocation: selectedLocation,
            isDropdownVisible: isDropdownVisible
        });

        return self;
    }
);
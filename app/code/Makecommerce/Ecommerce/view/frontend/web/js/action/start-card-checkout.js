
define(
    [
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/model/error-processor',
        'jquery',
        'Magento_Checkout/js/model/url-builder',
        'mage/storage'
    ],
    function (quote, fullScreenLoader, errorProcessor, $, urlBuilder, storage) {
        'use strict';
        var makecommerce = window.checkoutConfig.payment.makecommerce;
        var quoteData = window.checkoutConfig.quoteData;

        var billingAddress = quote.billingAddress();
        var referenceText = makecommerce.ui.referenceText;
            referenceText = referenceText ? referenceText : '';
        var transactionData;
        function init(data, orderId)
        {
            Maksekeskus.Checkout.initialize(data);
        }
        //From bind @makecommerce-method
        window.makecommerceCardCancelled = function () {
            fullScreenLoader.stopLoader();
            this.modalOpen(false);
        };

        window.makecommerceCardCompleted = function (data) {
            fullScreenLoader.startLoader();
            var payUrl = urlBuilder.createUrl('/makecommerce/pay/:transaction-id', { 'transaction-id': transactionData.id});
            storage.post(payUrl, JSON.stringify({token:data}))
            .done(
                function (data) {
                    $.mage.redirect(makecommerce.baseUrl + "checkout/onepage/success");
                }
            ).fail(
                function (response) {
                    errorProcessor.process(response, self.messageContainer);
                    fullScreenLoader.stopLoader();
                }
            );
        };

        return function (transaction, orderId) {
            var billingAddress = quote.billingAddress();
            var firstName = billingAddress.firstname;
            var lastName = billingAddress.lastname;
            var clientName = '';
            if (parseInt(makecommerce.ui.cardPrefill) === 1) {
                clientName = (firstName == null || lastName == null) ? '' : (firstName + ' ' + lastName);
            }
            var data = {
                'amount': parseFloat(quote.getTotals()().base_grand_total).toFixed(2),
                'currency': quoteData.quote_currency_code,
                'email': quoteData.customer_email,
                'clientName': clientName,
                'transaction': transaction.id,
                'key': makecommerce.publicKey,
                'locale': makecommerce.locale,
                'name': makecommerce.ui.cardShopName,
                'cancelled': 'makecommerceCardCancelled',
                'noConflict': false,
                'completed': 'makecommerceCardCompleted',
                'description': referenceText.replace('%', orderId)
            };
            init(data, orderId);
            transactionData = transaction;

            Maksekeskus.Checkout.open(data);
        };
    }
);

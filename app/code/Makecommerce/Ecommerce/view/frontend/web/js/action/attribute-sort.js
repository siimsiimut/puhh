
/*global define*/
define(
    [
    ],
    function () {
        "use strict";
        return function (methodOne, methodTwo) {
            if (!Array.isArray(this.sortOrder)  || !(typeof this.attribute === 'string') ) {
                console.error('attibute-sort is used by binding the sortOrder to the function');
                console.error('example: [{"attribute":"value2"}, {"attribute":"value"}].sort(attributeSort.bind({sortOrder: ["value"], attribute: "test" }))');
            }

            var sortOrder = this.sortOrder;
            var attribute = this.attribute;
            if (sortOrder.indexOf(methodOne[attribute]) === sortOrder.indexOf(methodTwo[attribute])) {
                return 0;
            }
            if (sortOrder.indexOf(methodOne[attribute]) === -1) {
                return 1;
            }
            if (sortOrder.indexOf(methodTwo[attribute]) === -1) {
                return -1;
            }
            if (sortOrder.indexOf(methodOne[attribute]) < sortOrder.indexOf(methodTwo[attribute])) {
                return -1;
            }
            return 1;
        };
    }
);
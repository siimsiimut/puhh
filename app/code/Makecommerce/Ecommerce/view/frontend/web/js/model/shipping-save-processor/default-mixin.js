/*global define,alert*/
define(
    [
        'ko',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/resource-url-manager',
        'mage/storage',
        'Magento_Checkout/js/model/payment-service',
        'Magento_Checkout/js/model/payment/method-converter',
        'Magento_Checkout/js/model/error-processor',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/action/select-billing-address'
    ],
    function (
        ko,
        quote,
        resourceUrlManager,
        storage,
        paymentService,
        methodConverter,
        errorProcessor,
        fullScreenLoader,
        selectBillingAddressAction
    ) {
        'use strict';

        return function (defaultSaveProcessor) {

            defaultSaveProcessor.saveShippingInformation = function () {
                var payload;

                if (!quote.billingAddress()) {
                    selectBillingAddressAction(quote.shippingAddress());
                }
                var billingAddress = quote.billingAddress();
                if (billingAddress.custom_attributes) {
                    if (!billingAddress.custom_attributes.location_id) {
                        delete(billingAddress['custom_attributes']);
                    }
                }

                var shippingMethod = quote.shippingMethod();
                var shippingMethodCode = shippingMethod ? shippingMethod.method_code : null;
                var shippingCarrierCode = shippingMethod ? shippingMethod.carrier_code : null;
                var locationId = (quote.shippingAddress().custom_attributes) ? quote.shippingAddress().custom_attributes.location_id : null;

                payload = {
                    addressInformation: {
                        shipping_address: quote.shippingAddress(),
                        billing_address: quote.billingAddress(),
                        shipping_method_code: shippingMethodCode,
                        shipping_carrier_code: shippingCarrierCode,
                        extension_attributes: {
                            location_id: locationId
                        }
                    }
                };

                fullScreenLoader.startLoader();
                return storage.post(
                    resourceUrlManager.getUrlForSetShippingInformation(quote),
                    JSON.stringify(payload)
                ).done(
                    function (response) {
                        quote.setTotals(response.totals);
                        paymentService.setPaymentMethods(methodConverter(response.payment_methods));
                        fullScreenLoader.stopLoader();
                    }
                ).fail(
                    function (response) {
                        errorProcessor.process(response);
                        fullScreenLoader.stopLoader();
                    }
                );
            };
            
            return defaultSaveProcessor;
        };
    }
);
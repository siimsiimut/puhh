/**
 * Makecommerce Magento JS component
 *
 * @category    Makecommerce
 * @package     Makecommerce_Ecommerce
 * @author      Maksekeskus AS <info@maksekeskus.ee>
 * @copyright   Makecommerce
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/*browser:true*/
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'makecommerce',
                component: 'Makecommerce_Ecommerce/js/view/payment/method-renderer/makecommerce-method'
            }
        );
        /** Add view logic here if needed */
        return Component.extend({});
    }
);

define(
    [
        'ko',
        'Makecommerce_Ecommerce/js/action/parse-sort-order',
        'Makecommerce_Ecommerce/js/action/attribute-sort',
        'Makecommerce_Ecommerce/js/model/country',
        'Makecommerce_Ecommerce/js/model/ui-manager',
        'Magento_Checkout/js/model/quote',
        'underscore'
    ],
    function (
        ko,
        parseSortOrder,
        attributeSort,
        Country,
        UIManager,
        quote
    ) {
        'use strict';
        var countryCollection;
        var selectedCountry = ko.observable(null);
        var makecommerce = window.checkoutConfig.payment.makecommerce;
        var instance;

        var countrySortOrder = parseSortOrder(window.checkoutConfig.payment.makecommerce.ui.countrySortOrder);
        var countrySort = attributeSort.bind({'sortOrder': countrySortOrder, 'attribute': 'name'});
        function CountryManager()
        {
            if (!instance) {
                instance = this;
                return this;
            }
            return instance;
        }
        CountryManager.getCountryTitle = function (countryName) {
            if (CountryManager.getCollection() && CountryManager.getCollection().length > 1) {
                var country = CountryManager.findByData({name: countryName}).pop();
                if (!country) {
                    return ''; }
                return country.title;
            }
            return '';
        };
        CountryManager.setCollection = function (collection) {
            countryCollection = collection;
        };
        CountryManager.getCollection = function () {
            return countryCollection;
        };
        CountryManager.selectCountry = function (country) {
            return function () {
                selectedCountry(country);
                return true;
            }
        };
        CountryManager.getSelectedCountry = function () {
            return selectedCountry();
        };
        CountryManager.getObserver = function () {
            return selectedCountry;
        };
        CountryManager.findByData = function (countryData) {
            var keys = Object.keys(countryData);
            return CountryManager.getCollection().filter(function (country) {
                return (
                    _.isEqual(
                        keys,
                        keys.filter(
                            function (key) {
                                return countryData[key] === country[key];
                            }
                        )
                    )
                );
            });
        };
        CountryManager.isCountrySelected = function (country) {
            return selectedCountry() === country;
        };
        CountryManager.sortCollection = function () {
            if (!Array.isArray(countryCollection)) {
                throw "The countryCollection is invalid";
            }
            countryCollection = countryCollection.sort(countrySort);
        };
        CountryManager.initialize = function (config) {
            CountryManager.setCollection(Country.prototype.fromData(config.countries));
            CountryManager.sortCollection();
            CountryManager.selectDefaultCountry(countryCollection);
        };
        CountryManager.selectDefaultCountry = function () {
            this.selectCountry(this.getCollection()[0])();

            var flag = false;
            this.getCollection().some(function (country) {
                if (country.name.toUpperCase() === quote.shippingAddress().countryId) {
                    CountryManager.selectCountry(country)();
                    return (flag = true);
                }
            });
            if (!flag) {
                this.getCollection().some(function (country) {
                    if (country.name.toUpperCase() === makecommerce.localeCountry) {
                        CountryManager.selectCountry(country)();
                        return true;
                    }
                });
            }

        };
        return CountryManager;
    }
);

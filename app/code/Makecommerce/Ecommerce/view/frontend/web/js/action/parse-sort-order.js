
/*global define*/
define(
    [
    ],
    function () {
        "use strict";
        return function (sortOrder) {
            if (!(typeof sortOrder === 'string')) {
                return [];
            }
            return sortOrder.split(',').filter(Boolean).map(function (element) {
                return element.trim();
            });
        };
    }
);
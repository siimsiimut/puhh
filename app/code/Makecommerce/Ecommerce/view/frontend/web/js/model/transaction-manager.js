
define(
    [
        'Magento_Checkout/js/model/quote',
        'jquery',
        'ko'
    ],
    function (
        quote,
        $,
        ko
    ) {
        'use strict';
        var ChannelManager;
        var instance;
        function TransactionManager()
        {
            if (!instance) {
                instance = this;
                return this;
            }
            return instance;
        }

        TransactionManager.initialize = function (channelManager) {
            ChannelManager = channelManager;
        };
        TransactionManager.transaction = ko.observable(null);
        TransactionManager.createTransaction = function () {
            var makecommerce = window.checkoutConfig.payment.makecommerce;
            var createTransactionUrl = window.checkoutConfig.payment.makecommerce.createTransaction;
            var quoteData = window.checkoutConfig.quoteData;
            return $.when(
                $.ajax({
                    url: makecommerce.baseUrl + createTransactionUrl.replace(/^\//, ''),
                    contentType: 'application/json; charset=utf-8',
                    method: 'POST',
                    data: JSON.stringify({
                        'transaction': {
                            'amount': quote.getTotals()().grand_total,
                            'currency': quoteData.quote_currency_code,
                            'reference': quote.getQuoteId()
                        },
                        'customer': {
                            'email': quote.guestEmail ? quote.guestEmail : quoteData.customer_email,
                            'ip': quoteData.remote_ip,
                            'country': ChannelManager.getSelectedChannel().country,
                            'locale' : makecommerce.locale
                        }
                    }),
                    success: function (data) {
                        TransactionManager.transaction(JSON.parse(data));
                    }
                })
            );
        };

        return TransactionManager;
    }
);

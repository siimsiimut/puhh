/**
 * Makecommerce Magento JS component
 *
 * @category    Makecommerce
 * @package     Makecommerce_Ecommerce
 * @author      Maksekeskus AS <info@maksekeskus.ee>
 * @copyright   Makecommerce
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/*browser:true*/
/*global define*/
define(
    [
        'mage/translate',
        'Magento_Ui/js/modal/alert',
        'Magento_Checkout/js/model/payment-service',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_Checkout/js/model/quote',
        'jquery',
        'ko',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/action/select-payment-method',
        'Makecommerce_Ecommerce/js/action/place-order',
        'Makecommerce_Ecommerce/js/model/channel-manager',
        'Makecommerce_Ecommerce/js/model/ui-manager',
        'Makecommerce_Ecommerce/js/model/country-manager',
        'Makecommerce_Ecommerce/js/action/start-card-checkout',
        'Makecommerce_Ecommerce/js/model/transaction-manager',
        window.checkoutConfig.payment.makecommerce.checkoutJs,
        window.checkoutConfig.payment.makecommerce.tokenizer
    ],
    function (
        $t,
        magentoAlert,
        paymentService,
        Component,
        additionalValidators,
        quote,
        $,
        ko,
        fullScreenLoader,
        selectPaymentMethod,
        placeOrderAction,
        ChannelManager,
        UIManager,
        CountryManager,
        startCardCheckout,
        TransactionManager
    ) {
        'use strict';
        var makecommerce = window.checkoutConfig.payment.makecommerce;
        var quoteData = window.checkoutConfig.quoteData;
        CountryManager.initialize(makecommerce);
        ChannelManager.initialize(makecommerce, CountryManager, quoteData.base_grand_total);
        TransactionManager.initialize(ChannelManager);
        UIManager.initialize(ChannelManager, CountryManager);
        var lastRequest = 0;
        window.makecommerceCardCancelled = window.makecommerceCardCancelled.bind(UIManager);


        var self = Component.extend({
            //this prevents magento core from redirecting to onepage success, which we don't want
            redirectAfterPlaceOrder: false,
            defaults: {
                template: 'Makecommerce_Ecommerce/payment/form/makecommerce-form'
            },
            getCheckoutJs: function () {
                return makecommerce.checkoutJs;
            },
            getTokenizer: function () {
                return makecommerce.tokenizer;
            },
            getChannelManager: function () {
                return ChannelManager;
            },
            getUIManager: function () {
                return UIManager.updateState(this);
            },
            getCountryManager: function () {
                return CountryManager;
            },
            getCode: function () {
                return 'makecommerce';
            },
            getCardCode: function () {
                return 'card_makecommerce';
            },
            getTitle: function () {
                return makecommerce.ui.widgetTitle;
            },
            getCardGroupTitle: function () {
                return makecommerce.ui.widgetGroupTitle;
            },
            validate: function () {
                var billingAddr = quote.billingAddress();

                if (!billingAddr.firstname || !billingAddr.lastname || !billingAddr.city
                    || !billingAddr.countryId || !billingAddr.postcode) {
                    magentoAlert({
                        title: $t('Error'),
                        content: $t('Your session has expired'),
                        actions: {
                            always: function () {
                                var url = window.checkoutConfig.checkoutUrl;
                                window.location.replace(url);
                            }
                        }
                    });
                    return false;
                }
                return true;
            },
            placeOrder: function (data, event) {
                var self = this;
                var placeOrder;
                if (event) {
                    event.preventDefault();
                }
                fullScreenLoader.startLoader();
                if (self.validate() && additionalValidators.validate()) {
                    var country = CountryManager.getSelectedCountry() ? CountryManager.getSelectedCountry().name : 'et';
                    placeOrder = placeOrderAction(self.getData(), self.messageContainer, country);
                    placeOrder.fail(function () {
                        fullScreenLoader.stopLoader();
                    }).done(self.afterPlaceOrder.bind(self));
                }
                fullScreenLoader.stopLoader();

                return true;
            },
            afterPlaceOrder: function (data) {
                fullScreenLoader.stopLoader();

                var orderId = data['increment_id'];
                var transaction = data['transaction'];
                var selectedChannel = ChannelManager.getSelectedChannel();
                var name = selectedChannel.name;
                var type = selectedChannel.methodType;
                var country = selectedChannel.country;

                if (!transaction) {
                    fullScreenLoader.stopLoader();
                    ChannelManager.disableType(type);
                }

                var self = this;
                if (selectedChannel.methodType === 'cards') {
                    UIManager.modalOpen(true);
                    startCardCheckout(transaction, orderId);
                } else if (transaction.payment_methods[type.toLowerCase()]) {
                    $.each(transaction.payment_methods[type.toLowerCase()], function (idx, val) {
                        if (val.name === name && val.country === country) {
                            $.mage.redirect(val.url);
                        }
                    });
                } else {
                    ChannelManager.disableType(type);
                    fullScreenLoader.stopLoader();
                }
            },
            isAvailable: function () {
                return parseInt(makecommerce.isAvailable) === 1 ? true : false;
            }
        });

        //Eventually moving this to phpconfig
        window.checkoutConfig.payment.makecommerce.titles = {
            'seb': 'SEB',
            'swedbank': 'Swedbank',
            'lhv': 'LHV',
            'nordea': 'Nordea',
            'danske': 'Danske',
            'krediidipank': 'Krediidipank',
            'citadele': 'Citadele',
            'pohjola': 'Pohjala',
            'alandsbanken': 'Alandsbanken',
            'handelsbanken': 'Handelsbanken',
            'spankki': 'Spankki',
            'mastercard': 'MasterCard',
            'visa': 'Visa',
            'maestro': 'Maestro',
            'liisi_ee': 'Liisi ID',
            'slice': 'Slice',
            'finora': 'Finora',
            'mtasku': 'mTasku'
        };

        return self;
    }
);

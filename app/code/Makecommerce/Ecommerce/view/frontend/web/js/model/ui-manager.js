
define(
    [
        'ko',
        'Magento_Checkout/js/model/quote',
        'Makecommerce_Ecommerce/js/action/select-default-method'
    ],
    function (ko, quote, selectDefaultMethod) {
        'use strict';
        var instance;
        var component = ko.observable(null);
        var ChannelManager;
        var CountryManager;
        var makecommerce = window.checkoutConfig.payment.makecommerce;
        var activeType = ko.observable(null);

        var setItem = sessionStorage.setItem.bind(sessionStorage);
        var getItem = sessionStorage.getItem.bind(sessionStorage);
        var removeItem = sessionStorage.removeItem.bind(sessionStorage);

        function UIManager()
        {
            if (!instance) {
                instance = this;
                return this;
            }
            return instance;
        }

        UIManager.initialize = function ( channelManager, countryManager ) {
            ChannelManager = channelManager;
            CountryManager = countryManager;
            selectDefaultMethod(component, this, channelManager, countryManager);
        };
        UIManager.updateState = function (state) {
            component(state);
            return this;
        };
        /**
         * These methods are not the same as in ChannelManager, they also change the ui type and select the payment method
         */
        UIManager.isChannelActive = function (channel) {
            return ((activeType() === 'channel') && (ChannelManager.getSelectedChannel() === channel)) && component().isChecked() === 'makecommerce';
        };
        UIManager.selectChannel = function (channel) {
            var self = this;
            return function () {
                if (self.isDivided()) {
                    var country = CountryManager.findByData({name: channel.country}).shift();
                    country = typeof country === 'undefined' ? null : country;
                    CountryManager.selectCountry(country)();
                }
                ChannelManager.selectChannel(channel)();
                component().selectPaymentMethod();
                activeType('channel');
                setItem('makecommerce-channel', JSON.stringify(channel));
                setItem('makecommerce-type', 'channel');
                return true;
            }
        };
        UIManager.isCountryActive = function (country) {
            return (activeType() === 'country' && CountryManager.getSelectedCountry() === country ) && component().isChecked() === 'makecommerce';
        };
        UIManager.selectCountry = function (country) {
            return function () {
                CountryManager.selectCountry(country)();
                component().selectPaymentMethod();
                activeType('country');
                setItem('makecommerce-country', JSON.stringify(country));
                setItem('makecommerce-type', 'country');
                return true;
            }
        };
        UIManager.isCardActive = function () {
            return (activeType() === 'card') && (component().isChecked() === 'makecommerce');
        };
        UIManager.selectCard = function () {
            return function () {
                component().selectPaymentMethod();
                activeType('card');
                setItem('makecommerce-type', 'card');
                return true;
            }
        };
        UIManager.isMonoActive = function () {
            return (activeType() === 'mono') && (component().isChecked() === 'makecommerce');
        };
        /**
         * Mono means the original method, with all banks and all cards
         */
        UIManager.selectMono = function () {
            return function () {
                component().selectPaymentMethod();
                activeType('mono');
                setItem('makecommerce-type', 'mono');
                return true;
            }
        };
        UIManager.isOpenByDefault = function () {
            return makecommerce.ui.openDefault ? parseInt(makecommerce.ui.openDefault) : false;
        };
        UIManager.areBanksGrouped = function () {
            return makecommerce.ui.groupBanklinks ? parseInt(makecommerce.ui.groupBanklinks) : false;
        };
        UIManager.areCardsGrouped = function () {
             return makecommerce.ui.widgetGroupcc ? parseInt(makecommerce.ui.widgetGroupcc) : false;
        };
        UIManager.getLogoSize = function () {
            switch (makecommerce.ui.logoSize) {
                case 's':
                    return 'payment-method-makecommerce-links small';
                case 'm':
                    return 'payment-method-makecommerce-links medium';
                case 'l':
                    return 'payment-method-makecommerce-links large';
                default:
                    return 'payment-method-makecommerce-links medium';
            }
        };
        UIManager.useFlags = function () {
            return makecommerce.ui.widgetCountryselector === 'flag';
        };
        UIManager.isDivided = function () {
            return makecommerce.ui.mode === 'inline';
        };
        UIManager.modalOpen = ko.observable(false);

        UIManager.isOrderPlaceAllowed = function (country) {
            var selectedChannel = ChannelManager.getSelectedChannel();
            if (ChannelManager.isChannelDisabled(selectedChannel)) {
                return false;
            }
            if (
                UIManager.modalOpen() ||
                quote.billingAddress() == null ||
                !selectedChannel) {
                return false;
            }
            if ((activeType() === 'card') && (selectedChannel.methodType === 'banklinks' || selectedChannel.methodType === 'payLater' || selectedChannel.methodType === 'other')) {
                return false;
            }
            if (activeType() === 'mono') {
                if (selectedChannel.methodType !== 'cards') {
                    return CountryManager.getSelectedCountry().name === selectedChannel.country;
                } else if (!UIManager.areCardsGrouped()) {
                    return true;
                } else {
                    return false;
                }
            }
            if (country) {
                if (selectedChannel.methodType !== 'cards') {
                    return UIManager.isCountryActive(country) && (selectedChannel.country === country.name);
                } else if (!UIManager.areCardsGrouped()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        };
        return UIManager;
    }
);

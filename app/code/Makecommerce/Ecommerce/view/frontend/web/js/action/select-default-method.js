
/*global define*/
define(
    [
        'Magento_Checkout/js/model/quote'
    ],
    function (quote) {
        "use strict";
        var setItem = sessionStorage.setItem.bind(sessionStorage);
        var getItem = sessionStorage.getItem.bind(sessionStorage);
        var removeItem = sessionStorage.removeItem.bind(sessionStorage);
        return function (component, UIManager, ChannelManager, CountryManager) {
            function handleOpenByDefault()
            {
                if (UIManager.isOpenByDefault()) {
                    if (!UIManager.isDivided()) {
                        if (!UIManager.areBanksGrouped()) {
                            UIManager.selectMono()();
                        } else {
                            UIManager.selectCountry(CountryManager.getSelectedCountry())();
                        }
                    } else {
                        UIManager.selectChannel(ChannelManager.getCollection()[0])();
                    }
                }
            }
            var selectDefault = component.subscribe(function (state) {
                selectDefault.dispose();
                if (!quote.getPaymentMethod()()) {
                    handleOpenByDefault();
                } else if (quote.getPaymentMethod()().method === 'makecommerce') {
                    if (
                        (['undefined', 'null'].indexOf(getItem('makecommerce-channel')) !== -1) ||
                        (['undefined', 'null'].indexOf(getItem('makecommerce-type')) !== -1) ||
                        (['undefined', 'null'].indexOf(getItem('makecommerce-country')) !== -1) ||
                        (!getItem('makecommerce-type')) ||
                        (getItem('makecommerce-type') === 'mono' && UIManager.isDivided()) ||
                        (getItem('makecommerce-type') === 'channel' && !UIManager.isDivided()) ||
                        (getItem('makecommerce-type') === 'card' && !UIManager.areCardsGrouped())
                    ) {
                        removeItem('makecommerce-channel');
                        removeItem('makecommerce-type');
                        removeItem('makecommerce-country');
                        handleOpenByDefault();
                        return;
                    }
                    switch (getItem('makecommerce-type')) {
                        case 'channel':
                            UIManager.selectChannel(
                                ChannelManager.findByData(
                                    JSON.parse(getItem('makecommerce-channel'))
                                ).pop()
                            )();
                            break;
                        case 'country':
                            UIManager.selectCountry(
                                CountryManager.findByData(
                                    JSON.parse(getItem('makecommerce-country'))
                                ).pop()
                            )();
                            break;
                        case 'card':
                            UIManager.selectCard()();
                            break;
                        case 'mono':
                            UIManager.selectMono()();
                            break;
                        default:
                            UIManager.selectMono()();
                            break;
                    }
                }
            });
        };
    }
);
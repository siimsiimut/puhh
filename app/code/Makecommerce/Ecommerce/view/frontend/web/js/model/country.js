
define(
    [
    ],
    function () {
        'use strict';
        function Country(name, imagePath, title)
        {
            var country = this;
            this.name = name;
            this.imagePath = imagePath;
            this.title = title;
        }

        Country.prototype.fromData = function (data) {
            if (Array.isArray(data)) {
                return data
                .map(function (country) {
                        return new Country(
                            country.name,
                            country.imagePath,
                            country.title
                        );
                });
            }
        };
        return Country;
    }
);

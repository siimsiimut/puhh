/*global define,alert*/
define(
    [
        'Magento_Checkout/js/action/set-shipping-information',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Checkout/js/model/quote',
        'mage/translate'
    ],
    function (
        setShippingInformationAction,
        stepNavigator,
        quote,
        $t
    ) {
        'use strict';

        var mixin = {
            setShippingInformation: function () {
                if (quote.shippingAddress().custom_attributes) {
                    if (quote.shippingAddress().custom_attributes.location_id) {
                        if (quote.shippingAddress().custom_attributes.location_id === '__empty') {
                            alert($t('Please choose a shipping method location!'));
                            return false;
                        }
                    }
                }
                if (this.validateShippingInformation()) {
                    setShippingInformationAction().done(
                        function () {
	                    if (quote.shippingAddress().telephone == '') {
	                        alert($t('Mobile phone number with international prefix must be given! Now is ' + quote.shippingAddress().telephone ));
	                        return false;
	                    } else if (quote.shippingAddress().telephone.substring( 0, 1) === '+') {
		                if ((quote.shippingAddress().countryId.substring( 0, 2) === 'EE') &&  ( quote.shippingAddress().telephone.substring( 0, 5) != '+3725')) {
        	            	    alert($t('Estonian mobile number must begin with +3725! Now is ' + quote.shippingAddress().telephone));
                	    	    return false;
                		}
                		if ((quote.shippingAddress().countryId.substring( 0, 2) === 'LV') &&  ( quote.shippingAddress().telephone.substring( 0, 5) != '+3712')) {
                    		    alert($t('Latvian mobile number must begin with +3712! Now is ' + quote.shippingAddress().telephone));
                    		    return false;
                		}
                		if ((quote.shippingAddress().countryId.substring( 0, 2) === 'LT') &&  ( quote.shippingAddress().telephone.substring( 0, 5) != '+3706')) {
                    		    alert($t('Lithuanian mobile number must begin with +3706! Now is ' + quote.shippingAddress().telephone));
                    		    return false;
                		}
            		    } else {
                		alert($t('Phone number must begin with international prefix! Now is ' + quote.shippingAddress().telephone));
                		return false;
            		    }
                            stepNavigator.next();
                        }
                    );
                }
            }
        }

        return function (shipping) {
            return shipping.extend(mixin);
        };
    }
);
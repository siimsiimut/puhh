
define(
    [
        'jquery',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/url-builder',
        'mage/storage',
        'mage/url',
        'Magento_Checkout/js/model/error-processor',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/full-screen-loader'
        //M2.1+ only 'Magento_Checkout/js/model/place-order'
    ],
    function ($, quote, urlBuilder, storage, url, errorProcessor, customer, fullScreenLoader /* //M2.1+ only, placeOrderService*/) {
        'use strict';

        return function (paymentData, messageContainer, country) {
            var serviceUrl, payload;
            var quoteData = window.checkoutConfig.quoteData;

            // fix for M2.0
            var agreementForm = $('.payment-method._active form[data-role=checkout-agreements]'),
                agreementData = agreementForm.serializeArray(),
                agreementIds = [];

            if (agreementData.length == 0) {
                agreementForm = $('.payment-method._active div[data-role=checkout-agreements] input');
                agreementData = agreementForm.serializeArray();
            }

            agreementData.forEach(function(item) {
                agreementIds.push(item.value);
            });
            // ========

            paymentData.extension_attributes = {agreement_ids: agreementIds};
            payload = {
                cartId: quote.getQuoteId(),
                paymentMethod: paymentData,
                billingAddress: quote.billingAddress(),
                country: country
            };
            /** Checkout for guest and registered customer. */
            if (!customer.isLoggedIn()) {
                serviceUrl = urlBuilder.createUrl('/makecommerce/payment-adapter/:quoteId', {
                    quoteId: quote.getQuoteId()
                });
                payload.email = quote.guestEmail ? quote.guestEmail : window.checkoutConfig.quoteData.customer_email;
            } else {
                serviceUrl = urlBuilder.createUrl('/makecommerce/payment-adapter/mine', {});
            }

            fullScreenLoader.startLoader();

            // keep compatible with 2.*
            return storage.post(
                serviceUrl,
                JSON.stringify(payload)
            );
            // ========

            //M2.1+ only return placeOrderService(serviceUrl, payload, messageContainer);
        };
    }
);
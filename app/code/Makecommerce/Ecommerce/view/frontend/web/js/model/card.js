/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'ko'
    ],
    function (ko) {
        'use strict';
        var useLogo = window.checkoutConfig.payment.makecommerce.ui.inlineUselogo;
        function Card( name, imagePath, methodType)
        {
            var card = this;
            this.name = name;
            this.imagePath = imagePath;
            this.methodType = methodType;
            this.disabled = ko.observable(false);

            this.getTitle  = function () {
                var titles = window.checkoutConfig.payment.makecommerce.titles;
                if (useLogo === 'logo') {
                    return '<img class="makecommerce-logo" src="'+ card.imagePath +'">';
                }
                if (useLogo === 'text' ) {
                    return '<span class="makecommerce-title">'+(titles[card.name] ? titles[card.name] : card.name ) + '</span>';
                }
                if (useLogo === 'text&logo') {
                    return '<span class="makecommerce-title logo-inline">'+ (titles[card.name] ? titles[card.name] : card.name) + '</span>'+'<img class="makecommerce-logo" src="'+ card.imagePath +'">';
                }
            };
            this.getId = function () {
                return this.name + '_card';
            }
        }


        Card.prototype.fromData = function (data) {
            if (Array.isArray(data)) {
                return data
                .map(function (card) {
                        return new Card(
                            card.name,
                            card.imagePath,
                            card.methodType,
                            card.country
                        );
                });
            }
        };
        Card.prototype.newFromInstance = function (card) {
            return new Card(card.name, card.imagePath, card.methodType, card.country);
        };

        return Card;
    }
);

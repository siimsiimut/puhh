
define(
    [
        'ko',
        'Makecommerce_Ecommerce/js/action/parse-sort-order',
        'Makecommerce_Ecommerce/js/action/attribute-sort',
        'Makecommerce_Ecommerce/js/model/card',
        'Makecommerce_Ecommerce/js/model/bank',
        'Makecommerce_Ecommerce/js/model/other',
        'Makecommerce_Ecommerce/js/model/paylater',
        'underscore'
    ],
    function (
        ko,
        parseSortOrder,
        attributeSort,
        Card,
        Bank,
        Other,
        Paylater,
        _
    ) {
        'use strict';
        var channelCollection;
        var selectedChannel = ko.observable(null);
        var instance;
        var initialized = false;
        var component = {};
        var disabledChannels = ko.observableArray([]);
        var cards;
        var banks;
        var others;
        var paylaters;
        var makecommerce = window.checkoutConfig.payment.makecommerce;
        var CountryManager = null;

        var chorder = parseSortOrder(window.checkoutConfig.payment.makecommerce.ui.chorder);
        var channelSort = attributeSort.bind({'sortOrder': chorder, 'attribute': 'name'});
        function ChannelManager()
        {
            if (!instance) {
                instance = this;
                return this;
            }
            return instance;
        }
        ChannelManager.setCollection = function (collection) {
            channelCollection = collection;
        };
        ChannelManager.getCollection = function () {
            return channelCollection;
        };
        ChannelManager.getBanks = function () {
            if (!banks) {
                banks = channelCollection.filter(function (channel) {
                    return channel.methodType === 'banklinks';
                });
            }
            return banks;
        };
        ChannelManager.getPaylaters = function () {
            if (!paylaters) {
                paylaters = channelCollection.filter(function (channel) {
                    return channel.methodType === 'payLater';
                });
            }
            return paylaters;
        };
        ChannelManager.getOthers = function () {
            if (!others) {
                others = channelCollection.filter(function (channel) {
                    return channel.methodType === 'other';
                });
            }
            return others;
        };
        ChannelManager.getNonCards = function () {
            console.log('siin on getNonCards 1');
            var nonCards = [];
            nonCards = nonCards.concat( this.getBanks, this.getOthers, this.getPaylaters);
            console.log('siin on getNonCards 2');
            return nonCards;
        };
        ChannelManager.getCards = function () {
            if (!cards) {
                cards = channelCollection.filter(function (channel) {
                    return channel.methodType === 'cards';
                });
            }
            return cards;
        };
        ChannelManager.selectChannel = function (channel) {
            return function () {
                selectedChannel(channel);
                return true;
            }
        };
        ChannelManager.findByData = function (channelData) {
            var keys = Object.keys(channelData);
            return ChannelManager.getCollection().filter(function (channel) {
                return (
                    _.isEqual(
                        keys,
                        keys.filter(
                            function (key) {
                                return channelData[key] === channel[key];
                            }
                        )
                    )
                );
            });
        };
        ChannelManager.getSelectedChannel = function () {
            return selectedChannel();
        };
        ChannelManager.isChannelSelected = function (channel) {
            return selectedChannel() === channel;
        };
        ChannelManager.disableType = function (type) {
            ChannelManager.findByData({"methodType": type})
            .forEach(
                function (channel) {
                    ChannelManager.disableChannel(channel);
                }
            );
        };
        ChannelManager.disableChannel = function (channel) {
            if (!ChannelManager.isChannelDisabled(channel)) {
                disabledChannels.push(channel);
            }
        };
        ChannelManager.isChannelDisabled = function (channel) {
            return disabledChannels.indexOf(channel) !== -1;
        };
        ChannelManager.isChannelVisible = function (channel) {
            if (!channel || !CountryManager.getSelectedCountry()) {
                return false; }
            if (!channel.country) {
                return true; }
            return channel.country === CountryManager.getSelectedCountry().name;
        };
        ChannelManager.sortCollection = function () {
            if (!Array.isArray(channelCollection)) {
                throw "The channelCollection is invalid";
            }
            var banks = this.getBanks();
            var others = this.getOthers();
            var paylaters = this.getPaylaters();
            var card = this.getCards();
            channelCollection = [];
            if (window.checkoutConfig.quoteData.store_currency_code === 'EUR') {
                CountryManager.getCollection().forEach(function (country) {
                    var countryBanks = banks.filter(function (bank) {
                            return bank.country === country.name;
                    })
                        .sort(channelSort);
                    var countryOthers = others.filter(function (other) {
                            return other.country === country.name;
                    })
                        .sort(channelSort);
                    var countryPaylaters = paylaters.filter(function (paylater) {
                            return paylater.country === country.name;
                    })
                        .sort(channelSort);
                    channelCollection = channelCollection.concat(countryBanks,countryPaylaters,countryOthers);
                });
            }
            channelCollection = channelCollection.concat(ChannelManager.getCards().sort(channelSort));
        };
        ChannelManager.initialize = function (state, countryManager, amount) {

            CountryManager = countryManager;
            var cards     = Card.prototype.fromData(makecommerce.paymentChannels.cards);
            console.log(cards);
            if (window.checkoutConfig.quoteData.store_currency_code === 'EUR') {
                var banks     = Bank.prototype.fromData(makecommerce.paymentChannels.banklinks, "banklinks", amount);
                var other     = Other.prototype.fromData(makecommerce.paymentChannels.other, "other", amount);
                var paylater  = Paylater.prototype.fromData(makecommerce.paymentChannels.payLater, "payLater", amount);
                ChannelManager.setCollection(cards.concat(banks, paylater, other));
            } else {
                ChannelManager.setCollection(cards);
            }

            ChannelManager.sortCollection();
        console.log(banks);
        console.log(other);
        console.log(paylater);
        console.log(cards);
        };
        return ChannelManager;
    }
);

define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/shipping-rates-validator',
        'Magento_Checkout/js/model/shipping-rates-validation-rules',
        'Makecommerce_Ecommerce/js/model/shipping-validator',
        'Makecommerce_Ecommerce/js/model/shipping-validation-rules'
    ],
    function (
        Component,
        defaultShippingRatesValidator,
        defaultShippingRatesValidationRules,
        shippingRatesValidator,
        shippingRatesValidationRules
    ) {
        'use strict';

        defaultShippingRatesValidator.registerValidator('omniva', shippingRatesValidator);
        defaultShippingRatesValidationRules.registerRules('omniva', shippingRatesValidationRules);

        defaultShippingRatesValidator.registerValidator('smartpost', shippingRatesValidator);
        defaultShippingRatesValidationRules.registerRules('smartpost', shippingRatesValidationRules);

        defaultShippingRatesValidator.registerValidator('dpd', shippingRatesValidator);
        defaultShippingRatesValidationRules.registerRules('dpd', shippingRatesValidationRules);

        return Component;
    }
);
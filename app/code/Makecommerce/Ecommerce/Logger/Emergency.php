<?php
namespace Makecommerce\Ecommerce\Logger;

class Emergency extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = \Monolog\Logger::EMERGENCY;

    /**
     * Logging level
     * @var int
     */
    protected $level = \Monolog\Logger::EMERGENCY;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/makecommerce_ecommerce_emergency.log';

    /**
     * {@inheritdoc}
     */
    public function isHandling(array $record)
    {
        return $record['level'] === $this->level;
    }
}

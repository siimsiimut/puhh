<?php
namespace Makecommerce\Ecommerce\Logger;

class Notice extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = \Monolog\Logger::NOTICE;

    /**
     * Logging level
     * @var int
     */
    protected $level = \Monolog\Logger::NOTICE;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/makecommerce_ecommerce_notice.log';

    /**
     * {@inheritdoc}
     */
    public function isHandling(array $record)
    {
        return $record['level'] === $this->level;
    }
}

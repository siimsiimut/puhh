<?php
namespace Makecommerce\Ecommerce\Logger;

class Info extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = \Monolog\Logger::INFO;

    /**
     * Logging level
     * @var int
     */
    protected $level = \Monolog\Logger::INFO;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/makecommerce_ecommerce_info.log';

    /**
     * {@inheritdoc}
     */
    public function isHandling(array $record)
    {
        return $record['level'] === $this->level;
    }
}

<?php
namespace Makecommerce\Ecommerce\Logger;

class Alert extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = \Monolog\Logger::ALERT;

    /**
     * Logging level
     * @var int
     */
    protected $level = \Monolog\Logger::ALERT;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/makecommerce_ecommerce_alert.log';

    /**
     * {@inheritdoc}
     */
    public function isHandling(array $record)
    {
        return $record['level'] === $this->level;
    }
}

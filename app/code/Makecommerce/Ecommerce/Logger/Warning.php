<?php
namespace Makecommerce\Ecommerce\Logger;

class Warning extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = \Monolog\Logger::WARNING;

    /**
     * Logging level
     * @var int
     */
    protected $level = \Monolog\Logger::WARNING;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/makecommerce_ecommerce_warning.log';

    /**
     * {@inheritdoc}
     */
    public function isHandling(array $record)
    {
        return $record['level'] === $this->level;
    }
}

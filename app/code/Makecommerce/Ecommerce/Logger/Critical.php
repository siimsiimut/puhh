<?php
namespace Makecommerce\Ecommerce\Logger;

class Critical extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = \Monolog\Logger::CRITICAL;

    /**
     * Logging level
     * @var int
     */
    protected $level = \Monolog\Logger::CRITICAL;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/makecommerce_ecommerce_critical.log';

    /**
     * {@inheritdoc}
     */
    public function isHandling(array $record)
    {
        return $record['level'] === $this->level;
    }
}

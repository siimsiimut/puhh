<?php
namespace Makecommerce\Ecommerce\Observer;

use Magento\Framework\Event\ObserverInterface;

class CheckoutSubmitAllAfter implements ObserverInterface
{
    // @codingStandardsIgnoreStart

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * CheckoutSubmitAllAfter constructor.
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     */
    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
    ) {
        $this->orderCollectionFactory = $orderCollectionFactory;
    }

    /**
     * Sets payment method specific data
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @used-by \Magento\Framework\Event\Invoker\InvokerDefault::_callObserverMethod()
     * @see \Magento\Framework\App\Action\Action::dispatch()
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getQuote();

        if ($quote->getPayment()->getMethodInstance() instanceof \Makecommerce\Ecommerce\Model\Makecommerce) {
            $quote->setReservedOrderId(null);
            $quote->setIsActive(true)->save();
        }
    }
}

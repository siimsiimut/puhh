<?php

namespace Makecommerce\Ecommerce\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class SendShippingAfterPayment implements ObserverInterface
{
    /**
     * @var \Makecommerce\Ecommerce\Helper\Carrier
     */
    protected $carrierHelper;

    /**
     * @param \Makecommerce\Ecommerce\Helper\Carrier $carrierHelper
     */
    public function __construct(
        \Makecommerce\Ecommerce\Helper\Carrier $carrierHelper
    ) {
        $this->carrierHelper = $carrierHelper;
    }
    public function execute(EventObserver $observer)
    {
        $order = $observer->getEvent()->getInvoice()->getOrder();

        $carrier = $this->carrierHelper->getCarrierModel($order->getShippingMethod());
        
        if ($carrier) {
            $carrier->sendShipmentData([$order->getId()]);
        }
        return $this;
    }
}

<?php

namespace Makecommerce\Ecommerce\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class SaveQuoteDataToOrder implements ObserverInterface
{
    /**
     * @var \Magento\Quote\Model\QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     */
    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
    ) {
        $this->quoteRepository = $quoteRepository;
    }

    public function execute(EventObserver $observer)
    {
        $order = $observer->getOrder();
        // Block sencing of order confirmation until return from payment, if method is makecommerce
        $payment = $order->getPayment();
        $method = $payment->getMethodInstance();
        if ($method->getCode() == 'makecommerce')
            $order->setCanSendNewEmailFlag(false);
        $quote = $this->quoteRepository->get($order->getQuoteId());
        $order->setLocationId($quote->getLocationId());
        return $this;
    }
}

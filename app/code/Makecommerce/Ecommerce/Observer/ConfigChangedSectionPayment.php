<?php

namespace Makecommerce\Ecommerce\Observer;

use Magento\Framework\Event\ObserverInterface;

class ConfigChangedSectionPayment implements ObserverInterface
{
    // @codingStandardsIgnoreStart
    /**
     * @var \Makecommerce\Ecommerce\Helper\Updater
     */
    protected $updater;

    /**
     * @var \Makecommerce\Ecommerce\Config
     */
    protected $config;
    // @codingStandardsIgnoreEnd

    /**
     * ConfigChangedSectionPayment constructor.
     * @param \Makecommerce\Ecommerce\Helper\Updater $updater
     * @param \Makecommerce\Ecommerce\Config $config
     */
    public function __construct(
        \Makecommerce\Ecommerce\Helper\Updater $updater,
        \Makecommerce\Ecommerce\Config $config
    ) {
        $this->updater = $updater;
        $this->config = $config;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->config->getValue('payment_method')) {
            if ($this->config->getApiValue('environment') === 'test') {
                $shopId     = $this->config->getApiValue('test_shop_id');
                $privateKey = $this->config->getApiValue('test_key_secret');
            } else {
                $shopId     = $this->config->getApiValue('shop_id');
                $privateKey = $this->config->getApiValue('api_secret');
            }

            if (!empty(trim($shopId)) && !empty(trim($privateKey))) {
                try {
                    $this->updater->update();
                } catch (\Exception $e) {
                    error_log("update failed");
                }
            }
        }
    }
}

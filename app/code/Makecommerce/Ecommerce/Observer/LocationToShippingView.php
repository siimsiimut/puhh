<?php

namespace Makecommerce\Ecommerce\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class LocationToShippingView implements ObserverInterface
{
    /**
     * @var \Magento\Framework\View\Element\Template
     */
    protected $elementTemplate;

    /**
     * @var \Makecommerce\Ecommerce\Helper\Carrier
     */
    protected $carrierHelper;

    /**
     * @param \Magento\Framework\View\Element\Template $elementTemplate
     * @param \Makecommerce\Ecommerce\Helper\Carrier $carrierHelper
     */
    public function __construct(
        \Magento\Framework\View\Element\Template $elementTemplate,
        \Makecommerce\Ecommerce\Helper\Carrier $carrierHelper
    ) {
        $this->elementTemplate = $elementTemplate;
        $this->carrierHelper = $carrierHelper;
    }

    public function execute(EventObserver $observer)
    {
        if ($observer->getElementName() == 'order_shipping_view') {
            $orderShippingViewBlock = $observer->getLayout()->getBlock($observer->getElementName());
            $order = $orderShippingViewBlock->getOrder();
            $locationId = $order->getLocationId();
            if ($locationId) {
                $carrier = $this->carrierHelper->getCarrierModel($order->getShippingMethod());
                $locationName = $carrier->getLocationName($locationId);
                $locationBlock = $this->elementTemplate;
                $locationBlock->setLocation($locationName);
                $locationBlock->setTemplate('Makecommerce_Ecommerce::order_location_info.phtml');
                $html = $observer->getTransport()->getOutput() . $locationBlock->toHtml();
                $observer->getTransport()->setOutput($html);
            }
        }
    }
}

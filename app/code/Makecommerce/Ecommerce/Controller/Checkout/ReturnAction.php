<?php
// we have to use the postfix URL, because return is a keyword in php
namespace Makecommerce\Ecommerce\Controller\Checkout;

// use Magento\Framework\App\CsrfAwareActionInterface;
// use Magento\Framework\App\RequestInterface;
// use Magento\Framework\App\Request\InvalidRequestException;

use Makecommerce\Ecommerce\Helper\Validator;
use Makecommerce\Ecommerce\Model\Makecommerce;

class ReturnAction extends \Magento\Framework\App\Action\Action
{

    /**
     * Order processor
     * @var \Makecommerce\Ecommerce\Helper\OrderProcessor
     */
    protected $orderProcessor;

    /**
     * Validator ensures that the required fields exist and are in the correct format
     * @var \Makecommerce\Ecommerce\Helper\Validator $validator
     */
    protected $validator;

    /**
     * Logger logs into MAGENTOROOT/var/log/mk_ecommerce_$level.log
     * Example mk_ecommerce_info.log
     * @var \Makecommerce\Ecommerce\Helper\Validator $validator
     */
    protected $logger;

    /**
     * Payment token factory
     * @var \Makecommerce\Ecommerce\Model\PaymentTokenFactory
     */
    protected $paymentTokenFactory;

    /**
     * Transaction manager
     * @var \Makecommerce\Ecommerce\Model\TransactionManager
     */
    protected $transactionManager;

    /**
     * ReturnAction constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param Validator $validator
     * @param \Makecommerce\Ecommerce\Logger\Logger $logger
     * @param \Makecommerce\Ecommerce\Helper\OrderProcessor $orderProcessor
     * @param \Makecommerce\Ecommerce\Model\TransactionManager $transactionManager
     * @param \Makecommerce\Ecommerce\Model\PaymentTokenFactory $paymentTokenFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        Validator $validator,
        \Makecommerce\Ecommerce\Logger\Logger $logger,
        \Makecommerce\Ecommerce\Helper\OrderProcessor $orderProcessor,
        \Makecommerce\Ecommerce\Model\TransactionManager $transactionManager,
        \Makecommerce\Ecommerce\Model\PaymentTokenFactory $paymentTokenFactory
    ) {
        $this->paymentTokenFactory = $paymentTokenFactory;
        $this->transactionManager = $transactionManager;
        $this->orderProcessor = $orderProcessor;
        $this->validator      = $validator;
        $this->logger         = $logger;
        parent::__construct($context);
        if (interface_exists('\Magento\Framework\App\CsrfAwareActionInterface')) {
            $request = $this->getRequest();
            if ($request->isPost() && empty($request->getParam('form_key'))) {
                $formKey = $this->_objectManager->get('\Magento\Framework\Data\Form\FormKey');
                $request->setParam('form_key', $formKey->getFormKey());
            }
        }
    }

    /**
     * Receives the return request from mk
     * Redirect to checkout/onepage|multishipping/success, if the payment is successful
     * We only care about the payment not sending false data or failure/refunds, everything else means success
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        $transactionData = $this->getRequest()->getParams();
        $this->logger->info(
            'Transaction return begin: ' .
            json_encode($transactionData, JSON_PRETTY_PRINT)
        );

        //return the decoded json if successful otherwise throws an error
        $transactionData = $this->validator->validate($transactionData);

        $this->logger->info(
            'Transaction return data parsed:' .
            json_encode($transactionData, JSON_PRETTY_PRINT)
        );
        if ($transactionData['message_type'] === Validator::TOKEN_RETURN) {
            if (isset($transactionData['token'])) {
                $token = $this->paymentTokenFactory->create()->setData($transactionData['token']);
                $transactionId = $transactionData['transaction']['id'];
                $transactionData = $this->transactionManager->pay($transactionId, $token)->getData();
            } else {
                return $this->_redirect('checkout/onepage/failure');
            }
        } elseif ($transactionData['message_type'] === Validator::PAYMENT_RETURN) {
            $this->orderProcessor->process($transactionData);
        }

        if (in_array(
            $transactionData['status'],
            [Makecommerce::STATUS_MAKSEKESKUS_COMPLETED,
                Makecommerce::STATUS_MAKSEKESKUS_PENDING,
                Makecommerce::STATUS_MAKSEKESKUS_PAID,
            Makecommerce::STATUS_MAKSEKESKUS_RECEIVED,
            ],
            true
        )
        ) {
            return $this->_redirect('checkout/onepage/success');
        } else {
            return $this->_redirect('checkout/onepage/failure');
        }
    }
}

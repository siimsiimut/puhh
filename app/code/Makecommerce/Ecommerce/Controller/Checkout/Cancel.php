<?php

namespace Makecommerce\Ecommerce\Controller\Checkout;

use Magento\Framework\Exception\LocalizedException;
use Makecommerce\Ecommerce\Model\Makecommerce;

class Cancel extends \Magento\Framework\App\Action\Action
{

    /**
     * OrderCollectionFactory
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * QuoteResolver finds a quote from either a masked id or entity id
     * QuoteResolver throws an exception if it does not find one
     * @var \Makecommerce\Ecommerce\Helper\QuoteResolver $quoteResolver
     */
    protected $quoteResolver;

    /**
     * Validator ensures that the required fields exist and are in the correct format
     * @var \Makecommerce\Ecommerce\Helper\Validator $validator
     */
    protected $validator;

    /**
     * Logger logs into MAGENTOROOT/var/log/mk_ecommerce_$level.log
     * Example mk_ecommerce_info.log
     * @var \Makecommerce\Ecommerce\Helper\Validator $validator
     */
    protected $logger;

    /**
     * Cancel constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Makecommerce\Ecommerce\Helper\QuoteResolver $quoteResolver
     * @param \Makecommerce\Ecommerce\Helper\Validator $validator
     * @param \Makecommerce\Ecommerce\Logger\Logger $logger
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Makecommerce\Ecommerce\Helper\QuoteResolver $quoteResolver,
        \Makecommerce\Ecommerce\Helper\Validator $validator,
        \Makecommerce\Ecommerce\Logger\Logger $logger
    ) {
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->quoteResolver          = $quoteResolver;
        $this->validator              = $validator;
        $this->logger                 = $logger;
        parent::__construct($context);
        if (interface_exists('\Magento\Framework\App\CsrfAwareActionInterface')) {
            $request = $this->getRequest();
            if ($request->isPost() && empty($request->getParam('form_key'))) {
                $formKey = $this->_objectManager->get('\Magento\Framework\Data\Form\FormKey');
                $request->setParam('form_key', $formKey->getFormKey());
            }
        }
    }

    /**
     * Receives the return request from mk
     *
     * Redirect to checkout/onepage|multishipping/success, if the payment is successful
     *
     * @return \Magento\Framework\App\ResponseInterface
     * @throws LocalizedException
     */
    public function execute()
    {
        $transactionData = $this->getRequest()->getParams();

        $this->validator->validate($transactionData);

        $transactionData = json_decode($transactionData['json'], true);

        $quote = $this->quoteResolver->resolve($transactionData['reference']);

        $orders = $this->orderCollectionFactory->create()->addFieldToFilter('quote_id', $quote->getId())->load();

        if ($orders->getSize() === 0) {
            throw new LocalizedException(__('No orders could be found for that quote id'));
        }

        if ($transactionData['status'] === Makecommerce::STATUS_MAKSEKESKUS_CANCELLED) {
            foreach ($orders as $order) {
                $order->cancel()->save();
            }
            $quote->setIsActive(false);
            $quote->save();
        }

        return $this->_redirect('checkout/onepage/failure');
    }
}

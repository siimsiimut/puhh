<?php

namespace Makecommerce\Ecommerce\Controller\Adminhtml\Order;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Makecommerce\Ecommerce\Api\Data\ShipmentResponseInterface;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;

class MassPrint extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction
{

    /**
     * @var \Makecommerce\Ecommerce\Helper\Carrier
     */
    protected $carrierHelper;

    /**
     * @var \Makecommerce\Ecommerce\Model\ResourceModel\ShipmentResponse\CollectionFactory
     */
    protected $shipmentResponseCollectionFactory;

    /**
     * @var \Makecommerce\Ecommerce\Model\Shipping\LabelManager
     */
    protected $labelManager;

    /**
     * @var array
     */
    private $ordersToPrint = [];

    /**
     * @param Context $context
     * @param Filter $filter
     * @param \Makecommerce\Ecommerce\Helper\Carrier
     * @param \Makecommerce\Ecommerce\Model\ResourceModel\ShipmentResponse\CollectionFactory
     *                                                     $shipmentResponseCollectionFactory
     * @param \Makecommerce\Ecommerce\Model\Shipping\LabelManager $labelManager
     */
    public function __construct(
        Context $context,
        Filter $filter,
        \Makecommerce\Ecommerce\Helper\Carrier $carrierHelper,
        \Makecommerce\Ecommerce\Model\ResourceModel\ShipmentResponse\CollectionFactory
        $shipmentResponseCollectionFactory,
        \Makecommerce\Ecommerce\Model\Shipping\LabelManager $labelManager,
        CollectionFactory $collectionFactory
    ) {
        parent::__construct($context, $filter);
        $this->carrierHelper = $carrierHelper;
        $this->shipmentResponseCollectionFactory = $shipmentResponseCollectionFactory;
        $this->labelManager = $labelManager;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Cancel selected orders
     *
     * @param AbstractCollection $collection
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    protected function massAction(AbstractCollection $collection)
    {
        foreach ($collection->getItems() as $order) {
            if (!$this->canPrintLabel($order)) {
                continue;
            }
            $this->addToLabelRequest($order->getId());
        }
        
        $response = $this->printLabels();

        if ($response->getError()) {
            if ($response->getErrorMessage()) {
                $this->messageManager->addError(__($response->getErrorMessage()));
            } else {
                $this->messageManager->addError(__('Could not print labels for selected orders.'));
            }
        } else {
            $this->messageManager->addSuccess(__(sprintf('We printed labels for %s order(s).', count($this->ordersToPrint))));
        }
        
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath($this->getComponentRefererUrl());
        return $resultRedirect;
    }

    /**
     * Checks if order was made with MC shipping method
     * and if shipment has been registered
     *
     * @param \Magento\Sales\Model\Order $order
     * @return bool
     */
    protected function canPrintLabel($order)
    {
        $carrier = $this->carrierHelper->getCarrierModel($order->getShippingMethod());
        if (!$carrier) {
            return false;
        }
        $shipmentResponseCollection = $this->shipmentResponseCollectionFactory->create()
            ->addFieldToFilter(ShipmentResponseInterface::ORDER_ID, ['eq' => $order->getId()]);
        if ($shipmentResponseCollection->count() < 1) {
            return false;
        }
        return true;
    }

    /**
     * Adds order ID to array which will be used to print labels
     *
     * @param int $orderId
     * @return void
     */
    protected function addToLabelRequest($orderId)
    {
        $this->ordersToPrint[] = $orderId;
    }

    /**
     * Sends request to print labels
     *
     * @return \Magento\Framework\DataObject
     */
    protected function printLabels()
    {
        return $this->labelManager->printLabels($this->ordersToPrint);
    }
}

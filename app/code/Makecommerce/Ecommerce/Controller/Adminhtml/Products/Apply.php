<?php

namespace Makecommerce\Ecommerce\Controller\Adminhtml\Products;

class Apply extends \Magento\Backend\App\Action
{

    /**
     * @var \Makecommerce\Ecommerce\Model\Shipping\ProductManager
     */
    protected $productManager;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Makecommerce\Ecommerce\Model\Shipping\ProductManager $productManager
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Makecommerce\Ecommerce\Model\Shipping\ProductManager $productManager
    ) {
        parent::__construct($context);
        $this->productManager = $productManager;
    }

    public function execute()
    {
        $allowed = $this->getRequest()->getParam('allowed');
        $allowed = (int)$allowed;
        $allowed = $allowed ? 1 : 0;
        try {
            $this->productManager->setAllowedForParcel($allowed);
        } catch (\Exception $e) {
            $this->getResponse()->setHttpResponseCode(204);
        }
    }
}

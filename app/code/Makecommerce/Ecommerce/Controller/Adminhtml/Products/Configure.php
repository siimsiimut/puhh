<?php

namespace Makecommerce\Ecommerce\Controller\Adminhtml\Products;

class Configure extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var array
     */
    public $_publicActions = ['configure'];

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * Load the page defined in view/adminhtml/layout/makecommerce_products_configure.xml
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Makecommerce Product Configuration'));
        $resultPage->addContent(
            $resultPage->getLayout()->createBlock('Makecommerce\Ecommerce\Block\Adminhtml\Configure')
        );
        return $resultPage;
    }
}

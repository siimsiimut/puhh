<?php
namespace Makecommerce\Ecommerce\Controller\Adminhtml\Configuration;

use Makecommerce\Ecommerce\Helper\Updater;

class Update extends \Magento\Backend\App\Action
{
    // @codingStandardsIgnoreStart
    /**
     * @var Updater
     */
    protected $updater;
    // @codingStandardsIgnoreEnd

    /**
     * Update constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param Updater $updater
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        Updater $updater
    ) {
        $this->updater = $updater;
        parent::__construct($context);
    }

    /**
     * Updates the payment methods in the config
     *
     * @return void
     */
    public function execute()
    {
        $this->updater->update();
    }
}

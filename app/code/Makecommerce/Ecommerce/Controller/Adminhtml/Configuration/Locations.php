<?php

namespace Makecommerce\Ecommerce\Controller\Adminhtml\Configuration;

use Magento\Framework\Exception\LocalizedException;

class Locations extends \Magento\Backend\App\Action
{
    /**
     * @var \Makecommerce\Ecommerce\Model\Shipping\Locations\Update
     */
    protected $locationUpdater;

    /**
     * @var \Makecommerce\Ecommerce\Logger\Logger
     */
    protected $customLogger;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Makecommerce\Ecommerce\Model\Shipping\Locations\Update $locationUpdater
     * @param \Makecommerce\Ecommerce\Logger\Logger
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Makecommerce\Ecommerce\Model\Shipping\Locations\Update $locationUpdater,
        \Makecommerce\Ecommerce\Logger\Logger $logger
    ) {
        $this->locationUpdater = $locationUpdater;
        $this->customLogger = $logger;
        parent::__construct($context);
    }

    /**
     * Updates shipping method's locations
     *
     * @return \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $this->customLogger->info(__('Updating shipping locations.'));
        try {
            $this->locationUpdater->update();
        } catch (LocalizedException $e) {
            $this->customLogger->error($e->getMessage());
            $this->getResponse()->setHttpResponseCode(204);
        }
    }
}

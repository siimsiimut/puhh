<?php
declare(strict_types = 1);
namespace Makecommerce\Ecommerce\Plugin\Cookie;

use Magento\Framework\Stdlib\Cookie\PublicCookieMetadata as Base;
use Magento\Framework\Stdlib\Cookie\CookieMetadata;

/**
 * Class PublicCookieMetadata
 * for PublicCookieMetadata
 */

class PublicCookieMetadata extends Base
{
    const KEY_SAMESITE = 'same_site';
    /**
     * Set SameSite flag
     *
     * @param string $sameSite
     * @return \Magento\Framework\Stdlib\Cookie\PublicCookieMetadata
     */
    public function setSameSite($sameSite): CookieMetadata
    {
        if (!method_exists('Magento\Framework\Stdlib\Cookie\PublicCookieMetadata', 'setSameSite')) {
            return parent::setSameSite($sameSite);
        }

        return $this->set(self::KEY_SAME_SITE, $sameSite);
    }
    
}

<?php

namespace Makecommerce\Ecommerce\Plugin\Quote;

use Makecommerce\Ecommerce\Model\Carrier\AbstractCarrier;

class AddressPlugin
{

    /**
     * @var \Magento\Shipping\Model\CarrierFactoryInterface
     */
    protected $carrierFactory;

    /**
     * @param \Magento\Shipping\Model\CarrierFactoryInterface $carrierFactory
     */
    public function __construct(\Magento\Shipping\Model\CarrierFactoryInterface $carrierFactory)
    {
        $this->carrierFactory = $carrierFactory;
    }

    /**
     * Sets correct carrier models for shipping methods
     *
     * @param \Magento\Quote\Model\Quote\Address $subject
     * @param callable $proceed
     * @return array
     */
    public function aroundGetGroupedAllShippingRates(
        \Magento\Quote\Model\Quote\Address $subject,
        callable $proceed
    ) {
        $rates = [];
        foreach ($subject->getShippingRatesCollection() as $rate) {
            $carrierCode = $this->isMakecommerceRate(
                $rate->getCarrier()
            ) ? sprintf('%s_%s', $rate->getCarrier(), $rate->getMethod()) : $rate->getCarrier();
            if (!$rate->isDeleted() && $this->carrierFactory->get($carrierCode)) {
                if (!isset($rates[$rate->getCarrier()])) {
                    $rates[$rate->getCarrier()] = [];
                }

                $rates[$rate->getCarrier()][] = $rate;
                $rates[$rate->getCarrier()][0]->carrier_sort_order =
                   $this->carrierFactory->get($carrierCode)->getSortOrder();
            }
        }
        uasort($rates, [$this, '_sortRates']);

        return $rates;
    }

    /**
     * Checks if is makecommerce rate
     *
     * @param string $carrierCode
     * @return bool
     */
    private function isMakecommerceRate($carrierCode)
    {
        $makecommerceRates = [
            AbstractCarrier::CARRIER_OMNIVA,
            AbstractCarrier::CARRIER_SMARTPOST,
            AbstractCarrier::CARRIER_DPD
        ];
        return in_array($carrierCode, $makecommerceRates);
    }

    /**
     * Sort rates recursive callback
     *
     * @param array $firstItem
     * @param array $secondItem
     * @return int
     */
    protected function _sortRates($firstItem, $secondItem)
    {
        if ((int)$firstItem[0]->carrier_sort_order < (int)$secondItem[0]->carrier_sort_order) {
            return -1;
        } elseif ((int)$firstItem[0]->carrier_sort_order > (int)$secondItem[0]->carrier_sort_order) {
            return 1;
        } else {
            return 0;
        }
    }
}

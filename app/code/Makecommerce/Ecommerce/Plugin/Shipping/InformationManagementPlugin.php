<?php

namespace Makecommerce\Ecommerce\Plugin\Shipping;

class InformationManagementPlugin
{
    /**
     * @var Magento\Quote\Model\QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var \Makecommerce\Ecommerce\Model\LocationFactory
     */
    protected $locationFactory;

    /**
     * @var string
     */
    protected $_quoteId;

    /**
     * @var \Magento\Quote\Api\CartTotalRepositoryInterface
     */
    protected $cartTotalsRepository;

    /**
     * @var \Makecommerce\Ecommerce\Helper\Carrier
     */
    protected $carrierHelper;

    /**
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Makecommerce\Ecommerce\Model\LocationFactory $locationFactory
     * @param \Magento\Quote\Api\CartTotalRepositoryInterface $cartTotalsRepository
     * @param \Magento\Quote\Model\Quote\TotalsCollector $totalsCollector
     * @param \Makecommerce\Ecommerce\Helper\Carrier $carrierHelper
     */
    public function __construct(
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Makecommerce\Ecommerce\Model\LocationFactory $locationFactory,
        \Magento\Quote\Api\CartTotalRepositoryInterface $cartTotalsRepository,
        \Magento\Quote\Model\Quote\TotalsCollector $totalsCollector,
        \Makecommerce\Ecommerce\Helper\Carrier $carrierHelper
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->locationFactory = $locationFactory;
        $this->cartTotalsRepository = $cartTotalsRepository;
        $this->_totalsCollector = $totalsCollector;
        $this->carrierHelper = $carrierHelper;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $extAttributes = $addressInformation->getExtensionAttributes();
        $locationId = $extAttributes->getLocationId();
        $quote = $this->quoteRepository->getActive($cartId);
        $quote->setLocationId($locationId);
        $this->_quoteId = $cartId;
    }

    public function afterSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $result
    ) {
        $quote = $this->quoteRepository->getActive($this->_quoteId);
        $locationId = $quote->getLocationId();
        if ($locationId) {
            $shippingMethod = $quote->getShippingAddress()->getShippingMethod();
            $carrier = $this->carrierHelper->getCarriermodel($shippingMethod);
            if ($carrier) {
                //has Makecommerce shipping method
                $country = $this->locationFactory->create()->load($locationId)->getCountry();

                $request = new \Magento\Framework\DataObject();
                $request->setAllItems(
                    $quote->getAllItems()
                )->setDestCountryId($country)->setPackageValue($quote->getSubtotal());
                $price = $carrier->getPrice($request);

                foreach ($quote->getShippingAddress()->getAllShippingRates() as $rate)
                {
                    if ($rate->getCode() === $shippingMethod)
                    {
                        $rate->setPrice($price);
                        $rate->save();
                    }
                }
                $quote->collectTotals();
                $quote->save();
            }
        }
        $result->setTotals($this->cartTotalsRepository->get($this->_quoteId));
        return $result;
    }
}

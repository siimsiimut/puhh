<?php

namespace Makecommerce\Ecommerce\Plugin\Shipping;

use Makecommerce\Ecommerce\Model\Carrier\AbstractCarrier;

class ShippingPlugin
{
    /**
     * If is makecommerce shipping method unset carrier limit
     *
     * @param  \Magento\Shipping\Model\Shipping       $subject
     * @param  \Magento\Quote\Model\Quote\Address\RateRequest $request
     * @return array
     */
    public function beforeCollectRates(
        \Magento\Shipping\Model\Shipping $subject,
        \Magento\Quote\Model\Quote\Address\RateRequest $request
    ) {
        if ($request->getLimitCarrier()) {
            if (in_array(
                $request->getLimitCarrier(),
                [
                    AbstractCarrier::CARRIER_OMNIVA,
                    AbstractCarrier::CARRIER_SMARTPOST,
                    AbstractCarrier::CARRIER_DPD
                ]
            )) {
                $request->setLimitCarrier(null);
            }
        }
        return [$request];
    }
}

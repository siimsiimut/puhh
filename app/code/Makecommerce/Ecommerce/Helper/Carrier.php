<?php

namespace Makecommerce\Ecommerce\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Makecommerce\Ecommerce\Model\Carrier\AbstractCarrier;

class Carrier extends AbstractHelper
{
    /**
     * @var \Makecommerce\Ecommerce\Model\Carrier\Omniva\Parcel
     */
    protected $omnivaParcel;

    /**
     * @var \Makecommerce\Ecommerce\Model\Carrier\Omniva\Courier
     */
    protected $omnivaCourier;

    /**
     * @var \Makecommerce\Ecommerce\Model\Carrier\Smartpost\Parcel
     */
    protected $smartpostParcel;

    /**
     * @var \Makecommerce\Ecommerce\Model\Carrier\Smartpost\Courier
     */
    protected $smartpostCourier;

    /**
     * @var \Makecommerce\Ecommerce\Model\Carrier\Dpd\Parcel
     */
    protected $dpdParcel;

    /**
     * @param Context $context
     * @param \Makecommerce\Ecommerce\Model\Carrier\Omniva\Parcel $omnivaParcel
     * @param \Makecommerce\Ecommerce\Model\Carrier\Omniva\Courier $omnivaCourier
     * @param \Makecommerce\Ecommerce\Model\Carrier\Smartpost\Parcel $smartpostParcel
     * @param \Makecommerce\Ecommerce\Model\Carrier\Smartpost\Courier $smartpostCourier
     * @param \Makecommerce\Ecommerce\Model\Carrier\Dpd\Parcel $dpdParcel
     */
    public function __construct(
        Context $context,
        \Makecommerce\Ecommerce\Model\Carrier\Omniva\Parcel $omnivaParcel,
        \Makecommerce\Ecommerce\Model\Carrier\Omniva\Courier $omnivaCourier,
        \Makecommerce\Ecommerce\Model\Carrier\Smartpost\Parcel $smartpostParcel,
        \Makecommerce\Ecommerce\Model\Carrier\Smartpost\Courier $smartpostCourier,
        \Makecommerce\Ecommerce\Model\Carrier\Dpd\Parcel $dpdParcel
    ) {
        $this->omnivaParcel = $omnivaParcel;
        $this->omnivaCourier = $omnivaCourier;
        $this->smartpostParcel = $smartpostParcel;
        $this->smartpostCourier = $smartpostCourier;
        $this->dpdParcel = $dpdParcel;
        parent::__construct($context);
    }

    /**
     * Returns method's model based on carrier code
     *
     * @param string $carrierCode
     * @return mixed
     */
    public function getCarrierModel($carrierCode)
    {
        switch ($carrierCode) {
            case AbstractCarrier::CARRIER_OMNIVA . '_' . AbstractCarrier::METHOD_PARCEL:
                return $this->omnivaParcel;
            case AbstractCarrier::CARRIER_OMNIVA . '_' . AbstractCarrier::METHOD_COURIER:
                return $this->omnivaCourier;
            case AbstractCarrier::CARRIER_SMARTPOST . '_' . AbstractCarrier::METHOD_PARCEL:
                return $this->smartpostParcel;
            case AbstractCarrier::CARRIER_SMARTPOST . '_' . AbstractCarrier::METHOD_COURIER:
                return $this->smartpostCourier;
            case AbstractCarrier::CARRIER_DPD . '_' . AbstractCarrier::METHOD_PARCEL:
                return $this->dpdParcel;
            default:
                return null;
        }
    }
}

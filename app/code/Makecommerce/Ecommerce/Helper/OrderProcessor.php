<?php

namespace Makecommerce\Ecommerce\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order\Payment;
use Makecommerce\Ecommerce\Model\Makecommerce;

class OrderProcessor extends AbstractHelper
{
    /**
     * Makes sure that ALL the items get saved
     *
     * @var \Magento\Framework\DB\TransactionFactory $transactionFactory
     */
    protected $transactionFactory;

    /**
     * Logger logs into MAGENTOROOT/var/log/mk_ecommerce_$level.log
     * Example mk_ecommerce_info.log
     * @var \Makecommerce\Ecommerce\Helper\Validator $validator
     */
    protected $logger;

    /**
     * Order collection factory
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * QuoteResolver finds a quote from either a masked id or entity id
     * QuoteResolver throws an exception if it does not find one
     * @var \Makecommerce\Ecommerce\Helper\QuoteResolver $_quoteResolver
     */
    protected $quoteResolver;

    /**
     * Transaction model factory
     * @var \Makecommerce\Ecommerce\Model\TransactionFactory
     */
    protected $transactionModelFactory;

    /**
     * OrderProcessor constructor.
     * @param Context $context
     * @param \Magento\Framework\DB\TransactionFactory $transactionFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Makecommerce\Ecommerce\Logger\Logger $logger
     * @param QuoteResolver $quoteResolver
     * @param \Makecommerce\Ecommerce\Model\TransactionFactory $transactionModelFactory
     */
    public function __construct(
        Context $context,
        \Magento\Framework\DB\TransactionFactory $transactionFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $sender,
        \Makecommerce\Ecommerce\Logger\Logger $logger,
        QuoteResolver $quoteResolver,
        \Makecommerce\Ecommerce\Model\TransactionFactory $transactionModelFactory
    ) {
        $this->transactionModelFactory = $transactionModelFactory;
        $this->logger = $logger;
        $this->transactionFactory = $transactionFactory;
        $this->quoteResolver = $quoteResolver;
        $this->sender = $sender;
        $this->orderCollectionFactory = $orderCollectionFactory;
        parent::__construct($context);
    }

    /**
     * Process the order
     *
     * @param string[string] $transactionData
     * @throws LocalizedException
     * @return void
     */
    public function process($transactionData)
    {
        $quote = $this->quoteResolver->resolve($transactionData['reference']);

        $transactionId = isset($transactionData['transaction']) ?
            $transactionData['transaction'] : $transactionData['id'];

        $transactionModel = $this->transactionModelFactory->create()->load($transactionId, 'id');

        $orderIds = $transactionModel->getOrderIds();

        $orders = $this->orderCollectionFactory
            ->create()
            ->addFieldToFilter('entity_id', ['in' => [$orderIds]])->load();

        $logger = $this->logger;

        if ($orders->getSize() === 0) {
            $logger->error(__('No orders could be found for that quote id:' . $quote->getId()));
            throw new LocalizedException(__('No orders could be found for that quote id:' . $quote->getId()));
        }
        if ($transactionData['status'] === Makecommerce::STATUS_MAKSEKESKUS_COMPLETED) {
            foreach ($orders as $order) {
                if ((round((float)$order->getBaseTotalDue(), 2) === round((float)$transactionData['amount'], 2)) &&
                    !$order->hasInvoices() && !$order->isCanceled()) {
                    $invoice = $order->prepareInvoice();
                    if (!$invoice) {
                        $logger->error(__('An invoice could not be created.'));
                        throw new LocalizedException(__('An invoice could not be created.'));
                    }

                    $invoice->register()->capture();
                    $order = $invoice->getOrder();
                    $order->setCanSendNewEmailFlag(true);
                    $order->save();
                    $this->sender->send($order, true);
                    $payment = $order->getPayment();
                    $transaction = $payment->getCreatedTransaction();

                    $dbTransaction = $this->transactionFactory->create();
                    $dbTransaction
                        ->addObject($payment)
                        ->addObject($transaction)
                        ->addObject($invoice)
                        ->addObject($order)
                        ->save();
                } else {
                    $order->addStatusToHistory(
                        $order->getStatus(),
                        __('The order was paid earlier, but still got notification')
                    );
                }
            }
            $quote->setIsActive(false)->save();
        }
    }
}

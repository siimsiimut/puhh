<?php

namespace Makecommerce\Ecommerce\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Makecommerce\Ecommerce\Config;
use Makecommerce\Ecommerce\Logger\Logger;

class Validator extends AbstractHelper
{
    /*
     * +Return types
     */
    const TOKEN_RETURN = 'token_return';//card
    const PAYMENT_RETURN = 'payment_return';//bank
    /*
     * -Return types
     */

    // @codingStandardsIgnoreStart
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Logger
     */
    protected $logger;
    // @codingStandardsIgnoreEnd

    /**
     * Validator constructor.
     * @param Context $context
     * @param Config $config
     * @param Logger $logger
     */
    public function __construct(
        Context $context,
        Config $config,
        Logger $logger
    ) {
        $this->logger = $logger;
        $this->config = $config;
        parent::__construct($context);
    }

    /**
     * Validate transaction data
     *
     * @param string[string] $transactionData
     * @return string[string]
     * @throws LocalizedException
     */
    public function validate($transactionData)
    {
        $logger = $this->logger;
        if (!isset($transactionData['json'])) {
            $logger->error(__('Transaction is missing json'));
            throw new LocalizedException(__('Transaction is missing json'));
        }
        if (!isset($transactionData['mac'])) {
            $logger->error(__('Transaction is missing mac'));
            throw new LocalizedException(__('Transaction is missing mac'));
        }
        $json = $transactionData['json'];

        $decoded = json_decode($json, true);

        $this->validateFields($decoded);
        $mac = $transactionData['mac'];

        $config = $this->config;

        if ($config->getApiValue('environment') === 'test') {
            $privateKey = $config->getApiValue('test_key_secret');
        } else {
            $privateKey = $config->getApiValue('api_secret');
        }

        $calculatedMac = strtoupper(hash('sha512', $json . $privateKey));

        if ($mac !== $calculatedMac) {
            $logger->error(__('The calculated MAC does not match the sent MAC'));
            throw new LocalizedException(__('The calculated MAC does not match the sent MAC'));
        }
        $logger->info(__('Validator succeeded'));
        return $decoded;
    }

    /**
     * Ensures that the json response has all the required fields
     *
     * @param string[string|int] $json
     * @throws \Magento\Framework\Exception\LocalizedException;
     * @return void
     */
    protected function validateFields($json)
    {
        if ($this->config->getApiValue('environment') === 'test') {
            $shopId = $this->config->getApiValue('test_shop_id');
        } else {
            $shopId = $this->config->getApiValue('shop_id');
        }

        if (!isset($json['message_type'])) {
            $this->logger->error(__('Return data is missing message type'));
            throw new LocalizedException(__('Return data is missing message type'));
        }
        if ($json['message_type'] === self::TOKEN_RETURN) {
            if (!isset($json['transaction'])) {
                $this->logger->error(__('Return data is missing transaction'));
                throw new LocalizedException(__('Return data is missing transaction'));
            }
            /*if (!isset($json['token'])) {
                $this->logger->error(__('Return data is missing token'));
                throw new LocalizedException(__('Return data is missing token'));
            }*/
            return;
        }
        if (!isset($json['reference'])) {
            $this->logger->error(__('Transaction is missing quote id'));
            throw new LocalizedException(__('Transaction is missing quote id'));
        }
        if (!isset($json['status'])) {
            $this->logger->error(__('Transaction is missing status'));
            throw new LocalizedException(__('Transaction is missing status'));
        }
        if (!isset($json['amount'])) {
            $this->logger->error(__('Transaction is missing amount'));
            throw new LocalizedException(__('Transaction is missing amount'));
        }
        if (!(isset($json['shop']) && $json['shop'] === $shopId)) {
            $this->logger->error(__('The shop id sent by the response does not match ours'));
            throw new LocalizedException(__('The shop id sent by the response does not match ours'));
        }
    }
}

<?php
namespace Makecommerce\Ecommerce\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Exception\AuthorizationException;
use Magento\Framework\Exception\LocalizedException;

/**
 * UNUSED FILE, keep just in case
 *
 **/
class ErrorHandler extends AbstractHelper
{
    // @codingStandardsIgnoreStart
    /**
     * $logger Logs to makecommerce files
     * @var \Makecommerce\Ecommerce\Logger\Logger
     */
    protected $logger;

    /**
     * ErrorHandler constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Makecommerce\Ecommerce\Logger\Logger $logger
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Makecommerce\Ecommerce\Logger\Logger $logger
    ) {
        parent::__construct($context);
        $this->logger = $logger;
    }
    // @codingStandardsIgnoreEnd

    /**
     * Handles the error
     * @param string $code
     * @param string $message
     * @throws AuthenticationException
     * @throws AuthorizationException
     * @throws LocalizedException
     * @return void
     */
    public function handle($code, $message)
    {
        $logger = $this->logger;

        switch ($code) {
            case '1001':
                $logger->error('Error ' . $code . ' with message: ' . $message);
                throw new LocalizedException(
                    __('(' . $code . ') : ' . $message)
                );
                break;
            case '1002':
                $logger->error('Error ' . $code . ' with message: ' . $message);
                throw new LocalizedException(
                    __('(' . $code . ') : ' . $message)
                );
                break;
            case '1004':
                $logger->error('Error ' . $code . ' with message: ' . $message);
                throw new AuthorizationException(
                    __('Authorization failed: (' . $code . ') : ' . $message)
                );
                break;
            case '1005':
                $logger->error('Error ' . $code . ' with message: ' . $message);
                throw new AuthenticationException(
                    __('Corresponding authentication level required. (' . $code . ') : ' . $message)
                );
                break;
            case '1011':
                $logger->error('Error ' . $code . ' with message: ' . $message);
                throw new LocalizedException(
                    __('(' . $code . ') : ' . $message)
                );
                break;
            case '1012':
                $logger->error('Error ' . $code . ' with message: ' . $message);
                throw new LocalizedException(
                    __('(' . $code . ') : ' . $message)
                );
                break;
            case '1013':
                $logger->error('Error ' . $code . ' with message: ' . $message);
                throw new LocalizedException(
                    __('(' . $code . ') : ' . $message)
                );
                break;
            case '1021':
                $logger->error('Error ' . $code . ' with message: ' . $message);
                throw new LocalizedException(
                    __('(' . $code . ') : ' . $message)
                );
                break;
            case preg_match('/103[0-9]/', $code):
                    $logger->error('Error ' . $code . ' with message: ' . $message);
                throw new LocalizedException(
                    __('(' . $code . ') : ' . $message)
                );
                break;
            case '1041':
                $logger->error('Error ' . $code . ' with message: ' . $message);
                throw new LocalizedException(
                    __('(' . $code . ') : ' . $message)
                );
                break;
            case '1042':
                $logger->error('Error ' . $code . ' with message: ' . $message);
                throw new LocalizedException(
                    __('(' . $code . ') : ' . $message)
                );
                break;
            case (preg_match('/105[0-9]/', $code) ? true : false):
                $logger->error('Error ' . $code . ' with message: ' . $message);
                throw new LocalizedException(
                    __('(' . $code . ') : ' . $message)
                );
            case preg_match('/106[0-9]/', $code):
                        $logger->error('Error ' . $code . ' with message: ' . $message);
                throw new LocalizedException(
                    __('(' . $code . ') : ' . $message)
                );
                break;
            default:
                $logger->error('Error ' . $code . ' with message: ' . $message);
                throw new LocalizedException(
                    __('Request failed with error message: (' . $code . ') : ' . $message)
                );
        }
    }
}

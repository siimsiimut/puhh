<?php

namespace Makecommerce\Ecommerce\Helper;

use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Makecommerce\Ecommerce\Client\MakecommerceClientWrapper;

class Updater extends AbstractHelper
{
    // @codingStandardsIgnoreStart
    /**
     * @var Config
     */
    protected $resourceConfig;

    /**
     * @var \Makecommerce\Ecommerce\Logger\Logger
     */
    protected $logger;

    /**
     * @var MakecommerceClientWrapper
     */
    protected $api;

    /**
     * @var \Makecommerce\Ecommerce\Model\Shipping\Locations\Update
     */
    protected $locationUpdater;

    /**
     * @var \Makecommerce\Ecommerce\Config
     */
    protected $mkConfig;

    /**
     * @var \Makecommerce\Ecommerce\Helper\Data
     */
    protected $scopeHelper;
    // @codingStandardsIgnoreEnd

    /**
     * Updater constructor.
     * @param Context $context
     * @param Config $resourceConfig
     * @param \Makecommerce\Ecommerce\Logger\Logger $logger
     * @param MakecommerceClientWrapper $api,
     * @param \Makecommerce\Ecommerce\Model\Shipping\Locations\Update $locationUpdater
     * @param \Makecommerce\Ecommerce\Config $mkConfig
     * @param \Makecommerce\Ecommerce\Helper\Data $scopeHelper
     */
    public function __construct(
        Context $context,
        Config $resourceConfig,
        \Makecommerce\Ecommerce\Logger\Logger $logger,
        MakecommerceClientWrapper $api,
        \Makecommerce\Ecommerce\Model\Shipping\Locations\Update $locationUpdater,
        \Makecommerce\Ecommerce\Config $mkConfig,
        \Makecommerce\Ecommerce\Helper\Data $scopeHelper
    ) {
        $this->api = $api;
        $this->logger = $logger;
        $this->resourceConfig = $resourceConfig;
        $this->locationUpdater = $locationUpdater;
        $this->mkConfig = $mkConfig;
        $this->scopeHelper = $scopeHelper;
        parent::__construct($context);
    }

    /**
     * Update
     *
     * @param bool $isCmd shows if is executed from command line
     * @throws LocalizedException
     * @return void
     */
    public function update($isCmd = false)
    {
        $scopeType = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
        $scopeId = null;
        if (!$isCmd) {
            $scopeData = $this->scopeHelper->getScopeData();
            $scopeType = $scopeData->getScopeType();
            $scopeId = $scopeData->getScopeId();
        }
        $this->setApiConf($scopeType, $scopeId);

        $logger = $this->logger;
        // $logger->info("Shop config: ");
        $methods = $this->api->getShopConfig();
        // $logger->info(json_encode($methods));
        if (!$methods) {
            throw new LocalizedException(__('Unable to get payment methods'));
        }
        if (isset($methods->paymentMethods)) {
            $methods = $methods->paymentMethods;
        }

        $methods = json_encode($methods);
        $this->logger->info(__('Updating payment methods in config.'));

        $this->resourceConfig->saveConfig(
            'payment/' .
            \Makecommerce\Ecommerce\Model\Makecommerce::PAYMENT_METHOD_CODE .
            '/payment_methods', //path
            $methods, //value
            $scopeType, //'default', //scope
            $scopeId//0//scope code
        );
    }

    /**
     * Updates shipping method locations
     *
     * @return void
     */
    public function updateLocations()
    {
        try {
            $this->locationUpdater->update();
        } catch (LocalizedException $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * Sets api values for correct scope
     *
     * @param string $scopeType
     * @param int $scopeId
     * @return void
     */
    private function setApiConf($scopeType, $scopeId)
    {
        $isTest = $this->mkConfig->getApiValue('environment') === 'test';
        $shopIdPath = $isTest ? 'test_shop_id' : 'shop_id';
        $privateKeyPath = $isTest ? 'test_key_secret' : 'api_secret';
        $publicKeyPath = $isTest ?  'test_key_public' : 'api_public';
        $urlPath = $isTest ?
            \Makecommerce\Ecommerce\Client\MakecommerceLib::TEST_URL_PATH :
            \Makecommerce\Ecommerce\Client\MakecommerceLib::URL_PATH;
        $this->api->setShopId($this->mkConfig->getApiValue($shopIdPath, $scopeId));
        $this->api->setPublishableKey($this->mkConfig->getApiValue($publicKeyPath, $scopeId));
        $this->api->setSecretKey($this->mkConfig->getApiValue($privateKeyPath, $scopeId));
        $this->api->setApiUrl($urlPath);
    }
}

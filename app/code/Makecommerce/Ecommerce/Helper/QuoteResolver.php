<?php

namespace Makecommerce\Ecommerce\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;

class QuoteResolver extends AbstractHelper
{
    // @codingStandardsIgnoreStart
    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var \Magento\Quote\Model\QuoteIdMaskFactory
     */
    protected $quoteIdMaskFactory;

    /**
     * @var \Makecommerce\Ecommerce\Logger\Logger
     */
    protected $logger;
    // @codingStandardsIgnoreEnd

    /**
     * QuoteResolver constructor.
     * @param Context $context
     * @param \Magento\Quote\Model\QuoteFactory $quoteFactory
     * @param \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory
     * @param \Makecommerce\Ecommerce\Logger\Logger $logger
     */
    public function __construct(
        Context $context,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory,
        \Makecommerce\Ecommerce\Logger\Logger $logger
    ) {
        $this->quoteFactory = $quoteFactory;
        //used to determine real quote id, in case the customer is a quest
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->logger = $logger;
        parent::__construct($context);
    }

    /**
     * Resolves the quote from either masked or unmasked entity id
     *
     * @param string $quoteId
     * @return \Magento\Quote\Model\Quote
     * @throws LocalizedException
     */
    public function resolve($quoteId)
    {
        $quote = $this->quoteFactory->create();
        $realQuoteId = $this->quoteIdMaskFactory->create()->load($quoteId, 'masked_id')->getQuoteId();
        if ($realQuoteId) {
            $quote = $quote->load($realQuoteId);
        } else {
            $realQuoteId = $quoteId;
            $quote = $quote->load($realQuoteId);
        }
        if ($quote->getId() === $realQuoteId) {
            return $quote;
        }
        $this->logger->error(__('Quote with the following id could not be found:' . $quoteId));
        throw new LocalizedException(__('Quote with the following id could not be found:' . $quoteId));
    }
}

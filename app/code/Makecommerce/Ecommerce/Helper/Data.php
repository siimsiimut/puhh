<?php

namespace Makecommerce\Ecommerce\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\DataObject;

class Data extends AbstractHelper
{
    /**
     * @var \Magento\Framework\App\State
     */
    protected $appState;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    protected $productMetadata;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\State $appState
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param  \Magento\Framework\App\ProductMetadataInterface $productMetadata
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\State $appState,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata
        // \Magento\Framework\Serialize\SerializerInterface $serializer
    ) {
        $this->appState = $appState;
        $this->storeManager = $storeManager;
        $this->productMetadata = $productMetadata;
        // $this->serializer = $serializer;
        parent::__construct($context);
    }

    /**
     * Gets current selected scope data
     *
     * @return DataObject
     */
    public function getScopeData()
    {
        $scopeData = new DataObject();
        $scopeType = \Magento\Framework\App\Config\ScopeConfigInterface::SCOPE_TYPE_DEFAULT;
        $scopeId = 1;

        $areaCode = null;
        try {
            $areaCode = $this->appState->getAreaCode();
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }

        if ($areaCode === \Magento\Backend\App\Area\FrontNameResolver::AREA_CODE) {
            if (!empty($this->_request->getParam('scope_type')) && !empty($this->_request->getParam('scope_id'))) {
                $scopeData
                    ->setScopeType($this->_request->getParam('scope_type'))
                    ->setScopeId($this->_request->getParam('scope_id'));
                return $scopeData;
            }
            $storeId = $this->_request->getParam('store');
            $websiteId = $this->_request->getParam('website');
            if (!$storeId && !$websiteId) {
                $scopeId = 0;
            } elseif (!$storeId && $websiteId) {
                $scopeType = \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES;
                $scopeId = $websiteId;
            } else {
                $scopeType = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
                $scopeId = $storeId;
            }
        } else {
            $scopeType = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
            $scopeId = $this->storeManager->getStore()->getId();
        }

        $scopeData->setScopeType($scopeType)->setScopeId($scopeId);
        return $scopeData;
    }

    /**
     * Unserializes config data based on Magento version
     *
     * @param  obj $config
     * @return array
     */
    public function unserializeConfig($config)
    {
        if (empty($config) || !(is_string($config))) {
            return null;
        } else {
            //in 2.2 serialized data was replaced with json_encoded data
            if (version_compare($this->getMagentoVersion(), '2.2', '>')) {
                return json_decode($config, true);
            } else {
                return json_decode($config, true);
                // return $this->serializer->unserialize($config);
            }
        }
    }

    /**
     * Gets Magento version
     *
     * @return string
     */
    public function getMagentoVersion()
    {
        return $this->productMetadata->getVersion();
    }
}

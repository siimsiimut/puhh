<?php

namespace Makecommerce\Ecommerce\Helper;

use Magento\Directory\Model\CountryFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\View\Asset\Repository;
use Makecommerce\Ecommerce\Config;

class Environment extends AbstractHelper
{

    // @codingStandardsIgnoreStart
    /**
     * @var \Makecommerce\Ecommerce\Logger\Logger
     */
    protected $logger;

    /**
     * @var Repository
     */
    protected $assetRepository;

    /**
     * @var CountryFactory
     */
    protected $countryFactory;

    /**
     * @var Config
     */
    protected $config;
    // @codingStandardsIgnoreEnd

    /**
     * Environment constructor.
     * @param Context $context
     * @param Repository $assetRepository
     * @param CountryFactory $countryFactory
     * @param Config $config
     * @param \Makecommerce\Ecommerce\Logger\Logger $logger
     */
    public function __construct(
        Context $context,
        Repository $assetRepository,
        CountryFactory $countryFactory,
        Config $config,
        \Makecommerce\Ecommerce\Logger\Logger $logger
    ) {
        $this->logger = $logger;
        $this->countryFactory = $countryFactory;
        $this->assetRepository = $assetRepository;
        $this->config = $config;
        parent::__construct($context);
    }

    /**
     * Returns the methods with image and type fields
     * The methods are parsed from json
     * The fields are added based on the name and the array they belong in
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return string[int]|bool
     */
    public function getChannels()
    {
        $imageUrlPath = 'https://static.maksekeskus.ee/img/channel/lnd/';

        try {
            $paymentChannels = json_decode($this->config->getValue('payment_methods'), true);
            if (isset($paymentChannels['paymentMethods'])) {
                /**
                 * if configuration method was used instead of methods
                 * @see \Makecommerce\Ecommerce\Client\ZendApiClient::configuration
                 */
                $paymentChannels = $paymentChannels['paymentMethods'];
            }
            foreach ($paymentChannels as $methodType => $methods) {
                foreach ($methods as $index => $methodData) {
                    if (!isset($methodData['name'])) {
                        continue;
                    }
                    $paymentChannels[$methodType][$index]['imagePath'] = $methodData['logo_url'];
                    $paymentChannels[$methodType][$index]['methodType'] = $methodType;
                }
            }
            return $paymentChannels;
        } catch (\Exception $exception) {
            $this->logger->error(__('The provided Payment methods contain invalid or missing data:'
                . $exception->getMessage()));
            return false;
            //checkout doesn't handle errors
            // throw new LocalizedException(__('The provided Payment methods contain invalid or missing data:'
            //     .$exception->getMessage()));
        }
    }

    /**
     * Returns the countries found on the payment methods and adds imagepath to them
     * Also normalizes the array so that json.parse would return an array not an object
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return array|bool
     */
    public function getCountries()
    {
        $country = $this->countryFactory->create();
        $countries = [];
        $tmp = [];
        $size = '32';
        try {
            $paymentMethods = json_decode($this->config->getValue('payment_methods'), true);
            if (isset($paymentMethods['paymentMethods'])) {
                $paymentMethods = $paymentMethods['paymentMethods'];
            }
            foreach ($paymentMethods as $methodType => $methods) {
                foreach ($methods as $index => $methodData) {
                    if (!isset($methodData['country'])) {
                        continue;
                    }
                    $tmp[] = [
                        'title' => $country->loadByCode(strtoupper($methodData['country']))->getName(),
                        'name' => $methodData['country'],
                        'imagePath' => $this->assetRepository
                            ->getUrl('Makecommerce_Ecommerce::images/' . $methodData['country'] . $size . '.png'),
                    ];
                }
            }
            //remove duplicate keys
            $tmp = array_unique($tmp, SORT_REGULAR);
            //ensure an non assoc array
            foreach ($tmp as $country) {
                $countries[] = $country;
            }
            return $countries;
        } catch (\Exception $exception) {
            $this->logger->error(__('The provided countries contain invalid or missing data:'
                . $exception->getMessage()));
            return false;
            //checkout doesn't handle errors
            //throw new LocalizedException(__('The provided countries contain invalid or missing data'));
        }
    }
}

<?php

namespace Makecommerce\Ecommerce;

use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\ScopeInterface;

class Config implements \Magento\Payment\Model\Method\ConfigInterface
{
    // @codingStandardsIgnoreStart
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var int
     */
    protected $_storeId;

    /**
     * @var string
     */
    protected $_pathPattern;

    /**
     * @var string
     */
    protected $_methodCode;

    /**
     * @var \Magento\Framework\Component\ComponentRegistrarInterface
     */
    protected $_componentRegistrar;

    /**
     * @var \Magento\Framework\Filesystem\Directory\ReadFactory
     */
    protected $_readDirFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Makecommerce\Ecommerce\Helper\Data
     */
    protected $helper;
    // @codingStandardsIgnoreEnd

    /**
     * Config constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Component\ComponentRegistrarInterface $componentRegistrar
     * @param \Magento\Framework\Filesystem\Directory\ReadFactory $readDirFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Makecommerce\Ecommerce\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Component\ComponentRegistrarInterface $componentRegistrar,
        \Magento\Framework\Filesystem\Directory\ReadFactory $readDirFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Makecommerce\Ecommerce\Helper\Data $helper
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_componentRegistrar = $componentRegistrar;
        $this->_readDirFactory = $readDirFactory;
        $this->_storeManager = $storeManager;
        $this->helper = $helper;

        $this->setMethodCode(\Makecommerce\Ecommerce\Model\Makecommerce::PAYMENT_METHOD_CODE);
    }

    /**
     * Retrieve information from payment configuration
     *
     * @param string $field
     * @param null | string $scopeType
     * @param null | int $scopeId
     * @return mixed|null|string
     * @throws LocalizedException
     */
    public function getValue($field, $scopeType = null, $scopeId = null)
    {
        if (!$this->getMethodCode()) {
            throw new LocalizedException(__("Method code must be set"));
        }
        $underscored = strtolower(preg_replace('/(.)([A-Z])/', "$1_$2", $field));
        $path = $this->_getSpecificConfigPath($underscored);

        $scopeData = $this->helper->getScopeData();
        $scopeType = $scopeType ? $scopeType : $scopeData->getScopeType();
        $scopeId = $scopeId ? $scopeId : $scopeData->getScopeId();

        if ($path !== null) {
            $value = $this->_scopeConfig->getValue(
                $path,
                $scopeType,
                $scopeId
            );
            $value = $this->_prepareValue($underscored, $value);
            return $value;
        }
        return null;
    }

    /**
     * Retrieve shipping methods return address data
     *
     * @param string $method
     * @param string $data
     * @param int|null $storeId
     * @return mixed
     */
    public function getReturnAddressData($method, $data, $storeId = null)
    {
        return $this->_scopeConfig->getValue(
            sprintf('makecommerce/%s_return/%s', $method, $data),
            ScopeInterface::SCOPE_STORE,
            $storeId ? $storeId : $this->_storeId
        );
    }

    /**
     * Retrieve API configuration values
     *
     * @param string $field
     * @param int|null $storeId
     * @return mixed
     */
    public function getApiValue($field, $storeId = null)
    {
        return $this->_scopeConfig->getValue(
            'makecommerce/ecommerce/' . $field,
            ScopeInterface::SCOPE_STORE,
            $storeId ? $storeId : $this->_storeId
        );
    }

    /**
     * Retrieve carrier configuration values
     *
     * @param string $carrier
     * @param string $field
     * @param int|null $storeId
     * @return mixed
     */
    public function getCarrierValue($carrier, $field, $storeId = null)
    {
        return $this->_scopeConfig->getValue(
            'carriers/' . $carrier . '/' . $field,
            ScopeInterface::SCOPE_STORE,
            $storeId ? $storeId : $this->_storeId
        );
    }

    /**
     * Overrides carrier name in order array with carriername from credentials array
     * 
     * @var array requestBody
     * @return array
     */
    public function overrideOrderCarrierWithCredentialCarrier($requestBody)
    {
        $carriers = array();
        //get carrier name from credentials
        foreach ($requestBody["credentials"] as $row) {
            $carriers[] = $row["carrier"];
        }

        //override order carrier with credential carrier
        foreach ($requestBody["orders"] as &$orderRow) {
            foreach ($carriers as $carrier) {
                if (substr($carrier, 0, strlen($orderRow["carrier"])) === $orderRow["carrier"]) {
                    $orderRow["carrier"] = $carrier;
                    break;
                }
            }
        }

        return $requestBody;
    }

    /**
     * Sets path pattern
     *
     * @param string $pathPattern
     * @return void
     */
    public function setPathPattern($pathPattern)
    {
        $this->_pathPattern = $pathPattern;
    }

    /**
     * Set store id
     *
     * @param int|string $storeId
     * @return $this
     */
    public function setStoreId($storeId)
    {
        $this->_storeId = (int)$storeId;
        return $this;
    }

    /**
     * Gets method code
     *
     * @return string
     */
    public function getMethodCode()
    {
        return $this->_methodCode;
    }

    /**
     * Sets method code
     *
     * @param string $methodCode
     * @return void
     */
    public function setMethodCode($methodCode)
    {
        $this->_methodCode = $methodCode;
    }

    /**
     * Map any supported payment method into a config path by specified field name
     *
     * @param string $fieldName
     * @return string|null
     */
    protected function _getSpecificConfigPath($fieldName)
    {
        if ($this->_pathPattern) {
            return sprintf($this->_pathPattern, $this->getMethodCode(), $fieldName);
        }

        return "payment/{$this->getMethodCode()}/{$fieldName}";
    }

    /**
     * Perform additional config value preparation and return new value if needed
     *
     * @param string $key Underscored key
     * @param string $value Old value
     * @return string Modified value or old value
     */
    protected function _prepareValue($key, $value)
    {
        return $value;
    }

    /**
     * Read the module version from composer.json file
     *
     * @return string
     */
    public function getModuleVersion()
    {
        $modulePath = $this->_componentRegistrar->getPath(
            \Magento\Framework\Component\ComponentRegistrar::MODULE,
            \Makecommerce\Ecommerce\Model\Makecommerce::MODULE_NAME
        );
        $moduleDir = $this->_readDirFactory->create($modulePath);

        if ($moduleDir->isExist('composer.json')) {
            $rawData = [];
            $composerFile = $moduleDir->readFile('composer.json');

            if ($composerFile) {
                $rawData = \Zend_Json::decode($composerFile);
            }

            return isset($rawData['version']) ? $rawData['version'] : '';
        }

        return '';
    }
}

<?php

namespace Makecommerce\Ecommerce\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

class InstallData implements InstallDataInterface
{
    /**
     * @var CategorySetupFactory
     */
    protected $categorySetupFactory;

    /**
     * @param CategorySetupFactory $categorySetupFactory
     */
    public function __construct(
        CategorySetupFactory $categorySetupFactory
    ) {
        $this->categorySetupFactory = $categorySetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);

        $entityTypeId = $categorySetup->getEntityTypeId(Product::ENTITY);

        foreach ($this->getCustomProductAttributes() as $attribute => $data) {
            $categorySetup->addAttribute($entityTypeId, $attribute, $data);
        }

        $setup->endSetup();
    }

    /**
     * Returns array of attributes
     *
     * @return array
     */
    protected function getCustomProductAttributes()
    {
        return [
            'allowed_for_parcel' => [
                'type' => 'int',
                'label' => __('Allow shipment to parcel machine'),
                'input' => 'boolean',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'searchable' => false,
                'filterable' => true,
                'unique' => false,
                'is_used_in_grid' => 1,
                'is_visible_in_grid' => 1,
                'default' => 1
            ],
            'free_shipping' => [
                'type' => 'int',
                'label' => __('Free shipping'),
                'input' => 'boolean',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'searchable' => false,
                'filterable' => true,
                'unique' => false,
                'used_in_product_listing' => false
            ]
        ];
    }
}

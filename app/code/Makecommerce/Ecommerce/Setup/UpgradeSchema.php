<?php
namespace Makecommerce\Ecommerce\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * Upgrade schema
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        //$setup->getConnection()->dropTable( $setup->getTable('makecommerce_ecommerce_transactions') );

        $table = $setup->getConnection()->newTable(
            $setup->getTable('makecommerce_ecommerce_transactions')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Transaction id'
        )->addColumn(
            'links',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            5000,
            [],
            'Extra links json'
        )->addColumn(
            'merchant_data',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Extra data as json { "quote_id": "70", "order_ids": [10000002]}'
        )->addColumn(
            'object',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Object type'
        )->addColumn(
            'payment_methods',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            5000,
            [],
            'Payment methods json'
        )->addColumn(
            'reference',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Quote id'
        )->addColumn(
            'status',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Status of transaction'
        )->addColumn(
            'type',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Status of transaction'
        )->addColumn(
            'amount',
            \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Amount'
        )->addColumn(
            'country',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            2,
            ['nullable' => false],
            'Country code'
        )->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_DATETIME,
            null,
            ['nullable' => false],
            'Created at'
        )->addColumn(
            'currency',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            3,
            ['nullable' => false],
            'Currency'
        )->addColumn(
            'customer',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            ['nullable' => false],
            'Customer id'
        )->addColumn(
            'order_ids',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            65000,
            [],
            'Order Ids'
        )->setComment(
            'Makecommerce transactions'
        );

        $locationTable = $setup->getConnection()->newTable(
            $setup->getTable('mc_shipping_locations')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'primary' => true],
            'Location identifier'
        )->addColumn(
            'type',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            3,
            ['nullable' => false],
            'Location type'
        )->addColumn(
            'name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Location name'
        )->addColumn(
            'city',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Location city'
        )->addColumn(
            'country',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            2,
            ['nullable' => false],
            'Location country'
        )->addColumn(
            'availability',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Availability'
        )->addColumn(
            'comment',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Comment'
        )->addColumn(
            'provider',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Provider'
        )->addColumn(
            'carrier',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Carrier name'
        )->addColumn(
            'address',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Location address'
        )->addColumn(
            'zip',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Postcode'
        )->setComment(
            'Makecommerce shipping method locations'
        );

        $shipmentResponseTable = $setup->getConnection()->newTable(
            $setup->getTable('mc_shipment_response')
        )->addColumn(
            'barcode',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'primary' => true],
            'Shipment barcode'
        )->addColumn(
            'order_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Order ID'
        )->addColumn(
            'carrier',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Carrier name'
        )->addColumn(
            'shipping_method',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Shipping method'
        )->addColumn(
            'label_url',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Label URL'
        )->addColumn(
            'manifest_url',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Manifest URL'
        )->setComment(
            'Shipment registering responses for Makecommerce shipping methods'
        );

        $setup->getConnection()->createTable($table);
        $setup->getConnection()->createTable($locationTable);
        $setup->getConnection()->createTable($shipmentResponseTable);

        $setup->getConnection()->addColumn(
            $setup->getTable('quote'),
            'location_id',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'comment' => 'Unique ID for shipping location for Makecommerce shipping methods'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'location_id',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'comment' => 'Unique ID for shipping location for Makecommerce shipping methods'
            ]
        );

        $setup->getConnection()->modifyColumn(
            $setup->getTable('quote'),
            'remote_ip',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 50
            ]
        );

        $setup->getConnection()->modifyColumn(
            $setup->getTable('sales_order'),
            'remote_ip',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 50
            ]
        );

        $setup->endSetup();
    }
}

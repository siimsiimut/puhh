<?php

namespace Makecommerce\Ecommerce\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
// use Magento\Framework\Serialize\SerializerInterface;

class UpgradeData implements UpgradeDataInterface
{

    /**
     * @var CategorySetupFactory
     */
    protected $categorySetupFactory;

    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    protected $productMetadata;

    // private $serializer;

    /**
     * @param CategorySetupFactory $categorySetupFactory
     * @param \Magento\Framework\App\ProductMetadataInterface $productMetadata
     */
    public function __construct(
        CategorySetupFactory $categorySetupFactory,
        // \Magento\Framework\Serialize\SerializerInterface $serializer,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata
    ) {
        $this->categorySetupFactory = $categorySetupFactory;
        $this->productMetadata = $productMetadata;
        // $this->serializer = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.0.3', '<')) {

            $this->changeApiPaths($setup);

            $this->addProductAttributes($setup);
        }
        if (version_compare($this->productMetadata->getVersion(), '2.2', '>=')) {
            $this->changeSerializedToJsonEncoded($setup);
        }
        $setup->endSetup();
    }

    /**
     * Changes serialized data to json_encoded
     *
     * @param  ModuleDataSetupInterface $setup
     * @return void
     */
    protected function changeSerializedToJsonEncoded($setup)
    {
        $paths = [
            'carriers/smartpostmc_parcel/configuration',
            'carriers/smartpostmc_courier/configuration',
            'carriers/omnivamc_parcel/configuration',
            'carriers/omnivamc_courier/configuration',
            'carriers/dpdmc_parcel/configuration'
        ];

        $connection = $setup->getConnection();
        $coreTable = $connection->getTableName('core_config_data');

        $oldValues = $connection->fetchAll(
            $connection->select()
                ->from($coreTable)
                ->where('path IN (?)', $paths)
        );

        $newValues = [];
        foreach ($oldValues as $row) {
            if (isset($row['config_id'])) {
                $decoded = json_decode($row['value']);
                //if decoded return null, value is serialized and need to be converted
                if (!is_array($decoded) && !$decoded) {
                    $newValues[$row['config_id']] = $row['value'];
                }
            }
        }

        $mapped = [];

        foreach ($newValues as $id => $value) {
            // $mapped[$id] = json_encode($this->serializer->unserialize($value));
            $mapped[$id] = json_encode(json_decode($value, true));
        }

        foreach ($mapped as $id => $value) {
            $sql = "UPDATE `$coreTable` SET value = '$value' WHERE config_id = $id";
            $connection->query($sql);
        }
    }

    /**
     * Replaces old API data paths with new ones and deletes old data
     *
     * @param ModuleDataSetupInterface $setup
     * @return void
     */
    protected function changeApiPaths($setup)
    {
        $pathsToChange = [
            'payment/makecommerce/api_test' => 'makecommerce/ecommerce/environment',
            'payment/makecommerce/shop_id' => 'makecommerce/ecommerce/shop_id',
            'payment/makecommerce/api_secret' => 'makecommerce/ecommerce/api_secret',
            'payment/makecommerce/api_public' => 'makecommerce/ecommerce/api_public',
            'payment/makecommerce/test_shop_id' => 'makecommerce/ecommerce/test_shop_id',
            'payment/makecommerce/test_key_secret' => 'makecommerce/ecommerce/test_key_secret',
            'payment/makecommerce/test_key_public' => 'makecommerce/ecommerce/test_key_public'
        ];

        $connection = $setup->getConnection();
        $coreConfigTable = $connection->getTableName('core_config_data');

        foreach ($pathsToChange as $oldPath => $newPath) {
            $updateSql = "UPDATE `$coreConfigTable` SET path = '$newPath' WHERE path = '$oldPath'";
            $deleteSql = "DELETE FROM `$coreConfigTable` WHERE path = '$oldPath'";
            $connection->query($updateSql);
            $connection->query($deleteSql);
        }
    }

    /**
     * Adds allowed_for_parcel and free_shipping attributes to product
     *
     * @param ModuleDataSetupInterface $setup
     * @return void
     */
    protected function addProductAttributes($setup)
    {
        $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);

        $entityTypeId = $categorySetup->getEntityTypeId(Product::ENTITY);

        foreach ($this->getCustomProductAttributes() as $attribute => $data) {
            $categorySetup->addAttribute($entityTypeId, $attribute, $data);
        }
    }

    /**
     * Returns array of attributes
     *
     * @return array
     */
    protected function getCustomProductAttributes()
    {
        return [
            'allowed_for_parcel' => [
                'type' => 'int',
                'label' => __('Allow shipment to parcel machine'),
                'input' => 'boolean',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'searchable' => false,
                'filterable' => true,
                'unique' => false,
                'is_used_in_grid' => 1,
                'is_visible_in_grid' => 1,
                'default' => 1
            ],
            'free_shipping' => [
                'type' => 'int',
                'label' => __('Free shipping'),
                'input' => 'boolean',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'searchable' => false,
                'filterable' => true,
                'unique' => false,
                'used_in_product_listing' => false
            ]
        ];
    }
}

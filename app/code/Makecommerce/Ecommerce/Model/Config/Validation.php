<?php

namespace Makecommerce\Ecommerce\Model\Config;

use Magento\Config\Model\Config\Backend\Serialized\ArraySerialized;
use Magento\Framework\Exception\LocalizedException;
use Makecommerce\Ecommerce\Block\System\Config\Form\Configuration;

class Validation extends ArraySerialized
{
    /**
     * Validate value
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * if there is no field value, search value is empty or regular expression is not valid
     */
    public function beforeSave()
    {
        $values = $this->getValue();

        foreach ($values as $rowKey => $row) {

            if ($rowKey === '__empty') {
                continue;
            }

            if (empty($row[Configuration::COUNTRY_FIELD])) {
                throw new LocalizedException(
                    __('Country field must not be empty')
                );
            }

            if (!is_numeric($row[Configuration::PRICE_FIELD]) &&
                !is_numeric($row[Configuration::FREE_SHIPPING_FIELD])) {
                if (!(ctype_space($row[Configuration::PRICE_FIELD]) || empty($row[Configuration::PRICE_FIELD]))) {
                    throw new LocalizedException(
                        __('Price field must be a numerical value')
                    );
                }
                if (!( ctype_space($row[Configuration::FREE_SHIPPING_FIELD]) ||
                             empty($row[Configuration::FREE_SHIPPING_FIELD]))) {
                    throw new LocalizedException(
                        __('Free Shipping field must be a numerical value')
                    );
                }
            }
        }
        return parent::beforeSave();
    }
}

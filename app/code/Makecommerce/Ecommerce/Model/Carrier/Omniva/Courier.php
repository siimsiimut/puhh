<?php

namespace Makecommerce\Ecommerce\Model\Carrier\Omniva;

use Makecommerce\Ecommerce\Model\Carrier\Courier as DefaultCourier;
use Makecommerce\Ecommerce\Model\Carrier\AbstractCarrier;

class Courier extends DefaultCourier
{
    /**
     * @var string
     */
    protected $_code = AbstractCarrier::CARRIER_OMNIVA;

    /**
     * @var string
     */
    protected $_method = AbstractCarrier::METHOD_COURIER;

    /**
     * @var string
     */
    protected $serviceType = 'QK';

    /**
     * @var \Makecommerce\Ecommerce\Model\System\Config\Source\Countries\Omniva
     */
    protected $countries;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Xml\Security $xmlSecurity
     * @param \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory
     * @param \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory
     * @param \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Directory\Helper\Data $directoryData
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Makecommerce\Ecommerce\Config $makecommerceConfig
     * @param \Makecommerce\Ecommerce\Model\ResourceModel\Location\CollectionFactory $locationCollectionFactory
     * @param \Makecommerce\Ecommerce\Model\LocationFactory $locationFactory
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Sales\Api\ShipmentRepositoryInterface $shipmentRepository
     * @param \Makecommerce\Ecommerce\Logger\Logger $mcLogger
     * @param \Makecommerce\Ecommerce\Client\MakecommerceClientWrapper $api
     * @param \Makecommerce\Ecommerce\Model\ResourceModel\ShipmentResponse\CollectionFactory
     *                         $shipmentResponseCollectionFactory
     * @param \Makecommerce\Ecommerce\Model\ShipmentResponseFactory $shipmentResponseFactory
     * @param \Makecommerce\Ecommerce\Helper\Data $makecommerceHelper
     * @param \Makecommerce\Ecommerce\Model\System\Config\Source\Countries\Omniva $omnivaCountries
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Xml\Security $xmlSecurity,
        \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory,
        \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory,
        \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Directory\Helper\Data $directoryData,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Makecommerce\Ecommerce\Config $makecommerceConfig,
        \Makecommerce\Ecommerce\Model\ResourceModel\Location\CollectionFactory $locationCollectionFactory,
        \Makecommerce\Ecommerce\Model\LocationFactory $locationFactory,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Api\ShipmentRepositoryInterface $shipmentRepository,
        \Makecommerce\Ecommerce\Logger\Logger $mcLogger,
        \Makecommerce\Ecommerce\Client\MakecommerceClientWrapper $api,
        \Makecommerce\Ecommerce\Model\ResourceModel\ShipmentResponse\CollectionFactory
        $shipmentResponseCollectionFactory,
        \Makecommerce\Ecommerce\Model\ShipmentResponseFactory $shipmentResponseFactory,
        \Makecommerce\Ecommerce\Helper\Data $makecommerceHelper,
        \Makecommerce\Ecommerce\Model\System\Config\Source\Countries\Omniva $countries,
        array $data = []
    ) {
        $this->countries = $countries;
        parent::__construct(
            $scopeConfig,
            $rateErrorFactory,
            $logger,
            $xmlSecurity,
            $xmlElFactory,
            $rateResultFactory,
            $rateMethodFactory,
            $trackFactory,
            $trackErrorFactory,
            $trackStatusFactory,
            $regionFactory,
            $countryFactory,
            $currencyFactory,
            $directoryData,
            $stockRegistry,
            $productRepository,
            $makecommerceConfig,
            $locationCollectionFactory,
            $locationFactory,
            $orderRepository,
            $shipmentRepository,
            $mcLogger,
            $api,
            $shipmentResponseCollectionFactory,
            $shipmentResponseFactory,
            $makecommerceHelper,
            $data
        );
    }

    /**
     * Get shipping method's countries
     *
     * @return array
     */
    public function getCountries()
    {
        return $this->countries->toOptionArray();
    }

    /**
     * Gets service type
     *
     * @return string
     */
    protected function getServiceType()
    {
        //TODO: make this configurable
        // return ['serviceType' => $this->serviceType];
        return ['serviceType' => $this->getConfigData('handover_method')];
    }

    /**
     * {@inheritdoc}
     */
    protected function areApiFieldsFilled()
    {
        $user = $this->makecommerceConfig->getApiValue('omniva_user');
        $pswd = $this->makecommerceConfig->getApiValue('omniva_password');
        if (!$user || !$pswd) {
            return false;
        }
        return parent::areApiFieldsFilled();
    }

    /**
     * Gets shipping credentials
     *
     * @return array
     */
    public function getCredentials()
    {
        $username = $this->makecommerceConfig->getApiValue('omniva_user');
        $password = $this->makecommerceConfig->getApiValue('omniva_password');
        $carrierName = $this->getNameFromCode($this->_code);

        if (!empty($this->makecommerceConfig->getApiValue('omniva_integration_country'))) {
            $carrierName = $this->makecommerceConfig->getApiValue('omniva_integration_country');
        }

        return [
            'carrier' => $carrierName,
            'username' => $username,
            'password' => $password
        ];
    }
}

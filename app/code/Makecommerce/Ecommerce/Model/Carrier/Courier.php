<?php

namespace Makecommerce\Ecommerce\Model\Carrier;

use Makecommerce\Ecommerce\Model\Carrier\AbstractCarrier;

class Courier extends AbstractCarrier
{
    /**
     * Creates shipping destination data
     *
     * @param \Magento\Sales\Model\Order $order
     * @return array
     */
    protected function getDestinationData($order)
    {
        $shippingAddress = $order->getShippingAddress();
        $streetData = $shippingAddress->getStreet();
        $street = $streetData[0];
        if (count($streetData) > 1) {
            $street .= ' ' . $streetData[1];
        }
        return [
            'postalCode' => $shippingAddress->getPostcode(),
            'country' => $shippingAddress->getCountryId(),
            'county' => $shippingAddress->getRegion(),
            'city' => $shippingAddress->getCity(),
            'street' => $street
        ];
    }
}

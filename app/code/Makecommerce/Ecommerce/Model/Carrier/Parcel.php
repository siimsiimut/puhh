<?php

namespace Makecommerce\Ecommerce\Model\Carrier;

use Makecommerce\Ecommerce\Model\Carrier\AbstractCarrier;
use Makecommerce\Ecommerce\Api\Data\LocationInterface;

class Parcel extends AbstractCarrier
{
    /**
     * Checks if method is allowed for checkout
     *
     * @param RateRequest $request
     * @return bool
     */
    public function isMethodAllowed($request)
    {
        $items = $request->getAllItems();
        foreach ($items as $item) {
            if ($item->isDeleted()) {
                continue;
            }
            $product = $this->productRepository->getById($item->getProductId());
            if (!$product->getAllowedForParcel()) {
                return false;
            }
        }
        return parent::isMethodAllowed($request);
    }

    /**
     * Gets methods locations
     *
     * @return array
     */
    public function getLocations()
    {
        $carrier = $this->getNameFromCode($this->_code);
        $countries = $this->getCountries();
        $locations = [];

        foreach ($countries as $countryData) {
            $countryCode = $countryData['value'];
            $countryLabel = $countryData['label'];

            $collection = $this->locationCollectionFactory->create()
                ->addFieldToFilter(LocationInterface::CARRIER, ['eq' => $carrier])
                ->addFieldToFilter(LocationInterface::COUNTRY, ['eq' => $countryCode])
                ->addFieldToFilter(LocationInterface::TYPE, ['eq' => $this->getLocationType()])
                ->setOrder('country', 'ASC')
                ->setOrder('city', 'DESC')
                ->setOrder('name', 'DESC');

            $locations = array_merge(
                $locations,
                $this->sortLocationsByImportance($collection, $countryCode, $countryLabel)
            );
        }
        return ['locations' => $locations];
    }

    /**
     * Orders locations by importance
     *
     * @param \Makecommerce\Ecommerce\Model\ResourceModel\Location\Collection $collection
     * @param string $countryCode
     * @param string $countryLabel
     */
    protected function sortLocationsByImportance($collection, $countryCode, $countryLabel)
    {
        $priorityQueue = new \SplPriorityQueue();
        foreach ($collection as $location) {
            $priorityQueue->insert($location, $this->getPriority($location->getCity()));
        }
        $countryGroup = [];
        $areaGroup = [];
        $area = '';
        foreach ($priorityQueue as $location) {
            if ($area !== $location->getCity()) {
                if (!empty($areaGroup)) {
                    $countryGroup[] = [
                        'groupName' => $area,
                        'groupCountry' => $countryCode,
                        'locations' => $areaGroup
                    ];
                    $areaGroup = [];
                }
                $area = $location->getCity();
            }
            $areaGroup[] = [
                'location_id' => $location->getId(),
                'name' => $location->getName()
            ];
        }
        $countryGroup[] = [
            'groupName' => $area !== 'NULL' ? $area : $countryLabel,
            'groupCountry' => $countryCode,
            'locations' => $areaGroup
        ];
        array_unshift(
            $countryGroup,
            [
                'groupName' => '',
                'groupCountry' => $countryCode,
                'locations' => [
                    [
                        'location_id' => '__empty',
                        'name' => __('Please choose a location!')
                    ]
                ]
            ]
        );
        if (isset($countryGroup[1])) {
            $t = $countryGroup[1];
            unset($countryGroup[1]);
            $countryGroup[] = $t;
        }
        return $countryGroup;
    }

    /**
     * Creates shipping destination data
     *
     * @param \Magento\Sales\Model\Order $order
     * @return array
     */
    protected function getDestinationData($order)
    {
        return ['destinationId' => $order->getLocationId()];
    }
}

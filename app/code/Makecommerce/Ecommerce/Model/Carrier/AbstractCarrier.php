<?php

namespace Makecommerce\Ecommerce\Model\Carrier;

use Magento\Shipping\Model\Carrier\AbstractCarrierOnline;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\DataObject;
use Makecommerce\Ecommerce\Block\System\Config\Form\Configuration;

abstract class AbstractCarrier extends AbstractCarrierOnline implements CarrierInterface
{
    const CARRIER_OMNIVA = 'omnivamc';
    const CARRIER_SMARTPOST = 'smartpostmc';
    const CARRIER_DPD = 'dpdmc';
    const METHOD_PARCEL = 'parcel';
    const METHOD_COURIER = 'courier';

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Makecommerce\Ecommerce\Config
     */
    protected $makecommerceConfig;

    /**
     * @var \Makecommerce\Ecommerce\Model\ResourceModel\Location\CollectionFactory
     */
    protected $locationCollectionFactory;

    /**
     * @var \Makecommerce\Ecommerce\Model\LocationFactory
     */
    protected $locationFactory;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var \Magento\Sales\Api\ShipmentRepositoryInterface
     */
    protected $shipmentRepository;

    /**
     * @var \Makecommerce\Ecommerce\Logger\Logger
     */
    protected $mcLogger;

    /**
     * @var \Makecommerce\Ecommerce\Client\MakecommerceClientWrapper
     */
    protected $api;

    /**
     * @var \Makecommerce\Ecommerce\Model\ResourceModel\ShipmentResponse\CollectionFactory
     */
    protected $shipmentResponseCollectionFactory;

    /**
     * @var \Makecommerce\Ecommerce\Model\ShipmentResponseFactory
     */
    protected $shipmentResponseFactory;

    /**
     * @var \Makecommerce\Ecommerce\Helper\Data
     */
    protected $makecommerceHelper;

    /**
     * The ShipmentFactory is used to create a new Shipment.
     *
     * @var \Magento\Sales\Model\Order\ShipmentFactory
     */
    protected $shipmentFactory;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Xml\Security $xmlSecurity
     * @param \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory
     * @param \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory
     * @param \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Directory\Helper\Data $directoryData
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Makecommerce\Ecommerce\Config $makecommerceConfig
     * @param \Makecommerce\Ecommerce\Model\ResourceModel\Location\CollectionFactory $locationCollectionFactory
     * @param \Makecommerce\Ecommerce\Model\LocationFactory $locationFactory
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Sales\Api\ShipmentRepositoryInterface $shipmentRepository
     * @param \Makecommerce\Ecommerce\Logger\Logger $mcLogger
     * @param \Makecommerce\Ecommerce\Client\MakecommerceClientWrapper $api
     * @param \Makecommerce\Ecommerce\Model\ResourceModel\ShipmentResponse\CollectionFactory $shipmentResponseCollectionFactory
     * @param \Makecommerce\Ecommerce\Model\ShipmentResponseFactory $shipmentResponseFactory
     * @param \Makecommerce\Ecommerce\Helper\Data $makecommerceHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Xml\Security $xmlSecurity,
        \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory,
        \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory,
        \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Directory\Helper\Data $directoryData,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Makecommerce\Ecommerce\Config $makecommerceConfig,
        \Makecommerce\Ecommerce\Model\ResourceModel\Location\CollectionFactory
        $locationCollectionFactory,
        \Makecommerce\Ecommerce\Model\LocationFactory $locationFactory,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Api\ShipmentRepositoryInterface $shipmentRepository,
        \Makecommerce\Ecommerce\Logger\Logger $mcLogger,
        \Makecommerce\Ecommerce\Client\MakecommerceClientWrapper $api,
        \Makecommerce\Ecommerce\Model\ResourceModel\ShipmentResponse\CollectionFactory
        $shipmentResponseCollectionFactory,
        \Makecommerce\Ecommerce\Model\ShipmentResponseFactory $shipmentResponseFactory,
        \Makecommerce\Ecommerce\Helper\Data $makecommerceHelper,
        array $data = []
    ) {
        $this->productRepository = $productRepository;
        $this->makecommerceConfig = $makecommerceConfig;
        $this->locationCollectionFactory = $locationCollectionFactory;
        $this->locationFactory = $locationFactory;
        $this->orderRepository = $orderRepository;
        $this->shipmentRepository = $shipmentRepository;
        $this->mcLogger = $mcLogger;
        $this->api = $api;
        $this->shipmentResponseCollectionFactory = $shipmentResponseCollectionFactory;
        $this->shipmentResponseFactory = $shipmentResponseFactory;
        $this->makecommerceHelper = $makecommerceHelper;
        $this->container = \Magento\Framework\App\ObjectManager::getInstance();
        $this->locale = $this->container->get('\Magento\Framework\Locale\Resolver');
        $this->fileFactory = $this->container->get('\Magento\Framework\App\Response\Http\FileFactory');
        $this->orderInterface = $this->container->get('\Magento\Sales\Api\Data\OrderInterface');
        $this->shipmentFactory = $this->container->get('\Magento\Sales\Model\Order\ShipmentFactory');
        $this->trackingFactory = $this->container->get('\Magento\Sales\Model\Order\Shipment\TrackFactory');
        $this->commentInterface = $this->container->get('\Magento\Sales\Api\Data\ShipmentCommentCreationInterface');
        $this->shipOrderService = $this->container->get('\Magento\Sales\Model\ShipOrder');
        $this->orderConverter = $this->container->get('\Magento\Sales\Model\Convert\Order');
        // $this->carrierHelper = $this->container->get('\Makecommerce\Ecommerce\Helper\Carrier');
        $this->_redirect = $this->container->get('\Magento\Framework\App\Response\RedirectInterface');
        parent::__construct(
            $scopeConfig,
            $rateErrorFactory,
            $logger,
            $xmlSecurity,
            $xmlElFactory,
            $rateResultFactory,
            $rateMethodFactory,
            $trackFactory,
            $trackErrorFactory,
            $trackStatusFactory,
            $regionFactory,
            $countryFactory,
            $currencyFactory,
            $directoryData,
            $stockRegistry,
            $data
        );
    }

    /**
     * Return code of carrier
     *
     * @return string
     */
    public function getCarrierCode()
    {
        return isset($this->_code) && isset($this->_method)
            ? sprintf('%s_%s', $this->_code, $this->_method)
            : null;
    }

    /**
     * Retrieve information from carrier configuration
     *
     * @param   string $field
     * @return  void|false|string
     */
    public function getConfigData($field)
    {
        if (!$this->getCarrierCode()) {
            return false;
        }
        $path = sprintf('carriers/%s/%s', $this->getCarrierCode(), $field);

        return $this->_scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStore()
        );
    }

    /**
     * Retrieve config flag for store by field
     *
     * @param string $field
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     * @api
     */
    public function getConfigFlag($field)
    {
        if (!$this->getCarrierCode()) {
            return false;
        }
        $path = sprintf('carriers/%s/%s', $this->getCarrierCode(), $field);

        return $this->_scopeConfig->isSetFlag(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->getStore()
        );
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        return [$this->getCarrierCode() => $this->getConfigData('title')];
    }

    /**
     * @param RateRequest $request
     * @return bool|Result
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active') || !$this->isMethodAllowed($request)) {
            return false;
        }

        $result = $this->_rateFactory->create();
        $method = $this->_rateMethodFactory->create();

        $method->setCarrier($this->_code);
        $method->setCarrierTitle($this->getConfigData('title'));

        $method->setMethod($this->_method);
        $method->setMethodTitle($this->getConfigData('method_title'));

        $price = $this->getPrice($request);
        $method->setPrice($price);
        $method->setCost($price);

        $result->append($method);

        return $result;
    }

    /**
     * Checks if method is allowed for checkout.
     * Override if additional validation is needed
     *
     * @param RateRequest $request
     * @return bool
     */
    public function isMethodAllowed($request)
    {
        $shippingCountry = $request->getDestCountryId();
        $methodConfig = $this->makecommerceHelper->unserializeConfig(
            $this->makecommerceConfig->getCarrierValue($this->getCarrierCode(), 'configuration')
        );
        if ($methodConfig) {
            foreach ($methodConfig as $config) {
                if ($config[Configuration::COUNTRY_FIELD] === $shippingCountry) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks if any item product has free_shipping attribute
     *
     * @param array $items
     * @return bool
     */
    protected function hasFreeShippingProduct($items)
    {
        foreach ($items as $item) {
            if ($item->isDeleted()) {
                continue;
            }
            $product = $this->productRepository->getById($item->getProductId());
            if ($product->getFreeShipping()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets shipping method price
     *
     * @param RateRequest $request
     * @return float|null
     */
    public function getPrice($request)
    {
        if ($this->hasFreeShippingProduct($request->getAllItems())) {
            return 0;
        }
        $subtotal = $request->getPackageValue();
        $methodConfig = $this->makecommerceHelper->unserializeConfig(
            $this->makecommerceConfig->getCarrierValue($this->getCarrierCode(), 'configuration')
        );
        if ($methodConfig) {
            $shippingCountry = $request->getDestCountryId();
            foreach ($methodConfig as $config) {
                if ($config[Configuration::COUNTRY_FIELD] === $shippingCountry) {
                    $freeShippingAmount = $config[Configuration::FREE_SHIPPING_FIELD];
                    if (!empty($freeShippingAmount) && $subtotal > $freeShippingAmount) {
                        $request->setFreeShipping(true);
                        return 0;
                    } else {
                        return $config[Configuration::PRICE_FIELD];
                    }
                }
            }
        }
        return $this->getConfigData('price');
    }

    /**
     * Sends shipment request to carrier web service
     *
     * @param \Magento\Framework\DataObject $request
     * @return \Magento\Framework\DataObject
     */
    protected function _doShipmentRequest(\Magento\Framework\DataObject $request)
    {
    }

    /**
     * Additional validators check
     *
     * @param \Magento\Framework\DataObject $request
     * @return bool
     */
    public function proccessAdditionalValidation(\Magento\Framework\DataObject $request)
    {
        return true;
    }

    /**
     * Registers shipment based on order
     *
     * @var int[] $orderIds
     * @return DataObject
     */
    public function sendShipmentData($orderIds)
    {
        $result = new DataObject();
        if ($this->areApiFieldsFilled()) {
            $orders = [];
            foreach ($orderIds as $orderId) {
                $order = $this->orderRepository->get($orderId);
                $orders[$orderId] = $order;
            }

            $requestBody = $this->createShipmentRequestBody($orders);

            //set the correct carrier for orders
            $requestBody = $this->makecommerceConfig->overrideOrderCarrierWithCredentialCarrier($requestBody);   

            try {
                $response = $this->api->createShipments($requestBody);
            } catch (\Exception $e) {
                $result->setError(true);
                $result->setErrorMessage($e->getMessage());
                $this->mcLogger->error($e->getMessage());
                return $result;
            }
            // error_log("response: " . json_encode($response) );
            $response = json_decode(json_encode($response), true);
            $this->saveShipmentResponse($response);

            $result->setError(false);
            $order->addStatusHistoryComment(__('Sent shipping info to carrier.'));
            $order->setCanShipPartially('0');
            $order->setCanShipPartiallyItem('0');
            $order->addStatusHistoryComment(__('Disabled partial shipping.'));
            $order->save();
        } else {
            $result->setError(true);
            $message = __(sprintf('Could not send shipment data to %s, because API fields are not filled in.', $this->getCarrierCode()));
            $this->mcLogger->error($message);
            $result->setErrorMessage($message);
        }
        return $result;
    }

    /**
     * Checks that api fields in config are not empty
     *
     * @return bool
     */
    protected function areApiFieldsFilled()
    {
        //TODO: replace strings
        $isTest = $this->makecommerceConfig->getApiValue('environment') === 'test';
        $shopId = $this->makecommerceConfig->getApiValue($isTest ? 'test_shop_id' : 'shop_id');
        $secretKey = $this->makecommerceConfig->getApiValue($isTest ? 'test_key_secret' : 'api_secret');
        $publicKey = $this->makecommerceConfig->getApiValue($isTest ? 'test_key_public' : 'api_public');
        return $shopId && $secretKey && $publicKey;
    }

    /**
     * Creates request body for registering shipments
     *
     * @param array $orders
     * @return array
     */
    protected function createShipmentRequestBody($orders)
    {
        return [
            'credentials' => [$this->getCredentials()],
            'orders' => $this->getOrderDataList($orders)
        ];
    }

    /**
     * Creates array with order data
     * This data is sent to api
     *
     * @param array $orders
     * @return array
     */
    protected function getOrderDataList($orders)
    {
        $data = [];
        foreach ($orders as $order) {
            $data[] = $this->getOrderData($order);
        }
        return $data;
    }

    /**
     * Creates array with order data
     *
     * @param \Magento\Sales\Model\Order $order
     * @return array
     */
    protected function getOrderData($order)
    {
        $carrierCode = $order->getShippingMethod(true)->getCarrierCode();
        $data = [
            'orderId' => $order->getId(),
            'carrier' => $this->getNameFromCode($carrierCode),
            'destination' => $this->getDestinationData($order),
            'recipient' => $this->getRecipientData($order),
            'sender' => $this->getSenderData($carrierCode)
        ];
        if ($carrierCode === self::CARRIER_OMNIVA) {
            $data['services'] = $this->getServiceType();
        }
        return $data;
    }

    /**
     * Gets return address for carrier
     *
     * @param string $carrierName
     * @return array
     */
    protected function getSenderData($carrierName)
    {
        $senderData = [
            'name' => $this->makecommerceConfig->getReturnAddressData($carrierName, 'name'),
            'phone' => $this->makecommerceConfig->getReturnAddressData($carrierName, 'phone'),
            'email' => $this->makecommerceConfig->getReturnAddressData($carrierName, 'email'),
            'postalCode' => $this->makecommerceConfig->getReturnAddressData($carrierName, 'postcode')
        ];
        if ($carrierName === self::CARRIER_OMNIVA) {
            $senderData['country'] = $this->makecommerceConfig->getReturnAddressData($carrierName, 'country');
            $senderData['city'] = $this->makecommerceConfig->getReturnAddressData($carrierName, 'city');
            $senderData['street'] = $this->makecommerceConfig->getReturnAddressData($carrierName, 'street');
            // $senderData['pickUpTimeStart'] = $this->getPickUpTime(
            //     $this->makecommerceConfig->getReturnAddressData($carrierName, 'pickupstart')
            // );
            // $senderData['pickUpTimeFinish'] = $this->getPickUpTime(
            //     $this->makecommerceConfig->getReturnAddressData($carrierName, 'pickupfinish')
            // );
        }
        return $senderData;
    }

    /**
     * Gets pickuptime in correct format
     *
     * @param string $hours
     * @return string
     */
    private function getPickUpTime($hours)
    {
        $nextDayDate = date('Y-m-d', strtotime(' +1 day'));
        if (!$hours) {
            $hours = '00:00:00';
        }
        $hourDate = null;
        $colonCount = substr_count($hours, ':');
        if ($colonCount === 0) {
            $hourDate = substr($hours, 0, 2) . ':00:00';
        } elseif ($colonCount === 1) {
            $hourDate = $hours . ':00';
        } elseif ($colonCount === 2) {
            $hourDate = $hours;
        }
        return !$hourDate ? $nextDayDate . 'T00:00:00' : $nextDayDate . 'T' . $hourDate;
    }

    /**
     * Gets recipient data from order
     *
     * @param \Magento\Sales\Model\Order $order
     * @return array
     */
    protected function getRecipientData($order)
    {
        $shippingAddress = $order->getShippingAddress();
        return [
            'name' => $shippingAddress->getName(),
            'phone' => $shippingAddress->getTelephone(),
            'email' => $shippingAddress->getEmail()
        ];
    }

    /**
     * Saves response data that is returned when registering shipment
     *
     * @param array $data
     * @return void
     */
    protected function saveShipmentResponse($data)
    {
        //TODO: test if there can be more than one manifest, and if so how to know for which carrier
        $manifestUrl =
            array_key_exists('manifests', $data) && !empty($data['manifests']) ? $data['manifests'][0] : null;
        if (array_key_exists('shipments', $data)) {
            $shipments = $data['shipments'];
        } else {
            $shipments = $data;
        }

        foreach ($shipments as $shipment)
        {
            if (!isset($shipment['shipmentId'])) {
                error_log("ShipmentId missing for order " . $shipment['orderId']);
                continue;
            }

            $shipmentResponse = $this->shipmentResponseFactory->create();
            $shipmentResponse
                ->setOrderId($shipment['orderId'])
                ->setCarrier($this->getNameFromCode($this->_code))
                ->setShippingMethod($this->getCarrierCode())
                ->setManifestUrl($manifestUrl)
                ->setBarcode($shipment['shipmentId'])
                ->save();
            $this->createShipment(
                $shipment['orderId'],
                [],
                'test comment',
                false,
                true,
                $shipment['shipmentId'],
                $this->getCarrierCode(),
                $this->getNameFromCode($this->_code)
            );
        }
    }

    /**
     * Retrieves shipping location's name
     *
     * @param string $locationId
     * @return string
     */
    public function getLocationName($locationId)
    {
        $location = $this->locationFactory->create()->load($locationId);
        return $location->getName();
    }

    /**
     * Creates request body for print label request based on order
     *
     * @param \Magento\Sales\Model\Order $order
     * @return array
     */
    public function getPrintLabelRequestBody($order)
    {
        $carrierCode = $order->getShippingMethod(true)->getCarrierCode();
        $data = [
            'orderId' => $order->getId(),
            'carrier' => $this->getNameFromCode($carrierCode),
            'destination' => $this->getDestinationData($order),
            'recipient' => $this->getRecipientData($order),
            'sender' => $this->getSenderData($carrierCode),
            'shipmentId' => $this->getOrderBarcode($order->getId())
        ];

        return $data;
    }

    /**
     * Gets barcode associated with order
     *
     * @param int $orderId
     * @return string
     */
    protected function getOrderBarcode($orderId)
    {
        $shipmentResponses = $this->shipmentResponseCollectionFactory->create()
            ->addFieldToFilter('order_id', ['eq' => $orderId]);
        return $shipmentResponses->count() > 0 ? $shipmentResponses->getFirstItem()->getBarcode() : null;
    }

    /**
     * Gets carrier name from carrier code
     * (removes mc from the end and uppercases it)
     *
     * @param string $code
     * @return string
     */
    protected function getNameFromCode($code)
    {
        return strtoupper(substr($code, 0, -2));
    }

    /**
     * Prepares tracking data form tracking number.
     *
     * @param $trackingNumber
     *
     * @return \Magento\Sales\Model\Order\Shipment\Track
     */
    protected function setTrackingData($trackingNumber, $shipmentMethod, $shipmentTitle)
    {
        $track = $this->trackingFactory->create();
        $track->setTrackNumber($trackingNumber);
        $track->setCarrierCode($shipmentMethod);
        $track->setTitle($shipmentTitle);
        $trackInfo[] = $track;
        return $trackInfo;
    }

    /**
     * @param string $comment
     *
     * @return \Magento\Sales\Api\Data\ShipmentCommentCreationInterface
     */
    protected function setShipmentComment($comment)
    {
        //comment can not be empty
        $comment = !empty($comment) ? $comment : 'Not Available';

        return $this->commentInterface->setComment($comment);
    }

    public function getTrackingInfo($trackingNumber)
    {
        $service = $this->getCarrierCode();
        $lang = substr($this->locale->getLocale(), 0, 2);
        $carrier = $this->getCarrierCode();

        $tracking = $this->_trackStatusFactory->create();

        if (isset($trackingNumber)) {
            if (substr($service, 0, 8) == 'omnivamc') {
                if ($lang == 'et') {
                    $url = 'https://www.omniva.ee/abi/jalgimine?barcode='.$trackingNumber;
                } elseif ($lang == 'ru') {
                    $url = 'https://www.omniva.ee/chastnyj/otslezhivanie_posylki?barcode='.$trackingNumber;
                } elseif ($lang == 'lv') {
                    $url = 'https://www.omniva.lv/privats/sutijuma_atrasanas_vieta?barcode='.$trackingNumber;
                } elseif ($lang == 'lt') {
                    $url = 'https://www.omniva.lt/verslo/siuntos_sekimas?barcode='.$trackingNumber;
                } else {
                    $url = 'https://www.omniva.ee/private/track_and_trace?barcode='.$trackingNumber;
                }
            } elseif (substr($service, 0, 11) == 'smartpostmc') {
                $url = 'https://uus.smartpost.ee/saadetise-otsing?barcode='.$trackingNumber;
            } elseif (substr($service, 0, 5) == 'dpdmc') {
                $url = 'https://tracking.dpd.de/status/'.$this->locale->getLocale().'/parcel/'.$trackingNumber;
            } else {
                $url=null;
            }
        } else {
            $url = null;
        }
        $tracking->setData([
            'carrier' => $this->_code,
            'carrier_title' => $this->getConfigData('title'),
            'tracking' => $trackingNumber,
            'url' => $url,
        ]);
        return $tracking;
    }

    protected function createShipment(
        $orderId,
        array $items,
        $comment = null,
        $notify = false,
        $includeComment = false,
        $trackingNumber = '',
        $trackingMethod = 'custom',
        $trackingTitle = ''
    )
    {
        $order = $this->orderRepository->get($orderId);
        if ($order->canShip()) {
            try {
                // $orderId = $order->getId();
                $tracks = $this->setTrackingData($trackingNumber, $trackingMethod, $trackingTitle);
                $comment = $this->setShipmentComment($comment);
                $shippedItems = $this->createShipmentItems($items, $order);
                $shipmentId = $this->shipOrderService->execute(
                    $orderId,
                    $shippedItems,
                    $notify,
                    $includeComment,
                    $comment,
                    $tracks
                );
            } catch (\Exception $e) {
                error_log($e->getMessage());
            }
            $orderIds[]=$orderId;
            $this->printLabels($orderIds);
            return $shipmentId;
        }
        return null;
    }

    /**
     * Create shipment items required to create shipment.
     *
     * @param array                      $items
     * @param \Magento\Sales\Model\Order $order
     *
     * @return array
     */
    protected function createShipmentItems(array $items, $order)
    {
        $shipmentItem = [];
        foreach ($order->getAllItems() as $orderItem) {
            if (array_key_exists($orderItem->getId(), $items)) {
                $shipmentItem[] = $this->orderConverter
                       ->itemToShipmentItem($orderItem)
                       ->setQty($items[$orderItem->getId()]);
            }
        }

        return $shipmentItem;
    }

    public function printLabels($orderIds)
    {
        $result = new DataObject();
        $carrierCredentials = [];
        $orderDataList = [];

        if (!$orderIds) {
            $result->setError(true);
            $result->setErrorMessage(__('No applicable orders.'));
            return $result;
        }

        foreach ($orderIds as $orderId) {
            $orderData = [];
            $order = $this->orderRepository->get($orderId);

            $credentials = $this->getCredentials();
            $credentialExists = false;
            foreach ($carrierCredentials as $carrierCredential) {
                if ($credentials['carrier'] === $carrierCredential['carrier']) {
                    $credentialExists = true;
                    break;
                }
            }
            if (!$credentialExists) {
                $carrierCredentials[] = $credentials;
            }

            $orderData = $this->getPrintLabelRequestBody($order);
            $orderDataList[] = $orderData;
        }

        //set printformat
        $printFormat = "A4";
        if (!empty($this->makecommerceConfig->getApiValue('label_format'))) {
            $printFormat = $this->makecommerceConfig->getApiValue('label_format');
        }

        $requestBody = [
            'credentials' => $carrierCredentials,
            'orders' => $orderDataList,
            'printFormat' => $printFormat
        ];

        //set the correct carrier for orders
        $requestBody = $this->makecommerceConfig->overrideOrderCarrierWithCredentialCarrier($requestBody);

        $response = $this->api->createLabels($requestBody);
        if (property_exists($response, 'errors')) {
            $result->setError(true);
            $result->setErrorMessage($response->errors[0]->errorMessage);
            return $result;
        }

        try {
            $this->saveLabelUrl($response, $orderIds);
            $result->setError(false);
        } catch (\Exception $e) {
            $result->setError(true);
            $result->setErrorMessage($e->getMessage());
        }
        return $result;
    }

    /**
     * Saves label URL to DB
     *
     * @param obj $response
     * @param array $orderIds
     * @return void
     */
    protected function saveLabelUrl($response, $orderIds)
    {
        if (property_exists($response, 'labelUrl')) {
            $shipmentResponseCollection = $this->shipmentResponseCollectionFactory->create();
            foreach ($shipmentResponseCollection as $shipmentResponse) {
                if (in_array($shipmentResponse->getOrderId(), $orderIds)) {
                    $shipmentResponse->setLabelUrl($response->labelUrl)->save();
                }
            }
        }
    }

    /**
     * Downloads label PDF
     *
     * @param obj $response
     * @return void
     */
    protected function downloadLabel($response)
    {
        if (property_exists($response, 'labelUrl')) {
            return $this->fileFactory->create(
                'shipping-labels/label-' . time() . '.pdf',
                file_get_contents($response->labelUrl),
                DirectoryList::VAR_DIR,
                'application/pdf'
            );
        }
    }
}

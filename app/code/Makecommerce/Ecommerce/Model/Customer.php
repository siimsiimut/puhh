<?php
namespace Makecommerce\Ecommerce\Model;

use Magento\Framework\Model\AbstractExtensibleModel;
use Makecommerce\Ecommerce\Api\Data\CustomerInterface;

class Customer extends AbstractExtensibleModel implements CustomerInterface
{

    /**
     * {@inheritdoc}
     */
    public function getEmail()
    {
        return $this->getData(self::EMAIL);
    }

    /**
     * {@inheritdoc}
     */
    public function getCountry()
    {
        return $this->getData(self::COUNTRY);
    }

    /**
     * {@inheritdoc}
     */
    public function getIp()
    {
        return $this->getData(self::REMOTE_ADDRESS);
    }

    /**
     * {@inheritdoc}
     */
    public function getLocale()
    {
        return $this->getData(self::LOCALE);
    }

    /**
     * {@inheritdoc}
     */
    public function setEmail($email = null)
    {
        return $this->setData(self::EMAIL, $email);
    }

    /**
     * {@inheritdoc}
     */
    public function setCountry($countryCode = null)
    {
        return $this->setData(self::COUNTRY, $countryCode);
    }

    /**
     * {@inheritdoc}
     */
    public function setIp($ip)
    {
        return $this->setData(self::REMOTE_ADDRESS, $ip);
    }

    /**
     * {@inheritdoc}
     */
    public function setLocale($locale = null)
    {
        return $this->setData(self::LOCALE, $locale);
    }
}

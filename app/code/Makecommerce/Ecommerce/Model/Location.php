<?php

namespace Makecommerce\Ecommerce\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;
use Makecommerce\Ecommerce\Api\Data\LocationInterface;

class Location extends AbstractModel implements LocationInterface, IdentityInterface
{
    /**
     * @var string
     */
    const CACHE_TAG = 'mc_shipping_locations';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Makecommerce\Ecommerce\Model\ResourceModel\Location');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return $this->getData(self::TYPE);
    }

    /**
     * {@inheritdoc}
     */
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * {@inheritdoc}
     */
    public function getCity()
    {
        return $this->getData(self::CITY);
    }

    /**
     * {@inheritdoc}
     */
    public function setCity($city)
    {
        return $this->setData(self::CITY, $city);
    }

    /**
     * {@inheritdoc}
     */
    public function getCountry()
    {
        return $this->getData(self::COUNTRY);
    }

    /**
     * {@inheritdoc}
     */
    public function setCountry($country)
    {
        return $this->setData(self::COUNTRY, $country);
    }

    /**
     * {@inheritdoc}
     */
    public function getAvailability()
    {
        return $this->getData(self::AVAILABILITY);
    }

    /**
     * {@inheritdoc}
     */
    public function setAvailability($availability)
    {
        return $this->setData(self::AVAILABILITY, $availability);
    }

    /**
     * {@inheritdoc}
     */
    public function getComment()
    {
        return $this->getData(self::COMMENT);
    }

    /**
     * {@inheritdoc}
     */
    public function setComment($comment)
    {
        return $this->setData(self::COMMENT, $comment);
    }

    /**
     * {@inheritdoc}
     */
    public function getProvider()
    {
        return $this->getData(self::PROVIDER);
    }

    /**
     * {@inheritdoc}
     */
    public function setProvider($provider)
    {
        return $this->setData(self::PROVIDER, $provider);
    }

    /**
     * {@inheritdoc}
     */
    public function getCarrier()
    {
        return $this->getData(self::CARRIER);
    }

    /**
     * {@inheritdoc}
     */
    public function setCarrier($carrier)
    {
        return $this->setData(self::CARRIER, $carrier);
    }

    /**
     * {@inheritdoc}
     */
    public function getAddress()
    {
        return $this->getData(self::ADDRESS);
    }

    /**
     * {@inheritdoc}
     */
    public function setAddress($address)
    {
        return $this->setData(self::ADDRESS, $address);
    }

    /**
     * {@inheritdoc}
     */
    public function getZip()
    {
        return $this->getData(self::ZIP);
    }

    /**
     * {@inheritdoc}
     */
    public function setZip($zip)
    {
        return $this->setData(self::ZIP, $zip);
    }
}

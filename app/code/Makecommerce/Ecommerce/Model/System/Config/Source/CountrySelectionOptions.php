<?php

namespace Makecommerce\Ecommerce\Model\System\Config\Source;

class CountrySelectionOptions implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Get country selection options
     *
     * @return string[string]
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'dropdown', 'label' => __('Dropdown')],
            ['value' => 'flag', 'label' => __('Flag')],
        ];
    }
}

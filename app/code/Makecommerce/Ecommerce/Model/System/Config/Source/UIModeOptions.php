<?php

namespace Makecommerce\Ecommerce\Model\System\Config\Source;

class UIModeOptions implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Get UI mode options
     *
     * @return string[string]
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'inline', 'label' => __('List')],
            ['value' => 'widget', 'label' => __('Grouped to widget')],
        ];
    }
}

<?php

namespace Makecommerce\Ecommerce\Model\System\Config\Source;

class ApiTypeOptions implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Get api type options
     *
     * @return string[string]
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'live', 'label' => 'Live'],
            ['value' => 'test', 'label' => 'Test'],
        ];
    }
}

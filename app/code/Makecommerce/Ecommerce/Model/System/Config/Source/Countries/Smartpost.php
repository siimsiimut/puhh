<?php

namespace Makecommerce\Ecommerce\Model\System\Config\Source\Countries;

class Smartpost implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            ['value' => 'EE', 'label' => __('Estonia')],
            ['value' => 'FI', 'label' => __('Finland')]
        ];
        return $options;
    }
}

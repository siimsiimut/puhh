<?php

namespace Makecommerce\Ecommerce\Model\System\Config\Source;

class LabelFormats implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Makecommerce\Ecommerce\Client\MakecommerceClientWrapper
     */
    protected $api;

    /**
     * @param \Makecommerce\Ecommerce\Client\MakecommerceClientWrapper $api
     */
    public function __construct(\Makecommerce\Ecommerce\Client\MakecommerceClientWrapper $api)
    {
        $this->api = $api;
    }

    /**
     * Get label formats
     *
     * @return array
     */
    public function toOptionArray()
    {
        $formats = array();
        foreach ($this->api->getLabelFormats() as $format) {
            $formats[] = array('value' => $format, 'label' => $format);
        }

        return $formats;
    }
}

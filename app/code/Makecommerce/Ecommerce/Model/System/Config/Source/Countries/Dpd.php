<?php

namespace Makecommerce\Ecommerce\Model\System\Config\Source\Countries;

class Dpd implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            ['value' => 'EE', 'label' => __('Estonia')],
            ['value' => 'LV', 'label' => __('Latvia')],
            ['value' => 'LT', 'label' => __('Lithuania')]
        ];
        return $options;
    }
}

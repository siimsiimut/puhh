<?php
namespace Makecommerce\Ecommerce\Model\System\Config\Source;

class LogoStyleOptions implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Get logo style options
     *
     * @return string[string]
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'logo', 'label' => __('Logo')],
            ['value' => 'text&logo', 'label' => __('Text & Logo')],
            ['value' => 'text', 'label' => __('Text')],
        ];
    }
}

<?php

namespace Makecommerce\Ecommerce\Model\System\Config\Source;

class IntegrationCountryOptionsOmniva implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Get integration country options for DPD
     *
     * @return string[string]
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'OMNIVA', 'label' => 'Estonia'],
            ['value' => 'OMNIVA_LV', 'label' => 'Latvia'],
            ['value' => 'OMNIVA_LT', 'label' => 'Lithuania'],
        ];
    }
}

<?php

namespace Makecommerce\Ecommerce\Model\System\Config\Source;

class SizeOptions implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Get size options
     *
     * @return string[string]
     */
    public function toOptionArray()
    {
        return [
            ['value' => 's', 'label' => __('S')],
            ['value' => 'm', 'label' => __('M')],
            ['value' => 'l', 'label' => __('L')],
        ];
    }
}

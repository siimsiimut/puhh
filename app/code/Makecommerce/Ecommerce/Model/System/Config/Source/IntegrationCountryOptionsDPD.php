<?php

namespace Makecommerce\Ecommerce\Model\System\Config\Source;

class IntegrationCountryOptionsDPD implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Get integration country options for DPD
     *
     * @return string[string]
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'DPD', 'label' => 'Estonia'],
            ['value' => 'DPD_LV', 'label' => 'Latvia'],
            ['value' => 'DPD_LT', 'label' => 'Lithuania'],
        ];
    }
}

<?php

namespace Makecommerce\Ecommerce\Model\System\Message\Product\Parcel\Shipping;

class Message implements \Magento\Framework\Notification\MessageInterface
{

    /**
     * @var \Makecommerce\Ecommerce\Config
     */
    protected $makecommerceConfig;

    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @param \Makecommerce\Ecommerce\Config $makecommerceConfig
     * @param \Magento\Backend\Model\UrlInterface $urlBuilder
     */
    public function __construct(
        \Makecommerce\Ecommerce\Config $makecommerceConfig,
        \Magento\Backend\Model\UrlInterface $urlBuilder
    ) {
        $this->makecommerceConfig = $makecommerceConfig;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function getIdentity()
    {
        return 'product_parcel_shipping_message';
    }

    /**
     * {@inheritdoc}
     */
    public function isDisplayed()
    {
        $hasBeenShown = (int)$this->makecommerceConfig->getApiValue('notification_is_shown');
        return !$hasBeenShown;
    }

    /**
     * {@inheritdoc}
     */
    public function getText()
    {
        $url = $this->urlBuilder->getUrl('makecommerce/products/configure');
        return __(sprintf('You have installed Makecommerce_Ecommerce module. For it to work correctly, you need to do some configurations. <br>You can do that <a href="%s">here</a>', $url));
    }

    /**
     * {@inheritdoc}
     */
    public function getSeverity()
    {
        return \Magento\Framework\Notification\MessageInterface::SEVERITY_MAJOR;
    }
}

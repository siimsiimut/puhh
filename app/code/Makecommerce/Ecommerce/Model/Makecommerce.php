<?php
namespace Makecommerce\Ecommerce\Model;

use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Model\InfoInterface;
use Magento\Payment\Model\Method\AbstractMethod;

class Makecommerce extends AbstractMethod
{
    const STATUS_MAKSEKESKUS_PENDING = 'PENDING';

    const STATUS_MAKSEKESKUS_COMPLETED = 'COMPLETED';

    const STATUS_MAKSEKESKUS_CANCELLED = 'CANCELLED';

    const STATUS_MAKSEKESKUS_RECEIVED = 'RECEIVED';

    const STATUS_MAKSEKESKUS_PAID = 'PAID';

    const STATUS_MAKSEKESKUS_EXPIRED = 'EXPIRED';

    const STATUS_MAKSEKESKUS_PART_REFUNDED = 'PART_REFUNDED';

    const STATUS_MAKSEKESKUS_REFUNDED = 'REFUNDED';

    const STATUS_MAKSEKESKUS_DECLINED = 'DECLINED';

    const PAYMENT_METHOD_CODE = 'makecommerce';

    const MODULE_NAME = 'Makecommerce_Ecommerce';

    // @codingStandardsIgnoreStart
    protected $_code = self::PAYMENT_METHOD_CODE;

    protected $_canUseInternal = false;
    protected $_isGateway = true;
    protected $_canAuthorize = true;
    protected $_canCapture = true;
    protected $_canRefund = true;
    protected $_canRefundInvoicePartial = true;
    protected $_canVoid = true;

    /**
     * @var \Makecommerce\Ecommerce\Client\MakecommerceClientWrapper
     */
    protected $_api;

    /**
     * @var \Makecommerce\Ecommerce\Logger\Logger
     */
    protected $_mkLogger;

    /**
     * Payment additional info block
     *
     * @var string
     */
    protected $_formBlockType = 'Makecommerce\Ecommerce\Block\Form\Makecommerce';

    /**
     * Sidebar payment info block
     *
     * @var string
     */
    protected $_infoBlockType = 'Makecommerce\Ecommerce\Block\Info\Makecommerce';

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * Makecommerce constructor.
     * @param \Makecommerce\Ecommerce\Client\MakecommerceClientWrapper $api
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Payment\Model\Method\Logger $logger
     * @param \Makecommerce\Ecommerce\Logger\Logger $mkLogger
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Makecommerce\Ecommerce\Client\MakecommerceClientWrapper $api,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Makecommerce\Ecommerce\Logger\Logger $mkLogger,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );
        $this->_mkLogger = $mkLogger;
        $this->_api = $api;
        $this->_messageManager = $messageManager;
    }
    // @codingStandardsIgnoreEnd

    /**
     * Capture
     *
     * @param \Magento\Framework\DataObject|InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @api
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function capture(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        parent::capture($payment, $amount);
        $info = $this->getInfoInstance();
        $data = $info->getAdditionalInformation();
        if (!isset($data['transaction_id'])) {
            throw new LocalizedException(__('Makecommerce Payment must have a transaction id.'));
        }
        $info->setTransactionId($data['transaction_id'])->save();

        return $this;
    }

    /**
     * Refund specified amount for payment
     *
     * @param \Magento\Framework\DataObject|InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @api
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function refund(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        try {
            $info = $this->getInfoInstance();
            $data = $info->getAdditionalInformation();

            if (!isset($data['transaction_id'])) {
                throw new LocalizedException(__('Makecommerce Payment must have a transaction id.'));
            }
            $transactionId = $data['transaction_id'];

            $comment = __($this->getConfigData('default_comment'));

            $this->_api->createRefund($transactionId, ["amount" => round($amount, 2), "comment" => $comment]);

            parent::refund($payment, $amount);
        } catch (\Exception $e) {
            $this->_mkLogger->error($e->getMessage() . 'ERROR REFUNDING');
            $this->_messageManager->addError($e->getMessage());
        }
        return $this;
    }

    /**
     * To check billing country is allowed for the payment method, we allow everything
     *
     * @param string $country
     * @return bool
     */
    public function canUseForCountry($country)
    {
        return true;
    }

    /**
     * Retrieve payment method title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->getConfigData('widget_title');
    }

    /**
     * Get order place redirect url
     *
     * @return string
     */
    public function getOrderPlaceRedirectUrl()
    {
        return false;
    }

    /**
     * Check method for processing with base currency
     *
     * @param string $currencyCode
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function canUseForCurrency($currencyCode)
    {
        return in_array($currencyCode, array_map('trim', explode(',', $this->getConfigData('currencies_allowed'))));
    }

    // @codingStandardsIgnoreStart
    /**
     * Assign corresponding data
     *
     * @param \Magento\Framework\DataObject|mixed $data
     * @return $this
     * @throws LocalizedException
     */
    public function assignData(\Magento\Framework\DataObject $data)
    {
        parent::assignData($data);

        $info = $this->getInfoInstance();
        $additional = $info->getAdditionalInformation();

        $transactionId = $data->getTransactionId();

        if (empty($transactionId)) { //Fixes the 2.0.(4?)+ compatibility issues
            if (isset($data->getAdditionalData()['transaction_id'])) {
                $transactionId = $data->getAdditionalData()['transaction_id'];
            }
        }

        $additional['transaction_id'] = $transactionId;
        $info->setAdditionalInformation($additional);

        return $this;
    }
    // @codingStandardsIgnoreEnd
}

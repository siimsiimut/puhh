<?php
namespace Makecommerce\Ecommerce\Model;

use Magento\Framework\Model\AbstractExtensibleModel;
use Makecommerce\Ecommerce\Api\Data\AdapterResponseInterface;

class AdapterResponse extends AbstractExtensibleModel implements AdapterResponseInterface
{
    /**
     * {@inheritdoc}
     */
    public function getIncrementId()
    {
        return $this->getData(self::INCREMENT_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function getTransaction()
    {
        return $this->getData(self::TRANSACTION);
    }

    /**
     *  {@inheritdoc}
     */
    public function setIncrementId($incrementId)
    {
        return $this->setData(self::INCREMENT_ID, $incrementId);
    }

    /**
     * {@inheritdoc}
     */
    public function setTransaction(\Makecommerce\Ecommerce\Api\Data\TransactionInterface $transaction)
    {
        return $this->setData(self::TRANSACTION, $transaction);
    }
}

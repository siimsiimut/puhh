<?php
namespace Makecommerce\Ecommerce\Model;

use Magento\Framework\Model\AbstractExtensibleModel;
use Makecommerce\Ecommerce\Api\Data\PaymentTokenInterface;

class PaymentToken extends AbstractExtensibleModel implements PaymentTokenInterface
{
    /**
     * {@inheritdoc}
     */
    public function getJson()
    {
        return $this->getData(self::JSON);
    }

    /**
     * {@inheritdoc}
     */
    public function getMac()
    {
        return $this->getData(self::MAC);
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentToken()
    {
        return $this->getData(self::PAYMENT_TOKEN);
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentTokenMultiuse()
    {
        return $this->getData(self::PAYMENT_TOKEN_MULTIUSE);
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentTokenValidUntil()
    {
        return $this->getData(self::PAYMENT_TOKEN_VALID_UNTIL);
    }

    /**
     * {@inheritdoc}
     */
    public function setJson($json)
    {
        return $this->setData(self::JSON, $json);
    }

    /**
     * {@inheritdoc}
     */
    public function setMac($mac)
    {
        return $this->setData(self::MAC, $mac);
    }

    /**
     * {@inheritdoc}
     */
    public function setPaymentToken($token)
    {
        return $this->setData(self::PAYMENT_TOKEN, $token);
    }

    /**
     * {@inheritdoc}
     */
    public function setPaymentTokenMultiuse($multiuse)
    {
        return $this->setData(self::PAYMENT_TOKEN_MULTIUSE, $multiuse);
    }

    /**
     * {@inheritdoc}
     */
    public function setPaymentTokenValidUntil($validUntil)
    {
        return $this->setData(self::PAYMENT_TOKEN_VALID_UNTIL, $validUntil);
    }
}

<?php
namespace Makecommerce\Ecommerce\Model;

use Magento\Framework\Model\AbstractExtensibleModel;
use Makecommerce\Ecommerce\Api\Data\CardDataInterface;

class CardData extends AbstractExtensibleModel implements CardDataInterface
{

    /**
     * {@inheritdoc}
     */
    public function getNumber()
    {
        return $this->getData(self::NUMBER);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * {@inheritdoc}
     */
    public function getCVC()
    {
        return $this->getData(self::CVC);
    }

    /**
     * {@inheritdoc}
     */
    public function getExpMonth()
    {
        return $this->getData(self::EXPIRATION_MONTH);
    }

    /**
     * {@inheritdoc}
     */
    public function getExpYear()
    {
        return $this->getData(self::EXPIRATION_YEAR);
    }

    /**
     * {@inheritdoc}
     */
    public function setNumber($number)
    {
        return $this->setData(self::NUMBER, $number);
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * {@inheritdoc}
     */
    public function setCvc($cvc)
    {
        return $this->setData(self::CVC, $cvc);
    }

    /**
     * {@inheritdoc}
     */
    public function setExpMonth($month)
    {
        return $this->setData(self::EXPIRATION_MONTH, $month);
    }

    /**
     * {@inheritdoc}
     */
    public function setExpYear($year)
    {
        return $this->setData(self::EXPIRATION_YEAR, $year);
    }
}

<?php

namespace Makecommerce\Ecommerce\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;
use Makecommerce\Ecommerce\Api\Data\ShipmentResponseInterface;

class ShipmentResponse extends AbstractModel implements ShipmentResponseInterface, IdentityInterface
{
    /**
     * @var string
     */
    const CACHE_TAG = 'mc_shipment_response';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Makecommerce\Ecommerce\Model\ResourceModel\ShipmentResponse');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * {@inheritdoc}
     */
    public function getBarcode()
    {
        return $this->getData(self::BARCODE);
    }

    /**
     * {@inheritdoc}
     */
    public function setBarcode($barcode)
    {
        return $this->setData(self::BARCODE, $barcode);
    }

    /**
     * {@inheritdoc}
     */
    public function getCarrier()
    {
        return $this->getData(self::CARRIER);
    }

    /**
     * {@inheritdoc}
     */
    public function setCarrier($carrier)
    {
        return $this->setData(self::CARRIER, $carrier);
    }

    /**
     * {@inheritdoc}
     */
    public function getShippingMethod()
    {
        return $this->getData(self::SHIPPING_METHOD);
    }

    /**
     * {@inheritdoc}
     */
    public function setShippingMethod($shippingMethod)
    {
        return $this->setData(self::SHIPPING_METHOD, $shippingMethod);
    }

    /**
     * {@inheritdoc}
     */
    public function getLabelUrl()
    {
        return $this->getData(self::LABEL_URL);
    }

    /**
     * {@inheritdoc}
     */
    public function setLabelUrl($labelUrl)
    {
        return $this->setData(self::LABEL_URL, $labelUrl);
    }

    /**
     * {@inheritdoc}
     */
    public function getManifestUrl()
    {
        return $this->getData(self::MANIFEST_URL);
    }

    /**
     * {@inheritdoc}
     */
    public function setManifestUrl($manifestUrl)
    {
        return $this->setData(self::MANIFEST_URL, $manifestUrl);
    }
}

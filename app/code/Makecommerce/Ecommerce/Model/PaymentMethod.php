<?php

namespace Makecommerce\Ecommerce\Model;

use Magento\Framework\Model\AbstractExtensibleModel;
use Makecommerce\Ecommerce\Api\Data\PaymentMethodInterface;

class PaymentMethod extends AbstractExtensibleModel implements PaymentMethodInterface
{

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * {@inheritdoc}
     */
    public function getCountry()
    {
        return $this->getData(self::COUNTRY);
    }

    /**
     * {@inheritdoc}
     */
    public function getMinAmount()
    {
        return $this->getData(self::MIN_AMOUNT);
    }

    /**
     * {@inheritdoc}
     */
    public function getMaxAmount()
    {
        return $this->getData(self::MAX_AMOUNT);
    }

    /**
     * {@inheritdoc}
     */
    public function getUrl()
    {
        return $this->getData(self::URL);
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * {@inheritdoc}
     */
    public function setCountry($countryCode = null)
    {
        return $this->setData(self::COUNTRY, $countryCode);
    }

    /**
     * {@inheritdoc}
     */
    public function setUrl($url = null)
    {
        return $this->setData(self::URL, $url);
    }

    /**
     * {@inheritdoc}
     */
    public function setMinAmount($min_amount = 0)
    {
        if (is_null($min_amount))
            $min_amount = 0;
        return $this->setData(self::MIN_AMOUNT, $min_amount);
    }

    /**
     * {@inheritdoc}
     */
    public function setMaxAmount($max_amount = 0)
    {
        if (is_null($max_amount))
            $max_amount = 0;
        return $this->setData(self::MAX_AMOUNT, $max_amount);
    }
}

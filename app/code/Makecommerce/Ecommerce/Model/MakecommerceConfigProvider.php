<?php

namespace Makecommerce\Ecommerce\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Makecommerce\Ecommerce\Model\Carrier\AbstractCarrier;

class MakecommerceConfigProvider implements ConfigProviderInterface
{
    // @codingStandardsIgnoreStart
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Locale\ResolverInterface
     */
    protected $resolver;

    /**
     * @var \Makecommerce\Ecommerce\Config
     */
    protected $config;

    /**
     * @var \Makecommerce\Ecommerce\Helper\Environment $environment
     */
    protected $environment;

    /**
     * @var \Makecommerce\Ecommerce\Model\Carrier\Omniva\Parcel
     */
    protected $omnivaParcel;

    /**
     * @var \Makecommerce\Ecommerce\Model\Carrier\Smartpost\Parcel
     */
    protected $smartpostParcel;

    /**
     * @var \Makecommerce\Ecommerce\Model\Carrier\Smartpost\Courier
     */
    protected $smartpostCourier;

    /**
     * @var \Makecommerce\Ecommerce\Model\Carrier\Dpd\Parcel
     */
    protected $dpdParcel;
    // @codingStandardsIgnoreEnd

    /**
     * MakecommerceConfigProvider constructor.
     * @param \Makecommerce\Ecommerce\Config $config
     * @param \Makecommerce\Ecommerce\Helper\Environment $environment
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Locale\ResolverInterface $resolver
     * @param \Makecommerce\Ecommerce\Model\Carrier\Omniva\Parcel $omnivaParcel
     * @param \Makecommerce\Ecommerce\Model\Carrier\Smartpost\Parcel $smartpostParcel
     * @param \Makecommerce\Ecommerce\Model\Carrier\Smartpost\Courier $smartpostCourier
     * @param \Makecommerce\Ecommerce\Model\Carrier\Dpd\Parcel $dpdParcel
     */
    public function __construct(
        \Makecommerce\Ecommerce\Config $config,
        \Makecommerce\Ecommerce\Helper\Environment $environment,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Locale\ResolverInterface $resolver,
        \Makecommerce\Ecommerce\Model\Carrier\Omniva\Parcel $omnivaParcel,
        \Makecommerce\Ecommerce\Model\Carrier\Smartpost\Parcel $smartpostParcel,
        \Makecommerce\Ecommerce\Model\Carrier\Smartpost\Courier $smartpostCourier,
        \Makecommerce\Ecommerce\Model\Carrier\Dpd\Parcel $dpdParcel
    ) {
        $this->config       = $config;
        $this->environment  = $environment;
        $this->resolver     = $resolver;
        $this->storeManager = $storeManager;
        $this->omnivaParcel = $omnivaParcel;
        $this->smartpostParcel = $smartpostParcel;
        $this->smartpostCourier = $smartpostCourier;
        $this->dpdParcel = $dpdParcel;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $config      = $this->config;
        $environment = $this->environment;
        $isTest      = ($config->getApiValue('environment') === 'test') ?: false;
        $publicKey   = ($isTest) ?
        $config->getApiValue('test_key_public') :
        $config->getApiValue('api_public');

        $checkoutJs = ($isTest) ?
        'https://payment.test.maksekeskus.ee/checkout/dist/checkout.min.js' :
        'https://payment.maksekeskus.ee/checkout/dist/checkout.min.js';
        $tokenizer = ($isTest) ?
        'https://payment.test.maksekeskus.ee/tokenizer/dist/tokenizer.min.js' :
        'https://payment.maksekeskus.ee/tokenizer/dist/tokenizer.min.js';
        $countries = $environment->getCountries();
        $channels  = $environment->getChannels();

        $baseUrl       = $this->storeManager->getStore()->getBaseUrl();
        $localeCountry = substr($this->resolver->getLocale(), 3);
        $locale        = substr($this->resolver->getLocale(), 0, 2);
        $makecommerce = [
                    'tokenizer'         => $tokenizer,
                    'checkoutJs'        => $checkoutJs,
                    'tokenPayUrl'       => $config->getValue('token_pay_url'),
                    'publicKey'         => $publicKey,
                    'countries'         => $environment->getCountries(),
                    'createTransaction' => $config->getValue(
                        'create_transaction_path'
                    ),
                    'paymentChannels'   => $environment->getChannels(),
                    'localeCountry'     => $localeCountry,
                    'locale'            => $locale,
                    'baseUrl'           => $baseUrl,
                    'isAvailable'       => !!($countries && $channels),
                ];
        $makecommerce['ui'] = [
                        'mode'                  => $config->getValue(
                            'ui_style'
                        ),
                        'inlineUselogo'         => $config->getValue(
                            'inline_logo_style'
                        ),
                        'chorder'               => $config->getValue(
                            'channel_order'
                        ),
                        'widgetTitle'           => $config->getValue(
                            'widget_title'
                        ),
                        'groupBanklinks'        => $config->getValue(
                            'group_banklinks'
                        ),
                        'widgetCountryselector' => $config->getValue(
                            'country_selection'
                        ),
                        'widgetGroupcc'         => $config->getValue(
                            'groupcc'
                        ),
                        'widgetGroupTitle'      => $config->getValue(
                            'cc_title'
                        ),
                        'widgetGroupCcInline'   => $config->getValue(
                            'groupcc_inline'
                        ),
                        'countrySortOrder'      => $config->getValue(
                            'country_sort_order'
                        ),
                        'logoSize'              => $config->getValue(
                            'logosize'
                        ),
                        'openDefault'           => $config->getValue(
                            'ui_open_by_default'
                        ),
                        'cardShopName'          => $config->getValue(
                            'cc_shop_name'
                        ),
                        'referenceText'         => $config->getValue(
                            'cc_reference'
                        ),
                        'cardPrefill'           => $config->getValue(
                            'cc_pass_data'
                        ),
                    ];
        return [
            'payment' => [
                'makecommerce' => $makecommerce,
            ],
            'shipping' => $this->getShippingMethodConfig()
        ];
    }

    /**
     * Gets shipping method locations
     *
     * @return array
     */
    protected function getShippingMethodConfig()
    {
        return [
            sprintf(
                '%s_%s',
                AbstractCarrier::CARRIER_OMNIVA,
                AbstractCarrier::METHOD_PARCEL
            ) => $this->omnivaParcel->getLocations(),
            sprintf(
                '%s_%s',
                AbstractCarrier::CARRIER_SMARTPOST,
                AbstractCarrier::METHOD_PARCEL
            ) => $this->smartpostParcel->getLocations(),
            sprintf(
                '%s_%s',
                AbstractCarrier::CARRIER_DPD,
                AbstractCarrier::METHOD_PARCEL
            ) => $this->dpdParcel->getLocations()
        ];
    }
}

<?php
namespace Makecommerce\Ecommerce\Model;

use Magento\Framework\Model\AbstractExtensibleModel;
use Makecommerce\Ecommerce\Api\Data\PaymentMethodsInterface;

class PaymentMethods extends AbstractExtensibleModel implements PaymentMethodsInterface
{

    /**
     * {@inheritdoc}
     */
    public function getCards()
    {
        return $this->getData(self::CARDS);
    }

    /**
     * {@inheritdoc}
     */
    public function getBanklinks()
    {
        return $this->getData(self::BANKLINKS);
    }

    /**
     * {@inheritdoc}
     */
    public function getCash()
    {
        return $this->getData(self::CASH);
    }

    /**
     * {@inheritdoc}
     */
    public function getOther()
    {
        return $this->getData(self::OTHER);
    }

    /**
     * {@inheritdoc}
     */
    public function getPaylater()
    {
        return $this->getData(self::PAYLATER);
    }

    /**
     * {@inheritdoc}
     */
    public function setCards($cards = null)
    {
        return $this->setData(self::CARDS, $cards);
    }

    /**
     * {@inheritdoc}
     */
    public function setBanklinks($bankLinks = null)
    {
        return $this->setData(self::BANKLINKS, $bankLinks);
    }

    /**
     * {@inheritdoc}
     */
    public function setCash($cash = null)
    {
        return $this->setData(self::CASH, $cash);
    }

    /**
     * {@inheritdoc}
     */
    public function setOther($other = null)
    {
        return $this->setData(self::OTHER, $other);
    }

    /**
     * {@inheritdoc}
     */
    public function setPaylater($other = null)
    {
        return $this->setData(self::PAYLATER, $other);
    }
}

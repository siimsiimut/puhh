<?php

namespace Makecommerce\Ecommerce\Model;

use Magento\Framework\Exception\LocalizedException;
use Makecommerce\Ecommerce\Api\Data\CardDataInterface;
use Makecommerce\Ecommerce\Api\Data\CustomerInterface;
use Makecommerce\Ecommerce\Api\Data\PaymentTokenInterface;
use Makecommerce\Ecommerce\Api\Data\TransactionInterface;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class TransactionManager implements \Makecommerce\Ecommerce\Api\TransactionManagerInterface
{
    //@codingStandardsIgnoreStart
    /**
     * @var \Makecommerce\Ecommerce\Client\MakecommerceClientWrapper
     */
    protected $api;

    /**
     * @var \Makecommerce\Ecommerce\Logger\Logger
     */
    protected $logger;

    /**
     * @var TransactionFactory
     */
    protected $transactionFactory;

    /**
     * @var PaymentResponseFactory
     */
    protected $paymentResponseFactory;

    /**
     * @var PaymentTokenFactory
     */
    protected $paymentTokenFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Makecommerce\Ecommerce\Config
     */
    protected $config;

    /**
     * @var \Makecommerce\Ecommerce\Helper\OrderProcessor
     */
    protected $orderProcessor;
    //@codingStandardsIgnoreEnd

    /**
     * TransactionManager constructor.
     * @param PaymentResponseFactory $paymentResponseFactory
     * @param \Makecommerce\Ecommerce\Client\MakecommerceClientWrapper $api
     * @param TransactionFactory $transactionFactory
     * @param PaymentTokenFactory $paymentTokenFactory
     * @param \Makecommerce\Ecommerce\Helper\OrderProcessor $orderProcessor
     * @param \Makecommerce\Ecommerce\Logger\Logger $logger
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Makecommerce\Ecommerce\Config $config
     */
    public function __construct(
        PaymentResponseFactory $paymentResponseFactory,
        \Makecommerce\Ecommerce\Client\MakecommerceClientWrapper $api,
        TransactionFactory $transactionFactory,
        PaymentTokenFactory $paymentTokenFactory,
        \Makecommerce\Ecommerce\Helper\OrderProcessor $orderProcessor,
        \Makecommerce\Ecommerce\Logger\Logger $logger,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Makecommerce\Ecommerce\Config $config
    ) {
        $this->orderProcessor = $orderProcessor;
        $this->api = $api;
        $this->logger = $logger;
        $this->transactionFactory = $transactionFactory;
        $this->paymentResponseFactory = $paymentResponseFactory;
        $this->paymentTokenFactory = $paymentTokenFactory;
        $this->storeManager = $storeManager;
        $this->config = $config;
    }

    /**
     * {@inheritdoc}
     */
    public function create(TransactionInterface $transaction, CustomerInterface $customer)
    {
        $baseUrl = $this->storeManager->getStore()->getBaseUrl();
        $config = $this->config;
        $transaction->setData('transaction_url', [
            'return_url' => [
                'url' => $baseUrl . $config->getValue('return_url'),
                'method' => $config->getValue('return_url_method'),
            ],
            'cancel_url' => [
                'url' => $baseUrl . $config->getValue('cancel_url'),
                'method' => $config->getValue('cancel_url_method'),
            ],
            'notification_url' => [
                'url' => $baseUrl . $config->getValue('notification_url'),
                'method' => $config->getValue('notification_url_method'),
            ],
        ]);
        $json = $this->api->createTransaction(
            [
                'transaction' => $transaction->getData(),
                'customer' => $customer->getData(),
            ]
        );

        $json = json_encode($json);
        //we need the json to be in an array format
        $decoded = json_decode($json, true);
        $transaction->load($decoded['reference']);
        $transaction->setData($decoded);
        $transaction->save();

        return $transaction;
    }

    /**
     * {@inheritdoc}
     */
    public function beforePay($transactionId, PaymentTokenInterface $token)
    {
        if ($this->config->getApiValue('environment') === 'test') {
            $privateKey = $this->config->getApiValue('test_key_secret');
        } else {
            $privateKey = $this->config->getApiValue('api_secret');
        }

        $calculatedMac = strtoupper(hash('sha512', $token->getJson() . $privateKey));

        if ($token->getMac() !== $calculatedMac) {
            $this->logger->error(__('The calculated MAC does not match the sent MAC'));
            throw new LocalizedException(__('The calculated MAC does not match the sent MAC'));
        }

        return $this->pay($transactionId, $token);
    }

    /**
     * {@inheritdoc}
     */
    public function pay($transactionId, PaymentTokenInterface $token)
    {
        $validUntil = $token->getPaymentTokenValidUntil() ?: $token->getValidUntil();
        if (strtotime($validUntil) < time()) {
            throw new LocalizedException(__('The token is expired'));
        }

        /** @var \Makecommerce\Ecommerce\Api\Data\TransactionInterface $transaction */
        $transaction = $this->transactionFactory->create()->load($transactionId);

        $token = $token->getPaymentToken() ?: $token->getId();
        $paymentConfirmation = $this->api->createPayment(
            $transactionId,
            [
                'amount' => round($transaction->getAmount(), 2),
                'currency' => $transaction->getCurrency(),
                'token' => $token,
            ]
        );

        $transactionData = json_decode(json_encode($paymentConfirmation->transaction), true);
        $this->orderProcessor->process($transactionData);

        $transaction->setData($transactionData)->save();
        return $transaction;
    }

    /**
     * {@inheritdoc}
     */
    public function token($transactionId, CardDataInterface $cardData)
    {
        $transaction = $this->transactionFactory->create()->load($transactionId);

        return $this->api->createToken(
            [
                'transaction' => [
                    'id' => $transactionId,
                ],
                'customer' => [
                    'email' => $transaction->getCustomer()->getEmail(),
                ],
                'card' => [
                    'number' => $cardData->getNumber(),
                    'name' => $cardData->getName(),
                    'cvc' => $cardData->getCVC(),
                    'exp_month' => $cardData->getExpMonth(),
                    'exp_year' => $cardData->getExpYear(),
                ],
            ]
        );
    }
}

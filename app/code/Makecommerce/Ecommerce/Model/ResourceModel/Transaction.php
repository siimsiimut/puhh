<?php
namespace Makecommerce\Ecommerce\Model\ResourceModel;

/**
 * Makecommerce transaction resource model
 */
class Transaction extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Is object new
     * @var bool
     */
    protected $_useIsObjectNew = true;

    /**
     * Primary key auto increment flag
     *
     * @var bool
     */
    protected $_isPkAutoIncrement = false;

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('makecommerce_ecommerce_transactions', 'id');
    }

    /**
     * Check if the objects exist in the database
     *
     * @param string $id
     * @param string $field = 'id'
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return boolean
     */
    public function exists($id, $field, $object)
    {
        $connection = $this->getConnection();
        $select = $this->_getLoadSelect($field, $id, $object);

        if ($connection->fetchRow($select)) {
            return true;
        }

        return false;
    }
}

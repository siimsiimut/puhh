<?php

namespace Makecommerce\Ecommerce\Model\ResourceModel;

class ShipmentResponse extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Primary key auto increment flag
     *
     * @var bool
     */
    protected $_isPkAutoIncrement = false;

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('mc_shipment_response', 'barcode');
    }
}

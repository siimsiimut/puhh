<?php
namespace Makecommerce\Ecommerce\Model\ResourceModel\Transaction;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Id field
     *
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Makecommerce\Ecommerce\Model\Transaction',
            'Makecommerce\Ecommerce\Model\ResourceModel\Transaction'
        );
    }
}

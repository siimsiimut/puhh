<?php

namespace Makecommerce\Ecommerce\Model\ResourceModel\Location;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Makecommerce\Ecommerce\Model\Location',
            'Makecommerce\Ecommerce\Model\ResourceModel\Location'
        );
    }
}

<?php

namespace Makecommerce\Ecommerce\Model\ResourceModel\ShipmentResponse;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'barcode';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Makecommerce\Ecommerce\Model\ShipmentResponse',
            'Makecommerce\Ecommerce\Model\ResourceModel\ShipmentResponse'
        );
    }
}

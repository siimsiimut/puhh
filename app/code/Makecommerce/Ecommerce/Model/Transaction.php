<?php
namespace Makecommerce\Ecommerce\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractExtensibleModel;
use Makecommerce\Ecommerce\Api\Data\PaymentMethodsInterface;
use Makecommerce\Ecommerce\Api\Data\TransactionInterface;

class Transaction extends AbstractExtensibleModel implements TransactionInterface, IdentityInterface
{
    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'makecommerce_ecommerce_transaction';

    // @codingStandardsIgnoreStart
    /**
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'makecommerce_ecommerce_transaction';

    /**
     * Parameter name in event
     *
     * In observe method you can use $observer->getEvent()->getObject() in this case
     *
     * @var string
     */
    protected $_eventObject = 'transaction';

    /**
     * Transaction constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param PaymentMethodsFactory $paymentMethodsFactory
     * @param PaymentMethodFactory $paymentMethodFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Makecommerce\Ecommerce\Model\PaymentMethodsFactory $paymentMethodsFactory,
        \Makecommerce\Ecommerce\Model\PaymentMethodFactory $paymentMethodFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->paymentMethodsFactory = $paymentMethodsFactory;
        $this->paymentMethodFactory  = $paymentMethodFactory;
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $resource,
            $resourceCollection,
            $data
        );
    }
    // @codingStandardsIgnoreEnd

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Makecommerce\Ecommerce\Model\ResourceModel\Transaction');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * {@inheritdoc}
     */
    public function getObject()
    {
        return $this->getData(self::OBJECT_TYPE);
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * {@inheritdoc}
     */
    public function getAmount()
    {
        return $this->getData(self::AMOUNT);
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrency()
    {
        return $this->getData(self::CURRENCY);
    }

    /**
     * {@inheritdoc}
     */
    public function getReference()
    {
        return $this->getData(self::REFERENCE);
    }

    /**
     * {@inheritdoc}
     */
    public function getMerchantData()
    {
        return $this->getData(self::MERCHANT_DATA);
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return $this->getData(self::TYPE);
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomer()
    {
        return $this->getData(self::CUSTOMER);
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentMethods()
    {
        $paymentMethods = $this->getData(self::PAYMENT_METHODS);

        if (is_string($paymentMethods)) {
            $paymentMethods = json_decode($paymentMethods, true);
        }

        return $this->paymentMethodsFactory->create()->setData($paymentMethods);
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderIds()
    {
        $orderIds = $this->getData(self::ORDER_IDS);
        if (is_string($orderIds)) {
            return explode(",", $orderIds);
        }
        return $orderIds;
    }

    /**
     * {@inheritdoc}
     */
    public function setReference($reference = null)
    {
        return $this->setData(self::REFERENCE, $reference);
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * {@inheritdoc}
     */
    public function setObject($object)
    {
        return $this->setData(self::OBJECT_TYPE, $object);
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * {@inheritdoc}
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * {@inheritdoc}
     */
    public function setAmount($amount)
    {
        return $this->setData(self::AMOUNT, $amount);
    }

    /**
     * {@inheritdoc}
     */
    public function setCurrency($currency)
    {
        return $this->setData(self::CURRENCY, $currency);
    }

    /**
     * {@inheritdoc}
     */
    public function setMerchantData($merchantData = null)
    {
        return $this->setData(self::MERCHANT_DATA, $merchantData);
    }

    /**
     * {@inheritdoc}
     */
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }

    /**
     * {@inheritdoc}
     */
    public function setCustomer($customer = null)
    {
        return $this->setData(self::CUSTOMER, $customer);
    }

    /**
     * {@inheritdoc}
     */
    public function setPaymentMethods(\Makecommerce\Ecommerce\Api\Data\PaymentMethodsInterface $paymentMethods)
    {
        return json_encode($this->setData(self::PAYMENT_METHODS, $paymentMethods));
    }

    /**
     * {@inheritdoc}
     */
    public function setOrderIds($orderIds)
    {
        if (is_array($orderIds)) {
            return $this->setData(self::ORDER_IDS, implode(",", $orderIds));
        }
        return $this->setData(self::ORDER_IDS, $orderIds);
    }

    /**
     * Check if the objects exist in the database, and load if it is
     *
     * @param string $id
     * @param string $field = 'id'
     * @return boolean
     */
    public function exists($id = null, $field = self::ID)
    {
        if (!isset($id)) {
            $id = $this->getId() ?: false;
        }
        if (empty($id)) {
            return false;
        }
        return $this->getResource()->exists($id, $field, $this);
    }

    /**
     * Check object state (true - if it is object without id on object just created)
     * This method can help detect if object just created in _afterSave method
     * problem is what in after save object has id and we can't detect what object was
     * created in this transaction
     *
     * @param bool|null $flag
     * @return bool
     */
    public function isObjectNew($flag = null)
    {
        if ($flag !== null) {
            $this->_isObjectNew = $flag;
        }
        if ($this->_isObjectNew !== null) {
            return $this->_isObjectNew;
        }

        return !(bool) $this->exists($this->getId());
    }

    /**
     * Processing object before save data
     *
     * @return void
     */
    public function beforeSave()
    {
        parent::beforeSave();
        $customer = $this->getCustomer();
        $this->setCustomer(
            isset($customer) ?
            (isset($customer['id']) ?
                $customer['id'] :
                null) :
            null
        );
        $links = $this->getLinks();
        if (!is_string($links)) {
            $this->setLinks(json_encode($links));
        }
        $paymentMethods = $this->getData(self::PAYMENT_METHODS);
        if (!is_string($paymentMethods)) {
            if (is_object($paymentMethods) && $paymentMethods instanceof PaymentMethodsInterface) {
                $this->setData(self::PAYMENT_METHODS, json_encode($paymentMethods->getData()));
            } elseif (is_array($paymentMethods)) {
                $this->setData(self::PAYMENT_METHODS, json_encode($paymentMethods));
            } else {
                $this->setData(self::PAYMENT_METHODS, null);
            }
        }
    }
}

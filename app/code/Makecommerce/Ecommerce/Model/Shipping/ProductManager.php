<?php

namespace Makecommerce\Ecommerce\Model\Shipping;

class ProductManager
{
    /**
     * @var \Magento\Framework\App\ResourceConnection $resource
     */
    protected $resource;

    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    protected $configWriter;

    /**
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
    ) {
        $this->resource = $resource;
        $this->configWriter = $configWriter;
    }

    /**
     * Sets all product's allowed_for_parcel attribute
     *
     * @param int $allowed
     * @return void
     * @throws \Exception
     */
    public function setAllowedForParcel($allowed)
    {
        try {
            $connection = $this->resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
            $attributeId = $connection->fetchOne(
                sprintf(
                    "SELECT attribute_id FROM `%s` WHERE attribute_code = 'allowed_for_parcel' AND entity_type_id = 4",
                    $connection->getTableName('eav_attribute')
                )
            );

            $connection->query(
                sprintf(
                    "DELETE FROM `%s` WHERE attribute_id = %s",
                    $connection->getTableName('catalog_product_entity_int'),
                    $attributeId
                )
            );

            $allProductIds = $connection->fetchAll(
                sprintf("SELECT entity_id FROM `%s`", $connection->getTableName('catalog_product_entity'))
            );
            $dataToInsert = [];
            foreach ($allProductIds as $data) {
                $id = $data['entity_id'];
                $dataToInsert[] = [
                    'attribute_id' => $attributeId,
                    'store_id' => 0,
                    'entity_id' => $id,
                    'value' => $allowed
                ];
            }

            if ($dataToInsert) {
                $connection->insertMultiple($connection->getTableName('catalog_product_entity_int'), $dataToInsert);
            }

            $connection->query(
                sprintf(
                    "UPDATE `%s` SET default_value = %s WHERE attribute_id = %s",
                    $connection->getTableName('eav_attribute'),
                    $allowed,
                    $attributeId
                )
            );

            $this->configWriter->save('makecommerce/ecommerce/notification_is_shown', 1);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()));
        }
    }
}

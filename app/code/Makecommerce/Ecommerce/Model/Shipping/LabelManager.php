<?php

namespace Makecommerce\Ecommerce\Model\Shipping;

use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\DataObject;

class LabelManager
{
    /**
     * @var \Makecommerce\Ecommerce\Config
     */
    protected $makecommerceConfig;
    
    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var \Makecommerce\Ecommerce\Helper\Carrier
     */
    protected $carrierHelper;

    /**
     * @var \Makecommerce\Ecommerce\Client\MakecommerceClientWrapper
     */
    protected $api;

    /**
     * @var \Makecommerce\Ecommerce\Model\ResourceModel\ShipmentResponse\CollectionFactory
     */
    protected $shipmentResponseCollectionFactory;

    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @param \Makecommerce\Ecommerce\Config $makecommerceConfig
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Makecommerce\Ecommerce\Helper\Carrier $carrierHelper
     * @param \Makecommerce\Ecommerce\Client\MakecommerceClientWrapper $api
     * @param \Makecommerce\Ecommerce\Model\ResourceModel\ShipmentResponse\CollectionFactory
     *                                                       $shipmentResponseCollectionFactory
     * @param FileFactory $fileFactory
     */
    public function __construct(
        \Makecommerce\Ecommerce\Config $makecommerceConfig,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Makecommerce\Ecommerce\Helper\Carrier $carrierHelper,
        \Makecommerce\Ecommerce\Client\MakecommerceClientWrapper $api,
        \Makecommerce\Ecommerce\Model\ResourceModel\ShipmentResponse\CollectionFactory
        $shipmentResponseCollectionFactory,
        FileFactory $fileFactory
    ) {
        $this->makecommerceConfig = $makecommerceConfig;
        $this->orderRepository = $orderRepository;
        $this->carrierHelper = $carrierHelper;
        $this->api = $api;
        $this->shipmentResponseCollectionFactory = $shipmentResponseCollectionFactory;
        $this->fileFactory = $fileFactory;
    }

    /**
     * Sends request to print shipping labels
     *
     * @param array $orderIds
     * @return DataObject
     */
    public function printLabels($orderIds)
    {
        $result = new DataObject();
        $carrierCredentials = [];
        $orderDataList = [];

        if (!$orderIds) {
            $result->setError(true);
            $result->setErrorMessage(__('No applicable orders.'));
            return $result;
        }

        foreach ($orderIds as $orderId) {
            $orderData = [];
            $order = $this->orderRepository->get($orderId);
            
            $carrier = $this->carrierHelper->getCarrierModel($order->getShippingMethod());
            
            $credentials = $carrier->getCredentials();
            $credentialExists = false;
            foreach ($carrierCredentials as $carrierCredential) {
                if ($credentials['carrier'] === $carrierCredential['carrier']) {
                    $credentialExists = true;
                    break;
                }
            }
            if (!$credentialExists) {
                $carrierCredentials[] = $credentials;
            }

            $orderData = $carrier->getPrintLabelRequestBody($order);
            $orderDataList[] = $orderData;
        }

        //set printformat
        $printFormat = "A4";
        if (!empty($this->makecommerceConfig->getApiValue('label_format'))) {
            $printFormat = $this->makecommerceConfig->getApiValue('label_format');
        }

        $requestBody = [
            'credentials' => $carrierCredentials,
            'orders' => $orderDataList,
            'printFormat' => $printFormat
        ];

        //set the correct carrier for orders
        $requestBody = $this->makecommerceConfig->overrideOrderCarrierWithCredentialCarrier($requestBody);

        $response = $this->api->createLabels($requestBody);

        if (property_exists($response, 'errors')) {
            $result->setError(true);
            $result->setErrorMessage($response->errors[0]->errorMessage);
            return $result;
        }

        try {
            $this->saveLabelUrl($response, $orderIds);
            $this->downloadLabel($response);
            $result->setError(false);
        } catch (\Exception $e) {
            $result->setError(true);
            $result->setErrorMessage($e->getMessage());
        }
        return $result;
    }

    /**
     * Saves label URL to DB
     *
     * @param obj $response
     * @param array $orderIds
     * @return void
     */
    protected function saveLabelUrl($response, $orderIds)
    {
        if (property_exists($response, 'labelUrl')) {
            $shipmentResponseCollection = $this->shipmentResponseCollectionFactory->create();
            foreach ($shipmentResponseCollection as $shipmentResponse) {
                if (in_array($shipmentResponse->getOrderId(), $orderIds)) {
                    $shipmentResponse->setLabelUrl($response->labelUrl)->save();
                }
            }
        }
    }

    /**
     * Downloads label PDF
     *
     * @param obj $response
     * @return void
     */
    protected function downloadLabel($response)
    {
        if (property_exists($response, 'labelUrl')) {
            return $this->fileFactory->create(
                'shipping-labels/label-' . time() . '.pdf',
                file_get_contents($response->labelUrl),
                DirectoryList::VAR_DIR,
                'application/pdf'
            );
        }
    }
}

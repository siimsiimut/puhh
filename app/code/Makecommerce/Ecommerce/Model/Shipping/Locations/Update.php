<?php

namespace Makecommerce\Ecommerce\Model\Shipping\Locations;

use Makecommerce\Ecommerce\Api\Data\LocationInterface;
use Makecommerce\Ecommerce\Model\Carrier\AbstractCarrier;

class Update
{
    /**
     * @var \Makecommerce\Ecommerce\Client\MakecommerceClientWrapper
     */
    protected $mkClient;

    /**
     * @var \Makecommerce\Ecommerce\Model\System\Config\Source\Countries\Smartpost
     */
    protected $smartpostCountries;

    /**
     * @var \Makecommerce\Ecommerce\Model\System\Config\Source\Countries\Omniva
     */
    protected $omnivaCountries;

    /**
     * @var \Makecommerce\Ecommerce\Model\System\Config\Source\Countries\Dpd
     */
    protected $dpdCountries;

    /**
     * @var \Makecommerce\Ecommerce\Model\ResourceModel\Location\CollectionFactory
     */
    protected $locationCollectionFactory;

    /**
     * @var \Makecommerce\Ecommerce\Model\LocationFactory
     */
    protected $locationFactory;

    /**
     * @param \Makecommerce\Ecommerce\Client\MakecommerceClientWrapper
     * @param \Makecommerce\Ecommerce\Model\System\Config\Source\Countries\Smartpost $smartpostCountries
     * @param \Makecommerce\Ecommerce\Model\System\Config\Source\Countries\Omniva $omnivaCountries
     * @param \Makecommerce\Ecommerce\Model\System\Config\Source\Countries\Dpd $dpdCountries
     * @param \Makecommerce\Ecommerce\Model\ResourceModel\Location\CollectionFactory $locationCollectionFactory
     * @param \Makecommerce\Ecommerce\Model\LocationFactory $locationFactory
     */
    public function __construct(
        \Makecommerce\Ecommerce\Client\MakecommerceClientWrapper $mkClient,
        \Makecommerce\Ecommerce\Model\System\Config\Source\Countries\Smartpost $smartpostCountries,
        \Makecommerce\Ecommerce\Model\System\Config\Source\Countries\Omniva $omnivaCountries,
        \Makecommerce\Ecommerce\Model\System\Config\Source\Countries\Dpd $dpdCountries,
        \Makecommerce\Ecommerce\Model\ResourceModel\Location\CollectionFactory $locationCollectionFactory,
        \Makecommerce\Ecommerce\Model\LocationFactory $locationFactory
    ) {
        $this->mkClient = $mkClient;
        $this->smartpostCountries = $smartpostCountries;
        $this->omnivaCountries = $omnivaCountries;
        $this->dpdCountries = $dpdCountries;
        $this->locationCollectionFactory = $locationCollectionFactory;
        $this->locationFactory = $locationFactory;
    }

    /**
     * Updates shipping method locations
     *
     * @return void
     */
    public function update()
    {
        $body = $this->createRequestBody();
        $destinations = $this->mkClient->getDestinations($body);
        $this->saveLocations($destinations);
    }

    /**
     * Creates request body for getting destinations request
     *
     * @return array
     */
    protected function createRequestBody()
    {
        //TODO: find a better way to gather countries
        $countries = [];
        foreach ($this->smartpostCountries as $country) {
            $countries[] = $country['value'];
        }
        foreach ($this->omnivaCountries as $country) {
            $countryCode = $country['value'];
            if (!in_array($countryCode, $countries)) {
                $countries[] = $countryCode;
            }
        }
        foreach ($this->dpdCountries as $country) {
            $countryCode = $country['value'];
            if (!in_array($countryCode, $countries)) {
                $countries[] = $countryCode;
            }
        }

        return [
            'carriers' => [
                strtoupper(substr(AbstractCarrier::CARRIER_SMARTPOST, 0, -2)),
                strtoupper(substr(AbstractCarrier::CARRIER_OMNIVA, 0, -2)),
                strtoupper(substr(AbstractCarrier::CARRIER_DPD, 0, -2))
            ],
            'countries' => $countries,
            'types' => [
                'APT',
                'PUP'
            ]
        ];
    }

    /**
     * Saves carrier destinations to database
     *
     * @param array $destinations
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function saveLocations($destinations)
    {
        //remove all destinations first (check if there are at least 10 entries in destinations)
        if (count($destinations) > 10) {
            $this->emptyLocationTable();
        }

        foreach ($destinations as $index => $destination) {
            if (property_exists($destination, LocationInterface::ID)
                   && property_exists($destination, LocationInterface::CARRIER)
                   && property_exists($destination, LocationInterface::COUNTRY)
                   && property_exists($destination, LocationInterface::TYPE)
                   && property_exists($destination, LocationInterface::NAME) ) {
                $this->saveLocation($destination);
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Received invalid location data')
                );
            }
        }
    }

    /**
     * Saves destination to DB
     *
     * @param array $destination
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function saveLocation($destination)
    {
        try {
            $location = $this->locationFactory->create();
            $location->setId($destination->{LocationInterface::ID});
            $location->setCarrier($destination->{LocationInterface::CARRIER});
            $location->setCountry($destination->{LocationInterface::COUNTRY});
            $location->setType($destination->{LocationInterface::TYPE});
            $location->setName($destination->{LocationInterface::NAME});
            if (property_exists($destination, LocationInterface::CITY)) {
                $location->setCity($destination->{LocationInterface::CITY});
            }
            if (property_exists($destination, LocationInterface::AVAILABILITY)) {
                $location->setAvailability($destination->{LocationInterface::AVAILABILITY});
            }
            if (property_exists($destination, LocationInterface::PROVIDER)) {
                $location->setProvider($destination->{LocationInterface::PROVIDER});
            }
            if (property_exists($destination, LocationInterface::ADDRESS)) {
                $location->setAddress($destination->{LocationInterface::ADDRESS});
            }
            if (property_exists($destination, LocationInterface::ZIP)) {
                $location->setZip($destination->{LocationInterface::ZIP});
            }
            //TODO: better way to save comments
            if (property_exists($destination, 'commentEt')) {
                $location->setComment($destination->commentEt);
            }
            if (property_exists($destination, 'commentFi')) {
                $location->setComment($destination->commentFi);
            }
            if (property_exists($destination, 'commentLv')) {
                $location->setComment($destination->commentLv);
            }
            if (property_exists($destination, 'commentLt')) {
                $location->setComment($destination->commentLt);
            }

            $location->save();
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __(sprintf('Something went wrong while saving locations. %s', $e->getMessage()))
            );
        }
    }

    /**
     * Deletes all entries from locations table
     *
     * @return void
     */
    private function emptyLocationTable()
    {
        $locationCollection = $this->locationCollectionFactory->create()->load();
        foreach ($locationCollection as $location) {
            $location->delete();
        }
    }
}

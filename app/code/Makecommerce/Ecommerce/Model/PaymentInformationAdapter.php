<?php

namespace Makecommerce\Ecommerce\Model;

use Makecommerce\Ecommerce\Api\Data\CustomerInterface;
use Makecommerce\Ecommerce\Api\Data\TransactionInterface;

class PaymentInformationAdapter implements \Makecommerce\Ecommerce\Api\PaymentInformationAdapterInterface
{
    //@codingStandardsIgnoreStart
    /**
     * @var \Magento\Checkout\Api\PaymentInformationManagementInterface
     */
    protected $paymentInformationManagement;

    /**
     * @var \Magento\Sales\Model\OrderRepository
     */
    protected $orderRepository;

    /**
     * @var \Makecommerce\Ecommerce\Api\TransactionManagerInterface
     */
    protected $transactionManager;

    /**
     * @var AdapterResponseFactory
     */
    protected $adapterResponseFactory;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * @var TransactionInterface
     */
    protected $transaction;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CustomerInterface
     */
    protected $customer;

    /**
     * @var \Magento\Framework\Locale\ResolverInterface
     */
    protected $resolver;
    //@codingStandardsIgnoreEnd

    /**
     * PaymentInformationAdapter constructor.
     * @param \Magento\Checkout\Api\PaymentInformationManagementInterface $paymentInformationManagement
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Makecommerce\Ecommerce\Api\TransactionManagerInterface $transactionManager
     * @param TransactionInterface $transaction
     * @param CustomerInterface $customer
     * @param AdapterResponseFactory $adapterResponseFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Locale\ResolverInterface $resolver
     */
    public function __construct(
        \Magento\Checkout\Api\PaymentInformationManagementInterface $paymentInformationManagement,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Makecommerce\Ecommerce\Api\TransactionManagerInterface $transactionManager,
        TransactionInterface $transaction,
        CustomerInterface $customer,
        AdapterResponseFactory $adapterResponseFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Locale\ResolverInterface $resolver
    ) {
        $this->paymentInformationManagement = $paymentInformationManagement;
        $this->orderRepository = $orderRepository;
        $this->quoteRepository = $quoteRepository;
        $this->transactionManager = $transactionManager;
        $this->transaction = $transaction;
        $this->customer = $customer;
        $this->adapterResponseFactory = $adapterResponseFactory;
        $this->storeManager = $storeManager;
        $this->resolver = $resolver;
    }

    /**
     * {@inheritdoc}
     */
    public function adaptPlaceOrder(
        $cartId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        $country,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        $orderId = $this->paymentInformationManagement->savePaymentInformationAndPlaceOrder(
            $cartId,
            $paymentMethod,
            $billingAddress
        );

        $order = $this->orderRepository->get($orderId);
        $quote = $this->quoteRepository->get($order->getQuoteId());

        $transaction = $this->transaction;
        $transaction->setAmount(round($order->getGrandTotal(), 2))
            ->setCurrency($this->storeManager->getStore()->getCurrentCurrency()->getCode())
            ->setReference($order->getQuoteId());
        
        $customer = $this->customer;
        $customer->setIp($quote->getRemoteIp())
            ->setCountry($country)
            ->setLocale(substr($this->resolver->getLocale(), 0, 2));

        $transaction = $this->transactionManager->create($transaction, $customer);
        $additionalInformation = $quote->getPayment()->getAdditionalInformation();
        $additionalInformation['transaction_id'] = $transaction->getId();
        $additionalInformation['method_title'] = $quote->getPayment()->getMethod();
        
        $quote->getPayment()->setAdditionalInformation($additionalInformation);
        $quote->save();
        $order->getPayment()->setAdditionalInformation($additionalInformation);
        $order->save();
        
        $transaction->setOrderIds([$orderId]);
        $transaction->save();

        $response = $this->adapterResponseFactory->create();
        $response->setTransaction($transaction);
        $response->setIncrementId($order->getIncrementId());

        return $response;
    }
}

define([
    "jquery",
    "jquery/ui"
], function($) {
    "use strict";
        $.widget('acty.instantpurchase', {
            _create: function() {
                var form_id = this.options.form_id;
                 this.element.on('click', function(e){
                    let form = document.getElementById(form_id);
                    if (form) {
                        console.log('submit form');
                        form.action += 'instantpurchase/1';
                        form.submit();
                    }
                });
            }
        });

    return $.acty.instantpurchase;
});

<?php
namespace Acty\InstantPurchase\Plugin\Checkout\Cart\Controller;

class Cart extends \Magento\Checkout\Controller\Cart\Add
{
    protected function getBackUrl($defaultUrl = null)
    {
        $url = parent::getBackUrl($defaultUrl);
        if ($this->getRequest()->getParam('instantpurchase')) {
            $url = $this->_url->getUrl('purchase');
        }
        return $url;
    }
}

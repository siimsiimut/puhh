<?php

namespace Acty\Framework\Extensions;

use Magento\Framework\App\Cache\Type\FrontendPool;
use Magento\Framework\Cache\Frontend\Decorator\TagScope;
use Magento\Store\Model\StoreManagerInterface;
use Zend_Cache;

abstract class CacheType extends TagScope
{
    protected $store;

    public function __construct(FrontendPool $cacheFrontendPool, StoreManagerInterface $store)
    {
        $this->store = $store;

        parent::__construct($cacheFrontendPool->get(static::$type), static::$tag);
    }

    public function remember($key, $callback, $tags = [], $expires = 7200)
    {
        $key = $this->key($key);
        $cached = $this->load($key);

        if ($cached) {
            return json_decode($cached, true);
        }

        $result = $callback();

        $this->save(json_encode($result), $key, $tags, $expires);

        return $result;
    }

    public function flush($tags = [])
    {
        $this->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, $tags);
    }

    public function key($key)
    {
        return $key.'_'.$this->store->getStore()->getId();
    }
}

<?php

namespace Acty\Framework\Utils;

use Exception;

class Serializer
{
    public function encode($data)
    {
        try {
            $encoded = base64_encode(json_encode($data));
        } catch (Exception $e) {
            return ['exception' => $e->getMessage(), 'data' => null];
        }

        $data = json_encode(['data' => $encoded, 'type' => gettype($data), 'timestamp' => time()]);

        return ['data' => $data, 'exception' => null];
    }

    public function decode($data)
    {
        try {
            $decoded = json_decode($data, true);
            $assoc = $decoded['type'] === 'object' ? false : true;
            $data = json_decode(base64_decode($decoded['data']), $assoc);
        } catch (Exception $e) {
            return ['exception' => $e->getMessage(), 'data' => null];
        }

        return ['data' => $data, 'exception' => null];
    }
}

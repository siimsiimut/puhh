<?php

namespace Acty\Framework\Support;

use Adbar\Dot;
use Magento\Framework\App\DeploymentConfig;

class EnvConfig
{
    protected $config;

    public function __construct(DeploymentConfig $deploymentConfig)
    {
        $this->config = new Dot($deploymentConfig->get('acty'));
    }

    public function get($key, $default = null)
    {
        return $this->config->get($key, $default);
    }
}

<?php
namespace Acty\Mailer\Model;

use Acty\Mailer\Model\Config;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Mail\Template\TransportBuilder;

class EmailSender
{
    protected $moduleConfig;

    protected $storeManager;

    protected $transportBuilder;

    public function __construct(
        Config $moduleConfig,
        StoreManagerInterface $storeManager,
        TransportBuilder $transportBuilder
     ) {
        $this->moduleConfig = $moduleConfig;
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
     }

    public function send($data)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/tempankerr.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        
        try{
            $this->transportBuilder
                ->setTemplateIdentifier($data['template'])
                ->setTemplateOptions(['area' => 'frontend', 'store' => $this->storeManager->getStore()->getId()])
                ->setTemplateVars($data['variables'])
                ->setFrom($this->getEmailSenderIdentity())
                ->addTo($data['email']);

            $this->transportBuilder->getTransport()->sendMessage();
            
            $this->transportBuilder
                ->setTemplateIdentifier($data['template'])
                ->setTemplateOptions(['area' => 'frontend', 'store' => $this->storeManager->getStore()->getId()])
                ->setTemplateVars($data['variables'])
                ->setFrom($this->getEmailSenderIdentity())
                ->addTo("");

            $this->transportBuilder->getTransport()->sendMessage();
        } catch (\Exception $e) {
            $logger->info("error data ". print_r($e->getMessage(),true));
        }
        return $this;
    }

    protected function getEmailSenderIdentity()
    {
        return $this->moduleConfig->getSenderIdentity();
    }
}

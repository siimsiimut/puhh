<?php
/**
 * Config
 *
 * @copyright Copyright © 2019 ACTY OU. All rights reserved.
 * @author    info@acty.ee
 */

namespace Acty\Mailer\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Config
{
    const SETTINGS_PATH = 'mailer/general/';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var \Magento\Eav\Model\Config
     */
    private $eavConfig;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        \Magento\Eav\Model\Config $eavConfig
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->eavConfig = $eavConfig;
    }

    public function getSenderIdentity()
    {
        return $this->getConfig(self::SETTINGS_PATH.'sender_identity');
    }


    protected function getConfig($path)
    {
        return $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}

var config = {
    map: {
        '*': {
            'acty-vue': 'Acty_Assets/js/vue',
            'acty-vue-min': 'Acty_Assets/js/vue.min',
            'acty-axios': 'Acty_Assets/js/axios',
            'acty-axios-min': 'Acty_Assets/js/axios.min'
        }
    }
}

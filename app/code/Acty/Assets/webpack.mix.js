const mix = require('laravel-mix')


if ( ! mix.inProduction()) {
    mix
        .js('resources/js/vue.js', 'view/frontend/web/js/vue.js')
        .js('resources/js/axios.js', 'view/frontend/web/js/axios.js')
        .sourceMaps()
} else {
    mix
        .js('resources/js/vue.js', 'view/frontend/web/js/vue.min.js')
        .js('resources/js/axios.js', 'view/frontend/web/js/axios.min.js')
}

<?php

namespace Acty\Buum\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $table = $installer->getConnection()
                    ->newTable($installer->getTable('buum_stock'))
                    ->addColumn(
                        'id',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        null,
                        ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                        'ID'
                    )
                    ->addColumn(
                        'stock_no',
                        \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        null,
                        ['unsigned' => true, 'nullable' => false],
                        'ID'
                    )
                    ->addColumn(
                        'stock_code',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        40,
                        ['nullable' => true, 'default' => null,],
                        'Stock code'
                    )
                    ->addColumn(
                        'stock_name',
                        \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        40,
                        ['nullable' => true, 'default' => null],
                        'Name'
                    )
                    ->addColumn(
                        'status',
                        \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                        null,
                        ['nullable' => false, 'unsigned' => true, 'default' => '0'],
                        'Enabled'
                    );

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}

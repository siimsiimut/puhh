<?php
namespace Acty\Buum\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (!$context->getVersion()) {
            $this->addXmlEnabled($setup);
        }

        if (version_compare($context->getVersion(), '1.0.10') < 0) {
            $this->addXmlEnabled($setup);
        }

        $setup->endSetup();
    }

    protected function addXmlEnabled($setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable('buum_stock'),
            'status_xml',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'length' => 1,
                'nullable' => false,
                'default'  =>'0',
                'unsigned' => true,
                'comment' => 'Stock status in xml feed'
            ]
        );
    }
}

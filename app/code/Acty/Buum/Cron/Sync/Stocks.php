<?php
namespace Acty\Buum\Cron\Sync;

use \Psr\Log\LoggerInterface;

class Stocks
{
    protected $logger;

    public function __construct(
        LoggerInterface $logger,
        \Acty\Buum\Sync\Stocks $stocks,
        \Acty\Buum\Helper\Config $config
    ) {
        $this->logger = $logger;
        $this->stocks = $stocks;
        $this->config = $config;
    }

    public function execute()
    {
        if (!$this->config->isCronEnabled()) {
            return;
        }
        $this->logger->info('Start buum stocks sync');
        $this->stocks->sync();
        $this->logger->info('End buum stocks sync');
    }
}

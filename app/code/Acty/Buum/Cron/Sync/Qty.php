<?php
namespace Acty\Buum\Cron\Sync;

use \Psr\Log\LoggerInterface;

class Qty
{
    protected $logger;
    protected $config;
    protected $qtySync;

    public function __construct(
        LoggerInterface $logger,
        \Acty\Buum\Sync\Qty $qtySync,
        \Acty\Buum\Helper\Config $config
    ) {
        $this->logger = $logger;
        $this->qtySync = $qtySync;
        $this->config  = $config;
    }

    public function executeChanged()
    {
        if (!$this->config->isCronEnabled()) {
            return;
        }
        $this->logger->info('Start buum qty changed sync');
        $this->qtySync->syncChanged();
        $this->logger->info('End buum qty changed sync');
    }


    public function executeAll()
    {
        if (!$this->config->isCronEnabled()) {
            return;
        }
        $this->logger->info('Start buum qty all sync');
        $this->qtySync->syncAll();
        $this->logger->info('End buum qty all sync');
    }
}

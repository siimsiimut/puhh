<?php
namespace Acty\Buum\Cron\Sync;

use \Psr\Log\LoggerInterface;

class Prices
{
    protected $logger;
    protected $config;
    protected $priceSync;

    public function __construct(
        LoggerInterface $logger,
        \Acty\Buum\Sync\Price $priceSync,
        \Acty\Buum\Helper\Config $config
    ) {
        $this->logger = $logger;
        $this->priceSync = $priceSync;
        $this->config  = $config;
    }


    public function execute()
    {
        if (!$this->config->isCronEnabled()) {
            return;
        }
        $this->logger->info('Start buum prices sync');
        $this->priceSync->syncAll();
        $this->logger->info('End buum prices sync');
    }
}

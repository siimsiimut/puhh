<?php
namespace Acty\Buum\Cron\Sync;

use \Psr\Log\LoggerInterface;

class StoresAvailability
{
    protected $logger;

    public function __construct(
        LoggerInterface $logger,
        \Acty\Buum\Sync\StoresAvailability $storesAvailability,
        \Acty\Buum\Helper\Config $config
    ) {
        $this->logger = $logger;
        $this->storesAvailability = $storesAvailability;
        $this->config = $config;
    }

    public function execute()
    {
        if (!$this->config->isCronEnabled()) {
            return;
        }
        $this->logger->info('Start buum storesAvailability sync');
        $this->storesAvailability->sync();
        $this->logger->info('End buum storesAvailability sync');
    }
    public function executeWithWebsite()
    {
        if (!$this->config->isCronEnabled()) {
            return;
        }
        $this->logger->info('Start buum storesAvailability sync with websiteId change');
        $this->storesAvailability->sync(true);
        $this->logger->info('End buum storesAvailability sync');
    }
}

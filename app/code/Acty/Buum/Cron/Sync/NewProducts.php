<?php
namespace Acty\Buum\Cron\Sync;

use \Psr\Log\LoggerInterface;

class NewProducts
{
    protected $logger;
    protected $config;
    protected $qty;

    public function __construct(
        LoggerInterface $logger,
        \Acty\Buum\Sync\NewProducts $newProductsSync,
        \Acty\Buum\Helper\Config $config
    ) {
        $this->logger = $logger;
        $this->newProductsSync = $newProductsSync;
        $this->config  = $config;
    }

    public function execute()
    {
        if (!$this->config->isCronEnabled()) {
            return;
        }
        $this->logger->info('Start new products sync');
        $log = $this->newProductsSync->sync();
        $this->logger->info(print_r($log, true));
        $this->logger->info('End new products sync');
    }
}

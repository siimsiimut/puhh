<?php
namespace Acty\Buum\Sync;

use Magento\CatalogInventory\Helper\Stock;
use Magento\Catalog\Model\Product\Attribute\Source\Status;


class StoresAvailability
{
    
    protected $buum_latvian_quantitities = [];
    protected $buum_product_quantitities = [];
    
    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Acty\Buum\Model\Buum\StockFactory $buumStockFactory,
        \Acty\Buum\Helper\Stock $stockHelper,
        \Acty\Buum\Helper\Config $config,
        Stock $stockFilter,
        Status $productStatus,
        \Magento\Framework\Model\ResourceModel\Iterator $iterator,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Acty\Buum\Logger\Logger $logger
    ) {
        $this->buumStockFactory = $buumStockFactory;
        $this->productFactory = $productFactory;
        $this->stockHelper = $stockHelper;
        $this->config = $config;
        $this->stockFilter  = $stockFilter;
        $this->productStatus = $productStatus;
        $this->iterator = $iterator;
        $this->productRepository = $productRepository;
        $this->logger = $logger;
    }

    public function sync(bool $syncLatvianWebsite = false)
    {
        
        $this->logger->info('starting storeavailability sync', [
            'websiteSync' => $syncLatvianWebsite
        ]);
        $this->buum_product_quantitities = $this->getBuumQty();
        $this->buum_latvian_quantitities = $this->getLatvianQty();
        
        $collection = $this->productFactory->create()->getCollection()->addAttributeToSelect(['warehouse_quantity', 'buum_id']);
        $collection->addAttributeToFilter('status', ['in' => $this->productStatus->getVisibleStatusIds()]);

        $this->iterator->walk(
            $collection->getSelect(),
            [[$this, 'syncStoresAvailability']],
            ['syncLatvian' => $syncLatvianWebsite]

        );

        $this->logger->info('finished storeavailability sync');
    }
    public function syncStoresAvailability ($args)
    {
        $product = $this->productRepository->getById($args['row']['entity_id']);
        $buumId = $product->getData('buum_id');
        $wh_qty = $product->getData('warehouse_quantity');

        $stock_data = [];
        if (array_key_exists($buumId, $this->buum_product_quantitities)) {
            $stock_data = $this->buum_product_quantitities[$buumId];
        }
        $stock_data = json_encode($stock_data);
        if ($wh_qty != $stock_data) {
            $product->addAttributeUpdate('warehouse_quantity', $stock_data, 0);
        }
        if (!$args['syncLatvian']) {
            return;
        }
        if (array_key_exists($buumId, $this->buum_latvian_quantitities)) {
            $lat_stock_data = $this->buum_latvian_quantitities[$buumId];
            $total = $this->getTotalQuantity($lat_stock_data);
            $existingWeb = $product->getWebsiteIds();
            $backorderStatus = $product->getExtensionAttributes()->getStockItem()->getBackorders();
            $newWeb = $this->getWebsiteConfiguration($existingWeb, $total, $backorderStatus);
            if($newWeb !== $existingWeb){
                try {
                    $this->logger->info('Try change websites for product', [
                        'sku' => $product->getSku(),
                        'websites' => $newWeb
                    ]);
                    $product->setWebsiteIds($newWeb);
                    $product->save();
                } catch (\Throwable $th) {
                    $this->logger->error('Error saving new webiste IDs', [
                        'error' => $th
                    ]);
                }
            }
        }
    }

    protected function getWebsiteConfiguration($existingWeb, $total, $backorderStatus)
    {
        $latWeb = $this->config->getLatvianWebsiteId();
        $newWeb = [];
        foreach($existingWeb as $web){
            if($web !== $latWeb){
                $newWeb[] = $web;
            }
        }
        if($total >= $this->config->getLatvianStockMin() || $backorderStatus !== 0) {
            $newWeb[] = $latWeb;
        }
        return $newWeb;
    }

    protected function getTotalQuantity($stock_data)
    {
        $total = 0;
        foreach($stock_data as $stock){
            $total += $stock;
        }
        return $total;
    }

    protected function getMagentoProducts()
    {
        $collection = $this->productFactory->create()->getCollection()->addAttributeToSelect(['warehouse_quantity', 'buum_id']);
        $collection->addAttributeToFilter('status', ['in' => $this->productStatus->getVisibleStatusIds()]);
        return $collection;
    }

    protected function getBuumQty()
    {
        $stockData = $this->stockHelper->getEnabledFrontendStocksData();
        $allStocks = $this->getAllStocks($stockData);
        $collection = $this->buumStockFactory->create()->getCollection();
        $collection->addFieldToFilter('STOCKNO', ['in' => [$allStocks]]);

        $products = [];
        $subStocks = $this->getSubStocksData($stockData);
        foreach ($collection as $buumrow) {
            if(array_key_exists($buumrow->getData('STOCKNO'), $subStocks)){
                $stockId = $subStocks[$buumrow->getData('STOCKNO')];
            }
            else{
                $stockId = $buumrow->getData('STOCKNO');
            }

            if(array_key_exists($buumrow->getData('ARTNO'), $products)){
                if(array_key_exists($stockId, $products[$buumrow->getData('ARTNO')])){
                    $oldValue = $products[$buumrow->getData('ARTNO')][$stockId];
                    $products[$buumrow->getData('ARTNO')][$stockId] = $oldValue + $buumrow->getData('VOLUME');
                    continue;
                }
            }
            $products[$buumrow->getData('ARTNO')][$stockId] = $buumrow->getData('VOLUME');
        }
        return $products;
    }

    protected function getSubStocksData($stockData)
    {   $subStocksData = [];
        foreach ($stockData as $stockId => $subStocks) {
            foreach($subStocks as $sub){
                $subStocksData[$sub] = $stockId;
            }
        }
        return $subStocksData;
    }

    protected function getAllStocks($stockData)
    {   $allStocks = [];
        foreach ($stockData as $stockId => $subStocks) {
            $allStocks[] = $stockId;
            foreach($subStocks as $sub){
                $allStocks[] = $sub;
            }
        }
        $allStocks = array_unique($allStocks);
        return $allStocks;
    }

    protected function getLatvianQty()
    {
        $collection = $this->buumStockFactory->create()->getCollection();
        $collection->addFieldToFilter('STOCKNO', ['in' => [$this->config->getLatvianStocks()]]);

        $products = [];
        foreach ($collection as $buumrow) {
            $products[$buumrow->getData('ARTNO')][$buumrow->getData('STOCKNO')] = $buumrow->getData('VOLUME');
        }
        return $products;
    }
}

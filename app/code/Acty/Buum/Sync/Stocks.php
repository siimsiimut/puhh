<?php
namespace Acty\Buum\Sync;

use \Acty\Buum\Model\Buum\StockCodFactory;
use \Acty\Buum\Model\StockFactory;


class Stocks
{
    public function __construct(
        StockCodFactory $stockCodFactory,
        StockFactory $StockFactory
    ) {
        $this->stockCodFactory = $stockCodFactory;
        $this->stockFactory = $StockFactory;
    }

    public function sync()
    {
        $buum_stock_collection    = $this->stockCodFactory->create()->getCollection();
        $magento_stock_collection = $this->getMagentoStocksArr();
        $this->refreshMagentoStocks($buum_stock_collection, $magento_stock_collection);
        $this->deleteOldStocks($buum_stock_collection, $magento_stock_collection);
    }

    protected function refreshMagentoStocks($buum_stock_collection, $magento_stock_collection)
    {
        $changed_stocks = $this->getChangedStocks($buum_stock_collection, $magento_stock_collection);
        foreach ($changed_stocks as $value) {
            $this->refreshStock($value, $magento_stock_collection);
        }
    }

    protected function refreshStock($value, $magento_stock_collection)
    {
        $model = $this->stockFactory->create();
        if (array_key_exists($value->getStockNo(), $magento_stock_collection)) {
            $model->load($magento_stock_collection[$value->getStockNo()]->getId());
        }
        $model->setStockNo($value->getStockNo());
        $model->setStockName($value->getStockName());
        $model->setStockCode($value->getStockCode());
        $model->save();
    }

    protected function getChangedStocks($buum_stock_collection, $magento_stock_collection)
    {
        $ret =  [];
        $magento_stocks = $this->getMagentoStocksArr($magento_stock_collection);
        foreach ($buum_stock_collection as $buum_stock) {
            if($this->needsRefresh($buum_stock, $magento_stocks)) {
                $ret[] = $buum_stock;
            }
        }
        return $ret;
    }

    protected function needsRefresh($buum_stock, $magento_stocks)
    {
        $stock_no = $buum_stock->getStockNo();
        if (!array_key_exists($stock_no, $magento_stocks)) {
            return true;
        }
        if ($buum_stock->getStockCode() != $magento_stocks[$stock_no]->getStockCode()) {
            return true;
        }

        if ($buum_stock->getStockName() != $magento_stocks[$stock_no]->getStockName()) {
            return true;
        }
        return false;
    }

    protected function getMagentoStocksArr()
    {
        $magento_stock_collection = $this->stockFactory->create()->getCollection();
        $ret = [];
        foreach ($magento_stock_collection as $magento_stock) {
            $ret[$magento_stock->getStockNo()] = $magento_stock;
        }
        return $ret;
    }

    protected function deleteOldStocks($buum_stock_collection, $magento_stock_collection)
    {
        $stock_no_buum = $buum_stock_collection->getAllIds();
        $stock_no_magento = array_keys($magento_stock_collection);
        foreach (array_diff($stock_no_magento, $stock_no_buum) as $id) {
            $model = $this->stockFactory->create()->load($magento_stock_collection[$id]->getId());
            $model->delete();
        }
    }

}

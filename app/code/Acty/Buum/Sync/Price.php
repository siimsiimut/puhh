<?php
namespace Acty\Buum\Sync;

use Acty\Buum\Repositories\Product;
use Acty\Buum\Repositories\Buum\Price as BuumPriceRepo;
use Acty\Buum\Model\Indexer;
use Acty\Buum\Logger\Logger;
use Acty\Buum\Model\CacheCleaner;

class Price
{
    public function __construct(
        BuumPriceRepo $buumPriceRepo,
        Product $productRepo,
        Indexer $indexer,
        Logger $logger,
        CacheCleaner $cacheCleaner
    ) {
        $this->buumPriceRepo  = $buumPriceRepo;
        $this->magentoProductRepo = $productRepo;
        $this->indexer = $indexer;
        $this->logger = $logger;
        $this->cacheCleaner = $cacheCleaner;
    }

    public function syncAll()
    {
        $buumPricesTable = $this->buumPriceRepo->getAll();
        $priceChanged = 0;
        $magentoPricesTable = $this->magentoProductRepo->getAllPrices();
        foreach ($this->magentoProductRepo->getAll(['buum_id', 'price', 'special_price', 'status', 'sku']) as $product) {
            if (!$product->getData('buum_id') or $product->getStatus() == 2) {
                continue;
            }
            if (array_key_exists($product->getData('buum_id'), $buumPricesTable)) {
                $isChanged = $this->magentoProductRepo->savePrices($product, $this->getPrices($magentoPricesTable, $product->getSku()), $buumPricesTable[$product->getData('buum_id')]['ORIGINAL_PRICE'], $buumPricesTable[$product->getData('buum_id')]['PRICE'], $buumPricesTable[$product->getData('buum_id')]['LV_PRICE']);
                $priceChanged += (int)$isChanged;
            }
            
        }

        $this->logger->info(sprintf('Prices changed: %u', $priceChanged));
        //mysql has gone away error, if to many products
        if ($priceChanged > 499) {
            $result = $this->indexer->indexPriceIndexes();
            $this->cacheCleaner->flushAll();
            $this->logger->info(sprintf('Prices changed: %s', $result));
        }
    }

    private function getPrices($magentoPricesTable, $sku){
        if(array_key_exists($sku, $magentoPricesTable)){
            return $magentoPricesTable[$sku];
        }
        return false;
    }
}

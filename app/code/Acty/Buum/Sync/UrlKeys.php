<?php
namespace Acty\Buum\Sync;

use Magento\Catalog\Api\CategoryRepositoryInterface;

class UrlKeys
{
    public function __construct(
        \Acty\Buum\Repositories\Product $productRepo,
        \Acty\Buum\Repositories\Category $categoryRepo,
        \Magento\Catalog\Model\Product\Url $productUrl,
        CategoryRepositoryInterface $categoryRepository
    ) {
        $this->productRepo = $productRepo;
        $this->categoryRepo = $categoryRepo;
        $this->productUrl = $productUrl;
        $this->categoryRepository = $categoryRepository;
    }

    public function fix($stores, $type, $level, $nocheck)
    {
        if($type === 'product'){
            $products = $this->productRepo->getAll(['name']);
            foreach($products as $product){
                $this->fixKey($product, $stores);
            }
        }
        elseif($type === 'category'){
            $categories = $this->categoryRepo->getCategories(['name', 'level']);
            $categories = $this->sortByLevel($categories);
            $ignore = [1,2];
            if($level){
                foreach($categories[$level] as $category){
                    if(!in_array($category->getId(), $ignore)){
                        $this->fixCatKey($category, $stores, $nocheck);
                    }
                }  
            }
            else{
                foreach($categories as $level){
                    foreach($level as $category){
                        if(!in_array($category->getId(), $ignore)){
                            $this->fixCatKey($category, $stores, $nocheck);
                        }
                    }
                }
            }
        }
        else{
            print_r("Exiting\n");
            return;
        }
        print_r("Done!\n");
    }

    private function sortByLevel($categories){
        $data = [];
        foreach($categories as $category){
            $data[$category->getLevel()][] = $category;
        }
        return $data;
    }
    public function fixmyproducts()
    {
        $stores = [1,3];
        $productIds = [];
        for ($i=32030; $i < 32468; $i++) { 
            $productIds[] = $i;
        }
        $products = $products = $this->productRepo->getAll(['name'])->addFieldToFilter('entity_id', ['in' => $productIds]);
        foreach ($products as $prod) {
            $this->fixKey($prod, $stores);
        }
    }
    private function fixKey($product, $stores)
    {
        foreach ($stores as $store) {
            $name = $product->setStoreId($store)->load($product->getId())->getName();
            $newUrlKey = $this->productUrl->formatUrlKey($name. '-' . $product->getSku());
            $oldUrlKey = $product->setStoreId($store)->load($product->getId())->getUrlKey();
            if($oldUrlKey !== $newUrlKey){
                print_r("Store: ".$store. "Change \n");
                $product->setStoreId($store)->setUrlKey($newUrlKey)->save();
            }
        }
        print_r($product->getSku()."\n");
    }

    private function fixCatKey($category, $stores, $nocheck)
    {
        print_r("Category: ".$category->getName(). "\n");
        foreach ($stores as $store) {
            $name = $category->setStoreId($store)->load($category->getId())->getName();
            $newUrlKey = $this->productUrl->formatUrlKey($name);
            $oldUrlKey = $category->setStoreId($store)->load($category->getId())->getUrlKey();
            if($oldUrlKey !== $newUrlKey || $nocheck == 'true'){
                print_r("Store: ".$store. " Change \n");
                $category->setStoreId($store)->setUrlKey($newUrlKey)->save();
            }
        }
        print_r("\n");
    }
}

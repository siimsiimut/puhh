<?php
namespace Acty\Buum\Sync;

class BuumId
{
    public function __construct(
        \Acty\Buum\Model\StockFactory $stockFactory,
        \Acty\Buum\Model\Buum\ItemFactory $buumItemFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory
    )
    {
        $this->stockFactory = $stockFactory;
        $this->buumItemFactory = $buumItemFactory;
        $this->productFactory = $productFactory;
    }

    public function sync() {
        $collection = $this->productFactory->create()->getCollection()->addAttributeToSelect('buum_id');
        $collection->setFlag('has_stock_status_filter', true);
        $relations = $this->getRelations();
        foreach ($collection as $key => $product) {
            $sku = $product->getSku();
            if (array_key_exists($sku, $relations) && $product->getBuumId() != $relations[$sku]) {
                $product->addAttributeUpdate('buum_id', $relations[$sku], 0);
            }
        }
    }

    public function getRelations()
    {
        $collection = $this->buumItemFactory->create()->getCollection();
        $ret = [];
        foreach ($collection as $item) {
            $ret[$item->getData('ARTCODE')] = $item->getData('ARTNO');
        }
        return $ret;
    }

    public function printMissingBuumIds() {
        $collection = $this->productFactory->create()->getCollection()->addAttributeToSelect('buum_id');
        $relations = $this->getRelations();
        foreach ($collection as $key => $product) {
            if ($product->getBuumId() == null) {
                echo $product->getSku()."\n";
            }
        }
    }
}

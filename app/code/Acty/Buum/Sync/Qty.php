<?php
namespace Acty\Buum\Sync;

use Magento\CatalogInventory\Api\StockItemRepositoryInterface;
use Magento\CatalogInventory\Api\StockItemCriteriaInterface;
use \Magento\CatalogInventory\Api\StockRegistryInterface;
use \Magento\Catalog\Model\ProductFactory;
use \Acty\Buum\Helper\Stock;

class Qty
{
    public function __construct(
        Stock $stockQtyHelper,
        ProductFactory $productFactory,
        StockRegistryInterface $stockRegistry,
        StockItemRepositoryInterface $stockItemRepo,
        StockItemCriteriaInterface $stockItemCriteria
    ) {
        $this->stockQtyHelper = $stockQtyHelper;
        $this->productFactory = $productFactory;
        $this->stockRegistry = $stockRegistry;
        $this->stockItemRepo = $stockItemRepo;
        $this->stockItemCriteria = $stockItemCriteria;
    }

    public function syncAll($force = false)
    {
        $buumQty = $this->stockQtyHelper->getBuumQtyGrouped('status');
        $qtyCache =$this->getQtyCache();
        foreach ($this->getBuumIdTable() as $buumId => $productId) {
            $newQty = (array_key_exists($buumId, $buumQty)) ? $buumQty[$buumId] : 0;

            if ($force or (array_key_exists($productId, $qtyCache) && $newQty != $qtyCache[$productId])) {
                $this->updateStockItem($productId, $newQty);
            }
        }
        //refresh products without buumid for backorder flag refresh
        if ($force) {
            foreach ($this->getProductsWithoutBuumId() as $productId) {
                $this->updateStockItem($productId, false);
            }
        }
    }

    public function syncChanged()
    {
        $buumQty = $this->stockQtyHelper->getBuumQtyGrouped('status', true);
        $qtyCache = $this->getQtyCache();
        $productIds = $this->getBuumIdTable(array_keys($buumQty));
        foreach ($buumQty as $buumId => $newQty) {
            if (!array_key_exists($buumId, $productIds)) {
                continue;
            }
            $productId = $productIds[$buumId];
            if (array_key_exists($productId, $qtyCache) && $newQty != $qtyCache[$productId]) {
                $this->updateStockItem($productId, $newQty);
            }
        }
    }

    protected function updateStockItem($productId, $newQty)
    {

        $item = $this->stockRegistry->getStockItem($productId);
        $oldQty = $item->getQty();

        if ($newQty === false) {
            $newQty = $item->getQty();
        }

        $item->setQty($newQty);
        $allowBackorders = $item->getUseConfigBackorders() == 0 and $item->getBackorders() == 1;
        $item->setIsInStock($allowBackorders or $newQty > 0);
        $this->stockItemRepo->save($item);
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/qty.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info("id: " . $productId . " qty: " . $newQty . " old qty: " . $oldQty, [
            'allow_backorders' => print_r($allowBackorders)
        ]);
    }

    protected function getQtyCache()
    {
        $qtyCache  = [];
        foreach ($this->stockItemRepo->getList($this->stockItemCriteria)->getItems() as $item) {
            $qtyCache[$item->getProductId()] = $item->getQty();
        }
        return $qtyCache;
    }

    //TODO buum_id filter
    protected function getBuumIdTable($buumIdFilter = false)
    {
        $collection = $this->productFactory->create()->getCollection()->addAttributeToSelect('buum_id');
        $collection->setFlag('has_stock_status_filter', true);
        if ($buumIdFilter) {
            $collection->addAttributeToFilter('buum_id', ['in' => $buumIdFilter]);
        }
        $arr = [];
        foreach ($collection as $product) {
            if ($product->getBuumId()) {
                $arr[$product->getBuumId()] = $product->getId();
            }
        }
        return $arr;
    }

    //TODO buum_id filter
    protected function getProductsWithoutBuumId()
    {
        $collection = $this->productFactory->create()->getCollection()->addAttributeToSelect('buum_id');
        $collection->setFlag('has_stock_status_filter', true);
        $arr = [];
        foreach ($collection as $product) {
            if (!$product->getBuumId()) {
                $arr[] = $product->getId();
            }
        }
        return $arr;
    }

}

<?php
namespace Acty\Buum\Sync;

use \Acty\Buum\Model\Buum\InvoiceFactory;
use \Acty\Buum\Model\Buum\InvoiceItemFactory;
use \Acty\Buum\Model\Buum\PaymentFactory;
use \Acty\Buum\Model\Buum\TransLogFactory;
use \Magento\Directory\Api\CountryInformationAcquirerInterface;
use Acty\Mailer\Model\EmailSender;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Store\Model\StoreManagerInterface;


class Invoice
{
    const TRANSACTION_EVENT_CODE_UPDATE = 'U';
    const TRANSACTION_EVENT_CODE_CREATE = 'A';

    protected $buumInvoice;
    protected $countryInformation;
    protected $productFactory;
    private $carrierFactory;
    /**
     * @var \ashtokalo\translit\Translit
     */
    protected $translitter;

    public function __construct(
        InvoiceFactory $buumInvoice,
        InvoiceItemFactory $buumInvoiceItem,
        PaymentFactory $buumPayment,
        TransLogFactory $transLogFactory,
        CountryInformationAcquirerInterface $countryInformation,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Acty\Buum\Helper\Warehouse $warehouse,
        EmailSender $emailSender,
        \Magento\Customer\Model\Address\Config $addressConfig,
        PaymentHelper $paymentHelper,
        StoreManagerInterface $storeManager,
        \Acty\Buum\Helper\Config $config,
        \Magento\Shipping\Model\CarrierFactoryInterface $carrierFactory,
        \ashtokalo\translit\Translit $translitter
    ) {
        $this->buumInvoice = $buumInvoice;
        $this->buumInvoiceItem = $buumInvoiceItem;
        $this->buumPayment = $buumPayment;
        $this->countryInformation = $countryInformation;
        $this->productFactory = $productFactory;
        $this->transLogFactory = $transLogFactory;
        $this->warehouse = $warehouse;
        $this->emailSender = $emailSender;
        $this->_addressConfig = $addressConfig;
        $this->paymentHelper = $paymentHelper;
        $this->storeManager = $storeManager;
        $this->config = $config;
        $this->carrierFactory = $carrierFactory;
        $this->translitter = $translitter;
    }

    public function pushOrderToBuum($order)
    {
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/templogank.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        
        try {
            if($order->getStore()->getWebsiteId() == $this->config->getLatvianWebsiteId()){
                $warehouseData = ['id' => $this->config->getLVDefaultWarehouse(), 'email' => ''];
            }
            else{
                $warehouseData = $this->warehouse->getWarehouseData($order);
            }
            $logger->info("order id ". print_r($order->getId(),true));
            $logger->info("warehouse data ". print_r($warehouseData,true));
            $invoice = $this->saveInvoice($order, $warehouseData['id']);
            $lineNo = 0;
            if ($invoice->getId()) {
                foreach ($order->getAllItems() as $item) {
                    $product = $this->productFactory->create()->load($item->getProductId());
                    if ($product->getBuumId()) {
                        $this->saveItem($item, $product, $invoice->getId(), $lineNo++);
                    }
                }
                $this->saveShipping($order, $invoice->getId(), $lineNo);
                $this->savePayment($invoice->getId(), $order);
                $this->saveTransaction($invoice->getId());
                $order->addStatusToHistory(false, 'Tellimus lisatud Buum vahetabelisse');
                $order->save();

                if($warehouseData['email']){
                    $logger->info("email send start ");
                    $template = $this->config->getTemplateStoreConfirmation();
                    $variables = $this->getEmailVariables($order);
                    $this->emailSender->send(['template' => $template, 'variables' => $variables, 'email' => $warehouseData['email']]);
                    $logger->info("email send end ");
                }
            }
        } catch (\Exception $e) {
            $order->addStatusToHistory(false, 'Ei saa andmeid BUUM-i lisada! Error: ' . $e->getMessage());
            $order->save();
        }
    }
    /**
     * @param \Magento\Sales\Model\Order $order
     */
    protected function getEmailVariables($order)
    {
        $variables = [];
        $variables['order_id'] = $order->getId();

        $renderer = $this->_addressConfig->getFormatByCode('html')->getRenderer();

        $address = $order->getShippingAddress();
        $variables['formattedShippingAddress'] = $renderer->renderArray($address);

        $billingAddress = $order->getBillingAddress();
        $variables['formattedBillingAddress'] = $renderer->renderArray($billingAddress);

        $variables['payment_html'] = $this->getPaymentHtml($order);

        $variables['formattedShippingMethodTitle'] = $this->getFormattedShippingMethodTitle($order);

        $variables['created_at'] = $order->getCreatedAtFormatted(1);

        $variables['increment_id'] = $order->getIncrementId();

        return $variables;
    }

    protected function getFormattedShippingMethodTitle($order)
    {
        $shippingMethodCarrier = $order->getShippingMethod(true)->getCarrierCode();

        $carrier = $this->carrierFactory->get($shippingMethodCarrier);

        if($shippingMethodCarrier == 'tablerate'){
            $result = $carrier->getConfigData('specificerrmsg') ?
                $carrier->getConfigData('title') . ' ' . $carrier->getConfigData('specificerrmsg') :
                $carrier->getConfigData('title');
        } else {
            $result = $carrier->getConfigData('title');
        }
        return $result;
    }

    protected function getPaymentHtml($order)
    {
        return $this->paymentHelper->getInfoBlockHtml(
            $order->getPayment(),
            $this->storeManager->getStore()->getId()
        );
    }

    protected function getInvoiceIdentifier($order)
    {
        return 'MEPS'.$order->getIncrementId();
    }

    protected function saveInvoice($order, $warehouseId)
    {
        
        //if russian store we need to translit cyryllic because buum does not support
        $rus = $order->getStore()->getId() == '2' ? true : false;

        $address = $order->getShippingAddress();

        $buumInvoice = $this->buumInvoice->create();
        $buumInvoice->setData('INVOICE', $this->getInvoiceIdentifier($order));
        if($order->getStore()->getWebsiteId() == $this->config->getLatvianWebsiteId()){
            $buumInvoice->setData('CUSTCODE', $this->config->getLatvianInvoiceCustomerCode());
        }
        else{
            $buumInvoice->setData('CUSTCODE', $this->config->getInvoiceCustomerCode());
        }

        $buumInvoice->setData('TYPE', $this->config->getInvoiceDocType());

        $address = $order->getShippingAddress();

        $customerName = $address->getName();

        if ($rus) {
            $buumInvoice->setData('CUSTNAME', $this->translitter->convert($customerName, 'ru'));
            $buumInvoice->setData('ADDRESS1', $this->translitter->convert(implode(' ', $address->getStreet()), 'ru'));
            $country = $this->countryInformation->getCountryInfo($address->getCountryId());
            $address2 = $this->translitter->convert($country->getFullNameLocale().', '. $address->getRegion() .', '. $address->getCity(), 'ru');
            $buumInvoice->setData('ADDRESS2', $address2);
        } else {
            $buumInvoice->setData('CUSTNAME', $customerName);
            $buumInvoice->setData('ADDRESS1', implode(' ', $address->getStreet()));
            $country = $this->countryInformation->getCountryInfo($address->getCountryId());
            $address2 = $country->getFullNameLocale().', '. $address->getRegion() .', '. $address->getCity();
            $buumInvoice->setData('ADDRESS2', $address2);
        }
        $buumInvoice->setData('PHONE', $address->getTelephone());
        $buumInvoice->setData('EMAIL', $address->getEmail());
        $buumInvoice->setData('ADDRESS3', $address->getPostcode());
        $buumInvoice->setData('COMMENT', $this->createInvoiceComment($order));

        $buumInvoice->setData('STOCKNO', $warehouseId);

        $buumInvoice->setData('AMOUNT', $order->getGrandTotal());
        $buumInvoice->setData('VATAMOUNT', $order->getTaxAmount());

        $buumInvoice->save();
        return $buumInvoice;
    }
    protected function createInvoiceComment(\Magento\Sales\Model\Order $order)
    {
        //if russian store we need to translit cyryllic because buum does not support
        $rus = $order->getStore()->getId() == '2' ? true : false;
        
        $method = $order->getShippingMethod();
        if ($rus) {
            if (strpos($method, 'actydpdlocker') !== false) {
                return $this->translitter->convert($order->getShippingDescription().' ('.substr($method, strpos($method, "_") + 1).')', 'ru'); 
            }
            return $this->translitter->convert($order->getShippingDescription(), 'ru');
        } else {
            if (strpos($method, 'actydpdlocker') !== false) {
                return $order->getShippingDescription().' ('.substr($method, strpos($method, "_") + 1).')'; ;
            }
            return $order->getShippingDescription();
        }
        

    }
    protected function saveItem($item, $product, $invoiceId, $lineNo)
    {
        $this->saveLine($invoiceId, $lineNo, $product->getBuumId(), $item->getQtyOrdered(), $item->getRowTotalInclTax(), $item->getDiscountAmount(), $item->getTaxAmount());
    }

    protected function saveShipping($order, $invoiceId, $lineNo)
    {
        $buumId = $this->config->getShippingMethodBuumId($order->getShippingMethod());
        if ($buumId) {
            $this->saveLine($invoiceId, $lineNo, $buumId, 1, $order->getShippingAmount()+$order->getShippingTaxAmount(), 0,$order->getShippingTaxAmount() );
        }
    }

    protected function saveLine($invno, $lineNo, $buumId, $qty, $amount,$discountAmount, $taxAmount, $rcomment = null, $sizeno = null)
    {
        $buumInvoiceItem = $this->buumInvoiceItem->create();
        $buumInvoiceItem->setData('INVNO', $invno);
        $buumInvoiceItem->setData('LINENO', $lineNo);
        $buumInvoiceItem->setData('ARTNO', $buumId);
        $buumInvoiceItem->setData('QTY', $qty);

        if ($discountAmount > 0) {
            $buumInvoiceItem->setData('AMOUNT', $amount - $discountAmount);
        } else {
            $buumInvoiceItem->setData('AMOUNT', $amount);
        }
        $buumInvoiceItem->setData('VATAMOUNT', $taxAmount);
        $buumInvoiceItem->setData('RCOMMENT', $rcomment);
        $buumInvoiceItem->setData('SIZENO', $sizeno);
       
        $buumInvoiceItem->save();
    }

    protected function savePayment($invoiceId, $order)
    {
        $method = $order->getPayment()->getMethodInstance();
        $methodCode = $method->getCode();

        $payment = $this->buumPayment->create();
        $payment->setData('INVNO', $invoiceId);
        $payment->setData('LINENO', 0);
        $payment->setData('AMOUNT', $order->getGrandTotal());
        $payment->setData('PAYDOC', $method->getTitle());
        switch ($methodCode) {
            case 'ambientia_estonianbanklinks_swedbank':
                $payment->setData('CAUSENO', 6);
                break;
            case 'ambientia_estonianbanklinks_seb':
                $payment->setData('CAUSENO', 43);
                break;
            case 'direct':
                $payment->setData('CAUSENO', 18);
                break;
            case 'liisipayment':
            case 'esto_hirepurchase':
            case 'buyplan':
                $payment->setData('CAUSENO', 45);
                break;
            default:
                break;
        }
        $payment->save();
    }

    protected function saveTransaction($invoiceId)
    {
        $transaction = $this->transLogFactory->create();
        $transaction->setData('TABLE_NAME', 'INVOICE');
        $transaction->setData('ID', $invoiceId);
        $transaction->setData('EVENT_CODE', self::TRANSACTION_EVENT_CODE_CREATE);
        $transaction->setData('RETRY_COUNT', 0);
        $transaction->save();
    }
}

<?php
namespace Acty\Buum\Sync;

use \Acty\Buum\Model\Buum\StockCodFactory;
use \Acty\Buum\Model\StockFactory;
use Acty\Buum\Repositories\Buum\Names as BuumNamesRepo;


class NewProducts
{
    /**
     * @var \Magento\Framework\Filter\FilterManager
     */
    protected $filter;

    public function __construct(
        BuumNamesRepo $buumNamesRepo,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Acty\Buum\Model\Buum\ItemFactory $buumProductFactory,
        \Acty\Buum\Model\Buum\StockFactory $buumStockFactory,
        \Acty\Buum\Model\Buum\PriceFactory $buumPriceFactory,
        \Acty\Buum\Model\Buum\BarcodeFactory $buumBarcodeFactory,
        \Acty\Buum\Helper\Config $config,
        \Acty\Buum\Repositories\Product $productRepo,
        \Magento\Framework\Filter\FilterManager $filter
    ) {
        $this->buumNamesRepo  = $buumNamesRepo;
        $this->productFactory = $productFactory;
        $this->buumProductFactory = $buumProductFactory;
        $this->buumStockFactory = $buumStockFactory;
        $this->buumPriceFactory = $buumPriceFactory;
        $this->buumBarcodeFactory = $buumBarcodeFactory;
        $this->config = $config;
        $this->productRepo = $productRepo;
        $this->filter = $filter;
    }

    public function sync()
    {
        $skus = $this->productRepo->getSkus();
        $buumProducts = $this->buumProductFactory->create()->getCollection();
        $quantities = $this->getQtyTable();

        $prices = $this->getPriceTable($this->config->getPricelistId());
        $specialPrices = $this->getPriceTable($this->config->getSpecialPricelistId());
        $log = [];

        $buumNamesTable = $this->buumNamesRepo->getAll();
        //print_r($buumNamesTable);

        $ean = $this->getDefaultBarcodes();

        foreach ($buumProducts as $product) {
            $createdAt = $product->getData('CREATED');

            //skip creation of products before 2022 to skip stale buum products

            if (strtotime($createdAt) < strtotime('2022-02-23')) {
                continue;
            }

            $buumId = $product->getData('ARTNO');
            $sku = trim($product->getData('ARTCODE'));


            if (in_array($sku, $skus)) {
                continue;
            }          

            if (!array_key_exists($buumId, $prices)) {
                continue; 
            }
            if (!array_key_exists($buumId, $ean)) {
                $productEAN = '';
            } else {
                $productEAN = $ean[$buumId];
            }

            $newProduct = [
                'name' => $product->getData('ARTNAME'),
                'buum_id' => $buumId,
                'sku' => $sku,
                'price' => $prices[$buumId],
                'category_ids' => [$this->config->getNewProductsCategoryId()],
                'url_key' => $this->filter->translitUrl($product->getData('ARTNAME')) . '-' . $sku,
                'ean13' => $productEAN
            ];
            if (array_key_exists($buumId, $specialPrices) && $specialPrices[$buumId] < $prices[$buumId]) {
                $newProduct['special_price'] = $specialPrices[$buumId];
            }

            $engName = array_key_exists($buumId, $buumNamesTable) ? $buumNamesTable[$buumId]['NAME'] : null;

            $log[] = $newProduct;
            try {
                $this->productRepo->storeSimpleProduct($newProduct, $engName, $this->config->getEnglishStoreIds());

                if (!array_key_exists($buumId, $quantities)) {
                    continue;
                }
                if ($quantities[$buumId] < 1) {
                    continue;
                }
                $this->productRepo->storeQty($sku, $quantities[$buumId]);

            } catch (\Exception $e) {
                $log[] = 'ERROR: '. $e->getMessage();
            }
        }
        return $log;
    }

    public function getQtyTable()
    {
        $collection = $this->buumStockFactory->create()->getCollection();
        $qty = [];
        foreach ($collection as $stock) {
            if (!array_key_exists($stock->getData('ARTNO'), $qty)) {
                $qty[$stock->getData('ARTNO')] = 0;
            }
            $qty[$stock->getData('ARTNO')] += $stock->getData('VOLUME');
        }
        return $qty;
    }

    public function getPriceTable($pricelistNo)
    {
        $collection = $this->buumPriceFactory->create()->getCollection();
        $collection->addFieldToFilter('PRICELISTNO', $pricelistNo);
        $priceTable = [];
        foreach ($collection as $price) {
            $priceTable[$price->getData('ARTNO')] = $price->getData('PRICE');
        }
        return $priceTable;
    }
    public function getDefaultBarcodes()
    {
        $collection = $this->buumBarcodeFactory->create()->getCollection();
        $collection->addFieldToFilter('ISDEFAULT', 'T');
        $defaultBarcodes = [];
        foreach ($collection as $bc) {
            $defaultBarcodes[$bc->getData('ARTNO')] = $bc->getData('BARCODE');
        }
        return $defaultBarcodes;
    }
}

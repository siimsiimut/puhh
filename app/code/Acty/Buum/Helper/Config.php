<?php
namespace Acty\Buum\Helper;

class Config extends \Magento\Framework\App\Helper\AbstractHelper
{
    public function getConfig($key)
    {
        return $this->scopeConfig->getValue($key, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getShippingMethodBuumId($shippingMethod)
    {
        $data = $this->getConfig('buum/invoice/shipping_methods_relations');
        $shippingMethodParts = explode('_', $shippingMethod, 2);
        foreach (json_decode($data, true) as $row) {
            if ($shippingMethodParts[0] == $row['shipping_method'] && $row['buum_product_id'] != null) {
                return $row['buum_product_id'];
            }
        }
        return false;
    }

    public function getInvoiceDocType()
    {
        return $this->getConfig('buum/invoice/doc_type');
    }

    public function getInvoiceCustomerCode()
    {
        return $this->getConfig('buum/invoice/cust_code');
    }

    public function getLatvianInvoiceCustomerCode()
    {
        return $this->getConfig('buum/invoice/lat_cust_code');
    }

    public function getLatvianWebsiteId()
    {
        return $this->getConfig('buum/invoice/lat_web_id');
    }

    public function isCronEnabled()
    {
        return $this->getConfig('buum/product_import/enable_crons');
    }

    public function isInvoiceSendingEnabled()
    {
        return $this->getConfig('buum/invoice/enable');
    }

    public function getNewProductsCategoryId()
    {
        return $this->getConfig('buum/product_import/category_id');
    }

    public function getPricelistId()
    {
        return -10;
    }

    public function getSpecialPricelistId()
    {
        return 38;
    }

    public function getLatvianPricelistId()
    {
        return 465;
    }

    public function getEstonianStoreIds()
    {
        return [1,2];
    }

    public function getLatvianStoreIds()
    {
        return [3,4,5];
    }

    public function getLangId()
    {
        return 1;
    }

    public function getEnglishStoreIds()
    {
        $string = $this->getConfig('buum/product_import/eng_stores');

        return explode(",", $string);
    }

    public function getLatvianStocks()
    {
        $string = $this->getConfig('buum/product_import/lat_stocks');

        return explode(",", $string);
    }

    public function getLatvianStockMin()
    {
        return $this->getConfig('buum/product_import/lat_min_amount');
    }

    public function getStockRelations()
    {
        return json_decode($this->getConfig('buum/stock/buum_stock_relations'), true);
    }

    public function getDefaultWarehouse()
    {
        return $this->getConfig('buum/stock/default_stock');
    }

    public function getLVDefaultWarehouse()
    {
        return $this->getConfig('buum/stock/default_stock_lv');
    }

    public function getTemplateStoreConfirmation()
    {
        return $this->getEmailTemplate('buum/email/template_store_confirmation', 'buum_email_template_store_confirmation');
    }

    protected function getEmailTemplate($path, $default)
    {
        $templateId = $this->getConfig($path);
        if ($templateId) {
            return $templateId;
        }
        return $default;
    }
}

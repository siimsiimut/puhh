<?php
namespace Acty\Buum\Helper;

use \Magento\Framework\App\Helper\Context;

class Warehouse extends \Magento\Framework\App\Helper\AbstractHelper
{

    public function __construct(
        Config $config,
        \Magento\Framework\App\Helper\Context $context
    ) {
        $this->config = $config;
        parent::__construct($context);
    }

    public function getWarehouseData($order){
        $id = $this->getShopWarehouse($order);
        $email = $this->getWarehouseEmail($id);
        return ['id' => $id, 'email' => $email];
    }

    protected function getWarehouseEmail($id){
        $data = $this->config->getStockRelations();
        foreach ($data as $row){
            if($row['buum_stock_id'] == $id){
                return $row['stock_email'];
            }
        }
        return false;
    }

    protected function getShopWarehouse($order)
    {
        $defaultStock = $this->config->getDefaultWarehouse();
        $shippingMethodParts = explode('_', $order->getShippingMethod(), 2);
        if ($shippingMethodParts[0] == 'actydelivery') {
            return $shippingMethodParts[1];
        } elseif ($shippingMethodParts[0] == 'tablerate'){
            $data = $this->getFilteredStocks('Tallinn', 'city_group');
        } else {
            $data = $this->config->getStockRelations();
        }
        $allowedData = $this->getAllowedData($order->getAllItems(), $data);
        
        /* 
            ONLY USED FOR HOLIDAY SEASON, DISABLED
            
            push orders to CFS (defaultstock) right away for holiday season
        
        if (in_array($defaultStock, $allowedData['allowed'])){
            return $defaultStock;
        }

        */
        
        if ($allowedData['allowed']){
            return $this->getStockByPriority($allowedData, $defaultStock);
        }
        return $defaultStock;
    }

    protected function getStockByPriority($data, $defaultStock) {
        $priorities = [];
        foreach($data['allowed'] as $stock){
            if ($stock != $defaultStock) {
                $priorities[$stock] = $data['stock_priority'][$stock];
            }
        }

        //in some edge cases only the defaultstock is allowed
        if (count($priorities) == 0) {
            foreach($data['allowed'] as $stock){
                if ($stock == $defaultStock) {
                    $priorities[$stock] = $data['stock_priority'][$stock];
                }
            }
        }
        
        $maxs = array_keys($priorities, max($priorities));
        return $maxs[0];
    }

    protected function getAllowedData($items, $data)
    {
        $allowed = [];
        $stockPriority = [];
        $first = true;
        foreach($items as $item){
            if($item->getHasChildren()){
                continue;
            }
            $qty = $item->getQtyOrdered();
            $warehouses = $item->getProduct()->getWarehouseQuantity() ? json_decode($item->getProduct()->getWarehouseQuantity(), true) : [];

            $itemAllowed = [];
            foreach ($data as $row){
                if(array_key_exists($row['buum_stock_id'], $warehouses)){
                    //perma disable järve keskus buum_stock_id = 27
                    //perma disable stroomi keskus buum_stock_id = 14
                    if($warehouses[$row['buum_stock_id']] >= $qty && $row['buum_stock_id'] != "27" && $row['buum_stock_id'] != "14") {
                        $itemAllowed[] = $row['buum_stock_id'];
                        if(array_key_exists($row['buum_stock_id'], $stockPriority)){
                            $previous = $stockPriority[$row['buum_stock_id']];
                            $stockPriority[$row['buum_stock_id']] = $warehouses[$row['buum_stock_id']] + $previous;
                        }
                        else{
                            $stockPriority[$row['buum_stock_id']] = $warehouses[$row['buum_stock_id']];
                        }
                    }
                }
            }

            if($first === true){
                $allowed = $itemAllowed;
                $first = false;
            }
            else{
                $allowed = array_intersect($allowed, $itemAllowed);
            }
        }
        return ['allowed' => $allowed, 'stock_priority' => $stockPriority];
    }

    /**
     * @param string $needle
     * @param string $attribute
     * @return array
     */
    public function getFilteredStocks(string $needle, string $attribute)
    {
        $stocks = $this->config->getStockRelations();
        $filteredResult = [];
        foreach ($stocks as $stock) {
            if (array_key_exists($attribute, $stock) && strtolower($stock[$attribute]) == strtolower($needle)) {
                $filteredResult[] = $stock;
            }
        }
        return $filteredResult;
    }


}

<?php
namespace Acty\Buum\Helper;

use \Magento\Framework\App\Helper\Context;
use \Acty\Buum\Model\Buum\StockCodFactory;
use \Acty\Buum\Model\StockFactory;

class Stock extends \Magento\Framework\App\Helper\AbstractHelper
{

    public function __construct(
        Config $config,
        \Acty\Buum\Model\StockFactory $stockFactory,
        \Acty\Buum\Model\Buum\StockFactory $buumStockFactory,
        \Magento\Framework\FlagManager $flagManager,
        \Magento\Framework\App\Helper\Context $context
    ) {
        $this->config = $config;
        $this->stockFactory = $stockFactory;
        $this->buumStockFactory = $buumStockFactory;
        $this->flagManager = $flagManager;
        parent::__construct($context);
    }

    public function getEnabledFrontendStocksData()
    {
        $stores = $this->config->getStockRelations();
        $ret = [];
        foreach ($stores as $store) {
            $ret[$store['buum_stock_id']] = explode(',', $store['sub_stock_ids']);
        }
        return $ret;
    }

    public function getBuumQtyGrouped($stocksFilter = false, $onlyChanged = false)
    {
        $collection = $this->buumStockFactory->create()->getCollection();
        $collection->addFieldToFilter('STOCKNO', ['in' => [ $this->getEnabledStocks($stocksFilter) ]]);
        $collection->getSelect()->columns(
                                [
                                    'VOLUME' => new \Zend_Db_Expr('SUM(VOLUME)'),
                                    'LASTEDIT' => new \Zend_Db_Expr('MAX(LASTCHANGE)'),
                                ])->group('ARTNO')
                                ->order('LASTEDIT DESC');

        if ($onlyChanged && $lastRun = $this->getLastRun()) {
            $collection->getSelect()->having('`LASTEDIT` >= (?)', $lastRun);
        }

        if ($collection->count()) {
            $this->setLastRun($collection->getFirstItem()->getData('LASTEDIT'));
        }

        return $this->collectionToTable($collection);
    }

    protected function collectionToTable($collection)
    {
        $products = [];
        foreach ($collection as $buumrow) {
            $products[$buumrow->getData('ARTNO')] = $buumrow->getData('VOLUME');
        }
        return $products;
    }

    protected function getEnabledStocks($stocksFilter)
    {
        $collection = $this->stockFactory->create()->getCollection();
        if ($stocksFilter) {
            $collection->addFieldToFilter($stocksFilter, 1);
        }
        return $collection->getColumnValues('stock_no');
    }

    protected function getLastRun()
    {
        return $this->flagManager->getFlagData('buum_last_qty_import');
    }

    protected function setLastRun($date)
    {
        return $this->flagManager->saveFlag('buum_last_qty_import', $date);
    }
}

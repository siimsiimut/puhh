<?php
namespace Acty\Buum\Model;

class CacheCleaner
{
    public function __construct(
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
    ) {
        $this->_cacheFrontendPool = $cacheFrontendPool;
    }

    public function flushAll()
    {
        $caches = [];
        foreach ($this->_cacheFrontendPool as $cacheFrontend) {
            $caches[] = $cacheFrontend;
        }

        foreach (array_reverse($caches) as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
    }
}

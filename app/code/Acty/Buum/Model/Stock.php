<?php
namespace Acty\Buum\Model;
class Stock extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'acty_buum_stock';

    protected $_cacheTag = 'acty_buum_stock';

    protected $_eventPrefix = 'acty_buum_stock';

    protected function _construct()
    {
        $this->_init('Acty\Buum\Model\ResourceModel\Stock');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}

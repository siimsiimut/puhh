<?php
namespace Acty\Buum\Model\Buum;

class Payment extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'acty_buum_payment';

    protected $_cacheTag = 'acty_buum_payment';

    protected $_eventPrefix = 'acty_buum_payment';

    protected function _construct()
    {
        $this->_init('Acty\Buum\Model\Buum\ResourceModel\Payment');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}

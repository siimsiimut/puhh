<?php
namespace Acty\Buum\Model\Buum;
class Invoice extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'acty_buum_invoice';

    protected $_cacheTag = 'acty_buum_invoice';

    protected $_eventPrefix = 'acty_buum_invoice';

    //$fields =
             //INVOICE - increment_id
            //CUSTCODE - backendist
           //TYPE -> backendist
          //CUSTNAME -> customer name
         //PHONE telephone
        //EMAIL email
       //ADDRESS1 - aadress . kui pole siis NULL või -?
      //ADDRESS2 - Küla/Linn/Maakond
     //ADDRESS3 - customer_name
    //COMMENT - transpordi meetod

          //OUTITEMS
         //INVNO
        //ARTNO product buum id
       //QTY - qty
      //AMOUNT - qty*price
     //SIZENO ??
    //RCOMMENT

        //PAYMENTS
       //INVNO
      //LINENO -> 0
     //AMOUNT -> order total
    //PAYDOC

    protected function _construct()
    {
        $this->_init('Acty\Buum\Model\Buum\ResourceModel\Invoice');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}

<?php
namespace Acty\Buum\Model\Buum;

class Barcode extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'acty_buum_barcode_log';

    protected $_cacheTag = 'acty_buum_barcode_trans_log';

    protected $_eventPrefix = 'acty_buum_barcode_trans_log';

    protected function _construct()
    {
        $this->_init('Acty\Buum\Model\Buum\ResourceModel\Barcode');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}

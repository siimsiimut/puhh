<?php
namespace Acty\Buum\Model\Buum;
class Item extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'acty_buum_item';

    protected $_cacheTag = 'acty_buum_item';

    protected $_eventPrefix = 'acty_buum_item';

    protected function _construct()
    {
        $this->_init('Acty\Buum\Model\Buum\ResourceModel\Item');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }


    public function getStockName()
    {
        return $this->getData('STOCKNAME');
    }

    public function getStockCode()
    {
        return $this->getData('STOCKCODE');
    }

    public function getStockNo()
    {
        return $this->getData('STOCKNO');
    }
}

<?php
namespace Acty\Buum\Model\Buum\ResourceModel;

class Invoice extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        $connectionName = 'buum'
    ) {
        parent::__construct($context, $connectionName);
    }

    protected function _construct()
    {
        $this->_init('INVOICE', 'INVNO');
    }

    public function afterDelete(\Magento\Framework\DataObject $object)
    {
        print_r('afterdelete');
        parent::afterDelete();
    }
}

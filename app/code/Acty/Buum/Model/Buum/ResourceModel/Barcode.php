<?php
namespace Acty\Buum\Model\Buum\ResourceModel;


class Barcode extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        $connectionName = 'buum'
    ) {
        parent::__construct($context, $connectionName);
    }

    protected function _construct()
    {
        $this->_init('BARCODE', 'LINENO');
    }
}

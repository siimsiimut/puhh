<?php
namespace Acty\Buum\Model\Buum\ResourceModel;

class TransLog extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        $connectionName = 'buum'
    ) {
        parent::__construct($context, $connectionName);
    }

    protected function _construct()
    {
        $this->_init('TRANS_LOG', 'TRANS_LOG_ID');
    }
}

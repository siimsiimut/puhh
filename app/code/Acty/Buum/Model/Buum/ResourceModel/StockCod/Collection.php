<?php
namespace Acty\Buum\Model\Buum\ResourceModel\StockCod;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'STOCKNO';
    protected $_eventPrefix = 'acty_buum_stockcod_collection';
    protected $_eventObject = 'stockcod_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Acty\Buum\Model\Buum\StockCod', 'Acty\Buum\Model\Buum\ResourceModel\StockCod');
    }
}

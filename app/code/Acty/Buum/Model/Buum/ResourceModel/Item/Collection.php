<?php
namespace Acty\Buum\Model\Buum\ResourceModel\Item;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'ARTNO';
    protected $_eventPrefix = 'acty_buum_item_collection';
    protected $_eventObject = 'item_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Acty\Buum\Model\Buum\Item', 'Acty\Buum\Model\Buum\ResourceModel\Item');
    }
}

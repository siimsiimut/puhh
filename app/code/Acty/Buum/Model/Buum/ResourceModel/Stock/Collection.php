<?php
namespace Acty\Buum\Model\Buum\ResourceModel\Stock;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'LINENO';
    protected $_eventPrefix = 'acty_buum_stock_collection';
    protected $_eventObject = 'stock_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Acty\Buum\Model\Buum\Stock', 'Acty\Buum\Model\Buum\ResourceModel\Stock');
    }
}

<?php
namespace Acty\Buum\Model\Buum;

class TransLog extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'acty_buum_trans_log';

    protected $_cacheTag = 'acty_buum_trans_log';

    protected $_eventPrefix = 'acty_buum_trans_log';

    protected function _construct()
    {
        $this->_init('Acty\Buum\Model\Buum\ResourceModel\TransLog');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}

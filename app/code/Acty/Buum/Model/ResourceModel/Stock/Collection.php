<?php
namespace Acty\Buum\Model\ResourceModel\Stock;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    protected $_eventPrefix = 'acty_buum_stock_collection';
    protected $_eventObject = 'stock_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Acty\Buum\Model\Stock', 'Acty\Buum\Model\ResourceModel\Stock');
    }

}


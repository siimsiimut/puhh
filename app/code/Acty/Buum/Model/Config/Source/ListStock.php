<?php
namespace Acty\Buum\Model\Config\Source;

class ListStock implements \Magento\Framework\Option\ArrayInterface
{
    public function __construct(
        \Acty\Buum\Helper\Config $config
    ) {
        $this->config = $config;
    }

    public function toOptionArray()
    {
        $arr = $this->toArray();
        $ret = [];
        foreach ($arr as $key => $value) {
            $ret[] = [
                'value' => $key,
                'label' => $value
            ];
        }
        return $ret;
    }

    public function toArray()
    {
        $stores = $this->config->getStockRelations();
        foreach ($stores as $store) {
            $options[$store['buum_stock_id']] = $store['store_name'];
        };
        return $options;
    }
}

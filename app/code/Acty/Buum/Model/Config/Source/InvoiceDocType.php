<?php
namespace Acty\Buum\Model\Config\Source;

class InvoiceDocType implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 100, 'label' => __('Arve')],
            ['value' => 103, 'label' => __('Tellimus')],
            ['value' => 107, 'label' => __('Tellimus-arve')],
            ['value' => 109, 'label' => __('Broneering')]
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [
            100 => __('Arve'),
            103 => __('Tellimus'),
            107 => __('Tellimus-arve'),
            109 => __('Broneering')
        ];
    }
}

<?php
namespace Acty\Buum\Model;

use Magento\Indexer\Model\IndexerFactory;

class Indexer
{
    protected $priceIndexers = [
                            'catalog_product_attribute',
                            'catalog_product_price',
                            'catalogrule_product',
                            'catalogsearch_fulltext',
                        ];

    protected $indexerFactory;

    public function __construct(IndexerFactory $indexerFactory)
    {
        $this->indexerFactory = $indexerFactory;
    }

    public function indexPriceIndexes()
    {
        $log = '';
        foreach ($this->priceIndexers as $indexerId) {
            $start = microtime(true);
            $indexer = $this->indexerFactory->create();
            $indexer->load($indexerId);
            $indexer->reindexAll();
            $log .= "\nIndex: " . $indexerId . ' time: '. (microtime(true) - $start);
        }
        return $log;
    }
}

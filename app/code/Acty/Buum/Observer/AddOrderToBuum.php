<?php
namespace Acty\Buum\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class AddOrderToBuum implements ObserverInterface
{
    public function __construct(
        \Acty\Buum\Sync\Invoice $invoice,
        \Acty\Buum\Helper\Config $config
    ) {
        $this->invoice = $invoice;
        $this->config  = $config;
    }

    public function execute(EventObserver $observer)
    {
        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();
        $order->addStatusHistoryComment(null, 'makstud');
        $order->setState('processing');
        $order->setStatus('makstud');

        if (!$this->config->isInvoiceSendingEnabled()) {
            return;
        }
        $this->invoice->pushOrderToBuum($order);
    }
}

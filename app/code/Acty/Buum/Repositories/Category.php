<?php

namespace Acty\Buum\Repositories;

use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;


class Category
{
    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    public function getCategories($attributes)
    {
        $collection = $this->collectionFactory->create()->addAttributeToSelect($attributes)->load();
        return $collection;
    }
}

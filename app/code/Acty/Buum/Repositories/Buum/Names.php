<?php
namespace Acty\Buum\Repositories\Buum;

use Magento\Framework\App\ResourceConnection;
use \Acty\Buum\Helper\Config;

class Names
{
    public function __construct(
        ResourceConnection $resourceConnection,
        Config $buumConfig
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->buumConfig = $buumConfig;
    }

    public function getAll()
    {
        $connection = $this->resourceConnection->getConnection('buum');

        $sql = 'SELECT ARTNO, NAME FROM `ARTNAMES` WHERE ARTNAMES.LANGNO = ' . $this->buumConfig->getLangId() . '';
        $result = $connection->fetchAll($sql);
        $arr = [];
        foreach ($result as $row) {
            $arr[$row['ARTNO']] = $row;
        }

        return $arr;
    }
}

<?php
namespace Acty\Buum\Repositories\Buum;

use Magento\Framework\App\ResourceConnection;
use \Acty\Buum\Helper\Config;

class Price
{
    public function __construct(
        ResourceConnection $resourceConnection,
        Config $buumConfig
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->buumConfig = $buumConfig;
    }

    public function getAll()
    {
        $connection = $this->resourceConnection->getConnection('buum');

        $sql = 'SELECT
          ITEM.ARTNO,
          ITEM.ARTCODE,
          ORIGINALPRICES.PRICE AS ORIGINAL_PRICE,
          PRICES.PRICE,
          LVPRICES.PRICE AS LV_PRICE
        FROM `ITEM`
        LEFT JOIN PRICES ON ITEM.ARTNO = PRICES.ARTNO AND PRICES.PRICELISTNO = '.$this->buumConfig->getSpecialPricelistId().'
        INNER JOIN PRICES ORIGINALPRICES ON ITEM.ARTNO = ORIGINALPRICES.ARTNO AND ORIGINALPRICES.PRICELISTNO = ' . $this->buumConfig->getPricelistId() . '
        LEFT JOIN PRICES LVPRICES ON ITEM.ARTNO = LVPRICES.ARTNO AND LVPRICES.PRICELISTNO = ' . $this->buumConfig->getLatvianPricelistId() . '
        WHERE ITEM.STOCKITEM = "T" AND ORIGINALPRICES.PRICE > 0
                GROUP BY ITEM.ARTNO';
        $result = $connection->fetchAll($sql);
        $arr = [];
        foreach ($result as $row) {
            $arr[$row['ARTNO']] = $row;
        }

        return $arr;
    }
}

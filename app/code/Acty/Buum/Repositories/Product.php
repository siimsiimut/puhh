<?php

namespace Acty\Buum\Repositories;

use Magento\Framework\App\ResourceConnection;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use \Acty\Buum\Helper\Config;

class Product
{
    const SIMPLE_ATTRIBUTE_SET = 4;

    protected $websites = [1,2];

    protected $resourceConnection;

    protected $db;

    protected $productFactory;

    protected $categoryLinkManagement;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product $productResourceModel,
        ResourceConnection $resourceConnection,
        ProductFactory $productFactory,
        StockRegistryInterface $stockRegistry,
        Config $buumConfig
        ) {
        $this->productResourceModel = $productResourceModel;
        $this->resourceConnection = $resourceConnection;
        $this->db = $this->resourceConnection->getConnection();
        $this->productFactory = $productFactory;
        $this->stockRegistry = $stockRegistry;
        $this->buumConfig = $buumConfig;
    }

    public function getAllPrices(){
        $query = "SELECT `catalog_product_entity_decimal`.`entity_id`, 
        `catalog_product_entity`.`sku`,
        `eav_attribute`.`attribute_code`,
        `catalog_product_entity_decimal`.`store_id`,
        `catalog_product_entity_decimal`.`value`
        FROM `catalog_product_entity_decimal`
        LEFT JOIN `eav_attribute` ON `catalog_product_entity_decimal`.`attribute_id` = `eav_attribute`.`attribute_id`
        LEFT JOIN `catalog_product_entity` ON `catalog_product_entity_decimal`.`entity_id` = `catalog_product_entity`.`entity_id`
        WHERE `eav_attribute`.`attribute_code`
        IN (
        'price', 'special_price'
        )
        AND `catalog_product_entity`.`sku` IS NOT NULL
        ORDER BY `catalog_product_entity_decimal`.`entity_id` ASC";

        $results = $this->db->fetchAll($query);

        $prices = $this->filterPrices($results);

        return $prices;
    }

    private function filterPrices($results)
    {
        $prices = [];
        foreach($results as $row){
            $prices[$row['sku']][$row['store_id']][$row['attribute_code']] = $row['value'];
        }
        return $prices;
    }

    public function getSkus()
    {
        $query = sprintf(
            "select sku from %s order by sku",
            $this->resourceConnection->getTableName('catalog_product_entity')
        );

        $skus = [];
        $results = $this->db->fetchAll($query);

        foreach ($results as $row) {
            $skus[$row['sku']] = $row['sku'];
        }
        return $skus;
    }

    public function storeSimpleProduct($data, $engName, $stores)
    {
        $product = $this->productFactory->create();
        $product->setTypeId(Type::TYPE_SIMPLE)
            ->setStoreId(0)
            ->setWebsiteIds($this->websites)
            ->setAttributeSetId(self::SIMPLE_ATTRIBUTE_SET)
            ->setVisibility(Visibility::VISIBILITY_BOTH)
            ->setStatus(Status::STATUS_DISABLED)
            ->setTaxClassId(4);

        $product->addData($data);
        $product->save();

        if($engName != null){
            foreach($stores as $store){
                $this->saveProductName($product->getId(), $engName, $store);
            }
        }
        return $product;
    }

    protected function saveProductName($productId, $engName, $store){
        $product = $this->productFactory->create()->setStoreId($store)->load($productId);
        $product->setName($engName);
        $product->save();
    }

    public function getAll($attributes)
    {
        $collection = $this->productFactory->create()->getCollection()->addAttributeToSelect($attributes);
        $collection->setFlag('has_stock_status_filter', true);
        return $collection;
    }

    public function storeQty($sku, $qty)
    {
        $stockItem = $this->stockRegistry->getStockItemBySku($sku);
        $stockItem->setQty($qty);
        $stockItem->setIsInStock((bool)$qty);
        $this->stockRegistry->updateStockItemBySku($sku, $stockItem);
    }


    public function savePrices($product, $productPrices, $price, $specialPrice, $latvianPrice)
    {
        $changedPrice = $this->saveProductPrices($product, $productPrices, $price, null, [0]);

        $changedSpecialPrice = $this->saveProductPrices($product, $productPrices, $price, $specialPrice, $this->buumConfig->getEstonianStoreIds());

        $changedLatvianPrice = $this->saveProductPrices($product, $productPrices, $price, $latvianPrice, $this->buumConfig->getLatvianStoreIds());

        return $changedPrice or $changedSpecialPrice or $changedLatvianPrice;
    }

    protected function saveProductPrices($product, $productPrices, $price, $specialPrice, $stores){
        if ($specialPrice === $price) {
            $specialPrice = null;
        }
        $changedPriceArray = [];
        $changedSpecialPriceArray = [];

        foreach($stores as $store){
            $changedPriceArray[] = $this->savePrice($product, $productPrices, 'price', $price, $store);
            $changedSpecialPriceArray[] = $this->savePrice($product, $productPrices, 'special_price', $specialPrice, $store);
        }
        $changedPrice = in_array(true, $changedPriceArray) ? true : false;
        $changedSpecialPrice = in_array(true, $changedSpecialPriceArray) ? true : false;
        //print_r($price." ".$specialPrice." ".$changedPrice." ".$changedSpecialPrice."\n");
        return $changedPrice or $changedSpecialPrice;
    }

    private function savePrice($product, $productPrices, $attributeCode, $value, $store)
    {   
        if($this->checkChange($productPrices, $attributeCode, $value, $store) == true){
            $productFactory = $this->productFactory->create();
            $this->productResourceModel->load($productFactory, $product->getId());
            $productFactory->setStoreId($store);
            $productFactory->setData($attributeCode, $value);
            $this->productResourceModel->saveAttribute($productFactory, $attributeCode);
            return true;
        }
       return false;
    }

    private function checkChange($productPrices, $attributeCode, $value, $store)
    {   
        if(array_key_exists($store, $productPrices)){
            if(array_key_exists($attributeCode, $productPrices[$store])){
                if($productPrices[$store][$attributeCode] !== $value){
                    return true;
                }
                return false;
            }
            elseif($attributeCode == 'special_price' && $value == null){
                return false;
            }
            return true;
        }
        return true;
    }

    protected function saveAttribute($product, $attributeCode, $value)
    {
        if ($product->getData($attributeCode) != $value) {
            $product->setData($attributeCode, $value);
            $product->getResource()->saveAttribute($product, $attributeCode);
            return true;
        }
        return false;
    }

    public function disable($product)
    {
        $product->setStoreId(0);
        return $this->saveAttribute($product, 'status', Status::STATUS_DISABLED);
    }
}

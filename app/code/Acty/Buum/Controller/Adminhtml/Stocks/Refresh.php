<?php
namespace Acty\Buum\Controller\Adminhtml\Stocks;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Refresh extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $stockHelper;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        \Acty\Buum\Sync\Stocks $stockHelper
    ) {
        parent::__construct($context);
        $this->stockHelper = $stockHelper;
    }
    /**
     * Check the permission to run it
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Acty_Buum::stocks');
    }

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $this->stockHelper->sync();
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/index');
    }
}

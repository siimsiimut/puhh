<?php
namespace Acty\Buum\Block\System\Config\Form\Field;

use Magento\Framework\Data\Form\Element\AbstractElement;

class ShippingMethodRelations extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
    /**
     * Grid columns
     *
     * @var array
     */
    protected $_columns = [];
    protected $_customerGroupRenderer;
    /**
     * Enable the "Add after" button or not
     *
     * @var bool
     */
    protected $_addAfter = false;
    /**
     * Label of add button
     *
     * @var string
     */
    protected $_addButtonLabel;
    /**
     * Check if columns are defined, set template
     *
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->_addButtonLabel = __('Add');
    }
    /**
     * Prepare to render
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        $this->addColumn('shipping_method', ['label' => __('Magento shipping_method')]);
        $this->addColumn('buum_product_id', ['label' => __('Buum product id')]);
        $this->addColumn('buum_product_name', ['label' => __('Description')]);
        #$this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

    protected function _prepareArrayRow(\Magento\Framework\DataObject $row)
    {
        $options = [];
        $row->setData('option_extra_attrs', $options);
    }
}

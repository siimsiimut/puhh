<?php
namespace Acty\Buum\Block\System\Config\Form\Field;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Exception\LocalizedException;
use Acty\Buum\Block\Adminhtml\Form\Field\FrontVisibleColumn;

class StockRelations extends \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray
{
    private $visibleRenderer;
    /**
     * Grid columns
     *
     * @var array
     */
    protected $_columns = [];
    protected $_customerGroupRenderer;
    /**
     * Enable the "Add after" button or not
     *
     * @var bool
     */
    protected $_addAfter = false;
    /**
     * Label of add button
     *
     * @var string
     */
    protected $_addButtonLabel;
    /**
     * Check if columns are defined, set template
     *
     * @return void
     */
    protected function _construct() {
        parent::_construct();
        $this->_addButtonLabel = __('Add');
    }
    /**
     * Prepare to render
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        $this->addColumn('store_name', ['label' => __('Shop Name')]);
        $this->addColumn('buum_stock_id', ['label' => __('Buum id')]);
        $this->addColumn('sub_stock_ids', ['label' => __('Buum sub stock ids (comma separated)')]);
        $this->addColumn('city_group', ['label' => __('City')]);
        $this->addColumn('stock_email', ['label' => __('E-mail')]);
        $this->addColumn('front_visible', [
            'label' => __('Visible in Front'),
            'renderer' => $this->getVisibleRenderer()
        ]);
        $this->_addButtonLabel = __('Add');
    }

    protected function _prepareArrayRow(\Magento\Framework\DataObject $row)
    {
        $options = [];

        $frontVisible = $row->getFrontVisible();
        if ($frontVisible !== null) {
            $options['option_' . $this->getVisibleRenderer()->calcOptionHash($frontVisible)] = 'selected="selected"';
        }

        $row->setData('option_extra_attrs', $options);
    }

    private function getVisibleRenderer()
    {
        if (!$this->visibleRenderer) {
            $this->visibleRenderer = $this->getLayout()->createBlock(
                FrontVisibleColumn::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->visibleRenderer;
    }
}

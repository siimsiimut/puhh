<?php
namespace Acty\Buum\Block\Product;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Registry;

class Availability extends Template
{
    protected $productWarehouses;

    public function __construct(
        \Acty\Buum\Helper\Config $config,
        Context $context,
        Registry $registry
    ) {
        $this->config = $config;
        $this->registry = $registry;
        parent::__construct($context);
    }

    public function getWarehouses()
    {
        $stores = $this->config->getStockRelations();
        $warehouses = [];
        foreach ($stores as $store) {
            if($store['front_visible'] == 1){
                $warehouse = [
                    'name' => $store['store_name'],
                    'qty'  => $this->getQty($store['buum_stock_id'])
                ];
                $warehouses[$store['city_group']][] = $warehouse;
            }
        }
        return $warehouses;
    }

    protected function initProductWarehouses()
    {
        return json_decode($this->registry->registry('current_product')->getData('warehouse_quantity'), true);
    }

    protected function getProductWarehouses()
    {
        if (!$this->productWarehouses) {
            $this->productWarehouses = $this->initProductWarehouses();
        }
        return $this->productWarehouses;
    }

    protected function getQty($storeNo)
    {
        $qty = 0;
        $productWarehouses = $this->getProductWarehouses();
        if (is_array($productWarehouses) && array_key_exists($storeNo, $productWarehouses)) {
            $qty = intval($productWarehouses[$storeNo]);
        }
        if ($qty < 0) {
            $qty = 0;
        }
        return $qty > 5 ? '5+':$qty;
    }
}

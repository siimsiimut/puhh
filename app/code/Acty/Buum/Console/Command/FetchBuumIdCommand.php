<?php
namespace Acty\Buum\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\State;
use \Acty\Buum\Sync\BuumId;

class FetchBuumIdCommand extends Command
{
    public function __construct(
        State $state,
        BuumId $buumId,
        $name = null
    ) {
        $this->state = $state;
        $this->buumId = $buumId;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('buum:fetch:buumid')
            ->setDescription('Refresh buum_id values in magento products');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        $this->buumId->sync();
        //$this->buumId->printMissingBuumIds();
    }
}

<?php
namespace Acty\Buum\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\State;
use \Acty\Buum\Sync\NewProducts;

class FetchNewProducts extends Command
{
    public function __construct(
        State $state,
        NewProducts $newProducts,
        $name = null
    ) {
        $this->state = $state;
        $this->newProducts = $newProducts;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('buum:fetch:newproducts')
            ->setDescription('Fetch new products');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        print_r($this->newProducts->sync());
    }
}

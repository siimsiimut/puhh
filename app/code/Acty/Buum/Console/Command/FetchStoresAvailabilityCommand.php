<?php
namespace Acty\Buum\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
class FetchStoresAvailabilityCommand extends Command
{
    public function __construct(
        \Magento\Framework\App\State $state,
        \Acty\Buum\Sync\StoresAvailability $stocksQty,
        $name = null
    ) {
        $this->state = $state;
        $this->stocksQty = $stocksQty;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('buum:fetch:storesAvailability')
            ->setDescription('Refresh buum stocks qty in product json field');
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        $this->stocksQty->sync();
    }
}

<?php
namespace Acty\Buum\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FetchStocksCommand extends Command
{
    public function __construct(
        \Magento\Framework\App\State $state,
        \Acty\Buum\Sync\Stocks $stocks,
        $name = null
    ) {
        $this->state = $state;
        $this->stocks = $stocks;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('buum:fetch:stocks')
            ->setDescription('Fetch buum stocks');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        $this->stocks->sync();
    }
}

<?php
namespace Acty\Buum\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class FetchQtyCommand extends Command
{
    const ONLY_CHANGED_QTY_OPTION = 'changed';
    const FORCE_QTY_REFRESH       = 'force';

    public function __construct(
        \Magento\Framework\App\State $state,
        \Acty\Buum\Sync\Qty $qty,
        $name = null
    ) {
        $this->state = $state;
        $this->qty = $qty;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('buum:fetch:qty')
            ->setDescription('Get fresh product qty from buum')
            ->setDefinition([
                new InputOption(
                    self::ONLY_CHANGED_QTY_OPTION,
                    null,
                    InputOption::VALUE_NONE,
                    'refresh only products qty what is changed after last qty import'
                ),
                new InputOption(
                    self::FORCE_QTY_REFRESH,
                    null,
                    InputOption::VALUE_NONE,
                    'force qty refresh even if the qty has not changed'
                )
            ]);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        $onlyChanged = $input->getOption(self::ONLY_CHANGED_QTY_OPTION);
        $force = ($input->getOption(self::FORCE_QTY_REFRESH) == 1) ? true: false;
        if ($onlyChanged) {
            $this->qty->syncChanged();
        } else {
            $this->qty->syncAll($force);
        }
    }
}

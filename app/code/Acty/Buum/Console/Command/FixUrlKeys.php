<?php
namespace Acty\Buum\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Magento\Framework\App\State;
use \Acty\Buum\Sync\UrlKeys;

class FixUrlKeys extends Command
{
    const STORE = 'store'; // key of parameter
    const TYPE = 'type'; // key of parameter
    const LEVEL = 'level'; // key of parameter
    const NOCHECK = 'nocheck'; // key of parameter

    public function __construct(
        State $state,
        UrlKeys $urlKeys,
        $name = null
    ) {
        $this->state = $state;
        $this->urlKeys = $urlKeys;
        parent::__construct($name);
    }

    protected function configure()
    {
        $options = [
            /* new InputOption(
                self::STORE, // the option name
                '-s', // the shortcut
                InputOption::VALUE_REQUIRED, // the option mode
                'Store views to update, comma separated' // the description
            ),
            new InputOption(
                self::TYPE, // the option name
                '-t', // the shortcut
                InputOption::VALUE_REQUIRED, // the option mode
                'Urlkey fix type, product or category' // the description
            ),
            new InputOption(
                self::LEVEL, // the option name
                '-l', // the shortcut
                InputOption::VALUE_REQUIRED, // the option mode
                'Urlkey fix category by level' // the description
            ),
            new InputOption(
                self::NOCHECK, // the option name
                '-a', // the shortcut
                InputOption::VALUE_REQUIRED, // the option mode
                'Urlkey fix all categories' // the description
            ), */
        ];
        $this->setName('buum:fix:urlkeys');
        $this->setDescription('Fix product (store) or category (store, level, all) url keys');
        $this->setDefinition($options);

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        $this->urlKeys->fixmyproducts();
        return;
        if($input->getOption(self::STORE) == null){
            print_r("No store entered \n");
        }
        elseif($input->getOption(self::TYPE) == null || !in_array($input->getOption(self::TYPE), ['product', 'category'])){
            print_r("Wrong type entered \n");
        }
        else{
            $stores = explode(",", $input->getOption(self::STORE));
            $type = $input->getOption(self::TYPE);
            $level = $input->getOption(self::LEVEL);
            $nocheck = $input->getOption(self::NOCHECK);
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
            $this->urlKeys->fix($stores, $type, $level, $nocheck);
        }
    }
}

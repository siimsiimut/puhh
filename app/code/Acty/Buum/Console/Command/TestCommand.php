<?php
namespace Acty\Buum\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\State;
use \Acty\Buum\Model\Buum\InvoiceFactory;
use \Acty\Buum\Sync\Qty;

class TestCommand extends Command
{
    public function __construct(
        State $state,
        Qty $qtySync,
        $name = null
    ) {
        $this->state = $state;
        $this->qtySync = $qtySync;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('buum:test')
            ->setDescription('test');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        print_r($this->qtySync->getProductsWithoutBuumId());
        //$buumInvoice = $this->buumInvoiceFactory->create();
        //$buumInvoice->load('MEP000000009', 'INVOICE');
        //print_r($buumInvoice->getData('EMAIL'));
        //$buumInvoice->delete();
    }
}

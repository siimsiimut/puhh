<?php
namespace Acty\Buum\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FetchPricesCommand extends Command
{
    public function __construct(
        \Magento\Framework\App\State $state,
        \Acty\Buum\Sync\Price $price,
        $name = null
    ) {
        $this->state = $state;
        $this->price = $price;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('buum:fetch:prices')
            ->setDescription('Refresh all product prices');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        $this->price->syncAll();
    }
}

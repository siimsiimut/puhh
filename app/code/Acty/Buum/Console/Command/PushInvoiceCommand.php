<?php
namespace Acty\Buum\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\State;
use Magento\Sales\Model\OrderFactory;
use \Acty\Buum\Sync\Invoice;

class PushInvoiceCommand extends Command
{
    public function __construct(
        State $state,
        Invoice $invoice,
        OrderFactory $orderFactory,
        $name = null
    ) {
        $this->state = $state;
        $this->invoice = $invoice;
        $this->orderFactory = $orderFactory;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('buum:push:invoice')
            ->setDescription('test');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        $order_id = 9;
        $order = $this->orderFactory->create();
        $order->load($order_id);
        $this->invoice->pushOrderToBuum($order);
    }
}

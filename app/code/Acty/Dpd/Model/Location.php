<?php

namespace Acty\Dpd\Model;

use Acty\Dpd\Model\ResourceModel\Location as Resource;
use Magento\Framework\Model\AbstractModel;

class Location extends AbstractModel
{
    const TYPE_ROBOT = 1;

    const TYPE_LOCKER = 2;

    const TYPE_STORE = 3;

    protected function _construct()
    {
        $this->_init(Resource::class);
    }
}

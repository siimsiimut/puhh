<?php

namespace Acty\Dpd\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Location extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('acty_dpd_locations', 'entity_id');
    }
}

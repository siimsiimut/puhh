<?php

namespace Acty\Dpd\Model\ResourceModel\Location;

use Acty\Dpd\Model\Location;
use Acty\Dpd\Model\ResourceModel\Location as Resource;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(Location::class, Resource::class);
    }
}

<?php

namespace Acty\Dpd\Model;

use Acty\Dpd\Model\Location;
use Magento\Framework\ObjectManagerInterface;

class LocationFactory
{
    protected $objectManager;

    public function __construct(ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function create($data = [])
    {
        return $this->objectManager->create(Location::class, ['sourceData' => $data]);
    }
}

<?php

namespace Acty\Dpd\Carriers;

use Acty\Dpd\Repositories\LocationRepository;
use Magento\Catalog\Model\ProductRepository;
use Magento\Checkout\Model\Cart;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\ResultFactory;
use Psr\Log\LoggerInterface;
use Exception;
use Magento\Store\Model\StoreManagerInterface;

abstract class Pickup extends AbstractCarrier implements CarrierInterface
{
    protected $locations = [];

    protected $locationRepository;

    protected $result;

    protected $method;

    protected $cart;

    protected $items = null;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $errorFactory,
        LoggerInterface $logger,
        LocationRepository $locationRepository,
        ResultFactory $result,
        MethodFactory $method,
        Cart $cart,
        ProductRepository $product,
        StoreManagerInterface $storeManager,
        $data = []
    ) {
        $this->locationRepository = $locationRepository;
        $this->result = $result;
        $this->method = $method;
        $this->cart = $cart;
        $this->product = $product;
        $this->storeManager = $storeManager;

        parent::__construct($scopeConfig, $errorFactory, $logger, $data);
    }

    public function getAllowedMethods()
    {
        $data = [];
        $web = [1 => 'EE', 2 => 'LV'];
        $currentWebsiteId = $this->storeManager->getStore()->getWebsiteId();

        foreach ($this->getLocations() as $location) {
            if($location['country'] === $web[$currentWebsiteId]){
                $data[$location['parcelshop_id']] = [
                    'place_id' => $location['parcelshop_id'],
                    'name' => $location['company'],
                    'group_name' => $location['city'],
                    'group_id' => md5($location['city'])
                ];
            }
        }

        return $data;
    }

    public function collectRates(RateRequest $request)
    {
        if ( ! $this->getConfigFlag('active')  || ! $this->hasValidItems($request)) {
            return false;
        }

        $price = $this->getPrice($request);
        $carrierTitle = $this->getConfigData('title');
        $methodTitle = $this->getConfigData('name');
        $methods = $this->getAvailableMethods();

        $result = $this->result->create();
        $method = $this->method->create();

        $this->createMethod(
            $method,
            $this->_code,
            $carrierTitle,
            $this->_code,
            $methodTitle,
            $price
        );

        $result->append($method);

        if (isset($methods[$this->getMethod($request)])) {
            $location = $methods[$this->getMethod($request)];
            $method = $this->method->create();

            $this->createMethod(
                $method,
                $this->_code,
                $carrierTitle,
                $location['parcelshop_id'],
                $location['company'],
                $price
            );

            $result->append($method);
        }

        return $result;
    }

    protected function createMethod(&$object, $carrier, $carrierTitle, $method, $methodTitle, $price)
    {
        $object->setCarrier($carrier);
        $object->setCarrierTitle($carrierTitle);
        $object->setMethod($method);
        $object->setMethodTitle($methodTitle);
        $object->setPrice($price);
        $object->setCost($price);
    }

    protected function getAvailableMethods()
    {
        $methods = [];

        foreach ($this->getLocations() as $location) {
            $method = $this->_code.'_'.$location['parcelshop_id'];
            $methods[$method] = $location;
        }

        return $methods;
    }

    protected function getLocations()
    {
        if ( ! isset($locations[static::$typeId])) {
            $locations[static::$typeId] = $this->locationRepository->getByTypeId(static::$typeId);
        }

        return $locations[static::$typeId];
    }

    protected function getMethod($request)
    {
        return $this->getItems($request)[0]->getQuote()->getShippingAddress()->getShippingMethod();
    }

    protected function getItems($request)
    {
        if (is_null($this->items)) {
            $this->items = $request->getAllItems();
        }

        return $this->items;
    }

    protected function hasValidItems($request)
    {
        $ids = [];

        foreach ($this->getItems($request) as $item) {
            try {
                $product = $this->product->getById($item->getProduct()->getId());

                if ($product->getData(static::$excluded)) {
                    return false;
                }
            } catch (Exception $e) {
                return false;
            }
        }

        return true;
    }

    protected function getPrice(RateRequest $request)
    {
        $price = $this->getConfigData('handling_fee');

        if ( ! $this->getConfigData('free_shipping')) {
            return $price;
        }

        $cartPrice = (float) $request->getPackageValueWithDiscount();
        $minimum = (float) $this->getConfigData('free_shipping_min');

        return $cartPrice >= $minimum ? 0 : $price;
    }
}

<?php

namespace Acty\Dpd\Carriers;

use Magento\Catalog\Model\ProductRepository;
use Magento\Checkout\Model\Cart;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\ResultFactory;
use Psr\Log\LoggerInterface;
use Exception;

class Courier extends AbstractCarrier implements CarrierInterface
{
    public $address;

    protected $_code = 'actydpdcourier';

    protected $result;

    protected $method;

    protected $cart;

    protected $items;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $errorFactory,
        LoggerInterface $logger,
        ResultFactory $result,
        MethodFactory $method,
        Cart $cart,
        ProductRepository $product,
        $data = []
    ) {
        $this->result = $result;
        $this->method = $method;
        $this->cart = $cart;
        $this->product = $product;

        parent::__construct($scopeConfig, $errorFactory, $logger, $data);
    }

    public function getAllowedMethods()
    {
        return [$this->_code = $this->getConfigData('name')];
    }

    public function collectRates(RateRequest $request)
    {
        if ( ! $this->getConfigFlag('active') || ! $this->hasValidItems($request)) {
            return false;
        }

        $result = $this->result->create();
        $method = $this->method->create();

        $price = $this->getPrice($request);

        $method->setCarrier($this->_code);
        $method->setCarrierTitle($this->getConfigData('title'));
        $method->setMethod($this->_code);
        $method->setMethodTitle($this->getConfigData('name'));
        $method->setPrice($price);
        $method->setCost($price);
        $method->sethasAddress(true);

        $result->append($method);

        return $result;
    }

    public function getHasAddress()
    {
        return true;
    }

    protected function getMethod($request)
    {
        return $this->getItems($request)[0]->getQuote()->getShippingAddress()->getShippingMethod();
    }

    protected function getItems($request)
    {
        if (is_null($this->items)) {
            $this->items = $request->getAllItems();
        }

        return $this->items;
    }

    protected function hasValidItems($request)
    {
        $ids = [];

        foreach ($this->getItems($request) as $item) {
            try {
                $product = $this->product->getById($item->getProduct()->getId());

                if ($product->getData('no_dpd_courer')) {
                    return false;
                }
            } catch (Exception $e) {
                return false;
            }
        }

        return true;
    }

    protected function getPrice(RateRequest $request)
    {
        $price = $this->getConfigData('handling_fee');

        if ( ! $this->getConfigData('free_shipping')) {
            return $price;
        }

        $cartPrice = (float) $request->getPackageValueWithDiscount();
        $minimum = (float) $this->getConfigData('free_shipping_min');

        return $cartPrice >= $minimum ? 0 : $price;
    }
}

<?php

namespace Acty\Dpd\Carriers;

use Acty\Dpd\Model\Location;

class Store extends Pickup
{
    protected $_code = 'actydpdstore';

    protected static $typeId = Location::TYPE_STORE;

    protected static $excluded = 'no_dpd_pickup_point';
}

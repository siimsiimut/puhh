<?php

namespace Acty\Dpd\Carriers;

use Acty\Dpd\Model\Location;

class Locker extends Pickup
{
    protected $_code = 'actydpdlocker';

    protected static $typeId = Location::TYPE_LOCKER;

    protected static $excluded = 'no_dpd_parcel_locker';
}

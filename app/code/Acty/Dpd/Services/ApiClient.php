<?php

namespace Acty\Dpd\Services;

use Acty\Dpd\Services\Logger\Logger;
use Acty\Framework\Support\EnvConfig;
use Exception;
use GuzzleHttp\Client;
use Magento\Framework\App\Config\ScopeConfigInterface;

class ApiClient
{
    protected $host = 'https://ee.integration.dpd.eo.pl';

    protected $uri = 'ws-mapper-rest';

    protected $carrier;

    protected $client;

    protected $config;

    protected $logger;

    protected $env;

    public function __construct(ScopeConfigInterface $config, Logger $logger, EnvConfig $env)
    {
        $this->config = $config;
        $this->logger = $logger;
        $this->env = $env;

        $this->client = new Client([
            'base_uri' => $this->host(),
            'verify' => false
        ]);
    }

    public function setCarrier($carrier)
    {
        $this->carrier = $carrier;
    }

    public function post($uri, $params = [], $callback)
    {
        try {
            $response = $this->rawPost($uri, $params)->getBody()->getContents();
            $response = json_decode($response, true);
        } catch (Exception $e) {
            $message = $e->getMessage();

            $this->logger->info('post failed: '.$message);

            return $callback($message, null);
        }

        return $callback(null, $response);
    }

    public function rawPost($uri, $params = [])
    {
        $params = array_merge($this->credentials(), $params);

        return $this->client->request('POST', $this->uri($uri), [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],

            'form_params' => $params
        ]);
    }

    protected function host()
    {
        $host = $this->env->get('dpd.host', null);

        return is_null($host) ? $this->host : $host;
    }

    protected function credentials()
    {
        return [
            'username' => $this->config->getValue('carriers/'.$this->carrier.'/username'),
            'password' => $this->config->getValue('carriers/'.$this->carrier.'/password')
        ];
    }

    protected function uri($uri)
    {
        return $this->uri.'/'.$uri;
    }
}

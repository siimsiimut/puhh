<?php

namespace Acty\Dpd\Crons;

use Acty\Dpd\Services\ApiClient;
use Acty\Dpd\Support\UpdateLocations;

class UpdateLocationsCron
{
    protected $update;

    protected $client;

    public function __construct(UpdateLocations $update, ApiClient $client)
    {
        $this->update = $update;
        $this->client = $client;
    }

    public function execute()
    {
        $this->client->setCarrier('actydpdlocker');
        $this->update->execute();
    }
}

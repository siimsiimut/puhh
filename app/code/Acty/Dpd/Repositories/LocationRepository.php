<?php

namespace Acty\Dpd\Repositories;

use Acty\Dpd\Model\LocationFactory;
use Acty\Dpd\Services\Logger\Logger;
use Exception;

class LocationRepository
{
    protected $locationFactory;

    protected $logger;

    public function __construct(LocationFactory $locationFactory, Logger $logger)
    {
        $this->locationFactory = $locationFactory;
        $this->logger = $logger;
    }

    public function store($typeId, $parcelshopId, $company, $country, $city, $pcode, $street)
    {
        $location = $this->locationFactory->create();

        $location->setTypeId($typeId);
        $location->setParcelshopId($parcelshopId);
        $location->setCompany($company);
        $location->setCountry($country);
        $location->setCity($city);
        $location->setPcode($pcode);
        $location->setStreet($street);
        $location->save();

        return $location;
    }

    public function batchStore($locations)
    {
        $db = $this->db();

        $db->beginTransaction();

        try {
            foreach ($locations as $location) {
                $this->store(
                    $location['type_id'],
                    $location['parcelshop_id'],
                    $location['company'],
                    $location['country'],
                    $location['city'],
                    $location['pcode'],
                    $location['street']
                );
            }
        } catch (Exception $e) {
            $this->logger->error('LocationRepository::batchStore: '.$e->getMessage());
            $db->rollBack();
        }

        $db->commit();
    }

    public function batchDelete($ids)
    {
        $db = $this->db();
        $db->beginTransaction();

        try {
            $locations = $this->locationFactory->create()->getCollection()
                ->addFieldToFilter('parcelshop_id', ['in' => $ids]);

            foreach ($locations as $location) {
                $location->delete();
            }
        } catch (Exception $e) {
            $this->logger->error('LocationRepository::batchDelete: '.$e->getMessage());
            $db->rollBack();
        }

        $db->commit();
    }

    public function batchUpdate($locations)
    {
        foreach ($this->locationFactory->create()->getCollection() as $model) {
            $data = $model->getData();

            if (isset($locations[$data['parcelshop_id']])) {
                $updated = $locations[$data['parcelshop_id']];

                $model->setCompany($updated['company']);
                $model->setCountry($updated['country']);
                $model->setCity($updated['city']);
                $model->setPcode($updated['pcode']);
                $model->setStreet($updated['street']);

                $model->save();
            }
        }
    }

    public function getAll()
    {
        return $this->locationFactory->create()->getCollection()->toArray();
    }

    public function getByTypeId($typeId)
    {
        return $this->locationFactory->create()->getCollection()
            ->addFieldToFilter('type_id', $typeId)->toArray()['items'];
    }

    public function getByParcelshopId($parcelshopId)
    {
        return $this->locationFactory->create()->load($parcelshopId, 'parcelshop_id');
    }

    protected function db()
    {
        return $this->locationFactory->create()->getResource()->getConnection();
    }
}

<?php

namespace Acty\Dpd\Repositories;

use Acty\Dpd\Services\ApiClient;
use Acty\Dpd\Services\Logger\Logger;
use Exception;

class CarrierRepository
{
    protected $client;

    protected $logger;

    public function __construct(ApiClient $client, Logger $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    public function getLocations()
    {
        $estLocations = $this->getEstLocations();
        $lvLocations = $this->getLvLocations();
        if($estLocations !== false && $lvLocations !== false){
            if(!isset($estLocations['status']) || $estLocations['status'] !== 'ok'){
                $response = ['status' => $estLocations['status'], 'errlog' => $estLocations['errlog'], 'parcelshops' => []];
            }
            elseif(!isset($lvLocations['status']) || $lvLocations['status'] !== 'ok'){
                $response = ['status' => $lvLocations['status'], 'errlog' => $lvLocations['errlog'], 'parcelshops' => []];
            }
            else{
                $response = ['status' => 'ok', 'errlog' => '', 'parcelshops' => array_merge($estLocations['parcelshops'], $lvLocations['parcelshops'])];
            }
            return $response;
        }
        return false;
    }

    private function getEstLocations()
    {
        $params = [
            'country' => 'EE',
            'fetchGsPUDOpoint' => 1
        ];

        return $this->client->post('parcelShopSearch_', $params, function($err, $res) {
            if (is_null($err)) {
                return $res;
            }

            $this->logger->error('CarrierRepository::getLocations: '.$err);

            return false;
        });
    }

    private function getLvLocations()
    {
        $params = [
            'country' => 'LV',
            'fetchGsPUDOpoint' => 1
        ];

        return $this->client->post('parcelShopSearch_', $params, function($err, $res) {
            if (is_null($err)) {
                return $res;
            }

            $this->logger->error('CarrierRepository::getLocations: '.$err);

            return false;
        });
    }

    public function createShipment(
        $name,
        $street,
        $city,
        $country,
        $pcode,
        $parcelType,
        $parcelshopId,
        $phone,
        $uid
    ) {
        $params = [
            'name1' => $name,
            'street' => $street,
            'city' => $city,
            'country' => $country,
            'pcode' => $pcode,
            'num_of_parcel' => 1,
            'parcel_type' => $parcelType,
            'parcelshop_id' => $parcelshopId,
            'phone' => $phone,
            'idm_sms_number' => $phone,
            'fetchGsPUDOpoin' => 1,
            'cod_purpose' => $uid
        ];

        $this->logger->info('CarrierRepository::createShipment: ', [
            'params' => $params
        ]);

        return $this->client->post('createShipment_', $params, function($err, $res) {
            if (is_null($err)) {
                $this->logger->info('CarrierRepository::createShipment: ', [
                    'response' => $res
                ]);
                return $res;
            }

            $this->logger->error('CarrierRepository::createShipment: '.$err);

            return false;
        });
    }

    public function createCourierShipment(
        $name,
        $street,
        $city,
        $country,
        $pcode,
        $parcelType,
        $phone,
        $uid
    ) {
        $params = [
            'name1' => $name,
            'street' => $street,
            'city' => $city,
            'country' => $country,
            'pcode' => $pcode,
            'num_of_parcel' => 1,
            'parcel_type' => $parcelType,
            'phone' => $phone,
            'idm_sms_number' => $phone,
            'cod_purpose' => $uid
        ];

        $this->logger->info('CarrierRepository::createCourierShipment: ', [
            'params' => $params
        ]);

        return $this->client->post('createShipment_', $params, function($err, $res) {
            if (is_null($err)) {
                $this->logger->info('CarrierRepository::createCourierShipment: ', [
                    'response' => $res
                ]);
                return $res;
            }

            $this->logger->error('CarrierRepository::createShipment: '.$err);

            return false;
        });
    }

    public function pickupOrderSave(
        $orderNr,
        $senderContact,
        $senderAddress,
        $senderCity,
        $senderCountry,
        $senderPostalCode,
        $senderPhone,
        $weight,
        $parcelsCount
    ) {
        $params = [
            'orderNr' => $orderNr,
            'senderContact' => $senderContact,
            'senderAddress' => $senderAddress,
            'senderCity' => $senderCity,
            'senderCountry' => $senderCountry,
            'senderPostalCode' => $senderPostalCode,
            'senderPhone' => $senderPhone,
            'weight' => $weight,
            'parcelsCount' => $parcelsCount
        ];

        try {
            $response = $this->client->rawPost('pickupOrderSave_', $params)->getBody()->getContents();
            $this->logger->info('CarrierRepository::pickupOrderSave: ', [
                'params' => $params,
                'response' => $response
            ]);
        } catch(Exception $e) {
            return [
                'errlog' => $e->getMessage()
            ];
        }

        if (strstr($response, 'DONE')) {
            return [
                'status' => 'ok'
            ];
        }

        return [
            'errlog' => $response
        ];
    }

    public function parcelPrint($parcel)
    {
        $params = [
            'parcels' => $parcel,
            'printType' => 'PDF',
            'printFormat' => 'A5'
        ];

        $response = $this->client->rawPost('parcelPrint_', $params);
        $header = $response->getHeader('printtype');

        if ( ! is_array($header) || empty($header) || $header[0] !== 'PDF') {
            return false;
        }

        return $response->getBody();
    }

    public function parcelManifestPrint($date)
    {
        $params = [
            'date' => $date
        ];

        return $this->client->post('parcelManifestPrint_', $params, function($err, $res) {
            if (is_null($err)) {
                return $res;
            }

            $this->logger->error('CarrierRepository::parcelManifestPrint_: '.$err);

            return false;
        });
    }
}

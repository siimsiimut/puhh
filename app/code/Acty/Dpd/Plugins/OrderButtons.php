<?php

namespace Acty\Dpd\Plugins;

use Magento\Sales\Block\Adminhtml\Order\View;
use Magento\Backend\Model\UrlInterface;

class OrderButtons
{
    protected $url;

    public function __construct(UrlInterface $url)
    {
        $this->url = $url;
    }

    public function beforeGetOrderId(View $subject)
    {
        $order = $subject->getOrder();
        $plNumber = $order->getActyDpdPlNumber();

        if ( ! is_null($plNumber)) {
            $order = $subject->getOrder();
            $carrier = $order->getActyDpdCarrier();
            $url = $this->url->getUrl('acty_dpd/label/printer/carrier/'.$carrier.'/pln/'.$plNumber);

            $subject->addButton('dpdLabel', [
                'label' => __('DPD Label'),
                'onclick' => 'setLocation("'.$url.'")',
                'class' => 'reset'
            ], -1);

            $url = $this->url->getUrl('acty_dpd/manifest/printer/carrier/'.$carrier.'/pln/'.$plNumber);

            $subject->addButton('dpdManifest', [
                'label' => __('DPD Manifest'),
                'onclick' => 'setLocation("'.$url.'")',
                'class' => 'reset'
            ], -1);
        }
    }
}

<?php

namespace Acty\Dpd\Observers;

use Acty\Dpd\Services\ApiClient;
use Acty\Dpd\Support\Pushers\CourierPusher;
use Acty\Dpd\Support\Pushers\LockerPusher;
use Acty\Dpd\Support\Pushers\StorePusher;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class PaymentObserver implements ObserverInterface
{
    const STORE = 'actydpdstore';

    const LOCKER = 'actydpdlocker';

    const COURIER = 'actydpdcourier';

    protected $store;

    protected $locker;

    protected $courier;

    protected $client;

    protected $config;

    public function __construct(StorePusher $store, LockerPusher $locker, CourierPusher $courier, ApiClient $client, ScopeConfigInterface $config)
    {
        $this->store = $store;
        $this->locker = $locker;
        $this->courier = $courier;
        $this->client = $client;
        $this->config = $config;
    }

    public function execute(Observer $observer)
    {
        $order = $observer->getPayment()->getOrder();
        $method = explode('_', (string) $order->getShippingMethod());

        if ( ! empty($method[0]) && $this->enabled($method[0])) {
            $this->client->setCarrier($method[0]);
            $this->push($order, $method[0]);
        }
    }

    protected function push($order, $method)
    {
        switch($method) {
            case self::STORE:
                $this->store->push($order, $method);
                break;
            case self::LOCKER:
                $this->locker->push($order, $method);
                break;
            case self::COURIER:
                $this->courier->push($order, $method);
                break;
        }
    }

    protected function enabled($carrier)
    {
        $key = 'carriers/'.$carrier.'/data_exchange';

        return (boolean) $this->config->getValue($key, ScopeInterface::SCOPE_STORES);
    }
}

<?php

namespace Acty\Dpd\Setup;

use Magento\Customer\Model\ResourceModel\Attribute;
use Magento\Eav\Model\Config;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Catalog\Model\Product\Attribute\Backend\Boolean as BackendBoolean;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean as SourceBoolean;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

class InstallData implements InstallDataInterface
{
    protected $eavSetupFactory;

    protected $eavConfig;

    public function __construct(EavSetupFactory $eavSetupFactory, Config $eavConfig)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig = $eavConfig;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $fields = [
            'no_dpd_pickup_point' => 'DPD store pickup disabled',
            'no_dpd_parcel_locker' => 'DPD locker pickup disabled',
            'no_dpd_courer' => 'DPD courier disabled'
        ];

        foreach ($fields as $column => $label) {
            $eavSetup->addAttribute(Product::ENTITY, $column, $this->getEntity($label));
        }

        $setup->endSetup();
    }

    public function getEntity($name)
    {
        return [
            'group' => 'General',
            'type' => 'int',
            'input' => 'boolean',
            'model' => Attribute::class,
            'backend' => BackendBoolean::class,
            'source' => SourceBoolean::class,
            'label' => __($name),
            'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
            'visible' => true,
            'required' => false,
            'user_defined' => true,
            'used_in_product_listing' => true,
            'default' => null
        ];
    }
}

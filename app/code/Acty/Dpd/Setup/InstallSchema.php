<?php

namespace Acty\Dpd\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $table = $setup->getConnection()->newTable(
            $setup->getTable('acty_dpd_locations')
        )->addColumn(
            'entity_id',
            Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'identity' => true, 'primary' => true],
            'ID'
        )->addColumn(
            'type_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable' => false],
            'Magento ID'
        )->addColumn(
            'parcelshop_id',
            Table::TYPE_TEXT,
            32,
            ['nullable' => false],
            'Parcelshop ID'
        )->addColumn(
            'company',
            Table::TYPE_TEXT,
            128,
            ['nullable' => false],
            'Company'
        )->addColumn(
            'country',
            Table::TYPE_TEXT,
            32,
            ['nullable' => true],
            'Country'
        )->addColumn(
            'city',
            Table::TYPE_TEXT,
            32,
            ['nullable' => true],
            'City'
        )->addColumn(
            'pcode',
            Table::TYPE_TEXT,
            32,
            ['nullable' => true],
            'Postikood'
        )->addColumn(
            'street',
            Table::TYPE_TEXT,
            32,
            ['nullable' => true],
            'Street'
        );

        $setup->getConnection()->createTable($table);

        $field = [
            'type' => Table::TYPE_TEXT,
            'length' => 32,
            'nullable' => true,
            'comment' => 'DPD No/Carrier'
        ];

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'), 'acty_dpd_pl_number', $field
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'), 'acty_dpd_carrier', $field
        );

        $setup->endSetup();
    }
}

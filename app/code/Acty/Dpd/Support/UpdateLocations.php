<?php

namespace Acty\Dpd\Support;

use Acty\Dpd\Model\Location;
use Acty\Dpd\Repositories\CarrierRepository;
use Acty\Dpd\Repositories\LocationRepository;
use Acty\Dpd\Services\Logger\Logger;

class UpdateLocations
{
    protected $logger;

    protected $carrier;

    protected $locationRepository;

    public function __construct(Logger $logger, CarrierRepository $carrier, LocationRepository $locationRepository)
    {
        $this->logger = $logger;
        $this->carrier = $carrier;
        $this->locationRepository = $locationRepository;
    }

    public function execute()
    {
        $locations = $this->carrier->getLocations();

        if ($locations === false) {
            return false;
        }

        if ( ! isset($locations['status']) || $locations['status'] !== 'ok') {
            $message = isset($locations['errlog']) ? $locations['errlog'] : 'unknown reasons';

            $this->logger->error('Synchronization failed: '.$message);

            return false;
        }

        $this->update($locations['parcelshops']);

        return true;
    }

    protected function update($locations)
    {
        $add = [];
        $delete = [];
        $magentoLocations = $this->getMagentoLocations();
        $carrierLocations = $this->getCarrierLocations($locations);

        foreach ($carrierLocations as $id => $location) {
            if ( ! isset($magentoLocations[$id])) {
                $location['type_id'] = $this->getType($id);
                $add[] = $location;
            }
        }

        foreach ($magentoLocations as $id => $location) {
            if ( ! isset($carrierLocations[$id])) {
                $delete[] = $id;
            }
        }

        $this->locationRepository->batchDelete($delete);
        $this->locationRepository->batchStore($add);
        $this->locationRepository->batchUpdate($carrierLocations);
    }

    protected function getCarrierLocations($locations)
    {
        $result = [];

        foreach ($locations as $location) {
            $result[$location['parcelshop_id']] = $location;
        }

        return $result;
    }

    protected function getMagentoLocations()
    {
        $result = [];
        $locations = $this->locationRepository->getAll();

        foreach ($locations['items'] as $location) {
            $result[$location['parcelshop_id']] = $location;
        }

        return $result;
    }

    protected function getType($id)
    {
        if (preg_match('/^EE91/', $id)) {
            return Location::TYPE_ROBOT;
        }

        if (preg_match('/^EE90/', $id)) {
            return Location::TYPE_LOCKER;
        }

        return Location::TYPE_STORE;
    }
}

<?php

namespace Acty\Dpd\Support\Responses;

use Magento\Framework\Controller\ResultFactory;
use Magento\Store\App\Response\Redirect;

class ControllerResponses
{
    protected $factory;

    protected $redirect;

    public function __construct(ResultFactory $factory, Redirect $redirect)
    {
        $this->factory = $factory;
        $this->redirect = $redirect;
    }

    public function pdf($content, $filename)
    {
        return $this->factory->create(ResultFactory::TYPE_RAW)
            ->setHeader('Content-type', 'application/pdf')
            ->setHeader('Content-Disposition', "attachment;filename=".$filename)
            ->setContents($content);
    }

    public function back()
    {
        return $this->factory->create(ResultFactory::TYPE_REDIRECT)
            ->setUrl($this->redirect->getRefererUrl());
    }
}

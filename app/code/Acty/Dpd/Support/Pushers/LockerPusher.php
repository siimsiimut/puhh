<?php

namespace Acty\Dpd\Support\Pushers;

use Acty\Dpd\Model\Location;

class LockerPusher extends PickupPusher
{
    protected static $type = Location::TYPE_LOCKER;

    protected static $carrierCode = 'actydpdlocker';
}

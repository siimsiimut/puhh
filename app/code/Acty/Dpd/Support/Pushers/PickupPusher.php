<?php

namespace Acty\Dpd\Support\Pushers;

use Acty\Dpd\Repositories\CarrierRepository;
use Acty\Dpd\Repositories\LocationRepository;
use Acty\Dpd\Support\Utils\GetUrl;

abstract class PickupPusher
{
    protected $location;

    protected $carrier;

    protected $print;

    protected $url;

    public function __construct(LocationRepository $location, CarrierRepository $carrier, GetUrl $url)
    {
        $this->location = $location;
        $this->carrier = $carrier;
        $this->url = $url;
    }

    public function push($order, $method)
    {
        $location = $this->getLocation($order->getShippingMethod());
        $address = $order->getShippingAddress();

        $response = $this->carrier->createShipment(
            $address->getFirstname().' '.$address->getLastname(),
            $location['street'],
            $location['city'],
            $location['country'],
            $location['pcode'],
            "PS",
            $location['parcelshop_id'],
            $address->getTelephone(),
            $order->getIncrementId()
        );

        if ($response === false) {
            return false;
        }

        if ( ! isset($response['status']) || $response['status'] !== 'ok') {
            return $this->handleError($order, $response);
        }

        return $this->handleSuccess($order, $response);
    }

    protected function handleError($order, $response)
    {
        $message = isset($response['errlog']) ? $response['errlog'] : 'unknown reasons';

        $this->addComment($order, 'Pushing DPD order failed: '.$message);

        return false;
    }

    protected function handleSuccess($order, $response)
    {
        $order
            ->setActyDpdPlNumber($response['pl_number'][0])
            ->setActyDpdCarrier(static::$carrierCode)
            ->setData('tracking_code', $response['pl_number'][0])
            ->save();
        $this->addComment($order, 'Pushed DPD order: '.$response['pl_number'][0]);

        return $order;
    }

    protected function addComment($order, $comment)
    {
        $order->addStatusHistoryComment($comment)->setIsCustomerNotified(null)->save();
    }

    protected function getLocation($method)
    {
        $method = explode('_', $method);

        return $this->location->getByParcelshopId($method[1])->toArray();
    }
}

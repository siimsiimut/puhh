<?php

namespace Acty\Dpd\Support\Pushers;


class CourierPusher extends PickupPusher
{
    protected static $carrierCode = 'actydpdcourier';

    
    public function push($order, $method)
    {
        $address = $order->getShippingAddress();

        $response = $this->carrier->createCourierShipment(
            $address->getFirstname().' '.$address->getLastname(),
            implode(" ", $address->getStreet()),
            $address->getCity(),
            $address->getCountryId(),
            $address->getPostcode(),
            "D-B2C",
            $address->getTelephone(),
            $order->getIncrementId()
        );

        if ($response === false) {
            return false;
        }

        if ( ! isset($response['status']) || $response['status'] !== 'ok') {
            return $this->handleError($order, $response);
        }

        return $this->handleSuccess($order, $response);
    }
}
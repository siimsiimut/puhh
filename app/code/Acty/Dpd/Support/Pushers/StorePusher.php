<?php

namespace Acty\Dpd\Support\Pushers;

use Acty\Dpd\Model\Location;

class StorePusher extends PickupPusher
{
    protected static $type = Location::TYPE_STORE;

    protected static $carrierCode = 'actydpdstore';
}

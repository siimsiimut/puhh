<?php

namespace Acty\Dpd\Support\Utils;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class GetStoreEmail
{
    protected $config;

    public function __construct(ScopeConfigInterface $config)
    {
        $this->config = $config;
    }

    public function get()
    {
        $email = $this->config->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE);

        if ( ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        return $email;
    }
}

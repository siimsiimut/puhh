<?php

namespace Acty\Dpd\Block\Elements;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Backend\Model\UrlInterface;
use Magento\Framework\Data\Form\Element\AbstractElement;

abstract class Synchronize extends Field
{
    const CHECK_TEMPLATE = 'elements/synchronize.phtml';

    protected $adminUrl;

    public function __construct(Context $context, UrlInterface $adminUrl, $data = [])
    {
        $this->adminUrl = $adminUrl;

        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        if ( ! $this->getTemplate()) {
            $this->setTemplate(static::CHECK_TEMPLATE);
        }

        return $this;
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $this->addData(
            [
                'location' => $this->adminUrl->getUrl('acty_dpd/synchronize/locations/id/'.static::$target),
                'title' => __('Synchronize'),
                'html_id' => $element->getHtmlId()
            ]
        );

        return $this->_toHtml();
    }
}

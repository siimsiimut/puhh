<?php

namespace Acty\Dpd\Controller\Adminhtml\Manifest;

use Acty\Dpd\Repositories\CarrierRepository;
use Acty\Dpd\Services\ApiClient;
use Acty\Dpd\Support\Responses\ControllerResponses;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;

class Printer extends Action
{
    protected $factory;

    protected $carrier;

    protected $client;

    protected $responses;

    public function __construct(Context $context, CarrierRepository $carrier, ApiClient $client, ControllerResponses $responses)
    {
        $this->carrier = $carrier;
        $this->client = $client;
        $this->responses = $responses;

        parent::__construct($context);
    }

    public function execute()
    {
        $request = $this->getRequest();
        $date = date("Y-m-d");


        $this->client->setCarrier($request->getParam('carrier'));

        $response = $this->carrier->parcelManifestPrint($date);

        if ($response === false || $response['status'] !== 'ok') {
            $message = isset($response['errlog']) ? $response['errlog'] : 'unknowen reasons';

            $this->messageManager->addErrorMessage(__('Manifest printing failed: '.$message));

            return $this->responses->back();
        }

        return $this->responses->pdf(base64_decode($response['pdf']), 'manifest-'.$date.'.pdf');
    }
}

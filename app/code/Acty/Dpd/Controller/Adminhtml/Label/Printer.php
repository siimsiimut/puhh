<?php

namespace Acty\Dpd\Controller\Adminhtml\Label;

use Acty\Dpd\Repositories\CarrierRepository;
use Acty\Dpd\Services\ApiClient;
use Acty\Dpd\Support\Responses\ControllerResponses;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;

class Printer extends Action
{
    protected $factory;

    protected $carrier;

    protected $client;

    protected $responses;

    public function __construct(Context $context, CarrierRepository $carrier, ApiClient $client, ControllerResponses $responses)
    {
        $this->carrier = $carrier;
        $this->client = $client;
        $this->responses = $responses;

        parent::__construct($context);
    }

    public function execute()
    {
        $request = $this->getRequest();
        $plNumber = $request->getParam('pln');

        $this->client->setCarrier($request->getParam('carrier'));

        $pdf = $this->carrier->parcelPrint($plNumber);

        if ($pdf === false) {
            $this->messageManager->addErrorMessage(__('Label printing failed'));

            return $this->responses->back();
        }

        return $this->responses->pdf($pdf, $plNumber.'.pdf');
    }
}

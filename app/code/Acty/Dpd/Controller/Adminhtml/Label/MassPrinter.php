<?php

namespace Acty\Dpd\Controller\Adminhtml\Label;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

use Acty\Dpd\Repositories\CarrierRepository;
use Acty\Dpd\Services\ApiClient;
use Acty\Dpd\Support\Responses\ControllerResponses;

class MassPrinter extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction
{
    protected $collectionFactory;
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;
    /**
     * Pdforders constructor.
     *
     * @param \Magento\Backend\App\Action\Context                        $context
     * @param \Magento\Ui\Component\MassAction\Filter                    $filter
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory
     * @param \Magento\Framework\App\Response\Http\FileFactory           $fileFactory
     * @param \Acty\BatchSlip\Model\Pdf\OrderFactory               $orderPdfFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime                $date
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
        CarrierRepository $carrier, 
        ApiClient $client, 
        ControllerResponses $responses
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->carrier = $carrier;
        $this->client = $client;
        $this->responses = $responses;
        parent::__construct($context, $filter);
    }
    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magento_Sales::sales_order');
    }
    /**
     * Print selected orders
     *
     * @param AbstractCollection $collection
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    protected function massAction(AbstractCollection $collection)
    {  
        $numbers = [];
        $carriers = [];
        foreach($collection as $order){
            $plNumber = $order->getActyDpdPlNumber();
            if($plNumber){
                $numbers[] = $plNumber;
                $carrier[] = $order->getActyDpdCarrier();
            }
        }
        if (sizeof($numbers) < 1) {
            $this->messageManager->addErrorMessage(__('No labels to print'));

            return $this->responses->back();
        }

        $this->client->setCarrier($carrier[0]);

        $pdf = $this->carrier->parcelPrint(implode("|", $numbers));

        if ($pdf === false) {
            $this->messageManager->addErrorMessage(__('Label printing failed'));

            return $this->responses->back();
        }

        return $this->responses->pdf($pdf, 'sildid.pdf');
    }
}
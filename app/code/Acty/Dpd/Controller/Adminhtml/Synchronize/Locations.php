<?php

namespace Acty\Dpd\Controller\Adminhtml\Synchronize;

use Acty\Dpd\Services\ApiClient;
use Acty\Dpd\Support\UpdateLocations;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\RedirectFactory;

class Locations extends Action
{
    protected $factory;

    protected $logger;

    protected $carrier;

    protected $update;

    protected $client;

    public function __construct(Context $context, RedirectFactory $factory, UpdateLocations $update, ApiClient $client)
    {
        $this->factory = $factory;
        $this->update = $update;
        $this->client = $client;

        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->factory->create();

        $this->client->setCarrier($this->getRequest()->getParam('id'));

        if ($this->update->execute()) {
            $this->messageManager->addSuccessMessage(__('DPD location information synchronized'));
        } else {
            $this->messageManager->addErrorMessage(__('DPD location synchronization failed'));
        }

        return $result->setUrl($this->_redirect->getRefererUrl());;
    }
}

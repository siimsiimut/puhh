<?php

namespace Acty\DiscountedCategory\Repositories;


use Magento\Catalog\Api\CategoryRepositoryInterface;
use Psr\Log\LoggerInterface;


class CategoryRepository
{
    protected $categoryRepository;

    protected $logger
    ;

    public function __construct(CategoryRepositoryInterface $categoryRepository, LoggerInterface $logger)
    {
        $this->categoryRepository = $categoryRepository;
        $this->logger = $logger;
    }

    public function assign($categoryId, $products)
    {
        try {
            $category = $this->categoryRepository->get($categoryId, 0);
            $category->setPostedProducts($products);
            $category->save();
        } catch (Exception $e) {
            $this->logger->critical();
            $this->logger->critical('Discount category assign error', [
                    'err' => $e->getMessage()
                ]
            );
        }
    }
}

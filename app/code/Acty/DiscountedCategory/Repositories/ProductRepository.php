<?php

namespace Acty\DiscountedCategory\Repositories;

use Acty\DiscountedCategory\Repositories\ConfigRepository;
use Magento\Catalog\Model\Product;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\ResourceConnection;

class ProductRepository
{
    protected $om;

    protected $config;

    public function __construct(ObjectManagerInterface $om, ConfigRepository $config, ResourceConnection $resourceConnection)
    {
        $this->om = $om;
        $this->config = $config;
        $this->resourceConnection = $resourceConnection;
        $this->db = $this->resourceConnection->getConnection();
    }

    public function getDiscountedProducts($storeId)
    {
        $products = [];
        $discount = (int) $this->config->get('discount_percent', $storeId);

        $collection = $this->om->create(Product::class)->setStoreId($storeId)->getCollection()
            ->setFlag('has_stock_status_filter', true)
            ->addAttributeToSelect(['price', 'special_price']);

        $specials = $this->getDbSpecialPrice($storeId);

        foreach ($collection as $product) {
            if(array_key_exists($product->getSku(), $specials)){
                $specialPrice = (float) $specials[$product->getSku()];
                $price = (float) $product->getPrice();

                if ($specialPrice > 0 && $price > 0) {
                    $percent = (1 - $specialPrice / $price) * 100;

                    if ($percent >= $discount && $discount !== 0) {
                        $products[$product->getId()] = 0;
                    }
                }
            }
        }

        return $products;
    }

    private function getDbSpecialPrice($store){
        $query = "SELECT `catalog_product_entity_decimal`.`entity_id`,
        `catalog_product_entity`.`sku`,
        `eav_attribute`.`attribute_code`,
        `catalog_product_entity_decimal`.`store_id`,
        `catalog_product_entity_decimal`.`value`
        FROM `catalog_product_entity_decimal`
        LEFT JOIN `eav_attribute` ON `catalog_product_entity_decimal`.`attribute_id` = `eav_attribute`.`attribute_id`
        LEFT JOIN `catalog_product_entity` ON `catalog_product_entity_decimal`.`entity_id` = `catalog_product_entity`.`entity_id`
        WHERE `eav_attribute`.`attribute_code` =  'special_price'
        AND `catalog_product_entity`.`sku` IS NOT NULL
        AND `catalog_product_entity_decimal`.`store_id` = ".$store."
        ORDER BY `catalog_product_entity_decimal`.`entity_id` ASC";

        $results = $this->db->fetchAll($query);

        $prices = $this->filterPrices($results);

        return $prices;
    }

    private function filterPrices($results)
    {
        $prices = [];
        foreach($results as $row){
            $prices[$row['sku']] = $row['value'];
        }
        return $prices;
    }
}

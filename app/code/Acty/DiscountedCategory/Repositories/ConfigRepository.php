<?php

namespace Acty\DiscountedCategory\Repositories;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class ConfigRepository
{
    const PATH = 'actydiscountedcategory/settings/';

    protected $config;

    public function __construct(ScopeConfigInterface $config)
    {
        $this->config = $config;
    }

    public function get($slug, $store)
    {
        return $this->config->getValue($this->merge($slug), ScopeInterface::SCOPE_STORE, $store);
    }

    protected function merge($slug)
    {
        return self::PATH.$slug;
    }
}

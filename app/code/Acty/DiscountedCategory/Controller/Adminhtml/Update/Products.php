<?php

namespace Acty\DiscountedCategory\Controller\Adminhtml\Update;

use Acty\DiscountedCategory\Commands\Consolidate;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\RedirectFactory;

class Products extends Action
{
    
    protected $consolidate;

    public function __construct(Context $context, RedirectFactory $factory, Consolidate $consolidate)
    {
        $this->factory = $factory;
        $this->consolidate = $consolidate;

        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->factory->create();
        //Default for button
        $this->consolidate->handle(1);
        $this->messageManager->addSuccessMessage(__('Discounted category updated'));
        return $result->setUrl($this->_redirect->getRefererUrl());;
    }
}

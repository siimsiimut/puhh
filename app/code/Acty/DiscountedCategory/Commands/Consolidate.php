<?php

namespace Acty\DiscountedCategory\Commands;

use Acty\DiscountedCategory\Repositories\CategoryRepository;
use Acty\DiscountedCategory\Repositories\ProductRepository;
use Acty\DiscountedCategory\Repositories\ConfigRepository;

class Consolidate
{
    protected $productRepository;

    protected $categoryRepository;

    protected $configRepository;

    public function __construct(ProductRepository $productRepository, CategoryRepository $categoryRepository, ConfigRepository $configRepository)
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
        $this->configRepository = $configRepository;
    }

    public function handle($store)
    {
        $categoryId = $this->configRepository->get('category_id', $store);
        $specials = $this->productRepository->getDiscountedProducts($store);
        $this->categoryRepository->assign($categoryId, $specials);
    }
}

<?php

namespace Acty\DiscountedCategory\Console\Command;

use Acty\DiscountedCategory\Commands\Consolidate;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\State;
use Magento\Framework\App\Area;

class ConsolidateCommand extends Command
{
    const STORE = 'store'; // key of parameter

    protected $consolidate;

    protected $state;

    public function __construct(Consolidate $consolidate, State $state, $name = null)
    {
        $this->consolidate = $consolidate;
        $this->state = $state;

        parent::__construct($name);
    }

    protected function configure()
    {
        $options = [
            new InputOption(
                self::STORE, // the option name
                '-s', // the shortcut
                InputOption::VALUE_REQUIRED, // the option mode
                'Store view to run in' // the description
            ),
        ];
        $this->setName('discounted:consolidate')->setDescription('Consolidate discounted products');
        $this->setDefinition($options);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //Reset store to 1
        if($input->getOption(self::STORE) == null){
            $store = 1;
        }
        else{
            $store = $input->getOption(self::STORE);
        }
        $this->state->setAreaCode(Area::AREA_ADMINHTML);
        $this->consolidate->handle($store);
    }
}

<?php
namespace Acty\DiscountedCategory\Cron\Sync;

use \Psr\Log\LoggerInterface;

class Discounted
{
    protected $logger;
    protected $priceSync;

    public function __construct(
        LoggerInterface $logger,
        \Acty\DiscountedCategory\Commands\Consolidate $consolidate,
        \Acty\DiscountedCategory\Repositories\ConfigRepository $config
    ) {
        $this->logger = $logger;
        $this->consolidate = $consolidate;
        $this->config  = $config;
    }


    public function execute()
    {
        if (!$this->config->get('cron_enabled', 1)) {
            return;
        }
        file_put_contents(BP . '/var/log/consolidate_discounted.log', date("Y-m-d H:i:s") . "\n", FILE_APPEND);
        file_put_contents(BP . '/var/log/consolidate_discounted.log', "START consolidate" . "\n", FILE_APPEND);
        $this->consolidate->handle(1);
        file_put_contents(BP . '/var/log/consolidate_discounted.log', "EST done" . "\n", FILE_APPEND);
        $this->consolidate->handle(3);
        file_put_contents(BP . '/var/log/consolidate_discounted.log', "LV done" . "\n", FILE_APPEND);
        file_put_contents(BP . '/var/log/consolidate_discounted.log', "END consolidate" . "\n", FILE_APPEND);
        file_put_contents(BP . '/var/log/consolidate_discounted.log', date("Y-m-d H:i:s") . "\n\n", FILE_APPEND);
    }
}

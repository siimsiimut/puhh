<?php

namespace Acty\DiscountedCategory\Block;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Backend\Model\UrlInterface;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Update extends Field
{
    const CHECK_TEMPLATE = 'update.phtml';

    protected $url;

    public function __construct(Context $context, UrlInterface $url, $data = [])
    {
        $this->url = $url;

        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        if ( ! $this->getTemplate()) {
            $this->setTemplate(self::CHECK_TEMPLATE);
        }

        return $this;
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $this->addData(
            [
               'location' => $this->url->getUrl('acty_discountedcategory/update/products'),
                'title' => __('Update'),
                'html_id' => $element->getHtmlId()
            ]
        );   
        //echo $this->url->getUrl('acty_discountedcategory/update/products');
        //die; 
        return $this->_toHtml();      
    }
}

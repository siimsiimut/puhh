<?php
namespace Acty\Astri\Controller\Adminhtml\System\Config;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Acty\Astri\Model\FeedWriter;

class Regeneratexml extends Action
{

    protected $resultJsonFactory;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        FeedWriter $feedWriter
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->feedWriter = $feedWriter;
        parent::__construct($context);
    }


    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        try {
            $this->feedWriter->generateXml();
        } catch (\Exception $e) {
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e->getMessage());
            return $result->setData(['success' => true, 'message' => $e->getMessage()]);
        }
        return $result->setData(['success' => true]);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Acty_Astri::config');
    }
}
?>

<?php
namespace Acty\Astri\Model;


class Products
{
    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\CatalogInventory\Helper\Stock $stockFilter,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Acty\Astri\Model\Product $product,
        \Acty\Astri\Model\ConfigurableProduct $configurableProduct,
        \Acty\Astri\Model\CustomOptionsProduct $customOptionsProduct
    ) {
        $this->productFactory = $productFactory;
        $this->stockFilter  = $stockFilter;
        $this->productStatus = $productStatus;
        $this->productVisibility = $productVisibility;
        $this->productRepository = $productRepository;
        $this->product = $product;
        $this->configurableProduct = $configurableProduct;
        $this->customOptionsProduct = $customOptionsProduct;
    }

    public function merge($itemsXml, $attributes)
    {
        $productsFeed = $itemsXml->addChild('products');
        $variationsFeed = $itemsXml->addChild('variations');

        $this->initProductType($this->product, $productsFeed, $variationsFeed, $attributes);
        $this->initProductType($this->configurableProduct, $productsFeed, $variationsFeed, $attributes);
        $this->initProductType($this->customOptionsProduct, $productsFeed, $variationsFeed, $attributes);

        foreach ($this->getProducts() as $product) {
            $type = $this->getType($product);
            $type->add($product);
        }
    }

    public function initProductType($type, $productsFeed, $variationsFeed, $attributes)
    {
        $type->setProductsFeed($productsFeed);
        $type->setVariationsFeed($variationsFeed);
        $type->setAttributes($attributes);
    }

    public function getType($product) {
        if ($product->getTypeId() == 'configurable') {
            return $this->configurableProduct;
        }

        if ($product->getData('has_options') == 1) {
            return $this->customOptionsProduct;
        }
        return $this->product;
    }

    public function getProducts()
    {
        $productCollection = $this->productFactory->create()->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToSort('id', 'DESC')
            ->addUrlRewrite()
            ->addFinalPrice()
            ->addAttributeToFilter('status', ['in' => $this->productStatus->getVisibleStatusIds()])
            //->setPageSize(1000)
            ->addMediaGalleryData();
            $productCollection->setVisibility($this->productVisibility->getVisibleInSiteIds());
            $this->stockFilter->addIsInStockFilterToCollection($productCollection);
        return $productCollection;
    }
}

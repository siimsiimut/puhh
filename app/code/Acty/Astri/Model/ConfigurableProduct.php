<?php
namespace Acty\Astri\Model;

class ConfigurableProduct extends Product
{

    public function addVariations($product, $productXml)
    {

        $variationsXml = $productXml->addChild('variations');
        $attributes = $this->getConfigurableAttributes($product);
        foreach ($this->getEnabledUsedProducts($product) as $value) {
            $id = $this->addConfigurableVariation($value, $attributes);
            if ($id !== false) {
                $variationsXml->addChild('variation', $id);
            }
        }
    }

    public function addConfigurableVariation($product, $allowedAttributes)
    {
        $qty = $this->getStockItem($product->getId())->getQty();
        if ($qty < $this->config->getMinimumQtyAllowed()) {
            return false;
        }
        $productXml = $this->variationsFeed->addChild('variation');
        $sku = $this->sanitize($product->getData('sku'));
        $productXml->addChild('id', $sku);
        $productXml->addChild('sku', $sku);
        $productXml->addChild('price', $product->getPrice());

        if ($product->getPrice() != $product->getFinalPrice()) {
            $productXml->addChild('discountPrice', $product->getFinalPrice());
        }

        $productXml->addChild('quantity', $qty);

        $this->addAttributes($productXml, $product, $allowedAttributes);
        return $sku;
    }

    protected function getConfigurableAttributes($product)
    {
        $attributes = [];
        foreach ($product->getTypeInstance()->getConfigurableAttributesAsArray($product) as $id => $attribute) {
            $this->attributes->add($attribute['attribute_code'], $this->sanitize($attribute['store_label']));
            $attributes[] = $attribute['attribute_code'];
        }
        return $attributes;
    }

    protected function getQty($product)
    {
        $sumQty = 0;
        foreach ($this->getEnabledUsedProducts($product) as $childProduct) {
            $qty = $this->getStockItem($childProduct->getId())->getQty();
            if ($qty < $this->config->getMinimumQtyAllowed()) {
                continue;
            }
            $sumQty += $qty;
        }
        return $sumQty;
    }

    protected function getEnabledUsedProducts($product)
    {
        $products = [];
        foreach ($product->getTypeInstance()->getUsedProducts($product) as $childProduct) {
            if ($childProduct->getStatus() != 1) {
                continue;
            }
            $products[] = $childProduct;
        }
        return $products;
    }
}

<?php
namespace Acty\Astri\Model;


class Config
{
    public function getStandardDeliveryDisabledAttributes()
    {
        return ['noestoniansmartpost', 'nopost24', 'no_dpd_parcel_locker'];
    }

    public function getUsedStoreId()
    {
        return 1;
    }

    public function getMinimumQtyAllowed()
    {
        return 5;
    }
}

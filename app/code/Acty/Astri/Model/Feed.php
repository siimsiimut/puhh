<?php
namespace Acty\Astri\Model;


class Feed
{
    protected $emulation;

    protected $config;

    protected $products;

    protected $attributes;


    public function __construct(
        \Magento\Store\Model\App\Emulation $emulation,
        \Acty\Astri\Model\Config $config,
        \Acty\Astri\Model\Products $products,
        \Acty\Astri\Model\Attributes $attributes
    ) {
        $this->emulation = $emulation;
        $this->config = $config;
        $this->attributes = $attributes;
        $this->products = $products;
    }

    public function getXml()
    {
        $this->emulation->startEnvironmentEmulation($this->config->getUsedStoreId(), 'frontend');

        $feed = new \SimpleXMLElement('<feed/>');
        $attributes = $this->attributes->merge($feed);
        $itemsXml = $feed->addChild('items');
        $this->products->merge($itemsXml, $attributes);

        $this->emulation->stopEnvironmentEmulation();

        return $feed->asXML();
    }
}

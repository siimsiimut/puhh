<?php
namespace Acty\Astri\Model;


class CategoryCache
{
    protected $categoryFactory;

    protected $categories = false;


    public function __construct(
        \Magento\Catalog\Model\CategoryFactory $categoryFactory
    ) {
        $this->categoryFactory = $categoryFactory;
    }

    public function getProductCategories($product)
    {
        $this->initCategories();
        $categories = [];
        foreach ($product->getCategoryIds() as $id) {
            if (!array_key_exists($id, $this->categories)) {
                continue;
            }
            $name = $this->categories[$id];
            if (in_array($name, $categories)) {
                continue;
            }
            $categories[] = $name;
        }
        return $categories;
    }

    protected function initCategories()
    {
        if ($this->categories !== false) {
            return;
        }

        $this->saveToCache($this->getCategoryCollection());
    }

    protected function getCategoryCollection()
    {
        return $this->categoryFactory
                    ->create()
                    ->getCollection()
                    ->addAttributeToSelect('*')
                    ->addFieldToFilter('is_active', 1);
    }

    protected function saveToCache($collection)
    {
        $this->categories = [];
        foreach ($collection as $cat) {
            if (in_array($cat->getId(), [1, 2])) {
                continue;
            }
            $this->categories[$cat->getId()] = $cat->getName();
        }
    }
}

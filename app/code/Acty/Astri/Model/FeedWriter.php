<?php
namespace Acty\Astri\Model;


class FeedWriter
{
    protected $filesystem;

    protected $feed;

    protected $logger;


    public function __construct(
        \Magento\Framework\Filesystem $filesystem,
        \Acty\Astri\Model\Feed $feed,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->filesystem = $filesystem;
        $this->feed = $feed;
        $this->logger = $logger;
    }

    public function generateXml()
    {
        try {
            $media = $this->filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
            $media->writeFile("astri/feed.xml", $this->feed->getXml());
        } catch(Exception $e) {
            $this->logger->error(__FILE__.' '.__FUNCTION__);
            $this->logger->error($e->getMessage());
        }
    }
}

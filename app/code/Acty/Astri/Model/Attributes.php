<?php
namespace Acty\Astri\Model;

class Attributes
{
    public function __construct(
         \Magento\Framework\Filter\FilterManager $filter,
         \Magento\Catalog\Api\ProductAttributeRepositoryInterface $attributeRepository

    ) {
        $this->filter = $filter;
        $this->attributeRepository = $attributeRepository;
    }

    const USED_ATTRIBUTES = ['gender', 'character', 'vanus_kuudes'];
    protected $addedAttributesCodes = [];

    protected $attributesFeed;

    public function merge($feed)
    {
        $this->attributesFeed = $feed->addChild('attributeTypes');
        $this->addAtttributeLabels();
        return $this;
    }

    protected function addAtttributeLabels()
    {
        foreach (self::USED_ATTRIBUTES as $attributeCode) {
            $this->add($attributeCode, $this->getLabel($attributeCode));
        }
    }

    protected function getLabel($attributeCode)
    {
        //$attribute = $this->attributeRepository->get($attributeCode);
        //return $attribute->getStoreLabel();
        $attributes = ['gender' => 'Sugu', 'character' => 'Tegelane', 'vanus_kuudes'=>'Vanus'];
        return $attributes[$attributeCode];
    }

    public function add($attributeCode, $label)
    {
        if (in_array($attributeCode, $this->addedAttributesCodes)) {
            return;
        }
        $this->addedAttributesCodes[] = $attributeCode;

        $xml = $this->attributesFeed->addChild('attributeType');
        $xml->addAttribute('code', $attributeCode);
        $xml->addChild('et',$label);
    }

    public function generateCode($label)
    {
        $code = substr(preg_replace('/[^a-z_0-9]/', '_', $this->filter->translitUrl($label)), 0, 30);
        $validatorAttrCode = new \Zend_Validate_Regex(['pattern' => '/^[a-z][a-z_0-9]{0,29}[a-z0-9]$/']);
        if (!$validatorAttrCode->isValid($code)) {
            $code = 'attr_' . ($code ?: substr(md5(time()), 0, 8));
        }
        return $code;
    }
}

<?php
namespace Acty\Astri\Model;

class CustomOptionsProduct extends Product
{
    public function addVariations($cProduct, $productXml)
    {
        $product = $this->productRepository->getById($cProduct->getId());
        $variationsXml = $productXml->addChild('variations');
        foreach ($product->getOptions() as $options) {
            $attributeCode = $this->attributes->generateCode($options->getTitle());
            $this->attributes->add($attributeCode, $options->getTitle());
            foreach ($options->getValues() as $data) {
                $id = $this->addVariation($product, $attributeCode, $data);
                $variationsXml->addChild('variation', $id);
            }
        }
    }

    public function addVariation($product, $attributeCode, $data)
    {
        $productXml = $this->variationsFeed->addChild('variation');
        $sku = $this->sanitize($data['sku']);
        $productXml->addChild('id', $sku);
        $productXml->addChild('sku', $sku);
        $productXml->addChild('price', $product->getPrice());

        if ($product->getPrice() != $product->getFinalPrice()) {
            $productXml->addChild('discountPrice', $product->getFinalPrice());
        }

        $productXml->addChild('quantity', $this->getStockItem($product->getId())->getQty());

        $this->addCustomOption($productXml, $attributeCode, $data->getTitle());

        return $data['sku'];
    }

    protected function addCustomOption($productXml, $attributeCode, $value)
    {
        $attributesXml = $productXml->addChild('attributes');
        $value = $this->getAttributeValue($value);
        if (!$value) {
            return;
        }
        $attributeXml = $attributesXml->addChild('attribute');
        $attributeXml->addAttribute('id', $attributeCode . '_' . $this->attributes->generateCode($value));
        $attributeXml->addAttribute('code', $attributeCode);
        $attributeXml->addChild('et', $this->sanitize($value));
    }
}

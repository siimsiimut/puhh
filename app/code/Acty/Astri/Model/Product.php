<?php
namespace Acty\Astri\Model;

use Magento\Framework\App\Helper\Context;

use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Helper\Stock;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Helper\Image;

class Product
{
    protected $mainCategories = false;
    protected $qtyTable = false;

    protected $productsFeed;
    protected $variationsFeed;

    public function __construct(
        ProductFactory $productFactory,
        Image $imageHelper,
        \Magento\Catalog\Model\Product\Option $options,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        CategoryCache $categoryCache,
        Config $config
    ) {
        $this->productFactory = $productFactory;
        $this->imageHelper   = $imageHelper;
        $this->options = $options;
        $this->productRepository = $productRepository;
        $this->stockItemRepository = $stockItemRepository;
        $this->categoryCache = $categoryCache;
        $this->config = $config;
    }

    public function setProductsFeed($productsFeed)
    {
        $this->productsFeed = $productsFeed;
    }

    public function setVariationsFeed($variationsFeed)
    {
        $this->variationsFeed = $variationsFeed;
    }

    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    public function add($product)
    {
        $qty = $this->getQty($product);
        if ($qty < $this->config->getMinimumQtyAllowed()) {
            return;
        }
        $productXml = $this->productsFeed->addChild('product');

        $productXml->addChild('id', $product->getId());
        $this->addTitles($productXml, $product);
        $this->addDescriptions($productXml, $product);
        $this->addImages($productXml, $product);
        $this->addCategories($productXml, $product);
        $productXml->addChild('brand', $this->sanitize($product->getAttributeText('brand')));

        $productXml->addChild('sku', $this->sanitize($product->getSku()));
        $productXml->addChild('price', $product->getPrice());
        if ($product->getPrice() != $product->getFinalPrice()) {
            $productXml->addChild('discountPrice', $product->getFinalPrice());
        }

        $productXml->addChild('quantity', $qty);
        $productXml->addChild('ean', $this->sanitize($product->getData('ean13')));
        $productXml->addChild('deliveryOption', $this->getDeliveryOption($product));

        $this->addAttributes($productXml, $product, ['gender', 'character', 'vanus_kuudes']);

        $this->addVariations($product, $productXml);
    }

    protected function getQty($product)
    {
        return $this->getStockItem($product->getId())->getQty();
    }

    protected function getDeliveryOption($product)
    {
        return $this->hasRestrictedShippingMethods($product) ? 'COURIER' : 'STANDARD';
    }

    protected function hasRestrictedShippingMethods($product)
    {
        foreach ($this->config->getStandardDeliveryDisabledAttributes() as $attributeCode) {
            if ($product->getData($attributeCode) == 1) {
                return true;
            }
        }
        return false;
    }

    public function getStockItem($productId)
    {
        #friday fix
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        return $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface')->getStockItem($productId);
        #return $this->stockItemRepository->get($productId);
    }

    public function addVariations($cProduct, $productXml)
    {
        return;
    }

    protected function addAttributes($productXml, $product, $allowedAttributes)
    {
        $attributesXml = $productXml->addChild('attributes');
        foreach ($allowedAttributes as $attributeCode) {
            $value = $this->getAttributeValue($product->getAttributeText($attributeCode));
            if (!$value) {
                continue;
            }
            $attributeXml = $attributesXml->addChild('attribute');
            $attributeXml->addAttribute('id', $this->getValueKey($product, $attributeCode));
            $attributeXml->addAttribute('code', $attributeCode);
            $attributeXml->addChild('et', $this->sanitize($value));
        }
    }

    protected function getAttributeValue($value)
    {
        if (is_array($value)) {
            $value = implode(',', $value);
        }
        return $value;
    }

    protected function getValueKey($product, $attributeCode)
    {
        $value = $product->getData($attributeCode);
        $value = str_replace(',', '_', $value);
        return $attributeCode . '_' . $value;
    }

    protected function addTitles($productXml, $product)
    {
        $node = $productXml->addChild('titles');
        $node->addChild('et', $this->sanitize($product->getName()));
    }


    protected function addDescriptions($productXml, $product)
    {
        $node = $productXml->addChild('descriptions');
        $node->addChild('et', $this->sanitize($product->getDescription()));
    }

    protected function addImages($productXml, $product)
    {
        $pictures = $productXml->addChild('images');
        foreach ($product->getMediaGalleryEntries() as $image) {
            $img = $this->imageHelper->init($product, 'product_page_image_large')->setImageFile($image->getFile());
            $pictures->addChild('image', str_replace('/pub/', '/', $img->getUrl()));
        }
    }

    protected function addCategories($productXml, $product)
    {
        $categoriesXml = $productXml->addChild('categories');
        foreach (array_slice($this->categoryCache->getProductCategories($product), 0, 3) as $name) {
            $categoriesXml->addChild('category', $this->sanitize($name));
        }
    }

    protected function sanitize($value)
    {
        return htmlspecialchars($value);
    }
}

<?php
namespace Acty\Astri\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\State;
use Acty\Astri\Model\FeedWriter;

class GenerateXml extends Command
{
    public function __construct(
        State $state,
        FeedWriter $feedWriter,
        $name = null
    ) {
        $this->state = $state;
        $this->feedWriter = $feedWriter;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('astri:generate:xml');
        $this->setDescription('Generate fresh astri xml');

        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
        $this->feedWriter->generateXml();
    }
}

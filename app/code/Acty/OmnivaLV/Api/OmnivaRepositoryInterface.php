<?php
namespace Acty\OmnivaLV\Api;

use Acty\OmnivaLV\Api\Data\OmnivaInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface OmnivaRepositoryInterface
{
    public function save(OmnivaInterface $page);

    public function getById($id);

    public function getList(SearchCriteriaInterface $criteria);

    public function delete(OmnivaInterface $page);

    public function deleteById($id);
}

<?php
namespace Acty\OmnivaLV\Setup;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Api\AttributeRepositoryInterface;



class UpgradeData implements UpgradeDataInterface
{

    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavAttributeSetupInterface;

    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory, AttributeRepositoryInterface $eavAttributeSetupInterface)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavAttributeSetupInterface = $eavAttributeSetupInterface;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            /** @var EavSetup $eavSetup */
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $attributesExist = true;

            try {
                $this->eavAttributeSetupInterface->get(\Magento\Catalog\Model\Product::ENTITY, 'nopost24lv');
                $attributesExist = true;
            }
            catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                $attributesExist = false;
            }

            if (!$attributesExist) {
                $eavSetup->addAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    'nopost24lv',
                    [
                        'group' => 'General',
                        'type' => 'int',
                        'input' => 'boolean',
                        'model' => 'Magento\Catalog\Model\ResourceModel\Eav\Attribute',
                        'backend' => 'Magento\Catalog\Model\Product\Attribute\Backend\Boolean',
                        'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                        'label' => 'OmnivaLV pakiautomaat keelatud',
                        'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => true,
                        'used_in_product_listing' => true,
                        'default' => null
                    ]
                );
            }
        }


        $setup->endSetup();
    }
}

<?php
namespace Acty\OmnivaLV\Model\Rate;

class Result extends \Magento\Shipping\Model\Rate\Result
{

    /*
     * Disable sorting by price.
     */
    public function sortRatesByPrice()
    {
        return $this;
    }

}

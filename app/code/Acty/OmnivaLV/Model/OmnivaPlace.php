<?php
namespace Acty\OmnivaLV\Model;
class OmnivaPlace extends \Magento\Framework\Model\AbstractModel implements \Acty\OmnivaLV\Api\Data\OmnivaInterface, \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'omniva_lv_parcels';

    protected function _construct()
    {
        $this->_init('Acty\OmnivaLV\Model\ResourceModel\OmnivaPlace');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}

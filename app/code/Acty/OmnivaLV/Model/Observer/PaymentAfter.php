<?php
namespace Acty\OmnivaLV\Model\Observer;
use Magento\Framework\Event\ObserverInterface;

class PaymentAfter implements ObserverInterface
{
    protected $_objectManager;
    protected $_logger;
    protected $helper;
    protected $service;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Acty\OmnivaLV\Helper\Data $helper,
        \Acty\OmnivaLV\Model\Service $service
    )
    {
        $this->_logger = $logger;
        $this->helper = $helper;
        $this->service = $service;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $sendType = $this->helper->getSendType();
        if ($sendType != \Acty\OmnivaLV\Model\Source\Sendtype::POST24_MAIN_AFTER_PAYMENT)  {
            return;
        }
        /* @var $order \Magento\Sales\Model\Order */
        $order = $observer->getPayment()->getOrder();

        $result = $this->service->sendEplisPreSendMsg($order);

        if ($result !== false and is_array($result)) {
            $comment = $this->_createOrderComment($result);
            $order->addStatusHistoryComment($comment)->setIsCustomerNotified(null);
            $order->save();
        }
    }

    private function _createOrderComment($result)
    {
        $orderComment = '<b>' . __('Post24lv EPLIS') . '</b><br />';
        $orderComment .= ' - ' . __('Message') . ': ' . __($result['msg']) . '<br />';
        if (!empty($result['saved'])) {
            $orderComment .= ' --- <i>' . __('Saved') . ': </i><br />';
            foreach($result['saved'] as $saved) {
                $orderComment .= ' --- --- ' . __('SKU') . ': ' . $saved['sku'] . ' ' . __('Barcode') . ': ' . $saved['barcode'] .  '<br />';
            }
        }
        if (!empty($result['faulty'])) {
            $orderComment .= ' --- <i>' . __('Faulty') . ': </i><br />';
            foreach($result['faulty'] as $faulty) {
                $orderComment .= ' --- --- ' . __('SKU') . ': ' . $faulty['sku'] . ' ' . __('Barcode') . ': ' . $faulty['barcode'] .  '<br />';
            }
        }
        return $orderComment;
    }

}

<?php
namespace Acty\OmnivaLV\Model\ResourceModel\OmnivaPlace;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Acty\OmnivaLV\Model\OmnivaPlace','Acty\OmnivaLV\Model\ResourceModel\OmnivaPlace');
    }
}

<?php
namespace Acty\OmnivaLV\Model\ResourceModel;
class OmnivaPlace extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('omnivalv_parcel_points','id');
    }
}

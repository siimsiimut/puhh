<?php
namespace Acty\OmnivaLV\Model;

class Cron extends \Magento\Framework\Model\AbstractModel
{
    protected $service;

    protected $omnivaPlaces;

    public function __construct(
        \Acty\OmnivaLV\Model\Service $service,
        \Acty\OmnivaLV\Model\OmnivaPlaceFactory $omnivaPlaceFactory
    ) {
        $this->service = $service;
        $this->_omnivaPlaces = $omnivaPlaceFactory;
    }

    public function execute()
    {
        $this->refreshPackMachines();
    }

    public function refreshPackMachines()
    {
        $places = $this->service->getPackMachines();

        $this->removeOldPackMachines($places);
        foreach ($places as $place) {
            $parcelPoint = $this->_omnivaPlaces->create();
            $oldEntry = $parcelPoint->getCollection()->addFieldToFilter('place_id', $place['zip'])->addFieldToSelect('id')->getFirstItem();
            if ($oldEntry->getId()) {
                $parcelPoint->setData('id', $oldEntry->getId());
            }
            $parcelPoint->setData('place_id', $place['zip']);
            $parcelPoint->setData('name', $place['name']);
            $parcelPoint->setData('country', $place['country']);
            $parcelPoint->setData('group', $place['group_name']);
            $parcelPoint->save();
        }
    }

    protected function removeOldPackMachines($vendorPlaces)
    {
        $places = [];
        foreach ($vendorPlaces as $place) {
            $places[$place['zip']] = true;
        }

        foreach ($this->_omnivaPlaces->create()->getCollection() as $place) {
            $id = $place->getData('place_id');

            if (! isset($places[$id])) {
                $place->delete();
            }
        }
    }
}

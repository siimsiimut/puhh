<?php
namespace Acty\OmnivaLV\Model\Carrier;

use Magento\Catalog\Model\ProductRepository;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;
use Acty\OmnivaLV\Model\Service;
use Magento\Framework\App\ObjectManager;
use Acty\OmnivaLV\Helper\Data;

class Post24 extends \Magento\Shipping\Model\Carrier\AbstractCarrier
    implements \Magento\Shipping\Model\Carrier\CarrierInterface
{

    const POST24_NOPOST24_PRODUCT_ATTRIBUTE = 'nopost24lv';

    const PACK_MACHINE_NAME_SUFFIX = 'Post24 pack';

    const CARRIER_CODE = 'post24lv';

    /**
     * @var string
     */
    protected $_code = self::CARRIER_CODE;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart = null;

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $_http = null;

    protected $service;

    protected $productRepository;

    protected $rateResult;

    protected $_rateResultFactory;
    protected $_rateMethodFactory;

    protected $helper;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Magento\Shipping\Model\Rate\Result $rateResult,
        Service $service,
        ProductRepository $productRepository,
        \Acty\OmnivaLV\Helper\Data $helper,
        array $data = []
    ) {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        $this->_rateErrorFactory = $rateErrorFactory;

        $this->_cart = $cart;
        $this->_http = $httpClientFactory;

        $this->service = $service;
        $this->productRepository = $productRepository;
        $this->rateResult = $rateResult;
        $this->helper = $helper;

        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * @return array
     */
    // public function getAllowedMethods()
    // {
    //     return [$this->_code => $this->getConfigData('name')];
    // }

    public function getAllowedMethods()
    {
        $om = ObjectManager::getInstance();
        $data = [];

        foreach ($om->create(Data::class)->getPackMachines() as $point) {
            $data[$point['place_id']] = $point;
            $data[$point['place_id']]['group_id'] = md5($point['group']);
            $data[$point['place_id']]['group_name'] = $point['group'];
        }

        return $data;
    }

    /**
     * @param RateRequest $request
     * @return bool|Result
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        // check for excluded products
        $isCheckEnabled = $this->getConfigFlag('check_enable');
        if ($isCheckEnabled) {
            $excludedProduct = false;
            foreach ($request->getAllItems() as $item) {
                $product = $this->productRepository->getById($item->getProduct()->getId());
                $nopost24 = $product->getData(self::POST24_NOPOST24_PRODUCT_ATTRIBUTE);
                if ($nopost24) {
                    $excludedProduct = true;
                }
            }

            if ($excludedProduct) {
                //return false;
                if ($this->getConfigData('showmethod')) {
                    $error = $this->_rateErrorFactory->create();
                    $error->setCarrier($this->_code);
                    $error->setCarrierTitle($this->getConfigData('title'));
                    $errorMsg = $this->getConfigData('specificerrmsg');
                    $error->setErrorMessage($errorMsg ? $errorMsg : __('One or more products are not allowed to be shipped with Omniva.'));
                    return $error;
                }
                else {
                    return false;
                }
            }
        }

        $freeBoxes = 0;
        if ($request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {

                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }

                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                            $freeBoxes += $item->getQty() * $child->getQty();
                        }
                    }
                } elseif ($item->getFreeShipping()) {
                    $freeBoxes += $item->getQty();
                }
            }
        }
        $this->setFreeBoxes($freeBoxes);

        $isFreeShippingEnabled = $this->getConfigFlag('free_shipping_enable');
        $packageValue = $request->getPackageValueWithDiscount();

        $shippingPrice = $this->getConfigData('handling_fee');
        if (empty($shippingPrice) || !is_numeric($shippingPrice) || $shippingPrice < 0) {
            $shippingPrice = '0.00';
        }

        if ($isFreeShippingEnabled && $packageValue >= $this->getConfigData('free_shipping_subtotal')) {
            $shippingPrice = '0.00';
        }

        if ($request->getPackageQty() == $this->getFreeBoxes()) {
            $shippingPrice = '0.00';
        }

        $result = $this->_rateResultFactory->create();

        $method = $this->_rateMethodFactory->create();

        $method->setCarrier($this->_code);
        $method->setCarrierTitle($this->getConfigData('title'));

        //$method->setMethod($this->_code);
        $method->setMethodTitle($this->getConfigData('name'));

        $method->setPrice($shippingPrice);
        $method->setCost($shippingPrice);

        $result->append($method);

        $packetMachines = $this->helper->getPackMachines();

        $items = $request->getAllItems();

        if (!$items) {
            return $result;
        }
        $shippingMethod = $items[0]->getQuote()->getShippingAddress()->getShippingMethod();
        if (strpos($shippingMethod, self::CARRIER_CODE) !== false) {
            $preSetPlaceId = str_replace(self::CARRIER_CODE."_","",$shippingMethod);
            foreach ($packetMachines as $place) {
                if($place['place_id'] == $preSetPlaceId) {
                    $method = $this->_rateMethodFactory->create();
                    $method->setCarrier($this->_code);
                    $method->setCarrierTitle($this->getConfigData('title'));
                    $method->setMethod($place['place_id']);
                    $method->setMethodTitle($place['name']);
                    $method->setCost($shippingPrice);
                    $method->setPrice($shippingPrice);
                    $result->append($method);
                }
            }
        }

        return $result;
    }
}

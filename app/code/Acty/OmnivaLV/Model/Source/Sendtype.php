<?php
namespace Acty\OmnivaLV\Model\Source;

class Sendtype implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * Defines after order place type
     */
    const POST24_MAIN_SEND_AFTER_ORDER = 'after_order_place';

    /**
     * Defines after order payment paid
     */
    const POST24_MAIN_AFTER_PAYMENT = 'after_payment_paid';


    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = array();
        $options[] = array(
            'label' => __('Peale tellimuse maksmist'),
            'value' => self::POST24_MAIN_AFTER_PAYMENT
        );
        return $options;
    }
}

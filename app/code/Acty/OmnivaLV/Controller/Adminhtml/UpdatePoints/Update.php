<?php
namespace Acty\OmnivaLV\Controller\Adminhtml\UpdatePoints;

use Magento\Framework\Controller\ResultFactory;

class Update extends \Magento\Backend\App\Action
{
    protected $cron;

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magento_AdminNotification::show_list');
    }

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Acty\OmnivaLV\Model\Cron $cron
    ){
        parent::__construct($context);
        $this->cron = $cron;
    }


    public function execute()
    {
        $this->cron->refreshPackMachines();

        $this->messageManager->addSuccessMessage('Pickup points updated');
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}

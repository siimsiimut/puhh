<?php
namespace Acty\OmnivaLV\Helper;

use Acty\OmnivaLV\Model\Carrier\POST24;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_POST24_DATA_EXCHANGE_ENABLE  = 'carriers/post24lv/data_exchange_enable';
    const XML_PATH_POST24_PARTNER_ID            = 'carriers/post24lv/partner_id';
    const XML_PATH_POST24_USERNAME              = 'carriers/post24lv/username';
    const XML_PATH_POST24_PASSWORD              = 'carriers/post24lv/password';
    const XML_PATH_POST24_SERVICE               = 'carriers/post24lv/service';
    const XML_PATH_POST24_PRESELECTED           = 'carriers/post24lv/preselected';
    const XML_PATH_POST24_SENDTYPE              = 'carriers/post24lv/send_type';
    const XML_PATH_POST24_FILE_ID_PREFIX        = 'carriers/post24lv/file_id_prefix';

    protected $scopeConfig;
    protected $checkoutSession;
    protected $omnivaPlaces;

    public function __construct(\Magento\Framework\App\Helper\Context $context, \Magento\Checkout\Model\Session $session, \Acty\OmnivaLV\Model\OmnivaPlaceFactory $omnivaPlaces)
    {
        $this->scopeConfig = $context->getScopeConfig();
        $this->checkoutSession = $session;
        $this->omnivaPlaces = $omnivaPlaces;

        return parent::__construct($context);
    }

    public function isPost24($code)
    {
        return POST24::CARRIER_CODE == $code;
    }

    public function isPost24PreSelected($code)
    {
        return $this->isPost24($code) && $this->isPreselected();
    }

    public function isPreselected()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_POST24_PRESELECTED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function isDataExchangeEnabled()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_POST24_DATA_EXCHANGE_ENABLE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getPartnerId()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_POST24_PARTNER_ID, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getUserName()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_POST24_USERNAME, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getSendType()
    {
        if ($this->scopeConfig->getValue(self::XML_PATH_POST24_SENDTYPE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
            return $this->scopeConfig->getValue(self::XML_PATH_POST24_SENDTYPE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        }
        else {
            return \Acty\OmnivaLV\Model\Source\Sendtype::POST24_MAIN_SEND_AFTER_ORDER;
        }
    }

    public function getPassword()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_POST24_PASSWORD, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getService()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_POST24_SERVICE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getFileIdPrefix()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_POST24_FILE_ID_PREFIX, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * Function to set quote shipping price
     * @param $shippingPrice
     * @return bool
     */
    public function setQuoteShipping($placeId) {
        $quote = $this->checkoutSession->getQuote();

        $places = $this->getPackMachines();

        if ($placeId) {
            foreach ($places as $place) {
                if ($place['place_id'] == $placeId) {
                    $method = POST24::CARRIER_CODE . '_' . $place['place_id'];
                    $quote->getShippingAddress()->setShippingDescription('Omniva - ' . trim($place['name']));
                    $quote->getShippingAddress()->setShippingMethod($method);
                    $quote->getShippingAddress()->save();
                    $quote->save();
                    $methodCode = $place['place_id'];
                    return $methodCode;
                }
            }
        }
        return true;
    }

    public function getPackMachines()
    {
        return $this->_getPlaces();
    }

    private function _getPlaces()
    {
        $parcels = $this->omnivaPlaces->create();
        $parcelPoints = $parcels->getCollection()->getData();

        return $parcelPoints;
    }

    /**
     * Sorts pack machines by the first three characters of their group_name.
     */
    public static function sort($a, $b)
    {
        $a = $a['group_name'];
        $b = $b['group_name'];
        return strncmp($a, $b, 3);
    }

}

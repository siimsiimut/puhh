<?php

namespace Acty\Checkout\Observers;

use Acty\Checkout\Services\CacheManager;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class ProductImageObserver implements ObserverInterface
{
    protected $cache;

    public function __construct(CacheManager $cache)
    {
        $this->cache = $cache;
    }

    public function execute(EventObserver $observer)
    {
        $key = 'image.'.$observer->getProduct()->getId().'.product_base_image';

        $this->cache->remove($this->cache->key($key));
    }
}

<?php

namespace Acty\Checkout\PaymentGateways;

abstract class AbstractGateway
{
    abstract public function get();

    public function isOrderNeeded()
    {
        return true;
    }
}

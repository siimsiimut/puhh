<?php

namespace Acty\Checkout\PaymentGateways;

use Acty\Checkout\PaymentGateways\AbstractGateway;
use Acty\Checkout\Support\Utils\GetUrl;

class Buyplan extends AbstractGateway
{
    protected $url;

    public function __construct(GetUrl $url)
    {
        $this->url = $url;
    }

    public function get()
    {
        return [
            'redirect' => $this->url->base('buyplan/redirect')
        ];
    }
}

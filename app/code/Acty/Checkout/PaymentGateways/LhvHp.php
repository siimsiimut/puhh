<?php

namespace Acty\Checkout\PaymentGateways;

use Acty\Checkout\PaymentGateways\AbstractGateway;
use Magento\Framework\ObjectManagerInterface;
use Magento\Sales\Model\OrderRepository;

class LhvHp extends AbstractGateway
{
    protected $builder;

    protected $config;

    protected $order;

    public function __construct(ObjectManagerInterface $objectManager, OrderRepository $order)
    {
        $this->builder = $objectManager->create(\Lhv\HirePurchase\Model\Request\BuilderComposite::class);
        $this->config = $objectManager->create(\Lhv\HirePurchase\Gateway\Config\Config::class);
        $this->order = $order;
    }

    public function get()
    {
        return ['post' => $this->config->getEndpointUrl()];
    }

    public function fields($orderId)
    {
        $fields = [];

        foreach ($this->builder->build($this->order->get($orderId)) as $name => $value) {
            $fields[] = ['name' => $name, 'value' => $value];
        }

        return $fields;
    }
}

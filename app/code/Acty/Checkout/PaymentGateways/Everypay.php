<?php

namespace Acty\Checkout\PaymentGateways;

use Acty\Checkout\PaymentGateways\AbstractGateway;
use Magento\Framework\ObjectManagerInterface;

class Everypay extends AbstractGateway
{
    protected $endpoint;

    protected $builder;

    protected $objectManager;

    public function __construct(ObjectManagerInterface $objectManager)
    {
        $this->endpoint = $objectManager->create(\Aedes\EveryPay\Model\Config::class);
        $this->builder = $objectManager->create(\Aedes\EveryPay\Model\Form\FormDataBuilder::class);
        $this->objectManager = $objectManager;
    }

    public function get()
    {
        return ['post' => $this->endpoint->getApiEndpoint()];
    }

    public function fields($orderId)
    {
        $fields = [];

        foreach ($this->builder->getFields($orderId) as $name => $value) {
            $fields[] = [
                'name' => $name, 'value' => $value
            ];
        }

        return $fields;
    }
}

<?php

namespace Acty\Checkout\PaymentGateways;

use Acty\Checkout\PaymentGateways\AbstractGateway;
use Magento\Checkout\Model\Session;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Session\SessionManagerInterface as CoreSession;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Makecommerce extends AbstractGateway
{
    protected $orderRepository;

    protected $transactionManager;

    protected $quoteRepository;

    protected $transaction;

    protected $storeManager;

    protected $customer;

    protected $resolver;

    protected $objectManager;

    protected $checkoutSession;

    protected $coreSession;

    protected $cards = ['mastercard', 'visa', 'maestro'];

    public function __construct(
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Locale\ResolverInterface $resolver,
        Session $checkoutSession,
        CoreSession $coreSession,
        ObjectManagerInterface $objectManager,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->orderRepository = $orderRepository;
        $this->quoteRepository = $quoteRepository;
        $this->transactionManager = $objectManager->create('\Makecommerce\Ecommerce\Api\TransactionManagerInterface');
        $this->transaction = $objectManager->create('\Makecommerce\Ecommerce\Api\Data\TransactionInterface');
        $this->customer = $objectManager->create('\Makecommerce\Ecommerce\Api\Data\CustomerInterface');
        $this->storeManager = $storeManager;
        $this->resolver = $resolver;
        $this->checkoutSession = $checkoutSession;
        $this->coreSession = $coreSession;
        $this->config = $objectManager->create('\Makecommerce\Ecommerce\Config');
        $this->scopeConfig = $scopeConfig;
    }

    public function get()
    {
        return [
            'redirect' => ''
        ];
    }

    public function getUrl()
    {
        $order = $this->checkoutSession->getLastRealOrder();

        $order->setStatus('pending_payment');
        $order->setState('pending_payment');
        $order->getResource()->save($order);

        $orderId = $order->getEntityId();
        $country = strtolower($this->getScope('general/country/default'));
        $selected_payment_method = $this->coreSession->getMakecommerceMethod();
        $quote = $this->quoteRepository->get($order->getQuoteId());

        $transaction = $this->transaction;
        $transaction
            ->setAmount(number_format($order->getGrandTotal(), 2))
            ->setCurrency($this->storeManager->getStore()->getCurrentCurrency()->getCode())
            ->setReference($order->getQuoteId());

        $customer = $this->customer;
        $customer
            ->setIp($quote->getRemoteIp())
            ->setCountry($country)
            ->setLocale(substr($this->resolver->getLocale(), 0, 2));

        $transaction = $this->transactionManager->create($transaction, $customer);
        $additionalInformation = $quote->getPayment()->getAdditionalInformation();
        $additionalInformation['transaction_id'] = $transaction->getId();
        $additionalInformation['method_title'] = $quote->getPayment()->getMethod();

        $quote->getPayment()->setAdditionalInformation($additionalInformation);
        $quote->save();
        $order->getPayment()->setAdditionalInformation($additionalInformation);
        $order->save();

        $transaction->setOrderIds([$orderId]);
        $transaction->save();

        if (in_array($selected_payment_method, $this->cards)) {
            $env = $this->scopeConfig->getValue('makecommerce/ecommerce/environment');

            $data = $transaction->getData();
            $key = ($env === 'test') ? 'test_key_public' : 'api_public';
            $data['key'] = $this->scopeConfig->getValue('makecommerce/ecommerce/'.$key);
            $data['name'] = $this->scopeConfig->getValue('payment/makecommerce/cc_shop_name');

            if ($env === 'test') {
                $data['src'] = 'https://payment-test.maksekeskus.ee/checkout/dist/checkout.js';
            } else {
                $data['src'] = 'https://payment.maksekeskus.ee/checkout/dist/checkout.js';
            }

            return $data;
        }

        $banklinks = $transaction->getPaymentMethods()->getBanklinks();

        foreach ($banklinks as $payment_method) {
            if ($payment_method['name'] == $selected_payment_method and $payment_method['country'] == $country) {
                return $payment_method['url'];
            }
        }

        return false;
    }

    protected function getScope($option)
    {
        return $this->scopeConfig->getValue($option, ScopeInterface::SCOPE_STORE);
    }
}

<?php

namespace Acty\Checkout\PaymentGateways;

use Acty\Checkout\PaymentGateways\AbstractGateway;
use Acty\Checkout\Support\Utils\GetUrl;

class AmbientiaEstonianbanklinksSwedbank extends AbstractGateway
{
    protected $url;

    public function __construct(GetUrl $url)
    {
        $this->url = $url;
    }

    public function get()
    {
        return [
            'redirect' => $this->url->base('ambientia_estonianbanklinks/payment/start/bankcode/swedbank')
        ];
    }
}

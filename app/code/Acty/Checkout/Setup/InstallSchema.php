<?php

namespace Acty\Checkout\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $field = [
            'type' => Table::TYPE_TEXT,
            'length' => 32,
            'nullable' => true,
            'comment' => 'Company registration code'
        ];

        $setup->getConnection()->addColumn(
            $setup->getTable('quote_address'), 'registration_code', $field
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('quote'), 'customer_comment', $field
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_address'), 'registration_code', $field
        );

        $setup->endSetup();
    }
}

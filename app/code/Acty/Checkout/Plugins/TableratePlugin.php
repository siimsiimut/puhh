<?php


namespace Acty\Checkout\Plugins;


use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;

class TableratePlugin
{
    /**
     * @var \Acty\Buum\Helper\Warehouse
     */
    protected $warehouse;

    /**
     * TableratePlugin constructor.
     * @param \Acty\Buum\Helper\Warehouse $warehouse
     */
    public function __construct(\Acty\Buum\Helper\Warehouse $warehouse)
    {
        $this->warehouse = $warehouse;
    }

    public function afterCollectRates(\Magento\OfflineShipping\Model\Carrier\Tablerate $subject, Result $result, RateRequest $request)
    {
        $items = $request->getAllItems();
        $filteredStocks = $this->warehouse->getFilteredStocks('Tallinn', 'city_group');

        $allowedData = $this->getAllowedData($items, $filteredStocks);

        if ($allowedData['allowed']) {
            return $result;
        }

//        $carrierRates = $result->getRatesByCarrier($subject->getCarrierCode());
//
//        foreach ($carrierRates as $rate) {
//            $rate->setData('error_message', 'Toode/tooted ei ole saadaval samas Tallinna laos ning antud tellimust ei ole võimalik 2h kohale toimetada.');
//            $rate->setData('price', null);
//            $rate->setData('cost', null);
//            ObjectManager::getInstance()->get(LoggerInterface::class)->info(json_encode($rate->getData()));
//        }
//        $result->setError(true);

        return false;
    }

    protected function getAllowedData($items, $data)
    {
        $allowed = [];
        $stockPriority = [];
        $first = true;

        foreach ($items as $item) {
            if ($item->getHasChildren()) {
                continue;
            }
            $qty = $item->getQty();
            $warehouses = $item->getProduct()->getWarehouseQuantity() ? json_decode($item->getProduct()->getWarehouseQuantity(), true) : [];

            $itemAllowed = [];
            foreach ($data as $row) {
                if (array_key_exists($row['buum_stock_id'], $warehouses)) {
                    if ($warehouses[$row['buum_stock_id']] >= $qty) {
                        $itemAllowed[] = $row['buum_stock_id'];
                        if (array_key_exists($row['buum_stock_id'], $stockPriority)) {
                            $previous = $stockPriority[$row['buum_stock_id']];
                            $stockPriority[$row['buum_stock_id']] = $warehouses[$row['buum_stock_id']] + $previous;
                        } else {
                            $stockPriority[$row['buum_stock_id']] = $warehouses[$row['buum_stock_id']];
                        }
                    }
                }
            }

            if ($first === true) {
                $allowed = $itemAllowed;
                $first = false;
            } else {
                $allowed = array_intersect($allowed, $itemAllowed);
            }
        }
        return ['allowed' => $allowed, 'stock_priority' => $stockPriority];
    }

}
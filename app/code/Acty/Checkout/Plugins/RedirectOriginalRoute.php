<?php

namespace Acty\Checkout\Plugins;

use Magento\Framework\Controller\ResultFactory;

class RedirectOriginalRoute
{
    protected $factory;

    public function __construct(ResultFactory $factory)
    {
        $this->factory = $factory;
    }

    public function aroundExecute()
    {
        return $this->factory->create(ResultFactory::TYPE_REDIRECT)->setPath('purchase');
    }
}

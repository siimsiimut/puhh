<?php

namespace Acty\Checkout\Validators;

use Acty\Checkout\Repositories\CustomerRepository;
use Acty\Framework\Support\EnvConfig;
use Magento\Customer\Model\Session as Customer;

class EmailValidates
{
    protected $customerRepository;

    protected $customer;

    protected $env;

    public function __construct(CustomerRepository $customerRepository, Customer $customer, EnvConfig $env)
    {
        $this->customerRepository = $customerRepository;
        $this->customer = $customer;
        $this->env = $env;
    }

    public function validates($email)
    {
        $customer = $this->customerRepository->getByEmail($email);

        if (is_null($customer) || $this->env->get('checkout.customer.email', false) === true) {
            return true;
        }

        if ($this->customer->isLoggedIn()) {
            $loggedCustomerEmail = $this->customer->getCustomer()->getEmail();

            if ($customer['email'] == $loggedCustomerEmail) {
                return true;
            }
        }

        return false;
    }
}

<?php

namespace Acty\Checkout\Repositories;

use Magento\Framework\App\ResourceConnection;

class CustomerRepository
{
    public function __construct(ResourceConnection $resourceConnection)
    {
        $this->resourceConnection = $resourceConnection;
        $this->db = $this->resourceConnection->getConnection();
    }

    public function getByEmail($email)
    {
        $query = sprintf(
            "select * from %s where email='%s'",
            $this->resourceConnection->getTableName('customer_entity'),
            $email
        );

        $result = $this->db->fetchAll($query);

        return empty($result) ? null : $result[0];
    }

    public function getLastOrderId($customerId)
    {
        $query = sprintf(
            "select * from %s where customer_id='%s' order by created_at desc limit 1",
            $this->resourceConnection->getTableName('sales_order'),
            $customerId
        );

        $result = $this->db->fetchAll($query);

        return empty($result) ? null : $result[0]['entity_id'];
    }
}

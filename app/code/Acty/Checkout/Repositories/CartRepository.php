<?php

namespace Acty\Checkout\Repositories;

use Magento\Checkout\Model\Cart;
use Exception;

class CartRepository
{
    protected $cart;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    public function removeItem($itemId)
    {
        try {
            $this->cart->removeItem($itemId)->save();
        } catch (Exception $e) {
            return __('Cannot remove item from cart');
        }

        return true;
    }

    public function updateItemQty($itemId, $qty)
    {
        try {
            $this->cart->updateItems([
                $itemId => ['qty' => $qty]
            ])->save();
        } catch (Exception $e) {
            return __('Cannot updatem item quantity');
        }

        return true;
    }

    public function __call($name, $args)
    {
        return $this->cart->{$name}(...$args);
    }
}

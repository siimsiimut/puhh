<?php

namespace Acty\Checkout\Block;

use Acty\Checkout\Support\Data\GetAgreements;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Component\ComponentRegistrar;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;

class Submit extends Template
{
    protected $getAgreements;

    public function __construct(Context $context, GetAgreements $getAgreements)
    {
        $this->getAgreements = $getAgreements;

        parent::__construct($context);
    }

    public function getAgreements()
    {
        return $this->getAgreements->get();
    }
}

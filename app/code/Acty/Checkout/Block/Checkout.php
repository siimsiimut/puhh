<?php

namespace Acty\Checkout\Block;

use Acty\Checkout\Support\Data\GetCountries;
use Acty\Checkout\Support\Data\GetPolicy;
use Acty\Checkout\Support\Payment\GetPaymentMethods;
use Acty\Framework\Support\EnvConfig;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\State;
use Magento\Framework\Component\ComponentRegistrar;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;

class Checkout extends Template
{
    protected $components = [
        'crosssell',
        'shipping',
        'billing',
        'customer',
        'payment',
        'submit',
        'coupon',
        'cart'
    ];

    protected $getCountries;

    protected $scopeConfig;

    protected $getPaymentMethods;

    protected $componentRegistrar;

    protected $getPolicy;

    protected $state;

    protected $env;

    public function __construct(
        Context $context,
        GetCountries $getCountries,
        ScopeConfigInterface $scopeConfig,
        GetPaymentMethods $getPaymentMethods,
        ComponentRegistrar $componentRegistrar,
        GetPolicy $getPolicy,
        State $state,
        EnvConfig $env,
        \Magento\Framework\Locale\Resolver $store
    ) {
        $this->getCountries = $getCountries;
        $this->scopeConfig = $scopeConfig;
        $this->getPaymentMethods = $getPaymentMethods;
        $this->componentRegistrar = $componentRegistrar;
        $this->getPolicy = $getPolicy;
        $this->state = $state;
        $this->env = $env;
        $this->_store = $store;

        parent::__construct($context);
    }

    public function getComponents()
    {
        return $this->components;
    }

    public function getCountries()
    {
        return json_encode($this->getCountries->get());
    }

    public function getPaymentMethods()
    {
        return json_encode($this->getPaymentMethods->get());
    }

    public function getTranslations()
    {
        $path = $this->componentRegistrar->getPath(ComponentRegistrar::MODULE, 'Acty_Checkout');
        $validation = $this->translate(require($path.'/resources/lang/validation.php'));

        $translations = [
            'validation' => $validation
        ];

        return json_encode($translations);
    }

    public function getOptions()
    {
        $options = [
            'canDeleteItem' => $this->env->get('checkout.cart.delete', false),
            'canUpdateQty' => $this->env->get('checkout.cart.update', false),
            'hasCrosssell' => $this->env->get('checkout.crosssell.enabled', false),
            'hasCoupon' => $this->env->get('checkout.coupon.enabled', false),
            'hasBilling' => $this->env->get('checkout.billing.enabled', false),
            'hasComment' => $this->env->get('checkout.customer.comment', false),
            'hasPrivacy' => $this->env->get('checkout.privacy.enabled', false)
        ];

        return json_encode($options);
    }

    public function getPolicy()
    {
        return json_encode($this->getPolicy->get());
    }

    public function isProduction()
    {
        if ($this->state->getMode() === State::MODE_PRODUCTION) {
            return true;
        }

        return false;
    }

    protected function translate($translations)
    {
        foreach ($translations as & $translation) {
            $translation = __($translation);
        }

        return $translations;
    }

    public function getLocale()
    {
        // we use no special slug for store view, instead we use a subdomain

        /* $lang = $this->_store->getLocale();
        $langArray = ['en_US' => '/en', 'ru_RU' => '/ru'];
        if(array_key_exists($lang, $langArray)){
            return json_encode($langArray[$lang]);
        } */
        
        return json_encode("");
    }
}

<?php

namespace Acty\Checkout\Controller\Get;

use Acty\Checkout\Support\Data\GetInitialData;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Initial extends Action
{
    protected $factory;

    protected $getInitialData;

    public function __construct(Context $context, JsonFactory $factory, GetInitialData $getInitialData)
    {
        $this->factory = $factory;
        $this->getInitialData = $getInitialData;

        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->factory->create();

        $result->setData($this->getInitialData->get());

        return $result;
    }
}

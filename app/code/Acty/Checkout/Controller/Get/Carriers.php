<?php

namespace Acty\Checkout\Controller\Get;

use Acty\Checkout\Support\Carrier\GetCarriers;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Carriers extends Action
{
    protected $factory;

    protected $request;

    protected $getCarriers;

    public function __construct(Context $context, JsonFactory $factory, GetCarriers $getCarriers)
    {
        $this->request = $context->getRequest();
        $this->factory = $factory;
        $this->getCarriers = $getCarriers;

        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->factory->create();
        $country = $this->request->getParam('country', 'EE');

        $result->setData($this->getCarriers->get($country));

        return $result;
    }
}

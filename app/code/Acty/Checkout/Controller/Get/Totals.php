<?php

namespace Acty\Checkout\Controller\Get;

use Acty\Checkout\Support\Quote\GetTotals;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Totals extends Action
{
    protected $factory;

    protected $totals;

    public function __construct(Context $context, JsonFactory $factory, GetTotals $totals)
    {
        $this->factory = $factory;
        $this->totals = $totals;

        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->factory->create();

        $result->setData($this->totals->get());

        return $result;
    }
}

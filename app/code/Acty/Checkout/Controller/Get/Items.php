<?php

namespace Acty\Checkout\Controller\Get;

use Acty\Checkout\Support\Quote\GetCartItems;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Items extends Action
{
    protected $factory;

    protected $items;

    public function __construct(Context $context, JsonFactory $factory, GetCartItems $items)
    {
        $this->factory = $factory;

        parent::__construct($context);
        $this->items = $items;
    }

    public function execute()
    {
        $result = $this->factory->create();

        $result->setData($this->items->get());

        return $result;
    }
}

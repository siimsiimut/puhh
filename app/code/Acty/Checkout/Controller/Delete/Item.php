<?php

namespace Acty\Checkout\Controller\Delete;

use Acty\Checkout\Repositories\CartRepository;
use Acty\Checkout\Support\Quote\GetTotals;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Item extends Action
{
    protected $factory;

    protected $setCarrier;

    protected $getTotals;

    protected $cartRepository;

    public function __construct(Context $context, JsonFactory $factory, GetTotals $getTotals, CartRepository $cartRepository)
    {
        $this->factory = $factory;
        $this->getTotals = $getTotals;
        $this->cartRepository = $cartRepository;

        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->factory->create();
        $input = json_decode(file_get_contents('php://input'), true);
        $response = $this->cartRepository->removeItem($input['id']);

        if ($response !== true) {
            return $result->setData(['error' => $response]);
        }

        return $result->setData([
            'totals' => $this->getTotals->get($this->cartRepository->getQuote()),
            'message' => __('Product was removed from the cart')
        ]);
    }
}

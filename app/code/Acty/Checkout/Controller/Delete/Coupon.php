<?php

namespace Acty\Checkout\Controller\Delete;

use Acty\Checkout\Support\Quote\SetCouponCode;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Coupon extends Action
{
    protected $factory;

    protected $coupon;

    public function __construct(Context $context, JsonFactory $factory, SetCouponCode $coupon)
    {
        $this->factory = $factory;
        $this->coupon = $coupon;

        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->factory->create();

        $response = $this->coupon->set(null);

        if ($response === false) {
            return $result->setData(['error' => __('Coupon code not removed')]);
        }

        return $result->setData([
            'totals' => $response,
            'message' => __('Coupon code removed')
        ]);
    }
}

<?php

namespace Acty\Checkout\Controller\Finalize;

use Acty\Checkout\Support\Payment\GetGatewayResult;
use Acty\Checkout\Support\Quote\SetQuoteData;
use Acty\Checkout\Support\Utils\Subscribe;
use Acty\Checkout\Validators\EmailValidates;
use Exception;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Index extends Action
{
    protected $factory;

    protected $setQuoteData;

    protected $getGatewayResult;

    protected $emailValidates;

    public function __construct(
        Context $context,
        JsonFactory $factory,
        SetQuoteData $setQuoteData,
        GetGatewayResult $getGatewayResult,
        EmailValidates $emailValidates,
        Subscribe $subscribe
    ) {
        $this->factory = $factory;
        $this->setQuoteData = $setQuoteData;
        $this->getGatewayResult = $getGatewayResult;
        $this->emailValidates = $emailValidates;
        $this->subscribe = $subscribe;

        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->factory->create();

        try {
            $result = $this->store($result);
        } catch (Exception $e) {
            $result->setHttpResponseCode(422);

            $result->setData([
                'system' => __($e->getMessage())
            ]);

            return $result;
        }

        return $result;
    }

    public function store($result)
    {
        $input = json_decode(file_get_contents('php://input'), true);
        $errors = $this->validate($input['customer']['email']);

        if ( ! empty($errors)) {
            $result->setHttpResponseCode(422);
            $result->setData($errors);

            return $result;
        }

        if ($input['subscribed']) {
            $this->subscribe->handle($input['customer']['email']);
        }

        $this->setQuoteData->setShippingAddress(
            $input['customer']['firstname'],
            $input['customer']['lastname'],
            $input['customer']['email'],
            $input['customer']['phone'],
            $input['customer']['country'],
            isset($input['address']) ? $input['address']['street'] : null,
            isset($input['address']) ? $input['address']['city'] : null,
            isset($input['address']) ? $input['address']['postcode'] : null
        );

        if ($input['billing']['enabled'] === true) {
            $this->setQuoteData->setBillingAddress(
                $input['billing']['company'],
                $input['billing']['code'],
                $input['billing']['street'],
                $input['billing']['city'],
                $input['billing']['postcode']
            );
        }

        $this->setQuoteData->set(
            $input['shipping']['carrier'].'_'.$input['shipping']['method'],
            $input['payment']['method'],
            $input['customer']['comment']
        );

        $result->setData($this->getGatewayResult->get());

        return $result;
    }

    protected function validate($email)
    {
        $errors = [];

        if ( ! $this->emailValidates->validates($email)) {
            $errors['email'] = true;
        }

        return $errors;
    }
}

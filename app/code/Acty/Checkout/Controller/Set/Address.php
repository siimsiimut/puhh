<?php

namespace Acty\Checkout\Controller\Set;

use Acty\Checkout\Support\Quote\SetAddressData;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Address extends Action
{
    protected $factory;

    protected $setAddressData;

    public function __construct(Context $context, JsonFactory $factory, SetAddressData $setAddressData)
    {
        $this->factory = $factory;
        $this->setAddressData = $setAddressData;

        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->factory->create();
        $input = json_decode(file_get_contents('php://input'), true);

        $this->setAddressData->set($input['fields'], $input['billing']);

        $result->setData(true);

        return $result;
    }
}

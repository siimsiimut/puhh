<?php

namespace Acty\Checkout\Controller\Set;

use Acty\Checkout\Support\Quote\SetCartItem;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Item extends Action
{
    protected $factory;

    protected $cart;

    public function __construct(Context $context, JsonFactory $factory, SetCartItem $cart)
    {
        $this->factory = $factory;
        $this->cart = $cart;

        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->factory->create();
        $input = json_decode(file_get_contents('php://input'), true);

        $product = $this->cart->set($input['id'], $input['qty']);

        $result->setData([
            'success' => true,
            'message' => sprintf(__('You added %s to your shopping cart.'), $product->getName())
        ]);

        return $result;
    }
}

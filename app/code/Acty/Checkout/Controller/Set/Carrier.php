<?php

namespace Acty\Checkout\Controller\Set;

use Acty\Checkout\Support\Carrier\SetCarrier;
use Acty\Checkout\Support\Quote\GetTotals;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Carrier extends Action
{
    protected $factory;

    protected $setCarrier;

    protected $getTotals;

    public function __construct(Context $context, JsonFactory $factory, SetCarrier $setCarrier, GetTotals $getTotals)
    {
        $this->factory = $factory;
        $this->setCarrier = $setCarrier;
        $this->getTotals = $getTotals;

        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->factory->create();
        $input = json_decode(file_get_contents('php://input'), true);

        $this->setCarrier->set($input['carrier']);
        $result->setData($this->getTotals->get());

        return $result;
    }
}

<?php

namespace Acty\Checkout\Controller\Set;

use Acty\Checkout\Repositories\CartRepository;
use Acty\Checkout\Support\Quote\GetTotals;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Qty extends Action
{
    protected $factory;

    protected $getTotals;

    protected $cartRepository;

    public function __construct(Context $context, JsonFactory $factory, GetTotals $getTotals, CartRepository $cartRepository)
    {
        $this->factory = $factory;
        $this->getTotals = $getTotals;
        $this->cartRepository = $cartRepository;

        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->factory->create();
        $input = json_decode(file_get_contents('php://input'), true);
        $response = $this->cartRepository->updateItemQty($input['id'], $input['qty']);

        if ($response !== true) {
            return $result->setData(['error' => $response]);
        }

        return $result->setData([
            'totals' => $this->getTotals->get(),
            'message' => __('Cart item quantity updated')
        ]);
    }
}

<?php

namespace Acty\Checkout\Controller\Set;

use Acty\Checkout\Support\Quote\SetPayment;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Payment extends Action
{
    protected $factory;

    protected $setCarrier;

    protected $getTotals;

    protected $setPayment;

    public function __construct(Context $context, JsonFactory $factory, SetPayment $setPayment)
    {
        $this->factory = $factory;
        $this->setPayment = $setPayment;

        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->factory->create();
        $input = json_decode(file_get_contents('php://input'), true);

        $this->setPayment->set($input['payment'], $input['name']);

        $result->setData(true);

        return $result;
    }
}

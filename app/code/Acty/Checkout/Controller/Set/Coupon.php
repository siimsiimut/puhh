<?php

namespace Acty\Checkout\Controller\Set;

use Acty\Checkout\Support\Quote\SetCouponCode;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class Coupon extends Action
{
    protected $factory;

    protected $coupon;

    public function __construct(Context $context, JsonFactory $factory, SetCouponCode $coupon)
    {
        $this->factory = $factory;
        $this->coupon = $coupon;

        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->factory->create();
        $input = json_decode(file_get_contents('php://input'), true);

        $response = $this->coupon->set($input['code']);

        if ($response === false) {
            return $result->setData(['error' => __('Coupon code invalid')]);
        }

        return $result->setData([
            'totals' => $response,
            'message' => sprintf(__('Coupon code [ %s ] accepted'), $input['code'])
        ]);
    }
}

const mix = require('laravel-mix')

mix.js('resources/js/checkout.js', 'view/frontend/web/js/checkout.min.js')

if ( ! mix.inProduction()) {
    mix.sourceMaps().webpackConfig({ devtool: 'inline-source-map' })
}

<?php

namespace Acty\Checkout\Support\Carrier;

use Magento\Checkout\Model\Session;
use Magento\Quote\Model\Quote\TotalsCollector;
use Magento\Shipping\Model\Config;
use Devall\Smartpost\Helper\ParcelPoints;



class GetCarriers
{
    protected $carriers;

    protected $session;

    protected $totalsCollector;

    /**
     * @var ParcelPoints
     */
    protected $parcelPoints;

    public function __construct(Config $carriers, Session $session, TotalsCollector $totalsCollector, ParcelPoints $parcelPoints)
    {
        $this->carriers = $carriers;
        $this->session = $session;
        $this->totalsCollector = $totalsCollector;
        $this->parcelPoints = $parcelPoints;
    }

    public function get($country = 'EE')
    {
        return $this->getList($country);
    }

    protected function getList($country)
    {
        $shippingRates = $this->getShippingRates($country);

        $activeCarriers = $this->carriers->getActiveCarriers();
        $carriers = [];

        foreach ($shippingRates as $carrierRates) {
            foreach ($carrierRates as $rate) {
                $code = $rate->getCarrier();

                if (array_key_exists($code, $activeCarriers)) {
                    $carriers[$code] = $this->getCarrier($code, $rate, $activeCarriers[$code]);
                }
            }
        }

        return $carriers;
    }

    protected function getCarrier($code, $rate, $model)
    {
        $carrier = [
            'code' => $code,
            'address' => $code == 'tablerate' ? true : $model->getHasAddress(),
            'label' => $rate->getCarrierTitle(),
            'fee' => $rate->getPrice(),
            'active' => strlen($rate->getErrorMessage()) == 0,
            'errormsg' => $rate->getErrorMessage(),
            'methods' => $this->getMethods($model, $code)
        ];

        return $carrier;
    }

    protected function getMethods($model, $carrierCode)
    {
        $methods = [];

        //devall smartpost needs separate point fetching
        if ($carrierCode == 'smartpostestonia' || $carrierCode == 'smartpostfinland') {

            $pp = $this->parcelPoints->getParcelPointsListByMethod();

            foreach ($pp[$carrierCode] as $item) {
                $methods[] = [
                    'code' => $item['place_id'],
                    'label' => $item['fullName'],
                    'group_id' => $item['city'],
                    'group_name' => $item['city'] ?? __('Select')
                ];
            }
        } else {
            foreach ($model->getAllowedMethods() as $code => $item) {
                $item = is_object($item) ? (string) $item : $item;
                $groupId = $item['group_id'] ?? 0;
                $code = $code === 0 || empty($code) ? $carrierCode : $code;

                $methods[] = [
                    'code' => $code,
                    'label' => $item['name'] ?? $item,
                    'group_id' => $groupId,
                    'group_name' => $item['group_name'] ?? __('Select')
                ];
            } 
        }

        usort($methods, function($a, $b) {
            return $a['label'] <=> $b['label'];
        });

        $sorted = [];

        //smartpost doesnt have group_id's use group_name instead
        if ($carrierCode !== 'estoniansmartpost') {
            foreach ($methods as $method) {
                if ( ! isset($sorted[$method['group_id']])) {
                    $sorted[$method['group_id']] = [
                        'name' => $method['group_name'],
                        'points' => []
                    ];
                }

                $sorted[$method['group_id']]['points'][] = [
                    'code' => $method['code'],
                    'label' => $method['label']
                ];
            }
        } else {
            foreach ($methods as $method) {
                if ( ! isset($sorted[$method['group_name']])) {
                    $sorted[$method['group_name']] = [
                        'name' => $method['group_name'],
                        'points' => []
                    ];
                }
                $sorted[$method['group_name']]['points'][] = [
                    'code' => $method['code'],
                    'label' => $method['label']
                ];
            }
        }

        usort($sorted, function($a, $b) {
            return $a['name'] <=> $b['name'];
        });
        if ($carrierCode === 'estoniansmartpost' || $carrierCode === 'smartpostestonia' ) {
            $prevTln = array_search('Tallinn', array_column($sorted, 'name'));
            $prevTrt = array_search('Tartu', array_column($sorted, 'name'));
            $tln = $sorted[$prevTln];
            $trt = $sorted[$prevTrt];
            unset($sorted[$prevTln]);
            unset($sorted[$prevTrt]);
            
            array_unshift($sorted, $trt);
            array_unshift($sorted, $tln);
        }
        return $sorted;
    }

    protected function getShippingRates($country)
    {
        $quote = $this->session->getQuote();
        $shippingAddress = $quote->getShippingAddress();

        $shippingAddress->setCountryId($country);
        $shippingAddress->save();

        $shippingAddress->setCollectShippingRates(true);
        $this->totalsCollector->collectAddressTotals($quote, $shippingAddress);

        return $shippingAddress->getGroupedAllShippingRates();
    }
}

<?php

namespace Acty\Checkout\Support\Carrier;

use Magento\Checkout\Model\Session as Checkout;

class SetCarrier
{
    protected $checkout;

    public function __construct(Checkout $checkout)
    {
        $this->checkout = $checkout;
    }

    public function set($carrier)
    {
        $quote = $this->checkout->getQuote();

        $quote->getShippingAddress()
            ->setShippingDescription($carrier)
            ->setShippingMethod($carrier)
            ->save();

        $quote->save();
    }
}

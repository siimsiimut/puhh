<?php

namespace Acty\Checkout\Support\Quote;

use Magento\Checkout\Model\Session as Checkout;
use Magento\Shipping\Model\CarrierFactory;
use Exception;

class GetQuoteData
{
    protected $checkout;

    protected $carrierFactory;

    public function __construct(Checkout $checkout, CarrierFactory $carrierFactory)
    {
        $this->checkout = $checkout;
        $this->carrierFactory = $carrierFactory;
    }

    public function get()
    {
        $quote = $this->checkout->getQuote();
        $address = $quote->getShippingAddress();
        $payment = $quote->getPayment();
        $shipping = $this->getShippingMethod($address->getShippingMethod());

        $data = [
            'address' => [
                'firstname' => $address->getFirstname(),
                'lastname' => $address->getLastname(),
                'email' => $address->getEmail(),
                'phone' => $address->getTelephone(),
                'country' => $address->getCountryId()
            ],

            'shipping' => [
                'carrier' => $shipping['carrier'],
                'method' => $shipping['method'],
                'address' => $this->getHasAddress($shipping['carrier'])
            ],

            'payment' => [
                'method' => $quote->getPayment()->getMethod()
            ],
            'quoteId' => $quote->getId()
        ];

        return $data;
    }

    protected function getHasAddress($method)
    {
        if (is_null($method)) {
            return false;
        }

        try {
            $method = $this->carrierFactory->get($method);
        } catch (Exception $e) {
            return false;
        }

        return $method->getHasAddress();
    }

    protected function getShippingMethod($code)
    {
        $split = explode('_', $code);

        if (count($split) != 2) {
            return $this->data(null, null);
        }

        return $this->data($split[0], $split[1]);
    }

    protected function data($carrier, $method)
    {
        return ['carrier' => $carrier, 'method' => $method];
    }
}

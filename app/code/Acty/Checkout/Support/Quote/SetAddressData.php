<?php

namespace Acty\Checkout\Support\Quote;

use Magento\Checkout\Model\Session as Checkout;
use Exception;

class SetAddressData
{
    protected $checkout;

    public function __construct(Checkout $checkout)
    {
        $this->checkout = $checkout;
    }

    public function set($data, $billing = false)
    {
        $quote = $this->checkout->getQuote();
        $address = $billing ? $quote->getBillingAddress() : $quote->getShippingAddress();

        foreach ($data as $field => $value) {
            $method = 'set'.$this->camel($field);

            try {
                $address->{$method}(trim($value));
                $address->save();
                $quote->save();
            } catch(Exception $e) {}
        }

        return $quote;
    }

    protected function camel($code)
    {
        $arr = explode('_', $code);

        foreach ($arr as &$str) {
            $str = ucfirst($str);
        }

        return implode('', $arr);
    }
}

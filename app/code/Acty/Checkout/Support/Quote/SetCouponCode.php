<?php

namespace Acty\Checkout\Support\Quote;

use Acty\Checkout\Support\Quote\GetTotals;
use Magento\Checkout\Model\Session;

class SetCouponCode
{
    protected $session;

    protected $totals;

    public function __construct(Session $session, GetTotals $totals)
    {
        $this->session = $session;
        $this->totals = $totals;
    }

    public function set($code)
    {
        $quote = $this->session->getQuote();

        $quote->setCouponCode($code)->collectTotals()->save();
        $this->totals->get();
        $this->session->resetCheckout();

        if ($quote->getCouponCode() === $code) {
            return $this->totals->get();
        }

        return false;
    }
}

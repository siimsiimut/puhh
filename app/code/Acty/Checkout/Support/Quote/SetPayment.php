<?php

namespace Acty\Checkout\Support\Quote;

use Magento\Checkout\Model\Session as Checkout;

class SetPayment
{
    protected $checkout;

    public function __construct(Checkout $checkout)
    {
        $this->checkout = $checkout;
    }

    public function set($payment, $name)
    {
        $quote = $this->checkout->getQuote();

        //check if modena and add modena_option_code
        //check their usage in controller Webexpert\Direct\Controller\Request\SaveOption
        if (strpos($payment,'ModenaDirect') !== false) {
            //modena payment method code set as
            //'ModenaDirect_'.$code,
            $code = explode('_', $payment)[1];

            $quote->getPayment()->unsAdditionalInformation();
            $quote->getPayment()->setAdditionalInformation('modena_option_code', $code);
            $quote->getPayment()->setAdditionalInformation('modena_option_label', $name);
        }

        //add additional information form holm method
        //need to do this because Acty_Checkout bypasses Payment method assignData
        //method, which means it does not trigger the observer that normally
        //assigns the method. See HolmBank/Payments/Observer/AssignHolmPartnerAdditionalPaymentData.php

        if (strpos($payment, 'holm_partner') !== false) {
            
            //hardcoded code for liisi järelmaks
            $code = "PRN_HP";
            $quote->getPayment()->unsAdditionalInformation();
            $quote->getPayment()->setAdditionalInformation('method_type', $code);
        }

        $quote->getPayment()->addData(['method' => $payment])->save();
        $quote->save();

        return $quote;
    }
}

<?php

namespace Acty\Checkout\Support\Quote;

use Acty\Framework\Support\EnvConfig;
use Magento\Catalog\Block\Product\ListProduct;
use Magento\Checkout\Model\Session as Checkout;
use Magento\Framework\View\Element\BlockFactory;

class GetCartItems
{
    protected $productIds = [];

    protected $checkout;

    protected $blockFactory;

    protected $env;

    public function __construct(Checkout $checkout, BlockFactory $blockFactory, EnvConfig $env)
    {
        $this->checkout = $checkout;
        $this->blockFactory = $blockFactory;
        $this->env = $env;
    }

    public function get()
    {
        $quote = $this->checkout->getQuote();
        $items = [];

        /**
         * @var \Magento\Quote\Model\Quote\Item\AbstractItem
         */
        foreach ($quote->getItemsCollection() as $item) {
            if (is_null($item->getParentItemId())) {
                $simple = $item->getOptionByCode('simple_product');
                $product = is_null($simple) ? $item->getProduct() : $simple->getProduct();
                $this->productIds[] = $item->getProductId();

                $stockItem = $product->getExtensionAttributes()->getStockItem();

                $allowBackorders = $stockItem->getUseConfigBackorders() == 0 && $stockItem->getBackorders() == 1;              

                $available = $allowBackorders
                    ? 1000
                    : $product->getExtensionAttributes()->getStockItem()->getQty();

                $longShippingTime = $allowBackorders ? $product->getResource()->getAttribute('tarneaeg')->getFrontend()->getValue($product) : false;

                $discount = $this->calculateDiscount($item->getBasePriceInclTax(), $product->getPriceInfo()->getPrice('regular_price')->getValue());

                $cartItem = [
                    'id' => $item->getId(),
                    'sku' => $item->getSku(),
                    'product_id' => $item->getProductId(),
                    'real_product_id' => $product->getId(),
                    'name' => $item->getName(),
                    'qty' => $item->getQty(),
                    'price_incl_tax' => $item->getPriceInclTax(),
                    'base_price_incl_tax' => $item->getBasePriceInclTax(),
                    'row_total_incl_tax' => $item->getRowTotalInclTax(),
                    'base_row_total_incl_tax' => $item->getBaseRowTotalInclTax(),
                    'product_regular_price' => $product->getPriceInfo()->getPrice('regular_price')->getValue(),
                    'product_catrule_discount' => $discount,
                    'thumbnail' => $this->getThumbnail($product),
                    'available' => $available,
                    'error' => $item->getHasError(),
                    'related' => $item->getRelatedProductIds(),
                    'url' => $item->getProduct()->getProductUrl(),
                    'longShipping' => $longShippingTime,
                    'realStock' => $product->getExtensionAttributes()->getStockItem()->getQty()
                ];

                if ($this->env->get('checkout.cart.options', false)) {
                    $cartItem['options'] = $this->getOptions($item);
                }

                $items[] = $cartItem;
            }
        }

        return $items;
    }
    protected function calculateDiscount ($priceInCart, $productRegularPrice)
    {
        $discount = 0;
        $discount = $productRegularPrice - $priceInCart;
        return $discount;
    }
    public function getProductIds()
    {
        return $this->productIds;
    }

    protected function getOptions($item)
    {
        $results = [];
        $itemOptions = $item->getBuyRequest()->getOptions();
        $productOptions = $item->getProduct()->getOptions();

        if (is_array($itemOptions) && is_array($productOptions)) {
            foreach ($productOptions as $options) {
                foreach ($options->getValues() as $option) {
                    $data = $option->getData();

                    if (isset($itemOptions[$data['option_id']]) && $itemOptions[$data['option_id']] == $data['option_type_id']) {
                        $results[] = [
                            'title' => empty($data['store_title']) ? $data['title'] : $data['store_title'],
                        ];
                    }
                }
            }
        }

        return $results;
    }

    protected function getThumbnail($product)
    {
        $block = $this->blockFactory->createBlock(ListProduct::class);

        return $block->getImage($product, 'product_page_image_small')->getImageUrl();
    }
}

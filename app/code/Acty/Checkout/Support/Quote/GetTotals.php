<?php

namespace Acty\Checkout\Support\Quote;

use Magento\Checkout\Model\Session as Checkout;

class GetTotals
{
    protected $checkout;
    /**
     * @var \Magento\SalesRule\Api\RuleRepositoryInterface
     */
    protected $ruleRepo;

    public function __construct(
        Checkout $checkout,
        \Magento\SalesRule\Api\RuleRepositoryInterface $ruleRepo    
    )
    {
        $this->checkout = $checkout;
        $this->ruleRepo = $ruleRepo;
    }

    public function get($quote = null)
    {
        $totals = [];
        $quote = $this->checkout->getQuote();

        $quote->getShippingAddress()->setCollectShippingRates(true)->save();
        $quote->save();

        $collectedTotals = $quote->collectTotals()->getTotals();

        foreach ($collectedTotals as $key => $model) {
            $totals[$key] = $model->getData();
        }

        $totals['shipping']['value'] = (float) $quote->getShippingAddress()->getTotals()['shipping']->getShippingInclTax();
        $totals['shipping']['title'] = $quote->getShippingAddress()->getShippingDescription();
        $totals['discount'] = $this->getDiscount($totals);
        $totals['coupon'] = $quote->getCouponCode();
        $totals['coupon_label'] = $this->getCouponLabel($quote);

        return $totals;
    }
    /**
     * @param \Magento\Quote\Model\Quote
     */
    protected function getCouponLabel($quote)
    {
        try {
            $ruleIds = explode(',',$quote->getAppliedRuleIds());
            if (!$ruleIds) {return '';}
            if (count($ruleIds) > 1) {
                return __('Multiple discounts');
            }
            $rule = $this->ruleRepo->getById($ruleIds[0]);
            $labels = $rule->getStoreLabels();
            if ($labels) {
                $storeId = $quote->getStoreId();
                foreach ($labels as $label) {
                    if ($label->getStoreId() == $storeId) {
                        return $label->getStoreLabel();
                    }
                }
            }
            return __('Coupon discount');
        }
        catch (\Exception $e) {}
    }
    protected function getDiscount($totals)
    {
        $value = $totals['subtotal']['value'] + $totals['shipping']['value'] - $totals['grand_total']['value'];
        $value = round((float) $value * 100) / 100;

        if ($value <= 0) {
            return 0;
        }

        return $value;
    }
}

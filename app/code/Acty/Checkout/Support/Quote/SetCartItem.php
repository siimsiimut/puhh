<?php

namespace Acty\Checkout\Support\Quote;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Cart;

class SetCartItem
{
    protected $cart;

    protected $product;

    public function __construct(Cart $cart, ProductRepositoryInterface $product)
    {
        $this->cart = $cart;
        $this->product = $product;
    }

    public function set($productId, $qty)
    {
        $product = $this->product->getById($productId);

        $this->cart->addProduct($product, [
            'product' => $product->getId(),
            'qty' => $qty,
            'price' => $product->getPriceInfo()->getPrice('final_price')->getValue(),
            'options' => []
        ]);

        $this->cart->save();

        return $product;
    }
}

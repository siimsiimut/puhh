<?php

namespace Acty\Checkout\Support\Quote;

use Magento\Checkout\Model\Session as Checkout;
use Magento\Customer\Api\Data\GroupInterface;
use Magento\Customer\Model\Session as Customer;
use Magento\Framework\Session\SessionManagerInterface;

class SetQuoteData
{
    protected $address = [
        'shipping' => [],
        'billing' => []
    ];

    protected $checkout;

    protected $customer;

    protected $session;

    public function __construct(Checkout $checkout, Customer $customer, SessionManagerInterface $session)
    {
        $this->checkout = $checkout;
        $this->customer = $customer;
        $this->session = $session;
    }

    public function setShippingAddress(
        $firstname,
        $lastname,
        $email,
        $telephone,
        $country,
        $street = null,
        $city = null,
        $postcode = null
    ) {
        $this->address['shipping'] = [
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email' => $email,
            'telephone' => $telephone,
            'country' => $country,
            'street' => is_null($street) ? 'n/a' : $street,
            'city' => is_null($city) ? 'n/a' : $city,
            'postcode' => is_null($postcode) ? 'n/a' : $postcode
        ];

        return $this;
    }

    public function setBillingAddress($company, $code, $street, $city, $postcode)
    {
        $this->address['billing'] = [
            'company' => $company,
            'code' => $code,
            'street' => $street,
            'city' => $city,
            'postcode' => $postcode
        ];

        return $this;
    }

    public function set($carrier, $payment, $comment)
    {
        $quote = $this->checkout->getQuote();
        $shipping = $quote->getShippingAddress();
        $billing = $quote->getBillingAddress();
        $payment = $this->getPayment($payment);

        $shipping
            ->setFirstname($this->address['shipping']['firstname'])
            ->setLastname($this->address['shipping']['lastname'])
            ->setEmail($this->address['shipping']['email'])
            ->setTelephone($this->address['shipping']['telephone'])
            ->setCountryId($this->address['shipping']['country'])
            ->setCity($this->address['shipping']['city'])
            ->setStreet($this->address['shipping']['street'])
            ->setPostcode($this->address['shipping']['postcode'])
            ->setShippingDescription($carrier)
            ->setShippingMethod($carrier)
            ->setCollectShippingRates(true)
            ->collectShippingRates()
            ->save();

        $billing
            ->setFirstname($this->address['shipping']['firstname'])
            ->setLastname($this->address['shipping']['lastname'])
            ->setEmail($this->address['shipping']['email'])
            ->setTelephone($this->address['shipping']['telephone'])
            ->setCountryId($this->address['shipping']['country'])
            ->setCity($this->address['shipping']['city'])
            ->setStreet($this->address['shipping']['street'])
            ->setPostcode($this->address['shipping']['postcode'])
            ->save();

        if ( ! empty($this->address['billing'])) {
            $billing
                ->setCompany($this->address['billing']['company'])
                ->setRegistrationCode($this->address['billing']['code'])
                ->setStreet($this->address['billing']['street'])
                ->setCity($this->address['billing']['city'])
                ->setPostcode($this->address['billing']['postcode'])
                ->save();
        }

        $quote->setCustomerComment($comment);
        $quote->getPayment()->addData(['method' => $payment])->save();
        $quote->setCustomerEmail($this->address['shipping']['email']);
        $quote->save();

        return $this->finalizeQuote();
    }

    protected function getPayment($payment)
    {
        if (preg_match('/^makecommerce_(.+)$/', $payment, $matches)) {
            $payment = 'makecommerce';

            $this->session->setMakecommerceMethod($matches[1]);
        }

        return $payment;
    }

    protected function finalizeQuote()
    {
        $quote = $this->checkout->getQuote();

        if ( ! $this->customer->isLoggedIn()) {
            $quote->setCustomerId(null);
            $quote->setCustomerIsGuest(true);
            $quote->setCustomerGroupId(GroupInterface::NOT_LOGGED_IN_ID);
        } else {
            $quote->setCustomerId($this->customer->getCustomer()->getId());
        }

        $quote->getShippingAddress()->setCollectShippingRates(true)->save();
        $quote->setInventoryProcessed(false);
        $quote->collectTotals();
        $quote->save();

        return $quote;
    }
}

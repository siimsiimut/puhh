<?php

namespace Acty\Checkout\Support\Payment;

use Acty\Checkout\Support\CreateOrder;
use Acty\Checkout\Support\Payment\GetPaymentGateway;

class GetGatewayResult
{
    protected $createOrder;

    protected $getPaymentGateway;

    protected $checkout;

    public function __construct(CreateOrder $createOrder, GetPaymentGateway $getPaymentGateway)
    {
        $this->createOrder = $createOrder;
        $this->getPaymentGateway = $getPaymentGateway;
    }

    public function get()
    {
        $gateway = $this->getPaymentGateway->get();

        if ($this->getPaymentGateway->isOrderNeeded()) {
            $order = $this->createOrder->create();
        }

        if (isset($gateway['redirect']) and $gateway['redirect'] == '') {
            $gateway['redirect'] = $this->getPaymentGateway->getUrl();
        }

        if (isset($gateway['post']) && isset($order)) {
            $gateway['fields'] = $this->getPaymentGateway->fields($order->getId());
        }

        return $gateway;
    }
}

<?php

namespace Acty\Checkout\Support\Payment;

use Magento\Checkout\Model\Session as Checkout;
use Magento\Framework\ObjectManagerInterface;

class GetPaymentGateway
{
    protected $objectManager;

    protected $checkout;

    public function __construct(Checkout $checkout, ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
        $this->checkout = $checkout;
    }

    public function get($code = null)
    {
        $code = $this->getGatewayCode($code);

        return $this->objectManager->create($this->getClass($code))->get();
    }

    public function fields($orderId)
    {
        $code = $this->getGatewayCode(null);

        return $this->objectManager->create($this->getClass($code))->fields($orderId);
    }

    public function isOrderNeeded()
    {
        $code = $this->getGatewayCode(null);

        return $this->objectManager->create($this->getClass($code))->isOrderNeeded();
    }

    public function getUrl()
    {
        $code = $this->getGatewayCode(null);

        return $this->objectManager->create($this->getClass($code))->getUrl();
    }

    public function getGatewayCode($code)
    {
        if ( ! is_null($code)) {
            return $code;
        }

        return $this->checkout->getQuote()->getPayment()->getMethod();
    }

    public function getClass($code)
    {
        if (strpos($code, 'ModenaDirect') !== false) {
            return 'Acty\\Checkout\\PaymentGateways\\ModenaDirect';
        }
        else {
            return 'Acty\\Checkout\\PaymentGateways\\'.$this->camel($code);
        }
        
    }

    public function camel($code)
    {
        $arr = explode('_', $code);

        foreach ($arr as &$str) {
            $str = ucfirst($str);
        }

        return implode('', $arr);
    }
}

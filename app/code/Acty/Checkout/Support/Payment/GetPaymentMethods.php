<?php

namespace Acty\Checkout\Support\Payment;

use Acty\Checkout\Services\CacheManager;
use Acty\Checkout\Support\Utils\GetUrl;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Payment\Model\Config;
use Magento\Store\Model\ScopeInterface;
use Makecommerce\Ecommerce\Model\MakecommerceConfigProvider;
use Magento\Customer\Model\Session;

class GetPaymentMethods
{
    protected $scope;

    protected $config;

    protected $url;

    protected $store;

    protected $cacheManager;

    protected $objectManager;

    /**
     * @var \Webexpert\Direct\Model\ConfigProvider
     */
    private $modenaDirectConfigProvider;

    public function __construct(
        ScopeConfigInterface $scope,
        Config $config,
        GetUrl $url,
        CacheManager $cacheManager,
        ObjectManagerInterface $objectManager,
        \Webexpert\Direct\Model\ConfigProvider $modenaDirectConfigProvider
    ) {
        $this->scope = $scope;
        $this->config = $config;
        $this->url = $url;
        $this->cacheManager = $cacheManager;
        $this->objectManager = $objectManager;
        $this->modenaDirectConfigProvider = $modenaDirectConfigProvider;
    }

    public function get()
    {
        return $this->getList();

        // maksemeetodid on page cachetud anyway...

        // return $this->cacheManager->remember('paymentmethods', function() {
        //     return $this->getList();
        // });
    }

    protected function getList()
    {
        $methods = [];

        foreach ($this->config->getActiveMethods() as $code => $model) {
            if ( ! strstr('makecommerce', $code) && ! strstr('paypal_billing_agreement', $code) && ! strstr('free', $code) && ! strstr('direct', $code)) {
                $methods[] = [
                    'label' => trim($this->getScope('payment/'.$code.'/title')),
                    'code' => $code,
                    'image' => $this->url->view('Acty_Checkout::img/'.$code.'.png')
                ];
            }
        }

        $methods = array_merge($methods, $this->getMakecommerce());
        $methods = array_merge($this->getModenaDirect(), $methods);
        
        // push modena buyplan and liisi järelmaks to the end
        $reordered_methods = array();
        foreach ($methods as $item) {
            if ($item["code"] != "buyplan" && $item["code"] != "holm_partner") {
                $reordered_methods[] = $item;
            }
        }
        foreach ($methods as $item) {
            if ($item["code"] == "buyplan" || $item["code"] == "holm_partner") {
                $reordered_methods[] = $item;
            }
        }
        
        $methods = $reordered_methods;        

        $bankTransferGroups = $this->getBankTransferGroups();

        if (empty($bankTransferGroups)) {
            return $methods;
        }

        $groupId = $this->objectManager->create(Session::class)->getCustomer()->getGroupId();

        foreach ($methods as $key => $method) {
            if ($method['code'] === 'banktransfer' && ! isset($bankTransferGroups[$groupId])) {
                unset($methods[$key]);
            }
        }

        return $methods;
    }

    public function getBankTransferGroups()
    {
        $item = trim($this->getScope('actycheckout/general/bank_transfer_groups'));

        if (empty($item)) {
            return [];
        }

        $groups = preg_split("/\r\n|\n|\r/", $item);

        return array_flip($groups);
    }
    protected function getModenaDirect()
    {
        $methods = [];
        if (array_key_exists('direct', $this->config->getActiveMethods())){
            $config = $this->modenaDirectConfigProvider->getconfig();
        
            foreach ($config['payment']['direct']['icons'] as $b) {
                $methods[] = [
                    'code' => 'ModenaDirect_'.$b->code,
                    'label' => $b->name,
                    'image' => $b->buttonUrl
                ];
            }
        }
        return $methods;
    }
    protected function getMakecommerce()
    {
        if ( ! class_exists(MakecommerceConfigProvider::class)) {
            return [];
        }

        if (!array_key_exists('makecommerce', $this->config->getActiveMethods())){
            return [];
        }
        $config = $this->objectManager->create(MakecommerceConfigProvider::class)->getConfig();

        if ( ! $config['payment']['makecommerce']['isAvailable']) {
            return [];
        }

        $methods = [];
        $country = strtolower($this->getScope('general/country/default'));

        foreach ($config['payment']['makecommerce']['paymentChannels']['banklinks'] as $item) {
            if ($item['country'] == $country) {
                $methods[] = [
                    'label' => $item['name'],
                    'code' => 'makecommerce_'.$item['name'],
                    'image' => $this->url->view('Acty_Checkout::img/'.$item['name'].'.png')
                ];
            }
        }

        foreach ($config['payment']['makecommerce']['paymentChannels']['cards'] as $item) {
            $methods[] = [
                'label' => $item['name'],
                'code' => 'makecommerce_'.$item['name'],
                'pcard' => true,
                'image' => $this->url->view('Acty_Checkout::img/'.$item['name'].'.png')
            ];
        }

        return $methods;
    }

    protected function getScope($option)
    {
        return $this->scope->getValue($option, ScopeInterface::SCOPE_STORE);
    }
}

<?php

namespace Acty\Checkout\Support\Utils;

use Acty\Checkout\Services\CacheManager;
use Magento\Catalog\Block\Product\ImageBuilder;
use Magento\Catalog\Model\Product;

class GetProductImageUrl
{
    protected $cache;

    protected $image;

    public function __construct(CacheManager $cache, ImageBuilder $image)
    {
        $this->cache = $cache;
        $this->image = $image;
    }

    public function get(Product $product, $imageId)
    {
        $key = 'image.'.$product->getId().'.'.$imageId;

        return $this->cache->remember($key, function() use ($product, $imageId) {
            return $this->image->setProduct($product)
                ->setImageId($imageId)
                ->setAttributes([])
                ->create()->getImageUrl();
        }, ['product.images'], 3600 * 24 * 7);
    }
}

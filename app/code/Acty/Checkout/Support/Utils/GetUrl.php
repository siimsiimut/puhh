<?php

namespace Acty\Checkout\Support\Utils;

use Magento\Framework\View\Asset\Repository;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\StoreManagerInterface;

class GetUrl
{
    protected $request;

    protected $asset;

    protected $store;

    public function __construct(RequestInterface $request, Repository $asset, StoreManagerInterface $store)
    {
        $this->request = $request;
        $this->asset = $asset;
        $this->store = $store;
    }

    public function view($id, $params = [])
    {
        $params = array_merge(['_secure' => $this->request->isSecure()], $params);

        return $this->asset->getUrlWithParams($id, $params);
    }

    public function base($url = null)
    {
        $base = $this->store->getStore()->getUrl();

        if (is_null($url)) {
            return $base;
        }

        return $base.$url;
    }
}

<?php

namespace Acty\Checkout\Support\Utils;

use Magento\Newsletter\Model\SubscriberFactory;

class Subscribe
{
    protected $subscriber;

    public function __construct(SubscriberFactory $subscriber)
    {
        $this->subscriber = $subscriber;
    }

    public function handle($email)
    {
        $this->subscriber->create()->subscribe($email);
    }
}

<?php

namespace Acty\Checkout\Support\Data;

use Acty\Checkout\Support\Data\GetBillingAddress;
use Acty\Checkout\Support\Data\GetCrosssellProducts;
use Acty\Checkout\Support\Data\GetCustomer;
use Acty\Checkout\Support\Data\GetShippingAddress;
use Acty\Checkout\Support\Quote\GetCartItems;
use Acty\Checkout\Support\Quote\GetQuoteData;
use Acty\Checkout\Support\Quote\GetTotals;
use Acty\Framework\Support\EnvConfig;

class GetInitialData
{
    protected $getCustomer;

    protected $getCartItems;

    protected $getTotals;

    protected $getQuoteData;

    protected $getCrosssellProducts;

    protected $getBillingAddress;

    protected $env;

    protected $quoteData = null;

    public function __construct(
        GetCustomer $getCustomer,
        GetCartItems $getCartItems,
        GetTotals $getTotals,
        GetQuoteData $getQuoteData,
        GetCrosssellProducts $getCrosssellProducts,
        GetBillingAddress $getBillingAddress,
        GetShippingAddress $getShippingAddress,
        EnvConfig $env
    ) {
        $this->getCustomer = $getCustomer;
        $this->getCartItems = $getCartItems;
        $this->getTotals = $getTotals;
        $this->getQuoteData = $getQuoteData;
        $this->getCrosssellProducts = $getCrosssellProducts;
        $this->getBillingAddress = $getBillingAddress;
        $this->getShippingAddress = $getShippingAddress;
        $this->env = $env;
    }

    public function get()
    {
        $data = [
            'customer' => $this->getCustomer(),
            'shipping' => $this->getQuoteData('shipping'),
            'payment' => $this->getQuoteData('payment'),
            'items' => $this->getCartItems->get(),
            'totals' => $this->getTotals->get(),
            'billing' => $this->getBillingAddress->get(),
            'address' => $this->getShippingAddress->get(),
            'quoteId' => $this->getQuoteData('quoteId')
        ];

        if ($this->env->get('checkout.crosssell.enabled', false)) {
            $data['crosssell'] = $this->getCrosssellProducts->get($this->getCartItems->getProductIds());
        }

        return $data;
    }

    protected function getCustomer()
    {
        $customer = $this->getCustomer->get();
        $address = $this->getQuoteData('address');

        foreach (array_keys($customer) as $key) {
            if ( ! empty($address[$key])) {
                $customer[$key] = $address[$key];
            }
        }

        return $customer;
    }

    protected function getQuoteData($section)
    {
        if (is_null($this->quoteData)) {
            $this->quoteData = $this->getQuoteData->get();
        }

        return $this->quoteData[$section];
    }
}

<?php

namespace Acty\Checkout\Support\Data;

use Magento\Checkout\Model\Session;

class GetBillingAddress
{
    protected $checkout;

    public function __construct(Session $checkout)
    {
        $this->checkout = $checkout;
    }

    public function get()
    {
        $address = $this->checkout->getQuote()->getBillingAddress();

        return [
            'company' => $address->getCompany(),
            'code' => $address->getRegistrationCode(),
            'street' => $this->getStreet($address),
            'city' => $address->getCity(),
            'postcode' => $address->getPostcode()
        ];
    }

    protected function getStreet($address)
    {
        $street = $address->getStreet();

        if (is_array($street)) {
            $street = array_shift($street);
        }

        return $street;
    }
}

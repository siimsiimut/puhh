<?php

namespace Acty\Checkout\Support\Data;

use Acty\Checkout\Repositories\CustomerRepository;
use Magento\Customer\Model\Session as Customer;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Store\Model\ScopeInterface;

class GetCustomer
{
    protected $customer;

    protected $scopeConfig;

    protected $orderFactory;

    protected $customerRepository;

    public function __construct(Customer $customer, ScopeConfigInterface $scopeConfig, OrderFactory $orderFactory, CustomerRepository $customerRepository)
    {
        $this->customer = $customer;
        $this->scopeConfig = $scopeConfig;
        $this->orderFactory = $orderFactory;
        $this->customerRepository = $customerRepository;
    }

    public function get()
    {
        $data = [
            'isLoggedIn' => $this->customer->isLoggedIn(),
            'firstname' => '',
            'lastname' => '',
            'email' => '',
            'phone' => '',
            'country' => $this->getDefaultCountry(),
            'comment' => ''
        ];

        if ($data['isLoggedIn']) {
            $customer = $this->customer->getCustomer();

            $data['firstname'] = $customer->getFirstname();
            $data['lastname'] = $customer->getLastname();
            $data['email'] = $customer->getEmail();
            $data['phone'] = $this->getLastTelephone($customer);
        }

        if (empty($data['phone'])) {
            $data['phone'] = $this->getPhoneCountryCode();
        }

        return $data;
    }

    protected function getPhoneCountryCode()
    {
        return $this->scopeConfig->getValue('actycheckout/general/phone_start', ScopeInterface::SCOPE_STORE);
    }

    protected function getLastTelephone($customer)
    {
        $orderId = $this->customerRepository->getLastOrderId($customer->getId());

        if ( ! $orderId) {
            return '';
        }

        $order = $this->orderFactory->create()->load($orderId);

        return $order->getShippingAddress()->getTelephone();
    }

    protected function getDefaultCountry()
    {
        return $this->scopeConfig->getValue('general/country/default', ScopeInterface::SCOPE_STORE);
    }
}

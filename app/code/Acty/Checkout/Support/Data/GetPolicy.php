<?php

namespace Acty\Checkout\Support\Data;

use Acty\Framework\Support\EnvConfig;
use Magento\Cms\Model\PageFactory;
use Magento\Store\Model\StoreManagerInterface;

class GetPolicy
{
    protected $page;
    protected $_storeManager;

    protected $config;

    public function __construct(PageFactory $page, StoreManagerInterface $storeManager, EnvConfig $config)
    {
        $this->_storeManager = $storeManager;
        $this->page = $page;
        $this->config = $config;
    }

    public function getStoreCode()
    {
        return $this->_storeManager->getStore()->getCode();
    }

    public function get()
    {
        if ( ! $this->config->get('checkout.privacy.enabled')) {
            return '';
        }

        $storeCode = $this->getStoreCode();

        $policyPageId = null;
        if ($storeCode === 'toysoutlet_ru') {
            $policyPageId = 'privaatsuspoliitika-76';
        } elseif($storeCode === 'toysoutlet_lv') {
            $policyPageId = 'privatuma-politika';
        } elseif($storeCode === 'toysoutlet_en') {
            $policyPageId = 'privacy-policy';
        } elseif($storeCode === 'et') {
            $policyPageId = 'privaatsuspoliitika-76';
        } elseif($storeCode === 'ru') {
            $policyPageId = 'politika-konfidencial-nosti';
        } elseif($storeCode === 'en') {
            $policyPageId = 'privacy-policy';
        } else {
            $policyPageId = 'privaatsuspoliitika-76';
        }

        return $this->page->create()->setStoreId($this->_storeManager->getStore()->getId())->load($policyPageId, 'identifier')->getContent();
    }
}

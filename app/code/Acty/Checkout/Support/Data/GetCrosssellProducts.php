<?php

namespace Acty\Checkout\Support\Data;

use Acty\Checkout\Support\Utils\GetProductImageUrl;
use Acty\Framework\Support\EnvConfig;
use Magento\Catalog\Model\Product\LinkFactory;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Checkout\Model\Session as Checkout;
use Magento\Quote\Model\Quote\Item\RelatedProducts;
use Magento\Store\Model\StoreManagerInterface;

class GetCrosssellProducts
{
    protected $link;

    protected $store;

    protected $visibility;

    protected $related;

    protected $checkout;

    protected $image;

    protected $env;

    public function __construct(
        LinkFactory $link,
        StoreManagerInterface $store,
        Visibility $visibility,
        RelatedProducts $related,
        Checkout $checkout,
        GetProductImageUrl $image,
        EnvConfig $env
    ) {
        $this->link = $link;
        $this->store = $store;
        $this->visibility = $visibility;
        $this->related = $related;
        $this->checkout = $checkout;
        $this->image = $image;
        $this->env = $env;
    }

    public function get($productIds, $limit = 3)
    {
        $limit = $this->env->get('checkout.crosssell.limit', $limit);
        $data = [];
        $collection = [];
        $collections[0] = $this->getCartCollection($productIds, $limit);
        $count = $collections[0]->count();

        if ($count == 0) {
            $collections[0] = $this->getGlobalCollection($productIds, $productIds, $limit);
        } elseif ($count < $limit) {
            foreach ($collections[0] as $product) {
                $excluded[] = $product->getId();
            }

            $excluded = array_merge($productIds, $excluded);
            $collections[1] = $this->getGlobalCollection($productIds, $excluded, $limit - $count);
        }

        foreach ($collections as $cols) {
            foreach ($cols as $product) {
                $product = $product->load($product->getId());

                $data[] = $this->getData($product);
            }
        }

        return $data;
    }

    protected function getData($product)
    {
        return [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'type' => $product->getTypeId(),
            'base_price' => (float) $product->getPrice(),
            'price' => $product->getPriceInfo()->getPrice('final_price')->getValue(),
            'image' => $this->image->get($product, 'product_base_image'),
            'url' => $product->getProductUrl(),
            'saleable' => $product->isSaleable(),
            'options' => ! empty($product->getOptions())
        ];
    }

    protected function getCartCollection($productIds, $limit)
    {
        $collection = $this->getCollection($productIds, $limit)
            ->addProductFilter($productIds)
            ->addExcludeProductFilter($productIds)
            ->load();

        return $collection;
    }

    public function getGlobalCollection($productIds, $excluded, $limit)
    {
        //"global collection" now limited to "new" category id = 1940)
        //removed excluded products filter, does not work together with categoriesfilter
        $cat = [1940];
        $collection = $this->getCollection($productIds, $limit)
            ->addCategoriesFilter(['in' => $cat])
            ->load();

        return $collection;
    }

    protected function getCollection($productIds, $limit)
    {
        $collection = $this->link->create()->useCrossSellLinks()->getProductCollection();


        $collection->getSelect()->orderRand('e.entity_id');
        $collection->getSelect()->group('e.entity_id');

        $collection
            ->setStoreId($this->store->getStore()->getId())
            ->addStoreFilter()->setPageSize($limit)
            ->setVisibility($this->visibility->getVisibleInCatalogIds());

        return $collection;
    }
}

<?php

namespace Acty\Checkout\Support\Data;

use Magento\CheckoutAgreements\Model\CheckoutAgreementsRepository;

class GetAgreements
{
    protected $agreements;

    public function __construct(CheckoutAgreementsRepository $agreements)
    {
        $this->agreements = $agreements;
    }

    public function get()
    {
        $agreements = [];

        foreach ($this->agreements->getList() as $agreement) {
            if ($agreement->getIsActive()) {
                $agreements[] = $agreement->getData();
            }
        }

        return $agreements;
    }
}

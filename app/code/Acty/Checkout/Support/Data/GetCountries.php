<?php

namespace Acty\Checkout\Support\Data;

use Acty\Checkout\Services\CacheManager;
use Magento\Directory\Model\ResourceModel\Country\CollectionFactory;

class GetCountries
{
    protected $collection;

    protected $cache;

    public function __construct(CollectionFactory $collection, CacheManager $cache)
    {
        $this->collection = $collection;
        $this->cache = $cache;
    }

    public function get()
    {
        return $this->cache->remember('countries', function() {
            return $this->getList();
        });
    }

    protected function getList()
    {
        $countries = [];

        foreach ($this->collection->create()->loadByStore() as $country) {
            $countries[] = [
                'label' => $country->getName(),
                'value' => $country->getCountryId()
            ];
        }

        usort($countries, function($a, $b) {
            return $a['label'] <=> $b['label'];
        });

        return $countries;
    }
}

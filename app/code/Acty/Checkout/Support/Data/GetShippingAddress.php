<?php

namespace Acty\Checkout\Support\Data;

use Magento\Checkout\Model\Session;
use Magento\Customer\Model\Session as Customer;
use Acty\Checkout\Repositories\CustomerRepository;
use Magento\Sales\Model\OrderFactory;

class GetShippingAddress
{
    protected $checkout;

    protected $customer;

    protected $customerRepository;

    protected $orderFactory;

    public function __construct(Session $checkout, Customer $customer, CustomerRepository $customerRepository, OrderFactory $orderFactory)
    {
        $this->checkout = $checkout;
        $this->customer = $customer;
        $this->customerRepository = $customerRepository;
        $this->orderFactory = $orderFactory;
    }

    public function get()
    {
        $address = $this->getQuote();

        if (count($address) === 3) {
            return $address;
        }

        $address = array_merge($address, $this->getLastOrder($address));

        return $this->clean($address);
    }

    protected function clean($address)
    {
        $data = [];

        foreach ($address as $key => $value) {
            if ($value != 'n/a') {
                $data[$key] = $value;
            }
        }

        return $data;
    }

    protected function getQuote()
    {
        $data = [];
        $address = $this->checkout->getQuote()->getShippingAddress();
        $street = $address->getStreet();

        if ($street) {
            $street = is_array($street) ? join(' ', $street) : $street;

            $data['street'] = trim($street);
        }

        if ($address->getCity()) {
            $data['city'] = $address->getCity();
        }

        if ($address->getPostcode()) {
            $data['postcode'] = $address->getPostcode();
        }

        return $data;
    }

    protected function getLastOrder($data)
    {
        $orderId = $this->customerRepository->getLastOrderId($this->customer->getCustomer()->getId());

        if ( ! $orderId) {
            return [];
        }

        $address = $this->orderFactory->create()->load($orderId)->getShippingAddress();

        if ( ! isset($data['street']) && $address->getStreet()) {
            $data['street'] = $address->getStreet();
        }

        if ( ! isset($data['city']) && $address->getCity()) {
            $data['city'] = $address->getCity();
        }

        if ( ! isset($data['postcode']) && $address->getPostcode()) {
            $data['postcode'] = $address->getPostcode();
        }

        return $data;
    }
}

<?php

namespace Acty\Checkout\Support;

use Magento\Framework\Event\ManagerInterface as Event;
use Magento\Checkout\Model\Session as Checkout;
use Magento\Quote\Model\QuoteManagement;

class CreateOrder
{
    protected $event;

    protected $checkout;

    protected $quoteManagement;

    public function __construct(Event $event, Checkout $checkout, QuoteManagement $quoteManagement)
    {
        $this->event = $event;
        $this->checkout = $checkout;
        $this->quoteManagement = $quoteManagement;
    }

    public function create()
    {
        $quote = $this->checkout->getQuote();

        $this->event->dispatch('checkout_submit_before', ['quote' => $quote]);

        $order = $this->quoteManagement->submit($quote);

        $order->getBillingAddress()->setRegistrationCode(
            $quote->getBillingAddress()->getRegistrationCode()
        )->save();

        $comment = $quote->getCustomerComment();

        if ( ! empty($comment)) {
            $comment = __('Customer comment').': '.$comment;

            $order->addStatusHistoryComment($comment)->setIsCustomerNotified(null)->save();
        }

        $this->checkout->setLastQuoteId($quote->getId());
        $this->checkout->setLastSuccessQuoteId($quote->getId());
        $this->checkout->setLastOrderId($order->getId());
        $this->checkout->setLastRealOrderId($order->getIncrementId());
        $this->checkout->setLastOrderStatus($order->getStatus());

        $this->event->dispatch('checkout_submit_all_after', ['order' => $order, 'quote' => $quote]);

        return $order;
    }
}

<?php

return [
    'firstname_required' => 'Firstname is required',
    'lastname_required' => 'Lastname is required',
    'phone_required' => 'Telephone is required',
    'phone_invalid' => 'Telephone format invalid (+3725555555)',
    'email_required' => 'Email is required',
    'email_invalid' => 'Email format invalid',
    'email_taken' => 'This email is taken',
    'carrier_required' => 'Carrier is required',
    'carrier_method_required' => 'Carrier method is required',
    'payment_method_required' => 'Payment method is required',
    'validation_error' => 'Please resolve validation errors',
    'company_required' => 'Compane name is required',
    'code_required' => 'Registration code is required',
    'street_required' => 'Street is required',
    'city_required' => 'City is required',
    'postcode_required' => 'Postcode is required',
    'postcode_invalid' => 'Postcode format invalid',
    'latvian_phone_invalid' => 'Telephone format invalid (+3715555555)'
];

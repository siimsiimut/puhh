module.exports = (value)=> {
    return parseFloat(value).toFixed(2)
}

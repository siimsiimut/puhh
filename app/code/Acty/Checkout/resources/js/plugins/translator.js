const translations = window.actyCheckout.translations
const path = require('object-path-get')

module.exports = {
    install: function(Vue) {
        Vue.prototype.$t = (key)=> {
            return path(translations, key, key)
        }
    }
}

const translator = require('../plugins/translator')

module.exports = {
    template: '#acty-checkout-billing',

    data() {
        return {
            enabled: window.actyCheckout.options.hasBilling,
            address: this.$root.$data.billing,

            errors: {
                company: false,
                code: false,
                street: false,
                city: false,
                postcode: false
            },

            translator: translator
        }
    },

    methods: {
        setAddressData(field, value) {
            const data = {
                billing: true,
                fields: {}
            }

            data.fields[field] = value

            window.axios.post('/purchase/set/address', data).then((res)=> {})
        }
    },

    mounted() {
        window.emitter.$on('acty.checkout.billing.errors', (errors)=> {
            this.errors = {
                company: false,
                code: false,
                street: false,
                city: false,
                postcode: false
            }

            for (let field in errors) {
                this.errors[field] = 'validation.' + errors[field][0]
            }
        })
    }
}

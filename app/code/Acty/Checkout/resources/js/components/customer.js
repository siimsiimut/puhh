const translator = require('../plugins/translator')

module.exports = {
    template: '#acty-checkout-customer',

    data() {
        return {
            customer: this.$root.$data.customer,
            countries: window.actyCheckout.countries,
            options: window.actyCheckout.options,

            errors: {
                firstname: false,
                lastname: false,
                email: false,
                telephone: false
            },

            translator: translator
        }
    },

    methods: {
        setAddressData(field, value) {
            const data = {
                billing: false,
                fields: {}
            }

            data.fields[field] = value

            window.axios.post('/purchase/set/address', data).then((res)=> {})
        }
    },

    watch: {
        'customer.country': function (value) {
            window.emitter.$emit('acty.checkout.country', value)

            this.setAddressData('country_id', value)
        },

        'customer.email': function(value) {
            this.customer.email = value.trim()
        }
    },

    mounted() {
        window.emitter.$on('acty.checkout.customer.errors', (errors)=> {
            this.errors = {
                firstname: false,
                lastname: false,
                email: false,
                telephone: false
            }

            for (let field in errors) {
                this.errors[field] = 'validation.' + errors[field][0]
            }
        })
    }
}

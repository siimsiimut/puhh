module.exports = {
    template: '#acty-checkout-shipping',

    data() {
        return {
            shipping: this.$root.$data.shipping,
            address: this.$root.$data.address,
            carriers: [],
            country: null,

            errors: {
                carrier: false,
                method: false,
                street: false,
                city: false,
                postcode: false
            },

            init: false
        }
    },

    methods: {
        getCarriers(country) {
            axios.get(window.actyCheckout.locale+'/purchase/get/carriers', { params: { country } }).then((res)=> {
                this.carriers = res.data
                this.init = true

                if (this.country === null) {
                    this.country = country
                }
            })
        },

        setCarrier() {
            let carrier = ''

            if (this.carriers[this.shipping.carrier] !== undefined) {
                this.shipping.address = this.carriers[this.shipping.carrier].address
            }

            if (this.shipping.carrier !== null && this.shipping.method !== null) {
                carrier = this.shipping.carrier + '_' + this.shipping.method
            }

            window.axios.post(window.actyCheckout.locale+'/purchase/set/carrier', { carrier }).then((res)=> {
                window.emitter.$emit('acty.checkout.totals', res.data)
            })
        },

        setAddressData(field, value) {
            const data = {
                billing: false,
                fields: {}
            }

            data.fields[field] = value

            window.axios.post('/purchase/set/address', data).then((res)=> {})
        }
    },

    watch: {
        'shipping.carrier': function(value) {
            if (this.init === true) {
                this.shipping.method = null
            }

            if (this.carriers.length !== 0) {
                const methods = this.carriers[value].methods

                if (methods.length === 1) {
                    this.shipping.method = methods[0].points[0].code
                }
            }
        },

        'shipping.method': function() {
            if (this.init === true) {
                this.setCarrier()
            }
        }
    },

    mounted() {
        window.emitter.$on('acty.checkout.country', (country)=> {
            if (this.init === true) {
                this.shipping.carrier = null
                this.shipping.method = null
            }

            this.getCarriers(country)
        })

        window.emitter.$on('acty.checkout.totals', ()=> {
            if (this.country !== null) {
                this.getCarriers(this.country)
            }
        })

        window.emitter.$on('acty.checkout.shipping.errors', (errors)=> {
            this.errors.carrier = false
            this.errors.method = false

            for (let field in errors) {
                this.errors[field] = 'validation.' + errors[field][0]
            }
        })

        window.emitter.$on('acty.checkout.address.errors', (errors)=> {
            this.errors.street = false
            this.errors.city = false
            this.errors.postcode = false

            for (let field in errors) {
                this.errors[field] = 'validation.' + errors[field][0]
            }
        })
    }
}

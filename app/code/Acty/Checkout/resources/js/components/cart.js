module.exports = {
    template: '#acty-checkout-cart',

    data() {
        return {
            items: [],
            options: window.actyCheckout.options,
            totals: {
                subtotal: {},
                shipping: {},
                tax: {},
                grand_total: {},
                category_rule_discount: {},
                regular_cart_price: {}
            },
            warnings: {
                multipleItems: false
            },
        }
    },

    methods: {
        updateMagentoCart() {
            window.require([
                'Magento_Customer/js/customer-data'
            ], function (data) {
                const sections = ['cart']

                data.invalidate(sections)
                data.reload(sections, true)
            })
        },

        message(text, type = 'success') {
            window.require([
                'Magento_Customer/js/customer-data'
            ], function (data) {
                data.set('messages', {
                    messages: [{ text, type }]
                })
            })
        },

        removeItem(id) {
            window.axios.post('/purchase/delete/item', { id }).then((res)=> {
                if (res.data.error === undefined) {
                    const items = []

                    for (const i in this.items) {
                        if (id !== this.items[i].id) {
                            items.push(this.items[i])
                        }
                    }

                    this.items = items
                    this.$root.$data.items = this.items.length

                    window.emitter.$emit('acty.checkout.totals', res.data.totals)
                    this.updateMagentoCart()
                    this.message(res.data.message)
                }
            })
        },

        updateQty(id, qty, i) {
            window.axios.post('/purchase/set/qty', { id, qty }).then((res)=> {
                if (res.data.error === undefined) {
                    this.items[i].base_row_total_incl_tax = this.items[i].base_price_incl_tax * qty

                    for (const i in this.items) {
                        if (this.items[i].qty <= this.items[i].available) {
                            this.items[i].error = false
                        }
                    }

                    window.emitter.$emit('acty.checkout.totals', res.data.totals)
                    this.updateMagentoCart()
                    this.message(res.data.message)
                }
            })
        },
        hasTax() {
            const tax = parseFloat(this.totals.tax.value)

            return tax > 0 ? true : false
        },

        hasDiscount(item) {
            return item.product_regular_price - item.base_price_incl_tax > 0;
        },
        maxAllowedChange(item) {
            if (item.qty > 10) {
                return item.qty
            }

            return (item.available < 10 && item.available > 0) ? item.available : 10
        },
        setCategoryDiscount() {
            let discount = 0;
            for (const i in this.items) {
                discount += this.items[i].product_catrule_discount * this.items[i].qty;
            }
            this.totals.category_rule_discount = discount;
        },
        setRegularCartPrice() {
            let regularCartPrice = 0;
            for (const i in this.items) {
                regularCartPrice += this.items[i].product_regular_price * this.items[i].qty;
            }
            this.totals.regular_cart_price = regularCartPrice;
        },
        setWarnings(len = this.$root.$data.items) {
            if (len > 1) {
                this.warnings.multipleItems = true;
            } else {
                this.warnings.multipleItems = false;
            }
        },
        getLongShippingQty(item) {
            if (item.realStock <= 0) {
                return item.qty;
            }
            if (item.qty - item.realStock <= 0) {
                return item.realStock - item.qty;
            }
            return item.qty - item.realStock;
        }
    },

    mounted() {
        window.emitter.$on('acty.checkout.items', (items)=> {
            this.items = items
            this.setWarnings(items.length);
        })

        window.emitter.$on('acty.checkout.totals', (totals)=> {
            for (const name in totals) {
                this.totals[name] = totals[name]
            }
            this.setCategoryDiscount();
            this.setRegularCartPrice();
            this.setWarnings();
        })
    }
}

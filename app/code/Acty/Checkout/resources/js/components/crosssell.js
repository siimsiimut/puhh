module.exports = {
    template: '#acty-checkout-crosssell',

    data() {
        return {
            items: [],
            enabled: window.actyCheckout.options.hasCrosssell
        }
    },

    methods: {
        updateMagentoCart() {
            window.require([
                'Magento_Customer/js/customer-data'
            ], function (data) {
                const sections = ['cart']

                data.invalidate(sections)
                data.reload(sections, true)
            })
        },

        message(text, type = 'success') {
            window.require([
                'Magento_Customer/js/customer-data'
            ], function (data) {
                data.set('messages', {
                    messages: [{ text, type }]
                })
            })
        },

        add(id) {
            window.axios.post('/purchase/set/item', { id, qty: 1}).then((res)=> {
                if (res.data.success === true) {
                    const message = res.data.message

                    this.updateMagentoCart()

                    window.axios.get('/purchase/get/items').then((res)=> {
                        window.emitter.$emit('acty.checkout.items', res.data)
                        this.message(message)
                    })
                    window.axios.get('/purchase/get/totals').then((res)=> {
                        window.emitter.$emit('acty.checkout.totals', res.data)
                    })
                }
            })
        },

        saleable(item) {
            return item.type === 'simple' && item.saleable === true && item.options === false
        }
    },

    mounted() {
        window.emitter.$on('acty.checkout.crosssell', (items)=> {
            this.items = items
        })
    }
}

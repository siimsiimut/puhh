module.exports = {
    template: '#acty-checkout-payment',

    data() {
        return {
            payment: this.$root.$data.payment,
            paymentMethods: window.actyCheckout.paymentMethods,
            shipping: this.$root.$data.shipping,

            errors: {
                payment: false
            }
        }
    },

    methods: {
        getByCodes(...codes) {
            const list = []

            for (const i in codes) {
                const code = codes[i]

                for (const j in this.paymentMethods) {
                    const method = this.paymentMethods[j]

                    if (method.code === code) {
                        list.push(method)
                    }
                }
            }

            return list
        }
    },

    watch: {
        'payment.method': function() {
            window.axios.post(window.actyCheckout.locale + '/purchase/set/payment', { payment: this.payment.method, name: this.getByCodes(this.payment.method)[0]['label'] }).then((res)=> {})
        },

        'shipping.method': function() {
            if (this.payment.method == 'omniva_courier_payment' && this.shipping.method != 'omnivacourier') {
                this.payment.method = null
            }
        }
    },

    mounted() {
        window.emitter.$on('acty.checkout.payment.errors', (errors)=> {
            this.errors = {
                method: false
            }

            for (let field in errors) {
                this.errors[field] = 'validation.' + errors[field][0]
            }
        })
    }
}

const constraints = require('../validation/constraints')

module.exports = {
    template: '#acty-checkout-submit',

    data() {
        return {
            options: window.actyCheckout.options,
            policyContent: window.actyCheckout.policy,
            customer: this.$root.$data.customer,
            address: this.$root.$data.address,
            billing: this.$root.$data.billing,
            shipping: this.$root.$data.shipping,
            payment: this.$root.$data.payment,
            quote: {},
            quoteId: null,
            orderValue: null,
            shippingValue: null,
            couponLabel: null,
            error: false,
            accepted: null,
            agreements: false,
            subscribed: false,
            policy: false,
            submitting: false,

            errors: {
                submit: false
            },

            post: {
                url: null,
                fields: []
            }
        }
    },

    methods: {
        toggleAgreements() {
            this.agreements = !this.agreements
        },

        togglePolicy() {
            this.policy = !this.policy
        },

        validate(data, constraints) {
            const validation = window.validate(data, constraints)
            const errors = {}

            if (validation !== undefined) {
                for (let field in validation) {
                    errors[field] = validation[field]
                }
            }

            return errors
        },

        hasErrors() {
            if (this.customer.country === 'LV') {
                constraints.address.postcode.format.pattern = '^[Ll][Vv][- ]{0,1}\\d{4}$'
                constraints.customer.phone.format.pattern = '^\\+371[0-9]+'
                constraints.customer.phone.format.message = '^latvian_phone_invalid'
            }

            const validators = {
                customer: this.validate(this.customer, constraints.customer),
                shipping: this.validate(this.shipping, constraints.shipping),
                payment: this.validate(this.payment, constraints.payment)
            }

            if (this.billing.enabled === true) {
                validators.billing = this.validate(this.billing, constraints.billing)
            }

            if (this.shipping.address === true) {
                validators.address = this.validate(this.address, constraints.address)
            }

            this.error = false

            for (let name in validators) {
                if (Object.keys(validators[name]).length !== 0) {
                    this.error = true
                }

                window.emitter.$emit('acty.checkout.' + name + '.errors', validators[name])
            }

            if (this.accepted !== true) {
                this.error = true
                this.accepted = false
            }

            return this.error
        },

        submit() {
            this.customer.phone = this.customer.phone.trim()
            if (!this.hasErrors()) {
                this.submitGA()
                this.finalize()
            }
        },

        makecommerce(data) {
            const script = document.createElement('script')

            window.maksekeskusCallback = (data) => {}

            script.onload = () => {
                window.Maksekeskus.Checkout.initialize({
                    'key': data.key,
                    'transaction': data.id,
                    'amount': parseFloat(data.amount).toFixed(2),
                    'currency': data.currency,
                    'email': this.customer.email,
                    'clientName': this.customer.firstname + ' ' + this.customer.lastname,
                    'locale': 'et',
                    'name': data.name,
                    'description': data.reference + ' ' + data.order_ids,
                    'completed': 'maksekeskusCallback',
                    'cancelled': 'maksekeskusCallback',
                    'noConflict': false,
                    'openOnLoad': true
                })
            }

            script.async = true

            script.setAttribute('src', data.src)
            document.head.appendChild(script)
        },

        submitGA() {
            //we only deal with gtag stuff on toysoutlet.lv site
            if (window.checkout.websiteId !== '2') {
                return true;
            }
            window.dataLayer = window.dataLayer || [];
            function gtag() { dataLayer.push(arguments); }
            dataLayer.push({ ecommerce: null });  // Clear the previous ecommerce object.
            gtag("event", "purchase",
                {
                    transaction_id: 'LV_' + this.quoteId,
                    value: this.orderValue,
                    shipping: this.shippingValue,
                    currency: "EUR",
                    coupon: this.couponLabel,
                    items: this.getOrderItems()
                }
            );
        },
        
        getOrderItems() {
            window.require([
                'Magento_Customer/js/customer-data'
            ], function (data) {
                var cart = data.get('cart')();
                var gaItemsData = [];
                cart.items.forEach(item => {
                    gaItemsData.push(
                        {
                            item_id: item.product_sku,
                            item_name: item.product_name,
                            price: item.product_price_value,
                            quantity: item.qty
                        }
                    )
                });
                return gaItemsData;
            })
        },

        finalize() {
            const data = {
                customer: this.customer,
                shipping: this.shipping,
                payment: this.payment,
                billing: this.billing,
                subscribed: this.subscribed
            }

            if (this.shipping.address === true) {
                data.address = this.address
            }

            this.submitting = true
            this.post.url = null

            //if shipping to finland set country FI
            if (this.shipping.carrier.indexOf('smartpostfinland') == 0) {
                data.customer.country = 'FI'
            }

            window.axios.post(window.actyCheckout.locale + '/purchase/finalize', data).then((res) => {
                const data = res.data

                if (data.redirect !== undefined) {
                    if (data.redirect.id === undefined) {
                        return window.location.href = data.redirect
                    }

                    this.makecommerce(data.redirect)
                }

                if (data.post !== undefined) {
                    this.post.url = data.post
                    this.post.fields = data.fields

                    this.$nextTick(() => {
                        this.$refs.postFieldsForm.submit()
                    })
                }

                this.submitting = false
            }).catch((res) => {
                const data = res.response.data

                if (data.email === true) {
                    this.error = true

                    window.emitter.$emit('acty.checkout.customer.errors', {
                        email: ['email_taken']
                    })
                }

                if (data.system !== undefined) {
                    this.error = data.system
                }

                this.submitting = false
            })
        }
    },

    mounted() {
        window.emitter.$on('acty.checkout.quote', (quote) => {
            this.quote = quote
        })
        window.emitter.$on('acty.checkout.quoteId', (quoteId) => {
            this.quoteId = quoteId
        })
        window.emitter.$on('acty.checkout.totals', (totals) => {
            this.orderValue = totals['subtotal']['value'];
            this.shippingValue = totals['shipping']['value'];
            this.couponLabel = totals['coupon_label'];
        })
    }
}

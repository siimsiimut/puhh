const translator = require('../plugins/translator')

module.exports = {
    template: '#acty-checkout-coupon',

    data() {
        return {
            code: null,
            suggestion: '',
            error: false,
            enabled: window.actyCheckout.options.hasCoupon
        }
    },

    methods: {
        add(suggestion) {
            if (suggestion.length < 1) {
                return
            }

            this.error = false

            window.axios.post('/purchase/set/coupon', { code: suggestion }).then((res)=> {
                if (res.data.error !== undefined) {
                    return this.error = res.data.error
                }

                this.code = this.suggestion
                this.suggestion = ''

                this.message(res.data.message)

                window.axios.get('/purchase/get/totals').then((res)=> {
                    window.emitter.$emit('acty.checkout.totals', res.data)
                })
            })
        },

        remove() {
            window.axios.post('/purchase/delete/coupon').then((res)=> {
                if (res.data.error !== undefined) {
                    return this.error = res.data.error
                }

                this.code = null
                this.suggestion = ''

                this.message(res.data.message)

                window.axios.get('/purchase/get/totals').then((res)=> {
                    window.emitter.$emit('acty.checkout.totals', res.data)
                })
            })
        },

        message(text, type = 'success') {
            window.require([
                'Magento_Customer/js/customer-data'
            ], function (data) {
                data.set('messages', {
                    messages: [{ text, type }]
                })
            })
        },
    },

    mounted() {
        window.emitter.$on('acty.checkout.totals', (totals)=> {
            if (totals.coupon !== null) {
                this.code = totals.coupon
            }
        })
    }
}

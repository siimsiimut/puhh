module.exports = {
    customer: {
        firstname: {
            length: {
                minimum: 1,
                message: '^firstname_required'
            },
        },

        lastname: {
            length: {
                minimum: 1,
                message: '^lastname_required'
            }
        },

        email: {
            length: {
                minimum: 1,
                message: '^email_required'
            },

            email: {
                message: '^email_invalid'
            }
        },

        phone: {
            length: {
                minimum: 9,
                message: '^phone_required'
            },

            format: {
                pattern: '^\\+[0-9]+',
                message: '^phone_invalid'
            }
        }
    },

    address: {
        street: {
            length: {
                minimum: 1,
                message: '^street_required'
            },

            presence: {
                allowEmpty: false,
                message: '^street_required'
            }
        },

        city: {
            length: {
                minimum: 1,
                message: '^city_required'
            },

            presence: {
                allowEmpty: false,
                message: '^city_required'
            }
        },

        postcode: {
            length: {
                minimum: 1,
                message: '^postcode_required'
            },

            presence: {
                allowEmpty: false,
                message: '^postcode_required'
            },

            format: {
                pattern: '.+',
                message: '^postcode_invalid'
            }
        }
    },

    billing: {
        company: {
            length: {
                minimum: 1,
                message: '^company_required'
            },

            presence: {
                allowEmpty: false,
                message: '^company_required'
            }
        },

        code: {
            length: {
                minimum: 1,
                message: '^code_required'
            },

            presence: {
                allowEmpty: false,
                message: '^code_required'
            }
        },

        street: {
            length: {
                minimum: 1,
                message: '^street_required'
            },

            presence: {
                allowEmpty: false,
                message: '^street_required'
            }
        },

        city: {
            length: {
                minimum: 1,
                message: '^city_required'
            },

            presence: {
                allowEmpty: false,
                message: '^city_required'
            }
        },

        postcode: {
            length: {
                minimum: 1,
                message: '^postcode_required'
            },

            presence: {
                allowEmpty: false,
                message: '^postcode_required'
            }
        }
    },

    shipping: {
        carrier: {
            presence: {
                allowEmpty: false,
                message: '^carrier_required'
            }
        },

        method: {
            presence: {
                allowEmpty: false,
                message: '^carrier_method_required'
            }
        }
    },

    payment: {
        method: {
            presence: {
                allowEmpty: false,
                message: '^payment_method_required'
            }
        }
    }
}

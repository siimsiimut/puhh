const deps = window.actyCheckout.isProduction ? ['acty-vue-min', 'acty-axios-min'] : ['acty-vue', 'acty-axios']

requirejs(deps, ()=> {
    window.validate = require('validate.js')
    window.emitter = new window.Vue({})

    window.Vue.use(require('./plugins/translator'))

    window.Vue.filter('price', require('./filters/price'))

    new window.Vue({
        el: '#acty-checkout',

        components: {
            'acty-checkout-shipping': require('./components/shipping'),
            'acty-checkout-billing': require('./components/billing'),
            'acty-checkout-crosssell': require('./components/crosssell'),
            'acty-checkout-customer': require('./components/customer'),
            'acty-checkout-payment': require('./components/payment'),
            'acty-checkout-submit': require('./components/submit'),
            'acty-checkout-coupon': require('./components/coupon'),
            'acty-checkout-cart': require('./components/cart'),
        },

        data: {
            shared: {
                customer: true,
                address: true,
                shipping: true,
                payment: true,
                billing: true
            },

            items: null,

            customer: {
                isLoggedIn: false,
                firstname: '',
                lastname: '',
                email: '',
                country: '',
                comment: ''
            },

            address: {
                street: '',
                city: '',
                postcode: '',
                phone: ''
            },

            billing: {
                enabled: false,
                company: '',
                code: '',
                street: '',
                city: '',
                postcode: ''
            },

            shipping: {
                carrier: null,
                method: null,
                address: false
            },

            payment: {
                method: null
            }
        },

        methods: {
            init(data) {
                for (const section in data) {
                    if (this.shared[section] === true) {
                        for (const field in data[section]) {
                            this[section][field] = data[section][field]
                        }
                    }
                }

                window.emitter.$emit('acty.checkout.items', data.items)
                window.emitter.$emit('acty.checkout.totals', data.totals)
                window.emitter.$emit('acty.checkout.quote', data.quote)
                window.emitter.$emit('acty.checkout.quoteId', data.quoteId)
                window.emitter.$emit('acty.checkout.crosssell', data.crosssell)
                this.initGACheckout(data);
            },
            initGACheckout(data) {
                //we only deal with gtag stuff on toysoutlet.lv site
                if (window.checkout.websiteId !== '2') {
                    return true;
                }
                window.dataLayer = window.dataLayer || [];
                function gtag() { dataLayer.push(arguments); }
                dataLayer.push({ ecommerce: null });  // Clear the previous ecommerce object.
                gtag("event", "begin_checkout",
                    {
                        value: data.totals.grand_total.value,
                        currency: "EUR",
                        items: this.buildGAItems(data.items)
                    }
                );
            },
            buildGAItems(items) {
                var gaItemsData = [];
                items.forEach(item => {
                    gaItemsData.push(
                        {
                            item_id: item.id,
                            item_name: item.name,
                            price: item.price_incl_tax,
                            quantity: item.qty
                        }
                    )
                });
                return gaItemsData;
            }
        },

        mounted() {
            window.axios.get('/purchase/get/initial').then((res)=> {
                this.items = res.data.items.length

                this.$nextTick(()=> {
                    this.init(res.data)
                })
            })
        }
    })
})

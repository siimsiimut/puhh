<?php

namespace Acty\Checkout\Services;

use Acty\Framework\Extensions\CacheType;

class CacheManager extends CacheType
{
    protected static $type = 'acty_checkout';

    protected static $tag = 'ACTY_CHECKOUT';
}

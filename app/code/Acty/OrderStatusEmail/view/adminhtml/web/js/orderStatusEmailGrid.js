define(['mageUtils', 'jquery'], function (utils, $) {
    'use strict';

    function orderStatusEmailGrid(config) {
    }

    orderStatusEmailGrid.prototype.bulkActionCallback = function (action, data) {
        var selections = {};

        selections['selected'] = data['selected'];

        if (!selections['selected'].length) {
            selections['selected'] = false;
        }

        utils.submit({
            url: action.url,
            data: selections
        });
    };

    orderStatusEmailGrid.prototype.initContainer = function () {
    };

    return orderStatusEmailGrid;
});

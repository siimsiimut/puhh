<?php
namespace Acty\OrderStatusEmail\Model;

/**
 * Class Processor
 * @package Acty\OrderStatusEmail\Model
 */
class Processor
{
    const POSTITATUD_STATUS = 'postitatud';
    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Acty\OrderStatusEmail\Helper\Module
     */
    protected $configHelper;

    /**
     * @var \Magento\Sales\Model\Order\Config
     */
    protected $orderStatuses;

    /**
     * @var \Magento\Sales\Model\Order\Email\Sender\OrderCommentSender
     */
    protected $orderCommentSender;

    /**
     * Processor constructor.
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Acty\OrderStatusEmail\Helper\Module $configHelper
     * @param \Magento\Sales\Model\Order\Config $orderStatuses
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Sales\Model\Order\Email\Sender\OrderCommentSender $orderCommentSender
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Acty\OrderStatusEmail\Helper\Config $configHelper,
        \Magento\Sales\Model\Order\Config $orderStatuses,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Sales\Model\Order\Email\Sender\OrderCommentSender $orderCommentSender
    ) {
        $this->orderFactory = $orderFactory;
        $this->request = $request;
        $this->registry = $registry;
        $this->scopeConfig = $config;
        $this->messageManager = $messageManager;
        $this->configHelper = $configHelper;
        $this->orderStatuses = $orderStatuses;
        $this->orderCommentSender = $orderCommentSender;
    }

    /**
     * @return bool
     */
    public function processOrders($orderIds)
    {
        if (!$this->configHelper->isModuleEnabled()) {
            return false;
        }
        @set_time_limit(0);

        $newOrderStatus = $this->request->getParam('new_order_status');
        if (!is_array($orderIds) || empty($newOrderStatus)) {
            $this->messageManager->addErrorMessage(__('Please select order(s) as well as a new order status.'));
            return false;
        }

        $modifiedCount = 0;
        foreach ($orderIds as $orderId) {
            try {
                $isModified = false;

                /** @var \Magento\Sales\Model\Order $order */
                $order = $this->orderFactory->create()->load($orderId);

                if (!$order || !$order->getId()) {
                    $this->messageManager->addErrorMessage(
                        __(
                            'Could not process order with entity_id %1. Order has been deleted in the meantime?',
                            $orderId
                        )
                    );
                    continue;
                }

                $oldStatus = $order->getStatus();
                if (!empty($newOrderStatus)) {
                    $this->setOrderState($order, $newOrderStatus);
                    $order->setStatus($newOrderStatus)->save();
                    $isModified = true;
                }
                if ($oldStatus !== $order->getStatus()) {
                    $order->setIsStatusMassChange(true);
                    $order->addStatusHistoryComment('Status masschange', $order->getStatus())->setIsCustomerNotified(0);
                    $order->save();
                }

                if ($isModified) {
                    $modifiedCount++;
                }
            } catch (\Exception $e) {
                if (isset($order) && $order && $order->getIncrementId()) {
                    $orderId = $order->getIncrementId();
                }
                $this->messageManager->addErrorMessage('Exception (Order # ' . $orderId . '): ' . $e->getMessage());
            }
        }

        $this->messageManager->addSuccessMessage(__('Total of %1 order(s) were modified.', $modifiedCount));

        return true;
    }

    /**
     * @param \Magento\Sales\Api\OrderInterface[]
     * @return array
     */
    public function setOrderStatusPostitatud($orders)
    {
        $result = [];
        $newOrderStatus = $this::POSTITATUD_STATUS;
        foreach ($orders as $order) {
            $oldStatus = $order->getStatus();
            $this->setOrderState($order, $newOrderStatus);
            $order->setStatus($newOrderStatus)->save();
            if ($oldStatus !== $order->getStatus()) {
                $order->setIsStatusMassChange(true);
                $order->addStatusHistoryComment('Shipped from CF&S', $order->getStatus())->setIsCustomerNotified(0);
                $order->save();
            }
            $result[] = 'MEPS'.$order->getIncrementId();
        }
        return $result;
    }

    /**
     * @param $orders
     * @param $newOrderStatus
     * @return bool
     */
    protected function setOrderState($order, $newOrderStatus)
    {
        /*if ($this->scopeConfig->isSetFlag('OrderStatusEmail/general/force_status_change')) {
            return;
        }*/
        $orderStates = $this->orderStatuses->getStates();
        foreach ($orderStates as $state => $label) {
            $stateStatuses = $this->orderStatuses->getStateStatuses($state, false);
            foreach ($stateStatuses as $status) {
                if ($status == $newOrderStatus) {
                    $order->setData('state', $state);
                    return true;
                }
            }
        }
        return false;
    }
}

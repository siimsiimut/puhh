<?php
namespace Acty\OrderStatusEmail\Model;

class StatusEmail extends \Magento\Framework\Model\AbstractModel
{
    /**
     * @var Resource\Notification\Collection
     */
    protected $notificationCollection;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ResourceModel\Notification\Collection $notificationCollection
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Acty\OrderStatusEmail\Model\ResourceModel\StatusEmail\Collection $notificationCollection,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->notificationCollection = $notificationCollection;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }


    protected function _construct()
    {
        $this->_init('Acty\OrderStatusEmail\Model\ResourceModel\StatusEmail');
    }

    /**
     * @param $statusCode
     * @return array
     */
    public function getStatusEmails($statusCode)
    {
        $statusEmails = [];
        $collection = $this->notificationCollection->addFieldToFilter('status_code', $statusCode);
        foreach ($collection as $statusEmail) {
            $statusEmails[$statusEmail->getStoreId()] = $statusEmail->getTemplateId();
        }
        return $statusEmails;
    }
}

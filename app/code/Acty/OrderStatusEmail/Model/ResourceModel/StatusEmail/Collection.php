<?php
namespace Acty\OrderStatusEmail\Model\ResourceModel\StatusEmail;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource table
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            'Acty\OrderStatusEmail\Model\StatusEmail',
            'Acty\OrderStatusEmail\Model\ResourceModel\StatusEmail'
        );
    }
}

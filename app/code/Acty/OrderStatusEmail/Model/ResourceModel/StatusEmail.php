<?php
namespace Acty\OrderStatusEmail\Model\ResourceModel;

class StatusEmail extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('sales_order_status_email', 'id');
    }

    /**
     * @param $statusCode
     */
    public function removeByStatusCode($statusCode)
    {
        $adapter = $this->getConnection();
        $adapter->delete($this->getMainTable(), ['status_code = ?' => $statusCode]);
    }
}

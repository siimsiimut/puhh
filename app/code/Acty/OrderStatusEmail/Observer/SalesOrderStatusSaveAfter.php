<?php
namespace Acty\OrderStatusEmail\Observer;

class SalesOrderStatusSaveAfter implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Acty\OrderStatusEmail\Helper\Module
     */
    protected $configHelper;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var Resource\Notification
     */
    protected $statusEmailResource;

    /**
     * @var statusEmailFactory
     */
    protected $statusEmailFactory;

    /**
     * @param \Acty\OrderStatusEmail\Helper\Module $configHelper
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Acty\OrderStatusEmail\Model\ResourceModel\Notification $statusEmailResource
     * @param \Acty\OrderStatusEmail\Model\statusEmailFactory $statusEmailFactory
     */
    public function __construct(
        \Acty\OrderStatusEmail\Helper\Config $configHelper,
        \Magento\Framework\App\RequestInterface $request,
        \Acty\OrderStatusEmail\Model\ResourceModel\StatusEmail $statusEmailResource,
        \Acty\OrderStatusEmail\Model\StatusEmailFactory $statusEmailFactory
    ) {
        $this->configHelper = $configHelper;
        $this->request = $request;
        $this->statusEmailResource = $statusEmailResource;
        $this->statusEmailFactory = $statusEmailFactory;
    }

    /**
     * After order status configuration has been saved, update the DB
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->configHelper->isModuleEnabled()) {
            return;
        }
        $statusCode = $this->request->getParam('status', false);

        if (empty($statusCode)) {
            return;
        }

        $storeEmails = $this->request->getPost('store_email_templates', false);
        $this->statusEmailResource->removeByStatusCode($statusCode);
        if (!empty($storeEmails)) {
            foreach ($storeEmails as $storeId => $templateId) {
                $data = [
                    'store_id' => $storeId,
                    'status_code' => $statusCode,
                    'template_id' => $templateId
                ];
                $this->statusEmailFactory->create()->addData($data)->save();
            }
        }
    }
}

<?php
namespace Acty\OrderStatusEmail\Plugin\Ui\Component;

use Magento\Ui\Component\MassAction;

class MassActionPlugin
{
    /**
     * @var \Acty\OrderStatusEmail\Helper\Module
     */
    protected $configHelper;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Framework\AuthorizationInterface
     */
    protected $authorization;

    /**
     * Adminhtml data
     *
     * @var \Magento\Backend\Helper\Data
     */
    protected $adminhtmlData = null;

    /**
     * @var \Acty\OrderStatusEmail\Model\System\Config\Source\Order\AllStatuses
     */
    protected $orderStatusSource;

    /**
     * @param \Acty\OrderStatusEmail\Helper\Config $configHelper
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\AuthorizationInterface $authorization
     * @param \Magento\Backend\Helper\Data $adminhtmlData
     * @param \Acty\OrderStatusEmail\Model\System\Config\Source\Order\AllStatuses $orderStatusSource
     */
    public function __construct(
        \Acty\OrderStatusEmail\Helper\Config $configHelper,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\AuthorizationInterface $authorization,
        \Magento\Backend\Helper\Data $adminhtmlData,
        \Acty\OrderStatusEmail\Model\System\Config\Source\Order\AllStatuses $orderStatusSource
    ) {
        $this->configHelper = $configHelper;
        $this->request = $request;
        $this->scopeConfig = $config;
        $this->registry = $registry;
        $this->authorization = $authorization;
        $this->adminhtmlData = $adminhtmlData;
        $this->orderStatusSource = $orderStatusSource;
    }

    /**
     *
     *
     * @param MassAction $subject
     * @param string $interceptedOutput
     * @return void
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterPrepare(MassAction $subject, $interceptedOutput)
    {
        if ($subject->getContext()->getNamespace() !== 'sales_order_grid') {
            return;
        }
        if (!$this->configHelper->isModuleEnabled()) {
            return;
        }


        $orderStatuses = $this->getFilteredOrderStatuses();
        $config = $subject->getData('config');

        if (!isset($config['component']) || strstr($config['component'], 'tree') === false) {
            // Temporary until added to core to support multi-level selects
            $config['component'] = 'Magento_Ui/js/grid/tree-massactions';
        }

        $subActions = [];
        foreach ($orderStatuses as $orderStatus) {
            $subActions[] = [
                'type' => $orderStatus['value'],
                'label' => __('%1', $orderStatus['label']),
                'url' => $this->adminhtmlData->getUrl(
                    'orderstatusemail/grid/mass',
                    [
                        'new_order_status' => $orderStatus['value'],
                        'namespace' => $subject->getContext()->getNamespace()
                    ]
                ),
                'callback' => [
                    'provider' => 'sales_order_grid.sales_order_grid.orderStatusEmailGrid',
                    'target' => 'bulkActionCallback'
                ]
            ];
        }
        $config['actions'][] = [
            'type' => 'change_orderstatus',
            'label' => __('Change order status'),
            'actions' => $subActions
        ];


        $subject->setData('config', $config);
    }


    public function getFilteredOrderStatuses()
    {
        $orderStatuses = $this->orderStatusSource->toOptionArray();


        array_shift($orderStatuses);

        $visibleOrderStatuses = $this->scopeConfig->getValue('sales_email/order_status_emails/visible_order_statuses');
        $visibleOrderStatuses = explode(",",$visibleOrderStatuses);



        if ($visibleOrderStatuses[0] !== '') {
            foreach ($orderStatuses as $key => $orderStatus) {
                if (!in_array($orderStatus['value'], $visibleOrderStatuses)) {
                    unset($orderStatuses[$key]);
                }
            }
        }

        return $orderStatuses;
    }
}

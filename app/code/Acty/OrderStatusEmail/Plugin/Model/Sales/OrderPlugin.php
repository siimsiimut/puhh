<?php
namespace Acty\OrderStatusEmail\Plugin\Model\Sales;

use Magento\Sales\Model\Order;
use Acty\OrderStatusEmail\Helper\Config;

class OrderPlugin
{
    protected $moduleHelper;
    protected $request;
    protected $registry;
    protected $statusEmailCollectionFactory;
    protected $orderCommentSender;

    public function __construct(
        Config $configHelper,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Model\Order\Email\Sender\OrderCommentSender $orderCommentSender,
        \Acty\OrderStatusEmail\Model\ResourceModel\StatusEmail\CollectionFactory $statusEmailCollectionFactory
    ) {
        $this->configHelper = $configHelper;
        $this->request = $request;
        $this->registry = $registry;
        $this->orderCommentSender = $orderCommentSender;
        $this->statusEmailCollectionFactory = $statusEmailCollectionFactory;
    }

    public function aroundAddStatusHistoryComment(Order $subject, \Closure $proceed, $comment, $status = false)
    {
        if (!$this->configHelper->isModuleEnabled()) {
            return $proceed($comment, $status);
        }

        $statusEmails = $this->getStatusEmails($subject->getStore()->getId(), $status ? $status : $subject->getStatus());
        if ($statusEmails->count() > 0) {
            if ($this->request->getActionName() === 'addComment' or $subject->getIsStatusMassChange()) {
                $this->registry->register(Config::STATUS_EMAILS_COLLECTION, $statusEmails, true);

                if ($this->request->getActionName() !== 'addComment') {
                    $this->orderCommentSender->send($subject);
                }
                $this->registry->register(Config::STATUS_EMAILS_SENDED, $this->getIsNotified(), true);
            }


        }
        return $proceed($comment, $status);
    }

    public function getIsNotified()
    {
        $isNotified = true;
        $postData = $this->request->getPost('history');
        if (!empty($postData)) {
            $isNotified = isset($postData['is_customer_notified']) ? $postData['is_customer_notified'] : false;
        }
        return $isNotified;
    }

    public function getStatusEmails($storeId, $status)
    {
        $statusEmails = $this->statusEmailCollectionFactory->create();
        $statusEmails->addFieldToFilter('template_id', ['neq' => -1])
            ->addFieldToFilter('store_id', $storeId)
            ->addFieldToFilter('status_code', $status);
        return $statusEmails;
    }
}

<?php
namespace Acty\OrderStatusEmail\Plugin\Model\Sales\Order\Status;

use Magento\Sales\Model\Order\Status\History;
use \Acty\OrderStatusEmail\Helper\Config;

class HistoryPlugin
{
    protected $registry;

    public function __construct(
        \Magento\Framework\Registry $registry
    ) {
        $this->registry = $registry;
    }

    public function aroundSetIsCustomerNotified(History $subject, \Closure $proceed, $flag = null)
    {
        return $proceed($this->isNotified() ? true : $flag);
    }

    protected function isNotified()
    {
        return $this->registry->registry(Config::STATUS_EMAILS_COLLECTION) !== null
            && $this->registry->registry(Config::STATUS_EMAILS_SENDED);
    }
}

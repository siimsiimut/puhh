<?php
namespace Acty\OrderStatusEmail\Plugin\Model\Sales\Order\Email\Container;

use Magento\Sales\Model\Order\Email\Container\OrderCommentIdentity;
use Acty\OrderStatusEmail\Helper\Config;

class OrderCommentIdentityPlugin
{
    protected $registry;

    public function __construct(
        \Magento\Framework\Registry $registry
    ) {
        $this->registry = $registry;
    }

    public function aroundGetGuestTemplateId(OrderCommentIdentity $subject, \Closure $proceed)
    {
        return $this->getCustomTemplateId($subject, $proceed);
    }

    public function aroundGetTemplateId(OrderCommentIdentity $subject, \Closure $proceed)
    {
        return $this->getCustomTemplateId($subject, $proceed);
    }

    protected function getCustomTemplateId(OrderCommentIdentity $subject, \Closure $proceed)
    {
        $collection = $this->registry->registry(Config::STATUS_EMAILS_COLLECTION);
        if ($collection && $collection->getItemByColumnValue('store_id', $subject->getStore()->getStoreId())) {
            $templateId = $collection->getItemByColumnValue('store_id', $subject->getStore()->getStoreId())->getTemplateId();
            if ($templateId == 0) {
                $templateId = Config::DEFAULT_EMAIL_TEMPLATE;
            }
            return $templateId;
        }
        return $proceed();
    }
}

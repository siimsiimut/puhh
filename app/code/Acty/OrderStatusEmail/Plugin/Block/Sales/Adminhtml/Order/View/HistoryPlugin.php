<?php
namespace Acty\OrderStatusEmail\Plugin\Block\Sales\Adminhtml\Order\View;

use Magento\Sales\Block\Adminhtml\Order\View\History;

class HistoryPlugin
{
    protected $moduleHelper;

    public function __construct(
        \Acty\OrderStatusEmail\Helper\Config $configHelper
    ) {
        $this->configHelper = $configHelper;
    }

    public function afterGetStatuses(History $subject, $interceptedOutput)
    {
        if (!$this->configHelper->isModuleEnabled() or !$this->configHelper->showAllStatuses()) {
            return $interceptedOutput;
        }

        $interceptedOutput = $subject->getOrder()->getConfig()->getStatuses();
        return $interceptedOutput;
    }
}

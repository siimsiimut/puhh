<?php
namespace Acty\OrderStatusEmail\Plugin\Block\Sales\Adminhtml\Order\Status\Edit;

use Magento\Sales\Block\Adminhtml\Order\Status\Edit\Form;

class FormPlugin extends \Acty\OrderStatusEmail\Plugin\Block\Sales\Adminhtml\Order\Status\AbstractForm
{
    /**
     * @var \Acty\OrderStatusEmail\Helper\Module
     */
    protected $configHelper;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Email\Model\ResourceModel\Template\CollectionFactory
     */
    protected $templatesFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Acty\OrderStatusEmail\Model\Notification
     */
    protected $notificationModel;

    /**
     * @param \Acty\OrderStatusEmail\Helper\Module $configHelper
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Email\Model\ResourceModel\Template\CollectionFactory $templatesFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Acty\OrderStatusEmail\Model\Notification $notificationModel
     */
    public function __construct(
        \Acty\OrderStatusEmail\Helper\Config $configHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Email\Model\ResourceModel\Template\CollectionFactory $templatesFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Acty\OrderStatusEmail\Model\StatusEmail $notificationModel
    ) {
        $this->registry = $registry;
        $this->notificationModel = $notificationModel;
        parent::__construct($configHelper, $templatesFactory, $storeManager);
    }

    /**
     * Prepare form fields and structure.. for notifications.
     *
     * @param Form $subject
     * @param string $interceptedOutput
     * @return string
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterSetForm(Form $subject, $interceptedOutput)
    {
        if (!$this->configHelper->isModuleEnabled()) {
            return $interceptedOutput;
        }

        $model = $this->registry->registry('current_status');
        $notifications = [];
        if (is_object($model)) {
            $notifications = $this->notificationModel->getStatusEmails($model->getStatus());
        }

        $this->addEmailFields($subject, $notifications);

        if ($model) {
            $subject->getForm()->addValues($model->getData());
        }
        return $interceptedOutput;
    }
}

<?php
namespace Acty\OrderStatusEmail\Plugin\Block\Sales\Adminhtml\Order\Status;

abstract class AbstractForm
{
    protected $emailTemplates = false;
    /**
     * @var \Acty\OrderStatusEmail\Helper\Module
     */
    protected $configHelper;

    /**
     * @var \Magento\Email\Model\ResourceModel\Template\CollectionFactory
     */
    protected $templatesFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param \Acty\OrderStatusEmail\Helper\Module $configHelper
     * @param \Magento\Email\Model\ResourceModel\Template\CollectionFactory $templatesFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Acty\OrderStatusEmail\Helper\Config $configHelper,
        \Magento\Email\Model\ResourceModel\Template\CollectionFactory $templatesFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->configHelper = $configHelper;
        $this->templatesFactory = $templatesFactory;
        $this->storeManager = $storeManager;
    }

    protected function addEmailFields($subject, $values = [])
    {
        $form = $subject->getForm();
        $fieldset = $form->addFieldset(
            'store_notifications_fieldset',
            ['legend' => __('Order Status Notifications'), 'class' => 'store-scope']
        );
        $renderer = $subject->getLayout()->createBlock('Magento\Backend\Block\Store\Switcher\Form\Renderer\Fieldset');
        $fieldset->setRenderer($renderer);

        foreach ($this->storeManager->getWebsites() as $website) {
            $this->addLabel($fieldset, 'w_' . $website->getId(), $website->getName(), 'website');
            foreach ($website->getGroups() as $group) {
                $stores = $group->getStores();
                if (count($stores) == 0) {
                    continue;
                }
                $this->addLabel($fieldset, 'sg_' . $group->getId(), $group->getName(), 'store-group');
                foreach ($stores as $store) {
                    $this->addEmailTemplateSelect($fieldset, $store->getId(), $store->getName(), $values);
                }
            }
        }
    }

    protected function addEmailTemplateSelect($fieldset, $storeId, $storeName, $values)
    {
        $fieldset->addField(
            "store_email_templates_".$storeId,
            'select',
            [
                'name' => 'store_email_templates[' . $storeId . ']',
                'required' => false,
                'label' => $storeName,
                'value' => $values[$storeId] ?? '',
                'values' => $this->getEmailTemplates(),
                'fieldset_html_class' => 'store'
            ]
        );
    }

    protected function addLabel($fieldset, $uniqId, $labelText, $class)
    {
        $fieldset->addField(
            $uniqId . "_notification_label",
            'note',
            ['label' => $labelText, 'fieldset_html_class' => $class]
        );
    }

    protected function getEmailTemplates()
    {
        if ($this->emailTemplates == false) {
            $templateColection = $this->templatesFactory->create();
            $this->emailTemplates = $templateColection->load()->toOptionArray();
            array_unshift(
                $this->emailTemplates,
                [
                    'value' => '0',
                    'label' => __('Order Status Change - Default Template'),
                ]
            );
            array_unshift(
                $this->emailTemplates,
                [
                    'value' => '-1',
                    'label' => __('--- No template selected - no notification ---'),
                ]
            );
        }
        return $this->emailTemplates;
    }
}

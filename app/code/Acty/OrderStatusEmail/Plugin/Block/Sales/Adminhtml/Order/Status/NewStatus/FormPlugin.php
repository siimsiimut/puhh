<?php
namespace Acty\OrderStatusEmail\Plugin\Block\Sales\Adminhtml\Order\Status\NewStatus;

use Magento\Sales\Block\Adminhtml\Order\Status\NewStatus\Form;

class FormPlugin extends \Acty\OrderStatusEmail\Plugin\Block\Sales\Adminhtml\Order\Status\AbstractForm
{
    /**
     * Prepare form fields and structure.. for notifications.
     *
     * @param Form $subject
     * @param string $interceptedOutput
     * @return string
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterSetForm(Form $subject, $interceptedOutput)
    {
        if (!$this->configHelper->isModuleEnabled()) {
            return $interceptedOutput;
        }
        $this->addEmailFields($subject);

        return $interceptedOutput;
    }
}

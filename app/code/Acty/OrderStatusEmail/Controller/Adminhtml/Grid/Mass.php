<?php
namespace Acty\OrderStatusEmail\Controller\Adminhtml\Grid;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;

/**
 * Handling mass actions
 *
 * @package Acty\OrderStatusEmail\Controller\Adminhtml\Grid
 */
class Mass extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction
{
    /**
     * @var \Acty\OrderStatusEmail\Model\Processor
     */
    protected $orderProcessor;

    /**
     * Mass constructor.
     *
     * @param Context $context
     * @param Filter $filter
     * @param \Acty\OrderStatusEmail\Model\Processor $orderProcessor
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        \Acty\OrderStatusEmail\Model\Processor $orderProcessor
    ) {
        parent::__construct($context, $filter);
        $this->collectionFactory = $collectionFactory;
        $this->orderProcessor = $orderProcessor;
    }

    /**
     * Update selected orders
     *
     * @param AbstractCollection $collection
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    protected function massAction(AbstractCollection $collection)
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $this->orderProcessor->processOrders($collection->getAllIds());
        $resultRedirect->setPath('sales/order');
        return $resultRedirect;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Acty_OrderStatusEmail::actions');
    }
}

<?php
namespace Acty\OrderStatusEmail\Helper;

class Config extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $configPath = 'sales_email/order_status_emails/';

    const DEFAULT_EMAIL_TEMPLATE   = 'orderstatusemail_template';
    const STATUS_EMAILS_COLLECTION = 'advancedorderstatus_notifications';
    const STATUS_EMAILS_SENDED     = 'advancedorderstatus_notified';

    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

    public function isModuleEnabled()
    {
       return $this->scopeConfig->getValue($this->configPath . 'enabled', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function showAllStatuses()
    {
       return $this->scopeConfig->getValue($this->configPath . 'all_statuses_enabled', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}

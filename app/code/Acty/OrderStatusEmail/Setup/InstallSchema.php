<?php
namespace Acty\OrderStatusEmail\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('sales_order_status_email');
        if ($installer->getConnection()->isTableExists($tableName)) {
            $installer->endSetup();
            return;
        }
        $table = $installer->getConnection()
            ->newTable($tableName)
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'id'
            )->addColumn(
                'status_code',
                Table::TYPE_TEXT,
                32,
                ['nullable' => false],
                'Status Code'
            )->addColumn(
                'store_id',
                Table::TYPE_SMALLINT,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Store ID'
            )->addColumn(
                'template_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Template ID'
            )->addIndex(
                $installer->getIdxName(
                    'sales_order_status_email',
                    ['status_code', 'store_id'],
                    AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                ['status_code', 'store_id'],
                ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
            )->addForeignKey(
                $installer->getFkName('sales_order_status_email', 'store_id', 'store', 'store_id'),
                'store_id',
                $installer->getTable('store'),
                'store_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->setComment(
                'Order statuses and email templates connections'
            );
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}

<?php
namespace Acty\RegularCustomerPrice\Block\Catalog\Pricing\Render;

use Magento\Catalog\Pricing\Price;
use Magento\Framework\Pricing\Render\PriceBox as BasePriceBox;
use Magento\Msrp\Pricing\Price\MsrpPrice;
use Magento\Catalog\Model\Product\Pricing\Renderer\SalableResolverInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Pricing\SaleableInterface;
use Magento\Framework\Pricing\Price\PriceInterface;
use Magento\Framework\Pricing\Render\RendererPool;
use Magento\Framework\App\ObjectManager;
use Magento\Catalog\Pricing\Price\MinimalPriceCalculatorInterface;
use Magento\Framework\Pricing\Amount\AmountFactory;
use Magento\Framework\App\Http\Context as HttpContext;
use Acty\RegularCustomerPrice\Model\Calculator;


class FinalPriceBox extends \Magento\Catalog\Pricing\Render\FinalPriceBox
{
    protected $amountFactory;
    protected $httpContext;
    protected $calculator;


    protected $guestPromoPrice = false;

    public function __construct(
        Context $context,
        SaleableInterface $saleableItem,
        PriceInterface $price,
        RendererPool $rendererPool,
        array $data = [],
        SalableResolverInterface $salableResolver = null,
        MinimalPriceCalculatorInterface $minimalPriceCalculator = null,
        AmountFactory $amountFactory,
        HttpContext $httpContext,
        Calculator $calculator


    ) {
        $this->amountFactory = $amountFactory;
        $this->httpContext = $httpContext;
        $this->calculator = $calculator;
        parent::__construct($context, $saleableItem, $price, $rendererPool, $data, $salableResolver, $minimalPriceCalculator);
    }

    public function getGuestPromoPriceAmount()
    {
        return $this->amountFactory->create($this->getGuestPromoPrice(), []);
    }

    public function showGuestPromoPrice()
    {
        if ($this->getGroupId() != 0) {
            return false;
        }

        $guestPromoPrice = $this->getGuestPromoPrice();
        if ($guestPromoPrice == 0.00) {
            return false;
        }

        $finalPriceModel = $this->getPriceType('final_price');
        return $finalPriceModel->getAmount()->getValue() > $guestPromoPrice;
    }

    public function getGuestPromoPrice()
    {
        if ($this->guestPromoPrice === false) {
            $this->initGuestPromoPrice();
        }
        return $this->guestPromoPrice;
    }

    protected function initGuestPromoPrice()
    {
        $guestPromoPrice = $this->calculator->getLoggedInUserCatalogRulePrice($this->getSaleableItem());
        $this->guestPromoPrice = $guestPromoPrice;
    }

    public function getGroupId()
    {
        if ($this->httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH)) {
            return $this->httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_GROUP);
        }
        return '0';
    }
}

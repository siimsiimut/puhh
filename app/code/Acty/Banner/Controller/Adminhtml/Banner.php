<?php
namespace Acty\Banner\Controller\Adminhtml;

abstract class Banner extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Acty_Banner::banner';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     */
    public function __construct(\Magento\Backend\App\Action\Context $context)
    {
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function initPage($resultPage)
    {
        $resultPage->setActiveMenu('Acty_Banner::banner')
            ->addBreadcrumb(__('Acty'), __('Acty'))
            ->addBreadcrumb(__('Banner'), __('Banner'));
        return $resultPage;
    }
}

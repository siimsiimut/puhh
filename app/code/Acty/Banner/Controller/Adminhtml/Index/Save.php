<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Acty\Banner\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Backend\App\Action;
use Acty\Banner\Model\Banners;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\TestFramework\Inspection\Exception;
use Magento\Framework\App\Filesystem\DirectoryList;


class Save extends \Acty\Banner\Controller\Adminhtml\Banner
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;


    /**
     * @param Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getPostData();
        if ($data) {
            $bannerId = $this->getRequest()->getParam('banner_id');
            /** @var \Magento\Cms\Model\Block $model */
            $model = $this->_objectManager->create('Acty\Banner\Model\Banner')->load($bannerId);
            if (!$model->getId() && $bannerId) {
                $this->messageManager->addError(__('This Banner no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            $model->setData($data);
            try {
                $model->save();
                $this->messageManager->addSuccess(__('Banner Saved successfully'));
                $this->dataPersistor->clear('Acty_Banners');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['banner_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the banner.'.$e->getMessage()));
            }

            $this->dataPersistor->set('Acty_Banners', $data);
            return $resultRedirect->setPath('*/*/edit', ['banner_id' => $bannerId]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    protected function getPostData()
    {
        $data = $this->getRequest()->getPostValue();
        if (isset($data['status']) && $data['status'] === 'true') {
            $data['status'] = Block::STATUS_ENABLED;
        }
        if (empty($data['banner_id'])) {
            $data['banner_id'] = null;
        }

        $data['bannerimage'] = $this->getImagePath($data, 'bannerimage');
        $data['gifimage'] = $this->getImagePath($data, 'gifimage');
        return $data;
    }

    protected function getImagePath($data, $field)
    {
        if (!isset($data[$field][0]['name'])) {
            return null;
        }
        if (isset($data[$field][0]['tmp_name'])) {
            return '/banners/'.$data[$field][0]['name'];
        }
        return $data[$field][0]['name'];
    }
}

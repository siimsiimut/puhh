<?php
namespace Acty\Banner\Controller\Adminhtml\Image;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Upload
 */
class BannerImage extends AbstractUpload
{
   protected $field = 'bannerimage';
}

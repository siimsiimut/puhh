<?php
namespace Acty\Banner\Controller\Adminhtml\Image;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Upload
 */
class GifImage extends AbstractUpload
{
   protected $field = 'gifimage';
}

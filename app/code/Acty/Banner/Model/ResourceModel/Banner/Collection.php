<?php
namespace Acty\Banner\Model\ResourceModel\Banner;

use Acty\Banner\Api\Data\BannerInterface;


class Collection extends \Acty\Banner\Model\ResourceModel\Banner\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'banner_id';

    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Acty\Banner\Model\Banner', 'Acty\Banner\Model\ResourceModel\Banner');
        $this->_map['fields']['banner_id'] = 'main_table.banner_id';
        $this->_map['fields']['store'] = 'store_table.store_id';

    }


    protected function _afterLoad()
    {
        $entityMetadata = $this->metadataPool->getMetadata(BannerInterface::class);

        $this->performAfterLoad('acty_banner_store', $entityMetadata->getLinkField());

        return parent::_afterLoad();
    }

    public function addStoreFilter($store, $withAdmin = true)
    {
        $this->performAddStoreFilter($store, $withAdmin);

        return $this;
    }

    protected function _renderFiltersBefore()
    {
        $entityMetadata = $this->metadataPool->getMetadata(BannerInterface::class);
        $this->joinStoreRelationTable('acty_banner_store', $entityMetadata->getLinkField());
    }
}

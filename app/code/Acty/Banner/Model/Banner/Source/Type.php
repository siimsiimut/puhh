<?php
namespace Acty\Banner\Model\Banner\Source;

use Magento\Framework\Data\OptionSourceInterface;


class Type implements OptionSourceInterface
{
    const TYPE_MOBILE = 0;
    const TYPE_DESKTOP = 1;

    public function toOptionArray()
    {
        $availableOptions = [
            self::TYPE_MOBILE => 'Mobile',
            self::TYPE_DESKTOP => 'Desktop'
        ];

        $options = [];
        foreach ($availableOptions as $key => $label) {
            $options[] = [
                'label' => $label,
                'value' => $key,
            ];
        }
        return $options;
    }
}

<?php
namespace Acty\Banner\Model\Banner;

use Acty\Banner\Model\ResourceModel\Banner\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var \Magento\Cms\Model\ResourceModel\Block\Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    public $_storeManager;
    /**
     * @var array
     */
    protected $loadedData;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $blockCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $blockCollectionFactory,
        DataPersistorInterface $dataPersistor,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $blockCollectionFactory->create();
        $this->_storeManager=$storeManager;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        foreach ($this->collection->getItems() as $banner) {
            $this->loadedData[$banner->getId()] = $banner->getData();
        }
        $this->initDataFromPersistor();
        $this->addPath('bannerimage');
        $this->addPath('gifimage');
        return $this->loadedData;
    }

    protected function initDataFromPersistor()
    {
        $data = $this->dataPersistor->get('Acty_Banners');
        if (!empty($data)) {
            $banner = $this->collection->getNewEmptyItem();
            $banner->setData($data);
            $this->loadedData[$banner->getId()] = $banner->getData();
            $this->dataPersistor->clear('Acty_Banners');
        }
    }

    protected function addPath($field)
    {
        if (!$this->loadedData) {
            return;
        }
        foreach($this->loadedData as $id=>$data) {
            if ($data[$field] != null) {
                $this->loadedData[$id][$field] = $this->pathToArr($data[$field]);
            }
        }
    }

    protected function pathToArr($path)
    {
        $baseurl =  $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $img = [];
        $img[0]['name'] = $path;
        $img[0]['url'] = $baseurl . $path;
        return $img;
    }
}

<?php
namespace Acty\Banner\Model;

class Banner extends \Magento\Framework\Model\AbstractModel implements \Acty\Banner\Api\Data\BannerInterface
{
    protected function _construct()
    {
        $this->_init('Acty\Banner\Model\ResourceModel\Banner');
    }

    public function getId()
    {
        return $this->_getData('banner_id');
    }

    public function getStores()
    {
        return $this->hasData('stores') ? $this->getData('stores') : $this->getData('store_id');
    }
}

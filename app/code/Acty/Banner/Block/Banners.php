<?php
namespace Acty\Banner\Block;

use Magento\Framework\View\Element\Template;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\View\Element\Template\Context;
use Acty\Banner\Model\BannerRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;

class Banners extends Template
{
    protected $collectionFactory;
    protected $storeManager;

    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        BannerRepository $bannerRepository,
        FilterBuilder $filterBuilder,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrderBuilder,
        FilterGroupBuilder $filterGroupBuilder
    ) {
        $this->storeManager = $storeManager;
        $this->bannerRepository = $bannerRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
        parent::__construct($context);
    }

    public function getBanner()
    {
        return $this->getBannerByType(\Acty\Banner\Model\Banner\Source\Type::TYPE_DESKTOP);
    }

    public function getMobileBanner()
    {
        return $this->getBannerByType(\Acty\Banner\Model\Banner\Source\Type::TYPE_MOBILE);
    }

    public function getImgFullPath($path)
    {
        return $this->getMediaDirectoryUrl().$path;
    }

    protected function getMediaDirectoryUrl()
    {
        return $this->storeManager
            ->getStore()
            ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    //->addFilter('type', $type)
    protected function getBannerByType($type){
        $sortOrder = $this->sortOrderBuilder->setField('sort_order')->setDirection(\Magento\Framework\Api\SortOrder::SORT_ASC)->create();
        $searchCriteria = $this->searchCriteriaBuilder
                            ->addFilter('status', 1)
                            ->setSortOrders([$sortOrder])
                            ->create();

        return $this->bannerRepository->getList($searchCriteria)->getItems();
    }
}

<?php
namespace Acty\Banner\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Banner block CRUD interface.
 * @api
 * @since 100.0.2
 */
interface BannerRepositoryInterface
{
    /**
     * Save block.
     *
     * @param \Acty\Banner\Api\Data\BlockInterface $block
     * @return \Acty\Banner\Api\Data\BlockInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\BannerInterface $banner);

    /**
     * Retrieve block.
     *
     * @param int $bannerId
     * @return \Acty\Banner\Api\Data\BannerInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($bannerId);

    /**
     * Retrieve blocks matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Acty\Banner\Api\Data\BannerSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete block.
     *
     * @param \Magento\Banner\Api\Data\BannerInterface $block
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(Data\BannerInterface $block);

    /**
     * Delete block by ID.
     *
     * @param int $bannerId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($bannerId);
}

<?php

namespace Acty\Tracking\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class TrackingEmail extends Template
{

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    private $_orderFactory;
    /**
     * Constructor
     *
     * @param Context $context
     * @param array   $data
     */
    public function __construct(
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Cms\Model\BlockFactory $blockFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_filterProvider = $filterProvider;
        $this->_storeManager = $storeManager;
        $this->_blockFactory = $blockFactory;
        $this->_orderFactory = $orderFactory;
    }

    public function getTrackingInfo($orderIncrement)
    {
        $order = $this->_orderFactory->create();
        $order = $order->loadByIncrementId($orderIncrement);
        if($trackingCode = $order->getTrackingCode()){
            $shipping = current(explode('_', $order->getShippingMethod()));

            if(in_array($shipping, ['actydpdcourier', 'actydpdlocker', 'actydpdstore'])){
                return $this->getStaticBlock('dpd-tracking', $trackingCode);
            }
            elseif($shipping === 'post24ee' || $shipping === 'post24lv' || $shipping === 'omnivacourier'){
                return $this->getStaticBlock('omniva-tracking', $trackingCode);
            }
            elseif($shipping === 'estoniansmartpost'){
                return $this->getStaticBlock('itella-tracking', $trackingCode);
            }
        }

        return false;
    }

    private function getStaticBlock($blockId, $tracking){
        $html = '';
        if ($blockId) {
            $array['tracking'] = strip_tags($tracking);
            $storeId = $this->_storeManager->getStore()->getId();
            /** @var \Magento\Cms\Model\Block $block */
            $block = $this->_blockFactory->create();
            $block->setStoreId($storeId)->load($blockId);

            $html = $this->_filterProvider->getBlockFilter()->setStoreId($storeId)->setVariables($array)->filter($block->getContent());
        }
        return   $html;
    }
}
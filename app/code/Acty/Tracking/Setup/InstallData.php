<?php

namespace Acty\Tracking\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class InstallData implements InstallDataInterface
{
    private $blockFactory;

    public function __construct(\Magento\Cms\Model\BlockFactory $blockFactory)
    {
        $this->blockFactory = $blockFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        
        $dpdBlock = [
            'title' => 'DPD Tracking',
            'identifier' => 'dpd-tracking',
            'stores' => [0],
            'is_active' => 1,
            'content' => '<p>Your tracking code: {{var tracking}}</p>'
        ];
        $this->blockFactory->create()->setData($dpdBlock)->save();

        $omnivaBlock = [
            'title' => 'Omniva Tracking',
            'identifier' => 'omniva-tracking',
            'stores' => [0],
            'is_active' => 1,
            'content' => '<p>Your tracking code: {{var tracking}}</p>'
        ];
        $this->blockFactory->create()->setData($omnivaBlock)->save();

        $itellaBlock = [
            'title' => 'Itella Tracking',
            'identifier' => 'itella-tracking',
            'stores' => [0],
            'is_active' => 1,
            'content' => '<p>Your tracking code: {{var tracking}}</p>'
        ];
        $this->blockFactory->create()->setData($itellaBlock)->save();

        $setup->endSetup();
    }
}

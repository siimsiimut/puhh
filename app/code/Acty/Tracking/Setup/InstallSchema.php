<?php

namespace Acty\Tracking\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $field = [
            'type' => Table::TYPE_TEXT,
            'length' => 32,
            'nullable' => true,
            'comment' => 'Tracking code'
        ];

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'), 'tracking_code', $field
        );

        $setup->endSetup();
    }
}

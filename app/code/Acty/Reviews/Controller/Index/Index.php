<?php

namespace Acty\Reviews\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Acty\Reviews\Repositories\ReviewRepository;

class Index extends Action
{
    protected $factory;

    protected $request;

    protected $reviewRepository;

    public function __construct(Context $context, JsonFactory $factory, ReviewRepository $reviewRepository)
    {
        $this->request = $context->getRequest();
        $this->factory = $factory;
        $this->reviewRepository = $reviewRepository;

        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->factory->create();
        $size = $this->request->get('size');
        $page = $this->request->get('page');

        $result->setData($this->reviewRepository->paginate(
            $this->request->get('product_id'),
            is_null($size) ? 10 : $size,
            is_null($page) ? 1 : $page
        ));

        return $result;
    }

    protected function getReviews()
    {

    }
}

<?php

namespace Acty\Reviews\Repositories;

use Magento\Review\Model\ResourceModel\Review\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Review\Model\Review;

class ReviewRepository
{
    protected $collectionFactory;

    protected $storeManager;

    public function __construct(CollectionFactory $collectionFactory, StoreManagerInterface $storeManager)
    {
        $this->collectionFactory = $collectionFactory;
        $this->storeManager = $storeManager;
    }

    public function paginate($productId, $size, $page)
    {
        $collection = $this->collectionFactory->create()
            ->addStoreFilter($this->storeManager->getStore()->getId())
            ->addStatusFilter(Review::STATUS_APPROVED)
            ->addEntityFilter('product', $productId)
            ->setPageSize($size)
            ->setCurPage($page)
            ->setDateOrder()
            ->addRateVotes();

        foreach ($collection->getItems() as &$item) {
            $item->setRatingVotes($item->getRatingVotes()->getData());
        }

        $results = $collection->toArray();
        $items = [];

        $reviews = [
            'total' => $results['totalRecords']
        ];


        foreach ($results['items'] as $item) {
            $items[$item['review_id']] = [
                'id' => $item['review_id'],
                'date' => date('d.m.Y', strtotime($item['created_at'])),
                'datetime' => $item['created_at'],
                'detail' => $item['detail'],
                'nickname' => $item['nickname']
            ];

            foreach ($item['rating_votes'] as $vote) {
                $items[$item['review_id']]['votes'][] = [
                    'percent' => $vote['percent'],
                    'value' => $vote['value'],
                    'code' => $vote['rating_code']
                ];
            }
        }

        $reviews['items'] = array_values($items);

        return $reviews;

    }
}

// window.Vue = require('vue')
// window.axios = require('axios')
// window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'


const deps = window.actyReviews.isProduction ? ['acty-vue-min', 'acty-axios-min'] : ['acty-vue', 'acty-axios']

requirejs(deps, ()=> {
    window.validate = require('validate.js')
    window.emitter = new Vue({})

    new Vue({
        el: '#acty-review',

        data: {
            total: 0,
            items: [],
            page: 1,
            productId: window.actyReviews.productId,
            width: window.innerWidth
        },

        methods: {
            size() {
                return this.width < 768 ? 2 : 4
            },

            get() {
                const params = {
                    product_id: this.productId,
                    size: this.size(),
                    page: this.page
                }

                axios.get('/reviews', { params }).then((res)=> {
                    this.total = res.data.total
                    this.items = res.data.items
                })
            },

            load() {
                this.page = this.page + 1

                const params = {
                    product_id: this.productId,
                    size: this.size(),
                    page: this.page
                }

                axios.get('/reviews', { params }).then((res)=> {
                    this.total = res.data.total

                    for (let i in res.data.items) {
                        this.items.push(res.data.items[i])
                    }
                })
            },

            more() {
                if (this.items.length < this.total) {
                    this.load()
                }
            }
        },

        mounted() {
            window.addEventListener('resize', ()=> {
                this.width = window.innerWidth
                this.page = 1

                this.get()
            })

            this.get()
        }
    })
})

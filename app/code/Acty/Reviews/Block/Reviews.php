<?php

namespace Acty\Reviews\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\App\State;

class Reviews Extends Template
{
    protected $registry;

    protected $state;

    public function __construct(Context $context, Registry $registry, State $state, $data = [])
    {
        $this->registry = $registry;
        $this->state = $state;

        parent::__construct($context, $data);
    }

    public function getProductId()
    {
        return $this->registry->registry('current_product')->getId();
    }

    public function isProduction()
    {
        if ($this->state->getMode() === State::MODE_PRODUCTION) {
            return true;
        }

        return false;
    }
}

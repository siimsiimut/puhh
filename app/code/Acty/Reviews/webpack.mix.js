const mix = require('laravel-mix')

mix.js('resources/js/review.js', 'view/frontend/web/js/review.min.js')

if ( ! mix.inProduction()) {
    mix.sourceMaps().webpackConfig({ devtool: 'inline-source-map' })
}

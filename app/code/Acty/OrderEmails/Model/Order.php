<?php

namespace Acty\OrderEmails\Model;

class Order extends \Magento\Sales\Model\Order
{
    public function getFrontendStatusLabel()
    {
        return $this->getConfig()->getStatusLabel($this->getStatus(), \Magento\Framework\App\Area::AREA_FRONTEND);
    }
}
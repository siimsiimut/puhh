<?php

namespace Acty\OrderEmails\Model\Order;

class Config extends \Magento\Sales\Model\Order\Config
{
    public function getStatusLabel($code, $forceArea = null)
    {
        $area = $forceArea ?: $this->state->getAreaCode();
        $code = $this->maskStatusForArea($area, $code);
        $status = $this->orderStatusFactory->create()->load($code);

        if ($area == 'adminhtml') {
            return $status->getLabel();
        }

        return $status->getStoreLabel();
    }
}
<?php
namespace Acty\PdfInvoice\Overwrite\Sales\Model\Order\Pdf\Items\Invoice;

/**
 * Sales Order Invoice Pdf default items renderer
 */
class DefaultInvoice extends \Magento\Sales\Model\Order\Pdf\Items\Invoice\DefaultInvoice
{
    /**
     * Draw item line
     *
     * @return void
     */
    /**
     * Draw item line
     *
     * @return void
     */
    public function draw()
    {
        $order = $this->getOrder();
        $item = $this->getItem();
        $pdf = $this->getPdf();
        $page = $this->getPage();
        $lines = [];

        // draw Product name
        $lines[0] = [['text' => $this->string->split($item->getName(), 50, true, true), 'feed' => 35]];

        // draw SKU
        $lines[0][] = [
            'text' => $this->string->split($this->getSku($item), 17),
            'feed' => 335,
            'align' => 'right',
        ];

        //draw original price
        $lines[0][] = ['text' => $order->formatPriceTxt($item->getOrderItem()->getOriginalPrice()), 'feed' => 405, 'align' => 'right',  'font' => 'bold'];

        // draw item Prices
        $i = 0;
        $prices = $this->getItemPricesForDisplay();
        $feedPrice = 465;
        $feedSubtotal = 565;
        foreach ($prices as $priceData) {
            if (isset($priceData['label'])) {
                // draw Price label
                $lines[$i][] = ['text' => $priceData['label'], 'feed' => $feedPrice, 'align' => 'right'];
                // draw Subtotal label
                $lines[$i][] = ['text' => $priceData['label'], 'feed' => $feedSubtotal, 'align' => 'right'];
                $i++;
            }
            // draw Price
            $lines[$i][] = [
                'text' => $priceData['price'],
                'feed' => $feedPrice,
                'font' => 'bold',
                'align' => 'right',
            ];
            // draw Subtotal
            $lines[$i][] = [
                'text' => $priceData['subtotal'],
                'feed' => $feedSubtotal,
                'font' => 'bold',
                'align' => 'right',
            ];
            $i++;
        }

        // draw QTY
        $lines[0][] = ['text' => $item->getQty() * 1, 'feed' => 510, 'align' => 'right'];


        $ean = $item->getOrderItem()->getProduct()->getData('ean13');
        if ($ean) {
            $lines[][] = [
                'text' => $this->string->split('EAN: ' . $ean, 60, true, true),
                'font' => 'italic',
                'feed' => 45,
            ];
        }

        // custom options
        $options = $this->getItemOptions();
        if ($options) {
            foreach ($options as $option) {
                // draw options label
                $lines[][] = [
                    'text' => $this->string->split($this->filterManager->stripTags($option['label']).': '.$this->getOptionValue($option), 60, true, true),
                    'font' => 'italic',
                    'feed' => 45,
                ];
            }
        }

        $lineBlock = ['lines' => $lines, 'height' => 20];

        $page = $pdf->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
        $this->setPage($page);
    }

    protected function getOptionValue($option)
    {
        if (!$option['value']) {
            return '';
        }
        if (isset($option['print_value'])) {
            return $option['print_value'];
        }
        return $this->filterManager->stripTags($option['value']);
    }
}

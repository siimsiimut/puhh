<?php

namespace Acty\Delivery\Model\Config\Source;

/**
 * Used in creating options for getting product type value
 *
 */
class Checkbox
{
    public function __construct(
        \Acty\Buum\Helper\Config $config
    ) {
        $this->config = $config;
    }
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $stores = $this->config->getStockRelations();
        $options = [];
            foreach ($stores as $store) {
                $options[] = [
                    'value' => $store['buum_stock_id'],
                    'label'  => $store['store_name']
                ];
            };
        return $options;
    }
}
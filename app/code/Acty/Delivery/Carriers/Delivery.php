<?php

namespace Acty\Delivery\Carriers;

use Magento\Catalog\Model\ProductRepository;
use Magento\Checkout\Model\Cart;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\ResultFactory;
use Psr\Log\LoggerInterface;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Checkout\Model\Session as CartSession;

use Exception;

class Delivery extends AbstractCarrier implements CarrierInterface
{
    public $address;

    protected $_code = 'actydelivery';

    protected $result;

    protected $method;

    protected $cart;

    protected $items;

    protected $customerSession;

    protected $cartSession;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $errorFactory,
        LoggerInterface $logger,
        ResultFactory $result,
        MethodFactory $method,
        Cart $cart,
        ProductRepository $product,
        CustomerSession $customerSession,
        CartSession $cartSession,
        \Acty\Buum\Helper\Config $buumConfig,
        $data = []
    ) {
        $this->result = $result;
        $this->method = $method;
        $this->cart = $cart;
        $this->product = $product;

        parent::__construct($scopeConfig, $errorFactory, $logger, $data);
        $this->customerSession = $customerSession;
        $this->cartSession = $cartSession;
        $this->buumConfig = $buumConfig;
    }

    public function getAllowedMethods()
    {
        $items = $this->cartSession->getQuote()->getAllItems();
        $data = $this->buumConfig->getStockRelations();
        $active = explode(',', $this->getConfigData('buum_active_stores'));
        $activeData = $this->getActiveLocations($active, $data);
        $allowed = $this->getAllowedLocations($items, $activeData);

        $locations = [];
        foreach ($activeData as $row) {
            $id = $row['buum_stock_id'];
            if(!in_array($id, $allowed)){
                continue;
            }

            $locations[$id] = [
                'place_id' => $id,
                'name' => $row['store_name'],
                'group_id' => md5($row['city_group']),
                'group_name' => $row['city_group']
            ];
        }
        return $locations;
    }

    public function collectRates(RateRequest $request)
    {
        if ( ! $this->getConfigFlag('active') || ! $this->hasValidItems($request)) {
            return false;
        }

        if ( ! $this->validateCustomerGroup()) {
            return false;
        }

        $result = $this->result->create();
        $method = $this->method->create();

        $price = $this->getPrice($request);

        $method->setCarrier($this->_code);
        $method->setCarrierTitle($this->getConfigData('title'));
        $method->setMethod($this->_code);
        $method->setMethodTitle($this->getConfigData('name'));
        $method->setPrice($price);
        $method->setCost($price);
        $method->sethasAddress(true);

        $result->append($method);

        if (preg_match('/^'.$this->_code.'_/', $this->getMethod($request))) {
            $items = $this->getItems($request);
            $data = $this->buumConfig->getStockRelations();
            $active = explode(',', $this->getConfigData('buum_active_stores'));
            $activeData = $this->getActiveLocations($active, $data);
            $allowed = $this->getAllowedLocations($items, $data);

            foreach ($activeData as $row) {
                $id = $row['buum_stock_id'];
                if(!in_array($id, $allowed)){
                    continue;
                }

                $method = $this->method->create();

                $method->setCarrier($this->_code);
                $method->setCarrierTitle($this->getConfigData('title'));
                $method->setMethod($id);
                $method->setMethodTitle($row['store_name']);
                $method->setPrice($price);
                $method->setCost($price);
                $method->sethasAddress(true);

                $result->append($method);
            }
        }

        return $result;
    }

    protected function getActiveLocations($active, $data){
        $activeData = [];
        foreach($data as $store){
            if(in_array($store['buum_stock_id'], $active)){
                $activeData[] = $store;
            }
        }
        return $activeData;
    }

    protected function getAllowedLocations($items, $data)
    {
        $allowed = [];
        $first = true;
        foreach($items as $item){
            if($item->getHasChildren()){
                continue;
            }
            $qty = $item->getQty();
            $warehouses = $item->getProduct()->getWarehouseQuantity() ? json_decode($item->getProduct()->getWarehouseQuantity(), true) : [];

            $itemAllowed = [];
            foreach ($data as $row){
                if(array_key_exists($row['buum_stock_id'], $warehouses)){
                    if($warehouses[$row['buum_stock_id']] >= $qty){
                        $itemAllowed[] = $row['buum_stock_id'];
                    }
                }
            }

            if($first === true){
                $allowed = $itemAllowed;
                $first = false;
            }
            else{
                $allowed = array_intersect($allowed, $itemAllowed);
            }
        }
        return $allowed;
    }

    public function getHasAddress()
    {

        return (boolean) $this->getConfigData('requires_address');
    }

    protected function validateCustomerGroup()
    {
        $groups = trim($this->getConfigData('allowed_groups'));

        if ($groups === "") {
            return true;
        }

        $groups = array_map(function($value) {
            return (int) $value;
        }, explode(',', $groups));

        $groupId = $this->customerSession->isLoggedIn()
            ? $this->customerSession->getCustomer()->getGroupId()
            : 0;

        return in_array((int) $groupId, $groups);
    }

    protected function getMethod($request)
    {
        return $this->getItems($request)[0]->getQuote()->getShippingAddress()->getShippingMethod();
    }

    protected function getItems($request)
    {
        if (is_null($this->items)) {
            $this->items = $request->getAllItems();
        }

        return $this->items;
    }

    protected function hasValidItems($request)
    {
        $ids = [];

        foreach ($this->getItems($request) as $item) {
            try {
                $product = $this->product->getById($item->getProduct()->getId());

                if ($product->getData('no_acty_delivery')) {
                    return false;
                }
            } catch (Exception $e) {
                return false;
            }
        }

        return true;
    }

    protected function getPrice(RateRequest $request)
    {
        $price = $this->getConfigData('handling_fee');

        if ( ! $this->getConfigData('free_shipping')) {
            return $price;
        }

        $cartPrice = (float) $request->getPackageValueWithDiscount();
        $minimum = (float) $this->getConfigData('free_shipping_min');

        return $cartPrice >= $minimum ? 0 : $price;
    }
}

<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Acty\DiscountFromFullPrice\Model\Rule\Action\Discount;

use Magento\SalesRule\Model\Rule\Action\Discount\AbstractDiscount;

class CheapestFree extends AbstractDiscount
{
    
    /**
     * @param \Magento\SalesRule\Model\Rule $rule
     * @param \Magento\Quote\Model\Quote\Item\AbstractItem $item
     * @param float $qty
     * @return Data
     */
    public function calculate($rule, $item, $qty)
    {
        
        $quoteItems = $item->getQuote()->getAllItems();
        $prods = [];
        foreach ($quoteItems as $qi) {
            $prods[$qi->getId()] =  $this->validator->getItemPrice($qi);
        };
        $minPriceItemId = array_keys($prods, min($prods))[0];

        if ($item->getId() != $minPriceItemId) {
            /** @var \Magento\SalesRule\Model\Rule\Action\Discount\Data $discountData */
            return $this->discountFactory->create();
        }

        $discountData = $this->_calculate($rule, $item, $qty, $prods);
        return $discountData;
    }

    /**
     * @param \Magento\SalesRule\Model\Rule $rule
     * @param \Magento\Quote\Model\Quote\Item\AbstractItem $item
     * @param float $qty
     * @return Data
     */
    protected function _calculate($rule, $item, $qty)
    {
        /** @var \Magento\SalesRule\Model\Rule\Action\Discount\Data $discountData */
        $discountData = $this->discountFactory->create();

        $itemPrice = $this->validator->getItemPrice($item);
        $baseItemPrice = $this->validator->getItemBasePrice($item);
        $itemOriginalPrice = $this->validator->getItemOriginalPrice($item);
        $baseItemOriginalPrice = $this->validator->getItemBaseOriginalPrice($item);

        $discountData->setAmount($itemPrice);
        $discountData->setBaseAmount($qty * $baseItemPrice - $item->getBaseDiscountAmount());

        $discountData->setOriginalAmount($qty * $itemOriginalPrice - $item->getDiscountAmount());
        $discountData->setBaseOriginalAmount
            ($qty * $baseItemOriginalPrice - $item->getBaseDiscountAmount());

        return $discountData;
    }
}

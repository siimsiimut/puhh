<?php
namespace Acty\DiscountFromFullPrice\Plugin;

class AddNewSalesruleType
{
    public function afterGetMetadataValues($subject, $result)
    {
        $result['actions']['children']['simple_action']['arguments']['data']['config']['options'][] = ['label' => __('Percent of product full price discount'), 'value' =>  'by_percent_from_full_price'];
        $result['actions']['children']['simple_action']['arguments']['data']['config']['options'][] = ['label' => __('cheapest free'), 'value' =>  'cheapest_free'];
        return $result;
    }
}

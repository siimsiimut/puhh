<?php
namespace Acty\OmnivaCourier\Observer;

use Magento\Framework\Event\ObserverInterface;
use Acty\OmnivaCourier\Model\Carrier\OmnivaCourier;


class SalesOrderPaymentAfter implements ObserverInterface
{
    protected $_logger;

    protected $service;


    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Acty\OmnivaCourier\Model\Service $service
    ) {
        $this->_logger = $logger;
        $this->service = $service;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getPayment()->getOrder();
        if (!$this->isOmnivaCourierShipping($order)) {
            return;
        }
        $result = $this->service->sendEplisPreSendMsg($order);

        if ($result !== false and is_array($result)) {
            $comment = $this->_createOrderComment($result);
            $order->addStatusHistoryComment($comment)->setIsCustomerNotified(null);
            $order->save();
        }
    }


    public function isOmnivaCourierShipping($order)
    {
        $shippingMethod = $order->getShippingMethod();
        if (stripos($shippingMethod, OmnivaCourier::CARRIER_CODE) !== false) {
            return true;
        }
        return false;
    }

    private function _createOrderComment($result)
    {
        $orderComment = '<b>' . __('Post24 courier EPLIS') . '</b><br />';
        $orderComment .= ' - ' . __('Message') . ': ' . __($result['msg']) . '<br />';
        if (!empty($result['saved'])) {
            $orderComment .= ' --- <i>' . __('Saved') . ': </i><br />';
            foreach($result['saved'] as $saved) {
                $orderComment .= ' --- --- ' . __('SKU') . ': ' . $saved['sku'] . ' ' . __('Barcode') . ': ' . $saved['barcode'] .  '<br />';
            }
        }
        if (!empty($result['faulty'])) {
            $orderComment .= ' --- <i>' . __('Faulty') . ': </i><br />';
            foreach($result['faulty'] as $faulty) {
                $orderComment .= ' --- --- ' . __('SKU') . ': ' . $faulty['sku'] . ' ' . __('Barcode') . ': ' . $faulty['barcode'] .  '<br />';
            }
        }
        return $orderComment;
    }

}

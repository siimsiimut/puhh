<?php
namespace Acty\OmnivaCourier\Observer;
use Magento\Framework\Event\ObserverInterface;

class SalesOrderPlacementAfter implements ObserverInterface
{
    protected $_logger;

    public function __construct (
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->_logger = $logger;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();

        if ($this->isOmnivaCourierPayment($order)) {
            $this->markOrderAsPaid($order);
        }
    }

    public function isOmnivaCourierPayment($order)
    {
        $paymentMethod = $order->getPayment();
        return ($paymentMethod->getMethod() == 'omniva_courier_payment');
    }

    public function markOrderAsPaid($order)
    {
        if ($order->hasInvoices() or $order->isCanceled()) {
            return;
        }
        $invoice = $order->prepareInvoice();
        if (!$invoice) {
            $this->_logger->error(__('An invoice could not be created: .'. $order->getId()));
            return;
        }
        $invoice->register()->capture();
        $invoiceResource = $invoice->getResource();
        $invoiceResource->save($invoice);

        $order->setState('processing');
        $order->setStatus('makstud');
    }
}

<?php
namespace Acty\OmnivaCourier\Setup;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Eav\Api\AttributeRepositoryInterface;



class InstallData implements InstallDataInterface
{

    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavAttributeSetupInterface;

    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory, AttributeRepositoryInterface $eavAttributeSetupInterface)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavAttributeSetupInterface = $eavAttributeSetupInterface;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

            /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $attributesExist = true;

        try {
            $this->eavAttributeSetupInterface->get(\Magento\Catalog\Model\Product::ENTITY, 'no_omniva_courier');
            $attributesExist = true;
        }
        catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $attributesExist = false;
        }

        if (!$attributesExist) {
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'no_omniva_courier',
                [
                    'group' => 'shipping-methods',
                    'type' => 'int',
                    'input' => 'boolean',
                    'model' => 'Magento\Catalog\Model\ResourceModel\Eav\Attribute',
                    'backend' => 'Magento\Catalog\Model\Product\Attribute\Backend\Boolean',
                    'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                    'label' => 'Omniva courier keelatud',
                    'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'used_in_product_listing' => true,
                    'default' => null
                ]
            );
        }

        $setup->endSetup();
    }
}

<?php
namespace Acty\OmnivaCourier\Helper;

use Acty\OmnivaCourier\Model\Carrier\OmnivaCourier;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_POST24_DATA_EXCHANGE_ENABLE  = 'carriers/omnivacourier/data_exchange_enable';
    const XML_PATH_POST24_PARTNER_ID            = 'carriers/omnivacourier/partner_id';
    const XML_PATH_POST24_USERNAME              = 'carriers/omnivacourier/username';
    const XML_PATH_POST24_PASSWORD              = 'carriers/omnivacourier/password';
    const XML_PATH_POST24_SERVICE               = 'carriers/omnivacourier/service';
    const XML_PATH_POST24_SENDTYPE              = 'carriers/omnivacourier/send_type';
    const XML_PATH_POST24_FILE_ID_PREFIX        = 'carriers/omnivacourier/file_id_prefix';

    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    )
    {
        $this->scopeConfig = $context->getScopeConfig();

        return parent::__construct($context);
    }

    public function isOmnivaCourier($code)
    {
        return OmnivaCourier::CARRIER_CODE == $code;
    }

    public function isDataExchangeEnabled()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_POST24_DATA_EXCHANGE_ENABLE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getPartnerId()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_POST24_PARTNER_ID, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getUserName()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_POST24_USERNAME, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getPassword()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_POST24_PASSWORD, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getService()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_POST24_SERVICE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getFileIdPrefix()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_POST24_FILE_ID_PREFIX, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}

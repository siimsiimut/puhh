<?php
namespace Acty\OmnivaCourier\Model;

class Service
{
    /**
     * EPLIS WSDL URL.
     */
    const EPLIS_WEB_SERVICE_WSDL    = 'https://edixml.post.ee/epmx/services/messagesService.wsdl';

    /**
     * EPLIS web service URL.
     */
    const EPLIS_WEB_SERVICE         = 'https://edixml.post.ee/epmx/services/messagesService';

    /**
     * EPLIS web service schema.
     */
    const EPLIS_WEB_SERVICE_SCHEMA  = 'http://service.core.epmx.application.eestipost.ee/xsd';



    private static $_soapOptions = array(
        'soap_version'  => SOAP_1_2,
        'exceptions'    => true,
        'trace'         => 1,
        'cache_wsdl'    => WSDL_CACHE_NONE,
        'location'      => self::EPLIS_WEB_SERVICE,
        'uri'           => self::EPLIS_WEB_SERVICE_SCHEMA
    );


    /**
     * SOAP client for EPLIS.
     *
     * @var SoapClient
     */
    private $_soapClient = null;


    protected $helper;

    protected $logger;
    /**
     * Default constructor.
     */
    public function __construct(
        \Acty\OmnivaCourier\Helper\Data $helper,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->helper = $helper;
        $this->logger = $logger;
    }


    /**
     * Is data exchange enabled.
     *
     * @var boolean
     */
    public function isExchangeEnabled()
    {
        return $this->helper->isDataExchangeEnabled();
    }

    /**
     * Get Eesti Post partner ID.
     *
     * @var string
     */
    private function _getPartnerId()
    {
        return $this->helper->getPartnerId();
    }

    /**
     * Get username for EPLIS basic authentication.
     *
     * @var string
     */
    private function _getUsername()
    {
        return $this->helper->getUserName();
    }

    /**
     * Get password for EPLIS basic authentication.
     * @var string
     */
    private function _getPassword()
    {
        return $this->helper->getPassword();
    }

    /**
     * Get EPLIS SOAP client.
     */
    private function _getSoapClient()
    {
        if (is_null($this->_soapClient)) {
            $this->initSoapClient();
        }
        return $this->_soapClient;
    }


    /**
     * Initialize EPLIS SOAP client.
     */
    private function initSoapClient()
    {
        $username = $this->_getUsername();
        $password = $this->_getPassword();

        if (empty($username) or empty($password)) {
            throw new \Exception(__('Cannot send to EPLIS because of missing partner credentials!'));
        }

        $options = self::$_soapOptions;
        $options['login'] = $username;
        $options['password'] =  $password;

        try {
            $this->_soapClient = new \SoapClient(self::EPLIS_WEB_SERVICE_WSDL, $options);
        } catch (\Exception $e) {
            $this->logger->info('Error creating EPLIS SOAP client: ' . $e->getMessage());
        }
    }

    /**
     * Send shipment to EPLIS.
     *
     * @param $shipment Mage_Sales_Model_Order_Shipment
     * @return boolean
     */
    public function sendEplisPreSendMsg($shipment)
    {
        if (!$this->isExchangeEnabled()) {
            return false;
        }

        $partnerId = $this->_getPartnerId();
        if (empty($partnerId)) {
            return false;
        }

        $request = $this->_createEplisPreSendMsg($partnerId, $shipment);
        $this->logger->info('REQUEST: ' . print_r($request, true));
        if ($request !== false) {

            $client = $this->_getSoapClient();
            if (is_null($client)) {
                $this->logger->info('The EPLIS client is not initialized');
                return false;
            }

            try {
                $response = $client->preSendMsg($request);
                $result = $this->_readEplisResult($response);
                $this->logger->info('Response: ' . print_r($result, true));
                return $result;
            } catch (\SoapFault $e) {
                $this->logger->info('Error sending message to EPLIS (' . $e->faultcode . '): ' . $e->faultstring);
            } catch (\Exception $e) {
                $this->logger->info('Error sending message to EPLIS: ' . $e->getMessage());
            }
        }

        return false;
    }

    private function _readEplisResult($eplisResponse)
    {
        $eplisResponseArray = $this->_convertObjectToArray($eplisResponse);
        $message = isset($eplisResponseArray['prompt']) ? $eplisResponseArray['prompt'] : 'Unknown response from EPLIS';

        $result = array(
            'msg'       => $message,
            'saved'     => array(),
            'faulty'    => array()
        );

        $result['saved'] = $this->_createResult($eplisResponseArray, 'savedPacketInfo');
        $result['faulty'] = $this->_createResult($eplisResponseArray, 'faultyPacketInfo');

        return $result;
    }

    private function _createResult($eplisResponseArray, $packetInfoType)
    {
        $barcodes = array();
        if (isset($eplisResponseArray[$packetInfoType]) and !empty($eplisResponseArray[$packetInfoType])) {
            $faultyPacketInfo = $eplisResponseArray[$packetInfoType];
            if (isset($faultyPacketInfo['barcodeInfo'])) {
                $barcodeInfo = $faultyPacketInfo['barcodeInfo'];
                if (isset($barcodeInfo['clientItemId'])) {
                    $barcode = array(
                        'sku'       => $barcodeInfo['clientItemId'],
                        'barcode'   => isset($barcodeInfo['barcode']) ? $barcodeInfo['barcode'] : ''
                    );
                    $barcodes[] = $barcode;
                } else {
                    foreach ($barcodeInfo as $info) {
                        $barcode = array(
                            'sku'       => isset($info['clientItemId']) ? $info['clientItemId'] : '',
                            'barcode'   => isset($info['barcode']) ? $info['barcode'] : ''
                        );
                        $barcodes[] = $barcode;
                    }
                }
            }
        }
        return $barcodes;
    }

    /**
     * Create PreSendMsg for EPLIS.
     *
     * @param $partnerId string
     * @param $shipment Mage_Sales_Model_Order_Shipment
     * @return array
     */
    public function _createEplisPreSendMsg($partnerId, $shipment)
    {
        $order = $shipment;
        $email = $order->getCustomerEmail();

        $request = array(
            'partner'     => $partnerId,
            'interchange' => array (
                    'msg_type'  => 'elsinfov1',
                    'header'    => array(
                        'file_id'       => $this->helper->getFileIdPrefix().$shipment->getIncrementId(),
                        'sender_cd'     => $partnerId,
                        'currency_cd'   => $order->getOrderCurrencyCode()
                ),
                'item_list' => array(
                    'item' => array()
                )
            )
        );
        $weight = 0;
        foreach ($shipment->getAllItems() as $item) {
            if ($item->getProductType() != 'configurable') {
                $weight += $item->getWeight();
            }
        }

        $requestItem = $this->_createEplisItem($weight, $shipment->getShippingAddress(), $email);
        if ($this->isOmnivaCourierPayment($shipment)) {
            $requestItem['monetary_values'] = array(
                'values' => array(
                    'code' => 'item_value',
                    'amount' => $shipment->getGrandTotal()
                )
            );
            $requestItem['add_service']['option'][]['code'] = 'BP';
        }
        $request['interchange']['item_list']['item'][] = $requestItem;

        return $request;
    }

    private function isOmnivaCourierPayment($order)
    {
        $paymentMethod = $order->getPayment();
        return ($paymentMethod->getMethod() == 'omniva_courier_payment');
    }

    private function _createEplisItem($weight, $address, $email)
    {
        $eplisItem = array(
            'service'       => 'CI',
            'measures' => array(
                'weight' => $weight
            ),
            'receiverAddressee' => array(
                'person_name'       => $address->getName(),
                'phone'             => $address->getTelephone(),
                'mobile'            => $address->getTelephone(),
                'email'             => $email,
                'address'           => array(
                    'postcode'          => $address->getPostCode(),
                    'deliverypoint'     => $address->getCity(),
                    'street'            => implode(' ', $address->getStreet()),
                    'country'           => $address->getCountryId()

                )
            ),
        );

        $eplisItem['add_service']['option'][]['code'] = 'ST';

        return $eplisItem;
    }

    /**
     * Converts stdClass object to arry.
     *
     * @param $data stdClass
     */
    private function _convertObjectToArray($data)
    {
        if (is_object($data)) {
            $data = get_object_vars($data);
        }
        return is_array($data) ? array_map(array($this, '_convertObjectToArray'), $data) : $data;
    }

}

<?php
namespace Acty\OmnivaCourier\Model\Payment;

use Magento\Payment\Model\InfoInterface;
use Magento\Payment\Model\Method\AbstractMethod;

class OmnivaCourierPayment extends AbstractMethod {

    protected $_code = 'omniva_courier_payment';

    public function capture(InfoInterface $payment, $amount)
    {

    }

}

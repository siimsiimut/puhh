<?php
namespace Acty\OmnivaCourier\Model\Carrier;

use Magento\Catalog\Model\ProductRepository;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;
use Magento\Framework\App\ObjectManager;

class OmnivaCourier extends \Magento\Shipping\Model\Carrier\AbstractCarrier
    implements \Magento\Shipping\Model\Carrier\CarrierInterface
{

    const NO_OMNIVA_COURIER_ATTRIBUTE = 'no_omniva_courier';

    const CARRIER_CODE = 'omnivacourier';

    /**
     * @var string
     */
    protected $_code = self::CARRIER_CODE;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart = null;

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $_http = null;

    protected $productRepository;

    protected $rateResult;

    protected $_rateResultFactory;
    protected $_rateMethodFactory;

    protected $helper;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Shipping\Model\Rate\Result $rateResult,
        ProductRepository $productRepository,
        array $data = []
    ) {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        $this->_rateErrorFactory = $rateErrorFactory;

        $this->_cart = $cart;

        $this->productRepository = $productRepository;
        $this->rateResult = $rateResult;

        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    public function getAllowedMethods()
    {
        return [$this->_code = $this->getConfigData('name')];
    }


    public function getHasAddress()
    {
        return true;
    }

    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        if (!$request->getAllItems()) {
            return false;
        }

        if ($this->hasExcludedProducts($request->getAllItems())) {
            return false;
        }

        $this->refreshFreeBoxes($request);


        $shippingPrice = $this->getPrice($request);


        $method = $this->_rateMethodFactory->create();
        $method->setCarrier($this->_code);
        $method->setCarrierTitle($this->getConfigData('title'));
        $method->setMethod($this->_code);
        $method->setMethodTitle($this->getConfigData('name'));
        $method->setPrice($shippingPrice);
        $method->setCost($shippingPrice);
        $method->sethasAddress(true);

        $result = $this->_rateResultFactory->create();
        $result->append($method);

        return $result;
    }

    protected function getPrice($request)
    {
        $isFreeShippingEnabled = $this->getConfigFlag('free_shipping_enable');
        $packageValue = $request->getPackageValueWithDiscount();

        $shippingPrice = $this->getConfigData('handling_fee');
        if (empty($shippingPrice) || !is_numeric($shippingPrice) || $shippingPrice < 0) {
            $shippingPrice = '0.00';
        }

        if ($isFreeShippingEnabled && $packageValue >= $this->getConfigData('free_shipping_subtotal')) {
            $shippingPrice = '0.00';
        }

        if ($request->getPackageQty() == $this->getFreeBoxes()) {
            $shippingPrice = '0.00';
        }
        return $shippingPrice;

    }

    protected function refreshFreeBoxes($request)
    {
        $freeBoxes = 0;
        foreach ($request->getAllItems() as $item) {
            if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                continue;
            }

            if ($item->getHasChildren() && $item->isShipSeparately()) {
                foreach ($item->getChildren() as $child) {
                    if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                        $freeBoxes += $item->getQty() * $child->getQty();
                    }
                }
            } elseif ($item->getFreeShipping()) {
                $freeBoxes += $item->getQty();
            }
        }
        $this->setFreeBoxes($freeBoxes);
    }

    protected function hasExcludedProducts($items)
    {
        $isCheckEnabled = $this->getConfigFlag('check_enable');
        if (!$this->getConfigFlag('check_enable')) {
            return false;
        }

        foreach ($items as $item) {
            $product = $this->productRepository->getById($item->getProduct()->getId());
            $nopost24 = $product->getData(self::NO_OMNIVA_COURIER_ATTRIBUTE);
            if ($nopost24) {
                return true;
            }
        }
        return false;
    }
}

<?php
namespace Acty\Puhh\Plugin\OfflineShipping\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Catalog\Model\ProductRepository;


class Flatrate
{
    protected $product;

    public function __construct(
        ProductRepository $product
    ) {
        $this->product = $product;
    }


    public function aroundCollectRates(\Magento\OfflineShipping\Model\Carrier\Flatrate $subject, callable $proceed, RateRequest $request)
    {
        if (!$this->hasValidItems($request)) {
            return false;
        }
        return $proceed($request);
    }


    protected function hasValidItems($request)
    {
        foreach ($request->getAllItems() as $item) {
            try {
                $product = $this->product->getById($item->getProduct()->getId());

                if ($product->getData('no_self_pickup')) {
                    return false;
                }
            } catch (Exception $e) {
                return false;
            }
        }

        return true;
    }
}

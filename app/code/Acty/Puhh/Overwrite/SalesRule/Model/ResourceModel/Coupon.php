<?php
namespace Acty\Puhh\Overwrite\SalesRule\Model\ResourceModel;


class Coupon extends \Magento\SalesRule\Model\ResourceModel\Coupon
{
    /**
     * Constructor adds unique fields
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('salesrule_coupon', 'coupon_id');
        #$this->addUniqueField(['field' => 'code', 'title' => __('Coupon with the same code')]);
    }
}

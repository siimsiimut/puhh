<?php

namespace Acty\Puhh\Helper;

use Magento\Framework\Stdlib\DateTime\TimezoneInterface;


class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var TimezoneInterface
     */
    protected $localeDate;

    public function __construct(
        TimezoneInterface $localeDate
    ) {
        $this->localeDate = $localeDate;
    }

    public function isProductNew($product)
    {
        $newsFromDate = $product->getNewsFromDate();
        $newsToDate = $product->getNewsToDate();
        if (!$newsFromDate && !$newsToDate) {
            return false;
        }

        return $this->localeDate->isScopeDateInInterval(
            $product->getStore(),
            $newsFromDate,
            $newsToDate
        );
    }

    public function DisplayDiscountLabel($_product)
    {
        $originalPrice = $_product->getPriceInfo()->getPrice('regular_price')->getAmount()->getValue();
        $finalPrice = $_product->getPriceInfo()->getPrice('final_price')->getAmount()->getValue();

        $percentage = 0;
        if ($originalPrice > $finalPrice) {
            $percentage = number_format(($originalPrice - $finalPrice) * 100 / $originalPrice,0);
        }

        if ($percentage) {
            return "-".$percentage."%";
        }

    }
}

<?php
namespace Acty\Puhh\Helper;

use Magento\Framework\Image\AdapterFactory;
use Magento\Framework\Filesystem;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class OptionsSettingsImg extends \Magento\Framework\App\Helper\AbstractHelper
{
    const IMG_PATH = 'amasty/shopby/option_images/';
    public function __construct(
        AdapterFactory $imageFactory,
        Filesystem $filesystem,
        StoreManagerInterface $storeManager
    ) {
        $this->_imageFactory = $imageFactory;
        $this->_filesystem = $filesystem;
        $this->_storeManager = $storeManager;
    }

    public function getResizedImageUrl($image, $width = null, $height = null)
    {
        if (empty($image)) {
            return false;
        }

        $mediaDir = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA);
        $absolutePath = $this->getOptionImagesFolderPath().$image;
        $imageResized = $this->getOptionImagesFolderPath().'resized/'.$width.'/'.$image;
        //create image factory...
        $imageResize = $this->_imageFactory->create();
        $imageResize->open($absolutePath);
        $imageResize->constrainOnly(true);
        $imageResize->keepTransparency(true);
        $imageResize->keepFrame(false);
        $imageResize->keepAspectRatio(true);
        $imageResize->resize($width, $height);
        $imageResize->save($imageResized);

        $resizedURL = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).self::IMG_PATH.'resized/'.$width.'/'.$image;
        return $resizedURL;
    }

    public function getOptionImagesFolderPath()
    {
        $mediaDir = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA);
        return $mediaDir->getAbsolutePath(self::IMG_PATH);
    }
}

<?php
namespace Acty\Puhh\Block\Review;

use Magento\Framework\View\Element\Template;

class Rating extends Template
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    public function getProduct()
    {
        return $this->_coreRegistry->registry('product');
    }

    public function getRatingSummary()
    {
        return $this->getProduct()->getRatingSummary() ?? '';
        
    }

    public function getReviewsCount()
    {
        $summary = $this->getProduct()->getRatingSummary();
        if ($summary) {
            return $summary->getReviewsCount();
        } 
    }
}

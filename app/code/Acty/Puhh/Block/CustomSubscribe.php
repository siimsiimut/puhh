<?php
namespace Acty\Puhh\Block;

use Magento\Newsletter\Block\Subscribe;

class CustomSubscribe extends Subscribe
{
    protected $pageFactory;
    protected $termsPage;
    protected $_storeManager;
    public function __construct(
        \Magento\Cms\Model\PageFactory $pageFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\Element\Template\Context $context
    ) {
        $this->pageFactory = $pageFactory;
        $this->_storeManager = $storeManager;

        parent::__construct($context);
    }

    /**
     * Get Store code
     *
     * @return string
     */
    public function getStoreCode()
    {
        return $this->_storeManager->getStore()->getCode();
    }

    public function getTermsPageContent()
    {
        $storeCode = $this->getStoreCode();
        $policyPageId = null;
        if ($storeCode === 'toysoutlet_ru') {
            $policyPageId = 'privaatsuspoliitika-76';
        } elseif($storeCode === 'toysoutlet_lv') {
            $policyPageId = 'privatuma-politika';
        } elseif($storeCode === 'toysoutlet_en') {
            $policyPageId = 'privacy-policy';
        } elseif($storeCode === 'et') {
            $policyPageId = 'privaatsuspoliitika-76';
        } elseif($storeCode === 'ru') {
            $policyPageId = 'privaatsuspoliitika-76';
        } elseif($storeCode === 'en') {
            $policyPageId = 'privacy-policy';
        } else {
            $policyPageId = 'privaatsuspoliitika-76';
        }
        if (is_null($this->termsPage)) {
            $this->termsPage = $this->setStoreId($this->_storeManager->getStore()->getId())->pageFactory->create();
            $this->termsPage->setStoreId($this->_storeManager->getStore()->getId())->load($policyPageId, 'identifier');
        }
        return $this->setStoreId($this->_storeManager->getStore()->getId())->termsPage->getContent();
    }
}

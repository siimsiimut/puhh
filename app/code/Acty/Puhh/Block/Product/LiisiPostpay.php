<?php
namespace Acty\Puhh\Block\Product;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Registry;

class LiisiPostpay extends \Magento\Framework\View\Element\Template
{
    protected $registry;
    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->registry = $registry;
        parent::__construct($context, $data);
    }

    public function getProductPrice()
    {
        return round($this->registry->registry('current_product')->getFinalPrice(), 2);
    }
}

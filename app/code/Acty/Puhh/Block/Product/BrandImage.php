<?php
namespace Acty\Puhh\Block\Product;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Registry;
use Amasty\ShopbyBase\Api\Data\OptionSettingRepositoryInterface;
use Acty\Puhh\Helper\OptionsSettingsImg;

class BrandImage extends \Magento\Framework\View\Element\Template
{
    protected $registry;
    protected $optionSettingsRepo;
    protected $brand = false;

    public function __construct(
        Context $context,
        Registry $registry,
        OptionSettingRepositoryInterface $optionSettingsRepo,
        OptionsSettingsImg $imgHelper,
        array $data = []
    ) {
        $this->registry = $registry;
        $this->optionSettingsRepo = $optionSettingsRepo;
        $this->imgHelper = $imgHelper;
        parent::__construct($context, $data);
    }

    public function getProduct()
    {
        return $this->registry->registry('current_product');
    }

    public function getBrand()
    {
        if ($this->brand === false) {
            $brandId = $this->getProduct()->getBrand();
            $this->brand = $this->optionSettingsRepo->getByParams('attr_brand', $brandId, 0);
        }
        return $this->brand;
    }

    public function getBrandImage()
    {
        $brand = $this->getBrand();
        if ($brand->getImage()) {
            return $this->getResizedImageUrl($brand->getImage(), 109);
        }
        return false;
    }

    public function getBrandTitle()
    {
        return $this->getBrand()->getTitle();
    }

    public function getResizedImageUrl($image, $width = null, $height = null)
    {
        return $this->imgHelper->getResizedImageUrl($image, $width, $height);
    }

    public function getBrandPageUrl()
    {
        return $this->getBrand()->getUrlPath();
    }
}

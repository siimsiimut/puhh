<?php
namespace Acty\Puhh\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use \Amasty\ShopbyBase\Model\OptionSettingFactory;
use Acty\Puhh\Helper\OptionsSettingsImg;

class PromoAttributes extends \Magento\Framework\View\Element\Template
{
    protected $optionsSettingsFactory;
    protected $collection = false;

    public function __construct(
        Context $context,
        \Magento\Eav\Model\Config $eavConfig,
        OptionSettingFactory $optionsSettingsFactory,
        OptionsSettingsImg $imgHelper,
        array $data = []
    ) {
        $this->optionsSettingsFactory = $optionsSettingsFactory;
        $this->imgHelper = $imgHelper;
        $this->eavConfig = $eavConfig;

        parent::__construct($context, $data);
    }

    public function getAttributeOptionText($attribute,$id)
    {
        $attribute = $this->eavConfig->getAttribute('catalog_product', $attribute);
        return $attribute->getSource()->getOptionText($id);
    }

    public function initCollection()
    {
        $this->collection = $this->optionsSettingsFactory->create()->getCollection();
        $this->collection->addFieldToFilter('filter_code', $this->getAttributeCode());

        $this->collection->getSelect()->joinRight(
            ['eav_attribute_option' => $this->collection->getTable('eav_attribute_option')],
            'main_table.value = eav_attribute_option.option_id'
        );
        $this->collection->setOrder('sort_order', 'ASC');
        $this->removeMissingPictures();
    }

    protected function removeMissingPictures() {
        $arr = [];
        foreach ($this->collection as $item) {
            if (is_file($this->imgHelper->getOptionImagesFolderPath().$item->getImage())) {
                $arr[] = $item;
            }
        }
        $this->collection = $arr;
    }

    public function getCollection()
    {
        if ($this->collection === false) {
            $this->initCollection();
        }
        return $this->collection;
    }

    public function hasValues()
    {
        return count($this->getCollection()) > 0;
    }

    protected function _toHtml()
    {
        if (!$this->hasValues()) {
            return '';
        }

        return parent::_toHtml();
    }

    public function getResizedImageUrl($image, $width = null, $height = null)
    {
        return $this->imgHelper->getResizedImageUrl($image, $width, $height);
    }
}

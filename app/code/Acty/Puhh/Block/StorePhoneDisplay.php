<?php
namespace Acty\Puhh\Block;

class StorePhoneDisplay extends \Magento\Framework\View\Element\Template
{
    protected $objMgr;
    protected $storeInfo;
    protected $scopeConfig;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context
    ) {
        parent::__construct($context);

        $this->_objMgr =  \Magento\Framework\App\ObjectManager::getInstance();
        $storeInformation = $this->_objMgr->create('Magento\Store\Model\Information');
        $store = $this->_objMgr->create('Magento\Store\Model\Store');
        $this->_storeInfo = $storeInformation->getStoreInformationObject($store);
    }

    public function getPhoneNumber()
    {
        return $this->_storeInfo->getPhone();
    }
}

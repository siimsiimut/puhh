require([
    'jquery',
    'domReady!'
], function ($, p) {
    var colorize = function() {
        var cols = $('.data-grid-cell-content')

        cols.each(function(i, col) {
            var col = $(col)
            var value = col.text()
            var color = '#ffffff'

            switch(value) {
                case 'Tellimusele võib järele tulla':
                    color = '#FF8C00'
                    break
                case 'Tellimus postitatud':
                    color = '#d393f6'
                    break
                case 'Tellimus on lõpetatud':
                    color = '#108510'
                    break
                case 'Canceled':
                    color = '#f697b8'
                    break
                case 'Tellimus on makstud':
                    color = '#84e984'
                    break
                case 'Tellimus on töös':
                    color = '#ffef9d'
                    break
                case 'Pending':
                case 'Processing':
                case 'Pending Payment':
                    color = '#a0bbf4'
                    break
            }

            col.closest('tr.data-row').find('td').css('background-color', color)
        })
    }

    $(document).ready(function() {
        var spinner = $('div[data-role="spinner"]')

        setInterval(function() {
            if ( ! spinner.is(':visible'))  {
                colorize()
            }
        }, 750)
    })
})

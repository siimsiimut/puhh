<?php

declare(strict_types=1);

namespace HolmBank\Payments\Observer;

use HolmBank\Payments\Model\HirePurchase;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Sales\Model\Order;

/**
 * Disable email sending before payment is accepted by gateway.
 */
class DisableEmailAfterOrderCreation implements ObserverInterface
{
    /**
     * @inheritdoc
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getEvent()->getOrder();

        $paymentMethod = $order->getPayment()->getMethod();

        if (false !== \strpos($paymentMethod, HirePurchase::PAYMENT_METHOD_HIREPURCHASE_CODE)) {
            $order->setCanSendNewEmailFlag(false);
        }
    }
}

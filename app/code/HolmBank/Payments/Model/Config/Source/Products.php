<?php

declare(strict_types=1);

namespace HolmBank\Payments\Model\Config\Source;

use HolmBank\Payments\Model\HolmBankProductFactory;
use HolmBank\Payments\Model\HolmbankProductRepository;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\App\Config\ScopeConfigInterface;
use HolmBank\Payments\Model\ResourceModel\Product;
use Psr\Log\LoggerInterface;

class Products extends Field
{
    const CONFIG_PATH = 'payment/holm_partner/products';

    protected $_template = 'HolmBank_Payments::checkbox.phtml';

    protected $_values = null;

    private Curl $curl;

    private ScopeConfigInterface $scopeConfig;

    private LoggerInterface $logger;
    private HolmbankProductRepository $holmbankProductRepository;
    private HolmBankProductFactory $holmbankProductFactory;


    /**
     * Checkbox constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        Curl                                    $curl,
        ScopeConfigInterface                    $scopeConfig,
        LoggerInterface                         $logger,
        HolmbankProductRepository               $holmbankProductRepository,
        HolmBankProductFactory                  $holmbankProductFactory,
        array                                   $data = []
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->curl = $curl;
        $this->logger = $logger;
        $this->holmbankProductRepository = $holmbankProductRepository;
        $this->holmbankProductFactory = $holmbankProductFactory;
        parent::__construct($context, $data);
    }

    /**
     * Retrieve element HTML markup.
     *
     * @param AbstractElement $element
     *
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $this->setNamePrefix($element->getName())->setHtmlId($element->getHtmlId());

        return $this->_toHtml();
    }

    public function getValues()
    {
        $values = [];
        $products = $this->getProducts();
        foreach ($products as $value) {
            if ($value == "ERROR") {
                return $values;
            }
            $values[$value['type']] = [
                'name' => $value['name'],
                'image' => $value['image']
            ];
        }
        return $values;
    }

    public function isApiConfigPresent()
    {
        $apiUrl = $this->scopeConfig->getValue('payment/holm_partner/api_url');
        $paymentKey = $this->scopeConfig->getValue('payment/holm_partner/payment_key');
        if (empty($apiUrl) || empty($paymentKey)) {
            return false;
        }
        return true;
    }

    private function getProducts()
    {
        $products = [];
        if (!$this->isApiConfigPresent()) {
            return $products;
        }
        $apiUrl = $this->scopeConfig->getValue('payment/holm_partner/api_url') . '/api/partners/public/payment-link/products';
        $paymentKey = $this->scopeConfig->getValue('payment/holm_partner/payment_key');
        try {
            $this->curl->addHeader("x-payment-link-key", $paymentKey);
            $this->curl->get($apiUrl);
            $response = $this->curl->getBody();
            $response = json_decode($response, true);
            if (empty($response)) {
                return
                    [
                        'type' => 'ERROR',
                        'name' => "Error getting products: " . $this->curl->getStatus()
                    ];
            }
            for ($i = 0; $i < count($response); $i++) {
                $product = $response[$i];
                $logo = $this->_assetRepo->getUrl('HolmBank_Payments::images/logo.png');
                if (array_key_exists("logoUrl", $product) && !is_null($product['logoUrl'])) {
                    $logo = $product["logoUrl"];
                }
                $products[$i] =
                    [
                        'type' => $product["type"],
                        'image' => $logo,
                        'name' => $product["name"]
                    ];
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            return
                [
                    'type' => 'ERROR',
                    'label' => "Error getting products: " . $e->getMessage()
                ];
        }
        $this->storeProducts($products);
        return $products;
    }

    /**
     * Get checked value. Used in Checkbox.phtml
     *
     * @param  $name
     * @return boolean
     */
    public function getIsChecked($name)
    {
        $checked = $this->getCheckedValues();
        return in_array($name, $checked);
    }

    /**
     *
     * Retrieve the checked values from config
     */
    public function getCheckedValues()
    {
        if (is_null($this->_values)) {
            $data = $this->getConfigData();
            if (isset($data[self::CONFIG_PATH])) {
                $data = $data[self::CONFIG_PATH];
            } else {
                $data = '';
            }
            $this->_values = explode(',', $data);
        }

        return $this->_values;
    }

    private function storeProducts(array $products)
    {
        $this->holmbankProductRepository->clearProducts();
        foreach ($products as $prod) {
            $product = $this->holmbankProductFactory->create();
            $product->setHolmBankProductType($prod['type']);
            $product->setHolmBankProductLogo($prod['image']);
            $product->setHolmBankProductName($prod['name']);
            $this->holmbankProductRepository->save($product);
        }
    }
}

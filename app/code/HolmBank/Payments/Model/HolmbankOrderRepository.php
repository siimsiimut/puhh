<?php

declare(strict_types=1);

namespace HolmBank\Payments\Model;

use HolmBank\Payments\Api\Data\HolmbankOrderInterface;
use HolmBank\Payments\Api\HolmbankOrderRepositoryInterface;
use HolmBank\Payments\Model\ResourceModel\HolmBankOrder;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class HolmbankOrderRepository implements HolmbankOrderRepositoryInterface
{
    /**
     * @var HolmBankOrder
     */
    private HolmBankOrder $resource;

    /**
     * @var HolmBankOrderFactory
     */
    private HolmBankOrderFactory $orderFactory;

    /**
     * Class constructor.
     *
     * @param HolmBankOrder $resource
     * @param HolmBankOrderFactory $orderFactory
     */
    public function __construct(
        HolmBankOrder $resource,
        HolmBankOrderFactory $orderFactory
    ) {
        $this->resource = $resource;
        $this->orderFactory = $orderFactory;
    }

    /**
     * @inheritDoc
     *
     * @param HolmbankOrderInterface $order
     * @return void
     * @throws CouldNotSaveException
     */
    public function save(HolmbankOrderInterface $order): void
    {
        try {
            $this->resource->save($order);
        } catch (AlreadyExistsException|\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
    }

    /**
     * @inheritDoc
     *
     * @param $orderId
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getByOrderId($orderId): HolmbankOrderInterface
    {
        $order = $this->orderFactory->create();
        $this->resource->load($order, $orderId, 'holmbank_order_id');
        if (!$order->getId()) {
            throw new NoSuchEntityException(__('The Order with the "%1" ID doesn\'t exist.', $orderId));
        }
        return $order;
    }
}

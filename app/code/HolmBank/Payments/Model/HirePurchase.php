<?php

declare(strict_types=1);

namespace HolmBank\Payments\Model;

class HirePurchase extends Payment
{
    /**
     * Payment code
     *
     * @var string
     */
    const PAYMENT_METHOD_HIREPURCHASE_CODE = 'holm_partner';

    /**
     * Payment method code
     *
     * @var string
     */
    protected $_code = self::PAYMENT_METHOD_HIREPURCHASE_CODE;

    /**
     * Availability option
     *
     * @var bool
     */
    protected $_isOffline = true;
}

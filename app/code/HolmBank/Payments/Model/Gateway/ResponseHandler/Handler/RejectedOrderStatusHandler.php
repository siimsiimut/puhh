<?php

declare(strict_types=1);

namespace HolmBank\Payments\Model\Gateway\ResponseHandler\Handler;

use HolmBank\Payments\Api\Data\HolmbankOrderInterface;
use HolmBank\Payments\Model\HolmbankOrderRepository;
use HolmBank\Payments\Model\Payment;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;

/**
 * Rejected payment status response handler
 */
class RejectedOrderStatusHandler implements ResponseHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private OrderRepositoryInterface $orderRepository;

    /**
     * @var OrderManagementInterface
     */
    private OrderManagementInterface $orderManagement;

    /**
     * @var HolmbankOrderRepository
     */
    private HolmbankOrderRepository $holmbankOrderRepository;

    /**
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderManagementInterface $orderManagement
     * @param HolmbankOrderRepository $holmbankOrderRepository
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        OrderManagementInterface $orderManagement,
        HolmbankOrderRepository $holmbankOrderRepository
    ) {
        $this->orderRepository = $orderRepository;
        $this->orderManagement = $orderManagement;
        $this->holmbankOrderRepository = $holmbankOrderRepository;
    }

    /**
     * @inheritdoc
     */
    public function handle(OrderInterface $order, HolmbankOrderInterface $holmbankOrder): void
    {
        $holmbankOrder->setOrderStatus(Payment::REJECTED_STATUS);
        $this->holmbankOrderRepository->save($holmbankOrder);

        if ($order->getStatus() === Order::STATE_CANCELED) {
            return;
        }

        $order->addCommentToStatusHistory(__('Payment rejected on payment gateway.'));
        $this->orderRepository->save($order);
        $this->orderManagement->cancel($order->getId());
    }
}

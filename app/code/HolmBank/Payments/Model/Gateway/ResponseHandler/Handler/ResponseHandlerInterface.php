<?php

declare(strict_types=1);

namespace HolmBank\Payments\Model\Gateway\ResponseHandler\Handler;

use HolmBank\Payments\Api\Data\HolmbankOrderInterface;
use Magento\Sales\Api\Data\OrderInterface;

/**
 * Interface for response handlers.
 */
interface ResponseHandlerInterface
{
    /**
     * Process response data.
     *
     * @param OrderInterface $order
     * @param HolmbankOrderInterface $holmbankOrder
     *
     * @return void
     */
    public function handle(OrderInterface $order, HolmbankOrderInterface $holmbankOrder): void;
}

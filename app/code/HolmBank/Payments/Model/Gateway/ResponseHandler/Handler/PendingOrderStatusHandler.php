<?php

declare(strict_types=1);

namespace HolmBank\Payments\Model\Gateway\ResponseHandler\Handler;

use HolmBank\Payments\Api\Data\HolmbankOrderInterface;
use HolmBank\Payments\Model\HolmbankOrderRepository;
use HolmBank\Payments\Model\Payment;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Config as OrderConfig;

/**
 * Pending payment status response handler
 */
class PendingOrderStatusHandler implements ResponseHandlerInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private OrderRepositoryInterface $orderRepository;

    /**
     * @var HolmbankOrderRepository
     */
    private HolmbankOrderRepository $holmbankOrderRepository;

    /**
     * @var OrderConfig
     */
    private OrderConfig $orderConfig;

    /**
     * @param OrderRepositoryInterface $orderRepository
     * @param HolmbankOrderRepository $holmbankOrderRepository
     * @param OrderConfig $orderConfig
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        HolmbankOrderRepository $holmbankOrderRepository,
        OrderConfig $orderConfig
    ) {
        $this->orderRepository = $orderRepository;
        $this->holmbankOrderRepository = $holmbankOrderRepository;
        $this->orderConfig = $orderConfig;
    }

    /**
     * @inheritdoc
     */
    public function handle(OrderInterface $order, HolmbankOrderInterface $holmbankOrder): void
    {
        $holmbankOrder->setOrderStatus(Payment::PENDING_STATUS);
        $this->holmbankOrderRepository->save($holmbankOrder);

        $order->setState(Order::STATE_PENDING_PAYMENT);
        $order->setStatus($this->orderConfig->getStateDefaultStatus($order->getState()));
        $order->addCommentToStatusHistory(__('Received payment pending status from payment gateway.'));
        $this->orderRepository->save($order);
    }
}

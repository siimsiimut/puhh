<?php

declare(strict_types=1);

namespace HolmBank\Payments\Model\Gateway\ResponseHandler;

use HolmBank\Payments\Model\Gateway\ResponseHandler\Handler\ResponseHandlerInterface;
use Magento\Framework\ObjectManagerInterface;

/**
 * Class responsible for providing response handler.
 */
class ResponseHandlerFactory
{
    /**
     * @var string[]
     */
    private array $handlers;

    /**
     * @var ObjectManagerInterface
     */
    private ObjectManagerInterface $objectManager;

    /**
     * @param ObjectManagerInterface $objectManager
     * @param string[] $handlers
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        array $handlers = []
    ) {
        $this->handlers = $handlers;
        $this->objectManager = $objectManager;
    }

    /**
     * Retrieve response handler by response status.
     *
     * @param string $status
     *
     * @return ResponseHandlerInterface
     */
    public function create(string $status): ResponseHandlerInterface
    {
        $status = \strtolower($status);

        if (!isset($this->handlers[$status])) {
            throw new \InvalidArgumentException("HolmBank Handler for transaction status {$status} doesn't exist.");
        }

        return $this->objectManager->create($this->handlers[$status]);
    }
}

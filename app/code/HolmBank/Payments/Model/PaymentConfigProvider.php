<?php

declare(strict_types=1);

namespace HolmBank\Payments\Model;

use HolmBank\Payments\Model\Service\ConfigurationProvider;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\View\Asset\Repository as AssetRepository;

/**
 * @inheritdoc
 */
class PaymentConfigProvider implements ConfigProviderInterface
{
    /**
     * @var AssetRepository
     */
    private AssetRepository $assetRepository;

    /**
     * @var HolmbankProductRepository
     */
    private HolmbankProductRepository $holmbankProductRepository;

    /**
     * @var ConfigurationProvider
     */
    private ConfigurationProvider $configurationProvider;

    /**
     * @param AssetRepository $assetRepository
     * @param HolmbankProductRepository $holmbankProductRepository
     * @param ConfigurationProvider $configurationProvider
     */
    public function __construct(
        AssetRepository $assetRepository,
        HolmbankProductRepository $holmbankProductRepository,
        ConfigurationProvider $configurationProvider
    ) {
        $this->assetRepository = $assetRepository;
        $this->holmbankProductRepository = $holmbankProductRepository;
        $this->configurationProvider = $configurationProvider;
    }

    /**
     * @inheritdoc
     */
    public function getConfig(): array
    {
        $activeProducts = $this->configurationProvider->getActiveProducts();
        $paymentData = [];

        foreach ($activeProducts as $productCode) {
            $paymentData[] = $this->getPaymentData($productCode);
        }

        return ['payment' => ['holm_bank' => $paymentData]];
    }

    /**
     * Retrieve payment data.
     *
     * @param string $productCode
     *
     * @return array
     */
    private function getPaymentData(string $productCode): array
    {
        try {
            $product = $this->holmbankProductRepository->getByProductType($productCode);
        } catch (\Throwable $exception) {
            return [];
        }

        $logoUrl = $product->getHolmBankProductLogo()
            ?: $this->assetRepository->getUrl('HolmBank_Payments::images/logo.png');

        return [
            'isActive' => true,
            'paymentAcceptanceMarkSrc' => $logoUrl,
            'title' => $product->getHolmBankProductName(),
            'type' => $product->getHolmBankProductType(),
        ];
    }
}

<?php

declare(strict_types=1);

namespace HolmBank\Payments\Controller\Payment;

use HolmBank\Payments\Model\HolmbankOrderRepository;
use HolmBank\Payments\Model\Payment;
use Laminas\Http\Response;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Phrase;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Sales\Model\Order;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\OrderRepositoryInterface;
use Psr\Log\LoggerInterface;

class Index extends Action implements CsrfAwareActionInterface
{
    /**
     * @var HolmbankOrderRepository
     */
    private HolmbankOrderRepository $holmbankOrderRepository;

    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * @var OrderRepositoryInterface
     */
    private OrderRepositoryInterface $orderRepository;

    /**
     * @var CheckoutSession
     */
    private CheckoutSession $checkoutSession;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @param Context $context
     * @param HolmbankOrderRepository $holmbankOrderRepository
     * @param SerializerInterface $serializer
     * @param OrderRepositoryInterface $orderRepository
     * @param CheckoutSession $checkoutSession
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        HolmbankOrderRepository $holmbankOrderRepository,
        SerializerInterface $serializer,
        OrderRepositoryInterface $orderRepository,
        CheckoutSession $checkoutSession,
        LoggerInterface $logger
    ) {
        parent::__construct($context);

        $this->holmbankOrderRepository = $holmbankOrderRepository;
        $this->orderRepository = $orderRepository;
        $this->serializer = $serializer;
        $this->checkoutSession = $checkoutSession;
        $this->logger = $logger;
    }

    /**
     * Order success action
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        if ($this->getRequest()->isPost()) {
            return $this->handlePostEvent();
        }

        $orderId = $this->getRequest()->getParam('orderId');
        try {
            $holmOrder = $this->holmbankOrderRepository->getByOrderId($orderId);
        } catch (NoSuchEntityException $e) {
            $this->logger->error('HolmBank payment result error.', ['message' => $e->getMessage()]);

            return $this->redirectToCheckoutCart(__('Something went wrong while processing the order.'));
        }

        $displayStatus = $this->getRequest()->getParam('status', '');
        $loanStatus = (string)$holmOrder->getOrderStatus();

        if (\strtoupper($loanStatus) !== Payment::PENDING_STATUS) {
            $displayStatus = $loanStatus;
        }

        switch (\strtoupper($displayStatus)) {
            case Payment::APPROVED_STATUS:
            case Payment::PENDING_STATUS:
                return $this->resultRedirectFactory
                    ->create()
                    ->setPath('checkout/onepage/success', ['_query' => ['utm_nooverride' => '1']]);
            case Payment::REJECTED_STATUS:
                return $this->redirectToCheckoutCart(__('Transaction declined by gateway.'));
            default:
                return $this->redirectToCheckoutCart(__('Something went wrong while processing the order.'));
        }
    }

    /**
     * Return to cart with error message
     *
     * @param Phrase $message
     * @return Redirect
     */
    private function redirectToCheckoutCart(Phrase $message): Redirect
    {
        $this->checkoutSession->restoreQuote();
        $this->messageManager->addWarningMessage($message);

        return $this->resultRedirectFactory
            ->create()
            ->setPath('checkout/cart', ['_query' => ['utm_nooverride' => '1']]);
    }

    /**
     * Handle post request
     *
     * @return Json
     *
     * @throws NoSuchEntityException
     * @throws CouldNotSaveException
     */
    private function handlePostEvent(): Json
    {
        $reqData = $this->serializer->unserialize($this->getRequest()->getContent());
        $holmOrder = $this->holmbankOrderRepository->getByOrderId($reqData['orderId']);
        $newStatus = \strtoupper($reqData['status']);

        $this->logger->debug('POST request received for ' . $holmOrder->getOrderId() . ' with status ' . $newStatus);

        if ($newStatus !== \strtoupper($holmOrder->getOrderStatus())) {
            $holmOrder->setOrderStatus($newStatus);
            $this->holmbankOrderRepository->save($holmOrder);
            $holmOrder = $this->holmbankOrderRepository->getByOrderId($reqData['orderId']);
        }

        $this->handleStoreOrder($holmOrder->getOrderId(), $holmOrder->getOrderStatus(), $reqData['contractNumber']);

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setHttpResponseCode(Response::STATUS_CODE_202);
        $resultJson->setHeader("Content-Type", "application/json");
        $resultJson->setStatusHeader(Response::STATUS_CODE_202);

        return $resultJson->setData($holmOrder);
    }

    /**
     * Handle webstore order.
     *
     * @param int $orderId
     * @param string $status
     * @param string|null $contractNo
     *
     * @return void
     */
    private function handleStoreOrder(int $orderId, string $status, ?string $contractNo)
    {
        $order = $this->orderRepository->get($orderId);
        switch ($status) {
            case Payment::APPROVED_STATUS:
                $customProcessedStatus = 'makstud';
                $order->setState(Order::STATE_PROCESSING);
                $order->setStatus($customProcessedStatus);
                $order->addStatusToHistory($order->getStatus(), "Holm Bank hire purchase contract (".$contractNo.") paid out");
                break;
            case Payment::REJECTED_STATUS:
                $order->setState(Order::STATE_CANCELED);
                $order->setStatus(Order::STATE_CANCELED);
                $order->addStatusToHistory($order->getStatus(), 'Holm Bank hire purchase contract not signed');
                break;
            default:
                $order->setState(Order::STATE_PENDING_PAYMENT);
                $order->setStatus(Order::STATE_PENDING_PAYMENT);
                $order->addStatusToHistory($order->getStatus(), 'Holm Bank hire purchase contract pending');
        }
        $this->orderRepository->save($order);
        $this->logger->debug("Order " . $order->getEntityId() . " updated to " . $order->getStatus());
    }

    /**
     * @inheritdoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        $requestId = $request->getHeader("x-payment-link-req-id");
        if ($request->isGet()) {
            return true;
        }

        if (!$requestId) {
            $this->logger->error("x-payment-link-req-id is missing");
            return false;
        }

        $reqData = $this->serializer->unserialize($request->getContent());

        try {
            $order = $this->holmbankOrderRepository->getByOrderId($reqData['orderId']);
            if ($order->getRequestId() == $requestId && $order->getHolmbankOrderId() == $reqData['orderId']) {
                return true;
            }
            $this->logger->error("invalid x-payment-link-req-id for order " . $order->getOrderId());
        } catch (NoSuchEntityException $e) {
            $this->logger->error($e->getMessage());
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        $reqData = $this->serializer->unserialize($request->getContent());
        $error = 'Holm Bank order ' . $reqData['orderId'] . ' not found.';

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setHttpResponseCode(400);
        $resultJson->setData(['error' => $error]);

        return new InvalidRequestException($resultJson);
    }
}

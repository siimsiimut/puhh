<?php

declare(strict_types=1);

namespace HolmBank\Payments\Controller\Payment;

use HolmBank\Payments\Api\HolmbankOrderRepositoryInterface;
use HolmBank\Payments\Model\HolmBankOrderFactory;
use HolmBank\Payments\Model\Payment;
use Magento\Checkout\Controller\Action;
use Magento\Checkout\Model\Session;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Sales\Model\Order;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

class Create extends Action
{
    /**
     * @var JsonFactory
     */
    private JsonFactory $resultJsonFactory;

    /**
     * @var Curl
     */
    private Curl $curl;

    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;

    /**
     * @var Json
     */
    private Json $json;

    /**
     * @var Session
     */
    private Session $session;

    /**
     * @var HolmBankOrderFactory
     */
    private HolmBankOrderFactory $holmbankOrderFactory;

    /**
     * @var HolmbankOrderRepositoryInterface
     */
    private HolmbankOrderRepositoryInterface $holmbankOrderRepository;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;
    private LoggerInterface $logger;

    /**
     * Class construct.
     *
     * @param Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param AccountManagementInterface $accountManagement
     * @param JsonFactory $resultJsonFactory
     * @param Curl $curl
     * @param ScopeConfigInterface $scopeConfig
     * @param Json $json
     * @param Session $session
     * @param HolmBankOrderFactory $holmbankOrderFactory
     * @param StoreManagerInterface $storeManager
     * @param HolmbankOrderRepositoryInterface $holmbankOrderRepository
     */
    public function __construct(
        Context                          $context,
        \Magento\Customer\Model\Session  $customerSession,
        CustomerRepositoryInterface      $customerRepository,
        AccountManagementInterface       $accountManagement,
        JsonFactory                      $resultJsonFactory,
        LoggerInterface                  $logger,
        Curl                             $curl,
        ScopeConfigInterface             $scopeConfig,
        Json                             $json,
        Session                          $session,
        HolmBankOrderFactory             $holmbankOrderFactory,
        StoreManagerInterface            $storeManager,
        HolmbankOrderRepositoryInterface $holmbankOrderRepository
    )
    {
        parent::__construct(
            $context,
            $customerSession,
            $customerRepository,
            $accountManagement
        );
        $this->session = $session;
        $this->holmbankOrderFactory = $holmbankOrderFactory;
        $this->logger = $logger;
        $this->storeManager = $storeManager;
        $this->holmbankOrderRepository = $holmbankOrderRepository;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->curl = $curl;
        $this->scopeConfig = $scopeConfig;
        $this->json = $json;
    }

    public function execute()
    {
        $order = $this->session->getLastRealOrder();
        $paymentLinkReqId = uniqid();

        try {
            $apiUrl = $this->scopeConfig->getValue('payment/holm_partner/api_url') . '/api/partners/public/payment-link/orders';
            $xPaymentLinkKey = $this->scopeConfig->getValue('payment/holm_partner/payment_key');
        } catch (LocalizedException $e) {
            throw new LocalizedException(__('Payment method not found.'));
        }

        $paymentAdditionalInformation = $order->getPayment()->getAdditionalInformation();

        if (empty($paymentAdditionalInformation['method_type'])) {
            throw new LocalizedException(__('Payment method is not available.'));
        }

        $productArray = [];
        foreach ($order->getAllVisibleItems() as $item) {
            $productArray[] = [
                "productSKU" => $item->getSku(),
                "productName" => $item->getName(),
                "totalPrice" => $item->getBaseRowTotalInclTax(),
                "quantity" => $item->getQtyOrdered()
            ];
        }
        $shippingArray[] = [
            "productSKU" => 'SHIP',
            "productName" => 'Shipping',
            "totalPrice" => $order->getShippingInclTax(),
            "quantity" => '1',
        ];
        $array = [
            "language" => "et",
            "totalAmount" => $order->getGrandTotal(),
            "paymentType" => strtoupper($paymentAdditionalInformation['method_type']),
            "orderNumber" => $order->getIncrementId(),
            "products" => array_merge($productArray, $shippingArray),
            "returnUrl" => $this->storeManager->getStore()->getUrl('holmbank/payment/index'),
        ];

        if ($order->getDiscountAmount() < 0) {
            $array["products"] = array_merge($array["products"], [[
                "productSKU" => 'DISC',
                "productName" => 'Discount',
                "totalPrice" => $order->getDiscountAmount(),
                "quantity" => '1',
            ]]);
        }

        $this->curl->addHeader("Content-Type", "application/json");
        $this->curl->addHeader("x-payment-link-key", $xPaymentLinkKey);
        $this->curl->addHeader("x-payment-link-req-id", $paymentLinkReqId);

        $payload = $this->json->serialize($array);
        $this->curl->post($apiUrl, $payload);
        $response = $this->curl->getBody();
        $data = $this->json->unserialize($response);
        $this->createHolmbankOrder($data['orderId'], $order, $paymentLinkReqId);

        $result = $this->resultJsonFactory->create();
        $result->setHeader("Content-Type", "application/json");
        return $result->setData([
            'redirectUrl' => $data["orderLink"]
        ]);
    }

    public function createHolmbankOrder($holmBankOrderId, Order $order, $paymentLinkReqId): void
    {
        $orderModel = $this->holmbankOrderFactory->create();
        $orderModel->setOrderId((int)$order->getId());
        $orderModel->setHolmbankOrderId($holmBankOrderId);
        $orderModel->setOrderStatus(Payment::PENDING_STATUS);
        $orderModel->setRequestId($paymentLinkReqId);

        if (null !== $order->getCustomerId()) {
            $orderModel->setCustomerId((int)$order->getCustomerId());
        }

        $this->holmbankOrderRepository->save($orderModel);
    }
}

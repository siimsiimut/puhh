define([
    'jquery',
    'mage/translate',
    'jquery/ui',
], function ($, $t, alert) {
    'use strict';

    return function (widget) {
        $.widget('mage.catalogAddToCart', widget, {
            /**
             * Handler for the form 'submit' event
             *
             * @param {jQuery} form
             */
            submitForm: function (form) {
                this.ajaxSubmit(form);
                this.gtagEvent(form);
            },
            gtagEvent: function (form) {
                if ($('body').hasClass('catalog-product-view')) {
                    var productName = $('[itemprop="name"]').text().trim(),
                    qty = form.find('#qty').val(),
                    sku = form.attr('data-product-sku'),
                    price = $('#product-price-' + form.find('input[name="product"]').val()).attr('data-price-amount');
                } else {
                    var productName = form.parents('.product-item').find('.product-item-link').text().trim(),
                    qty = 1,
                    sku = form.attr('data-product-sku'),
                    price = $('#product-price-' + form.find('input[name="product"]').val()).attr('data-price-amount');
                }
                
                window.dataLayer = window.dataLayer || [];
                function gtag() { dataLayer.push(arguments); }
                dataLayer.push({ ecommerce: null });  // Clear the previous ecommerce object.
                gtag(
                    "event", "add_to_cart",
                    {
                        
                        currency: "EUR",
                        value: parseFloat(qty * price),
                        items: [{
                            item_name: productName,
                            item_id: sku,
                            price: parseFloat(price)
                        }]
                    }
                );
            }
        });

        return $.mage.catalogAddToCart;
    }
});

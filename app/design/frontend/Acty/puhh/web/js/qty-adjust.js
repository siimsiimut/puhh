require(['jquery'], function ($) {
    $(document).on('click', '.qty-adjust', function (event) {
        var qtyInput = $('#qty'),
            val = parseFloat(qtyInput.val()) || 1;

        if ($(this).hasClass('qty-minus')) {
            if (val > 1) {
                qtyInput.val(val - 1);
            }
        } else {
            qtyInput.val(val + 1);
        }
    });
});
require([
    "jquery"
], function($) {
    var subHeader = $('.header.content');
    var pageWrapper = $('.page-wrapper');
    var mobileMenu = $('.mobile-menu-wrap');
    var autoComplete = $('.autocomplete-suggestions');
    var didScroll = false;

    var mobileScroll = function() {
        if( $(window).width() < 768 ) {
            if ($(window).scrollTop() >= subHeader.offset().top && !(subHeader.hasClass('sticky'))) {
                subHeader.addClass('sticky');
                $('.autocomplete-suggestions').addClass('sticky');
                if(mobileMenu.length) {
                    mobileMenu.css('padding-top', (subHeader.height() + 10));
                } else {
                    pageWrapper.css('padding-top', (subHeader.height() + 10));
                }
            } else if ($(window).scrollTop() < 55 && subHeader.hasClass('sticky')) {
                subHeader.removeClass('sticky');
                $('.autocomplete-suggestions').removeClass('sticky');
                if(mobileMenu.length) {
                    mobileMenu.css('padding-top', 0);
                } else {
                    pageWrapper.css('padding-top', 0);
                }
            }
        }
        if ($(window).scrollTop() >= subHeader.offset().top && !(subHeader.hasClass('sticky'))) {

        }
    }

    var raf = window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        window.oRequestAnimationFrame;
    var $window = $(window);
    var lastScrollTop = $window.scrollTop();

    if (raf) {
        loop();
    }

    function loop() {
        if( $(window).width() < 768 ) {
            var scrollTop = $window.scrollTop();
            if (lastScrollTop === scrollTop) {
                raf(loop);
                return;
            } else {
                lastScrollTop = scrollTop;

                // fire scroll function if scrolls verticall
                mobileScroll();
                raf(loop);
            }
        }
    }

    $(window).scroll(function() {
        if( $(window).width() < 768 ) {
            if (didScroll) {
                return;
            }
            didScroll = true;

            mobileScroll();
            setTimeout(function () {
                didScroll = false;
            }, 100);
        }
    });
});

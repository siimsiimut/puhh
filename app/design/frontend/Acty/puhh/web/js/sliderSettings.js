require([
    'jquery',
    'slick'
], function ($) {
    $(document).ready(function () {

        // FRONT PAGE LARGE BANNERS
        $(".brand-slider").not('.slick-initialized').slick({
            infinite: true,
            slidesToShow: 8,
            slidesToScroll: 8,
            dots: true,
            speed: 200,
            arrows: true,
            responsive: [
                {
                    breakpoint: 1300,
                    settings: {
                        slidesToShow: 7,
                        slidesToScroll: 7
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        dots: false
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        dots: false
                    }
                }
            ]
        });

        // PRODUCT VIEW UPSELL PRODUCTS, PRODUCT VIEW RELATED PRODUCTS
        $(".products-upsell > .product-items, .products-related > .product-items").not('.slick-initialized').slick({
            infinite: true,
            slidesToShow: 5,
            slidesToScroll: 5,
            dots: true,
            arrows: false,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 640,
                    settings: "unslick"
                }
            ]
        });
        // HOME SLIDER
        $(".banner-wrapper #slides .inner").not('.slick-initialized').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 4000,
            fade: true
        });
    });
});

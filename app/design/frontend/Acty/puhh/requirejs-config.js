var config = {
    deps: [
        'js/sliderSettings',
        'js/checkout-sticky',
        'js/sticky'
    ],
    paths: {
        slick: 'js/slick.min',
    },
    shim: {
        slick: {
            deps: ['jquery']
        }
    }
};

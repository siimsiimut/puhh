define([
    'ko',
    'jquery',
    'uiComponent'
],function(ko, $, Component){
    return Component.extend({
        qtyOptions: ko.observableArray([]),
        initialize: function(){
            this._super();
            for(var i =1; i<= 10; i++){
                this.qtyOptions.push(i);
            }
            return this;
        }
    });
});

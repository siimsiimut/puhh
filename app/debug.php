<?php
if (!function_exists('backtrace')) {
    function backtrace()
    {
        $logger = new \Zend\Log\Logger();
        $logger->addWriter(
            new \Zend\Log\Writer\Stream(BP . '/var/log/debug_backtrace_' . date('m_d') . '.log')
        );
        $logger->info(\Magento\Framework\Debug::backtrace(true, false));
    }
}

if (!function_exists('debuglog')) {
    function debuglog($message)
    {
        $logger = new \Zend\Log\Logger();
        $logger->addWriter(
            new \Zend\Log\Writer\Stream(BP . '/var/log/debug_log_' . date('m_d') . '.log')
        );
        $logger->info($message);
    }
}

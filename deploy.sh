start=`date +%s`

bin/magento maintenance:enable &&
bin/magento s:up &&
bin/magento setup:di:compile &&

bin/magento setup:static-content:deploy --theme Acty/puhh --area=frontend et_EE &&
bin/magento setup:static-content:deploy --theme Acty/toysoutlet --area=frontend lv_LV &&

bin/magento maintenance:disable

end=`date +%s`

runtime=$((end-start))
minutes=$((runtime / 60))
seconds=$((runtime % 60))

printf "Total deployment time: $minutes min $seconds s \n"

printf "starting admin static deployment \n"
start=`date +%s`
bin/magento setup:static-content:deploy --area=adminhtml et_EE en_US &&
end=`date +%s`

runtime=$((end-start))
minutes=$((runtime / 60))
seconds=$((runtime % 60))

printf "Total admin static deployment time: $minutes min $seconds s \n"

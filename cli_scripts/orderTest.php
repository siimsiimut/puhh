<?php
require dirname(__FILE__) . '/../app/bootstrap.php';
$bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
require dirname(__FILE__) . '/abstract.php';


class changeSku extends AbstractApp
{
    protected $_registry;
    protected $_state;
    protected $_response;
    /** @var \Magento\Catalog\Api\ProductRepositoryInterface; */
    protected $_productRepository;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $_orderRepository;

    public function __construct(
        \Magento\Framework\App\Response\Http $ResponseHttp,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Store\Model\App\Emulation $emulation,
        \Magento\Framework\App\State $state
    )
    {
        $this->_response = $ResponseHttp;
        $this->_orderRepository = $orderRepository;
        $this->_logger = $logger;
        $this->emulation = $emulation;
        $this->_state = $state;
        
    }
   
    public function run()
    {   
        
        $time_start = microtime(true);
        $this->_state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
        $order = $this->_orderRepository->get(72760);

        print_r($order->getShippingAddress()->getCountryId());
        
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        echo 'Total Execution Time: '.round($execution_time, 2).'s'.PHP_EOL;
    }
}

/** @var \Magento\Framework\App\Http $app */
$app = $bootstrap->createApplication('changeSku');
$bootstrap->run($app);
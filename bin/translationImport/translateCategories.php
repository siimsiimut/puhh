<?php
include('db.php');

$csvFile = "categoryLV.csv";

$storeIdsFields = array(
    3 => 3,
    2 => 4,
    4 => 4
);

$conn->set_charset("utf8");
$sql = "SELECT value_id, entity_id, store_id, value FROM catalog_category_entity_varchar LEFT JOIN eav_attribute ON catalog_category_entity_varchar.attribute_id = eav_attribute.attribute_id where attribute_code in ('name') and entity_id not in(1)";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $databaseArray = [];
    while($row = $result->fetch_assoc()) {
        $databaseArray[$row['entity_id']][$row['store_id']] = array('id' => $row['value_id'], 'value' => $row['value']);
    }
} else {
    echo "0 results";
}

$csvDataArray = csvImport($csvFile);
print_r($csvDataArray);

$changes = 0;
$new = 0;

foreach($csvDataArray as $translation){
    if($translation[0] == 'entity_id'){
        continue;
    }
    if(array_key_exists($translation[0], $databaseArray)){
        foreach($storeIdsFields as $store => $field){
            if($translation[$field] != ''){
                if(array_key_exists($store, $databaseArray[$translation[0]])){
                    if($databaseArray[$translation[0]][$store]['value'] != $translation[$field]){
                        $sql = "UPDATE catalog_category_entity_varchar SET value='".$translation[$field]."' WHERE value_id='".$databaseArray[$translation[0]][$store]['id']."'";
                        if ($conn->query($sql) === TRUE) {
                            echo "Lisan kirje muudatuse ".$translation[0]." ".$translation[$field], PHP_EOL;
                            $changes++;
                        } else {
                            echo "Error updating record: " . $conn->error;
                        }
                        
                    }
                }
                else{
                    $sql = "INSERT INTO catalog_category_entity_varchar (attribute_id, store_id, entity_id, value) VALUES (45, ".$store.", ".$translation[0].", '".$translation[$field]."')";
                    if ($conn->query($sql) === TRUE) {
                        echo "Lisan uue kirje ".$translation[0]." ".$translation[$field], PHP_EOL;
                        $new++;
                    } else {
                        echo "Error: " . $sql . "<br>" . $conn->error;
                    }
                }
            }
        }
    }
}

echo "Kokku muudatusi ".$changes, PHP_EOL;
echo "Kokku uusi ".$new, PHP_EOL;

function csvImport($csvFile){
    $csvFile = file($csvFile);
    $csvData = [];
    foreach ($csvFile as $line) {
        $csvData[] = str_getcsv($line);
    }
    return $csvData;
}
?> 
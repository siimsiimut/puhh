<?php
include('db.php');

$csvFile = "attributesLV.csv";

$storeIdsFields = array(
    3 => 2,
    2 => 3,
    4 => 3
);

$conn->set_charset("utf8");
$sql = "SELECT eav_attribute.attribute_code, eav_attribute_option_value.value_id, eav_attribute_option_value.option_id, eav_attribute_option_value.store_id, eav_attribute_option_value.value from
eav_attribute_option_value
LEFT JOIN eav_attribute_option ON eav_attribute_option.option_id = eav_attribute_option_value.option_id
LEFT JOIN eav_attribute ON eav_attribute.attribute_id = eav_attribute_option.attribute_id
where attribute_code not in ('fabric_color', 'fabric_color1') and eav_attribute.entity_type_id in (3,4) and value not REGEXP '^[0-9/]+$';";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $databaseArray = [];
    while($row = $result->fetch_assoc()) {
        $databaseArray[$row['option_id']][$row['store_id']] = array('val_id' => $row['value_id'], 'value' => $row['value']);
    }
} else {
    echo "0 results";
}

$csvDataArray = csvImport($csvFile);

$changes = 0;
$new = 0;

foreach($databaseArray as $opt_id => $data){
    if(array_key_exists(0, $data)){
        $existing = 0;
    }
    elseif(array_key_exists(1, $data)){
        $existing = 1;
    }
    foreach($csvDataArray as $translation){
        if($translation[0] == 'entity_id'){
            continue;
        }
        if($data[$existing]['value'] == $translation[1]){
            foreach($storeIdsFields as $store => $field){
                if($translation[$field] != ''){
                    if(array_key_exists($store, $data)){
                        if($data[$store]['value'] != $translation[$field]){
                            $translatefield = addslashes($translation[$field]);
                            $sql = "UPDATE eav_attribute_option_value SET value='".$translatefield."' WHERE value_id='".$data[$store]['val_id']."'";
                            if ($conn->query($sql) === TRUE) {
                                echo "Lisan kirje muudatuse ".$data[$store]['val_id']." ".$opt_id." ".$translation[0]." ".$translatefield, PHP_EOL;
                                $changes++;
                            } else {
                                echo "Error updating record: " . $conn->error;
                            }
                        }
                    }
                    else{
                        $translatefield = addslashes($translation[$field]);
                        $sql = "INSERT INTO eav_attribute_option_value (option_id, store_id, value) VALUES (".$opt_id.", ".$store.",'".$translatefield."')";
                        if ($conn->query($sql) === TRUE) {
                            echo "Lisan uue kirje ".$translation[0]." ".$translatefield, PHP_EOL;
                            $new++;
                        } else {
                            echo "Error: " . $sql . "<br>" . $conn->error;
                        }
                    }
                }
            }
            break;
        }
    }
}

$sql = "SELECT attribute_label_id, attribute_id, store_id, value FROM eav_attribute_label;";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $databaseArray = [];
    while($row = $result->fetch_assoc()) {
        $databaseArray[$row['attribute_id']][$row['store_id']] = array('attrl_id' => $row['attribute_label_id'], 'value' => $row['value']);
    }
} else {
    echo "0 results";
}


foreach($databaseArray as $attr_id => $data){
    if(array_key_exists(0, $data)){
        $existing = 0;
    }
    elseif(array_key_exists(1, $data)){
        $existing = 1;
    }
    foreach($csvDataArray as $translation){
        if($translation[0] == 'entity_id'){
            continue;
        }
        if($data[$existing]['value'] == $translation[1]){
            foreach($storeIdsFields as $store => $field){
                if(array_key_exists($store, $data)){
                    if($data[$store]['value'] != $translation[$field]){
                        $translatefield = addslashes($translation[$field]);
                        $sql = "UPDATE eav_attribute_label SET value='".$translatefield."' WHERE attribute_label_id='".$data[$existing]['attrl_id']."'";
                        if ($conn->query($sql) === TRUE) {
                            echo "Lisan kirje muudatuse ".$translation[0]." ".$translatefield, PHP_EOL;
                            $changes++;
                        } else {
                            echo "Error updating record: " . $conn->error;
                        }
                    }
                }
                else{
                    $translatefield = addslashes($translation[$field]);
                    $sql = "INSERT INTO eav_attribute_label (attribute_id, store_id, value) VALUES (".$attr_id.", ".$store.",'".$translatefield."')";
                    if ($conn->query($sql) === TRUE) {
                        echo "Lisan uue kirje ".$translation[0]." ".$translatefield, PHP_EOL;
                        $new++;
                    } else {
                        echo "Error: " . $sql . "<br>" . $conn->error;
                    }
                }
            }
            break;
        }
    }
}

echo "Kokku muudatusi ".$changes, PHP_EOL;
echo "Kokku uusi ".$new, PHP_EOL;

function csvImport($csvFile){
    $csvFile = file($csvFile);
    $csvData = [];
    foreach ($csvFile as $line) {
        $csvData[] = str_getcsv($line);
    }
    return $csvData;
}
?> 
<?php
include('db.php');

$csvFile = "productsLV3.csv";

$storeIdsFields = array(
    3 => 4
);

$conn->set_charset("utf8");
$sql = "SELECT `catalog_product_entity_varchar`.`entity_id` as entity_id, sku, catalog_product_entity_varchar.attribute_id, attribute_code, value, store_id, value_id FROM catalog_product_entity_varchar LEFT JOIN eav_attribute ON catalog_product_entity_varchar.attribute_id = eav_attribute.attribute_id left join catalog_product_entity on catalog_product_entity_varchar.entity_id= catalog_product_entity.entity_id where attribute_code in ('name') and value is not null and sku is not null;";
$result = $conn->query($sql);

$sqld = "SELECT `catalog_product_entity_text`.`entity_id` as entity_id, sku, catalog_product_entity_text.attribute_id, attribute_code, value, store_id, value_id FROM catalog_product_entity_text LEFT JOIN eav_attribute ON catalog_product_entity_text.attribute_id = eav_attribute.attribute_id left join catalog_product_entity on catalog_product_entity_text.entity_id= catalog_product_entity.entity_id where attribute_code in ('description') and value is not null and sku is not null;";
$resultdesc = $conn->query($sqld);

if ($result->num_rows > 0) {
    $databaseArray = [];
    while($row = $result->fetch_assoc()) {
        $databaseArray[$row['sku']][$row['attribute_code']][$row['store_id']] = array('ent_id' => $row['entity_id'], 'val_id' => $row['value_id'], 'attr_id' => $row['attribute_id'], 'value' => $row['value']);
    }
} else {
    echo "0 names";
}
if ($resultdesc->num_rows > 0) {
    while($row = $resultdesc->fetch_assoc()) {
        $databaseArray[$row['sku']][$row['attribute_code']][$row['store_id']] = array('ent_id' => $row['entity_id'], 'val_id' => $row['value_id'], 'attr_id' => $row['attribute_id'], 'value' => $row['value']);
    }
} else {
    echo "0 descriptions";
}

$csvDataArray = csvImport($csvFile);

$changes = 0;
$new = 0;

print_r(sizeof($databaseArray));
foreach($csvDataArray as $translation){
    //print_r($translation);
    if($translation[0] == 'entity_id'){
        continue;
    }
    if(array_key_exists($translation[1], $databaseArray)){
        if(array_key_exists($translation[2], $databaseArray[$translation[1]])){
            foreach($storeIdsFields as $store => $field){
                if($translation[$field] != ''){
                    if(array_key_exists($store, $databaseArray[$translation[1]][$translation[2]])){
                        if($databaseArray[$translation[1]][$translation[2]][$store]['value'] != $translation[$field]){
                            $translatefield = addslashes($translation[$field]);
                            if($translation[2] == "name"){
                                $sql = "UPDATE catalog_product_entity_varchar SET value='".$translatefield."' WHERE value_id='".$databaseArray[$translation[1]][$translation[2]][$store]['val_id']."'";
                                if ($conn->query($sql) === TRUE) {
                                    echo "Lisan kirje muudatuse ".$translation[0]." ".$translation[$field], PHP_EOL;
                                    $changes++;
                                } else {
                                    echo "Error updating record: " . $conn->error;
                                }
                            }
                            elseif($translation[2] == "description"){
                                $sql = "UPDATE catalog_product_entity_text SET value='".$translatefield."' WHERE value_id='".$databaseArray[$translation[1]][$translation[2]][$store]['val_id']."'";
                                if ($conn->query($sql) === TRUE) {
                                    echo "Lisan kirje muudatuse ".$translation[0]." ".$translation[$field], PHP_EOL;
                                    $changes++;
                                } else {
                                    echo "Error updating record: " . $conn->error;
                                }
                           }
                        }
                    }
                    else{
                        if($translation[2] == "name"){
                            $attribute_id = $databaseArray[$translation[1]][$translation[2]][0]['attr_id'];
                            $translatefield = addslashes($translation[$field]);
                            $sql = "INSERT INTO catalog_product_entity_varchar (attribute_id, store_id, entity_id, value) VALUES (".$attribute_id.", ".$store.", ".$translation[0].", '".$translatefield."')";
                            if ($conn->query($sql) === TRUE) {
                                echo "Lisan uue kirje ".$translation[0]." ".$translation[$field], PHP_EOL;
                                $new++;
                            } else {
                                echo "Error: " . $sql . "<br>" . $conn->error;
                            }
                        }
                        elseif($translation[2] == "description"){
                            $attribute_id = $databaseArray[$translation[1]][$translation[2]][0]['attr_id'];
                            $translatefield = addslashes($translation[$field]);
                            $sql = "INSERT INTO catalog_product_entity_text (attribute_id, store_id, entity_id, value) VALUES (".$attribute_id.", ".$store.", ".$translation[0].", '".$translatefield."')";
                            if ($conn->query($sql) === TRUE) {
                                echo "Lisan uue kirje ".$translation[0]." ".$translation[$field], PHP_EOL;
                                $new++;
                            } else {
                                echo "Error: " . $sql . "<br>" . $conn->error;
                            }
                        }
                    }
                }
            }
        }
        elseif($translation[2] == "description"){
            if($translation[3] != ''){
                $attribute_id = 75;
                $defaultstore = 0;
                $translatefield = addslashes($translation[3]);
                $sql = "INSERT INTO catalog_product_entity_text (attribute_id, store_id, entity_id, value) VALUES (".$attribute_id.", ".$defaultstore.", ".$translation[0].", '".$translatefield."')";
                if ($conn->query($sql) === TRUE) {
                    echo "Lisan uue kirje ".$translation[0]." ".$translation[3], PHP_EOL;
                    $new++;
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                foreach($storeIdsFields as $store => $field){
                    if($translation[$field] != ''){
                        $translatefield = addslashes($translation[$field]);
                        $sql = "INSERT INTO catalog_product_entity_text (attribute_id, store_id, entity_id, value) VALUES (".$attribute_id.", ".$store.", ".$translation[0].", '".$translatefield."')";
                        if ($conn->query($sql) === TRUE) {
                            echo "Lisan uue kirje ".$translation[0]." ".$translation[$field], PHP_EOL;
                            $new++;
                        } else {
                            echo "Error: " . $sql . "<br>" . $conn->error;
                        }
                    }
                }
            }
        }
    }
}

echo "Kokku muudatusi ".$changes, PHP_EOL;
echo "Kokku uusi ".$new, PHP_EOL;

function csvImport($csvFile){
    $file = fopen($csvFile, "r");
    $csvData = [];
    while (($line = fgetcsv($file, 0, ',', '"')) !== false) {
        $csvData[] = $line;
    }
    fclose($file);
    return $csvData;
}
/*
function csvImport($csvFile){
    $csvFile = file($csvFile);
    $csvData = [];
    foreach ($csvFile as $line) {
        $csvData[] = str_getcsv($line);
    }
    return $csvData;
}*/
?> 
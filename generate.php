<?php
/**
 * Copyright © Lumav Commerce. All rights reserved.
 */

use Magento\Framework\App\Bootstrap;
require __DIR__ . '/app/bootstrap.php';

$bootstrap = Bootstrap::create(BP, $_SERVER);

$obj = $bootstrap->getObjectManager();

$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

$generator = $obj->get('\Lumav\XmlFeed\Cron\Generator');



echo nl2br('Start!').PHP_EOL;
$startTime = microtime(true);

$generator->generate(false);


$endTime = microtime(true);
echo nl2br('Total importing time: ' . ($endTime - $startTime)) . PHP_EOL;
